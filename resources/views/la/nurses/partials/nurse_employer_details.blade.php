<div id="tab_6" class="tab-pane " >
                                
                                <div class="content-head">
                                                <h4>Employer Details</h4>
                                                <!-- <div class="id">Nurse ID : <b>#12675</b></div> -->
                                        </div>
                                        {!! Form::open(['action' => 'LA\NursesController@store', 'id' => 'employer-det-form']) !!}
               
                        <div class="pane">
                                
                                <div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
														<select name="main_agent" id="main_agent" class="searchSelect">
                                                            <option value="Not-applicable">Not Applicable</option>
															<?php if(count($employers) > 0) { 
																foreach($employers as $employer) { ?>
																	<option value="<?php echo $employer->name; ?>"><?php echo $employer->name; ?> (<?php echo $employer->userType; ?>)</option>	
															<?php 
																}
															} ?>											
														</select>
														<label for="employer" style="margin-top: -40px;font-size: 13px;">Employer/ Main Agent</label>
                                                </div>


                                        </div>
                                        <div class=" col s6 ">
                                        <div class="input-field">
                                                        <select name="work_site" id="work_site" class="formSelect">
															<option value="">Select</option>
                                                            <option value="Care-home">Care Home</option>
                                                            <option value="Hospitals">Hospitals</option>
                                                            <option value="not-applicable">Not Applicable</option>
														</select>												
													<label for="work_site">Work Site  (Care Home/Hospitals) </label>
											</div>
                                        </div>
                                </div>

                                <div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
                                                <select name="mode_interview" id="mode_interview" class="formSelect">
															<option value="">Select</option>
                                                            <option value="Skype">Skype</option>
                                                            <option value="Zoom">Zoom</option>
                                                            <option value="Google-Meet">Google Meet</option>
                                                            <option value="WhatsApp">WhatsApp</option>
                                                            <option value="Face-to-face">Face to face</option>
                                                            <option value="Others">Others</option>
                                                            <option value="Not-Applicable">Not Applicable</option>
														</select>												
													<label for="mode_interview">Mode of Interview</label>
                                                </div>
                                        </div>
										
										<div class="col s6 hide" id="mode_interview_wrapper">
                                            <div class="input-field">
                                                <input name="mode_interview_text"  id="mode_interview_text" type="text" class="datepicker">
												<label for="mode_interview_text">If others specify</label>
                                            </div>    
										</div>	
                                </div>
								
								<div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
                                                <input name="interview_schedule_date"  id="interview_schedule_date" type="text" class="datepicker">
												<label for="interview_schedule_date">Interview Schedule Date</label>
                                                </div>
                                        </div>
										
										<div class="col s6">
                                            <div class="input-field">
                                                <input name="interview_schedule_time"  id="interview_schedule_time" type="text" class="timepicker">
												<label for="interview_schedule_time">Interview Schedule Time</label>
                                            </div>    
										</div>	
                                </div>							
								<div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
                                                <select name="interview_status" id="interview_status" class="formSelect">
															<option value="">Select</option>
                                                            <option value="Passed">Passed</option>
                                                            <option value="Failed">Failed</option>
                                                            <option value="Missed">Missed</option>
                                                            <option value="Not-Applicable">Not Applicable</option>
														</select>												
													<label for="interview_status">Interview Status </label>
                                                </div>


                                        </div>
										
										<div class="col s6">
                                        <div class="input-field">
                                                <select name="offer_letter_status" id="offer_letter_status" class="formSelect">
															<option value="">Select</option>
                                                            <option value="Waiting">Waiting</option>
                                                            <option value="Received">Received</option>
                                                            <option value="Rejected">Rejected</option>
                                                            <option value="Accepted">Accepted</option>
														</select>												
													<label for="offer_letter_status">Offer Letter Status </label>
                                                </div>
										</div>	
                                </div>

								<div id="off-letter-det" class="hide">
									<div class="row">
											<div class=" col s6 ">
													<div class="input-field">
													<input name="offer_letter_date"  id="offer_letter_date" type="text" class="datepicker">
													<label for="offer_letter_date">Offer Letter Received Date</label>
													</div>


											</div>
											
											<div class="col s6">
												<div class="input-field">
													<input name="offer_letter_signed"  id="offer_letter_signed" type="text" class="datepicker">
													<label for="offer_letter_signed">Offer Letter Signed Date</label>
												</div>    
											</div>	
									</div>			
									<div class="row">
											<div class=" col s6 ">
											<label>Offer Letter File Upload</label>
													<div class="file-field input-field">


																	<div class="btn7">
																			<span>Upload</span>
																			<input type="file"><i class="fa fa-cloud-upload"></i> 
																	</div>
																	<div class="file-path-wrapper">
																			<input class="file-path validate" name="offer_letter_file"  type="text">
																		  

																	</div>
													</div>
												  
													<div class="uploaded-file" id="offer_letter_file" data-type="file"></div>
											</div>	
									</div>
                                </div>
                        </div>
                        <div class="controls">
                                        <a class="mgbtn2">Back</a>
                                        <input id="form_name" type="hidden" class="validate" name="type_of" value="employer_details">
                                        <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                        {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
                </div>

                