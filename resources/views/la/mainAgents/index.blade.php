@extends("la.layouts.app")

@section("contentheader_title", "MainAgents")
@section("contentheader_description", "MainAgents listing")
@section("section", "MainAgents")
@section("sub_section", "Listing")
@section("htmlheader_title", "MainAgents Listing")

@section("headerElems")
@la_access("Employees", "create")
	<button class="btn btn-success btn-sm pull-right addbtn" data-toggle="modal" data-target="#AddModal"><i class="icon-user-follow"></i> Add MainAgent</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('success_message'))
	<div class="alert alert-success">{{ session('success_message') }}</div>
@endif



<!--  Staff filter -->   
@if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER"])) 
<!--<div class="reportfilter">
	<div class="row">
		<div class="col-md-9">
		<div class="clearfix">
	@if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER"]))
	<div class="col-md-6">	
		<div class="form-group">
			<select name="staff_id" id="staff_id" class="searchSelect">
				<option value="">Select Staff</option>
				<?php
					/*foreach($staffs as $staffsData) {
						echo '<option id="staff_data" value="'.$staffsData->id.'">'.$staffsData->name.'</option>';
					}*/
				?>
			</select>
		</div>
	</div>
	@else
		<input type="hidden" name="staff_id" id="staff_id">
	@endif
		</div>
		</div>

	<div class="col-md-3">
		<div class="col-md-8 pl0"><div class="form-group">
			<button class="btn btn-success btn-sm pull-right searchbtn"><span class="fa fa-search"></span> Search</button>
		</div>
    	</div>
    	<div class="col-md-4 pl0"><div class="form-group">
			<button class="btn btn-success btn-sm pull-right clearbtn"><span class="fa fa-times"></span> Clear</button>
		</div>
		</div>
	</div>

	</div>
</div> -->
<br>
@endif
<!-- End staff listing -->



<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-hover  mg-table mgt2">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			@endforeach
			<th>Assigned Staff</th>
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Employees", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog mgmodal-dialog" role="document">
		<div class="modal-content mgmodal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add MainAgent</h4>
			</div>
			<!-- {!! Form::open(['action' => 'LA\MainAgentsController@store', 'id' => 'employee-add-form']) !!} -->
			{!! Form::open(['route' => [config('laraadmin.adminRoute') . '.'.$type.'.main_agents.store'], 'id' => 'employee-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
                    @la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'designation')
					@la_input($module, 'gender')
					@la_input($module, 'mobile')
					@la_input($module, 'mobile2')
					@la_input($module, 'email')
					@la_input($module, 'dept')
					@la_input($module, 'city')
					@la_input($module, 'address')
					@la_input($module, 'about')
					@la_input($module, 'date_birth')
					--}}
					<div class="form-group">
						<label for="Profile image" style="display:block;">Profile image :</label>
						<input class="form-control" placeholder="Enter Profile image" id="profile-pic" name="profilePic" type="hidden" value="">
						<a class="btn btn-default" data-url="<?php echo url('/'); ?>" id="mx_upload_image" file_type="image" selecter="Profile image">Upload <i class="fa fa-cloud-upload"></i></a>
						<br/>
						<div class="uploaded_image hide" id="view_upload_image">
							<a href="#" target="_blank"><img src=""></a>
							<i title="Remove Image" class="fa fa-times"></i>
						</div>
					</div>						
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default mgbtn2" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn mgbtn']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@include('la.common.imageUpload')
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/js/common.js') }}"></script>

<!-- Search -->
<!-- <script src="{{ asset('la-assets/stdentsform/js/main.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> -->
<!-- Search -->

<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/mainAgents_dt_ajax') }}?type=<?php echo $type; ?>",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#employee-add-form").validate({
		
	});

	/* Search by Staff */
	$('body').on('click', '.searchbtn', function() { 
		var staff_id = document.getElementById("staff_id").value;
		$("#example1").dataTable().fnDestroy(); 
		$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/mainAgents_dt_ajax') }}?type=<?php echo $type; ?>&staff_id="+staff_id,
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
		});
	});
	/* End Search by Staff */

	/* Clear Staff Selection */
	$('body').on('click', '.clearbtn', function() {
		$("#staff_id").val('');
		$("#example1").dataTable().fnDestroy();
		$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/mainAgents_dt_ajax') }}?type=<?php echo $type; ?>",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
		});
	});
	/* End Clear Staff */

});
</script>
@endpush
