<div id="tab_5" class="tab-pane " >
        
<div class="content-head">
                                                <h4>CBT Details</h4>
                                                <!-- <!-- <div class="id">Nurse ID : <b>#12675</b></div> --> -->
                                </div>
                                {!! Form::open(['action' => 'LA\NursesController@store', 'id' => '']) !!}
               
                                <div class="pane">


                                        <div class="row">
                                                <div class=" col s12 ">
                                                    <p>Have you cleared CBT ?</p>
													<p>
														<label>
																		<input name="cbt_cleared" type="radio" value="1" />
																		<span>Yes</span>
																	  </label>
																	  <label>
																				<input name="cbt_cleared" type="radio"  value="0" />
																				<span>No</span>
																			  </label>
													</p>
                                                                      <br>
                                                </div>

                                        </div>
                                        <div id="cbt-details" style="display: none;">
                                        <div class="row">
											<div class="col s6">
												<div class="input-field">
														<input name="cbt_attempt_date" id="cbt_attempt_date" class="datepicker" type="text">												
														<label for="cbt_attempt_date">CBT Attempt Date</label>
												</div>
											</div>
											<div class=" col s6 ">
													<div class="input-field">
															<input name="cbt_attempt_status" id="cbt_attempt_status" type="text">												
															<label for="cbt_attempt_status">CBT Attempt Status</label>
													</div>
											</div>
                                        </div>
										
                                        <div class="row">
											<div class="col s6">
												<div class="input-field">
														<input name="cbt_passed_date" id="cbt_passed_date" class="datepicker" type="text">												
														<label for="cbt_passed_date">CBT Passed Date </label>
												</div>
											</div>
											<div class=" col s6 ">
													<div class="input-field">
															<input name="cbt_attempts_no" id="cbt_attempts_no" type="number">												
															<label for="cbt_attempt_status">CBT Number of Attempts </label>
													</div>
											</div>
                                        </div>
										
                                        <div class="row">
                                                <div class="col s3">
                                                        <label>CBT Score Upload</label>
                                                        <div class="file-field input-field">
																<div class="file-field input-field">
																				<div class="btn">
																						<span>Upload</span>
																						<input type="file" name="cbt_score_upload"> <i class="fa fa-cloud-upload"></i> 
																				</div>
																		<div class="file-path-wrapper">
																				<input class="file-path validate">

																				<div>
																				</div>
																
																		</div>
																</div>

																<div class="uploaded-file" id="cbt_score_upload" data-type="file"></div>																
                                                        </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
								
                                <div class="controls">
                                                <a class="mgbtn2">Back</a>
                                                <input id="form_name" type="hidden" class="validate" name="type_of" value="cbt">
                                                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                                                {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
                        

</div>

                