<div id="tab_5" class="tab-pane" >
        
                   
<div class="content-head">
                                        
                                        <h4>Employment History</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                        </div>
                        {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'personal-add-form']) !!}

                <div class="pane">
					<div id="employement-wrapper">
					<div class="stdhst">
							<div class="index">1</div>
							<div class="col-md-6">
								<div class="mg-input-field ">
									<label for="employer_name">Name of employer/organisation</label>
									<input class="form-control" id="employer_name" name="employer_name" type="text">										
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="mg-input-field ">
									<label for="work_nature">Nature of work</label>
									<input class="form-control" id="work_nature" name="work_nature" type="text">								
								</div>
							</div>
							<div class="col-md-4">
									<div class="mg-input-field ">
											<label for="from_date">From</label>
											<input class="form-control datepicker" id="from_date" name="from_date" type="text">
									</div>
							</div>
							<div class="col-md-4">
									<div class="mg-input-field ">
											<label for="to_date">To</label>
											<input class="form-control datepicker" id="to_date" name="to_date" type="text">
									</div>
							</div>

							<div class="col-md-4">
									<div class="mg-input-field ">
											<label for="work_mode">Mode of work</label>
											<select id="work_mode" name="work_mode">
												  <option value="">Select</option>
												  <option value="Full-time"> Full time </option>
												  <option value="Part-time"> Part-time </option>											
											</select>
									</div>
							</div>	
							
							<div class="col-md-6">
											<label>Experience letter</label>
											<div class="file-field input-field">
															<div class="btn">
																	<span>Upload</span>
																	<input type="file" multiple > <i class="fa fa-cloud-upload"></i>
															</div>
															<div class="file-path-wrapper">
																	<input class="file-path validate" name="experience_letter" type="text" placeholder = "Upload file" >
															</div>
											</div>											
											<div class="uploaded-file" id="experience_letter" data-type="file"></div>
							</div>	
							
							
							<div class="clearfix"></div>
					</div>
					</div>
					<input type="hidden" value="employement" name="formtype" class="formtype">
					<div class="btn addshbtn"> <span class="glyphicon glyphicon-plus"></span></div>						
                </div>
				<input type="hidden" value="1" class="wrapper-count">						
                <div class="controls">
                                <a class="mgbtn2 back_button">Back</a>
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="employement_history">
                                 <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                              
                                {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                        </div>
                        {{ Form::close() }}
</div>


