#!/bin/bash

#Get servers list
set -f
string=$PROD_DEPLOY_SERVER
array=(${string//,/ })

#Iterate servers for deploy and pull last commit
for i in "${!array[@]}"; do
  echo "Deploy project on server ${array[i]}"
  ssh -p 50505 -o StrictHostKeyChecking=no ubuntu@${array[i]} "cd /var/www/maxglobal/ && sudo git pull origin master"
done