<div id="tab_8" class="tab-pane" >
        
<div class="content-head">

<h4>Further Details</h4>
<!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
</div>
{!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'misc-add-form']) !!}
               
<div class="pane">

	<div class="row">
			<div class="col s6">
					<div class="input-field ">
						<select name="aboutUs" id="aboutUs">
							<option value="">Select</option>
							<option value="Friend"> Friend </option>
							<option value="Website"> Website </option>
							<option value="social media"> social media </option>
						</select>
						<label for="aboutUs">How Did You Hear About Us?</label>
					</div>
			</div>
	</div>
	
	<h4>Next of Kin (Name and contact no.of family or friend to be contacted in emergency)</h4>

	<div class="row">
			<div class="col s3">
				<div class="input-field ">
					<input id="nxt_kin_country_code" name="nxt_kin_country_code" type="number" maxlength="4" class="">
					<label for="nxt_kin_country_code">Country code</label>
				</div>
			</div>
			<div class="col s6">
				<div class="input-field ">
					<input id="nxt_kin_phone" name="nxt_kin_phone" type="number" maxlength="15" class="">
					<label for="nxt_kin_phone">Phone number</label>
				</div>
			</div>		
	</div>	
	<div class="row">
			<div class="col s6">
				<div class="input-field ">
					<input id="skype_id" name="skype_id" type="text" maxlength="15" class="">
					<label for="nxt_kin_phone">Skype ID</label>
				</div>
			</div>
			<div class="col s6">
							<label for="">Skype profile screen shot</label>
							<div class="file-field input-field">
									<div class="btn">
											<span>Upload</span>
											<input type="file" multiple > <i class="fa fa-cloud-upload"></i>
									</div>
									<div class="file-path-wrapper">
											<input class="file-path validate" name="skpe_screenshot" type="text" placeholder = "Upload file" >
									</div>
							</div>
							<div class="uploaded-file" id="skpe_screenshot" data-type="file"></div>
			</div>
	</div>
	<div class="row">
			<div class="col s6">
				<div class="input-field ">
					<select name="country_permanent" id="country_permanent">
						<option value="">Select</option>
						<option value="Afghanistan"> Afghanistan </option>
						<option value="Albania"> Albania </option>
						<option value="Algeria"> Algeria </option>
						<option value="Andorra"> Andorra </option>
						<option value="Angola"> Angola </option>
						<option value="Antigua and Barbuda"> Antigua and Barbuda </option>
						<option value="Argentina"> Argentina </option>
						<option value="Armenia"> Armenia </option>
						<option value="Australia"> Australia </option>
						<option value="Austria"> Austria </option>
						<option value="Azerbaijan"> Azerbaijan </option>
						<option value="Bahamas"> Bahamas </option>
						<option value="Bahrain"> Bahrain </option>
						<option value="Bangladesh"> Bangladesh </option>
						<option value="Barbados"> Barbados </option>
						<option value="Belarus"> Belarus </option>
						<option value="Belgium"> Belgium </option>
						<option value="Belize"> Belize </option>
						<option value="Benin"> Benin </option>
						<option value="Bhutan"> Bhutan </option>
						<option value="Bolivia"> Bolivia </option>
						<option value="Bosnia and Herzegovina"> Bosnia and Herzegovina </option>
						<option value="Botswana"> Botswana </option>
						<option value="Brazil"> Brazil </option>
						<option value="Brunei"> Brunei </option>
						<option value="Bulgaria"> Bulgaria </option>
						<option value="Burkina Faso"> Burkina Faso </option>
						<option value="Burundi"> Burundi </option>
						<option value="Cabo Verde"> Cabo Verde </option>
						<option value="Cambodia"> Cambodia </option>
						<option value="Cameroon"> Cameroon </option>
						<option value="Canada"> Canada </option>
						<option value="Central African Republic (CAR)"> Central African Republic (CAR) </option>
						<option value="Chad"> Chad </option>
						<option value="Chile"> Chile </option>
						<option value="China"> China </option>
						<option value="Colombia"> Colombia </option>
						<option value="Comoros"> Comoros </option>
						<option value="Congo, Democratic Republic of the"> Congo, Democratic Republic of the </option>
						<option value="Congo, Republic of the"> Congo, Republic of the </option>
						<option value="Costa Rica"> Costa Rica </option>
						<option value="Cote d'Ivoire"> Cote d'Ivoire </option>
						<option value="Croatia"> Croatia </option>
						<option value="Cuba"> Cuba </option>
						<option value="Cyprus"> Cyprus </option>
						<option value="Czechia"> Czechia </option>
						<option value="Denmark"> Denmark </option>
						<option value="Djibouti"> Djibouti </option>
						<option value="Dominica"> Dominica </option>
						<option value="Dominican Republic"> Dominican Republic </option>
						<option value="Ecuador"> Ecuador </option>
						<option value="Egypt"> Egypt </option>
						<option value="El Salvador"> El Salvador </option>
						<option value="Equatorial Guinea"> Equatorial Guinea </option>
						<option value="Eritrea"> Eritrea </option>
						<option value="Estonia"> Estonia </option>
						<option value="Eswatini (formerly Swaziland)"> Eswatini (formerly Swaziland) </option>
						<option value="Ethiopia"> Ethiopia </option>
						<option value="Fiji"> Fiji </option>
						<option value="Finland"> Finland </option>
						<option value="France"> France </option>
						<option value="Gabon"> Gabon </option>
						<option value="Gambia"> Gambia </option>
						<option value="Georgia"> Georgia </option>
						<option value="Germany"> Germany </option>
						<option value="Ghana"> Ghana </option>
						<option value="Greece"> Greece </option>
						<option value="Grenada"> Grenada </option>
						<option value="Guatemala"> Guatemala </option>
						<option value="Guinea"> Guinea </option>
						<option value="Guinea-Bissau"> Guinea-Bissau </option>
						<option value="Guyana"> Guyana </option>
						<option value="Haiti"> Haiti </option>
						<option value="Honduras"> Honduras </option>
						<option value="Hungary"> Hungary </option>
						<option value="Iceland"> Iceland </option>
						<option value="India"> India </option>
						<option value="Indonesia"> Indonesia </option>
						<option value="Iran"> Iran </option>
						<option value="Iraq"> Iraq </option>
						<option value="Ireland"> Ireland </option>
						<option value="Israel"> Israel </option>
						<option value="Italy"> Italy </option>
						<option value="Jamaica"> Jamaica </option>
						<option value="Japan"> Japan </option>
						<option value="Jordan"> Jordan </option>
						<option value="Kazakhstan"> Kazakhstan </option>
						<option value="Kenya"> Kenya </option>
						<option value="Kiribati"> Kiribati </option>
						<option value="Kosovo"> Kosovo </option>
						<option value="Kuwait"> Kuwait </option>
						<option value="Kyrgyzstan"> Kyrgyzstan </option>
						<option value="Laos"> Laos </option>
						<option value="Latvia"> Latvia </option>
						<option value="Lebanon"> Lebanon </option>
						<option value="Lesotho"> Lesotho </option>
						<option value="Liberia"> Liberia </option>
						<option value="Libya"> Libya </option>
						<option value="Liechtenstein"> Liechtenstein </option>
						<option value="Lithuania"> Lithuania </option>
						<option value="Luxembourg"> Luxembourg </option>
						<option value="Madagascar"> Madagascar </option>
						<option value="Malawi"> Malawi </option>
						<option value="Malaysia"> Malaysia </option>
						<option value="Maldives"> Maldives </option>
						<option value="Mali"> Mali </option>
						<option value="Malta"> Malta </option>
						<option value="Marshall Islands"> Marshall Islands </option>
						<option value="Mauritania"> Mauritania </option>
						<option value="Mauritius"> Mauritius </option>
						<option value="Mexico"> Mexico </option>
						<option value="Micronesia"> Micronesia </option>
						<option value="Moldova"> Moldova </option>
						<option value="Monaco"> Monaco </option>
						<option value="Mongolia"> Mongolia </option>
						<option value="Montenegro"> Montenegro </option>
						<option value="Morocco"> Morocco </option>
						<option value="Mozambique"> Mozambique </option>
						<option value="Myanmar (formerly Burma)"> Myanmar (formerly Burma) </option>
						<option value="Namibia"> Namibia </option>
						<option value="Nauru"> Nauru </option>
						<option value="Nepal"> Nepal </option>
						<option value="Netherlands"> Netherlands </option>
						<option value="New Zealand"> New Zealand </option>
						<option value="Nicaragua"> Nicaragua </option>
						<option value="Niger"> Niger </option>
						<option value="Nigeria"> Nigeria </option>
						<option value="North Korea"> North Korea </option>
						<option value="North Macedonia (formerly Macedonia)"> North Macedonia (formerly Macedonia) </option>
						<option value="Norway"> Norway </option>
						<option value="Oman"> Oman </option>
						<option value="Pakistan"> Pakistan </option>
						<option value="Palau"> Palau </option>
						<option value="Palestine"> Palestine </option>
						<option value="Panama"> Panama </option>
						<option value="Papua New Guinea"> Papua New Guinea </option>
						<option value="Paraguay"> Paraguay </option>
						<option value="Peru"> Peru </option>
						<option value="Philippines"> Philippines </option>
						<option value="Poland"> Poland </option>
						<option value="Portugal"> Portugal </option>
						<option value="Qatar"> Qatar </option>
						<option value="Romania"> Romania </option>
						<option value="Russia"> Russia </option>
						<option value="Rwanda"> Rwanda </option>
						<option value="Saint Kitts and Nevis"> Saint Kitts and Nevis </option>
						<option value="Saint Lucia"> Saint Lucia </option>
						<option value="Saint Vincent and the Grenadines"> Saint Vincent and the Grenadines </option>
						<option value="Samoa"> Samoa </option>
						<option value="San Marino"> San Marino </option>
						<option value="Sao Tome and Principe"> Sao Tome and Principe </option>
						<option value="Saudi Arabia"> Saudi Arabia </option>
						<option value="Senegal"> Senegal </option>
						<option value="Serbia"> Serbia </option>
						<option value="Seychelles"> Seychelles </option>
						<option value="Sierra Leone"> Sierra Leone </option>
						<option value="Singapore"> Singapore </option>
						<option value="Slovakia"> Slovakia </option>
						<option value="Slovenia"> Slovenia </option>
						<option value="Solomon Islands"> Solomon Islands </option>
						<option value="Somalia"> Somalia </option>
						<option value="South Africa"> South Africa </option>
						<option value="South Korea"> South Korea </option>
						<option value="South Sudan"> South Sudan </option>
						<option value="Spain"> Spain </option>
						<option value="Sri Lanka"> Sri Lanka </option>
						<option value="Sudan"> Sudan </option>
						<option value="Suriname"> Suriname </option>
						<option value="Sweden"> Sweden </option>
						<option value="Switzerland"> Switzerland </option>
						<option value="Syria"> Syria </option>
						<option value="Taiwan"> Taiwan </option>
						<option value="Tajikistan"> Tajikistan </option>
						<option value="Tanzania"> Tanzania </option>
						<option value="Thailand"> Thailand </option>
						<option value="Timor-Leste"> Timor-Leste </option>
						<option value="Togo"> Togo </option>
						<option value="Tonga"> Tonga </option>
						<option value="Trinidad and Tobago"> Trinidad and Tobago </option>
						<option value="Tunisia"> Tunisia </option>
						<option value="Turkey"> Turkey </option>
						<option value="Turkmenistan"> Turkmenistan </option>
						<option value="Tuvalu"> Tuvalu </option>
						<option value="Uganda"> Uganda </option>
						<option value="Ukraine"> Ukraine </option>
						<option value="United Arab Emirates (UAE)"> United Arab Emirates (UAE) </option>
						<option value="United Kingdom (UK)"> United Kingdom (UK) </option>
						<option value="United States of America (USA)"> United States of America (USA) </option>
						<option value="Uruguay"> Uruguay </option>
						<option value="Uzbekistan"> Uzbekistan </option>
						<option value="Vanuatu"> Vanuatu </option>
						<option value="Vatican City (Holy See)"> Vatican City (Holy See) </option>
						<option value="Venezuela"> Venezuela </option>
						<option value="Vietnam"> Vietnam </option>
						<option value="Yemen"> Yemen </option>
						<option value="Zambia"> Zambia </option>
						<option value="Zimbabwe"> Zimbabwe </option>						
					</select>
					<label for="nxt_kin_country_code">Country of permanent residence</label>
				</div>
			</div>
			<div class="col s6">
				<div class="input-field ">
					<select name="country_residence" id="country_residence">
						<option value="">Select</option>
						<option value="Afghanistan"> Afghanistan </option>
						<option value="Albania"> Albania </option>
						<option value="Algeria"> Algeria </option>
						<option value="Andorra"> Andorra </option>
						<option value="Angola"> Angola </option>
						<option value="Antigua and Barbuda"> Antigua and Barbuda </option>
						<option value="Argentina"> Argentina </option>
						<option value="Armenia"> Armenia </option>
						<option value="Australia"> Australia </option>
						<option value="Austria"> Austria </option>
						<option value="Azerbaijan"> Azerbaijan </option>
						<option value="Bahamas"> Bahamas </option>
						<option value="Bahrain"> Bahrain </option>
						<option value="Bangladesh"> Bangladesh </option>
						<option value="Barbados"> Barbados </option>
						<option value="Belarus"> Belarus </option>
						<option value="Belgium"> Belgium </option>
						<option value="Belize"> Belize </option>
						<option value="Benin"> Benin </option>
						<option value="Bhutan"> Bhutan </option>
						<option value="Bolivia"> Bolivia </option>
						<option value="Bosnia and Herzegovina"> Bosnia and Herzegovina </option>
						<option value="Botswana"> Botswana </option>
						<option value="Brazil"> Brazil </option>
						<option value="Brunei"> Brunei </option>
						<option value="Bulgaria"> Bulgaria </option>
						<option value="Burkina Faso"> Burkina Faso </option>
						<option value="Burundi"> Burundi </option>
						<option value="Cabo Verde"> Cabo Verde </option>
						<option value="Cambodia"> Cambodia </option>
						<option value="Cameroon"> Cameroon </option>
						<option value="Canada"> Canada </option>
						<option value="Central African Republic (CAR)"> Central African Republic (CAR) </option>
						<option value="Chad"> Chad </option>
						<option value="Chile"> Chile </option>
						<option value="China"> China </option>
						<option value="Colombia"> Colombia </option>
						<option value="Comoros"> Comoros </option>
						<option value="Congo, Democratic Republic of the"> Congo, Democratic Republic of the </option>
						<option value="Congo, Republic of the"> Congo, Republic of the </option>
						<option value="Costa Rica"> Costa Rica </option>
						<option value="Cote d'Ivoire"> Cote d'Ivoire </option>
						<option value="Croatia"> Croatia </option>
						<option value="Cuba"> Cuba </option>
						<option value="Cyprus"> Cyprus </option>
						<option value="Czechia"> Czechia </option>
						<option value="Denmark"> Denmark </option>
						<option value="Djibouti"> Djibouti </option>
						<option value="Dominica"> Dominica </option>
						<option value="Dominican Republic"> Dominican Republic </option>
						<option value="Ecuador"> Ecuador </option>
						<option value="Egypt"> Egypt </option>
						<option value="El Salvador"> El Salvador </option>
						<option value="Equatorial Guinea"> Equatorial Guinea </option>
						<option value="Eritrea"> Eritrea </option>
						<option value="Estonia"> Estonia </option>
						<option value="Eswatini (formerly Swaziland)"> Eswatini (formerly Swaziland) </option>
						<option value="Ethiopia"> Ethiopia </option>
						<option value="Fiji"> Fiji </option>
						<option value="Finland"> Finland </option>
						<option value="France"> France </option>
						<option value="Gabon"> Gabon </option>
						<option value="Gambia"> Gambia </option>
						<option value="Georgia"> Georgia </option>
						<option value="Germany"> Germany </option>
						<option value="Ghana"> Ghana </option>
						<option value="Greece"> Greece </option>
						<option value="Grenada"> Grenada </option>
						<option value="Guatemala"> Guatemala </option>
						<option value="Guinea"> Guinea </option>
						<option value="Guinea-Bissau"> Guinea-Bissau </option>
						<option value="Guyana"> Guyana </option>
						<option value="Haiti"> Haiti </option>
						<option value="Honduras"> Honduras </option>
						<option value="Hungary"> Hungary </option>
						<option value="Iceland"> Iceland </option>
						<option value="India"> India </option>
						<option value="Indonesia"> Indonesia </option>
						<option value="Iran"> Iran </option>
						<option value="Iraq"> Iraq </option>
						<option value="Ireland"> Ireland </option>
						<option value="Israel"> Israel </option>
						<option value="Italy"> Italy </option>
						<option value="Jamaica"> Jamaica </option>
						<option value="Japan"> Japan </option>
						<option value="Jordan"> Jordan </option>
						<option value="Kazakhstan"> Kazakhstan </option>
						<option value="Kenya"> Kenya </option>
						<option value="Kiribati"> Kiribati </option>
						<option value="Kosovo"> Kosovo </option>
						<option value="Kuwait"> Kuwait </option>
						<option value="Kyrgyzstan"> Kyrgyzstan </option>
						<option value="Laos"> Laos </option>
						<option value="Latvia"> Latvia </option>
						<option value="Lebanon"> Lebanon </option>
						<option value="Lesotho"> Lesotho </option>
						<option value="Liberia"> Liberia </option>
						<option value="Libya"> Libya </option>
						<option value="Liechtenstein"> Liechtenstein </option>
						<option value="Lithuania"> Lithuania </option>
						<option value="Luxembourg"> Luxembourg </option>
						<option value="Madagascar"> Madagascar </option>
						<option value="Malawi"> Malawi </option>
						<option value="Malaysia"> Malaysia </option>
						<option value="Maldives"> Maldives </option>
						<option value="Mali"> Mali </option>
						<option value="Malta"> Malta </option>
						<option value="Marshall Islands"> Marshall Islands </option>
						<option value="Mauritania"> Mauritania </option>
						<option value="Mauritius"> Mauritius </option>
						<option value="Mexico"> Mexico </option>
						<option value="Micronesia"> Micronesia </option>
						<option value="Moldova"> Moldova </option>
						<option value="Monaco"> Monaco </option>
						<option value="Mongolia"> Mongolia </option>
						<option value="Montenegro"> Montenegro </option>
						<option value="Morocco"> Morocco </option>
						<option value="Mozambique"> Mozambique </option>
						<option value="Myanmar (formerly Burma)"> Myanmar (formerly Burma) </option>
						<option value="Namibia"> Namibia </option>
						<option value="Nauru"> Nauru </option>
						<option value="Nepal"> Nepal </option>
						<option value="Netherlands"> Netherlands </option>
						<option value="New Zealand"> New Zealand </option>
						<option value="Nicaragua"> Nicaragua </option>
						<option value="Niger"> Niger </option>
						<option value="Nigeria"> Nigeria </option>
						<option value="North Korea"> North Korea </option>
						<option value="North Macedonia (formerly Macedonia)"> North Macedonia (formerly Macedonia) </option>
						<option value="Norway"> Norway </option>
						<option value="Oman"> Oman </option>
						<option value="Pakistan"> Pakistan </option>
						<option value="Palau"> Palau </option>
						<option value="Palestine"> Palestine </option>
						<option value="Panama"> Panama </option>
						<option value="Papua New Guinea"> Papua New Guinea </option>
						<option value="Paraguay"> Paraguay </option>
						<option value="Peru"> Peru </option>
						<option value="Philippines"> Philippines </option>
						<option value="Poland"> Poland </option>
						<option value="Portugal"> Portugal </option>
						<option value="Qatar"> Qatar </option>
						<option value="Romania"> Romania </option>
						<option value="Russia"> Russia </option>
						<option value="Rwanda"> Rwanda </option>
						<option value="Saint Kitts and Nevis"> Saint Kitts and Nevis </option>
						<option value="Saint Lucia"> Saint Lucia </option>
						<option value="Saint Vincent and the Grenadines"> Saint Vincent and the Grenadines </option>
						<option value="Samoa"> Samoa </option>
						<option value="San Marino"> San Marino </option>
						<option value="Sao Tome and Principe"> Sao Tome and Principe </option>
						<option value="Saudi Arabia"> Saudi Arabia </option>
						<option value="Senegal"> Senegal </option>
						<option value="Serbia"> Serbia </option>
						<option value="Seychelles"> Seychelles </option>
						<option value="Sierra Leone"> Sierra Leone </option>
						<option value="Singapore"> Singapore </option>
						<option value="Slovakia"> Slovakia </option>
						<option value="Slovenia"> Slovenia </option>
						<option value="Solomon Islands"> Solomon Islands </option>
						<option value="Somalia"> Somalia </option>
						<option value="South Africa"> South Africa </option>
						<option value="South Korea"> South Korea </option>
						<option value="South Sudan"> South Sudan </option>
						<option value="Spain"> Spain </option>
						<option value="Sri Lanka"> Sri Lanka </option>
						<option value="Sudan"> Sudan </option>
						<option value="Suriname"> Suriname </option>
						<option value="Sweden"> Sweden </option>
						<option value="Switzerland"> Switzerland </option>
						<option value="Syria"> Syria </option>
						<option value="Taiwan"> Taiwan </option>
						<option value="Tajikistan"> Tajikistan </option>
						<option value="Tanzania"> Tanzania </option>
						<option value="Thailand"> Thailand </option>
						<option value="Timor-Leste"> Timor-Leste </option>
						<option value="Togo"> Togo </option>
						<option value="Tonga"> Tonga </option>
						<option value="Trinidad and Tobago"> Trinidad and Tobago </option>
						<option value="Tunisia"> Tunisia </option>
						<option value="Turkey"> Turkey </option>
						<option value="Turkmenistan"> Turkmenistan </option>
						<option value="Tuvalu"> Tuvalu </option>
						<option value="Uganda"> Uganda </option>
						<option value="Ukraine"> Ukraine </option>
						<option value="United Arab Emirates (UAE)"> United Arab Emirates (UAE) </option>
						<option value="United Kingdom (UK)"> United Kingdom (UK) </option>
						<option value="United States of America (USA)"> United States of America (USA) </option>
						<option value="Uruguay"> Uruguay </option>
						<option value="Uzbekistan"> Uzbekistan </option>
						<option value="Vanuatu"> Vanuatu </option>
						<option value="Vatican City (Holy See)"> Vatican City (Holy See) </option>
						<option value="Venezuela"> Venezuela </option>
						<option value="Vietnam"> Vietnam </option>
						<option value="Yemen"> Yemen </option>
						<option value="Zambia"> Zambia </option>
						<option value="Zimbabwe"> Zimbabwe </option>
					</select>
					<label for="nxt_kin_phone">Country of birth</label>
				</div>
			</div>		
	</div>	

	<div class="row">
			<div class="col s4">
				<div class="input-field ">
                    <input id="birth_place" name="birth_place" type="text" class="">
					<label for="birth_place">Country of birth</label>
				</div>
			</div>	
			<div class="col s4">
				<div class="input-field ">
					<select name="Nationality" id="Nationality">
          <option value="">Select</option>
            <option value="Afghan"> Afghan </option>
            <option value="Albanian"> Albanian </option>
            <option value="Algerian"> Algerian </option>
            <option value="American"> American </option>
            <option value="Andorran"> Andorran </option>
            <option value="Angolan"> Angolan </option>
            <option value="Anguillan"> Anguillan </option>
            <option value="Citizen of Antigua and Barbuda"> Citizen of Antigua and Barbuda </option>
            <option value="Argentine"> Argentine </option>
            <option value="Armenian"> Armenian </option>
            <option value="Australian"> Australian </option>
            <option value="Austrian"> Austrian </option>
            <option value="Azerbaijani"> Azerbaijani </option>
            <option value="Bahamian"> Bahamian </option>
            <option value="Bahraini"> Bahraini </option>
            <option value="Bangladeshi"> Bangladeshi </option>
            <option value="Barbadian"> Barbadian </option>
            <option value="Belarusian"> Belarusian </option>
            <option value="Belgian"> Belgian </option>
            <option value="Belizean"> Belizean </option>
            <option value="Beninese"> Beninese </option>
            <option value="Bermudian"> Bermudian </option>
            <option value="Bhutanese"> Bhutanese </option>
            <option value="Bolivian"> Bolivian </option>
            <option value="Citizen of Bosnia and Herzegovina"> Citizen of Bosnia and Herzegovina </option>
            <option value="Botswanan"> Botswanan </option>
            <option value="Brazilian"> Brazilian </option>
            <option value="British"> British </option>
            <option value="British-Virgin-Islander"> British Virgin Islander </option>
            <option value="Bruneian"> Bruneian </option>
            <option value="Bulgarian"> Bulgarian </option>
            <option value="Burkinan	"> Burkinan </option>
            <option value="Burmese"> Burmese </option>
            <option value="Burundian"> Burundian </option>
            <option value="Cambodian"> Cambodian </option>
            <option value="Cameroonian"> Cameroonian </option>
            <option value="Canadian	Cape Verdean"> Canadian Cape Verdean </option>
            <option value="Cayman Islander	"> Cayman Islander </option>
            <option value="Central African	"> Central African </option>
            <option value="Chadian	"> Chadian </option>
            <option value="Chilean"> Chilean </option>
            <option value="Chinese	"> Chinese </option>
            <option value="Colombian	"> Colombian </option>
            <option value="Comoran	Congolese (Congo)"> Comoran Congolese (Congo) </option>
            <option value="Congolese (DRC)	"> Congolese (DRC) </option>
            <option value="Cook Islander	"> Cook Islander </option>
            <option value="Costa Rican	"> Costa Rican </option>
            <option value="Croatian"> Croatian </option>
            <option value="Cuban	"> Cuban </option>
            <option value="Cymraes	"> Cymraes </option>
            <option value="Cymro	"> Cymro </option>
            <option value="Cypriot"> Cypriot </option>
            <option value="Czech	 	 	 "> Czech </option>
            <option value="Danish	"> Danish </option>
            <option value="Djiboutian	"> Djiboutian </option>
            <option value="Dominican	"> Dominican </option>
            <option value="Citizen of the Dominican Republic"> Citizen of the Dominican Republic </option>
            <option value="Dutch	 	 	 "> Dutch </option>
            <option value="East Timorese	"> East Timorese </option>
            <option value="Ecuadorean	"> Ecuadorean </option>
            <option value="Egyptian"> Egyptian </option>
            <option value="Emirati"> Emirati </option>
            <option value="English	"> English </option>
            <option value="Equatorial Guinean"> Equatorial Guinean </option>
            <option value="Eritrean	"> Eritrean </option>
            <option value="Estonian"> Estonian </option>
            <option value="Ethiopian	 	 	 "> Ethiopian </option>
            <option value="Faroese	"> Faroese </option>
            <option value="Fijian	"> Fijian </option>
            <option value="Filipino	"> Filipino </option>
            <option value="Finnish"> Finnish </option>
            <option value="French	 	 	 "> French </option>
            <option value="Gabonese	"> Gabonese </option>
            <option value="Gambian	"> Gambian </option>
            <option value="Georgian	"> Georgian </option>
            <option value="German"> German </option>
            <option value="Ghanaian	"> Ghanaian </option>
            <option value="Gibraltarian	"> Gibraltarian </option>
            <option value="Greek	"> Greek </option>
            <option value="Greenlandic"> Greenlandic </option>
            <option value="Grenadian"> Grenadian </option>
            <option value="Guamanian	"> Guamanian </option>
            <option value="Guatemalan	"> Guatemalan </option>
            <option value="Citizen of Guinea-Bissau"> Citizen of Guinea-Bissau </option>
            <option value="Guinean	Guyanese	 	 "> Guinean Guyanese </option>
            <option value="Haitian	"> Haitian </option>
            <option value="Honduran	"> Honduran </option>
            <option value="Hong Konger	"> Hong Konger </option>
            <option value="Hungarian"> Hungarian </option>
            <option value="Icelandic"> Icelandic </option>
            <option value="Indian	"> Indian </option>
            <option value="Indonesian	"> Indonesian </option>
            <option value="Iranian"> Iranian </option>
            <option value="Iraqi	"> Iraqi </option>
            <option value="Irish	"> Irish </option>
            <option value="Israeli	"> Israeli </option>
            <option value="Italian"> Italian </option>
            <option value="Ivorian	 	 	 "> Ivorian </option>
            <option value="Jamaican	"> Jamaican </option>
            <option value="Japanese	"> Japanese </option>
            <option value="Jordanian"> Jordanian </option>
            <option value="Kazakh	"> Kazakh </option>
            <option value="Kenyan	"> Kenyan </option>
            <option value="Kittitian	"> Kittitian </option>
            <option value="Citizen of Kiribati"> Citizen of Kiribati </option>
            <option value="Kosovan	"> Kosovan </option>
            <option value="Kuwaiti	"> Kuwaiti </option>
            <option value="Kyrgyz	 "> Kyrgyz </option>
            <option value="Lao	"> Lao </option>
            <option value="Latvian	"> Latvian </option>
            <option value="Lebanese"> Lebanese </option>
            <option value="Liberian"> Liberian </option>
            <option value="Libyan	"> Libyan </option>
            <option value="Liechtenstein citizen	"> Liechtenstein citizen </option>
            <option value="Lithuanian	"> Lithuanian </option>
            <option value="Luxembourger"> Luxembourger </option>
            <option value="Macanese	"> Macanese </option>
            <option value="Macedonian	"> Macedonian </option>
            <option value="Malagasy	"> Malagasy </option>
            <option value="Malawian"> Malawian </option>
            <option value="Malaysian	"> Malaysian </option>
            <option value="Maldivian	"> Maldivian </option>
            <option value="Malian	"> Malian </option>
            <option value="Maltese"> Maltese </option>
            <option value="Marshallese	"> Marshallese </option>
            <option value="Martiniquais	"> Martiniquais </option>
            <option value="Mauritanian	"> Mauritanian </option>
            <option value="Mauritian"> Mauritian </option>
            <option value="Mexican	"> Mexican </option>
            <option value="Micronesian	"> Micronesian </option>
            <option value="Moldovan	"> Moldovan </option>
            <option value="Monegasque"> Monegasque </option>
            <option value="Mongolian	"> Mongolian </option>
            <option value="Montenegrin	"> Montenegrin </option>
            <option value="Montserratian	"> Montserratian </option>
            <option value="Moroccan"> Moroccan </option>
            <option value="Mosotho	"> Mosotho </option>
            <option value="Mozambican	 	 "> Mozambican </option>
            <option value="Namibian	"> Namibian </option>
            <option value="Nauruan	"> Nauruan </option>
            <option value="Nepalese	"> Nepalese </option>
            <option value="New Zealander"> New Zealander </option>
            <option value="Nicaraguan	"> Nicaraguan </option>
            <option value="Nigerian	"> Nigerian </option>
            <option value="Nigerien	"> Nigerien </option>
            <option value="Niuean"> Niuean </option>
            <option value="North Korean"> North Korean </option>
            <option value="Northern Irish	"> Northern Irish </option>
            <option value="Norwegian	 "> Norwegian </option>
            <option value="Omani"> Omani </option>
            <option value="Pakistani	"> Pakistani </option>
            <option value="Palauan	"> Palauan </option>
            <option value="Palestinian	"> Palestinian </option>
            <option value="Panamanian"> Panamanian </option>
            <option value="Papua New Guinean	"> Papua New Guinean </option>
            <option value="Paraguayan	"> Paraguayan </option>
            <option value="Peruvian	"> Peruvian </option>
            <option value="Pitcairn Islander"> Pitcairn Islander </option>
            <option value="Polish	"> Polish </option>
            <option value="Portuguese	"> Portuguese </option>
            <option value="Prydeinig	Puerto Rican"> Prydeinig Puerto Rican </option>
            <option value="Qatari"> Qatari </option>
            <option value="Romanian	"> Romanian </option>
            <option value="Russian"> Russian </option>
            <option value="Rwandan"> Rwandan </option>
            <option value="Salvadorean"> Salvadorean </option>
            <option value="Sammarinese"> Sammarinese </option>
            <option value="Samoan	"> Samoan </option>
            <option value="Sao Tomean"> Sao Tomean </option>
            <option value="Saudi Arabian	"> Saudi Arabian </option>
            <option value="Scottish	"> Scottish </option>
            <option value="Senegalese	"> Senegalese </option>
            <option value="Serbian"> Serbian </option>
            <option value="Citizen of Seychelles	"> Citizen of Seychelles </option>
            <option value="Sierra Leonean	"> Sierra Leonean </option>
            <option value="Singaporean	"> Singaporean </option>
            <option value="Slovak"> Slovak </option>
            <option value="Slovenian	Solomon Islander	"> Slovenian Solomon Islander </option>
            <option value="Somali	"> Somali </option>
            <option value="South African"> South African </option>
            <option value="South Korean	"> South Korean </option>
            <option value="South Sudanese	"> South Sudanese </option>
            <option value="Spanish	"> Spanish </option>
            <option value="Sri Lankan"> Sri Lankan </option>
            <option value="St Helenian	"> St Helenian </option>
            <option value="St Lucian	"> St Lucian </option>
            <option value="Stateless	"> Stateless </option>
            <option value="Sudanese"> Sudanese </option>
            <option value="Surinamese	"> Surinamese </option>
            <option value="Swazi	"> Swazi </option>
            <option value="Swedish	"> Swedish </option>
            <option value="Swiss"> Swiss </option>
            <option value="Syrian	 	 	 "> Syrian </option>
            <option value="Taiwanese	"> Taiwanese </option>
            <option value="Tajik	"> Tajik </option>
            <option value="Tanzanian	"> Tanzanian </option>
            <option value="Thai"> Thai </option>
            <option value="Togolese	"> Togolese </option>
            <option value="Tongan	"> Tongan </option>
            <option value="Trinidadian	"> Trinidadian </option>
            <option value="Tristanian"> Tristanian </option>
            <option value="Tunisian	"> Tunisian </option>
            <option value="Turkish	"> Turkish </option>
            <option value="Turkmen	"> Turkmen </option>
            <option value="Turks and Caicos Islander"> Turks and Caicos Islander </option>
            <option value="Tuvaluan	 	 	 "> Tuvaluan </option>
            <option value="Ugandan	"> Ugandan </option>
            <option value="Ukrainian	"> Ukrainian </option>
            <option value="Uruguayan	"> Uruguayan </option>
            <option value="Uzbek"> Uzbek </option>
            <option value="Vatican citizen	Citizen of Vanuatu	"> Vatican citizen Citizen of Vanuatu </option>
            <option value="Venezuelan	"> Venezuelan </option>
            <option value="Vietnamese"> Vietnamese </option>
            <option value="Vincentian	 	 	 "> Vincentian </option>
            <option value="Wallisian"> Wallisian </option>
            <option value="Welsh"> Welsh </option>
            <option value="Yemeni"> Yemeni </option>
            <option value="Zambian	"> Zambian </option>
            <option value="Zimbabwean"> Zimbabwean </option>
					</select>	
					<label for="nxt_kin_phone">Nationality</label>
				</div>
			</div>
			<div class="col s4">
				<div class="input-field ">
                    <input name="prev_study_sector" type="text" id="prev_study_sector" value="" />
					<label for="prev_study_sector">Previous Study Sector</label>
				</div>
			</div>				
	</div>		
	
	<div class="row">
			<div class="col s6">
					<p>Have you also made an application to any University through UCAS</p>
					<p>
							<label>
									<input name="ucas_application" type="radio" value="0" checked/>
									<span>Yes</span>
							</label>
							<label>
									<input name="ucas_application" type="radio"
											value="1" />
									<span>No</span>
							</label>
					</p>
			</div>
			<div class="col s6">
				<div class="input-field ">
                    <input name="ucas_id" type="text" id="ucas_id" maxlength="25" value=""/>
					<label for="prev_study_sector">UCAS ID</label>
				</div>
			</div>	
	</div>
	<div class="row">
			<div class=" col s12 ">
					<p>Have you lived at any other address in past 2 years?</p>
					<p>
							<label>
									<input name="lived_other_address" type="radio" value="0" checked/>
									<span>Yes</span>
							</label>
							<label>
									<input name="lived_other_address" type="radio"
											value="0" />
									<span>No</span>
							</label>
					</p>
			</div>
	</div>
	<div class="row">
		<div class="input-field col s6">
			<input type="text" id="street_address" name="street_address">
			<label for="address">Street Address</label>
		</div>
		<div class="input-field col s6">
			<input type="text" id="street_address_line" name="street_address_line">
			<label for="address_2">Street Address Line2</label>
		</div>						
	</div>
	<div class="row">
		<div class="input-field col s6">
		   <input type="text" id="city" name="city">
		   <label for="city">City</label>
		</div>
		<div class="input-field col s6">
		   <input type="text" id="state" name="state">
		   <label for="state">State / Province</label>
		</div>						
	</div>
	<div class="row">
		<div class="input-field col s6">
		   <input type="text" id="zip_code" name="zip_code">
		   <label for="zip_code">Postal / Zip code</label>
		</div>
		<div class="input-field col s6">
			<select name="country" id="country">
				<option value="">Select</option>
				<option value="India">India</option>
				<option value="UK">UK</option>
				<option value="Australia">Australia</option>
			</select>							   
		   <label for="country">Country</label>
		</div>						
	</div>
	<div class="row">
		<div class="input-field col s6">
		   <input type="text" id="living_addr_start_date" name="living_addr_start_date" class="datepicker">
		   <label for="living_addr_start_date">When did you start living at this address?</label>
		</div>
		<div class="input-field col s6">
		   <input type="text" id="living_addr_stop_date" name="living_addr_stop_date" class="datepicker">
		   <label for="living_addr_stop_date">When did you stop living at this address?</label>
		</div>					
	</div>		

	<div class="row">
		<div class="input-field col s4">
			<select name="relatiion_status" id="relatiion_status">
				<option value="Married">Married</option>
				<option value="Single">Single</option>
				<option value="civil-partnership">Civil partnership</option>
			</select>			   
		   <label for="living_addr_start_date">Relationship Status</label>
		</div>				
	</div>	
	<div class="row">
			<div class=" col s3 ipfield">
					<div class="input-field">
						<select name="spouse_prefix" id="spouse_prefix">
							<option value="Mr">Mr.</option>
							<option value="Mrs">Mrs.</option>
						</select>
						<label for="spouse_prefix">Prefix</label>
					</div>
			</div>
			<div class=" col s3 ipfield">
					<div class="input-field">
									<input name="spouse_firstname" required id="spouse_firstname" type="text">
									<label for="spouse_firstname">First Name</label>
					</div>


			</div>
			<div class=" col s3 ipfield">
							<div class="input-field">
											<input id="spouse_middlename" name="spouse_middlename" type="text" class="validate">
											<label for="spouse_middlename">Middle Name</label>
							</div>


					</div>
					<div class=" col s3 ipfield">
									<div class="input-field">
													<input id="spouse_familyname" name="spouse_familyname" type="text" class="validate">
													<label for="spouse_familyname">Family Name</label>
									</div>
							</div>
	</div>						
	<div class="row">
			<div class=" col s6 ipfield">
					<div class="input-field">
						<input name="spouse_birth_date" class="datepicker" id="spouse_birth_date" type="text">
						<label for="spouse_birth_date">Date of birth of spouse</label>
					</div>
			</div>
			<div class=" col s6 ipfield">
					<div class="input-field">
						<select name="current_live_status" id="current_live_status">
							<option value="0">No</option>
							<option value="1">Yes</option>
						</select>
						<label for="current_live_status">Do they currently live with you?</label>
					</div>
			</div>
	</div>			
	
	<h4 class="sub_title">If No, provide the current address </h4>
	<div class="row">
		<div class="input-field col s6">
		   <input type="text" id="spouse_current_addr" name="spouse_current_addr">
		   <label for="spouse_current_addr">Street Address</label>
		</div>
		<div class="input-field col s6">
		   <input type="text" id="spouse_current_addr_line" name="spouse_current_addr_line">
		   <label for="spouse_current_addr_line">Street Address Line2</label>
		</div>						
	</div>

	<div class="row">
		<div class="input-field col s6">
		   <input type="text" id="spouse_current_city" name="spouse_current_city">
		   <label for="spouse_current_city">City</label>
		</div>
		<div class="input-field col s6">
		   <input type="text" id="spouse_current_state" maxlength="50" name="spouse_current_state">
		   <label for="spouse_current_state">State / Province</label>
		</div>						
	</div>
	
	<div class="row">
		<div class="input-field col s6">
		   <input type="text" id="spouse_current_zipcode" maxlength="20" name="spouse_current_zipcode">
		   <label for="spouse_current_zipcode">Postal / Zip code</label>
		</div>
		<div class="input-field col s6">
			<select name="spouse_current_country" id="spouse_current_country">
				<option value="">Select</option>
				<option value="India">India</option>
				<option value="UK">UK</option>
				<option value="Australia">Australia</option>
			</select>							   
		   <label for="country">Country</label>
		</div>						
	</div>						
	<div class="row">
			<div class=" col s6 ipfield">
					<div class="input-field">
						<input name="spouse_passport_no" class="" id="spouse_passport_no" maxlength="20" type="text">
						<label for="spouse_birth_date">Passport number of spouse</label>
					</div>
			</div>
			<div class=" col s6 ipfield">
					<div class="input-field">
						<select name="spouse_travel_uk" id="spouse_travel_uk">
							<option value="0">Yes</option>
							<option value="1">No</option>
						</select>
						<label for="current_live_status">Will they be travelling with you to the UK?</label>
					</div>
			</div>
	</div>
	
	<div class="row">
		<div class="col s6">
				<label>Passport of spouse</label>
				<div class="file-field input-field">
								<div class="btn">
										<span>Upload</span>
										<input type="file" multiple > <i class="fa fa-cloud-upload"></i>
								</div>
								<div class="file-path-wrapper">
										<input class="file-path validate" name="spouse_passport" type="text" placeholder = "Upload file" >
								</div>
				</div>
				<div class="uploaded-file" id="spouse_passport" data-type="file"></div>	
		</div>
	</div>	
</div>
<div class="controls">
<a class="mgbtn2">Back</a>
                <input id="form_name" type="hidden" class="validate" name="type_of" value="further_details">
                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">                 
                                                      {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
        
</div>