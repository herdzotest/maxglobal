<div id="tab_11" class="tab-pane">
        
                   
<div class="content-head">
                                        
                                        <h4>Course Applied</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                        </div>
                        {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'personal-add-form']) !!}
                <div class="pane dropdown_wrapper">
					<div id="course-wrapper-new">
					<div class="stdhst">
							<div class="index">1</div>
							<div class="col-md-6">
								<div class="mg-input-field ">
									<label for="course_country_new">Country</label>
									<select class="searchSelect" name="course_country_new" id="course_country_new">
										<option value="">Select</option>
										<?php
										foreach($countries as $countriesData) {
											echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
										}
										?>											
									</select>											
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="mg-input-field ">  
									<label for="level_study_new">Level of Study</label>
									<select class="formSelect" name="level_study_new" id="level_study_new">
									  <option value="">Select</option>
									  <option value="Diploma"> Diploma </option>
									  <option value="Bachelors"> Bachelors </option>
									  <option value="Masters"> Masters </option>
									  <option value="Doctoral"> Doctoral </option>
									</select>								
								</div>
							</div>
							
							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="course_title_new">Course Name</label>
											<select class="searchSelect" id="course_title_new" name="course_title_new">
												<option value="">Select</option>
												<?php
												foreach($courses as $courseData) {
													echo '<option value="'.$courseData->course_name.'">'.$courseData->course_name.'</option>';
												}
												?>											
											</select>
									</div>
							</div>

							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="mode_study_new">Mode of study</label>
											<select class="formSelect" id="mode_study_new" name="mode_study_new">
												<option value="">Select</option>
												<option value="Full-time">Full time</option>
												<option value="Distance">Distance</option>											
											</select>
									</div>
							</div>

							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="campus_stage_new">Campus</label>
											<select class="searchSelect" id="campus_stage_new" name="campus_stage_new">
												<option value="">Select</option>
												<?php
												foreach($campus as $campusData) {
													echo '<option value="'.$campusData->campus_name.'">'.$campusData->campus_name.'</option>';
												}
												?>											
											</select>
									</div>
							</div>

							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="stage_name_new">Stage (in months)</label>
											<input class="form-control" id="stage_name_new" name="stage_name_new" type="number" min=1>
									</div>
							</div>

							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="entry_date_new">Intake</label>
											<!--<input class="form-control entrydate" id="entry_date_new" name="entry_date_new" type="text">-->
											<?php 
											$curr_yr = date('Y');
											$curr_yr2 = date('Y', strtotime('-2 year'));
											$nxt_yr = date('Y', strtotime('+1 year'));
											$nxt_yr2 = date('Y', strtotime('+2 year'));
											$curr_mon = date('m');
    										$months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
											?>
											<select class="searchSelect" name="entry_date_new" id="entry_date_new">
												<option value="">Select</option>
												<?php
													for($i=$curr_yr2; $i<=$nxt_yr2;$i++) {
														foreach ($months as $key => $value) { /*if($i==$curr_yr && $key<$curr_mon) { } else { */ ?>
															<option value="<?php echo $value.' '.$i; ?>"> <?php echo $value.' '.$i; ?></option>
													<?php	//} 
														}
													}
												?>
											</select>
									</div>
							</div>
							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="course_university_new">University Name</label>
											<select class="searchSelect"  name="course_university_new" id="course_university_new">
												<option value="">Select</option>
   												<?php
												foreach($university as $universityData) {
													echo '<option value="'.$universityData->university.'">'.$universityData->university.'</option>';
												}
												?>												
											</select>												
									</div>

							</div>

							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="status_new">Status</label>
											<select class="searchSelect" id="status_new" name="status_new">
												<option value="">Select</option>
												<option value="Received Application at Scholab">Received Application at Scholab</option>
												<option value="Application in Progress">Application in Progress</option>
												<option value="Application on hold by Scholab team">Application on hold by Scholab team</option>
												<option value="Application submitted to the University">Application submitted to the University</option>
												<option value="Conditional Offer Received">Conditional Offer Received</option>
												<option value="Unconditional Offer Received">Unconditional Offer Received</option>
												<option value="Rejected by University">Rejected by University</option>
												<option value="Application on hold by University">Application on hold by University</option>
												<option value="Case Closed - Student Not Qualified">Case Closed - Student Not Qualified</option>
												<option value="Case Closed - Program Closed">Case Closed - Program Closed</option>
												<option value="Case Closed - Offer Received - Student not interested to pay">Case Closed - Offer Received - Student not interested to pay</option>
												<option value="Tuition Fee Received">Tuition Fee Received</option>
												<option value="CAS Requested">CAS Requested</option>
												<option value="CAS Received">CAS Received</option>
												<option value="Visa In Process">Visa In Process</option>
												<option value="Visa Received">Visa Received</option>
												<option value="Visa Rejected">Visa Rejected</option>
												<option value="Student Deferred">Student Deferred</option>
												<option value="Refund Request Initiated">Refund Request Initiated</option>											
											</select>
									</div>
							</div>
							<div class="col-md-8 course-repr-note">
								<div class="mg-input-field ">
									<label for="status_note">Representative Note</label>
									<textarea id="status_note" name="status_note[0]" class=""></textarea>
								</div>
							</div>

							<div class="clearfix"></div>
					</div>
					</div>
					
					<div class="btn addshbtn"> <span class="glyphicon glyphicon-plus"></span></div>						
                </div>
				<!--<input type="hidden" value="1" class="wrapper-count">-->
				<input type="hidden" value="<?php echo $wrp_count_courseapplied;  ?>" class="wrapper-count">
				<input type="hidden" value="courses_new" name="formtype" class="formtype">
                <div class="controls">
                                <a class="mgbtn2 back_button">Back</a>
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="courses_applied_new">
                                 <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                              
                                {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                        </div>
                        {{ Form::close() }}
</div>


