<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;


use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use Dwij\Laraadmin\Helpers\LAHelper;
use Dwij\Laraadmin\Models\LAConfigs;

use App\User;
use App\Models\Employee;
use App\Role;
use Mail;
use Log;

class MailSettingsController extends Controller
{
	public function __construct() {
		
		if(Auth::user()->type != 'SUPER_ADMIN' && Auth::user()->type != 'MANAGER' && Auth::user()->type != 'SUBCONTRACTORS' && Auth::user()->type != 'MAIN_AGENTS' && Auth::user()->type != 'UNIVERSITY_USER')
			return abort(403);		
	}
	
	/**
	 * compose mail setttings.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		/*$users = DB::table('users')->select('users.name as user_name')
				->addselect('users.id as user_id')->addselect('users.email')
				->leftJoin('employees', 'users.context_id', '=', 'employees.id')
				->whereNull('users.deleted_at')->where('users.type', '!=', 'SUPER_ADMIN')
				->where('users.id', '!=', Auth::user()->id)->get();*/

		$users = DB::table('users')->select('users.name as user_name')
				->addselect('users.id as user_id')->addselect('users.email')
				->leftJoin('employees', 'users.context_id', '=', 'employees.id')
				->whereNull('users.deleted_at')->whereNotIn('users.type', ['SUPER_ADMIN','NURSE','SUBCONTRACTORS','UNIVERSITY_USER']) 
				->where('users.id', '!=', Auth::user()->id)->get();
		
		return View('la.mailsettings.index', [ 
			'users' => $users
		]);
	}
	
	/**
	 * send & save mail details.
	 *
	 * @return \Illuminate\Http\Response
	 */	
	public function update(Request $request)
	{
		if(count($request->all()) > 0) {
			$postedData = $request->all();
			
			if(count($postedData['reciepents']) > 0) {
				$email = array();
				foreach($postedData['reciepents'] as $result) {
					
					$values = explode("_", $result); 
					$email[] = $values[1];
					
					DB::table('mail_settings')->insert([
						'mail_content' => $postedData['mail_content'],
						'subject' => $postedData['subject'],
						'from_user' => Auth::user()->id,
						'to_user' => $values[0]
					]);
				}
				if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
					
					$data = array('postedContent' => $postedData['mail_content'],'email' => $email,'postedSubject' => $postedData['subject']);
					// Mail::send('emails.send_mails',['mail_content' => $postedData['mail_content']], function ($m) use ($data) {
					// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
					// 	$m->to($data['email'])->subject(LAConfigs::getByKey('sitename').' - '.$data['postedSubject']);
					// });
				}		
				\Session::flash('success_mail_message', 'Mail successfully send!!!');
			}
		}
		
		return redirect()->route(config('laraadmin.adminRoute') . '.mailsettings.index');
	}
	
}
