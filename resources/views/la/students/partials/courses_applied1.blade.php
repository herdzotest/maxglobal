<div id="tab_2" class="tab-pane" >
        
                   
<div class="content-head">
                                        
                                        <h4>Courses Applied For</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                        </div>
                        {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'personal-add-form']) !!}
                <div class="pane dropdown_wrapper">
					<div id="course-wrapper">
					<div class="stdhst">
							<div class="index">1</div>
							<div class="col-md-6">
								<div class="mg-input-field ">
									<label for="course_country">Preferred Country</label>
									<select class="searchSelect" name="course_country" id="course_country">
												  <option value="">Select</option>
												  <option value="Afghanistan"> Afghanistan </option>
												  <option value="Albania"> Albania </option>
												  <option value="Algeria"> Algeria </option>
												  <option value="Andorra"> Andorra </option>
												  <option value="Angola"> Angola </option>
												  <option value="Antigua and Barbuda"> Antigua and Barbuda </option>
												  <option value="Argentina"> Argentina </option>
												  <option value="Armenia"> Armenia </option>
												  <option value="Australia"> Australia </option>
												  <option value="Austria"> Austria </option>
												  <option value="Azerbaijan"> Azerbaijan </option>
												  <option value="Bahamas"> Bahamas </option>
												  <option value="Bahrain"> Bahrain </option>
												  <option value="Bangladesh"> Bangladesh </option>
												  <option value="Barbados"> Barbados </option>
												  <option value="Belarus"> Belarus </option>
												  <option value="Belgium"> Belgium </option>
												  <option value="Belize"> Belize </option>
												  <option value="Benin"> Benin </option>
												  <option value="Bhutan"> Bhutan </option>
												  <option value="Bolivia"> Bolivia </option>
												  <option value="Bosnia and Herzegovina"> Bosnia and Herzegovina </option>
												  <option value="Botswana"> Botswana </option>
												  <option value="Brazil"> Brazil </option>
												  <option value="Brunei"> Brunei </option>
												  <option value="Bulgaria"> Bulgaria </option>
												  <option value="Burkina Faso"> Burkina Faso </option>
												  <option value="Burundi"> Burundi </option>
												  <option value="Cabo Verde"> Cabo Verde </option>
												  <option value="Cambodia"> Cambodia </option>
												  <option value="Cameroon"> Cameroon </option>
												  <option value="Canada"> Canada </option>
												  <option value="Central African Republic (CAR)"> Central African Republic (CAR) </option>
												  <option value="Chad"> Chad </option>
												  <option value="Chile"> Chile </option>
												  <option value="China"> China </option>
												  <option value="Colombia"> Colombia </option>
												  <option value="Comoros"> Comoros </option>
												  <option value="Congo, Democratic Republic of the"> Congo, Democratic Republic of the </option>
												  <option value="Congo, Republic of the"> Congo, Republic of the </option>
												  <option value="Costa Rica"> Costa Rica </option>
												  <option value="Cote d'Ivoire"> Cote d'Ivoire </option>
												  <option value="Croatia"> Croatia </option>
												  <option value="Cuba"> Cuba </option>
												  <option value="Cyprus"> Cyprus </option>
												  <option value="Czechia"> Czechia </option>
												  <option value="Denmark"> Denmark </option>
												  <option value="Djibouti"> Djibouti </option>
												  <option value="Dominica"> Dominica </option>
												  <option value="Dominican Republic"> Dominican Republic </option>
												  <option value="Ecuador"> Ecuador </option>
												  <option value="Egypt"> Egypt </option>
												  <option value="El Salvador"> El Salvador </option>
												  <option value="Equatorial Guinea"> Equatorial Guinea </option>
												  <option value="Eritrea"> Eritrea </option>
												  <option value="Estonia"> Estonia </option>
												  <option value="Eswatini (formerly Swaziland)"> Eswatini (formerly Swaziland) </option>
												  <option value="Ethiopia"> Ethiopia </option>
												  <option value="Fiji"> Fiji </option>
												  <option value="Finland"> Finland </option>
												  <option value="France"> France </option>
												  <option value="Gabon"> Gabon </option>
												  <option value="Gambia"> Gambia </option>
												  <option value="Georgia"> Georgia </option>
												  <option value="Germany"> Germany </option>
												  <option value="Ghana"> Ghana </option>
												  <option value="Greece"> Greece </option>
												  <option value="Grenada"> Grenada </option>
												  <option value="Guatemala"> Guatemala </option>
												  <option value="Guinea"> Guinea </option>
												  <option value="Guinea-Bissau"> Guinea-Bissau </option>
												  <option value="Guyana"> Guyana </option>
												  <option value="Haiti"> Haiti </option>
												  <option value="Honduras"> Honduras </option>
												  <option value="Hungary"> Hungary </option>
												  <option value="Iceland"> Iceland </option>
												  <option value="India"> India </option>
												  <option value="Indonesia"> Indonesia </option>
												  <option value="Iran"> Iran </option>
												  <option value="Iraq"> Iraq </option>
												  <option value="Ireland"> Ireland </option>
												  <option value="Israel"> Israel </option>
												  <option value="Italy"> Italy </option>
												  <option value="Jamaica"> Jamaica </option>
												  <option value="Japan"> Japan </option>
												  <option value="Jordan"> Jordan </option>
												  <option value="Kazakhstan"> Kazakhstan </option>
												  <option value="Kenya"> Kenya </option>
												  <option value="Kiribati"> Kiribati </option>
												  <option value="Kosovo"> Kosovo </option>
												  <option value="Kuwait"> Kuwait </option>
												  <option value="Kyrgyzstan"> Kyrgyzstan </option>
												  <option value="Laos"> Laos </option>
												  <option value="Latvia"> Latvia </option>
												  <option value="Lebanon"> Lebanon </option>
												  <option value="Lesotho"> Lesotho </option>
												  <option value="Liberia"> Liberia </option>
												  <option value="Libya"> Libya </option>
												  <option value="Liechtenstein"> Liechtenstein </option>
												  <option value="Lithuania"> Lithuania </option>
												  <option value="Luxembourg"> Luxembourg </option>
												  <option value="Madagascar"> Madagascar </option>
												  <option value="Malawi"> Malawi </option>
												  <option value="Malaysia"> Malaysia </option>
												  <option value="Maldives"> Maldives </option>
												  <option value="Mali"> Mali </option>
												  <option value="Malta"> Malta </option>
												  <option value="Marshall Islands"> Marshall Islands </option>
												  <option value="Mauritania"> Mauritania </option>
												  <option value="Mauritius"> Mauritius </option>
												  <option value="Mexico"> Mexico </option>
												  <option value="Micronesia"> Micronesia </option>
												  <option value="Moldova"> Moldova </option>
												  <option value="Monaco"> Monaco </option>
												  <option value="Mongolia"> Mongolia </option>
												  <option value="Montenegro"> Montenegro </option>
												  <option value="Morocco"> Morocco </option>
												  <option value="Mozambique"> Mozambique </option>
												  <option value="Myanmar (formerly Burma)"> Myanmar (formerly Burma) </option>
												  <option value="Namibia"> Namibia </option>
												  <option value="Nauru"> Nauru </option>
												  <option value="Nepal"> Nepal </option>
												  <option value="Netherlands"> Netherlands </option>
												  <option value="New Zealand"> New Zealand </option>
												  <option value="Nicaragua"> Nicaragua </option>
												  <option value="Niger"> Niger </option>
												  <option value="Nigeria"> Nigeria </option>
												  <option value="North Korea"> North Korea </option>
												  <option value="North Macedonia (formerly Macedonia)"> North Macedonia (formerly Macedonia) </option>
												  <option value="Norway"> Norway </option>
												  <option value="Oman"> Oman </option>
												  <option value="Pakistan"> Pakistan </option>
												  <option value="Palau"> Palau </option>
												  <option value="Palestine"> Palestine </option>
												  <option value="Panama"> Panama </option>
												  <option value="Papua New Guinea"> Papua New Guinea </option>
												  <option value="Paraguay"> Paraguay </option>
												  <option value="Peru"> Peru </option>
												  <option value="Philippines"> Philippines </option>
												  <option value="Poland"> Poland </option>
												  <option value="Portugal"> Portugal </option>
												  <option value="Qatar"> Qatar </option>
												  <option value="Romania"> Romania </option>
												  <option value="Russia"> Russia </option>
												  <option value="Rwanda"> Rwanda </option>
												  <option value="Saint Kitts and Nevis"> Saint Kitts and Nevis </option>
												  <option value="Saint Lucia"> Saint Lucia </option>
												  <option value="Saint Vincent and the Grenadines"> Saint Vincent and the Grenadines </option>
												  <option value="Samoa"> Samoa </option>
												  <option value="San Marino"> San Marino </option>
												  <option value="Sao Tome and Principe"> Sao Tome and Principe </option>
												  <option value="Saudi Arabia"> Saudi Arabia </option>
												  <option value="Senegal"> Senegal </option>
												  <option value="Serbia"> Serbia </option>
												  <option value="Seychelles"> Seychelles </option>
												  <option value="Sierra Leone"> Sierra Leone </option>
												  <option value="Singapore"> Singapore </option>
												  <option value="Slovakia"> Slovakia </option>
												  <option value="Slovenia"> Slovenia </option>
												  <option value="Solomon Islands"> Solomon Islands </option>
												  <option value="Somalia"> Somalia </option>
												  <option value="South Africa"> South Africa </option>
												  <option value="South Korea"> South Korea </option>
												  <option value="South Sudan"> South Sudan </option>
												  <option value="Spain"> Spain </option>
												  <option value="Sri Lanka"> Sri Lanka </option>
												  <option value="Sudan"> Sudan </option>
												  <option value="Suriname"> Suriname </option>
												  <option value="Sweden"> Sweden </option>
												  <option value="Switzerland"> Switzerland </option>
												  <option value="Syria"> Syria </option>
												  <option value="Taiwan"> Taiwan </option>
												  <option value="Tajikistan"> Tajikistan </option>
												  <option value="Tanzania"> Tanzania </option>
												  <option value="Thailand"> Thailand </option>
												  <option value="Timor-Leste"> Timor-Leste </option>
												  <option value="Togo"> Togo </option>
												  <option value="Tonga"> Tonga </option>
												  <option value="Trinidad and Tobago"> Trinidad and Tobago </option>
												  <option value="Tunisia"> Tunisia </option>
												  <option value="Turkey"> Turkey </option>
												  <option value="Turkmenistan"> Turkmenistan </option>
												  <option value="Tuvalu"> Tuvalu </option>
												  <option value="Uganda"> Uganda </option>
												  <option value="Ukraine"> Ukraine </option>
												  <option value="United Arab Emirates (UAE)"> United Arab Emirates (UAE) </option>
												  <option value="United Kingdom (UK)"> United Kingdom (UK) </option>
												  <option value="United States of America (USA)"> United States of America (USA) </option>
												  <option value="Uruguay"> Uruguay </option>
												  <option value="Uzbekistan"> Uzbekistan </option>
												  <option value="Vanuatu"> Vanuatu </option>
												  <option value="Vatican City (Holy See)"> Vatican City (Holy See) </option>
												  <option value="Venezuela"> Venezuela </option>
												  <option value="Vietnam"> Vietnam </option>
												  <option value="Yemen"> Yemen </option>
												  <option value="Zambia"> Zambia </option>
												  <option value="Zimbabwe"> Zimbabwe </option>
									</select>											
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="mg-input-field ">
									<label for="level_study">Level of study</label>
									<select class="formSelect" name="level_study" id="level_study">
									  <option value="">Select</option>
									  <option value="Entry-level"> Entry-level </option>
									  <option value="Level-1"> Level 1 </option>
									  <option value="Level-2"> Level 2 </option>
									  <option value="Level-3"> Level 3 </option>
									  <option value="Level-4"> Level 4 </option>
									  <option value="Level-5"> Level 5 </option>
									  <option value="Level-6/Bachelors/UG"> Level 6/Bachelors/UG </option>
									  <option value="Level-7/Masters Degree/PG"> Level 7/Masters Degree/PG </option>
									  <option value="Level-8/Doctorate"> Level 8/Doctorate </option>
									</select>								
								</div>
							</div>
							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="course_university">University</label>
											<select class="searchSelect" name="course_university" id="course_university">
												<option value="">Select</option>
                 <option value="University of Aberdeen"> University of Aberdeen </option>
                  <option value="Abertay University"> Abertay University </option>
                  <option value="Aberystwyth University"> Aberystwyth University </option>
                  <option value="Anglia Ruskin University, Cambridge"> Anglia Ruskin University, Cambridge </option>
                  <option value="Arden University, private, distance learning &amp; blended learning, London, Birmingham, Manchester, Berlin"> Arden University, private, distance learning &amp; blended learning, London, Birmingham, Manchester, Berlin </option>
                  <option value="Aston University, Birmingham"> Aston University, Birmingham </option>
                  <option value="Bangor University"> Bangor University </option>
                  <option value="University of Bath"> University of Bath </option>
                  <option value="Bath Spa University"> Bath Spa University </option>
                  <option value="University of Bedfordshire, Luton and Bedford"> University of Bedfordshire, Luton and Bedford </option>
                  <option value="University of Birmingham"> University of Birmingham </option>
                  <option value="Birmingham City University"> Birmingham City University </option>
                  <option value="University College Birmingham"> University College Birmingham </option>
                  <option value="Bishop Grosseteste University, Lincoln"> Bishop Grosseteste University, Lincoln </option>
                  <option value="University of Bolton"> University of Bolton </option>
                  <option value="The Arts University Bournemouth"> The Arts University Bournemouth </option>
                  <option value="Bournemouth University"> Bournemouth University </option>
                  <option value="BPP University, private"> BPP University, private </option>
                  <option value="University of Bradford"> University of Bradford </option>
                  <option value="University of Brighton"> University of Brighton </option>
                  <option value="University of Bristol"> University of Bristol </option>
                  <option value="Brunel University, Uxbridge and London"> Brunel University, Uxbridge and London </option>
                  <option value="University of Buckingham, private"> University of Buckingham, private </option>
                  <option value="Buckinghamshire New University"> Buckinghamshire New University </option>
                  <option value="University of Cambridge"> University of Cambridge </option>
                  <option value="Canterbury Christ Church University"> Canterbury Christ Church University </option>
                  <option value="Cardiff Metropolitan University "> Cardiff Metropolitan University </option>
                  <option value="Cardiff University"> Cardiff University </option>
                  <option value="University of Chester"> University of Chester </option>
                  <option value="University of Chichester"> University of Chichester </option>
                  <option value="Coventry University"> Coventry University </option>
                  <option value="Cranfield University"> Cranfield University </option>
                  <option value="University for the Creative Arts"> University for the Creative Arts </option>
                  <option value="University of Cumbria"> University of Cumbria </option>
                  <option value="De Montfort University"> De Montfort University </option>
                  <option value="University of Derby"> University of Derby </option>
                  <option value="University of Dundee"> University of Dundee </option>
                  <option value="Durham University"> Durham University </option>
                  <option value="University of East Anglia"> University of East Anglia </option>
                  <option value="University of East London"> University of East London </option>
                  <option value="Edge Hill University"> Edge Hill University </option>
                  <option value="University of Edinburgh"> University of Edinburgh </option>
                  <option value="Edinburgh Napier University"> Edinburgh Napier University </option>
                  <option value="University of Essex"> University of Essex </option>
                  <option value="University of Exeter"> University of Exeter </option>
                  <option value="Falmouth University"> Falmouth University </option>
                  <option value="University of Glasgow"> University of Glasgow </option>
                  <option value="Glasgow Caledonian University"> Glasgow Caledonian University </option>
                  <option value="University of Gloucestershire"> University of Gloucestershire </option>
                  <option value="University of Greenwich"> University of Greenwich </option>
                  <option value="Harper Adams University"> Harper Adams University </option>
                  <option value="Hartpury University"> Hartpury University </option>
                  <option value="Heriot-Watt University"> Heriot-Watt University </option>
                  <option value="University of Hertfordshire"> University of Hertfordshire </option>
                  <option value="University of the Highlands &amp; Islands "> University of the Highlands &amp; Islands </option>
                  <option value="University of Huddersfield"> University of Huddersfield </option>
                  <option value="University of Hull"> University of Hull </option>
                  <option value="Imperial College London"> Imperial College London </option>
                  <option value="Keele University"> Keele University </option>
                  <option value="University of Kent"> University of Kent </option>
                  <option value="Kingston University"> Kingston University </option>
                  <option value="University of Central Lancashire"> University of Central Lancashire </option>
                  <option value="Lancaster University"> Lancaster University </option>
                  <option value="University of Leeds"> University of Leeds </option>
                  <option value="Leeds Arts University"> Leeds Arts University </option>
                  <option value="Leeds Beckett University"> Leeds Beckett University </option>
                  <option value="Leeds Trinity University"> Leeds Trinity University </option>
                  <option value="University of Leicester"> University of Leicester </option>
                  <option value="University of Lincoln"> University of Lincoln </option>
                  <option value="University of Liverpool"> University of Liverpool </option>
                  <option value="Liverpool Hope University"> Liverpool Hope University </option>
                  <option value="Liverpool John Moores University"> Liverpool John Moores University </option>
                  <option value="University of London"> University of London </option>
                  <option value="London Metropolitan University"> London Metropolitan University </option>
                  <option value="London South Bank University"> London South Bank University </option>
                  <option value="Loughborough University"> Loughborough University </option>
                  <option value="University of Manchester"> University of Manchester </option>
                  <option value="Manchester Metropolitan University"> Manchester Metropolitan University </option>
                  <option value="Middlesex University"> Middlesex University </option>
                  <option value="Newcastle University"> Newcastle University </option>
                  <option value="Newcastle college of Higher education"> Newcastle college of Higher education </option>
                  <option value="Newman University, Birmingham"> Newman University, Birmingham </option>
                  <option value="University of Northampton"> University of Northampton </option>
                  <option value="Northumbria University"> Northumbria University </option>
                  <option value="Norwich University of the Arts"> Norwich University of the Arts </option>
                  <option value="University of Nottingham"> University of Nottingham </option>
                  <option value="Nottingham Trent University"> Nottingham Trent University </option>
                  <option value="The Open University"> The Open University </option>
                  <option value="University of Oxford"> University of Oxford </option>
                  <option value="Oxford Brookes University"> Oxford Brookes University </option>
                  <option value="Plymouth Marjon University "> Plymouth Marjon University </option>
                  <option value="University of Plymouth"> University of Plymouth </option>
                  <option value="University of Portsmouth"> University of Portsmouth </option>
                  <option value="Queen Margaret University"> Queen Margaret University </option>
                  <option value="Queen's University "> Queen's University </option>
                  <option value="Ravensbourne University London"> Ravensbourne University London </option>
                  <option value="University of Reading"> University of Reading </option>
                  <option value="Regent's University London"> Regent's University London </option>
                  <option value="The Robert Gordon University"> The Robert Gordon University </option>
                  <option value="Roehampton University"> Roehampton University </option>
                  <option value="Royal Agricultural University"> Royal Agricultural University </option>
                  <option value="University of Salford"> University of Salford </option>
                  <option value="University of Sheffield"> University of Sheffield </option>
                  <option value="Sheffield Hallam University"> Sheffield Hallam University </option>
                  <option value="University of South Wales"> University of South Wales </option>
                  <option value="University of Southampton"> University of Southampton </option>
                  <option value="Solent University"> Solent University </option>
                  <option value="University of St Andrews"> University of St Andrews </option>
                  <option value="St Mary's University"> St Mary's University </option>
                  <option value="Staffordshire University"> Staffordshire University </option>
                  <option value="University of Stirling"> University of Stirling </option>
                  <option value="University of Strathclyde"> University of Strathclyde </option>
                  <option value="University of Strathclyde"> University of Strathclyde </option>
                  <option value="University of Suffolk "> University of Suffolk </option>
                  <option value="University of Sunderland"> University of Sunderland </option>
                  <option value="University of Surrey"> University of Surrey </option>
                  <option value="University of Sussex"> University of Sussex </option>
                  <option value="Teesside University"> Teesside University </option>
                  <option value="University of Ulster"> University of Ulster </option>
                  <option value="University of the Arts London"> University of the Arts London </option>
                  <option value="Ulster University"> Ulster University </option>
                  <option value="University of Law"> University of Law </option>
                  <option value="University of Wales"> University of Wales </option>
                  <option value="The University of Wales, Trinity Saint David "> The University of Wales, Trinity Saint David </option>
                  <option value="University of Warwick, Coventry"> University of Warwick, Coventry </option>
                  <option value="University of the West of England, Bristol"> University of the West of England, Bristol </option>
                  <option value="University of the West of Scotland, Paisley, Hamilton, Ayr &amp; Dumfries"> University of the West of Scotland, Paisley, Hamilton, Ayr &amp; Dumfries </option>
                  <option value="University of West London, Ealing and Brentford"> University of West London, Ealing and Brentford </option>
                  <option value="University of Westminster, London"> University of Westminster, London </option>
                  <option value="University of Winchester"> University of Winchester </option>
                  <option value="University of Wolverhampton"> University of Wolverhampton </option>
                  <option value="University of Worcester"> University of Worcester </option>
                  <option value="Wrexham Glyndŵr University"> Wrexham Glyndŵr University </option>
                  <option value="University of York"> University of York </option>
                  <option value="York St John University"> York St John University </option>
                  <option value="University colleges"> University colleges </option>
                  <option value="AECC University College"> AECC University College </option>
                  <option value="London Institute of Banking and Finance[8]"> London Institute of Banking and Finance[8] </option>
                  <option value="University College of Estate Management, Reading"> University College of Estate Management, Reading </option>
                  <option value="University College of Osteopathy, London"> University College of Osteopathy, London </option>
                  <option value="Writtle University College"> Writtle University College </option>
                  <option value="Birkbeck, University of London"> Birkbeck, University of London </option>
                  <option value="City, University of London"> City, University of London </option>
                  <option value="Royal Central School of Speech and Drama"> Royal Central School of Speech and Drama </option>
                  <option value="Courtauld Institute of Art"> Courtauld Institute of Art </option>
                  <option value="Goldsmiths, University of London"> Goldsmiths, University of London </option>
                  <option value="Institute of Cancer Research"> Institute of Cancer Research </option>
                  <option value="King's College London"> King's College London </option>
                  <option value="London Business School"> London Business School </option>
                  <option value="London School of Economics and Political Science "> London School of Economics and Political Science </option>
                  <option value="London School of Hygiene and Tropical Medicine"> London School of Hygiene and Tropical Medicine </option>
                  <option value="Queen Mary, University of London"> Queen Mary, University of London </option>
                  <option value="Royal Academy of Music"> Royal Academy of Music </option>
                  <option value="Royal Holloway, University of London, Egham"> Royal Holloway, University of London, Egham </option>
                  <option value="Royal Veterinary College"> Royal Veterinary College </option>
                  <option value="St George's, University of London"> St George's, University of London </option>
                  <option value="School of Oriental and African Studies (SOAS)"> School of Oriental and African Studies (SOAS) </option>
                  <option value="University College London (UCL)"> University College London (UCL) </option>
											</select>												
									</div>

							</div>
							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="course_title">Course Title</label>
											<input class="form-control" id="course_title" name="course_title" type="text">
									</div>
							</div>
							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="mode_study">Mode of study</label>
											<select class="formSelect" id="mode_study" name="mode_study">
												  <option value="">Select</option>
												  <option value="Full time"> Full time </option>
												  <option value="Part-time"> Part-time </option>
												  <option value="Distance Learning"> Distance Learning </option>											
											</select>
									</div>
							</div>
							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="campus_stage">Campus</label>
											<input class="form-control" id="campus_stage" name="campus_stage" type="text">
									</div>
							</div>
							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="stage_name">Stage</label>
											<select class="formSelect" id="stage_name" name="stage_name">
												  <option value="">Select</option>
												  <option value="Year-1">Year 1</option>
												  <option value="Year-2">Year 2</option>
												  <option value="Year-3">Year 3</option>											
											</select>									
									</div>
							</div>						

							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="entry_date">Month & Year of entry</label>
											<!-- <input class="form-control datepicker" id="entry_date" name="entry_date" type="text"> -->
											<?php 
											$curr_yr = date('Y');
											$curr_yr2 = date('Y', strtotime('-2 year')); 
											$nxt_yr = date('Y', strtotime('+1 year'));
											$nxt_yr2 = date('Y', strtotime('+2 year'));
											$curr_mon = date('m');
    										$months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
											?>
											<select class="searchSelect" name="entry_date" id="entry_date">
												<option value="">Select</option>
												<?php
													for($i=$curr_yr2; $i<=$nxt_yr2;$i++) {
														foreach ($months as $key => $value) { /*if($i==$curr_yr && $key<$curr_mon) { } else { */ ?>
															<option value="<?php echo $value.' '.$i; ?>"> <?php echo $value.' '.$i; ?></option>
													<?php	//} 
														}
													}
												?>
											</select>
									</div>
							</div>								
							<div class="clearfix"></div>
					</div>
					</div>
					
					<div class="btn addshbtn"> <span class="glyphicon glyphicon-plus"></span></div>						
                </div>
				<input type="hidden" value="1" class="wrapper-count">
				<input type="hidden" value="courses" name="formtype" class="formtype">
                <div class="controls">
                                <a class="mgbtn2 back_button">Back</a>
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="courses_applied">
                                 <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                              
                                {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                        </div>
                        {{ Form::close() }}
</div>


