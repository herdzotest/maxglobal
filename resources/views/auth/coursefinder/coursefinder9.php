<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Google fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Outlined" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />

  <!-- Stylesheets -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/vendors.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/main.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/custom.css">

  <title>Course Finder</title>
  <link rel="icon" type="image/x-icon" href="la-assets/coursefinder/img/coursesCards/icons/zodocs_fav.png">
</head>

<body class="preloader-visible" data-barba="wrapper">
  <!-- preloader start -->
  <div class="preloader js-preloader">
    <div class="preloader__bg"></div>
  </div>
  <!-- preloader end -->

  <!-- barba container start -->
  <div class="barba-container" data-barba="container">


    <main class="main-content">


      <header class="header -base-sidebar border-bottom-light bg-white js-header">
        <div class="header__container py-20 px-10">
          <div class="row justify-between items-center">
            <div class="col-auto">
              <div class="d-flex items-center">
                <div class="header__explore text-dark-1">
                  <button class="d-flex items-center js-dashboard-home-9-sidebar-toggle">
                    <i class="fa fa-bars -dark-text-white"></i>
                  </button>
                </div>

                <div class="header__logo ml-30 md:ml-20">
                  <a data-barba href="<?php echo url('/coursefinder'); ?>">
                    <img class="-light-d-none" src="la-assets/coursefinder/img/general/logo.png" alt="logo">
                    <img class="-dark-d-none" src="la-assets/coursefinder/img/general/logo1.png" alt="logo">
                  </a>
                </div>
              </div>
            </div>

            <div class="col-auto">
              <div class="d-flex items-center">
                <div class="d-flex items-center sm:d-none">
                  <div>
                    <button class="js-darkmode-toggle text-light-1 d-flex items-center justify-center size-50 rounded-16 -hover-dshb-header-light">
                      <i class="text-light-1 text-24 fa fa-moon-o"></i>
                    </button>
                  </div>

                  <div class="relative">
                    <button data-maximize class="text-light-1 d-flex items-center justify-center size-50 rounded-16 -hover-dshb-header-light">
                      <i class="text-24 fa fa-window-maximize"></i>
                    </button>
                  </div>
                  
                </div>

                <div class="relative d-flex items-center ml-10">
                  <a href="#" data-el-toggle=".js-profile-toggle">
                    <img class="size-50" src="https://www.gravatar.com/avatar/ef1c169902a351837b6c7f8816cf9b5c.jpg?s=80&d=mm&r=g" alt="image">
                  </a>

                  <div class="toggle-element js-profile-toggle">
                    <div class="toggle-bottom -profile bg-white shadow-4 border-light rounded-8 mt-10">
                      <div class="px-30 py-30">

                        <div class="sidebar -dashboard">
                          <?php 
                          if(isset(Auth::user()->type)) { ?>
                          <div class="sidebar__item -is-active -dark-bg-dark-2">
                            <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="d-flex items-center text-17 lh-1 fw-500 -dark-text-white">
                              <i class="text-20 fa fa-dashboard mr-15"></i>
                              My Account
                            </a>
                          </div>

                          <div class="sidebar__item ">
                            <a href="<?php echo url('/logout'); ?>" class="d-flex items-center text-17 lh-1 fw-500 ">
                              <i class="text-20 fa fa-sign-out mr-15"></i>
                              Log out
                            </a>
                          </div>
                          <?php } else { ?>
                            <div class="sidebar__item -is-active -dark-bg-dark-2">
                            <a href="<?php echo url('/signup'); ?>" class="d-flex items-center text-17 lh-1 fw-500 -dark-text-white">
                              <i class="text-20 fa fa-user-plus mr-15"></i>
                              Sign up
                            </a>
                          </div>

                          <div class="sidebar__item ">
                            <a href="<?php echo url('/userlogin'); ?>" class="d-flex items-center text-17 lh-1 fw-500 ">
                              <i class="text-20 fa fa-sign-in mr-15"></i>
                              Log in
                            </a>
                          </div>
                          <?php } ?>

                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>


      <div class="content-wrapper js-content-wrapper">
        <div class="dashboard -home-9 px-0 js-dashboard-home-9">
          <div class="dashboard__sidebar -base scroll-bar-1 border-right-light lg:px-30">

            <div class="sidebar -base-sidebar">
              <div class="sidebar__inner">
                
                <div class="">
                  <div class="text-16 lh-1 fw-500 text-dark-1 mb-30">Pages</div>
                  <div class="">

                    <div class="">
                      <div class="accordion js-accordion">
                        <div class="accordion__item">
                          <div class="accordion__button py-10 px-20 bg-light-4 rounded-16">
                            <span class="text-16 fw-500 text-dark-1">Home</span>
                            <div class="accordion__icon">
                              <div class="icon size-20 mt-5" data-feather="chevron-down"></div>
                              <div class="icon size-20" data-feather="chevron-up"></div>
                            </div>
                          </div>
                          <div class="accordion__content">
                            <div class="accordion__content__inner px-30 pt-15 pb-10">
                              <div class="sidebar__links y-gap-5">
                                <div><a href="<?php echo url('/coursefinder'); ?>">Home 1</a></div>
                                <div><a href="<?php echo url('/coursefinder2'); ?>">Home 2</a></div>
                                <div><a href="<?php echo url('/coursefinder3'); ?>">Home 3</a></div>
                                <div><a href="<?php echo url('/coursefinder4'); ?>">Home 4</a></div>
                                <div><a href="<?php echo url('/coursefinder5'); ?>">Home 5</a></div>
                                <div><a href="<?php echo url('/coursefinder6'); ?>">Home 6</a></div>
                                <div><a href="<?php echo url('/coursefinder7'); ?>">Home 7</a></div>
                                <div><a href="<?php echo url('/coursefinder8'); ?>">Home 8</a></div>
                                <div><a href="<?php echo url('/coursefinder9'); ?>">Home 9</a></div>
                                <div><a href="<?php echo url('/coursefinder10'); ?>">Home 10</a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>


                    <div class="">
                      <a href="<?php echo url('/aboutus'); ?>" class="d-flex items-center justify-between py-15 px-20 rounded-16 text-16 lh-1 fw-500 -base-sidebar-menu-hover">
                        About Us
                      </a>
                    </div>

                    <div class="">
                      <div class="accordion js-accordion">
                        <div class="accordion__item">
                          <div class="accordion__button py-10 px-20 bg-light-4 rounded-16">
                            <span class="text-16 fw-500 text-dark-1">Courses</span>
                            <div class="accordion__icon">
                              <div class="icon size-20 mt-5" data-feather="chevron-down"></div>
                              <div class="icon size-20" data-feather="chevron-up"></div>
                            </div>
                          </div>
                          <div class="accordion__content">
                            <div class="accordion__content__inner px-30 pt-15 pb-10">
                              <div class="sidebar__links y-gap-5">
                                <div><a href="<?php echo url('/coursesearch'); ?>">Course 1</a></div>
                                <div><a href="<?php echo url('/coursesearch2'); ?>">Course 2</a></div>
                                <div><a href="<?php echo url('/coursesearch3'); ?>">Course 3</a></div>
                                <div><a href="<?php echo url('/coursesearch4'); ?>">Course 4</a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="">
                      <a href="<?php echo url('/blog'); ?>" class="d-flex items-center justify-between py-15 px-20 rounded-16 text-16 lh-1 fw-500 -base-sidebar-menu-hover">
                        Blog
                      </a>
                    </div>

                    <div class="">
                      <a href="<?php echo url('/topcourses'); ?>" class="d-flex items-center justify-between py-15 px-20 rounded-16 text-16 lh-1 fw-500 -base-sidebar-menu-hover">
                        Top Courses
                      </a>
                    </div>

                    <div class="">
                      <a href="<?php echo url('/contactus'); ?>" class="d-flex items-center justify-between py-15 px-20 rounded-16 text-16 lh-1 fw-500 -base-sidebar-menu-hover">
                        Contact Us
                      </a>
                    </div>

                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="dashboard__main mt-0">
            <div class="dashboard__content pt-0 px-15 pb-0">

              <section data-anim-wrap class="masthead -type-7 js-mouse-move-container">
                <div class="masthead__bg bg-purple-1 rounded-16">
                  <img src="la-assets/coursefinder/img/home-9/hero/bg.png" alt="image">
                </div>

                <div class="container">
                  <div class="row y-gap-20 justify-between items-center">
                    <div class="col-xl-7 col-lg-6">
                      <div class="masthead__content">
                        <h1 data-anim-child="slide-up delay-3" class="masthead__title text-white">
                          Process of Overseas Education Visa CRM with Zodocs
                        </h1>
                        <p data-anim-child="slide-up delay-4" class="masthead__text text-16 lh-2 text-white pt-10">
                          Zodocs CRM software is highly rated<br class="lg:d-none">in the study abroad education industry
                        </p>
                      </div>
                    </div>

                    <div class="col-xl-5 col-lg-6">
                      <div class="masthead-image">
                        <div class="masthead-image__img1">
                          <img data-move="20" class="js-mouse-move" src="la-assets/coursefinder/img/home-9/hero/1.png" alt="image">
                        </div>

                        <div class="-el-1"><img class="js-mouse-move" data-move="40" src="la-assets/coursefinder/img/home-9/hero/2.png" alt="icon"></div>
                        <div class="-el-2"><img class="js-mouse-move" data-move="40" src="la-assets/coursefinder/img/home-9/hero/3.png" alt="icon"></div>
                        <div class="-el-3"><img class="js-mouse-move" data-move="40" src="la-assets/coursefinder/img/home-9/hero/4.png" alt="icon"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>

              <section class="layout-pt-md layout-pb-md">
                <div data-anim-wrap class="container">
                  <div class="tabs -pills js-tabs">
                    <div class="row y-gap-20 justify-between items-end">
                      <div class="col-auto">
                        <div class="sectionTitle ">
                          <h2 class="sectionTitle__title "><br>Explore Featured Courses</h2>
                        </div>
                      </div>
                    </div>

                    <div class="tabs__content pt-60 lg:pt-50 js-tabs-content">

                      <div class="tabs__pane -tab-item-1 is-active">
                        <div class="overflow-hidden js-section-slider" data-gap="30" data-slider-cols="xl-4 lg-3 md-2 sm-2">
                          <div class="swiper-wrapper">
                            <!--Course list -->
                            <?php 
                              if(count($courses) > 0) { 
                                foreach($courses as $result) {
                            ?>
                            <div class="swiper-slide">
                              <div data-anim-child="slide-up delay-1">

                                <a href="<?php echo url('/coursedetails5/'.Crypt::encrypt($result->id)); ?>" class="coursesCard -type-1 ">
                                  <div class="relative">
                                    <div class="coursesCard__image overflow-hidden rounded-8">
                                      <?php if($result->university_logo!='') { ?>
                                        <img class="w-1/1 course_logo" src="<?php echo $result->university_logo; ?>" alt="image">
                                      <?php } else { ?>
                                        <img class="w-1/1 course_logo" src="la-assets/coursefinder/img/coursesCards/no-image.png" alt="image">
                                      <?php } ?>
                                      <div class="coursesCard__image_overlay rounded-8"></div>
                                    </div>
                                    <div class="d-flex justify-between py-10 px-10 absolute-full-center z-3">

                                    </div>
                                  </div>

                                  <div class="h-100 pt-15">
                                    <div class="d-flex items-center">
                                      <div class="text-14 lh-1 text-yellow-1 mr-10"><?php echo $result->university_name; ?></div>
                                    </div>

                                    <div class="text-17 lh-15 fw-500 text-dark-1 mt-10"><?php echo $result->course_name; ?></div>

                                    <div class="d-flex x-gap-10 items-center pt-10">

                                      <div class="d-flex items-center">
                                        <div class="mr-8">
                                          <i class="fa fa-map-marker"></i>
                                        </div>
                                        <div class="text-14 lh-1"><?php echo $result->country_name; ?></div>
                                      </div>

                                      <div class="d-flex items-center">
                                        <div class="mr-8">
                                          <img src="la-assets/coursefinder/img/coursesCards/icons/2.svg" alt="icon">
                                        </div>
                                        <div class="text-14 lh-1"><?php echo $result->intake; ?></div>
                                      </div>

                                      <div class="d-flex items-center">
                                        <div class="mr-8">
                                          <img src="la-assets/coursefinder/img/coursesCards/icons/1.svg" alt="icon">
                                        </div>
                                        <div class="text-14 lh-1"><?php echo $result->course_type; ?></div>
                                      </div>

                                    </div>

                                    <div class="coursesCard-footer">
                                      <div class="coursesCard-footer__author">
                                        <div>Annual Tuition Fees</div>
                                      </div>

                                      <div class="coursesCard-footer__price">
                                        <div></div>
                                        <div>&#8377; <?php echo $result->tuition_fees; ?></div>
                                      </div>
                                    </div>
                                  </div>
                                </a>

                              </div>
                            </div>
                            <?php }} ?>
                            <!-- End Course list -->
                          </div>


                          <button class="section-slider-nav -prev -dark-bg-dark-2 -white -absolute size-70 rounded-full shadow-5 js-prev">
                            <i class="fa fa-arrow-left text-24"></i>
                          </button>

                          <button class="section-slider-nav -next -dark-bg-dark-2 -white -absolute size-70 rounded-full shadow-5 js-next">
                            <i class="fa fa-arrow-right text-24"></i>
                          </button>

                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </section>

              <section class="layout-pt-md layout-pb-md">
                <div class="container">
                  <div class="row y-gap-20 justify-center text-center">
                    <div class="col-auto">
                      <div class="sectionTitle ">
                        <h2 class="sectionTitle__title ">How it works?</h2>
                      </div>
                    </div>
                  </div>

                  <div class="row y-gap-30 justify-between pt-60 lg:pt-40">

                    <div class="col-xl-2 col-lg-3 col-md-6">
                      <div class="d-flex flex-column items-center text-center">
                        <div class="relative size-120 d-flex justify-center items-center rounded-full bg-light-4">
                          <img src="la-assets/coursefinder/img/home-3/works/1.svg" alt="image">
                          <div class="side-badge">
                            <div class="size-35 d-flex justify-center items-center rounded-full bg-dark-1 -dark-bg-purple-1">
                              <span class="text-14 fw-500 text-white">01</span>
                            </div>
                          </div>
                        </div>
                        <div class="text-17 fw-500 text-dark-1 mt-30">Browse courses from our expert contributors.</div>
                      </div>
                    </div>


                    <div class="col-auto xl:d-none">
                      <div class="pt-30">
                        <img src="la-assets/coursefinder/img/misc/lines/1.svg" alt="icon">
                      </div>
                    </div>


                    <div class="col-xl-2 col-lg-3 col-md-6">
                      <div class="d-flex flex-column items-center text-center">
                        <div class="relative size-120 d-flex justify-center items-center rounded-full bg-light-4">
                          <img src="la-assets/coursefinder/img/home-3/works/2.svg" alt="image">
                          <div class="side-badge">
                            <div class="size-35 d-flex justify-center items-center rounded-full bg-dark-1 -dark-bg-purple-1">
                              <span class="text-14 fw-500 text-white">02</span>
                            </div>
                          </div>
                        </div>
                        <div class="text-17 fw-500 text-dark-1 mt-30">Purchase quickly and securely.</div>
                      </div>
                    </div>


                    <div class="col-auto xl:d-none">
                      <div class="pt-30">
                        <img src="la-assets/coursefinder/img/misc/lines/2.svg" alt="icon">
                      </div>
                    </div>


                    <div class="col-xl-2 col-lg-3 col-md-6">
                      <div class="d-flex flex-column items-center text-center">
                        <div class="relative size-120 d-flex justify-center items-center rounded-full bg-light-4">
                          <img src="la-assets/coursefinder/img/home-3/works/3.svg" alt="image">
                          <div class="side-badge">
                            <div class="size-35 d-flex justify-center items-center rounded-full bg-dark-1 -dark-bg-purple-1">
                              <span class="text-14 fw-500 text-white">03</span>
                            </div>
                          </div>
                        </div>
                        <div class="text-17 fw-500 text-dark-1 mt-30">That’s it! Start learning right away.</div>
                      </div>
                    </div>


                  </div>
                </div>
              </section>

              <section class="layout-pt-md layout-pb-lg">
                <div class="container">
                  <div class="row y-gap-20 justify-center text-center">
                    <div class="col-auto">
                      <div class="sectionTitle ">
                        <h2 class="sectionTitle__title ">Testimonials</h2>
                      </div>
                    </div>
                  </div>

                  <div class="row justify-center pt-60">
                    <div class="col-xl-6 col-lg-8 col-md-10">
                      <div class="overflow-hidden js-testimonials-slider">
                        <div class="swiper-wrapper">

                          <div class="swiper-slide h-100">
                            <div data-anim="slide-up" class="testimonials -type-2 text-center">
                              <div class="testimonials__icon">
                                <img src="la-assets/coursefinder/img/misc/quote.svg" alt="quote">
                              </div>
                              <div class="testimonials__text md:text-20 fw-500 text-dark-1">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.</div>
                              <div class="testimonials__author">
                                <h5 class="text-17 lh-15 fw-500">Lorem ipsum</h5>
                                <div class="mt-5">Lorem ipsum, Consetetur sadipscing</div>
                              </div>
                            </div>
                          </div>

                          <div class="swiper-slide h-100">
                            <div data-anim="slide-up" class="testimonials -type-2 text-center">
                              <div class="testimonials__icon">
                                <img src="la-assets/coursefinder/img/misc/quote.svg" alt="quote">
                              </div>
                              <div class="testimonials__text md:text-20 fw-500 text-dark-1">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.</div>
                              <div class="testimonials__author">
                                <h5 class="text-17 lh-15 fw-500">Lorem ipsum</h5>
                                <div class="mt-5">Lorem ipsum, Consetetur sadipscing</div>
                              </div>
                            </div>
                          </div>

                          <div class="swiper-slide h-100">
                            <div data-anim="slide-up" class="testimonials -type-2 text-center">
                              <div class="testimonials__icon">
                                <img src="la-assets/coursefinder/img/misc/quote.svg" alt="quote">
                              </div>
                              <div class="testimonials__text md:text-20 fw-500 text-dark-1">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.</div>
                              <div class="testimonials__author">
                                <h5 class="text-17 lh-15 fw-500">Lorem ipsum</h5>
                                <div class="mt-5">Lorem ipsum, Consetetur sadipscing</div>
                              </div>
                            </div>
                          </div>

                          <div class="swiper-slide h-100">
                            <div data-anim="slide-up" class="testimonials -type-2 text-center">
                              <div class="testimonials__icon">
                                <img src="la-assets/coursefinder/img/misc/quote.svg" alt="quote">
                              </div>
                              <div class="testimonials__text md:text-20 fw-500 text-dark-1">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.</div>
                              <div class="testimonials__author">
                                <h5 class="text-17 lh-15 fw-500">Lorem ipsum</h5>
                                <div class="mt-5">Lorem ipsum, Consetetur sadipscing</div>
                              </div>
                            </div>
                          </div>

                          <div class="swiper-slide h-100">
                            <div data-anim="slide-up" class="testimonials -type-2 text-center">
                              <div class="testimonials__icon">
                                <img src="la-assets/coursefinder/img/misc/quote.svg" alt="quote">
                              </div>
                              <div class="testimonials__text md:text-20 fw-500 text-dark-1">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.</div>
                              <div class="testimonials__author">
                                <h5 class="text-17 lh-15 fw-500">Lorem ipsum</h5>
                                <div class="mt-5">Lorem ipsum, Consetetur sadipscing</div>
                              </div>
                            </div>
                          </div>

                        </div>

                        <div class="pt-60 lg:pt-40">
                          <div class="pagination -avatars row x-gap-40 y-gap-20 justify-center js-testimonials-pagination">

                            <div class="col-auto">
                              <div class="pagination__item is-active">
                                <img src="la-assets/coursefinder/img/home-3/testimonials/1.png" alt="image">
                              </div>
                            </div>

                            <div class="col-auto">
                              <div class="pagination__item ">
                                <img src="la-assets/coursefinder/img/home-3/testimonials/2.png" alt="image">
                              </div>
                            </div>

                            <div class="col-auto">
                              <div class="pagination__item ">
                                <img src="la-assets/coursefinder/img/home-3/testimonials/3.png" alt="image">
                              </div>
                            </div>

                            <div class="col-auto">
                              <div class="pagination__item ">
                                <img src="la-assets/coursefinder/img/home-3/testimonials/4.png" alt="image">
                              </div>
                            </div>

                            <div class="col-auto">
                              <div class="pagination__item ">
                                <img src="la-assets/coursefinder/img/home-3/testimonials/5.png" alt="image">
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>

            <footer class="footer">
              <div class="container">
                <div class="py-30 border-top-light">
                  <div class="row items-center justify-between">
                    <div class="col-auto">
                      <div class="text-13 lh-1">© 2022 ZODOCS. All Right Reserved.</div>
                    </div>

                    <div class="col-auto">
                      <div class="d-flex items-center">
                        <div class="d-flex items-center flex-wrap x-gap-20">
                          <div>
                            Powered By <a href="http://demo.zodocs.com/" target="_blank" class="text-13 lh-1">ZODOCS</a>
                          </div>
                          <div>
                            Solution By <a href="https://www.labgex.com/" target="_blank" class="text-13 lh-1">LabGex Innovations (P) Ltd</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </footer>
          </div>
        </div>
      </div>
    </main>

    <aside class="sidebar-menu toggle-element js-msg-toggle js-dsbh-sidebar-menu">
      <div class="sidebar-menu__bg"></div>

      <div class="sidebar-menu__content scroll-bar-1 py-30 px-40 sm:py-25 sm:px-20 bg-white -dark-bg-dark-1">
        <div class="row items-center justify-between mb-30">
          <div class="col-auto">
            <div class="-sidebar-buttons">
              <button data-sidebar-menu-button="messages" class="text-17 text-dark-1 fw-500 -is-button-active">
                Messages
              </button>

              <button data-sidebar-menu-button="messages-2" data-sidebar-menu-target="messages" class="d-flex items-center text-17 text-dark-1 fw-500">
                <i class="icon-chevron-left text-11 text-purple-1 mr-10"></i>
                Messages
              </button>

              <button data-sidebar-menu-button="settings" data-sidebar-menu-target="messages" class="d-flex items-center text-17 text-dark-1 fw-500">
                <i class="icon-chevron-left text-11 text-purple-1 mr-10"></i>
                Settings
              </button>

              <button data-sidebar-menu-button="contacts" data-sidebar-menu-target="messages" class="d-flex items-center text-17 text-dark-1 fw-500">
                <i class="icon-chevron-left text-11 text-purple-1 mr-10"></i>
                Contacts
              </button>
            </div>
          </div>

          <div class="col-auto">
            <div class="row x-gap-10">
              <div class="col-auto">
                <button data-sidebar-menu-target="settings" class="button -purple-3 text-purple-1 size-40 d-flex items-center justify-center rounded-full">
                  <i class="icon-setting text-16"></i>
                </button>
              </div>
              <div class="col-auto">
                <button data-sidebar-menu-target="contacts" class="button -purple-3 text-purple-1 size-40 d-flex items-center justify-center rounded-full">
                  <i class="icon-friend text-16"></i>
                </button>
              </div>
              <div class="col-auto">
                <button data-el-toggle=".js-msg-toggle" class="button -purple-3 text-purple-1 size-40 d-flex items-center justify-center rounded-full">
                  <i class="icon-close text-14"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="relative js-menu-switch">
          <div data-sidebar-menu-open="messages" class="sidebar-menu__item -sidebar-menu -sidebar-menu-opened">
            <form class="search-field rounded-8 h-50" action="https://creativelayers.net/themes/educrat-html/post">
              <input class="bg-light-3 pr-50" type="text" placeholder="Search Courses">
              <button class="" type="submit">
                <i class="icon-search text-light-1 text-20"></i>
              </button>
            </form>

            <div class="accordion -block text-left pt-20 js-accordion">

              <div class="accordion__item border-light rounded-16">
                <div class="accordion__button">
                  <div class="accordion__icon size-30 -dark-bg-dark-2 mr-10">
                    <div class="icon d-flex items-center justify-center">
                      <span class="lh-1 fw-500">2</span>
                    </div>
                    <div class="icon d-flex items-center justify-center">
                      <span class="lh-1 fw-500">2</span>
                    </div>
                  </div>
                  <span class="text-17 fw-500 text-dark-1 pt-3">Starred</span>
                </div>

                <div class="accordion__content">
                  <div class="accordion__content__inner pl-20 pr-20 pb-20">
                    <div data-sidebar-menu-target="messages-2" class="row x-gap-10 y-gap-10 pointer">
                      <div class="col-auto">
                        <img src="img/dashboard/right-sidebar/messages/1.png" alt="image">
                      </div>
                      <div class="col">
                        <div class="text-15 lh-12 fw-500 text-dark-1 pt-8">Darlene Robertson</div>
                        <div class="text-14 lh-1 mt-5"><span class="text-dark-1">You:</span> Hello</div>
                      </div>
                      <div class="col-auto">
                        <div class="text-13 lh-12 pt-8">35 mins</div>
                      </div>
                    </div>

                    <div data-sidebar-menu-target="messages-2" class="row x-gap-10 y-gap-10 pt-15 pointer">
                      <div class="col-auto">
                        <img src="img/dashboard/right-sidebar/messages/1.png" alt="image">
                      </div>
                      <div class="col">
                        <div class="text-15 lh-12 fw-500 text-dark-1 pt-8">Darlene Robertson</div>
                        <div class="text-14 lh-1 mt-5"><span class="text-dark-1">You:</span> Hello</div>
                      </div>
                      <div class="col-auto">
                        <div class="text-13 lh-12 pt-8">35 mins</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="accordion__item border-light rounded-16">
                <div class="accordion__button">
                  <div class="accordion__icon size-30 -dark-bg-dark-2 mr-10">
                    <div class="icon d-flex items-center justify-center">
                      <span class="lh-1 fw-500">2</span>
                    </div>
                    <div class="icon d-flex items-center justify-center">
                      <span class="lh-1 fw-500">2</span>
                    </div>
                  </div>
                  <span class="text-17 fw-500 text-dark-1 pt-3">Group</span>
                </div>

                <div class="accordion__content">
                  <div class="accordion__content__inner pl-20 pr-20 pb-20">
                    <div data-sidebar-menu-target="messages-2" class="row x-gap-10 y-gap-10 pointer">
                      <div class="col-auto">
                        <img src="img/dashboard/right-sidebar/messages/1.png" alt="image">
                      </div>
                      <div class="col">
                        <div class="text-15 lh-12 fw-500 text-dark-1 pt-8">Darlene Robertson</div>
                        <div class="text-14 lh-1 mt-5"><span class="text-dark-1">You:</span> Hello</div>
                      </div>
                      <div class="col-auto">
                        <div class="text-13 lh-12 pt-8">35 mins</div>
                      </div>
                    </div>

                    <div data-sidebar-menu-target="messages-2" class="row x-gap-10 y-gap-10 pt-15 pointer">
                      <div class="col-auto">
                        <img src="img/dashboard/right-sidebar/messages/1.png" alt="image">
                      </div>
                      <div class="col">
                        <div class="text-15 lh-12 fw-500 text-dark-1 pt-8">Darlene Robertson</div>
                        <div class="text-14 lh-1 mt-5"><span class="text-dark-1">You:</span> Hello</div>
                      </div>
                      <div class="col-auto">
                        <div class="text-13 lh-12 pt-8">35 mins</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="accordion__item border-light rounded-16">
                <div class="accordion__button">
                  <div class="accordion__icon size-30 -dark-bg-dark-2 mr-10">
                    <div class="icon d-flex items-center justify-center">
                      <span class="lh-1 fw-500">2</span>
                    </div>
                    <div class="icon d-flex items-center justify-center">
                      <span class="lh-1 fw-500">2</span>
                    </div>
                  </div>
                  <span class="text-17 fw-500 text-dark-1 pt-3">Private</span>
                </div>

                <div class="accordion__content">
                  <div class="accordion__content__inner pl-20 pr-20 pb-20">
                    <div data-sidebar-menu-target="messages-2" class="row x-gap-10 y-gap-10 pointer">
                      <div class="col-auto">
                        <img src="img/dashboard/right-sidebar/messages/1.png" alt="image">
                      </div>
                      <div class="col">
                        <div class="text-15 lh-12 fw-500 text-dark-1 pt-8">Darlene Robertson</div>
                        <div class="text-14 lh-1 mt-5"><span class="text-dark-1">You:</span> Hello</div>
                      </div>
                      <div class="col-auto">
                        <div class="text-13 lh-12 pt-8">35 mins</div>
                      </div>
                    </div>

                    <div data-sidebar-menu-target="messages-2" class="row x-gap-10 y-gap-10 pt-15 pointer">
                      <div class="col-auto">
                        <img src="img/dashboard/right-sidebar/messages/1.png" alt="image">
                      </div>
                      <div class="col">
                        <div class="text-15 lh-12 fw-500 text-dark-1 pt-8">Darlene Robertson</div>
                        <div class="text-14 lh-1 mt-5"><span class="text-dark-1">You:</span> Hello</div>
                      </div>
                      <div class="col-auto">
                        <div class="text-13 lh-12 pt-8">35 mins</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>


          <div data-sidebar-menu-open="messages-2" class="sidebar-menu__item -sidebar-menu">
            <div class="row x-gap-10 y-gap-10">
              <div class="col-auto">
                <img src="img/dashboard/right-sidebar/messages-2/1.png" alt="image">
              </div>
              <div class="col">
                <div class="text-15 lh-12 fw-500 text-dark-1 pt-8">Arlene McCoy</div>
                <div class="text-14 lh-1 mt-5">Active</div>
              </div>
            </div>

            <div class="mt-20 pt-30 border-top-light">
              <div class="row y-gap-20">
                <div class="col-12">
                  <div class="row x-gap-10 y-gap-10 items-center">
                    <div class="col-auto">
                      <img src="img/dashboard/right-sidebar/messages-2/2.png" alt="image">
                    </div>
                    <div class="col-auto">
                      <div class="text-15 lh-12 fw-500 text-dark-1">Albert Flores</div>
                    </div>
                    <div class="col-auto">
                      <div class="text-14 lh-1 ml-3">35 mins</div>
                    </div>
                  </div>
                  <div class="bg-light-3 rounded-8 px-30 py-20 mt-15">
                    How likely are you to recommend our company to your friends and family?
                  </div>
                </div>

                <div class="col-12">
                  <div class="row x-gap-10 y-gap-10 items-center justify-end">
                    <div class="col-auto">
                      <div class="text-14 lh-1 mr-3">35 mins</div>
                    </div>
                    <div class="col-auto">
                      <div class="text-15 lh-12 fw-500 text-dark-1">You</div>
                    </div>
                    <div class="col-auto">
                      <img src="img/dashboard/right-sidebar/messages-2/3.png" alt="image">
                    </div>
                  </div>
                  <div class="text-right bg-light-7 -dark-bg-dark-2 text-purple-1 rounded-8 px-30 py-20 mt-15">
                    How likely are you to recommend our company to your friends and family?
                  </div>
                </div>

                <div class="col-12">
                  <div class="row x-gap-10 y-gap-10 items-center">
                    <div class="col-auto">
                      <img src="img/dashboard/right-sidebar/messages-2/3.png" alt="image">
                    </div>
                    <div class="col-auto">
                      <div class="text-15 lh-12 fw-500 text-dark-1">Cameron Williamson</div>
                    </div>
                    <div class="col-auto">
                      <div class="text-14 lh-1 ml-3">35 mins</div>
                    </div>
                  </div>
                  <div class="bg-light-3 rounded-8 px-30 py-20 mt-15">
                    Ok, Understood!
                  </div>
                </div>
              </div>
            </div>

            <div class="mt-30 pb-20">
              <form class="contact-form row y-gap-20" action="https://creativelayers.net/themes/educrat-html/post">

                <div class="col-12">

                  <textarea placeholder="Write a message" rows="7"></textarea>
                </div>

                <div class="col-12">
                  <button type="submit" class="button -md -purple-1 text-white">Send Message</button>
                </div>
              </form>
            </div>
          </div>
          <div data-sidebar-menu-open="contacts" class="sidebar-menu__item -sidebar-menu">
            <div class="tabs -pills js-tabs">
              <div class="tabs__controls d-flex js-tabs-controls">

                <button class="tabs__button px-15 py-8 rounded-8 text-dark-1 js-tabs-button is-active" data-tab-target=".-tab-item-1" type="button">Contacts</button>

                <button class="tabs__button px-15 py-8 rounded-8 text-dark-1 js-tabs-button " data-tab-target=".-tab-item-2" type="button">Request</button>

              </div>

              <div class="tabs__content pt-30 js-tabs-content">

                <div class="tabs__pane -tab-item-1 is-active">
                  <div class="row x-gap-10 y-gap-10 items-center">
                    <div class="col-auto">
                      <img src="img/dashboard/right-sidebar/contacts/1.png" alt="image">
                    </div>
                    <div class="col-auto">
                      <div class="text-15 lh-12 fw-500 text-dark-1">Darlene Robertson</div>
                    </div>
                  </div>
                </div>

                <div class="tabs__pane -tab-item-2 ">
                  <div class="row x-gap-10 y-gap-10 items-center">
                    <div class="col-auto">
                      <img src="img/dashboard/right-sidebar/contacts/1.png" alt="image">
                    </div>
                    <div class="col-auto">
                      <div class="text-15 lh-12 fw-500 text-dark-1">Darlene Robertson</div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>


          <div data-sidebar-menu-open="settings" class="sidebar-menu__item -sidebar-menu">
            <div class="text-17 text-dark-1 fw-500">Privacy</div>
            <div class="text-15 mt-5">You can restrict who can message you</div>
            <div class="mt-30">

              <div class="form-radio d-flex items-center ">
                <div class="radio">
                  <input type="radio">
                  <div class="radio__mark">
                    <div class="radio__icon"></div>
                  </div>
                </div>
                <div class="lh-1 text-13 text-dark-1 ml-12">My contacts only</div>
              </div>


              <div class="form-radio d-flex items-center mt-15">
                <div class="radio">
                  <input type="radio">
                  <div class="radio__mark">
                    <div class="radio__icon"></div>
                  </div>
                </div>
                <div class="lh-1 text-13 text-dark-1 ml-12">My contacts and anyone in my courses</div>
              </div>


              <div class="form-radio d-flex items-center mt-15">
                <div class="radio">
                  <input type="radio">
                  <div class="radio__mark">
                    <div class="radio__icon"></div>
                  </div>
                </div>
                <div class="lh-1 text-13 text-dark-1 ml-12">Anyone on the site</div>
              </div>

            </div>

            <div class="text-17 text-dark-1 fw-500 mt-30 mb-30">Notification preferences</div>
            <div class="form-switch d-flex items-center">
              <div class="switch">
                <input type="checkbox">
                <span class="switch__slider"></span>
              </div>
              <div class="text-13 lh-1 text-dark-1 ml-10">Email</div>
            </div>

            <div class="text-17 text-dark-1 fw-500 mt-30 mb-30">General</div>
            <div class="form-switch d-flex items-center">
              <div class="switch">
                <input type="checkbox">
                <span class="switch__slider"></span>
              </div>
              <div class="text-13 lh-1 text-dark-1 ml-10">Use enter to send</div>
            </div>
          </div>
        </div>
      </div>
    </aside>
  </div>
  <!-- barba container end -->

  <!-- JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js" integrity="sha512-QSkVNOCYLtj73J4hbmVoOV6KVZuMluZlioC+trLpewV8qMjsWqlIQvkn1KGX2StWvPMdWGBqim1xlC8krl1EKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
  <script src="la-assets/coursefinder/js/vendors.js"></script>
  <script src="la-assets/coursefinder/js/main.js"></script>
</body>

</html>