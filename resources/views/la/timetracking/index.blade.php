@extends("la.layouts.app")

@section("contentheader_title", "Time tracking settings")
@section("section", "Time tracking settings")
@section("htmlheader_title", "Time tracking settings")

@section("headerElems")
@la_access("Employees", "create")
	<button class="btn btn-success btn-sm pull-right addbtn" data-toggle="modal"  data-target="#AddModal"><i class="icon-plus"></i> Add time-settings</button>
@endla_access
@endsection

@section("main-content")

<div class="box box-success">
	<div class="box-body">
		<table id="example1" class="table table-hover mg-table mgt2">
		<thead>
			<tr class="success">
				<th>Status</th>
				<th>From-time</th>
				<th>To-time</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<tr role="row" class="odd">
				<?php
				if(is_array($timeSettings) && count($timeSettings) > 0) { ?>
					<td><a href="{{ url(config('laraadmin.adminRoute') . '/settings/timetracking/edit') }}"><?php echo $timeSettings->status; ?></a></td>
					<td><?php echo $timeSettings->from_time; ?></td>
					<td><?php echo $timeSettings->to_time; ?></td>
					<td>
						<a href="#" data-status="<?php echo $timeSettings->status; ?>" data-fromtime="<?php echo $timeSettings->from_time; ?>" data-totime="<?php echo $timeSettings->to_time; ?>" data-id="<?php echo $timeSettings->id; ?>"  class="btn btn-warning btn-xs mx-timetrack-edit"><i class="fa fa-edit"></i></a>
						<!--
						<form method="POST" action="#" style="display:inline" onsubmit="return confirm('Are you sure you wish to delete?');">
							<input name="_method" type="hidden" value="DELETE">
							<button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>
						</form>
						-->
					</td>
				<?php 
				} else { ?>
					<td>No entry found!!</td>
				<?php 
				}
				?>
			</tr>
		</tbody>
		</table>
	</div>
</div>

@la_access("Employees", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Time track settings</h4>
			</div>
			{{ Form::open(array('url' => route('timetracking.post.create'), 'id' => 'timetrack-edit-form')) }}
			<div class="modal-body">
				<div class="box-body">
				
					<input name="_method" type="hidden" value="PUT">
					<input name="id" id="timeTrackid" type="hidden" value="0">
					
					<div class="form-group">
						<label for="name">Status* :</label>
						<select class="form-control" name="status" id="status">
							<option value="ANYTIME">All Days</option>
							<option value="OFFICE-TIME">Week Days (Monday - Friday)</option>
						</select>
					</div>
					<div class="form-group">
						<label for="name">From-time* :</label>
						<input placeholder="Enter from-time" value="" type="text" name="input_starttime" id="input_starttime" class="form-control timepicker" required>
					</div>
					<div class="form-group">
						<label for="name">To-time* :</label>
						<input placeholder="Enter end-time" value="" type="text" name="input_endtime" id="input_endtime" class="form-control timepicker" required>
					</div>
					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default mgbtn2" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success mgbtn','value' => 'Submit']) !!}
			</div>
			{{ Form::close() }}
			
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {
    $('.timepicker').datetimepicker({
        format: 'hh:mm A',
    }); 
	$("#timetrack-edit-form").validate({	
	});
	
	$('body').off('click', '.mx-timetrack-edit');
	$('body').on('click', '.mx-timetrack-edit', function() {
		
		var id = $(this).attr('data-id');
		var fromtime = $(this).attr('data-fromtime');
		var totime = $(this).attr('data-totime');
		var status = $(this).attr('data-status');
		
		$("#input_starttime").val(fromtime);
		$("#input_endtime").val(totime);
		$("#timeTrackid").val(id);
		$("#status").val(status);
		$('#AddModal').modal('show');
	});
	$("#AddModal").on('hide.bs.modal', function(){
		$('#AddModal input').val('');
		$('#AddModal select').val('ANYTIME');
	});
});
</script>
@endpush
