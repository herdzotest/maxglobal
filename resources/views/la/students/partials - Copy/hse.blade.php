<div id="tab_4" class="tab-pane" >
                                        <div class="content-head">
                                        
                                                        <h4>HSE Details</h4>
                                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                                                </div>
                                                {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'school-add-form']) !!}
               
                                <div class="pane">


                                        <div class="row">
                                                <div class=" col s12 ">
                                                        <div class="input-field">
                                                                        <!-- <i class="material-icons prefix">account_circle</i> -->
                                                                        <input id="board_name_hse"  required name="board_name_hse" type="text" class="validate">
                                                                        <label for="board_name_hse">Board Name</label>
                                                                        <!-- <span class="helper-text" data-error="wrong"
                                                                        data-success="right">As per Passport</span> -->
                                                        </div>


                                                </div>

                                        </div>

                                        <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="hse_start_year" required name="hse_start_year" type="text" class="validate">
                                                                        <label for="hse_start_year">Start Year</label>
                                                                        <!-- <span class="helper-text" data-error="wrong"
                                                                        data-success="right">As per Passport</span> -->
                                                                </div>
                                                </div>

                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="hse_end_year" name="hse_end_year" type="text" required class="validate">
                                                                                <label for="hse_end_year">Completion Year</label>

                                                                        </div>
                                                </div>


                                        </div>
                                        <div class="row">
                                                        <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="hse_language" name="hse_language" type="text" class="validate">
                                                                                <label for="hse_language">Medium of Instruction</label>

                                                                        </div>
                                                        </div>
        
                                                        <div class="col s6">
                                                                        <div class="input-field ">
                                                                                        <input id="hse_grade" name="hse_grade" type="text" class="validate">
                                                                                        <label for="hse_grade">Percentage / Grade Obtained</label>
        
                                                                                </div>
                                                        </div>

                                                        <div class="col s6">
                                                                        <div class="input-field ">
                                                                                        <input id="hse_eng_score" name="hse_eng_score" type="text" class="validate">
                                                                                        <label for="hse_eng_score">Marks Obtained in English</label>
        
                                                                                </div>
                                                        </div>
        
        
                                                </div>
                                        <br>
                                        <div class="row">
                                                <div class="col s6">
                                                        <label>Upload HSE Documents</label>
                                                        <div class="file-field input-field">
                                                                        <div class="btn">
                                                                                <span>Upload</span>
                                                                                <input type="file" multiple > <i class="fa fa-cloud-upload"></i>
                                                                        </div>
                                                                        <div class="file-path-wrapper">
                                                                                <input class="file-path validate" name="document_higher" type="text" placeholder = "Upload file" >
                                                                        </div>
                                                        </div>
														<div class="uploaded-file" id="document_higher" data-type="file"></div>
                                                </div>


                                                      </div>



                                </div>
                                <div class="controls">
                                <input type="hidden" value="<?php echo $studentId; ?>" name="student_id" id="student_id">
                                                <a class="mgbtn2">Back</a>
                                                <input id="form_name" type="hidden" class="validate" name="type_of" value="higher">
                                                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                                                {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
                        </div>