Dear {{ $user }},<br><br>

Your next followup date & time is {{ $followup }}.<br>

For more details please contact your consultant.<br><br>

Best Regards,<br>
Zodocs


