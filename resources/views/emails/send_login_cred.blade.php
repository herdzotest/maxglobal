Dear {{ $user }},<br><br>

You have been registered on {{ url('/') }}.<br><br>

Your login credentials for the same are as below:<br><br>

Username: {{ $user }}<br>
password: {{ $password }}<br><br>

You can login on <a href="{{ url('/userlogin') }}">{{ str_replace("http://", "", url('/userlogin')) }}</a>.<br><br>

Best Regards,<br>
Zodocs
