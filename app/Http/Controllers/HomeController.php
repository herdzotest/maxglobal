<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $roleCount = \App\Role::count();
        // echo  $roleCount;
        // exit;
		if($roleCount != 0) {
			if($roleCount != 0) {
				return redirect('admin');
			//	return view('home');
			}
		} else {
			return view('errors.error', [
				'title' => 'Migration not completed',
				'message' => 'Please run command <code>php artisan db:seed</code> to generate required table data.',
			]);
		}
    }
	
	/*
	public function resetPassword(Request $request)
	{
		echo 'dsdssd'; exit;
		
		//Validate input
		$validator = Validator::make($request->all(), [
			'email' => 'required|email|exists:users,email',
			'password' => 'required|confirmed',
			//'token' => 'required' 
		]);

		//check if payload is valid before moving on
		if ($validator->fails()) {
			return redirect()->back()->withErrors(['email' => 'Please complete the form']);
		}

		$password = $request->password;
		// Validate the token
		$tokenData = DB::table('password_resets')
		->where('token', $request->token)->first();
		// Redirect the user back to the password reset request form if the token is invalid
		if (!$tokenData) return view('auth.passwords.email');

		$user = User::where('email', $tokenData->email)->first();
	// Redirect the user back if the email is invalid
		if (!$user) return redirect()->back()->withErrors(['email' => 'Email not found']);
	//Hash and update the new password
		$user->password = \Hash::make($password);
		$user->update(); //or $user->save();

		//login the user immediately they change password successfully
		Auth::login($user);

		//Delete the token
		DB::table('password_resets')->where('email', $user->email)
		->delete();

		//Send Email Reset Success Email
		if ($this->sendSuccessEmail($tokenData->email)) {
			return view('index');
		} else {
			return redirect()->back()->withErrors(['email' => trans('A Network Error occurred. Please try again.')]);
		}		
	}
	*/
}