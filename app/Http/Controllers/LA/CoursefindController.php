<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;
use Dwij\Laraadmin\Helpers\LAHelper;
use Dwij\Laraadmin\Models\LAConfigs;

use App\Models\Upload;
use App\User;
use App\Models\Employee;
use App\Role;
use Mail;
use Log;
use DateTime;


class CoursefindController extends Controller
{	
	public $show_action = true;
	public $view_col = 'name';
	public $listing_cols = ['id', 'name','mobile', 'email', 'city', 'date_birth'];

	public function __construct() {

		$path = \Request::path();
		$userTypeAccess = array('SUPER_ADMIN','MANAGER','STUDENT','CONSULTANTS','UNIVERSITY_USER','MAIN_AGENTS','SUBCONTRACTORS');
		if ($path=="registration" || $path=="registration_save" || \Request::is('user/verify/*')) {
			
		} else if(!in_array(Auth::user()->type, $userTypeAccess) || (Auth::user()->type == 'STUDENT' && $path == 'admin/students')) {
			return abort(403);	
		}
	}

	public function index()
	{			
		$countries = DB::table('countries')->select('country')->where('status', 1)->get();
		$course = DB::table('manage_courses')->select('course_name')->where('status', 1)->get();
		$university = DB::table('universities')->select('university')->where('status', 1)->get();			
		return View('la.coursefind.index', [
			'show_actions' => $this->show_action,
			'countries' => $countries,
			'university' => $university,
			'course' => $course
		]);
	}
  
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		
		if(isset($_GET['country'])){ $country = $_GET['country']; } else {$country = ''; }
		if(isset($_GET['course'])){ $course = $_GET['course']; } else {$course = ''; } 
		if(isset($_GET['university'])){ $university = $_GET['university']; } else {$university = ''; } 
		if(isset($_GET['intake_year'])){ $intake_year = $_GET['intake_year']; } else {$intake_year = ''; }
		if(isset($_GET['intake_month'])){ $intake_month = $_GET['intake_month']; } else {$intake_month = ''; }
		$conditions = "";
		if ($country != '') {
			$conditions .= " AND (c.country = '$country') ";
		}
		if ($course != '') { 
			$conditions .= " AND (mc.course_name = '$course') "; 
		}
		if ($university != '') { 
			$conditions .= " AND (u.university = '$university') "; 
		}
		if ($intake_year != '') { 
			$conditions .= " AND (a.intake_year = $intake_year) "; 
		}
		if ($intake_month != '') { 
			$conditions .= " AND (a.intake_month = '$intake_month') ";
		}

		$query = "SELECT a.*,u.university AS university_name,c.country AS country_name,mc.course_name,mc.university_logo FROM applynow AS a 
			LEFT JOIN universities AS u ON u.id = a.university 
			LEFT JOIN countries AS c ON c.id = a.country 
			LEFT JOIN manage_courses AS mc ON mc.id = a.course 
			WHERE a.status <> 'Deleted' 
			$conditions
			ORDER BY a.created_at DESC";
		$students = DB::select($query);
		
		
		$out =  array();
		$out['data'] = array();
		$out['draw'] = count($students);
		$out['recordsTotal'] = count($students);
		$out['recordsFiltered'] = count($students);
		
		foreach($students as $key => $result) {
			
			$studentData = array();
			$studentData[] = $key+1;
			$avatatImg = ($result->university_logo == '') ? '../la-assets/coursefinder/img/coursesCards/no-image.png' : $result->university_logo;
			$studentData[] = '<a href="'.url(config('laraadmin.adminRoute') . '/coursefind/'.$result->apply_id).'"><img src="'.$avatatImg.'" alt=""/><span>'.$result->university_name.'</span></a>';
			$studentData[] = $result->name;
			$studentData[] = $result->email;
			$studentData[] = $result->mobile;
			$studentData[] = $result->country_name;
			$studentData[] = $result->course_name;
			if($this->show_action) {
				$output = '';
				$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/coursefind/viewapplication/'.$result->apply_id).'" class="btn btn-warning btn-xs" data-toggle="tooltip" title="View" ><i class="fa fa-eye"></i></a>';
				/*$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/coursefind/'.$result->apply_id.'/edit').'" class="btn btn-warning btn-xs editbtn" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></a>';*/
				$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.coursefind.destroy', $result->apply_id], 'method' => 'delete', 'style'=>'display:inline','onSubmit'=>"return confirm('Are you sure you wish to delete?');"]);
					$output .= ' <button class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete" type="submit"><i class="fa fa-trash"></i></button>';
					$output .= Form::close();
				
				$studentData[] = (string)$output;
			}
			array_push($out['data'],$studentData);
		}
		
		$out = json_encode($out,true);
		
		return $out;
	}

	public function show($id)
	{
		$applyDetails = DB::table('applynow AS a')->select('a.*')
			->addSelect('unv.university AS university_name')->addSelect('cnt.country AS country_name')->addSelect('mc.course_name')->addSelect('mc.course_type')->addSelect('mc.university_logo')
			->leftJoin('universities AS unv', 'unv.id', '=', 'a.university')
			->leftJoin('countries AS cnt', 'cnt.id', '=', 'a.country')
			->leftJoin('manage_courses AS mc', 'mc.id', '=', 'a.course')
			->where('a.apply_id','=',$id)->get();
			return view('la.coursefind.show', [
				'no_header' => true,
				'no_padding' => "no-padding",
				'applyDetails' => $applyDetails
			]);		
	}

	public function edit($id)
	{	
		$countries = DB::table('countries')->select('id')->addSelect('country')->where('status', 1)->get();
		$courses = DB::table('manage_courses')->select('id')->addSelect('course_name')->where('status', 1)->get();
		$university = DB::table('universities')->select('id')->addSelect('university')->where('status', 1)->get();
		$applyDetails = DB::table('applynow AS a')->select('a.*')
			->addSelect('unv.university AS university_name')->addSelect('cnt.country AS country_name')->addSelect('mc.course_name')->addSelect('mc.course_type')->addSelect('mc.university_logo')
			->leftJoin('universities AS unv', 'unv.id', '=', 'a.university')
			->leftJoin('countries AS cnt', 'cnt.id', '=', 'a.country')
			->leftJoin('manage_courses AS mc', 'mc.id', '=', 'a.course')
			->where('a.apply_id','=',$id)->get();
			return view('la.coursefind.edit', [
				'applyDetails' => $applyDetails,
				'countries' => $countries,
				'university' => $university,
				'courses' => $courses
			]);		
	}

	public function update(Request $request, $id)
	{

	}


	public function destroy($id)
	{
		if($id != '') {
			DB::table('applynow')->where('apply_id', $id)->update([ 'status' => 'Deleted' ]);
		}
		return redirect(config('laraadmin.adminRoute')."/coursefind");
	}
		
}
