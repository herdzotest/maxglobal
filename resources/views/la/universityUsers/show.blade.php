@extends('la.layouts.app')

@section('htmlheader_title')
	University user View
@endsection


@section('main-content')
<div id="page-content" class="profile2">
	<div class="bg-primary clearfix">
		<div class="col-md-7">
			<div class="row">
				<div class="col-md-2">
					<!-- <img class="profile-image" src="{{ asset('la-assets/img/1.jpg') }}" alt=""> -->

				<?php
				$avatatImg = ($employee->profilePic == '') ? 'https://www.gravatar.com/avatar/ef1c169902a351837b6c7f8816cf9b5c.jpg?s=80&d=mm&r=g' : $employee->profilePic;
				?>
				<img class="profile-image" src="<?php echo $avatatImg; ?>" alt="{{ $employee->$view_col }}">

					<!-- <div class="profile-icon text-primary"><i class="fa {{ $module->fa_icon }}"></i></div> -->
				</div>
				<div class="col-md-10 prname">
					<h4 class="name">{{ $employee->$view_col }}</h4>
					<div class="label2">University User</div>
					<!--
					<div class="row stats">
						<div class="col-md-4"><i class="fa fa-facebook"></i> 234</div>
						<div class="col-md-4"><i class="fa fa-twitter"></i> 12</div>
						<div class="col-md-4"><i class="fa fa-instagram"></i> 89</div>
					</div>
					-->
					<!-- <p class="desc">{{ substr($employee->about, 0, 33) }}@if(strlen($employee->about) > 33)...@endif</p> -->
				</div>
			</div>
		</div>
		<div class="col-md-2 prdts">
			<div class="dats1 prof-ic"><i class="icon-envelope"></i> {{ $employee->email }}</div>
			<div class="dats1 prof-ic"><i class="icon-location-pin"></i> {{ $employee->city }}</div>
		</div>
		<div class="col-md-2">
			<!--
			<div class="teamview">
				<a class="face" data-toggle="tooltip" data-placement="top" title="John Doe"><img src="{{ asset('la-assets/img/user1-128x128.jpg') }}" alt=""><i class="status-online"></i></a>
				<a class="face" data-toggle="tooltip" data-placement="top" title="John Doe"><img src="{{ asset('la-assets/img/user2-160x160.jpg') }}" alt=""></a>
				<a class="face" data-toggle="tooltip" data-placement="top" title="John Doe"><img src="{{ asset('la-assets/img/user3-128x128.jpg') }}" alt=""></a>
				<a class="face" data-toggle="tooltip" data-placement="top" title="John Doe"><img src="{{ asset('la-assets/img/user4-128x128.jpg') }}" alt=""><i class="status-online"></i></a>
				<a class="face" data-toggle="tooltip" data-placement="top" title="John Doe"><img src="{{ asset('la-assets/img/user5-128x128.jpg') }}" alt=""></a>
				<a class="face" data-toggle="tooltip" data-placement="top" title="John Doe"><img src="{{ asset('la-assets/img/user6-128x128.jpg') }}" alt=""></a>
				<a class="face" data-toggle="tooltip" data-placement="top" title="John Doe"><img src="{{ asset('la-assets/img/user7-128x128.jpg') }}" alt=""></a>
				<a class="face" data-toggle="tooltip" data-placement="top" title="John Doe"><img src="{{ asset('la-assets/img/user8-128x128.jpg') }}" alt=""></a>
				<a class="face" data-toggle="tooltip" data-placement="top" title="John Doe"><img src="{{ asset('la-assets/img/user5-128x128.jpg') }}" alt=""></a>
				<a class="face" data-toggle="tooltip" data-placement="top" title="John Doe"><img src="{{ asset('la-assets/img/user6-128x128.jpg') }}" alt=""><i class="status-online"></i></a>
				<a class="face" data-toggle="tooltip" data-placement="top" title="John Doe"><img src="{{ asset('la-assets/img/user7-128x128.jpg') }}" alt=""></a>
			</div>
		
			<div class="dats1 pb">
				<div class="clearfix">
					<span class="pull-left">Task #1</span>
					<small class="pull-right">20%</small>
				</div>
				<div class="progress progress-xs active">
					<div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
						<span class="sr-only">20% Complete</span>
					</div>
				</div>
			</div>
			<div class="dats1 pb">
				<div class="clearfix">
					<span class="pull-left">Task #2</span>
					<small class="pull-right">90%</small>
				</div>
				<div class="progress progress-xs active">
					<div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 90%" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
						<span class="sr-only">90% Complete</span>
					</div>
				</div>
			</div>
			<div class="dats1 pb">
				<div class="clearfix">
					<span class="pull-left">Task #3</span>
					<small class="pull-right">60%</small>
				</div>
				<div class="progress progress-xs active">
					<div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 60%" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
						<span class="sr-only">60% Complete</span>
					</div>
				</div>
			</div>
			-->
		</div>
		<div class="col-md-1 actions">
			@la_access("Employees", "edit")
				<a href="{{ url(config('laraadmin.adminRoute') . '/university_users/'.$employee->id.'/edit') }}" class="btn btn-xs btn-edit btn-default"><i class="fa fa-pencil"></i></a><br>
			@endla_access
			
			@la_access("Employees", "delete")
				{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.university_users.destroy', $employee->id], 'method' => 'delete', 'style'=>'display:inline','onSubmit'=>"return confirm('Are you sure you wish to delete?');"]) }}
					<button class="btn btn-default btn-delete btn-xs" type="submit"><i class="fa fa-times"></i></button>
				{{ Form::close() }}
			@endla_access
		</div>

		<div class="row">
			<div class="col-md-12 prdts-prof vhbottom">
				<div class="dats1"><i class="fa fa-envelope-o mr5"></i> {{ $employee->email }} <span>|</span><i class="fa fa-map-marker mr5"></i> {{ $employee->city }} </div>
			</div>
		</div> 
		
	</div>

	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		<li class=""><a href="{{ url(config('laraadmin.adminRoute') . '/university_users/'.$employee->id) }}" data-toggle="tooltip" data-placement="right" title="Back to view"><i class="fa fa-chevron-left"></i></a></li>
		<li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> General Info</a></li>
		<!-- <li class=""><a role="tab" data-toggle="tab" href="#tab-timeline" data-target="#tab-timeline"><i class="fa fa-clock-o"></i> Timeline</a></li> -->
		@if(Entrust::hasRole("SUPER_ADMIN") || Entrust::hasRole("MANAGER"))
			<li class=""><a role="tab" data-toggle="tab" href="#tab-account-settings" data-target="#tab-account-settings"><i class="fa fa-key"></i> Account settings</a></li>
		@endif
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>General Info</h4>
					</div>
					<div class="panel-body">
						@la_display($module, 'name')
						@la_display($module, 'mobile')
						@la_display($module, 'mobile2')
						@la_display($module, 'email')
						@la_display($module, 'city')
						@la_display($module, 'address')
						@la_display($module, 'about')
						@la_display($module, 'date_birth')
					</div>
				</div>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane fade in p20 bg-white" id="tab-timeline">
			@if(Session::has('success_mail_delete'))
				<p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success_mail_delete') }}</p>
			@endif
			<?php
			if(count($mailDetails) == 0) {
			?>
				<div class="text-center p30"><i class="fa fa-list-alt" style="font-size: 100px;"></i> <br> No mails to show</div>
			<?php
			} else {
			?>
				<ul class="timeline timeline-inverse" id="mail-lists">
					<?php 
					$currentDate = '';
					foreach($mailDetails as $key => $result) {	
						$dates = explode(' ',$result->send_date);
						$old_timestamp = strtotime($dates[1]);
						$new_time = date('h:i a', $old_timestamp);
						
						if($dates[0] != $currentDate) {
							$currentDate = $dates[0];
							$old_date_timestamp = strtotime($dates[0]);
							$new_date = date('d M Y', $old_date_timestamp);
						?>
							<li class="time-label">
								<span class="bg-red"><?php echo $new_date; ?></span>
							</li>
						<?php
						}
						?>
						<li>
							<i class="fa fa-envelope bg-blue"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> <?php echo $new_time; ?></span>
								<h3 class="timeline-header header"><a href="javascript:void(0);"><?php echo $result->name; ?></a> has sended you an email</h3>
								<h3 class="timeline-header subject">Subject: &nbsp;<a href="javascript:void(0);"><?php echo $result->subject; ?></a></h3>
								<div class="timeline-body">
									<?php
									$mailContent = strip_tags($result->mail_content);
									if(strlen($mailContent) > 150)
										$mailContent = substring(0,150,$mailContent).'...';
									
									echo $mailContent;
									?>
								</div>
								<div class="timeline-footer">
									<a href="javascript:void(0);" data-time="<?php echo $new_time; ?>" data-user="<?php echo $result->name; ?>" data-date="<?php echo $new_date; ?>" data-subject="<?php echo $result->subject; ?>" data-mailcontent="<?php echo $result->mail_content; ?>" class="btn btn-primary btn-xs mail-read-more">Read more</a>
									<a href="javascript:void(0);" data-id="<?php echo $result->id; ?>" class="btn btn-danger btn-xs mail-delete">Delete</a>
									<form action="{{ url(config('laraadmin.adminRoute') . '/deleteUniversityuserMail/'.$result->id.'/'.$employee->id) }}" id="delete-mail-form-<?php echo $result->id; ?>" method="post">
										{{ csrf_field() }}
									</form>
								</div>
							</div>
						</li>
					<?php 
					}
					?>
					<li><i class="fa fa-clock-o bg-gray"></i></li>
				</ul>
				<ul class="timeline timeline-inverse" id="mail-details">
					<li class="time-label">
						<span class="bg-red"><?php echo $new_date; ?></span>
						<a href="javascript:void(0);" id="mail-back" class="btn btn-primary" style="float: right;margin-right: 15px;">Back</a>
					</li>
					<li>
						<i class="fa fa-envelope bg-blue"></i>
						<div class="timeline-item">
							<span class="time"><i class="fa fa-clock-o"></i><label></label></span>
							<h3 class="timeline-header header"><a href="javascript:void(0);"></a> has sended you an email</h3>
							<h3 class="timeline-header subject">Subject: &nbsp;<a href="javascript:void(0);"></a></h3>
							<div class="timeline-body"></div>
							<div class="timeline-footer">							
							</div>
						</div>
					</li>
					<li><i class="fa fa-clock-o bg-gray"></i></li>
				</ul>
			<?php
			}
			?>
		</div>
		
		@if(Entrust::hasRole("SUPER_ADMIN") || Entrust::hasRole("MANAGER"))
		<div role="tabpanel" class="tab-pane fade" id="tab-account-settings">
			<div class="tab-content">
				<form action="{{ url(config('laraadmin.adminRoute') . '/changeUniversityPass/'.$employee->id) }}" id="password-reset-form" class="general-form dashed-row white" method="post" accept-charset="utf-8">
					{{ csrf_field() }}
					<div class="panel">
						<div class="panel-default panel-heading">
							<h4>Account settings</h4>
						</div>
						<div class="panel-body">
							@if (count($errors) > 0)
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
							@if(Session::has('success_message'))
								<p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success_message') }}</p>
							@endif
							<div class="form-group">
								<label for="password" class=" col-md-2">Password</label>
								<div class=" col-md-10">
									<input type="password" name="password" value="" id="password" class="form-control" placeholder="Password" autocomplete="off" required="required" data-rule-minlength="6" data-msg-minlength="Please enter at least 6 characters.">
								</div>
							</div>
							<div class="form-group">
								<label for="password_confirmation" class=" col-md-2">Retype password</label>
								<div class=" col-md-10">
									<input type="password" name="password_confirmation" value="" id="password_confirmation" class="form-control" placeholder="Retype password" autocomplete="off" required="required" data-rule-equalto="#password" data-msg-equalto="Please enter the same value again.">
								</div>
							</div>
						</div>
						<div class="panel-footer">
							<button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> Change Password</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		@endif

	</div>
	</div>
	</div>
</div>
@endsection

@push('scripts')
<script>
$(function () {
	$("#mail-details").hide();
	$('body').off('click', '.mail-read-more');
	$('body').on('click', '.mail-read-more', function() {
		$("#mail-details").show(); $("#mail-lists").hide();
		var mailDate = $(this).attr('data-date');
		var mailContent = $(this).attr('data-mailcontent');
		var mailSubject = $(this).attr('data-subject');
		var mailFrom = $(this).attr('data-user');
		var mailTime = $(this).attr('data-time');
		
		$("#mail-details .time-label bg-red").text(mailDate);
		$("#mail-details .timeline-body").html(mailContent);
		$("#mail-details .subject a").text(mailSubject);
		$("#mail-details .header a").text(mailFrom);
		$("#mail-details .time label").text(mailTime);
	});
	$('body').off('click', '#mail-back');
	$('body').on('click', '#mail-back', function() {
		$("#mail-details").hide(); $("#mail-lists").show();
	});
	$('body').off('click', '.mail-delete');
	$('body').on('click', '.mail-delete', function() {
		var mailId = $(this).attr('data-id');
		if(confirm('are you sure?')) {
			$('#delete-mail-form-'+mailId).submit();
		} else { return false; }
	});
});
</script>
@endpush


