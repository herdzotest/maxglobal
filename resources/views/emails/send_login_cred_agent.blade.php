Dear {{ $name }},<br><br>

You have been registered on {{ url('/') }}.<br><br>

Your login credentials for the same are as below:<br><br>

Username: {{ $user }}<br>
password: {{ $password }}<br><br>

You can login on to <a href="{{ url('/login') }}">{{ str_replace("http://", "", url('/login')) }}</a> after your account is verified.<br><br>

Best Regards,<br>
Zodocs
