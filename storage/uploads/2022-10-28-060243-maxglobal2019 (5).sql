-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 19, 2022 at 07:16 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `maxglobal2019`
--

-- --------------------------------------------------------

--
-- Table structure for table `applynow`
--

CREATE TABLE `applynow` (
  `apply_id` int(10) NOT NULL,
  `email` varchar(250) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `country` int(10) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `dob` varchar(30) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `intake_year` varchar(10) DEFAULT NULL,
  `intake_month` varchar(10) DEFAULT NULL,
  `leap_year` varchar(10) DEFAULT NULL,
  `marital_status` varchar(10) DEFAULT NULL,
  `visa_reject` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` enum('Active','Inactive','Deleted') NOT NULL DEFAULT 'Active',
  `name` varchar(100) NOT NULL,
  `education_level` varchar(30) DEFAULT NULL,
  `language_test` varchar(50) DEFAULT NULL,
  `university` int(10) NOT NULL,
  `course` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `applynow`
--

INSERT INTO `applynow` (`apply_id`, `email`, `mobile`, `country`, `state`, `dob`, `gender`, `intake_year`, `intake_month`, `leap_year`, `marital_status`, `visa_reject`, `created_at`, `updated_at`, `status`, `name`, `education_level`, `language_test`, `university`, `course`) VALUES
(3, 'arjun345@yopmail.com', '9656774188', 1, 'Kerala', '1990-06-21', 'Male', '2023', 'Apr', 'No', 'Unmarried', 'No', '2022-10-08 01:13:00', '2022-10-08 01:13:00', 'Active', 'Arjun', 'Postgraduate Degree', 'TOEFL', 6, 9),
(4, 'sanjitha123@yopmail.com', '7890678956', 2, 'Ontario', '2022-10-08', 'Female', '2024', 'Jul', 'No', 'Married', 'Yes', '2022-10-08 01:23:01', '2022-10-08 01:23:01', 'Active', 'Sanjitha', 'Postgraduate Diploma', 'PTE', 6, 9),
(5, 'karthika345@yopmail.com', '9656774188', 3, 'Kerala', '2003-02-05', 'Male', '2022', 'Jan', 'Yes', 'Unmarried', 'Yes', '2022-10-08 01:24:47', '2022-10-08 01:24:47', 'Active', 'Karthika', 'Plus Two', 'IELTS', 6, 9),
(6, 'siya345@yopmail.com', '9656774188', 6, 'Kerala', '2022-10-08', 'Male', '2022', 'Jan', 'Yes', 'Unmarried', 'Yes', '2022-10-08 01:28:40', '2022-10-08 01:28:40', 'Active', 'Siya', 'Plus Two', 'IELTS', 6, 2),
(7, 'revathy345@yopmail.com', '9656774188', 1, 'Maharashtra', '2022-10-08', 'Male', '2022', 'Jan', 'Yes', 'Unmarried', 'Yes', '2022-10-08 01:30:42', '2022-10-08 01:30:42', 'Active', 'Revathy', 'Plus Two', 'IELTS', 112, 15),
(8, 'amitha345@yopmail.com', '9656774188', 6, 'Karnataka', '1995-02-23', 'Female', '', 'Mar', '', '', '', '2022-10-08 07:09:10', '2022-10-08 07:09:10', 'Active', 'Amitha', 'Undergraduate Degree', '', 6, 9),
(9, 'sanjeev345@yopmail.com', '9656774188', 2, 'Kerala', '1993-10-20', 'Male', '2023', 'Jul', 'No', 'Unmarried', 'No', '2022-10-12 01:45:05', '2022-10-12 01:45:05', 'Active', 'Sanjeev', 'Postgraduate Degree', 'PTE', 6, 9),
(10, 'avinash345@yopmail.com', '9656774188', 6, 'Kerala', '1993-08-10', 'Male', '2023', 'May', 'Yes', 'Married', 'Yes', '2022-10-12 01:48:28', '2022-10-12 01:48:28', 'Active', 'Avinash', 'Undergraduate Diploma', 'TOEFL', 6, 9),
(11, 'praveen345@yopmail.com', '9656774188', 9, 'Kerala', '1984-11-22', 'Male', '2022', 'Nov', 'No', 'Married', 'Yes', '2022-10-12 01:50:07', '2022-10-12 01:50:07', 'Active', 'Praveen', 'Postgraduate Degree', 'Not Planning To Take Any', 6, 9);

-- --------------------------------------------------------

--
-- Table structure for table `backups`
--

CREATE TABLE `backups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `file_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `backup_size` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `campus`
--

CREATE TABLE `campus` (
  `id` int(11) NOT NULL,
  `campus_name` varchar(250) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `campus`
--

INSERT INTO `campus` (`id`, `campus_name`, `status`) VALUES
(1, 'Campus 100', 1),
(3, 'Campus 200', 1),
(4, 'Campus 300', 1),
(5, 'Aldershot', 1),
(6, 'Altrincham', 1),
(7, 'Ashford', 1),
(8, 'Atherton', 1),
(9, 'Aylesbury', 1),
(10, 'Bamber Bridge', 1),
(11, 'Bangor', 1),
(12, 'Barnsley', 1),
(13, 'Barry', 1),
(14, 'Basildon', 1),
(15, 'Basingstoke', 1),
(16, 'Bath', 1),
(17, 'Batley', 1),
(18, 'Bebington', 1),
(19, 'Bedford', 1),
(20, 'Beeston', 1),
(21, 'Belfast', 1),
(22, 'Birkenhead', 1),
(23, 'Birmingham', 1),
(24, 'Blackburn', 1),
(25, 'Blackpool', 1),
(26, 'Bloxwich', 1),
(27, 'Bognor Regis', 1),
(28, 'Bolton', 1),
(29, 'Bootle', 1),
(30, 'Bournemouth', 1),
(31, 'Bracknell', 1),
(32, 'Bradford', 1),
(33, 'Brentwood', 1),
(34, 'Brighton and Hove', 1),
(35, 'Bristol', 1),
(36, 'Burnley', 1),
(37, 'Burton upon Trent', 1),
(38, 'Bury', 1),
(39, 'Cambridge (/ Milton)', 1),
(40, 'Cannock', 1),
(41, 'Canterbury', 1),
(42, 'Cardiff', 1),
(43, 'Carlisle', 1),
(44, 'Carlton', 1),
(45, 'Chatham', 1),
(46, 'Chelmsford', 1),
(47, 'Cheltenham', 1),
(48, 'Chester', 1),
(49, 'Chesterfield', 1),
(50, 'Christchurch', 1),
(51, 'Clacton-on-Sea', 1),
(52, 'Colchester', 1),
(53, 'Corby', 1),
(54, 'Coventry', 1),
(55, 'Craigavon (incl. Lurgan, Portadown)', 1),
(56, 'Crawley', 1),
(57, 'Crewe', 1),
(58, 'Crosby', 1),
(59, 'Cumbernauld', 1),
(60, 'Darlington', 1),
(61, 'Derby', 1),
(62, 'Derry (Londonderry)', 1),
(63, 'Dewsbury', 1),
(64, 'Doncaster', 1),
(65, 'Dudley', 1),
(66, 'Dundee', 1),
(67, 'Dunfermline', 1),
(68, 'Durham', 1),
(69, 'Eastbourne', 1),
(70, 'East Kilbride', 1),
(71, 'Eastleigh', 1),
(72, 'Edinburgh', 1),
(73, 'Ellesmere Port', 1),
(74, 'Esher', 1),
(75, 'Ewell', 1),
(76, 'Exeter', 1),
(77, 'Farnborough', 1),
(78, 'Filton', 1),
(79, 'Folkestone', 1),
(80, 'Gateshead', 1),
(81, 'Gillingham', 1),
(82, 'Glasgow', 1),
(83, 'Gloucester', 1),
(84, 'Gosport', 1),
(85, 'Gravesend', 1),
(86, 'Grays', 1),
(87, 'Grimsby', 1),
(88, 'Guildford', 1),
(89, 'Halesowen', 1),
(90, 'Halifax', 1),
(91, 'Hamilton', 1),
(92, 'Harlow', 1),
(93, 'Harrogate', 1),
(94, 'Hartlepool', 1),
(95, 'Hastings', 1),
(96, 'Hemel Hempstead', 1),
(97, 'Hereford', 1),
(98, 'High Wycombe', 1),
(99, 'Horsham', 1),
(100, 'Huddersfield', 1),
(101, 'Ipswich', 1),
(102, 'Keighley', 1),
(103, 'Kettering', 1),
(104, 'Kidderminster', 1),
(105, 'Kingston upon Hull (Hull)', 1),
(106, 'Kingswinford', 1),
(107, 'Kirkcaldy', 1),
(108, 'Lancaster', 1),
(109, 'Leeds', 1),
(110, 'Leicester', 1),
(111, 'Lincoln', 1),
(112, 'Littlehampton', 1),
(113, 'Liverpool', 1),
(114, 'Livingston', 1),
(115, 'London', 1),
(116, 'Loughborough', 1),
(117, 'Lowestoft', 1),
(118, 'Luton', 1),
(119, 'Macclesfield', 1),
(120, 'Maidenhead', 1),
(121, 'Maidstone', 1),
(122, 'Manchester', 1),
(123, 'Mansfield', 1),
(124, 'Margate', 1),
(125, 'Middlesbrough', 1),
(126, 'Milton Keynes', 1),
(127, 'Neath', 1),
(128, 'Newcastle (Newcastle upon Tyne)', 1),
(129, 'Newcastle-under-Lyme', 1),
(130, 'Newport', 1),
(131, 'Newtownabbey', 1),
(132, 'Northampton', 1),
(133, 'Norwich', 1),
(134, 'Nottingham', 1),
(135, 'Nuneaton', 1),
(136, 'Oldham', 1),
(137, 'Oxford', 1),
(138, 'Paignton', 1),
(139, 'Paisley', 1),
(140, 'Peterborough', 1),
(141, 'Plymouth', 1),
(142, 'Poole', 1),
(143, 'Portsmouth', 1),
(144, 'Preston', 1),
(145, 'Rayleigh', 1),
(146, 'Reading', 1),
(147, 'Redditch', 1),
(148, 'Rochdale', 1),
(149, 'Rochester', 1),
(150, 'Rotherham', 1),
(151, 'Royal Leamington Spa', 1),
(152, 'Royal Tunbridge Wells', 1),
(153, 'Rugby', 1),
(154, 'Runcorn', 1),
(155, 'Sale', 1),
(156, 'Salford', 1),
(157, 'Scarborough', 1),
(158, 'Scunthorpe', 1),
(159, 'Sheffield', 1),
(160, 'Shoreham-by-Sea', 1),
(161, 'Shrewsbury', 1),
(162, 'Sittingbourne', 1),
(163, 'Slough', 1),
(164, 'Smethwick', 1),
(165, 'Solihull', 1),
(166, 'Southampton', 1),
(167, 'Southend-on-Sea', 1),
(168, 'Southport', 1),
(169, 'South Shields', 1),
(170, 'Stafford', 1),
(171, 'St Albans', 1),
(172, 'Stevenage', 1),
(173, 'St Helens', 1),
(174, 'Stockport', 1),
(175, 'Stockton-on-Tees', 1),
(176, 'Stoke-on-Trent', 1),
(177, 'Stourbridge', 1),
(178, 'Sunderland', 1),
(179, 'Sutton Coldfield', 1),
(180, 'Swansea', 1),
(181, 'Swindon', 1),
(182, 'Tamworth', 1),
(183, 'Taunton', 1),
(184, 'Telford', 1),
(185, 'Torquay', 1),
(186, 'Tynemouth', 1),
(187, 'Wakefield', 1),
(188, 'Wallasey', 1),
(189, 'Walsall', 1),
(190, 'Walton-on-Thames', 1),
(191, 'Warrington', 1),
(192, 'Washington', 1),
(193, 'Watford', 1),
(194, 'Wellingborough', 1),
(195, 'Welwyn Garden City', 1),
(196, 'West Bromwich', 1),
(197, 'Weston-super-Mare', 1),
(198, 'Weymouth', 1),
(199, 'Widnes', 1),
(200, 'Wigan', 1),
(201, 'Willenhall', 1),
(202, 'Woking', 1),
(203, 'Wolverhampton', 1),
(204, 'Worcester', 1),
(205, 'Worthing', 1),
(206, 'Wrexham', 1),
(207, 'York', 1);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `country` varchar(200) NOT NULL,
  `short_name` varchar(50) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country`, `short_name`, `status`) VALUES
(1, 'India', 'IN', 1),
(2, 'Canada', 'CN', 1),
(3, 'Australia', 'AUS', 1),
(4, 'Ireland', 'IRD', 1),
(5, 'Singapore', 'SNG', 1),
(6, 'Germany', 'GMY', 1),
(7, 'United Kingdom', 'UK', 1),
(9, 'New Zealand', 'NZ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_courses_applied`
--

CREATE TABLE `data_courses_applied` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `course_country` varchar(100) DEFAULT NULL,
  `level_study` varchar(15) DEFAULT NULL,
  `course_university` varchar(100) DEFAULT NULL,
  `course_title` varchar(50) DEFAULT NULL,
  `mode_study` varchar(50) DEFAULT NULL,
  `campus_stage` varchar(100) DEFAULT NULL,
  `stage_name` varchar(25) DEFAULT NULL,
  `entry_date` varchar(15) DEFAULT NULL,
  `json_data` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_courses_applied`
--

INSERT INTO `data_courses_applied` (`id`, `user_id`, `course_country`, `level_study`, `course_university`, `course_title`, `mode_study`, `campus_stage`, `stage_name`, `entry_date`, `json_data`) VALUES
(1, 91, 'India', 'Level-5', 'University of Bedfordshire, Luton and Bedford', 'Test', 'Part-time', 'Campus1', NULL, 'Jun 25, 2020', '[{\"course_country\":\"United-Kingdom\",\"level_study\":\"Level-6\\/Bachelors\\/UG\",\"course_university\":\"Arden University, private, distance learning & blended learning, London, Birmingham, Manchester, Berlin\",\"course_title\":\"Masters of \",\"mode_study\":\"Distance Learning\",\"campus_stage\":\"Campus2\",\"entry_date\":\"Jun 27, 2020\"},{\"course_country\":\"United-Kingdom\",\"level_study\":\"Level-4\",\"course_university\":\"Anglia Ruskin University, Cambridge\",\"course_title\":\"Master of Science\",\"mode_study\":\"Part-time\",\"campus_stage\":\"Campus 3\",\"entry_date\":\"Jun 17, 2020\"}]'),
(2, 92, 'Argentina', 'Level-1', 'Abertay University', 'Course1', 'Full time', 'Campus22', NULL, 'Jun 26, 2020', '[{\"course_country\":\"India\",\"level_study\":\"Level-5\",\"course_university\":\"Anglia Ruskin University, Cambridge\",\"course_title\":\"Course3\",\"mode_study\":\"Distance Learning\",\"campus_stage\":\"Campus33\",\"entry_date\":\"Jun 26, 2020\"},{\"course_country\":\"United-Kingdom\",\"level_study\":\"Level-6\\/Bachelors\\/UG\",\"course_university\":\"Anglia Ruskin University, Cambridge\",\"course_title\":\"Course44\",\"mode_study\":\"Full time\",\"campus_stage\":\"Capmus44\",\"entry_date\":\"Jun 27, 2020\"}]'),
(3, 93, 'Algeria', 'Level-2', 'Abertay University', 'Course1', 'Full time', 'Campus2', NULL, 'Jun 27, 2020', '[{\"course_country\":\"Australia\",\"level_study\":\"Level-4\",\"course_university\":\"Anglia Ruskin University, Cambridge\",\"course_title\":\"Course2\",\"mode_study\":\"Part-time\",\"campus_stage\":\"Campus22\",\"entry_date\":\"Jun 19, 2020\"}]'),
(4, 94, 'Andorra', 'Level-4', 'Aberystwyth University', 'ttgtrrtt', 'Full time', 'fdrrr', NULL, '', NULL),
(5, 95, 'Algeria', 'Level-2', 'Anglia Ruskin University, Cambridge', 'aaa', 'Full time', '', NULL, 'Jun 25, 2020', '[{\"course_country\":\"India\",\"level_study\":\"Level-7\\/Masters Degree\\/PG\",\"course_university\":\"University of Bath\",\"course_title\":\"\",\"mode_study\":\"\",\"campus_stage\":\"\",\"entry_date\":\"\"}]'),
(6, 96, 'India', 'Level-1', 'Abertay University', 'Course', 'Full time', '', 'Year-2', 'Jul 22, 2020', '[{\"course_country\":\"Andorra\",\"level_study\":\"Level-4\",\"course_university\":\"Anglia Ruskin University, Cambridge\",\"course_title\":\"Cours2\",\"mode_study\":\"Distance Learning\",\"campus_stage\":\"Campus\",\"stage_name\":\"Year-2\",\"entry_date\":\"Jul 22, 2020\"}]'),
(7, 105, 'Angola', 'Level-5', 'Anglia Ruskin University, Cambridge', 'cc', 'Full time', 'cc', 'Year-1', 'Aug 26, 2020', NULL),
(8, 113, 'India', 'Level-3', 'St Mary\'s University', 'MCA', 'Full time', 'Campus', 'Year-1', 'Oct 03, 2020', NULL),
(9, 134, '', '', '', '', '', '', '', '', NULL),
(10, 141, '', '', '', '', '', '', '', '', NULL),
(11, 158, 'Afghanistan', 'Level-1', 'Birmingham City University', 'Sample title', 'Full time', '1', 'Year-1', 'Mar 11, 2021', '[{\"course_country\":\"Australia\",\"level_study\":\"Level-1\",\"course_university\":\"University of Aberdeen\",\"course_title\":\"qwe123\",\"mode_study\":\"Full time\",\"campus_stage\":\"\",\"stage_name\":\"Year-1\",\"entry_date\":\"Mar 23, 2021\"}]'),
(12, 159, 'Algeria', 'Level-3', 'BPP University, private', 'sdasdasd', 'Distance Learning', '1', 'Year-1', 'Mar 22, 2021', '[{\"course_country\":\"Australia\",\"level_study\":\"Level-7\\/Masters Degree\\/PG\",\"course_university\":\"Aston University, Birmingham\",\"course_title\":\"test\",\"mode_study\":\"Full time\",\"campus_stage\":\"\",\"stage_name\":\"Year-1\",\"entry_date\":\"Mar 23, 2021\"}]'),
(13, 165, 'United Kingdom (UK)', 'Level-7/Masters', 'Cardiff Metropolitan University ', '', 'Full time', '', 'Year-2', 'Mar 15, 2021', '{\"11\":{\"course_country\":\"\",\"level_study\":\"\",\"course_university\":\"\",\"course_title\":\"\",\"mode_study\":\"\",\"campus_stage\":\"\",\"stage_name\":\"\",\"entry_date\":\"\"}}'),
(14, 170, '', '', '', '', '', '', '', '', '{\"2\":{\"course_country\":\"Armenia\",\"level_study\":\"\",\"course_university\":\"\",\"course_title\":\"\",\"mode_study\":\"\",\"campus_stage\":\"\",\"stage_name\":\"\",\"entry_date\":\"\"}}'),
(15, 174, 'India', 'Level-2', 'Aberystwyth University', 'MSC Geology', 'Part-time', 'Campus2', 'Year-2', 'May 19, 2022', '{\"11\":{\"course_country\":\"Greece\",\"level_study\":\"Level-2\",\"course_university\":\"Aberystwyth University\",\"course_title\":\"MSC Computer\",\"mode_study\":\"Full time\",\"campus_stage\":\"Campus3\",\"stage_name\":\"Year-1\",\"entry_date\":\"Apr 11, 2024\"}}'),
(16, 186, 'Costa Rica', '', '', '', '', '', '', '', NULL),
(17, 196, 'Albania', '', '', '', '', '', '', '', '{\"2\":{\"course_country\":\"Dominican Republic\",\"level_study\":\"\",\"course_university\":\"\",\"course_title\":\"\",\"mode_study\":\"\",\"campus_stage\":\"\",\"stage_name\":\"\",\"entry_date\":\"\"}}'),
(18, 222, 'Germany', 'Bachelors', 'Birmingham City University', 'MCA', 'Part time', 'Aldershot', 'Year-1', 'Jul 2023', '[{\"course_country\":\"Canada\",\"level_study\":\"Masters\",\"course_university\":\"(KAPLAN) University of Nottingham International College\",\"course_title\":\"MBA\",\"mode_study\":\"Distance Learning\",\"campus_stage\":\"Basingstoke\",\"stage_name\":\"Year-2\",\"entry_date\":\"Oct 2023\"}]'),
(19, 206, 'Australia', 'Diploma', '(INTO) University of Manchester', 'MBA', 'Full time', 'Campus 100', 'Year-1', 'Oct 2023', '{\"1\":{\"course_country\":\"Singapore\",\"level_study\":\"\",\"course_university\":\"\",\"course_title\":\"\",\"mode_study\":\"\",\"campus_stage\":\"\",\"stage_name\":\"\",\"entry_date\":\"\"}}'),
(20, 232, 'Germany', NULL, NULL, 'MCA', NULL, NULL, NULL, NULL, NULL),
(21, 251, '', '', '', '', '', '', '', '', ''),
(22, 261, 'Germany', NULL, NULL, 'MCA', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `data_courses_applied_new`
--

CREATE TABLE `data_courses_applied_new` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `course_country_new` varchar(100) NOT NULL,
  `level_study_new` varchar(15) NOT NULL,
  `course_university_new` varchar(100) NOT NULL,
  `course_title_new` varchar(50) NOT NULL,
  `mode_study_new` varchar(50) NOT NULL,
  `campus_stage_new` varchar(100) NOT NULL,
  `stage_name_new` varchar(25) NOT NULL,
  `entry_date_new` varchar(15) NOT NULL,
  `json_data` text NOT NULL,
  `status_new` varchar(100) NOT NULL,
  `status_note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_courses_applied_new`
--

INSERT INTO `data_courses_applied_new` (`id`, `user_id`, `course_country_new`, `level_study_new`, `course_university_new`, `course_title_new`, `mode_study_new`, `campus_stage_new`, `stage_name_new`, `entry_date_new`, `json_data`, `status_new`, `status_note`) VALUES
(1, 222, 'India', 'Diploma', '(INTO) University of Manchester', 'MBA', 'Full-time', 'Campus 100', '12', 'Jun 2022', '[{\"course_country_new\":\"Australia\",\"level_study_new\":\"Masters\",\"course_title_new\":\"MBA\",\"mode_study_new\":\"Full-time\",\"campus_stage_new\":\"Campus 300\",\"stage_name_new\":\"2\",\"entry_date_new\":\"Nov 2023\",\"course_university_new\":\"(STUDY GROUP) University of Leeds, Leeds\",\"status_new\":\"Application on hold by Scholab team\",\"status_note\":[\"Note 3\",\"Note 31\",\"\"]},{\"course_country_new\":\"Ireland\",\"level_study_new\":\"\",\"course_title_new\":\"\",\"mode_study_new\":\"\",\"campus_stage_new\":\"\",\"stage_name_new\":\"\",\"entry_date_new\":\"\",\"course_university_new\":\"\",\"status_new\":\"\",\"status_note\":[\"\"]}]', 'Received Application at Scholab', '[\"Note 1\",\"Note 11\"]'),
(2, 232, '', '', '', '', '', '', '', '', '[{\"course_country_new\":\"\",\"level_study_new\":\"\",\"course_title_new\":\"\",\"mode_study_new\":\"\",\"campus_stage_new\":\"\",\"stage_name_new\":\"\",\"entry_date_new\":\"\",\"course_university_new\":\"\",\"status_new\":\"\",\"status_note\":[\"\"]}]', '', '[]');

-- --------------------------------------------------------

--
-- Table structure for table `data_dependant_det`
--

CREATE TABLE `data_dependant_det` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `childrens` varchar(6) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `birthdate` varchar(15) DEFAULT NULL,
  `relation` varchar(50) DEFAULT NULL,
  `current_live` varchar(6) DEFAULT NULL,
  `travel_to_uk` varchar(6) DEFAULT NULL,
  `json_data` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_dependant_det`
--

INSERT INTO `data_dependant_det` (`id`, `user_id`, `childrens`, `name`, `birthdate`, `relation`, `current_live`, `travel_to_uk`, `json_data`) VALUES
(1, 93, 'Yes', 'Joseph Kottackal', 'Jun 19, 20', 'Son', 'Yes', 'Yes', '[{\"name\":\"Anu Kottackal\",\"birthdate\":\"Jun 26, 2020\",\"relation\":\"Daughter\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"}]'),
(2, 96, 'Yes', 'Sijo2', 'Jul 23, 20', 'Son', 'Yes', 'Yes', '[{\"name\":\"Sj3\",\"birthdate\":\"Jul 25, 2020\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"1\"}]'),
(3, 113, 'No', '', '', 'Son', 'Yes', 'Yes', NULL),
(4, 134, 'Yes', '', '', 'Son', 'Yes', 'Yes', NULL),
(5, 141, 'Yes', '', '', 'Son', 'Yes', 'Yes', NULL),
(6, 174, 'Yes', 'Arya', 'Jan 16, 20', 'Daughter', 'Yes', 'Yes', '{\"-1\":{\"name\":\"Arun\",\"birthdate\":\"Jan 16, 2016\",\"relation\":\"Son\",\"current_live\":\"No\",\"travel_to_uk\":\"No\"}}'),
(7, 196, 'No', 'Arya', 'Mar 11, 2004', 'Daughter', 'Yes', 'No', '[{\"name\":\"Ajith\",\"birthdate\":\"Sep 24, 2010\",\"relation\":\"Son\",\"current_live\":\"No\",\"travel_to_uk\":\"No\"}]'),
(8, 222, 'Yes', 'Aneesh', 'May 10, 2007', 'Son', 'Yes', 'Yes', '[{\"name\":\"Amritha\",\"birthdate\":\"Mar 07, 2013\",\"relation\":\"Daughter\",\"current_live\":\"No\",\"travel_to_uk\":\"No\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `data_employement_history`
--

CREATE TABLE `data_employement_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `employer_name` varchar(100) DEFAULT NULL,
  `work_nature` varchar(50) DEFAULT NULL,
  `from_date` varchar(15) DEFAULT NULL,
  `to_date` varchar(15) DEFAULT NULL,
  `work_mode` varchar(50) DEFAULT NULL,
  `experience_letter` text DEFAULT NULL,
  `experience_letter_approved` tinyint(1) NOT NULL DEFAULT 0,
  `json_data` text DEFAULT NULL,
  `job_position` varchar(50) NOT NULL,
  `experience` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_employement_history`
--

INSERT INTO `data_employement_history` (`id`, `user_id`, `employer_name`, `work_nature`, `from_date`, `to_date`, `work_mode`, `experience_letter`, `experience_letter_approved`, `json_data`, `job_position`, `experience`) VALUES
(1, 93, 'Muthoot', 'Hsopitality', 'Jun 23, 20', 'Jun 18, 20', 'Full-time', 'http://maxglobal.herdzo.com/files/66zjx9crbr532gf4wyz4/MaxGlobalConsultants_website_Final_doc (1).pd', 0, '[{\"employer_name\":\"Appolo\",\"work_nature\":\"Hospitality\",\"from_date\":\"Jun 25, 2015\",\"to_date\":\"Apr 28, 2016\",\"work_mode\":\"\",\"experience_letter\":\"MaxGlobalConsultants_website_Final_doc (1).pdf\"},{\"employer_name\":\"Apolo\",\"work_nature\":\"Hospitaity\",\"from_date\":\"Apr 21, 2017\",\"to_date\":\"Jun 20, 2018\",\"work_mode\":\"\",\"experience_letter\":\"Nurse_Profile_Fields.docx\"}]', '', ''),
(2, 96, 'Muthoot', 'Work', 'Jul 21, 20', 'Jul 16, 20', 'Full-time', 'http://maxglobal.herdzo.com/files/rgn91uvmsuaw7raprjkh/Document.rtf', 0, NULL, '', ''),
(3, 113, 'IVL', 'Permanent', 'Apr 04, 20', 'Aug 31, 20', 'Full-time', 'http://maxglobal.herdzo.com/files/kdre2aqybezjwqwt2egy/Tulips.jpg?s=130', 0, NULL, '', ''),
(4, 134, '', '', '', '', '', '', 0, NULL, '', ''),
(5, 141, '', '', '', '', '', '', 0, NULL, '', ''),
(6, 158, '', '', '', '', '', NULL, 0, NULL, '', ''),
(7, 174, 'TLS', 'Permanent', 'Aug 20, 2019', 'Aug 25, 2020', 'Full-time', NULL, 0, '[{\"employer_name\":\"SCS\",\"work_nature\":\"Permanent\",\"from_date\":\"Sep 23, 2016\",\"to_date\":\"Sep 08, 2017\",\"work_mode\":\"Part-time\"}]', '', ''),
(8, 196, 'TSC', '', 'Mar 2017', 'Mar 2018', 'Full-time', 'http://localhost/maxglobal/files/mvyhoxwqshgky7yubxvl/Get_Started_With_Smallpdf.pdf', 0, '[{\"employer_name\":\"RSC\",\"job_position\":\"Sr SE\",\"from_date\":\"Apr 2018\",\"to_date\":\"Apr 2019\",\"experience\":\"1\",\"work_mode\":\"Part-time\",\"experience_letter\":\"http:\\/\\/localhost\\/maxglobal\\/files\\/jotm1gwhy8govgpprizk\\/Get_Started_With_Smallpdf.pdf\"},{\"employer_name\":\"NSS\",\"job_position\":\"SE\",\"from_date\":\"May 2019\",\"to_date\":\"May 2021\",\"experience\":\"2\",\"work_mode\":\"Full-time\",\"experience_letter\":\"http:\\/\\/localhost\\/maxglobal\\/files\\/r5jvmqlpdtw5yg3e1zmw\\/A Sample PDF.pdf\"},{\"employer_name\":\"TSS\",\"job_position\":\"Manager\",\"from_date\":\"Jun 2021\",\"to_date\":\"Mar 2022\",\"experience\":\"1\",\"work_mode\":\"Part-time\",\"experience_letter\":\"http:\\/\\/localhost\\/maxglobal\\/files\\/yqcg5fczdm81wirkh97q\\/ev-img01.jpg?s=130\"}]', 'Jr Software Engineer', '1'),
(9, 193, '', '', '', '', '', NULL, 0, '[{\"employer_name\":\"\",\"work_nature\":\"\",\"from_date\":\"\",\"to_date\":\"\",\"work_mode\":\"\",\"experience_letter\":\"http:\\/\\/localhost\\/maxglobal\\/files\\/s2rdvenzss1pgvzftrvv\\/A Sample PDF.pdf\"}]', '', ''),
(10, 222, '', '', '', '', '', 'http://localhost/maxglobal/files/hork4kb6srqwzljrwsic/A Sample PDF.pdf', 0, '[{\"employer_name\":\"\",\"work_nature\":\"\",\"from_date\":\"\",\"to_date\":\"\",\"work_mode\":\"\",\"experience_letter\":\"http:\\/\\/localhost\\/maxglobal\\/files\\/vpbpawh2bmkonhciv8xl\\/A Sample PDF.pdf\"},{\"employer_name\":\"\",\"work_nature\":\"\",\"from_date\":\"\",\"to_date\":\"\",\"work_mode\":\"\",\"experience_letter\":\"http:\\/\\/localhost\\/maxglobal\\/files\\/2z28gm7xpz4tdtsqehal\\/A Sample PDF.pdf\"}]', '', ''),
(11, 214, 'TCS', NULL, 'Sep 2017', 'Sep 2019', 'Full-time', 'http://localhost/maxglobal/files/7fyqol2kodunps8yaj9t/A Sample PDF.pdf', 0, '', 'Jr Software Engineer', '2'),
(12, 232, '', NULL, '', '', '', 'http://localhost/maxglobal/files/3bfku7jo3mmdsd99fudi/A Sample PDF.pdf,http://localhost/maxglobal/files/ntcgtqozh5szlgose3gk/pdf-sample.pdf,http://localhost/maxglobal/files/j9th1doptewyofccdsvm/dummy.pdf', 0, '[{\"employer_name\":\"\",\"job_position\":\"\",\"from_date\":\"\",\"to_date\":\"\",\"experience\":\"\",\"work_mode\":\"\",\"experience_letter\":\"http:\\/\\/localhost\\/maxglobal\\/files\\/tsxkiqbidl9kapeijzcm\\/A Sample PDF.pdf\"}]', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `data_eng_lang_proficiency`
--

CREATE TABLE `data_eng_lang_proficiency` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `eng_lang_check` tinyint(4) NOT NULL DEFAULT 0,
  `yrs_english` varchar(10) DEFAULT NULL,
  `qualifications` varchar(20) DEFAULT NULL,
  `other_qualifications` text DEFAULT NULL,
  `medium_instr_letter` varchar(100) DEFAULT NULL,
  `medium_instr_letter_approved` tinyint(1) NOT NULL DEFAULT 0,
  `listening_score` varchar(10) DEFAULT NULL,
  `reading_score` varchar(10) DEFAULT NULL,
  `writing_score` varchar(10) DEFAULT NULL,
  `speaking_score` varchar(10) DEFAULT NULL,
  `over_all_score` varchar(10) DEFAULT NULL,
  `test_date` varchar(15) DEFAULT NULL,
  `test_date_time` varchar(15) DEFAULT NULL,
  `test_time` varchar(10) DEFAULT NULL,
  `pte` tinyint(1) NOT NULL DEFAULT 0,
  `pte_overall_score` varchar(10) DEFAULT NULL,
  `pte_date_exam` varchar(15) DEFAULT NULL,
  `pte_reading` varchar(10) DEFAULT NULL,
  `pte_listening` varchar(10) DEFAULT NULL,
  `pte_speaking` varchar(10) DEFAULT NULL,
  `pte_writing` varchar(10) DEFAULT NULL,
  `duolingo` tinyint(1) NOT NULL DEFAULT 0,
  `duolingo_overall_score` varchar(10) DEFAULT NULL,
  `duolingo_date_exam` varchar(15) DEFAULT NULL,
  `duolingo_reading` varchar(10) DEFAULT NULL,
  `duolingo_listening` varchar(10) DEFAULT NULL,
  `duolingo_speaking` varchar(10) DEFAULT NULL,
  `duolingo_writing` varchar(10) DEFAULT NULL,
  `toefl` tinyint(1) NOT NULL DEFAULT 0,
  `toefl_overall_score` varchar(10) DEFAULT NULL,
  `toefl_date_exam` varchar(15) DEFAULT NULL,
  `toefl_reading` varchar(10) DEFAULT NULL,
  `toefl_listening` varchar(10) DEFAULT NULL,
  `toefl_speaking` varchar(10) DEFAULT NULL,
  `toefl_writing` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_eng_lang_proficiency`
--

INSERT INTO `data_eng_lang_proficiency` (`id`, `user_id`, `eng_lang_check`, `yrs_english`, `qualifications`, `other_qualifications`, `medium_instr_letter`, `medium_instr_letter_approved`, `listening_score`, `reading_score`, `writing_score`, `speaking_score`, `over_all_score`, `test_date`, `test_date_time`, `test_time`, `pte`, `pte_overall_score`, `pte_date_exam`, `pte_reading`, `pte_listening`, `pte_speaking`, `pte_writing`, `duolingo`, `duolingo_overall_score`, `duolingo_date_exam`, `duolingo_reading`, `duolingo_listening`, `duolingo_speaking`, `duolingo_writing`, `toefl`, `toefl_overall_score`, `toefl_date_exam`, `toefl_reading`, `toefl_listening`, `toefl_speaking`, `toefl_writing`) VALUES
(1, 93, 1, '8', 'IELTS', 'OTHERS', NULL, 0, '10', '9', '8', '7', NULL, 'Apr 30, 20', 'Jun 25, 2020', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 134, 0, '', '', '', NULL, 0, '0', '0', '0', '0', '0', '', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 141, 0, '', '', '', NULL, 0, '0', '0', '0', '0', '0', '', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 158, 0, '12', 'IELTS', '', NULL, 0, '0', '0', '0', '0', '0', '', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 165, 0, '4', 'IELTS', '', NULL, 0, '7', '7', '7', '7', '7', 'Mar 25, 20', 'Mar 23, 2021', '04:43 AM', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 174, 1, '2', 'IELTS', '', NULL, 0, '1', '4', '3', '5', '13', 'Jan 04, 20', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 193, 1, '4', 'OET', '', NULL, 0, '2', '4', '3', '5', '14', 'Jan 06, 2022', 'Jan 19, 2022', '03:15 PM', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 196, 1, '3', 'IELTS', '', NULL, 0, '4', '4', '3', '2', '13', 'Jan 08, 2022', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 222, 0, '5', 'OET', '', NULL, 0, '5', '3', '2', '4', '14', 'Apr 05, 2022', 'May 30, 2022', '04:30 PM', 1, '10', 'Apr 10, 2022', '1', '4', '3', '2', 0, '9', 'Apr 15, 2022', '4', '1', '2', '3', 1, '14', 'Apr 20, 2022', '4', '2', '5', '3');

-- --------------------------------------------------------

--
-- Table structure for table `data_follow_up`
--

CREATE TABLE `data_follow_up` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `follow_date` varchar(15) NOT NULL,
  `follow_time` varchar(10) NOT NULL,
  `representative_note` varchar(100) NOT NULL,
  `json_data` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_follow_up`
--

INSERT INTO `data_follow_up` (`id`, `user_id`, `follow_date`, `follow_time`, `representative_note`, `json_data`) VALUES
(1, 222, 'Jun 05, 2022', '10:30', 'Follow up notes', '[{\"follow_date\":\"Jun 05, 2022\",\"follow_time\":\"04:30 PM\",\"representative_note\":\" Follow up data\"}]'),
(2, 232, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `data_further_details`
--

CREATE TABLE `data_further_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `aboutUs` text DEFAULT NULL,
  `nxt_kin_name` varchar(100) DEFAULT NULL,
  `nxt_kin_country_code` int(6) DEFAULT NULL,
  `nxt_kin_phone` varchar(11) DEFAULT NULL,
  `skype_id` varchar(20) DEFAULT NULL,
  `skpe_screenshot` text DEFAULT NULL,
  `country_permanent` varchar(100) DEFAULT NULL,
  `country_residence` varchar(100) DEFAULT NULL,
  `country_birth` varchar(100) DEFAULT NULL,
  `birth_place` varchar(100) DEFAULT NULL,
  `Nationality` varchar(100) DEFAULT NULL,
  `prev_study_sector` varchar(55) DEFAULT NULL,
  `ucas_application` tinyint(1) NOT NULL DEFAULT 0,
  `ucas_id` varchar(25) DEFAULT NULL,
  `lived_other_address` tinyint(1) NOT NULL DEFAULT 0,
  `street_address` varchar(250) DEFAULT NULL,
  `street_address_line` varchar(250) DEFAULT NULL,
  `city_other_address` varchar(100) DEFAULT NULL,
  `state_other_address` varchar(100) DEFAULT NULL,
  `zip_code_other_address` varchar(50) DEFAULT NULL,
  `country_other_address` varchar(100) DEFAULT NULL,
  `living_addr_start_date` varchar(15) DEFAULT NULL,
  `living_addr_stop_date` varchar(15) DEFAULT NULL,
  `relatiion_status` varchar(10) DEFAULT NULL,
  `spouse_prefix` varchar(7) DEFAULT NULL,
  `spouse_firstname` varchar(50) DEFAULT NULL,
  `spouse_middlename` varchar(50) DEFAULT NULL,
  `spouse_familyname` varchar(50) DEFAULT NULL,
  `spouse_birth_date` varchar(15) DEFAULT NULL,
  `current_live_status` varchar(10) NOT NULL DEFAULT '',
  `spouse_current_addr` varchar(100) DEFAULT NULL,
  `spouse_current_addr_line` varchar(100) DEFAULT NULL,
  `spouse_current_city` varchar(50) DEFAULT NULL,
  `spouse_current_state` varchar(50) DEFAULT NULL,
  `spouse_current_zipcode` varchar(20) DEFAULT NULL,
  `spouse_current_country` varchar(100) DEFAULT NULL,
  `spouse_passport_no` varchar(20) DEFAULT NULL,
  `spouse_passport` varchar(100) DEFAULT NULL,
  `spouse_passport_approved` tinyint(1) NOT NULL DEFAULT 0,
  `spouse_travel_uk` varchar(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_further_details`
--

INSERT INTO `data_further_details` (`id`, `user_id`, `aboutUs`, `nxt_kin_name`, `nxt_kin_country_code`, `nxt_kin_phone`, `skype_id`, `skpe_screenshot`, `country_permanent`, `country_residence`, `country_birth`, `birth_place`, `Nationality`, `prev_study_sector`, `ucas_application`, `ucas_id`, `lived_other_address`, `street_address`, `street_address_line`, `city_other_address`, `state_other_address`, `zip_code_other_address`, `country_other_address`, `living_addr_start_date`, `living_addr_stop_date`, `relatiion_status`, `spouse_prefix`, `spouse_firstname`, `spouse_middlename`, `spouse_familyname`, `spouse_birth_date`, `current_live_status`, `spouse_current_addr`, `spouse_current_addr_line`, `spouse_current_city`, `spouse_current_state`, `spouse_current_zipcode`, `spouse_current_country`, `spouse_passport_no`, `spouse_passport`, `spouse_passport_approved`, `spouse_travel_uk`) VALUES
(1, 93, 'Website', NULL, 91, '2147483647', 'SKY7666', 'http://maxglobal.herdzo.com/files/zts2b313s9jp2qexr1uv/skype.png?s=130', 'Algeria', 'India', NULL, 'Kerala', 'Indian	', 'Hospitality', 1, 'ucas12', 0, 'City Lane2', 'Second Road', 'Kakanad', 'Kerala', '667766', 'India', 'Jun 27, 2014', 'Jun 03, 2020', 'Single', 'Mr', 'Spouse', 'M', 'Familyname', 'Jun 30, 20', 'Yes', 'Lane3', 'City Road', 'Mumbai', 'Mumbai', '99889988', 'India', 'ASD2233', 'http://maxglobal.herdzo.com/files/gtlrrkikipd6mf0qfcjk/Nurse_Profile_Fields.docx', 0, 'No'),
(2, 96, 'Website', 'SD2833', 0, '2147483647', 'skype', 'http://maxglobal.herdzo.com/files/l4ygzltizfkh6rx2ae1p/download3.jpg?s=130,http://maxglobal.herdzo.c', 'Algeria', 'Andorra', NULL, 'Pathanamthita', 'Andorran', 'Study', 0, '1223', 0, 'Miami', 'St2', 'Miami', 'AZ', '123', '', 'Jul 23, 2020', 'Jul 21, 2020', 'Single', 'Mr', '', '', '', '', 'Yes', '', '', '', '', '', '', '', '', 0, 'Yes'),
(3, 113, 'Friend', 'Arsha', 3, '2147483647', 'arsha555', 'http://maxglobal.herdzo.com/files/xofb0zzv1iwdol0v7451/g2.jpg?s=130', 'India', 'India', NULL, 'Tvm', 'Indian	', 'study sector', 0, 'UCAS444', 0, 'Lake View', 'St 789', 'Tvm', 'Kerala', '695581', 'India', 'Feb 05, 2020', 'Sep 24, 2020', 'Single', 'Mr', '', '', '', '', 'Yes', '', '', '', '', '', '', '', '', 0, 'Yes'),
(4, 134, '', '', 0, '0', '', '', '', '', NULL, '', '', '', 0, '', 0, '', '', '', '', '', '', '', '', 'Single', 'Mr', '', '', '', '', 'Yes', '', '', '', '', '', '', '', '', 0, 'Yes'),
(5, 141, '', '', 0, '0', '', '', '', '', NULL, '', '', '', 0, '', 0, '', '', '', '', '', '', '', '', 'Single', 'Mr', '', '', '', '', 'Yes', '', '', '', '', '', '', '', '', 0, 'Yes'),
(6, 170, '', '', 0, '0', '', '', 'Albania', '', NULL, '', '', '', 0, '', 0, '', '', '', '', '', 'Antigua and Barbuda', '', '', 'Married', 'Mr', '', '', '', '', 'Yes', '', '', '', '', '', '', '', '', 0, 'Yes'),
(7, 174, 'Friend', 'Arjun', 91, '9656774188', 'arjun3456', 'http://localhost/maxglobal/files/c2qevyvbokhg9awjpjbo/flower-img.jpg?s=130,http://localhost/maxgloba', 'India', 'India', NULL, 'Trivandrum', 'Indian	', 'Sector', 0, 'UCAS56789', 0, 'Street 555', 'Street 777', 'Attingal', 'Kerala', '695678', 'India', 'May 17, 2007', 'Aug 21, 2020', 'Single', 'Mr', '', '', '', '', 'Yes', '', '', '', '', '', '', '', '', 0, 'Yes'),
(8, 196, 'Friend', 'Ajay', 91, '9447034555', 'ajay345', 'http://localhost/maxglobal/files/i4ia8mshig7hi01pk6zl/ev-img01.jpg?s=130', 'Canada', 'India', NULL, 'Trivandrum', 'Indian	', 'Sector345', 0, 'UCAS56789', 0, 'Street 567', 'Lt Ar456', 'Attingal', 'Kerala', '695678', 'Ireland', 'Apr 01, 2022', 'May 01, 2022', 'Married', 'Mrs', 'Sreeja', 'R', 'Raj', 'May 11, 1983', 'No', 'St444', 'St777', 'NAGERCOIL', 'Tamil Nadu', '629001', 'Singapore', 'PT5484353', 'http://localhost/maxglobal/files/26kacyxaaecixzjdvjoy/ev-img02.jpg?s=130', 0, 'Yes'),
(9, 232, '', '', 0, '', '', 'http://localhost/maxglobal/files/nnr20ndruailyhufdc3h/ev-img01.jpg?s=130,http://localhost/maxglobal/files/q5rtrffeog7r16ra2wnx/ev-img02.jpg?s=130', '', '', NULL, '', '', '', 0, '', 0, '', '', '', '', '', '', '', '', 'Single', 'Mr', '', '', '', '', 'Yes', '', '', '', '', '', '', '', '', 0, 'Yes'),
(10, 224, '', '', 0, '', '', 'http://localhost/maxglobal/files/xymm3a2newdvnpfi8s8a/ev-img01.jpg?s=130,http://localhost/maxglobal/files/h9qqtjjejrfsw9x10fpb/ev-img02.jpg?s=130', '', '', NULL, '', '', '', 0, '', 0, '', '', '', '', '', '', '', '', 'Single', 'Mr', '', '', '', '', 'Yes', '', '', '', '', '', '', '', '', 0, 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `data_immigration_history`
--

CREATE TABLE `data_immigration_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `in_uk` tinyint(1) NOT NULL DEFAULT 0,
  `studied_uk` tinyint(1) NOT NULL DEFAULT 0,
  `studied_country` varchar(100) DEFAULT NULL,
  `studied_visa_start_date` varchar(20) DEFAULT NULL,
  `studied_visa_end_date` varchar(20) DEFAULT NULL,
  `studied_university` varchar(100) DEFAULT NULL,
  `studied_status` varchar(10) DEFAULT NULL,
  `studied_cas_start_date` varchar(15) DEFAULT NULL,
  `studied_cas_end_date` varchar(15) DEFAULT NULL,
  `cas_transcripts` varchar(250) DEFAULT NULL,
  `studied_course_title` varchar(100) DEFAULT NULL,
  `json_data_studied` text DEFAULT NULL,
  `uk_visa_issued` tinyint(1) NOT NULL DEFAULT 0,
  `other_countries` tinyint(1) NOT NULL DEFAULT 0,
  `other_country_name` varchar(100) DEFAULT NULL,
  `reason` text DEFAULT NULL,
  `visite_date` varchar(15) DEFAULT NULL,
  `leave_date` varchar(15) DEFAULT NULL,
  `json_data` text DEFAULT NULL,
  `course` text NOT NULL,
  `selected_university_imigration` varchar(100) NOT NULL,
  `tution_fee` int(11) NOT NULL,
  `scholarship_stat` varchar(20) DEFAULT NULL,
  `uni_aplctn_stat` varchar(10) DEFAULT NULL,
  `offer_stat` varchar(10) DEFAULT NULL,
  `offerletter_proof` text DEFAULT NULL,
  `tuitionfeecheck` varchar(10) DEFAULT NULL,
  `tution_paid` int(11) NOT NULL,
  `tution_balance` int(11) NOT NULL,
  `mode_mainatanence` varchar(25) DEFAULT NULL,
  `maintenance_proof` text DEFAULT NULL,
  `TB_proof` text DEFAULT NULL,
  `cas_stat` varchar(10) DEFAULT NULL,
  `cas_proof` text DEFAULT NULL,
  `visa_aplctn_stat` varchar(20) DEFAULT NULL,
  `visa_apntmnt_date` varchar(15) NOT NULL,
  `visa_apntmnt_time` varchar(10) NOT NULL,
  `visa_apntmnt_place` varchar(100) NOT NULL,
  `visastatus` varchar(25) DEFAULT NULL,
  `visa_file` text DEFAULT NULL,
  `visa_file_approved` tinyint(1) NOT NULL DEFAULT 0,
  `ticket_proof` text DEFAULT NULL,
  `enrollment_stat` text NOT NULL,
  `enrollment_num` int(11) NOT NULL,
  `placement_through` varchar(10) DEFAULT NULL,
  `invoice_stat` varchar(10) DEFAULT NULL,
  `invoice_proof` text DEFAULT NULL,
  `payment_stat` varchar(10) DEFAULT NULL,
  `sub_agent` int(1) NOT NULL,
  `sub_agent_invoice` text DEFAULT NULL,
  `sub_agent_paymnt_stat` varchar(10) DEFAULT NULL,
  `additional_proofs` text DEFAULT NULL,
  `last_communication` text NOT NULL,
  `add_doc_approved` tinyint(1) DEFAULT NULL,
  `invoice_doc_approved` tinyint(1) DEFAULT NULL,
  `flight_doc_approved` tinyint(1) DEFAULT NULL,
  `cas_doc_approved` tinyint(1) DEFAULT NULL,
  `tb_report_approved` tinyint(4) DEFAULT NULL,
  `maintainace_doc_approved` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_immigration_history`
--

INSERT INTO `data_immigration_history` (`id`, `user_id`, `in_uk`, `studied_uk`, `studied_country`, `studied_visa_start_date`, `studied_visa_end_date`, `studied_university`, `studied_status`, `studied_cas_start_date`, `studied_cas_end_date`, `cas_transcripts`, `studied_course_title`, `json_data_studied`, `uk_visa_issued`, `other_countries`, `other_country_name`, `reason`, `visite_date`, `leave_date`, `json_data`, `course`, `selected_university_imigration`, `tution_fee`, `scholarship_stat`, `uni_aplctn_stat`, `offer_stat`, `offerletter_proof`, `tuitionfeecheck`, `tution_paid`, `tution_balance`, `mode_mainatanence`, `maintenance_proof`, `TB_proof`, `cas_stat`, `cas_proof`, `visa_aplctn_stat`, `visa_apntmnt_date`, `visa_apntmnt_time`, `visa_apntmnt_place`, `visastatus`, `visa_file`, `visa_file_approved`, `ticket_proof`, `enrollment_stat`, `enrollment_num`, `placement_through`, `invoice_stat`, `invoice_proof`, `payment_stat`, `sub_agent`, `sub_agent_invoice`, `sub_agent_paymnt_stat`, `additional_proofs`, `last_communication`, `add_doc_approved`, `invoice_doc_approved`, `flight_doc_approved`, `cas_doc_approved`, `tb_report_approved`, `maintainace_doc_approved`) VALUES
(1, 93, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Angola', 'Work', 'Jun 24, 20', 'Jun 26, 20', '[{\"other_country_name\":\"\",\"reason\":\"Work\",\"visite_date\":\"Jun 30, 2020\",\"leave_date\":\"Jun 27, 2020\"},{\"other_country_name\":\"\",\"reason\":\"Tourism\",\"visite_date\":\"Jun 24, 2020\",\"leave_date\":\"Jun 27, 2020\"}]', '1', 'University A', 100, 'RECEIVED', 'REFUSED', 'RECEIVED', '', 'PAID', 900, 100, NULL, '', 'http://maxglobal.herdzo.com/files/yfje0uw7ykeyscbv1zjk/Nurse_Profile_Fields.docx', 'RECEIVED', '', '0', '', '0', '0', NULL, NULL, 0, 'http://maxglobal.herdzo.com/files/yb4hdarmyljgaqa3vkxk/MaxGlobalConsultants_Application_Development_Scope.odt', 'ENROLLED', 0, '2', 'SUBMITTED', '', 'RECEIVED', 3, 'RECEIVED', 'NOT-PAID', 'http://maxglobal.herdzo.com/files/qyykmzloo47fwu2snpeb/Nurse_Profile_Fields.docx', 'Last communication details. ', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 95, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', '', '', NULL, '3', 'University B', 0, 'OFFERED', 'SUBMITED', 'WAITING', '', NULL, 0, 0, NULL, '', '', NULL, '', '0', '', '0', '0', NULL, NULL, 0, '', '', 0, NULL, NULL, '', NULL, 0, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 96, 1, 0, 'Afghanistan', 'Jul 17, 2020', 'Jul 21, 2020', 'University', 'Completed', 'Jul 29, 20', 'Jul 25, 20', 'CAS', 'Course ', NULL, 0, 0, 'Afghanistan', 'Tourism', 'Jul 21, 20', 'Jul 25, 20', NULL, '', 'Abertay University', 1233, 'OFFERED', 'SUBMITED', 'WAITING', '', 'NOT-PAID', 1000, 100, 'Bank-Statement', 'http://maxglobal.herdzo.com/files/5nxvdfpampf9ohp3cvli/Document.rtf', 'http://maxglobal.herdzo.com/files/bgycwtlemxkuoptcztbw/skype.png?s=130,http://maxglobal.herdzo.com/files/vbmn5btd2qy8za7gvei0/Document.rtf', 'PENDING', '', 'SUBMITTED', 'Jul 20, 20', '04:13 PM', 'Centre', 'Status', 'http://maxglobal.herdzo.com/files/pdkztapvl359xwjj2aml/Document.rtf', 0, '', 'ENROLLED', 0, '2', 'SUBMITTED', '', 'NOT-RECEIV', 3, 'RECEIVED', 'NOT-PAID', 'http://maxglobal.herdzo.com/files/4lickpgn9nzgjweoa2ft/Document.rtf', 'Last Communication Last Communication Last Communication Last Communication Last Communication Last Communication Last Communication Last Communication Last Communication Last Communication Last Communication Last Communication Last Communication Last Communication Last Communication Last Communication Last Communication Last Communication Last Communication Last Communication Last Communication Last Communication Last Communication ', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 113, 0, 0, 'Denmark', 'Feb 12, 2020', 'Oct 23, 2020', 'Denmark', 'Completed', 'Jul 09, 20', 'Oct 15, 20', 'Visa copy', 'BCS', NULL, 0, 0, 'Finland', 'Tourism', 'May 06, 20', 'Jun 04, 20', NULL, '', 'University of Aberdeen', 7000, 'NOT-RECEIV', 'ACCEPTED', 'RECEIVED', 'http://maxglobal.herdzo.com/files/vq6y2vumrggxxu2heorm/Tulips.jpg?s=130', 'PAID', 6000, 0, 'Bank-Statement', 'http://maxglobal.herdzo.com/files/vwvzasy8xcflm8vqfilj/Tulips.jpg?s=130', 'http://maxglobal.herdzo.com/files/ngr8sieedavplvvehcfn/Tulips.jpg?s=130', 'RECEIVED', 'http://maxglobal.herdzo.com/files/2uaed6zj8zplqvsxkrsw/Tulips.jpg?s=130', 'SUBMITTED', 'Oct 01, 20', '03:56 PM', 'TVM', 'Pending', 'http://maxglobal.herdzo.com/files/kddw0vqeaf3mrld677sd/Tulips.jpg?s=130', 0, 'http://maxglobal.herdzo.com/files/a0acfw8iff21jrazoxgx/Tulips.jpg?s=130', 'ENROLLED', 454545777, '2', 'SUBMITTED', '', 'RECEIVED', 1, 'NOT-RECEIVED', 'PAID', 'http://maxglobal.herdzo.com/files/2ulndwkfam3mhcosfiu9/Tulips.jpg?s=130', 'Last communication', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 134, 0, 0, '', '', '', '', '', '', '', '', '', NULL, 0, 0, '', '', '', '', NULL, '', '', 0, 'OFFERED', 'SUBMITED', NULL, '', NULL, 0, 0, '', '', '', NULL, '', NULL, '', '', '', '', '', 0, '', '', 0, NULL, NULL, '', NULL, 0, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 140, 0, 0, '', '', '', '', '', '', '', '', '', NULL, 0, 0, '', '', '', '', NULL, '', '', 0, 'OFFERED', 'SUBMITED', NULL, '', NULL, 0, 0, '', '', '', NULL, '', NULL, '', '', '', '', '', 0, '', '', 0, NULL, NULL, '', NULL, 0, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 174, 0, 0, 'Colombia', 'May 20, 2016', 'Jun 25, 2020', 'SCNS', 'Completed', 'Jul 10, 2019', 'Jun 24, 2020', '5et4t5tre4', 'MSC', NULL, 0, 0, 'Bahrain', 'Tourism', 'Jul 27, 2018', 'Aug 27, 2019', NULL, 'aa', 'Abertay University', 6780, 'RECEIVED', 'ACCEPTED', 'RECEIVED', 'http://localhost/maxglobal/files/g6pkeqzd1uaosunvkmlo/A Sample PDF.pdf,http://localhost/maxglobal/files/70wj9npb0vfxso3n20nz/A Sample PDF.pdf', 'PAID', 7000, 2000, 'Bank-Statement', 'http://localhost/maxglobal/files/zxrjzmiwvdvnbewjfgf2/A Sample PDF.pdf,http://localhost/maxglobal/files/n8t1wvohtbdeobjvczaq/A Sample PDF.pdf,http://localhost/maxglobal/files/vzor0fwwjog9mrlbfwrr/A Sample PDF.pdf,http://localhost/maxglobal/files/4hkvzbcd7obbn2eyw4o1/Get_Started_With_Smallpdf.pdf', 'http://localhost/maxglobal/files/fskeabasbmkdefvqve2s/A Sample PDF.pdf,http://localhost/maxglobal/files/igzl5tmkglds539xschc/A Sample PDF.pdf,http://localhost/maxglobal/files/jk6njnost2xyu7ounnpv/Get_Started_With_Smallpdf.pdf', 'PENDING', '', 'SUBMITTED', 'Jan 05, 2019', '03:45 PM', 'SGH', 'Completed', 'http://localhost/maxglobal/files/ohx4fhclmsoxqzsiba55/A Sample PDF.pdf,http://localhost/maxglobal/files/frw7sa8acdliy7zu1coo/A Sample PDF.pdf,http://localhost/maxglobal/files/vorinokvmpa8j1w4hztc/Get_Started_With_Smallpdf.pdf,http://localhost/maxglobal/files/qcnt30udupv4phqxqlwe/Get_Started_With_Smallpdf.pdf', 0, 'http://localhost/maxglobal/files/u7cikajzaz6lk6oujcmj/ev-img01.jpg?s=130,http://localhost/maxglobal/files/g0zimhfz8dklwc5g2913/ev-img02.jpg?s=130,http://localhost/maxglobal/files/lxjfbihc875t5ii0inv8/A Sample PDF.pdf,http://localhost/maxglobal/files/g3rcwtubvvidzqooleuc/ev-img02.jpg?s=130', 'ENROLLED', 0, '1', 'SUBMITTED', '', 'RECEIVED', 3, 'RECEIVED', 'PAID', 'http://localhost/maxglobal/files/m9ojcoa3erwa7f8ybtsy/A Sample PDF.pdf,http://localhost/maxglobal/files/v3v1h4tioooayeci8zcl/A Sample PDF.pdf,http://localhost/maxglobal/files/ydgpxe33n9qerq0v52rp/Get_Started_With_Smallpdf.pdf', 'Last communication', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 196, 0, 0, 'Afghanistan', '', '', '', '', '', '', '', '', '[{\"studied_country\":\"Cambodia\",\"studied_visa_start_date\":\"\",\"studied_visa_end_date\":\"\",\"studied_university\":\"\",\"studied_course_title\":\"\",\"studied_status\":\"\",\"studied_cas_start_date\":\"\",\"studied_cas_end_date\":\"\",\"cas_transcripts\":\"\"},{\"studied_country\":\"Egypt\",\"studied_visa_start_date\":\"\",\"studied_visa_end_date\":\"\",\"studied_university\":\"\",\"studied_course_title\":\"\",\"studied_status\":\"\",\"studied_cas_start_date\":\"\",\"studied_cas_end_date\":\"\",\"cas_transcripts\":\"\"}]', 0, 0, 'Afghanistan', '', '', '', '[{\"other_country_name\":\"Australia\",\"reason\":\"\",\"visite_date\":\"\",\"leave_date\":\"\"},{\"other_country_name\":\"Egypt\",\"reason\":\"\",\"visite_date\":\"\",\"leave_date\":\"\"}]', 'MBA', '', 0, 'OFFERED', 'SUBMITED', NULL, '', NULL, 0, 0, '', 'http://localhost/maxglobal/files/ybdvpvdhmts1cgzzyoco/A Sample PDF.pdf,http://localhost/maxglobal/files/xyiio7vhwrh8d2f5z9mi/Get_Started_With_Smallpdf.pdf', 'http://localhost/maxglobal/files/o5bjgtnycdia5pnrcnlt/A Sample PDF.pdf,http://localhost/maxglobal/files/jewztpj5tnalhn9ybvtf/Get_Started_With_Smallpdf.pdf', NULL, '', NULL, '', '', '', '', '', 0, '', '', 0, NULL, NULL, '', NULL, 0, NULL, NULL, 'http://localhost/maxglobal/files/phdi1oy4r8bpexavjk3c/A Sample PDF.pdf,http://localhost/maxglobal/files/tf3crwiiudquc66wuexl/Get_Started_With_Smallpdf.pdf', '', NULL, NULL, NULL, NULL, NULL, NULL),
(9, 222, 0, 0, 'India', 'May 01, 2022', 'May 31, 2022', '(INTO) University of Manchester', 'Completed', 'Jun 01, 2022', 'Jun 15, 2022', 'TR1111', 'MBA', '[{\"studied_country\":\"Singapore\",\"studied_visa_start_date\":\"May 01, 2023\",\"studied_visa_end_date\":\"May 01, 2024\",\"studied_status\":\"Completed\",\"studied_university\":\"(KAPLAN) University of Nottingham International College\",\"studied_course_title\":\"MBA\",\"studied_cas_start_date\":\"Jul 01, 2022\",\"studied_cas_end_date\":\"Jul 31, 2022\",\"cas_transcripts\":\"TR3333\"},{\"studied_country\":\"Germany\",\"studied_visa_start_date\":\"Oct 01, 2022\",\"studied_visa_end_date\":\"Oct 20, 2022\",\"studied_status\":\"Not-completed\",\"studied_university\":\"University of Birmingham\",\"studied_course_title\":\"MCA\",\"studied_cas_start_date\":\"May 24, 2022\",\"studied_cas_end_date\":\"May 28, 2022\",\"cas_transcripts\":\"TR4444\"}]', 0, 0, 'Australia', 'Work', 'Jun 16, 2022', 'Jun 17, 2022', '[{\"other_country_name\":\"Singapore\",\"reason\":\"Tourism\",\"visite_date\":\"Sep 06, 2022\",\"leave_date\":\"Sep 23, 2022\"}]', 'MBA', '(STUDY GROUP) University of Leeds, Leeds', 50000, 'OFFERED', 'SUBMITED', 'RECEIVED', 'http://localhost/maxglobal/files/aotkvqyudxhxr0dp53u5/A Sample PDF.pdf,http://localhost/maxglobal/files/gt7gsno00lwmutkuynq9/A Sample PDF.pdf', 'PAID', 5000, 10000, 'Bank-Statement', 'http://localhost/maxglobal/files/lx6meyy4klwavsvq06zy/A Sample PDF.pdf', 'http://localhost/maxglobal/files/txdkbcrpvuuji3ntier4/A Sample PDF.pdf', 'RECEIVED', 'http://localhost/maxglobal/files/bkfxvhyapnijvap4zzti/A Sample PDF.pdf', 'SUBMITTED', 'May 25, 2022', '05:30 PM', 'TRS Centre', 'Complete', 'http://localhost/maxglobal/files/ps3rkfjxnlgv51onqerw/A Sample PDF.pdf', 0, 'http://localhost/maxglobal/files/yf0zlktahftnzyq9jpuw/A Sample PDF.pdf,http://localhost/maxglobal/files/iwmb9kteih19ltw1cnok/A Sample PDF.pdf', 'ENROLLED', 56666, '2', 'SUBMITTED', '', 'RECEIVED', 2, 'NOT-RECEIVED', 'NOT-PAID', 'http://localhost/maxglobal/files/i8bq1deso84zndnjrgms/A Sample PDF.pdf,http://localhost/maxglobal/files/rb0ioglgiyrktupesoqx/A Sample PDF.pdf', 'Final communication', NULL, NULL, NULL, NULL, NULL, NULL),
(10, 232, 0, 0, '', '', '', '', '', '', '', '', '', NULL, 0, 0, '', '', '', '', '', 'MBA', '', 0, 'OFFERED', 'SUBMITED', NULL, '', NULL, 0, 0, '', 'http://localhost/maxglobal/files/gzcldpahjbmekztyvk6k/A Sample PDF.pdf,http://localhost/maxglobal/files/fkb3qtx0gflsvcsms2li/dummy.pdf', 'http://localhost/maxglobal/files/h7g7n2ofwychdhx1rtf5/pdf-sample.pdf,http://localhost/maxglobal/files/5rjxlkab9qgz1pivav3x/dummy.pdf', NULL, '', NULL, '', '', '', '', '', 0, '', '', 0, NULL, NULL, '', NULL, 0, NULL, NULL, 'http://localhost/maxglobal/files/ormavzxypirjfwzqmxfe/dummy.pdf,http://localhost/maxglobal/files/bgppo3agqgqxmvz2ojjr/A Sample PDF.pdf', '', NULL, NULL, NULL, NULL, NULL, NULL),
(11, 261, 0, 0, '', '', '', '', '', '', '', '', '', NULL, 0, 0, '', '', '', '', '', 'MBA', '', 0, 'OFFERED', 'SUBMITED', NULL, '', NULL, 0, 0, '', 'http://localhost/maxglobal/files/2zqagfxndmdlyueivkk7/A Sample PDF.pdf,http://localhost/maxglobal/files/zkrra8ndfgf2ui6inh83/dummy.pdf', 'http://localhost/maxglobal/files/vcpmxd9a21qxn9qkk4ua/pdf-sample.pdf,http://localhost/maxglobal/files/qopvbzddswrse0rrw50v/dummy.pdf', NULL, '', NULL, '', '', '', '', '', 0, '', '', 0, NULL, NULL, '', NULL, 0, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `data_log`
--

CREATE TABLE `data_log` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL DEFAULT 0,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `created_by` int(10) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_log`
--

INSERT INTO `data_log` (`id`, `user_id`, `name`, `email`, `created_by`, `created_at`) VALUES
(1, 199, 'Sameer', 'sameer456@yopmail.com', 155, '2022-05-05 05:23:51'),
(2, 200, 'Afsal', 'afsal345@yopmail.com', 155, '2022-05-05 05:29:59'),
(3, 201, 'Remya', 'remya345@yopmail.com', 155, '2022-05-05 05:46:47'),
(4, 202, 'Anoop', 'anoop345@yopmail.com', 155, '2022-05-05 05:47:27'),
(5, 204, 'Ajith', 'ajith345@yopmail.com', 155, '2022-05-05 06:02:43'),
(6, 205, 'Kiran', 'kiran345@yopmail.com', 155, '2022-05-05 06:03:21'),
(7, 209, 'Saranya', 'saranya345@yopmail.com', 155, '2022-05-07 01:31:36'),
(8, 210, 'Tharun', 'tharun345@yopmail.com', 155, '2022-05-07 04:20:53'),
(9, 211, 'Archana', 'archana3345@yopmail.com', 155, '2022-05-07 04:50:57'),
(10, 212, 'Anupama', 'anupama345@yopmail.com', 155, '2022-05-07 06:09:42'),
(12, 214, 'Ardra', 'ardra123@yopmail.com', 0, '2022-05-10 02:10:05'),
(14, 216, 'Keerthy', 'keerthy123@yopmail.com', 0, '2022-05-10 04:08:40'),
(15, 217, 'Anupama', 'anupama567@yopmail.com', 0, '2022-05-10 04:16:55'),
(16, 219, 'Athul', 'athul345@yopmail.com', 0, '2022-05-10 06:48:08'),
(17, 221, 'Sreeja', 'sreeja345@yopmail.com', 155, '2022-05-11 20:18:04'),
(18, 222, 'Aswathy', 'aswathy543@yopmail.com', 155, '2022-05-13 23:58:00'),
(19, 223, 'Anagha', 'anagha456@yopmail.com', 0, '2022-05-23 05:48:55'),
(20, 224, 'Anagha', 'anagha567@yopmail.com', 0, '2022-05-23 05:50:51'),
(21, 225, 'Aneesh', 'aneesh567@yopmail.com', 0, '2022-05-23 05:54:52'),
(22, 226, 'Anitha', 'anitha456@yopmail.com', 175, '2022-05-24 20:14:11'),
(23, 227, 'Rihan', 'rihan567@yopmail.com', 178, '2022-05-24 20:17:50'),
(24, 228, 'Abhilash', 'abhilash456@yopmail.com', 178, '2022-05-24 20:32:33'),
(25, 229, 'Rahul', 'rahul789@yopmail.com', 178, '2022-05-24 20:38:08'),
(26, 230, 'Dhanya', 'dhanya345@yopmail.com', 178, '2022-05-24 20:49:25'),
(27, 231, 'Shivani', 'shivani678@yopmail.com', 178, '2022-05-24 20:57:25'),
(28, 232, 'Shiyas', 'shiyas123@yopmail.com', 176, '2022-05-25 22:49:36'),
(29, 236, 'Nithin', 'nithin777@yopmail.com', 0, '2022-08-16 07:12:52'),
(30, 237, 'Krishna', 'krishna777@yopmail.com', 0, '2022-08-15 19:55:04'),
(31, 238, 'Anil', 'anil777@yopmail.com', 0, '2022-08-15 20:02:31'),
(32, 239, 'Anila', 'anila777@yopmail.com', 0, '2022-08-15 20:05:08'),
(33, 240, 'Deepthi', 'deepthi777@yopmail.com', 0, '2022-08-15 20:10:48'),
(34, 241, 'Anoop', 'anoop777@yopmail.com', 0, '2022-08-15 20:17:25'),
(35, 242, 'Adithya', 'adithya777@yopmail.com', 0, '2022-08-15 20:26:43'),
(36, 243, 'Krithi', 'krithi777@yopmail.com', 0, '2022-08-15 20:34:36'),
(37, 244, 'Deepak', 'deepak333@yopmail.com', 0, '2022-08-15 20:41:29'),
(38, 245, 'Asim', 'asim333@yopmail.com', 0, '2022-08-15 20:43:49'),
(39, 246, 'Swathy', 'swathy555@yopmail.com', 0, '2022-08-15 20:51:24'),
(40, 247, 'Sreeja', 'sreeja333@yopmail.com', 0, '2022-08-15 21:23:57'),
(41, 248, 'Saranya', 'saranya567@yopmail.com', 155, '2022-08-16 22:58:54'),
(42, 249, 'Akshara', 'akshara567@yopmail.com', 0, '2022-08-16 23:07:43'),
(43, 250, 'Sreethu', 'sreethu567@yopmail.com', 155, '2022-08-16 23:10:16'),
(44, 251, 'Hareesh', 'hareesh444@yopmail.com', 0, '2022-08-29 07:01:25'),
(45, 252, 'Sarath', 'sarath444@yopmail.com', 0, '2022-08-29 07:03:14'),
(46, 253, 'Karthik', 'karthik444@yopmail.com', 0, '2022-08-29 07:04:53'),
(47, 254, 'Sameer', 'sameer444@yopmail.com', 0, '2022-08-29 07:06:41'),
(48, 255, 'Vijay', 'vijay444@yopmail.com', 0, '2022-08-29 07:21:43'),
(49, 256, 'Anoop', 'anoop789@yopmail.com', 175, '2022-09-05 23:38:01'),
(50, 257, 'Rajiv', 'rajiv578@yopmail.com', 155, '2022-09-05 23:40:33'),
(51, 258, 'Deepak', 'deepak689@yopmail.com', 175, '2022-09-05 23:42:46'),
(52, 259, 'Ashwin', 'ashwin578@yopmail.com', 175, '2022-09-05 23:44:00'),
(53, 260, 'Sitara', 'sitara467@yopmail.com', 175, '2022-09-05 23:48:56'),
(54, 261, 'Vijay', 'vijay578@yopmail.com', 176, '2022-09-06 00:22:00'),
(55, 262, 'Amal', 'amal568@yopmail.com', 178, '2022-09-07 00:59:23'),
(56, 263, 'Vineetha', 'vineetha345@yopmail.com', 155, '2022-09-17 04:47:21'),
(57, 264, 'Sajin', 'sajin345@yopmail.com', 155, '2022-09-17 04:47:55'),
(58, 265, 'Sarath', 'sarath3455@yopmail.com', 0, '2022-09-25 19:47:25'),
(59, 266, 'Sarath', 'sarath789@yopmail.com', 0, '2022-09-25 19:53:39'),
(60, 267, 'Hridya', 'hridya567@yopmail.com', 0, '2022-10-07 02:19:10'),
(61, 268, 'Syam', 'syam567@yopmail.com', 0, '2022-10-07 03:05:40'),
(62, 269, 'Syam', 'syam567@yopmail.com', 0, '2022-10-07 03:10:15'),
(63, 270, 'Syam', 'syam567@yopmail.com', 0, '2022-10-07 03:12:02'),
(64, 271, 'Syan', 'syam567@yopmail.com', 0, '2022-10-07 03:14:26'),
(65, 272, 'Riyad', 'riyad567@yopmail.com', 0, '2022-10-07 03:16:09'),
(66, 273, 'Revathy', 'revathy567@yopmail.com', 0, '2022-10-07 03:30:20'),
(67, 274, 'Riyas', 'riyas567@yopmail.com', 0, '2022-10-07 04:11:02'),
(68, 275, 'Anitha', 'anitha567@yopmail.com', 0, '2022-10-07 04:18:53'),
(69, 276, 'Anitha', 'anitha567@yopmail.com', 0, '2022-10-07 04:21:42'),
(70, 277, 'Kanika', 'kanika345@yopmail.com', 0, '2022-10-07 05:38:44'),
(71, 278, 'Rejin', 'rejin456@yopmail.com', 0, '2022-10-09 05:20:13'),
(72, 279, 'Ajmal', 'ajmal567@yopmail.com', 0, '2022-10-09 05:23:20'),
(73, 280, 'Anandhu', 'anandhu456@yopmail.com', 0, '2022-10-09 05:26:27'),
(74, 281, 'Simi', 'simi567@yopmail.com', 0, '2022-10-09 05:36:35'),
(75, 282, 'Maya', 'maya456@yopmail.com', 0, '2022-10-09 05:37:39'),
(76, 283, 'Neeraja', 'neeraja567@yopmail.com', 0, '2022-10-09 05:40:50'),
(77, 284, 'Saniya', 'saniya345@yopmail.com', 0, '2022-10-09 06:00:29'),
(78, 285, 'Amjith', 'amjith567@yopmail.com', 0, '2022-10-09 23:07:52'),
(79, 286, 'Sanya', 'sanya456@yopmail.com', 0, '2022-10-09 23:17:47'),
(80, 287, 'Krishna', 'krishna478@yopmail.com', 0, '2022-10-09 23:20:18'),
(81, 288, 'Anusha', 'anusha678@yopmail.com', 0, '2022-10-09 21:38:33'),
(82, 289, 'Keerthi', 'keerthi678@yopmail.com', 0, '2022-10-09 21:41:00'),
(83, 290, 'Parvathy', 'parvathy678@yopmail.com', 0, '2022-10-09 21:49:50'),
(84, 291, 'Krithi', 'krithi678@yopmail.com', 0, '2022-10-10 22:46:56'),
(85, 292, 'Sreeja', 'sreeja456@yopmail.com', 0, '2022-10-10 23:07:51'),
(86, 293, 'Soumya', 'soumya678@yopmail.com', 0, '2022-10-11 00:59:18'),
(87, 294, 'Seena', 'seena678@yopmail.com', 0, '2022-10-11 01:43:33'),
(88, 295, 'Shivani', 'shivanu678@yopmail.com', 155, '2022-10-14 01:24:50'),
(89, 296, 'Smitha', 'smitha455@yopmail.com', 155, '2022-10-14 01:37:46'),
(90, 297, 'Anjitha', 'anjitha257@yopmail.com', 155, '2022-10-14 01:38:51'),
(91, 298, 'Adarsh', 'adarsh545@yopmail.com', 155, '2022-10-14 01:45:15');

-- --------------------------------------------------------

--
-- Table structure for table `data_parent_det`
--

CREATE TABLE `data_parent_det` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mother_prefix` varchar(7) DEFAULT NULL,
  `mother_firstname` varchar(50) DEFAULT NULL,
  `mother_middlename` varchar(30) DEFAULT NULL,
  `mother_lastname` varchar(30) DEFAULT NULL,
  `mother_birth` varchar(15) DEFAULT NULL,
  `mother_nation` varchar(100) DEFAULT NULL,
  `father_prefix` varchar(5) DEFAULT NULL,
  `father_firstname` varchar(100) DEFAULT NULL,
  `father_middlename` varchar(100) DEFAULT NULL,
  `father_lastname` varchar(100) DEFAULT NULL,
  `father_birth` varchar(15) DEFAULT NULL,
  `father_nation` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_parent_det`
--

INSERT INTO `data_parent_det` (`id`, `user_id`, `mother_prefix`, `mother_firstname`, `mother_middlename`, `mother_lastname`, `mother_birth`, `mother_nation`, `father_prefix`, `father_firstname`, `father_middlename`, `father_lastname`, `father_birth`, `father_nation`) VALUES
(1, 93, 'Mr', 'Annamma', 'O', 'Philip', 'Jun 21, 19', 'Indian	', '0', '0', '0', '0', 'Jun 18, 19', 'Indian	'),
(2, 96, 'Mr', 'MotherName', 'M', 'Philip', 'Jul 20, 20', 'Angolan	', 'Mr', 'Father', 'O', 'Ab', 'Jul 27, 20', 'Angolan	'),
(3, 113, 'Mrs', 'Geetha', 'Ajay', 'Gosh', 'Oct 10, 19', 'Indian	', 'Mr', 'Ajay', 'N', 'Gosh', 'Apr 14, 19', 'Indian	'),
(4, 134, 'Mr', 'wqwwqw', 'wqwqwq', 'wqwqw', 'Mar 22, 20', 'Algerian	', 'Mr', 'wq', 'wqw', 'wqwqw', 'Mar 22, 20', 'Austrian'),
(5, 174, 'Mr', 'Sreeja', 'M', 'Raj', 'May 13, 1970', 'Indian	', 'Mr', 'Anoop', 'R', 'Raj', 'Jul 23, 1965', 'Indian	'),
(6, 196, 'Mrs', 'Sandhya', 'R', 'Raj', 'Apr 10, 1973', 'Indian', 'Mr', 'Ajay', 'N', 'Nair', 'Mar 11, 1970', 'American');

-- --------------------------------------------------------

--
-- Table structure for table `data_personal_information`
--

CREATE TABLE `data_personal_information` (
  `id` int(4) NOT NULL,
  `student_id` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `prefix` varchar(15) NOT NULL,
  `first_name` text NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `sur_name` varchar(100) DEFAULT NULL,
  `dob` varchar(30) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `coountry_code` int(11) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `passport_no` varchar(100) DEFAULT NULL,
  `image_back_approved` tinyint(4) DEFAULT NULL,
  `image_back` text DEFAULT NULL,
  `image_front_approved` tinyint(4) DEFAULT NULL,
  `image_front` text DEFAULT NULL,
  `expiry_date` varchar(30) DEFAULT NULL,
  `issued_date` varchar(30) DEFAULT NULL,
  `address` text NOT NULL,
  `address_2` text DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `zip_code` varchar(20) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `profile_image` text DEFAULT NULL,
  `lived_yrs` int(11) DEFAULT NULL,
  `lived_mnths` int(11) DEFAULT NULL,
  `whatsapp` varchar(11) NOT NULL,
  `age` varchar(3) NOT NULL,
  `district` varchar(50) NOT NULL,
  `coountry_code_whatsapp` int(11) NOT NULL DEFAULT 91,
  `current_status` varchar(30) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_personal_information`
--

INSERT INTO `data_personal_information` (`id`, `student_id`, `user_id`, `prefix`, `first_name`, `middle_name`, `sur_name`, `dob`, `gender`, `coountry_code`, `mobile`, `passport_no`, `image_back_approved`, `image_back`, `image_front_approved`, `image_front`, `expiry_date`, `issued_date`, `address`, `address_2`, `city`, `state`, `zip_code`, `country`, `profile_image`, `lived_yrs`, `lived_mnths`, `whatsapp`, `age`, `district`, `coountry_code_whatsapp`, `current_status`, `notify`) VALUES
(1, '', 91, 'Mr', 'Sijo', 'New', 'Student', 'Jun 09, 1988', 'male', 91, '2147483647', '5544ASD', NULL, 'http://maxglobal.herdzo.com/files/vptxphlwfgdtwuybpcxu/wallpaper.jpg?s=130', NULL, 'http://maxglobal.herdzo.com/files/epmvowv2cukxu4ezbjnm/wallpaper.jpg?s=130', NULL, NULL, '3432/5g, Kakanad', 'Kakanad', 'Cochin', 'Ernakulam', '77887788', 'India', 'http://maxglobal.herdzo.com/files/lapxqdcpodk2np4626rg/wallpaper.jpg?s=130,http://maxglobal.herdzo.c', 4, 0, '0', '0', '', 91, '', 0),
(2, '', 92, 'Mr', 'Sijo2', 'O', 'Philip', 'Jun 26, 2010', 'male', 91, '2147483647', 'ASD44334', NULL, 'http://maxglobal.herdzo.com/files/xxh3f5z3rryiwywced9h/wallpaper.jpg?s=130', NULL, 'http://maxglobal.herdzo.com/files/aezx8agclzcrg2ggud4d/wallpaper.jpg?s=130', NULL, NULL, 'Housr No 23ASD', ' Kakanad', 'City Lane', 'Ernakulam', '9989988', 'India', 'http://maxglobal.herdzo.com/files/xfdwka9rh31a9q18lrax/wallpaper.jpg?s=130', 5, 4, '0', '0', '', 91, '', 0),
(3, '', 93, 'Mr', 'Sijo3', 'SP', 'Philip', 'Jun 22, 2000', 'male', 91, '988998989', 'ASD44334', NULL, 'http://maxglobal.herdzo.com/files/xu45unmupgfb9wkkk40u/passport.jpg?s=130', NULL, 'http://maxglobal.herdzo.com/files/0nmt6ixp8qd0vvgfddns/wallpaper.jpg?s=130,http://maxglobal.herdzo.c', NULL, NULL, 'HS43', 'Nass Lane', 'Kakanad', 'Kerala', '88998899', 'Angola', 'http://maxglobal.herdzo.com/files/evalelq91gqpecmffbbk/dp.jpg?s=130', 5, 8, '0', '0', '', 91, '', 0),
(4, '', 94, 'Mr', 'sreejith', 'e', 'raju', 'May 11, 1987', 'male', 91, '563286122', 'derwgrtnjuytmetu', NULL, '', NULL, '', NULL, NULL, 'dcvefrbgterysn', 'fhnntnstnhjthd', 'hrtyh5r6j57y', 'ytjtjtjjj', 'dfr334', 'Algeria', 'http://maxglobal.herdzo.com/files/zep4a9lyrlcddsbzoihy/NHSmailLogonLog.txt', 6, 8, '0', '0', '', 91, '', 0),
(5, '', 95, 'Mr', 'maxglobal', 'maxglobal', 'maxglobal', 'Jun 21, 2020', 'male', 91, '0', '', NULL, '', NULL, '', NULL, NULL, 'dymmy_text', 'dymmy_text', 'dymmy_text', 'dymmy_text', 'dymmy_text', 'Angola', '', 9, 7, '0', '0', '', 91, '', 0),
(6, '', 96, 'Mr', 'SijoNew', 'O ', 'Philip', 'Jul 28, 1989', 'male', 91, '2147483647', '554455', NULL, 'http://maxglobal.herdzo.com/files/f29uceamprsyejpl0kyp/download3.jpg?s=130', NULL, 'http://maxglobal.herdzo.com/files/wvxjkgi1qo6yrv9juukp/download2.jpg?s=130', NULL, NULL, 'Miami', '2', 'Miami', 'AZ', '123', 'Albania', 'http://maxglobal.herdzo.com/files/d5xdsgnsojmutxn0cr0u/download.png?s=130', 3, 0, '0', '0', '', 91, '', 0),
(7, '', 98, 'Mr', 'Muhamad', '', 'Thaha', 'Nov 16, 1981', 'male', 91, '2147483647', '', NULL, '', NULL, 'http://maxglobal.herdzo.com/files/jydencyblchbndxnxz1t/201_HD_Cam.jpg?s=130', NULL, NULL, 'test', 'test', 'kollam', 'Kerala', '691319', 'American Samoa', 'http://maxglobal.herdzo.com/files/yfohmny7n2bgfzkczgdq/image.jpeg?s=130', 4, 5, '0', '0', '', 91, '', 0),
(9, '', 113, 'Miss.', 'Anju', 'Ajay', 'Gosh', 'Feb 14, 1990', 'female', 91, '2147483647', '', NULL, '', NULL, '', NULL, NULL, 'Lake View', 'Varkala', 'Trivandrum', 'Kerala', '695318', 'India', 'http://maxglobal.herdzo.com/files/zn4oxfc9ifrjigjty4pv/g2.jpg?s=130', 7, 3, '0', '0', '', 91, '', 0),
(10, '', 134, 'Mr', '111', '111', '111', 'Mar 16, 2021', 'male', 91, '88343434', '', NULL, 'http://maxglobal.herdzo.com/files/pya7ds10ntzidvzltwam/Koala.jpg?s=130', NULL, 'http://maxglobal.herdzo.com/files/kpaykeztdmyx1vrklqlh/Desert.jpg?s=130', NULL, NULL, 'dsd', 'asas', 'dsd', 'dsd', 'sds', 'United States', 'http://maxglobal.herdzo.com/files/57ujnvhrjrbfeh1a1ids/Chrysanthemum.jpg?s=130', 10, 8, '0', '0', '', 91, '', 0),
(11, '', 139, 'Mr', 'dsd11111', 'dsd', 'dsdsd', 'Mar 21, 2021', 'male', 91, '88343434', '', NULL, '', NULL, '', NULL, NULL, 'dsd', 'dsd', 'dsd', 'dsd', 'sds', 'Armenia', '', 0, 0, '0', '0', '', 91, '', 0),
(12, '', 140, 'Mr', 'dsd11111', 'dsd', 'dsdsd', 'Mar 14, 2021', 'male', 91, '88343434', '', NULL, '', NULL, '', NULL, NULL, 'dsd', 'dsd', 'dsd', 'dsd', 'sds', 'Armenia', '', 0, 0, '0', '0', '', 91, '', 0),
(13, '', 141, 'Mr', 'dsd', 'dsd', 'sdsd', 'Mar 21, 2021', 'male', 91, '88343434', '', NULL, '', NULL, '', NULL, NULL, 'dsd', 'dsd', 'dsd', 'dsd', 'sds', 'Armenia', '', 0, 0, '0', '0', '', 91, '', 0),
(14, '', 142, '', 'Mohammed', 'Iqbal', 'Khan', '03/01/2021', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '', 91, '', 0),
(15, '', 143, 'Mr', 'student', 'student', 'dsdsd', 'Mar 15, 2021', 'male', 91, '88343434', '', NULL, '', NULL, '', NULL, NULL, 'dsd', 'fdfdf', 'dsd', 'dsd', 'sds', '', '', 0, 0, '0', '0', '', 91, '', 0),
(16, '', 144, '', 'Mohammed', 'Iqbal', 'Khan', '08/12/1992', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '', 91, '', 0),
(17, '', 147, 'Mr', 'dsd', 'dsd', 'dsds', 'Mar 28, 2021', 'male', 91, '88343434', '', NULL, '', NULL, 'http://scholab.herdzo.com/files/3nvsjcktfrdacjgsomxm/browser_logos-100734193-large.jpg?s=130', NULL, NULL, 'dsd', 'sdd', 'dsd', 'dsd', 'sds', '', '', 0, 0, '0', '0', '', 91, '', 0),
(18, '', 158, 'Mr', 'Sample', 'student', 'test', '08/12/1992', 'male', 91, '2147483647', 'AES1234AS', NULL, 'http://scholab.herdzo.com/files/lww68zpuhfcnpdx4oth5/avatar-img-01.jpg?s=130', NULL, 'http://scholab.herdzo.com/files/ujcgxs6pxu0aseakebbh/avatar-img-01.jpg?s=130', NULL, NULL, 'Sample Address', 'Sample Address', 'Sample Address', 'Sample Address', '90876', 'Botswana', 'http://scholab.herdzo.com/files/offbmuw2hnspxy8zfij6/avatar-img-01.jpg?s=130', 1, 9, '0', '0', '', 91, '', 0),
(19, '', 159, 'Mr', 'Sample', 'Scholab', 'Agent', 'Mar 22, 2021', 'male', 91, '2147483647', 'As1234567', NULL, '', NULL, '', NULL, NULL, 'sample.student@yopmail.com', 'sample.student@yopmail.com', 'sample.student@yopmail.com', 'sample.student@yopmail.com', 'sample.student@yopma', 'Argentina', 'http://scholab.herdzo.com/files/awr0r4voawigpzue09x4/student.png?s=130', 7, 0, '0', '0', '', 91, '', 0),
(20, '', 160, 'Mr', 'sas', 'sasa', 'asas', 'Mar 16, 2021', 'male', 91, '323323', 'ssqas', NULL, '', NULL, '', NULL, NULL, 'wewe', '323323', 'ewe', '32323', 'wew', 'Angola', '', 8, 8, '0', '0', '', 91, '', 0),
(21, '', 163, 'Mr', 'Mohammed', 'Iqbal', 'Khan', 'May 04, 1994', 'male', 91, '2147483647', 'SA123456', NULL, 'http://scholab.herdzo.com/files/8vimb9izl2gqhkpavfj4/Registeronline_net_ad.jpg?s=130', NULL, 'http://scholab.herdzo.com/files/cburk8yqwy30qhullawt/avatar-img-01.jpg?s=130', NULL, NULL, 'Test Address', 'Test Address', 'Test Address', 'Test Address', '987654', 'Armenia', 'http://scholab.herdzo.com/files/8bvfatqt8u6nhteztfi8/student.png?s=130', 6, 0, '0', '0', '', 91, '', 0),
(22, '', 164, '', 'Sample', 'Scholab', 'Students', '03/03/1962', 'male', 91, '2147483647', 'SR1234567', NULL, 'http://scholab.herdzo.com/files/sqo8lmyz025phqxah4uu/TSD - gadgeon.pdf', NULL, 'http://scholab.herdzo.com/files/pezpx59jeqjhqjbrxqim/Registeronline_net_ad.jpg?s=130', NULL, NULL, 'Test Address', 'Test Address', 'Test Address', 'Test Address', '987654', '', 'http://scholab.herdzo.com/files/dnjmtaqy0sfwuds2yu2n/student.png?s=130', 2, 5, '0', '0', '', 91, '', 0),
(23, '', 165, 'Dr.', 'ajeesh', 'bin', 'abdullah', 'Mar 10, 2021', 'male', 91, '2147483647', '', NULL, '', NULL, '', NULL, NULL, 'MANNAROTTIL HOUSE PALLIKKUNNU PO, PALAKKAD,KERALA INDIA-678583', 'MANNAROTTIL HOUSE PALLIKKUNNU PO, PALAKKAD,KERALA INDIA-678583', 'Palakkad', 'Kerala', '678583', '', '', 0, 0, '0', '0', '', 91, '', 0),
(24, '', 166, 'Mr', 'Iqbal ', 'KHan', 'S', 'Apr 02, 2021', 'male', 91, '2147483647', 'As1234567', NULL, '', NULL, 'http://scholab.herdzo.com/files/avzt5rcp7unkyjopzoih/Age group-graph.png?s=130', NULL, NULL, 'Test Address', 'Test Address', 'Test Address', 'Test Address', '987654', 'Angola', 'http://scholab.herdzo.com/files/qethpvu4mybksxpxld5g/student.png?s=130', 6, 2, '0', '0', '', 91, '', 0),
(25, '', 170, 'Mr', 'Ajeesh', 'bin', 'abdullah', 'Jan 10, 2021', 'male', 91, '2147483647', '123456789', NULL, '', NULL, 'http://scholab.herdzo.com/files/u37ykgvp5a6zsddixvay/brooke-cagle-_wYQnEAMAQk-unsplash.jpg?s=130', NULL, NULL, 'MANNAROTTIL HOUSE PALLIKKUNNU PO, PALAKKAD,KERALA INDIA-678583', 'MANNAROTTIL HOUSE PALLIKKUNNU PO, PALAKKAD,KERALA INDIA-678583', 'Palakkad', 'Kerala', '678583', 'Andorra', '', 10, 9, '0', '0', '', 91, '', 0),
(26, '', 173, 'Mr', 'dsd', 'dsds', 'dsd', 'Apr 19, 2021', 'male', 91, '88343434', '', NULL, NULL, NULL, NULL, NULL, NULL, 'dsd', 'dsdsds', 'dsd', 'dsd', 'sds', '', '', 8, 8, '0', '0', '', 91, '', 0),
(27, '', 176, 'Mr', 'Anjali', 'R', 'Vijay', '', 'female', 91, '2147483647', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Varkala', 'Kerala', NULL, NULL, NULL, NULL, NULL, '2147483647', '25', 'Trivandrum', 91, '', 0),
(28, '', 193, 'Mr', 'Advik', 'R', 'Raj', 'Jan 13, 1994', 'male', 91, '9656774188', '5678/567/678', NULL, 'http://localhost/maxglobal/files/l7cgk88nsbyqyfg608b9/ev-img02.jpg?s=130', NULL, 'http://localhost/maxglobal/files/6nmum2vl38hf0uftgrzn/ev-img01.jpg?s=130', NULL, NULL, 'Street 555', 'Street 777', 'Attingal', 'Kerala', '695678', 'India', 'http://localhost/maxglobal/files/ek6oaazbf3fqn49skpev/avatar-img.jpg?s=130', 5, 4, '', '', '', 91, '', 0),
(29, '', 196, 'Mr', 'Manu', 'M', 'Menon', 'Jun 25, 1994', 'male', 91, '9656774188', '5678/567/679', NULL, 'http://localhost/maxglobal/files/ipxv4zketknhozvcxnjy/ev-img02.jpg?s=130', NULL, 'http://localhost/maxglobal/files/9fprmlkqitm4nrclcyov/ev-img01.jpg?s=130', NULL, NULL, 'Street 555', 'Street 777', 'Attingal', 'Kerala', '695678', 'India', 'http://localhost/maxglobal/files/zgr5do5r2gt4prcxbfm9/avatar-img.jpg?s=130', 8, 5, '', '', '', 91, '', 0),
(30, '', 206, 'Mr', 'Karan', 'R', 'Raj', 'May 12, 1993', 'male', 91, '9656774188', '', NULL, '', NULL, '', NULL, NULL, 'Street 555', 'Street 777', 'Attingal', 'Kerala', '695678', 'India', 'http://localhost/maxglobal/files/n6tggshbkgov0gn7m07v/avatar-img.jpg?s=130', 10, 6, '', '', '', 91, '', 0),
(31, '', 214, 'Ms.', 'Ardra', 'R', 'Raj', 'Wed 10, 1992', 'female', 91, '9656774188', '5678/567/678', NULL, 'http://localhost/maxglobal/files/9dulpm0xncmr81sqteam/ev-img02.jpg?s=130', NULL, 'http://localhost/maxglobal/files/r5p55ar4tyinvbbhfy5t/ev-img01.jpg?s=130', NULL, NULL, 'Street ABC', 'St 234', 'Attingal', 'Kerala', '678567', 'India', 'http://localhost/maxglobal/files/4si0bztcdify51jsuheu/avatar-img.jpg?s=130', 4, 6, '9447034555', '29', '', 91, '', 1),
(32, '', 215, '', 'Keerthi', NULL, NULL, 'Fri 03, 1992', '', NULL, '9656774188', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Attingal', NULL, NULL, NULL, NULL, NULL, NULL, '9447034555', '29', '', 91, '', 0),
(33, '', 222, 'Ms.', 'Aswathy', 'M', 'Nair', 'Jun 12, 1993', 'female', 91, '9656774188', '5678/567/678', NULL, 'http://localhost/maxglobal/files/ffx7mxy2an0zwzzc4nwy/ev-img02.jpg?s=130,http://localhost/maxglobal/', NULL, 'http://localhost/maxglobal/files/iljapukohcmjhqfn2u1d/ev-img01.jpg?s=130,http://localhost/maxglobal/', NULL, NULL, 'Street 555', 'Street 777', 'Attingal', 'Kerala', '695678', 'India', 'http://localhost/maxglobal/files/luxdpgfwiwtc14wjlmxa/avatar-img.jpg?s=130,http://localhost/maxgloba', 4, 6, '9447034555', '29', '', 91, '', 1),
(34, '', 224, 'Miss.', 'Anagha', 'J', 'Prakash', 'Aug 06, 1992', '', 91, '9656774188', '', NULL, 'http://localhost/maxglobal/files/ohjej2aftwyj5yqmbvco/ev-img03.jpg?s=130', NULL, 'http://localhost/maxglobal/files/2tvwue1jfa5nspyieire/ev-img01.jpg?s=130', NULL, NULL, 'zsfgg', 'dggh', 'Attingal', 'fgdhdh', '786667', '', 'http://localhost/maxglobal/files/re28oyowcj6oznmri7ec/avatar-img.jpg?s=130', NULL, NULL, '9447034555', '29', '', 91, '', 0),
(35, '', 225, 'Mr', 'Aneesh', 'M', 'Nair', 'Apr 10, 1995', 'male', 91, '9656774188', '5678/567/678', NULL, '', NULL, '', NULL, NULL, 'dxgdsgs', 'sdgsgsg', 'Attingal', 'dgsgsgs', '6758585', 'India', 'http://localhost/maxglobal/files/buetyaalwpxbmtnvmfsk/avatar-img.jpg?s=130', 8, 6, '9447034555', '27', '', 91, '', 0),
(36, '', 226, 'Ms.', 'Anitha', 'J', 'Vijay', 'Apr 06, 1990', 'female', 91, '9656774188', '5678/567/679', NULL, 'http://localhost/maxglobal/files/cvkbbjlzgld7gvb7suoq/ev-img02.jpg?s=130', NULL, 'http://localhost/maxglobal/files/hhccxnxf94awbzgecm26/ev-img01.jpg?s=130', NULL, NULL, 'St465636', 'St676767', 'Attingal', 'Kerala', '695678', 'India', 'http://localhost/maxglobal/files/qalqj5bbjpvrlbtktc4f/avatar-img.jpg?s=130', 4, 7, '9447034555', '32', '', 91, '', 0),
(37, '', 227, 'Mr', 'Rihan', 'T', 'Khan', 'Mar 11, 1994', 'male', 91, '9656774188', '5678/567/678', NULL, 'http://localhost/maxglobal/files/mcoxgfsbkgqzcuznbpcu/flower-img.jpg?s=130', NULL, 'http://localhost/maxglobal/files/nww4jnuybm39uxl4l8bk/ev-img03.jpg?s=130', NULL, NULL, 'St54747', 'St43452', 'Attingal', 'Kerala', '695678', 'India', 'http://localhost/maxglobal/files/ve7tbbjojmbiiv3ictua/avatar-img.jpg?s=130', 7, 5, '9447034555', '28', '', 91, '', 0),
(38, '', 228, 'Mr', 'Abhilash', 'M', 'Nair', 'Mar 10, 1988', 'male', 91, '9656774188', '5678/567/678', NULL, 'http://localhost/maxglobal/files/bol6iob3jewdqlnsyuto/ev-img02.jpg?s=130', NULL, 'http://localhost/maxglobal/files/ndtf5mu6x1ho0wpc0dlp/ev-img01.jpg?s=130', NULL, NULL, 'St53535', 'St6876', 'Attingal', 'Kerala', '695678', 'India', 'http://localhost/maxglobal/files/hedic6a7galawq0qfdms/avatar-img.jpg?s=130', 5, 3, '9447034555', '34', '', 91, '', 0),
(39, '', 229, 'Mr', 'Rahul', 'R', 'Roy', 'May 09, 1986', 'male', 91, '9656774188', '', NULL, '', NULL, '', NULL, NULL, 'St345336', 'St678859', 'Attingal', 'Kerala', '695678', 'Canada', 'http://localhost/maxglobal/files/ke18n3h9m2afpahhkntq/avatar-img.jpg?s=130', 0, 0, '9447034555', '36', '', 91, '', 1),
(40, '', 230, 'Ms.', 'Dhanya', 'R', 'Prakash', 'Mar 07, 1985', 'female', 91, '9656774188', '', NULL, '', NULL, '', NULL, NULL, 'St57474', 'St6786', 'Attingal', 'Kerala', '695678', '', 'http://localhost/maxglobal/files/7bneysjdrl4rmokkb6ys/avatar-img.jpg?s=130', 0, 0, '9447034554', '37', '', 91, '', 0),
(41, '', 231, 'Mrs.', 'Shivani', 'M', 'Nair', 'Feb 15, 1985', 'female', 91, '9656774188', '', NULL, '', NULL, '', NULL, NULL, 'St3525', 'St6878', 'Attingal', 'Kerala', '695678', '', 'http://localhost/maxglobal/files/csydccifzjduxqnngufz/avatar-img.jpg?s=130', 0, 0, '9447034555', '37', '', 91, '', 0),
(42, '', 232, '', 'Shiyas A', '', '', 'Apr 11, 1985', '', 971, '9656774189', '', NULL, 'http://localhost/maxglobal/files/uwxwwbrjzc8olujn7axz/flower-img.jpg?s=130,http://localhost/maxglobal/files/hgcrbpilx4u3e9pxiozo/ev-img02.jpg?s=130', NULL, 'http://localhost/maxglobal/files/8jf4rvhmm5klywzxxv5p/ev-img03.jpg?s=130,http://localhost/maxglobal/files/z7lg3tpceup2ww0hvzud/ev-img01.jpg?s=130', NULL, NULL, 'dsfsgsg', 'sdgsg', 'sdgsgs', 'fbcgnfh', '675858', '', 'http://localhost/maxglobal/files/7tq8ioyf07exctbclef9/avatar-img.jpg?s=130', NULL, NULL, '9447034556', '', '', 966, '', 0),
(43, '', 244, '', 'Deepak', '', '', 'May 07, 1991', '', 966, '8905678456', '', NULL, '', NULL, '', NULL, NULL, '', '', 'Tvm', '', '', '', '', NULL, NULL, '8907896785', '31', '', 91, '', 0),
(44, '', 251, '', 'Hareesh', '', '', 'Mar 07, 1990', '', 91, '9656774188', '', NULL, '', NULL, '', NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, '9447034555', '32', '', 91, '', 0),
(45, '', 260, 'Ms.', 'Sitara', 'R', 'Nair', 'Sep 03, 1993', 'female', 91, '9656774188', '', NULL, 'http://localhost/maxglobal/files/jawuqeqchylerrhssaqf/ev-img02.jpg?s=130', NULL, 'http://localhost/maxglobal/files/5fjghiamfnpf2rx7cqcj/ev-img01.jpg?s=130', NULL, NULL, '', '', '', '', '', '', 'http://localhost/maxglobal/files/qmu9wt9ohhfzc3kkz6ya/avatar-img.jpg?s=130', 0, 0, '9447034555', '29', '', 91, '', 1),
(46, '', 261, 'Mr', 'Vijay', '', '', 'Aug 19, 1993', 'male', 91, '9656774188', '', NULL, '', NULL, '', NULL, NULL, '', '', '', '', '', '', 'http://localhost/maxglobal/files/xmnckqzbu8tewcqxo3on/avatar-img.jpg?s=130', NULL, NULL, '9447034555', '29', '', 91, '', 1),
(47, '', 262, 'Mr', 'Amal', '', '', '', 'male', 91, '9656774188', '', NULL, '', NULL, '', NULL, NULL, '', '', '', '', '', '', '', 0, 0, '9447034553', '', '', 91, '', 1),
(48, '', 266, '', 'Sarath', '', '', 'Jul 12, 1995', '', 91, '9656774188', '', NULL, '', NULL, '', NULL, NULL, '', '', 'Attingal', '', '', '', 'http://localhost/maxglobal/files/jmxv2qox3gz0r4aaqwny/avatar-img.jpg?s=130', NULL, NULL, '9447034555', '27', '', 91, '', 0),
(50, '', 271, '', 'Syam', '', '', 'Aug 11, 1989', '', 91, '9656774188', '', NULL, '', NULL, '', NULL, NULL, '', '', 'Attingal', '', '', '', '', NULL, NULL, '9447034555', '33', '', 91, '', 1),
(51, '', 273, '', 'Revathy', '', '', 'Jul 01, 1994', '', 91, '9656774188', '', NULL, '', NULL, '', NULL, NULL, '', '', 'Attingal', '', '', '', '', NULL, NULL, '9447034555', '28', '', 91, '', 0),
(52, '', 274, '', 'Riyas', NULL, NULL, 'Jun 15, 1993', '', 91, '9656774188', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Attingal', NULL, NULL, NULL, NULL, NULL, NULL, '9447034555', '29', '', 91, '', 0),
(53, '', 277, '', 'Kanika', NULL, NULL, 'Jun 14, 1990', '', 91, '9656774188', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Attingal', NULL, NULL, NULL, NULL, NULL, NULL, '7896785674', '32', '', 91, '', 0),
(54, '', 278, '', 'Rejin', NULL, NULL, 'May 15, 1996', '', 91, '9656774188', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Attingal', NULL, NULL, NULL, NULL, NULL, NULL, '7878787878', '26', '', 91, '', 0),
(55, '', 279, '', 'Ajmal', '', '', 'Mar 10, 1994', '', 91, '9656774188', '', NULL, '', NULL, '', NULL, NULL, '', '', 'Attingal', '', '', '', '', NULL, NULL, '7897897897', '28', '', 91, '', 0),
(56, '', 280, '', 'Anandhu', '', '', 'Feb 08, 1990', '', 91, '9656774188', '', NULL, '', NULL, '', NULL, NULL, '', '', 'Attingal', '', '', '', '', NULL, NULL, '6786786789', '32', '', 91, '', 0),
(57, '', 290, '', 'Parvathy', NULL, NULL, 'Nov 13, 1991', '', 91, '9656774188', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Attingal', NULL, NULL, NULL, NULL, NULL, NULL, '7890678956', '30', '', 91, '', 0),
(58, '', 291, '', 'Krithi', NULL, NULL, 'Sep 15, 1993', '', 91, '9656774188', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Attingal', NULL, NULL, NULL, NULL, NULL, NULL, '7890789078', '29', '', 91, '', 0),
(59, '', 292, '', 'Sreeja', NULL, NULL, 'Aug 23, 1989', '', 91, '9656774188', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Attingal', NULL, NULL, NULL, NULL, NULL, NULL, '6789078908', '33', '', 91, '', 0),
(60, '', 298, 'Mr', 'Adarsh', 'S', 'Nair', 'Oct 19, 1988', 'male', 91, '9656774188', '', NULL, '', NULL, '', NULL, NULL, 'Street 555', 'Street 777', 'Attingal', 'Kerala', '695678', '', 'http://localhost/maxglobal/files/vwjjzm0mfxwpasixxcus/avatar-img.jpg?s=130', 0, 0, '9447034555', '34', '', 91, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_personal_stat`
--

CREATE TABLE `data_personal_stat` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `certificates` tinyint(1) NOT NULL DEFAULT 0,
  `eng_qualification` tinyint(1) NOT NULL DEFAULT 0,
  `resume` tinyint(1) NOT NULL DEFAULT 0,
  `passport` tinyint(1) NOT NULL DEFAULT 0,
  `reference` tinyint(1) NOT NULL DEFAULT 0,
  `statement` tinyint(1) NOT NULL DEFAULT 0,
  `imigration` tinyint(1) NOT NULL DEFAULT 0,
  `skype_profile` tinyint(1) NOT NULL DEFAULT 0,
  `tb_screenshot` tinyint(1) NOT NULL DEFAULT 0,
  `fund_proof` tinyint(1) NOT NULL DEFAULT 0,
  `exp_cert` tinyint(1) NOT NULL DEFAULT 0,
  `personal_statement` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_personal_stat`
--

INSERT INTO `data_personal_stat` (`id`, `user_id`, `certificates`, `eng_qualification`, `resume`, `passport`, `reference`, `statement`, `imigration`, `skype_profile`, `tb_screenshot`, `fund_proof`, `exp_cert`, `personal_statement`) VALUES
(1, 93, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'mmmmvcjcmncxqw\r\ncdwjcjnbcnjdsxnmcn\r\ndcjcmnvjbnvj \r\nfkvkcknv mnc \r\n\r\n\r\ncv ncv\r\nnvfnsnfmfvdhhdfvfncnncncncm\r\nhbghgjhnnnm\r\n\r\nnxdfd'),
(2, 96, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Personal statementPersonal statement Personal statement Per\r\nPersonal statement\r\nPersonal statement Personal statement Personal statement Personal statement\r\nPersonal statement Personal statement Personal statement Personal statement Personal statement Personal statementPersonal statement Personal statement Personal statement Personal statementPersonal statement Personal statement Personal statement Personal statementPersonal statement Personal statement Personal statement Personal statement Person'),
(3, 113, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, ' Course at this university and hope to do in future career '),
(4, 134, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, ''),
(5, 140, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(6, 158, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(7, 170, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, ''),
(8, 174, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 'Personal'),
(9, 193, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, ''),
(10, 196, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 'My personal stmt'),
(11, 232, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `data_school`
--

CREATE TABLE `data_school` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `board_name` varchar(100) DEFAULT NULL,
  `country_sslc` varchar(30) DEFAULT NULL,
  `start_year` varchar(25) NOT NULL,
  `end_year` varchar(25) NOT NULL,
  `mode_study_school` varchar(10) NOT NULL,
  `qualification` varchar(100) DEFAULT NULL,
  `grade` varchar(15) NOT NULL,
  `medium_of_instr` varchar(30) DEFAULT NULL,
  `eng_score` varchar(4) NOT NULL,
  `document` text DEFAULT NULL,
  `document_approved` tinyint(1) DEFAULT NULL,
  `hse_document_approved` tinyint(1) DEFAULT NULL,
  `UG_document_approved` tinyint(1) DEFAULT NULL,
  `PG_document_approved` tinyint(1) DEFAULT NULL,
  `PG_document` text DEFAULT NULL,
  `PG_eng_score` varchar(4) DEFAULT NULL,
  `PG_medium_of_instr` varchar(30) DEFAULT NULL,
  `PG_grade` varchar(15) DEFAULT NULL,
  `PG_qualification` int(11) DEFAULT NULL,
  `PG_mode_study` varchar(10) DEFAULT NULL,
  `PG_end_year` varchar(25) NOT NULL,
  `PG_start_year` varchar(25) NOT NULL,
  `PG_country` varchar(30) DEFAULT NULL,
  `PG_board_name` varchar(100) DEFAULT NULL,
  `UG_document` text DEFAULT NULL,
  `UG_eng_score` varchar(4) DEFAULT NULL,
  `UG_medium_of_instr` varchar(30) DEFAULT NULL,
  `UG_grade` varchar(15) DEFAULT NULL,
  `UG_qualification` int(11) DEFAULT NULL,
  `UG_mode_study` varchar(10) DEFAULT NULL,
  `UG_end_year` varchar(25) NOT NULL,
  `UG_start_year` varchar(25) NOT NULL,
  `UG_country` varchar(30) DEFAULT NULL,
  `UG_board_name` varchar(100) DEFAULT NULL,
  `hse_document` text DEFAULT NULL,
  `hse_eng_score` varchar(4) DEFAULT NULL,
  `hse_medium_of_instr` varchar(30) DEFAULT NULL,
  `hse_grade` varchar(15) DEFAULT NULL,
  `hse_qualification` int(11) DEFAULT NULL,
  `hse_mode_study` varchar(10) DEFAULT NULL,
  `hse_end_year` varchar(25) NOT NULL,
  `hse_start_year` varchar(25) NOT NULL,
  `hse_country` varchar(30) DEFAULT NULL,
  `hse_board_name` varchar(100) DEFAULT NULL,
  `stream` varchar(50) NOT NULL,
  `ug_university` varchar(70) NOT NULL,
  `pg_university` varchar(70) NOT NULL,
  `UG_backlogs` varchar(3) NOT NULL,
  `PG_backlogs` varchar(3) NOT NULL,
  `json_data` text DEFAULT NULL,
  `institute_name` varchar(100) NOT NULL,
  `hse_institute_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_school`
--

INSERT INTO `data_school` (`id`, `user_id`, `board_name`, `country_sslc`, `start_year`, `end_year`, `mode_study_school`, `qualification`, `grade`, `medium_of_instr`, `eng_score`, `document`, `document_approved`, `hse_document_approved`, `UG_document_approved`, `PG_document_approved`, `PG_document`, `PG_eng_score`, `PG_medium_of_instr`, `PG_grade`, `PG_qualification`, `PG_mode_study`, `PG_end_year`, `PG_start_year`, `PG_country`, `PG_board_name`, `UG_document`, `UG_eng_score`, `UG_medium_of_instr`, `UG_grade`, `UG_qualification`, `UG_mode_study`, `UG_end_year`, `UG_start_year`, `UG_country`, `UG_board_name`, `hse_document`, `hse_eng_score`, `hse_medium_of_instr`, `hse_grade`, `hse_qualification`, `hse_mode_study`, `hse_end_year`, `hse_start_year`, `hse_country`, `hse_board_name`, `stream`, `ug_university`, `pg_university`, `UG_backlogs`, `PG_backlogs`, `json_data`, `institute_name`, `hse_institute_name`) VALUES
(1, 91, 'Kerala State', 'India', 'Jun 23, 2020', 'Jun 10, 2020', 'Full-time', 'B.E', '70%', 'English', '75', 'http://maxglobal.herdzo.com/files/yvo3jcfkqkshtoqs21ex/Nurse_Profile_Fields.docx', NULL, NULL, NULL, NULL, 'http://maxglobal.herdzo.com/fi', '66', NULL, '75', 0, 'Full-time', '', 'Jun 28, 2012', 'India', 'CMS', 'http://maxglobal.herdzo.com/fi', '75', NULL, '75', 0, 'Full-time', 'Jun 24, 2004', 'Jun 16, 2005', 'India', 'CMS', 'http://maxglobal.herdzo.com/fi', '75', NULL, '75%', 0, 'Distance-l', 'Jun 17, 2004', 'Jun 23, 2020', 'India', 'SHHSS', '', '', '', '', '', NULL, '', ''),
(2, 93, 'SHSS', 'Algeria', 'Jun 30, 2020', 'Jun 26, 2020', 'Part-time', 'SSLC', '', '', '68', 'http://maxglobal.herdzo.com/files/wv8efjxniykjmlaxh7qk/MaxGlobalConsultants_website_Final_doc (1).pdf', NULL, NULL, NULL, NULL, 'http://maxglobal.herdzo.com/fi', '68', NULL, '', 0, 'Part-time', 'Jun 26, 2020', 'Jun 30, 2020', 'India', 'TIEC', 'http://maxglobal.herdzo.com/fi', '68', NULL, '', 0, 'Part-time', 'Jun 26, 2020', 'Jun 29, 2020', 'India', 'TIEC', 'http://maxglobal.herdzo.com/fi', '68', NULL, '', 2, 'Part-time', 'Jun 26, 2020', 'Jun 24, 2020', 'India', 'GHSSS', '', '', '', '', '', NULL, '', ''),
(3, 95, '', '', '', '', 'Full-time', '', '3', '3343434', '', 'http://maxglobal.herdzo.com/files/kvbp6hqr6wdimqese66w/receipt (1).pdf,http://maxglobal.herdzo.com/files/m9z3zv7iezrzsa8rtv98/receipt (1).pdf', NULL, NULL, NULL, NULL, '', '', NULL, '', 0, 'Full-time', '', '', '', '', '', '', NULL, '', 0, 'Full-time', '', '', '', '', '', '', NULL, '', 0, 'Full-time', '', '', '', '', '', '', '', '', '', NULL, '', ''),
(4, 134, 'ewew', 'Albania', 'Mar 16, 2021', 'Mar 26, 2021', 'Full-time', '', '', '', '', 'http://maxglobal.herdzo.com/files/ipujum3tkjvhxhtmhgbj/Chrysanthemum.jpg?s=130,http://maxglobal.herdzo.com/files/tifwkmtxpdhz3utyssrn/Koala.jpg?s=130,http://maxglobal.herdzo.com/files/d2jfyst4h94qjvo8gigz/Lighthouse.jpg?s=130', NULL, NULL, NULL, NULL, '', '', '', '', 0, 'Full-time', '', '', '', '', '', '', '', '', 0, 'Full-time', '', '', '', '', '', '', '', '', 0, 'Full-time', '', '', '', '', '', '', '', '', '', NULL, '', ''),
(5, 141, '', '', '', '', 'Full-time', '', '', '', '', '', NULL, NULL, NULL, NULL, '', '', '', '', 0, 'Full-time', '', '', '', '', '', '', '', '', 0, 'Full-time', '', '', '', '', '', '', '', '', 0, 'Full-time', '', '', '', '', '', '', '', '', '', NULL, '', ''),
(6, 142, '', '', '', '', 'Full-time', '', '', '', '', 'http://maxglobal.herdzo.com/files/alfoducpiegstufgzaef/Penguins.jpg?s=130', NULL, NULL, NULL, NULL, '', '', '', '', 0, 'Full-time', '', '', '', '', '', '', '', '', 0, 'Full-time', '', '', '', '', 'http://maxglobal.herdzo.com/fi', '', '', '', 0, 'Full-time', '', '', '', '', '', '', '', '', '', NULL, '', ''),
(7, 158, 'Sample Scool', 'Antigua and Barbuda', 'Mar 01, 2021', 'Mar 02, 2021', 'Full-time', 'SSLC', '100', 'English', '100', 'http://scholab.herdzo.com/files/mkkrlsbvmn81qxakcehr/avatar-img-01.jpg?s=130', NULL, NULL, NULL, NULL, '', '', '', '', 0, 'Full-time', '', '', '', '', '', '', '', '', 0, 'Full-time', '', '', '', '', 'http://scholab.herdzo.com/file', '', 'Malayalam', 'A', 12, 'Full-time', 'Mar 04, 2021', 'Mar 22, 2021', 'Australia', 'Sample', '', '', '', '', '', NULL, '', ''),
(8, 174, 'SSNSS', 'India', 'Jun 03, 2005', 'Mar 31, 2006', 'Full-time', '10th', '85.25', 'English', '90.5', 'http://localhost/maxglobal/files/qtxalv6ss2fwtbun55qz/A Sample PDF.pdf,http://localhost/maxglobal/files/ifur81ckttl8c52u5efk/flower-img.jpg?s=130,http://localhost/maxglobal/files/toil2tnpatrgqymjhfb4/Get_Started_With_Smallpdf.pdf,http://localhost/maxglobal/files/0yog9q1xj4cy155x2qom/Get_Started_With_Smallpdf.pdf,http://localhost/maxglobal/files/vq32srmq84ln9npc4ycc/Get_Started_With_Smallpdf.pdf', NULL, NULL, NULL, NULL, 'http://localhost/maxglobal/files/yqcddmjlxglq81qz3zzn/Get_Started_With_Smallpdf.pdf,http://localhost/maxglobal/files/sjsx2zmxshkx53kbbspq/A Sample PDF.pdf', '90.7', 'English', '70.56', 0, 'Full-time', 'Jul 24, 2014', 'Sep 01, 2011', 'India', 'CHN', 'http://localhost/maxglobal/files/rrplg8coyri32lvyyqgs/Get_Started_With_Smallpdf.pdf,http://localhost/maxglobal/files/frrezpftwa8yn17x6j2d/Get_Started_With_Smallpdf.pdf', '88.5', 'English', '86.70', 0, 'Full-time', 'Aug 31, 2011', 'Aug 04, 2008', 'India', 'SNC', 'http://localhost/maxglobal/files/osai9dcm7uixj0c2pncu/Get_Started_With_Smallpdf.pdf,http://localhost/maxglobal/files/lop24muwn3sz34sficwd/A Sample PDF.pdf', '92.4', 'English', '87.65', 12, 'Full-time', 'Mar 31, 2008', 'Jun 04, 2007', 'India', 'MGM', '', '', '', '', '', NULL, '', ''),
(9, 196, '', '', '', '', 'Full-time', '', '', '', '', 'http://localhost/maxglobal/files/y2f6ho7tudkq7sjrnv3s/A Sample PDF.pdf,http://localhost/maxglobal/files/zlxkoouqibipppupl64w/Get_Started_With_Smallpdf.pdf', NULL, NULL, NULL, NULL, 'http://localhost/maxglobal/files/fa9hzndyb70qg6tnwbun/A Sample PDF.pdf,http://localhost/maxglobal/files/6u2hk44cdwgfzfars3yt/Get_Started_With_Smallpdf.pdf', '', '', '', 0, 'Full-time', '', '', '', '', 'http://localhost/maxglobal/files/eprcl6wfgzflrh7soqvm/A Sample PDF.pdf,http://localhost/maxglobal/files/zdzxmgoizdryzmv5q0sx/Get_Started_With_Smallpdf.pdf', '', '', '', 0, 'Full-time', '', '', '', '', 'http://localhost/maxglobal/files/rwld0oqyprnmou07vull/A Sample PDF.pdf,http://localhost/maxglobal/files/nafvmwxp7jmf605tdivi/Get_Started_With_Smallpdf.pdf', '', '', '', 0, 'Full-time', '', '', '', '', '', '', '', '', '', NULL, '', ''),
(10, 222, 'ICSE', 'India', '2005', '2006', 'Full-time', '10th', '80', 'English', '85', 'http://localhost/maxglobal/files/ee2b3mgvxndmvw7awcg5/A Sample PDF.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, 'http://localhost/maxglobal/files/vj45yeqmx0va8mzfdckn/A Sample PDF.pdf', NULL, NULL, '75', NULL, 'Distance', '2011', '', NULL, 'MBA', 'http://localhost/maxglobal/files/4fwtel33gs4qsvtnba41/A Sample PDF.pdf', '95', 'Hindi', '90', 12, 'Part-time', '2008', '2007', 'Canada', 'CBSE', 'Computer', '(INTO) University of Manchester', '', '1', '', '[{\"UG_board_name\":\"MCA\",\"ug_university\":\"(INTO) University of East of Anglia\",\"UG_grade\":\"60\",\"UG_end_year\":\"2011\",\"UG_backlogs\":\"1\",\"UG_mode_study\":\"Full-time\",\"UG_document\":\"http:\\/\\/localhost\\/maxglobal\\/files\\/muflxe3isnbwtihze0zf\\/A Sample PDF.pdf\"},{\"UG_board_name\":\"MBA\",\"ug_university\":\"(KAPLAN) University of Nottingham International College\",\"UG_grade\":\"70\",\"UG_end_year\":\"2014\",\"UG_backlogs\":\"2\",\"UG_mode_study\":\"Distance\",\"UG_document\":\"http:\\/\\/localhost\\/maxglobal\\/files\\/lwcc9vp7s9qyseutvp8x\\/A Sample PDF.pdf\"}]', 'SSNSS', 'SSN'),
(11, 232, '', '', '', '2002', '', '', '81', '', '', 'http://localhost/maxglobal/files/2vz4vwfx3h80lfycdjtl/A Sample PDF.pdf,http://localhost/maxglobal/files/a0mxi6cn5hk7mmkvdx98/dummy.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, 'http://localhost/maxglobal/files/wybk0fhltun3ke2ry0ea/A Sample PDF.pdf,http://localhost/maxglobal/files/pvuboo6jorhmmk6oom66/dummy.pdf', NULL, NULL, '85', NULL, '', '2008', '', NULL, '', 'http://localhost/maxglobal/files/dx91vs4cdz2wdgnro9rs/A Sample PDF.pdf,http://localhost/maxglobal/files/kghr80tppqcr2mllusgu/pdf-sample.pdf', '', '', '83', 0, '', '2004', '', '', '', '', '(INTO) University of Manchester', '', '', '', '', '', ''),
(12, 224, '', '', '', '', '', '', '', '', '', 'http://localhost/maxglobal/files/anjnyqyovzb0voekxxol/A Sample PDF.pdf,http://localhost/maxglobal/files/mwpbkb6mrtcl3d9axwgt/dummy.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, 'http://localhost/maxglobal/files/dct2veeozg92fwvqfmv9/pdf-sample.pdf,http://localhost/maxglobal/files/xtok3itoiyhqt2rxjl75/dummy.pdf,http://localhost/maxglobal/files/obvdjbbumlmyoij1ngkm/pdf-sample.pdf,http://localhost/maxglobal/files/qxyhg3bic2q5xng3abww/A Sample PDF.pdf', NULL, NULL, '', NULL, '', '', '', NULL, '', 'http://localhost/maxglobal/files/dwmekhy3n2eybwpcu51a/dummy.pdf', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '[{\"UG_board_name\":\"\",\"ug_university\":\"\",\"UG_grade\":\"\",\"UG_end_year\":\"\",\"UG_backlogs\":\"\",\"UG_mode_study\":\"\",\"UG_document\":\"http:\\/\\/localhost\\/maxglobal\\/files\\/yhjcw89jzujde4opgdqr\\/A Sample PDF.pdf\"}]', '', ''),
(13, 261, NULL, NULL, '', '2013', '', NULL, '81', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '85', NULL, NULL, '2018', '', NULL, NULL, NULL, NULL, NULL, '83', NULL, NULL, '2015', '', NULL, NULL, '', '(INTO) University of East of Anglia', '', '', '', NULL, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `data_student_lead`
--

CREATE TABLE `data_student_lead` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lead_type` varchar(50) DEFAULT NULL,
  `lead_from` varchar(50) DEFAULT NULL,
  `agent` varchar(100) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_student_lead`
--

INSERT INTO `data_student_lead` (`id`, `user_id`, `lead_type`, `lead_from`, `agent`, `agent_id`) VALUES
(1, 222, 'Agent', 'Agents', 'Sneha', 176),
(2, 214, 'Agents', 'Instagram', '', 0),
(3, 224, 'Agents', 'Agents', 'Kiran', 205),
(4, 225, 'Agents', 'Agents', 'Anoop', 202),
(5, 193, 'Agent', 'Agents', 'Sneha', 176),
(6, 232, 'Agents', 'Agents', 'Sneha', 176),
(7, 260, 'Agent', 'Agents', 'Praveen', 181),
(8, 261, 'Agents', 'Agents', 'Sneha', 176),
(9, 266, 'Agents', 'Agents', 'Nithin', 236);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tags` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '[]',
  `color` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `tags`, `color`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Administration', '[]', '#000', NULL, '2019-11-08 12:05:03', '2019-11-08 12:05:03');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `designation` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Male',
  `mobile` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mobile2` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dept` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `about` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_birth` date NOT NULL DEFAULT '1990-01-01',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `type_user` tinyint(1) NOT NULL DEFAULT 1,
  `nurse-user` tinyint(1) NOT NULL DEFAULT 0,
  `profilePic` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enable-disable` tinyint(1) NOT NULL DEFAULT 1,
  `refer_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `designation`, `gender`, `mobile`, `mobile2`, `email`, `dept`, `city`, `address`, `about`, `date_birth`, `deleted_at`, `created_at`, `updated_at`, `role`, `type_user`, `nurse-user`, `profilePic`, `enable-disable`, `refer_code`) VALUES
(1, 'Admin', 'Super Admin', 'Male', '8888888888', '8888888882', 'info@herdzo.com', 1, 'Pune', 'Karve nagar, Pune 411030', 'Lorem Ipsum', '2019-11-08', NULL, '2019-11-08 12:06:31', '2020-10-06 05:47:41', 1, 1, 1, '', 1, ''),
(96, 'Scholab Admin', '', 'Male', '9876543210', '', 'scholab.admin@yopmail.com', 1, 'Kollam', 'Test Address 1234567', 'Test', '1990-01-01', NULL, '2021-03-29 08:47:01', '2021-03-29 08:47:01', 2, 1, 0, '', 1, ''),
(99, 'Sample Staff', '', 'Male', '9876543210', '', 'staff.scholab@yopmail.com', 1, 'dsd', 'dsd\r\nsdd', 'Scholab is overseas solution', '1990-01-01', '2021-03-29 10:44:53', '2021-03-29 10:44:32', '2021-03-29 10:44:53', 3, 1, 0, 'http://scholab.herdzo.com/files/2ctbuqkd9uhybradhfpd/avatar-img-01.jpg?s=130', 1, ''),
(100, 'Sample Staff', '', 'Male', '9876543210', '', 'staff.scholab.1@yopmail.com', 1, 'tvm', 'test address', 'Scholab is overseas solution', '1990-10-04', '2022-01-19 07:58:30', '2021-03-29 11:11:37', '2022-01-19 07:58:30', 3, 1, 0, 'http://scholab.herdzo.com/files/5ths2ezhqcihdsrg5ctc/avatar-img-01.jpg?s=130', 1, ''),
(101, 'Councillor 1 ', '', 'Male', '09876543210', '9447034555', 'staff.scholab.2@yopmail.com', 1, 'Test Address', 'Test Address\r\nTest Address', 'asdsad', '1990-01-01', NULL, '2021-04-02 10:15:10', '2022-09-23 02:07:07', 3, 1, 0, '', 1, ''),
(102, 'Farshad', '', 'Male', '917593992525', '917593992525', 'doc@scholabedu.com', 1, 'Malappuram', '', '', '1992-01-01', NULL, '2021-04-07 12:06:38', '2021-04-07 12:06:38', 4, 1, 0, '', 1, ''),
(103, '', '', 'Male', '', '', 'abacreationz@gmail.com', 1, '', '', '', '1990-01-01', '2021-04-07 17:31:52', '2021-04-07 12:27:22', '2021-04-07 17:31:52', 4, 1, 0, NULL, 1, ''),
(104, 'qqqqqq', '', 'Male', '06282389008', '', 'axlcreationz@gmail.com', 1, 'Palakkad', 'MANNAROTTIL HOUSE PALLIKKUNNU PO, PALAKKAD,KERALA INDIA-678583\r\nMANNAROTTIL HOUSE PALLIKKUNNU PO, PALAKKAD,KERALA INDIA-678583', '', '1990-01-01', '2021-04-07 17:44:27', '2021-04-07 17:43:38', '2021-04-07 17:44:27', 3, 1, 0, '', 1, ''),
(105, 'qqqqq', '', 'Male', '06282389008', '', 'ajcreationz111@gmail.com', 1, 'Palakkad', 'MANNAROTTIL HOUSE PALLIKKUNNU PO, PALAKKAD,KERALA INDIA-678583\r\nMANNAROTTIL HOUSE PALLIKKUNNU PO, PALAKKAD,KERALA INDIA-678583', '', '1990-01-01', NULL, '2021-04-07 17:45:21', '2021-04-07 17:45:21', 3, 1, 0, '', 1, ''),
(106, 'Priyanka', '', 'Male', '9656774188', '9447034555', 'priyanka333@yopmail.com', 1, 'Attingal', 'Street 555', 'I am a staff', '1995-08-07', NULL, '2022-01-08 03:30:30', '2022-01-12 00:19:41', 3, 1, 0, 'http://localhost/maxglobal/files/aky0xmhipzuxzink2xax/avatar-img.jpg?s=130', 1, ''),
(107, 'Sneha', '', 'Male', '9656774188', '9447034555', 'sneha333@yopmail.com', 1, 'Attingal', 'Street 555', 'I am an agent', '1994-05-03', NULL, '2022-01-08 03:32:29', '2022-01-12 00:20:26', 4, 1, 0, 'http://localhost/maxglobal/files/8zrr1dudub0cjlx3efle/avatar-img.jpg?s=130', 1, ''),
(108, 'Akhil', '', 'Male', '9656774188', '9447034555', 'akhil333@yopmail.com', 1, 'Attingal', 'Street 555', 'I am a sub contractor', '1992-09-07', NULL, '2022-01-08 03:35:55', '2022-09-06 08:15:46', 6, 1, 0, 'http://localhost/maxglobal/files/bwzurss17qsvcowfojgw/avatar-img.jpg?s=130', 1, ''),
(109, 'Karthik', '', 'Male', '9656774188', '9447034555', 'karthik333@yopmail.com', 1, 'Attingal', 'Street 555', 'I am a university user', '1991-06-02', NULL, '2022-01-08 03:37:06', '2022-01-12 00:21:09', 5, 1, 0, 'http://localhost/maxglobal/files/bpdgek2rrpxo9lj20pdk/avatar-img.jpg?s=130', 1, ''),
(110, 'Kiran', '', 'Male', '9656774188', '9447034555', 'kiran333@yopmail.com', 1, 'Attingal', 'Street 555', 'I am an employer', '1994-07-03', NULL, '2022-01-08 03:48:25', '2022-01-12 00:21:56', 7, 1, 0, 'http://localhost/maxglobal/files/nzj2vgi7lrdzauaode0g/avatar-img.jpg?s=130', 1, ''),
(111, 'Adarsh', '', 'Male', '9656774188', '9447034555', 'adarsh333@yopmail.com', 1, 'Attingal', 'Street 555', 'I am a sub contractor', '1991-02-04', NULL, '2022-01-08 03:50:21', '2022-01-11 02:27:03', 6, 1, 0, 'http://localhost/maxglobal/files/a3n9pubj6ktu3sxasflo/avatar-img.jpg?s=130', 1, ''),
(112, 'Praveen', '', 'Male', '9656774188', '9447034555', 'praveen333@yopmail.com', 1, 'Attingal', 'Street 555', 'I am an agent', '1995-03-07', NULL, '2022-01-08 03:51:44', '2022-01-08 03:51:44', 4, 1, 0, 'http://localhost/maxglobal/files/sdbuqncxfgqnflbifiwq/avatar-img.jpg?s=130', 1, ''),
(113, 'Afsal', '', 'Male', '9656774188', '9447034555', 'afsal333@yopmail.com', 1, 'Attingal', 'Street 555', 'I am a staff', '1993-02-08', NULL, '2022-01-08 03:52:51', '2022-01-08 03:52:51', 3, 1, 0, 'http://localhost/maxglobal/files/wy1iwxdhi4y3atc6z7xp/avatar-img.jpg?s=130', 1, ''),
(114, 'Ranjith', '', 'Male', '9656774188', '9447034555', 'ranjith333@yopmail.com', 1, 'Attingal', 'Street 555', 'I am an agent', '1993-08-05', NULL, '2022-01-08 06:55:37', '2022-01-08 06:55:37', 4, 1, 0, 'http://localhost/maxglobal/files/v9ux5rpwr3bpwqsk0vpe/avatar-img.jpg?s=130', 1, ''),
(115, 'Vivek', '', 'Male', '9656774188', '9447034555', 'vivek333@yopmail.com', 1, 'Attingal', 'Street 555', 'I am a sub contractor', '1990-10-10', '2022-01-11 02:26:54', '2022-01-11 01:53:25', '2022-01-11 02:26:54', 6, 1, 0, 'http://localhost/maxglobal/files/1bsusmqldtbfe60akrp5/avatar-img.jpg?s=130', 1, ''),
(116, 'Anwarr', '', 'Male', '9656774188', '9447034555', 'anwar333@yopmail.com', 1, 'Attingal', 'Street 555', 'I am a university user', '1990-11-11', '2022-01-11 02:29:34', '2022-01-11 02:05:38', '2022-01-11 02:29:34', 5, 1, 0, 'http://localhost/maxglobal/files/rxoaubkmwtimnxrgqqxh/bg-img.jpg?s=130', 1, ''),
(117, '', '', 'Male', '', '', 'vivek123@yopmail.com', 1, '', '', '', '1990-01-01', '2022-05-04 23:40:34', '2022-01-14 00:47:32', '2022-05-04 23:40:34', 4, 1, 0, NULL, 1, ''),
(118, 'Gayathri', '', 'Male', '9656774188', '9447034555', 'gayathri555@yopmail.com', 1, 'Attingal', 'Street 555', 'Student staff', '1990-04-11', NULL, '2022-05-05 00:23:17', '2022-05-05 00:23:17', 3, 1, 0, 'http://localhost/maxglobal/files/mcfek68lqopynlm1wvwu/avatar-img.jpg?s=130', 1, ''),
(119, 'Sameer', '', 'Male', '9656774188', '9447034555', 'sameer456@yopmail.com', 1, 'Attingal', 'Street 555', 'I am an agent', '1990-11-10', NULL, '2022-05-05 05:23:51', '2022-05-05 05:23:51', 4, 0, 1, 'http://localhost/maxglobal/files/80d9nzi2g00iwug1ypse/avatar-img.jpg?s=130', 1, 'c45c3ec'),
(120, 'Afsal', '', 'Male', '9656774188', '9656774188', 'afsal345@yopmail.com', 1, 'Attingal', 'Street 555', 'I am an agent', '1990-01-01', NULL, '2022-05-05 05:29:59', '2022-05-05 05:29:59', 4, 1, 0, '', 1, '8f8fff7'),
(121, 'Remya', '', 'Male', '9656774188', '9656774188', 'remya345@yopmail.com', 1, 'Attingal', 'Street 555', '', '1990-01-01', NULL, '2022-05-05 05:46:47', '2022-05-05 05:46:47', 4, 1, 0, '', 1, 'd24fd67'),
(122, 'Anoop', '', 'Male', '9656774188', '9656774188', 'anoop345@yopmail.com', 1, 'Attingal', 'Street 555', '', '1990-01-01', NULL, '2022-05-05 05:47:27', '2022-05-05 05:47:27', 4, 1, 0, '', 1, '19fdc8c'),
(123, 'Surya', '', 'Male', '9656774188', '9656774188', 'surya345@yopmail.com', 1, 'Attingal', 'Street 555', '', '1990-01-01', NULL, '2022-05-05 05:48:33', '2022-05-05 05:48:33', 3, 0, 1, '', 1, ''),
(124, 'Ajith', '', 'Male', '9656774188', '9656774188', 'ajith345@yopmail.com', 1, 'Attingal', 'Street 555', '', '1990-01-01', NULL, '2022-05-05 06:02:43', '2022-05-05 06:02:43', 4, 0, 1, '', 1, 'cab8160'),
(125, 'Kiran', '', 'Male', '9656774188', '9656774188', 'kiran345@yopmail.com', 1, 'Attingal', 'Street 555', '', '1990-01-01', NULL, '2022-05-05 06:03:21', '2022-05-05 06:03:21', 4, 1, 0, '', 1, 'ceb84fb'),
(126, 'Amaljith', '', 'Male', '9656774188', '9656774188', 'amaljith345@yopmail.com', 1, 'Attingal', 'Street 555', 'I am a staff', '1990-01-01', NULL, '2022-05-07 01:24:22', '2022-05-07 01:24:22', 3, 0, 1, 'http://localhost/maxglobal/files/fabckyvkynfj0bs0w6xe/avatar-img.jpg?s=130', 1, ''),
(127, 'Riyas', '', 'Male', '9656774188', '9656774188', 'riyas345@yopmail.com', 1, 'Attingal', 'Street 555', 'I am a staff', '1990-01-01', NULL, '2022-05-07 01:29:05', '2022-05-07 01:29:05', 3, 0, 1, 'http://localhost/maxglobal/files/9ldhc38eznkrlkuu6kcc/avatar-img.jpg?s=130', 1, ''),
(128, 'Saranya', '', 'Male', '9656774188', '9656774188', 'saranya345@yopmail.com', 1, 'Attingal', 'Street 555', 'I am a staff', '1990-01-01', NULL, '2022-05-07 01:31:36', '2022-05-07 01:31:36', 3, 0, 1, '', 1, ''),
(129, 'Tharun', '', 'Male', '9656774188', '9656774188', 'tharun345@yopmail.com', 1, 'Attingal', 'Street 555', 'I am a sub contractor', '1990-01-01', NULL, '2022-05-07 04:20:53', '2022-05-07 04:20:53', 6, 0, 1, 'http://localhost/maxglobal/files/18ebi8defqolvzmlacax/avatar-img.jpg?s=130', 1, ''),
(130, 'Archana', '', 'Male', '9656774188', '9656774188', 'archana3345@yopmail.com', 1, 'Attingal', 'Street 555', 'I am a university user', '1990-01-01', NULL, '2022-05-07 04:50:57', '2022-05-07 04:50:57', 5, 1, 0, 'http://localhost/maxglobal/files/gihnbxvpe0j2v6mdrcjv/avatar-img.jpg?s=130', 1, ''),
(131, 'Anupama', '', 'Male', '9656774188', '9656774188', 'anupama345@yopmail.com', 1, 'Attingal', 'Street 555', 'I am an employer', '1990-01-01', NULL, '2022-05-07 06:09:42', '2022-05-07 06:09:42', 7, 1, 0, 'http://localhost/maxglobal/files/x0yfn0lt6mikngaikit9/avatar-img.jpg?s=130', 1, ''),
(133, 'Athul', '', 'Male', '9656774188', '9447034555', 'athul345@yopmail.com', 1, 'Attingal', '', '', '1991-08-16', NULL, NULL, NULL, 4, 1, 0, NULL, 1, 'e7a303b'),
(135, 'Sreeja', '', 'Male', '9656774188', '9447034555', 'sreeja345@yopmail.com', 1, 'Attingal', 'Street 555', 'I am a staff', '1990-03-16', NULL, '2022-05-12 08:18:04', '2022-05-12 08:18:04', 3, 1, 0, 'http://localhost/maxglobal/files/7lfzmy562aazghj72yc6/avatar-img.jpg?s=130', 1, ''),
(136, 'Nithin', '', 'Male', '8976787657', '7890678957', 'nithin778@yopmail.com', 1, 'Varkala', 'Test address', 'I am an agent', '1980-05-09', NULL, NULL, '2022-09-05 23:47:44', 4, 1, 0, 'http://localhost/maxglobal/files/hy64eg4de9gcdzjj06pv/avatar-img.jpg?s=130', 1, '2688d51'),
(137, 'Krishna', '', 'Male', '9797979797', '8989898989', 'krishna777@yopmail.com', 1, 'Trivandrum', '', '', '1994-06-07', NULL, NULL, NULL, 4, 1, 0, NULL, 1, '973bdc0'),
(138, 'Anil', '', 'Male', '9089078967', '9089078967', 'anil777@yopmail.com', 1, 'Attingal', '', '', '1995-06-07', NULL, NULL, NULL, 4, 1, 0, NULL, 1, '73626d9'),
(139, 'Anila', '', 'Male', '9089089089', '8907897890', 'anila777@yopmail.com', 1, 'Tvm', '', '', '2004-03-04', NULL, NULL, NULL, 4, 1, 0, NULL, 1, 'd474f25'),
(140, 'Deepthi', '', 'Male', '7890890789', '7890890789', 'deepthi777@yopmail.com', 1, 'Tvm', '', '', '1989-05-16', NULL, NULL, NULL, 4, 1, 0, NULL, 1, '2553108'),
(141, 'Anoop', '', 'Male', '8907896785', '7890678567', 'anoop777@yopmail.com', 1, 'Tvm', '', '', '1999-05-13', NULL, NULL, NULL, 4, 1, 0, NULL, 1, '060504e'),
(142, 'Adithya', '', 'Male', '6789067890', '6789067890', 'adithya777@yopmail.com', 1, 'Tvm', '', '', '1990-07-04', NULL, NULL, NULL, 4, 1, 0, NULL, 1, '02d1fb7'),
(143, 'Krithi', '', 'Male', '8909090890', '9087678907', 'krithi777@yopmail.com', 1, 'Attingal', '', '', '1994-07-14', NULL, NULL, NULL, 4, 1, 0, NULL, 1, 'd90a0a2'),
(144, 'Sreeja', '', 'Male', '8097896785', '7890667854', 'sreeja333@yopmail.com', 1, 'Tvm', '', '', '1990-06-04', NULL, NULL, NULL, 4, 1, 0, NULL, 1, '48f41db'),
(145, 'Saranya', '', 'Male', '96789786979', '96789786979', 'saranya567@yopmail.com', 1, 'Test city', 'Test address', 'Test info', '1990-03-10', NULL, '2022-08-16 22:58:54', '2022-08-16 22:58:54', 4, 1, 0, '', 1, '411553c'),
(146, 'Akshara', '', 'Male', '9870678956', '7890678595', 'akshara567@yopmail.com', 1, 'Tvm', '', '', '1993-08-04', NULL, NULL, NULL, 4, 1, 0, NULL, 1, '80d3c75'),
(147, 'Sreethu', '', 'Male', '7878787878', '9898989898', 'sreethu567@yopmail.com', 1, 'xfvdxv', 'dsgsgsgs', 'dxgxs', '1990-04-05', NULL, '2022-08-16 23:10:15', '2022-08-16 23:10:15', 4, 1, 0, '', 1, '84cf5dc'),
(148, 'Sameer', '', 'Male', '9656774188', '9656774188', 'sameer444@yopmail.com', 1, 'Varkala', '', '', '1990-06-07', NULL, NULL, NULL, 4, 1, 0, NULL, 1, '6853e3a'),
(149, 'Vijay', '', 'Male', '9656774188', '9447034555', 'vijay444@yopmail.com', 1, 'Varkala', '', '', '1990-07-07', NULL, NULL, NULL, 4, 1, 0, NULL, 1, 'fdcb641'),
(150, 'Anoop', '', 'Male', '9656774188', '9447034555', 'anoop789@yopmail.com', 1, 'Varkala', 'Test address', 'I am a sub contractor', '1993-03-01', NULL, '2022-09-05 23:38:01', '2022-09-05 23:38:01', 6, 1, 0, 'http://localhost/maxglobal/files/srylael3p3cz1h0pqgwc/avatar-img.jpg?s=130', 1, ''),
(151, 'Rajiv', '', 'Male', '9656774188', 'rajin567@yopmail.com', 'rajiv578@yopmail.com', 1, 'Varkala', 'Test address', 'I am a sub contractor', '1993-03-01', NULL, '2022-09-05 23:40:33', '2022-09-05 23:40:33', 6, 1, 0, 'http://localhost/maxglobal/files/uiwk1rryvhbpj9qiajut/avatar-img.jpg?s=130', 1, ''),
(152, 'Deepak', '', 'Male', '9656774188', '9447034555', 'deepak689@yopmail.com', 1, 'Attingal', 'Test address', 'I am a sub contractor', '1993-01-01', NULL, '2022-09-05 23:42:46', '2022-09-05 23:46:32', 6, 1, 0, '', 1, ''),
(153, 'Ashwin', '', 'Male', '9656774188', '9447034555', 'ashwin578@yopmail.com', 1, 'Attingal', 'Test address', 'I am a university user', '1993-03-01', NULL, '2022-09-05 23:44:00', '2022-09-05 23:45:07', 5, 1, 0, '', 1, ''),
(154, 'Vineetha', '', 'Male', '9656774188', '9656774188', 'vineetha345@yopmail.com', 1, 'Attingal', 'Street 555', '', '1990-01-01', NULL, '2022-09-17 04:47:20', '2022-09-17 04:47:20', 3, 1, 0, '', 1, ''),
(155, 'Sajin', '', 'Male', '9656774188', '9656774188', 'sajin345@yopmail.com', 1, 'Attingal', 'Street 777', '', '1990-01-01', NULL, '2022-09-17 04:47:55', '2022-09-17 04:47:55', 3, 0, 1, '', 1, ''),
(156, 'Hridya', '', 'Male', '9656774188', '9447034555', 'hridya567@yopmail.com', 1, 'Varkala', '', '', '1991-06-06', NULL, NULL, NULL, 4, 1, 0, NULL, 1, '312ba50'),
(157, 'Riyad', '', 'Male', '9656774188', '9447034555', 'riyad567@yopmail.com', 1, 'Attingal', '', '', '1994-07-13', NULL, NULL, NULL, 4, 1, 0, NULL, 1, 'd969e9c'),
(158, 'Neeraja', '', 'Male', '9656774188', '7890678956', 'neeraja567@yopmail.com', 1, 'Attingal', '', '', '1993-08-19', NULL, NULL, NULL, 4, 1, 0, '', 1, 'dca97f8'),
(159, 'Saniya', '', 'Male', '9656774188', '5678956786', 'saniya345@yopmail.com', 1, 'Attingal', '', '', '1993-11-11', NULL, NULL, NULL, 4, 1, 0, '', 1, 'feb6865'),
(160, 'Amjith', '', 'Male', '9656774188', '8978978978', 'amjith567@yopmail.com', 1, 'Attingal', '', '', '1993-11-18', NULL, NULL, NULL, 4, 1, 0, NULL, 1, '5a60d09'),
(161, 'Sanya', '', 'Male', '9656774188', '7890678956', 'sanya456@yopmail.com', 1, 'Attingal', '', '', '1993-07-21', NULL, NULL, NULL, 4, 1, 0, NULL, 1, '7548222'),
(162, 'Krishna', '', 'Male', '9656774188', '7890678564', 'krishna478@yopmail.com', 1, 'Attingal', '', '', '1999-05-05', NULL, NULL, NULL, 4, 1, 0, NULL, 1, '41e8be9'),
(163, 'Anusha', '', 'Male', '9656774188', '7890678956', 'anusha678@yopmail.com', 1, 'Attingal', '', '', '1993-07-21', NULL, NULL, NULL, 4, 1, 0, NULL, 1, 'cd924dd'),
(164, 'Keerthi', '', 'Male', '9656774188', '8907890789', 'keerthi678@yopmail.com', 1, 'Attingal', '', '', '1993-09-14', NULL, NULL, NULL, 4, 1, 0, NULL, 1, 'dfb5d45'),
(165, 'Shivani', '', 'Male', '9656774188', '9656774199', 'shivani777@yopmail.com', 1, 'Attingal', 'Street 555', 'I am a staff', '1990-01-24', NULL, '2022-10-14 01:24:50', '2022-10-14 01:27:26', 3, 1, 0, 'http://localhost/maxglobal/files/vlwajdnoatiyslhddnd9/avatar-img.jpg?s=130', 1, ''),
(166, 'Smitha', '', 'Male', '9656774188', '9656774167', 'smitha455@yopmail.com', 1, 'Attingal', 'Street 555', 'I am a staff', '1990-01-01', NULL, '2022-10-14 01:37:46', '2022-10-14 01:37:46', 3, 1, 0, 'http://localhost/maxglobal/files/gzav0uxe5y0qkfc7m8q4/avatar-img.jpg?s=130', 1, ''),
(167, 'Anjitha', '', 'Male', '9656774188', '9656774145', 'anjitha257@yopmail.com', 1, 'Attingal', 'Street 555', 'I am an agent', '1990-01-01', NULL, '2022-10-14 01:38:50', '2022-10-14 01:38:50', 4, 1, 0, 'http://localhost/maxglobal/files/wumf0mdxwyvxupa1l9l5/avatar-img.jpg?s=130', 1, 'a4cc3be');

-- --------------------------------------------------------

--
-- Table structure for table `form_data`
--

CREATE TABLE `form_data` (
  `id` int(4) NOT NULL,
  `form_name` text NOT NULL,
  `form_type` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_data`
--

INSERT INTO `form_data` (`id`, `form_name`, `form_type`) VALUES
(1, 'Personal Information', 0),
(2, 'Passport Details', 0),
(5, 'SSLC Details', 0),
(6, 'HSE Details', 0),
(7, 'Bachelor Degree Details', 0),
(8, 'Master Degree Details', 0),
(9, 'IELTS Requirements', 0),
(10, 'Miscellaneous', 0),
(11, 'Application Details', 0);

-- --------------------------------------------------------

--
-- Table structure for table `la_configs`
--

CREATE TABLE `la_configs` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `section` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `la_configs`
--

INSERT INTO `la_configs` (`id`, `key`, `section`, `value`, `created_at`, `updated_at`) VALUES
(1, 'sitename', '', 'Schol Lab', '2019-11-08 12:05:08', '2021-03-20 18:03:38'),
(2, 'sitename_part1', '', 'Schol', '2019-11-08 12:05:08', '2021-03-20 18:03:38'),
(3, 'sitename_part2', '', 'Lab', '2019-11-08 12:05:09', '2021-03-20 18:03:38'),
(4, 'sitename_short', '', 'SL', '2019-11-08 12:05:09', '2021-03-20 18:03:38'),
(5, 'site_description', '', 'Schol Lab', '2019-11-08 12:05:09', '2021-03-20 18:03:38'),
(6, 'sidebar_search', '', '1', '2019-11-08 12:05:09', '2021-03-20 18:03:38'),
(7, 'show_messages', '', '1', '2019-11-08 12:05:09', '2021-03-20 18:03:38'),
(8, 'show_notifications', '', '1', '2019-11-08 12:05:09', '2021-03-20 18:03:38'),
(9, 'show_tasks', '', '1', '2019-11-08 12:05:09', '2021-03-20 18:03:38'),
(10, 'show_rightsidebar', '', '1', '2019-11-08 12:05:09', '2021-03-20 18:03:38'),
(11, 'skin', '', 'skin-blue', '2019-11-08 12:05:09', '2021-03-20 18:03:38'),
(12, 'layout', '', 'fixed', '2019-11-08 12:05:09', '2021-03-20 18:03:38'),
(13, 'default_email', '', 'info@zodocs.com', '2019-11-08 12:05:09', '2021-03-20 18:03:38');

-- --------------------------------------------------------

--
-- Table structure for table `la_menus`
--

CREATE TABLE `la_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'module',
  `parent` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `hierarchy` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `la_menus`
--

INSERT INTO `la_menus` (`id`, `name`, `url`, `icon`, `type`, `parent`, `hierarchy`, `created_at`, `updated_at`) VALUES
(2, 'Users', 'users', 'fa-group', 'module', 1, 0, '2019-11-08 12:05:03', '2019-11-08 12:05:03'),
(5, 'Employees', 'employees', 'fa-group', 'module', 1, 0, '2019-11-08 12:05:03', '2019-11-08 12:05:03'),
(6, 'Roles', 'roles', 'fa-user-plus', 'module', 1, 0, '2019-11-08 12:05:03', '2019-11-08 12:05:03'),
(8, 'Permissions', 'permissions', 'fa-magic', 'module', 1, 0, '2019-11-08 12:05:03', '2019-11-08 12:05:03');

-- --------------------------------------------------------

--
-- Table structure for table `mail_settings`
--

CREATE TABLE `mail_settings` (
  `id` int(11) NOT NULL,
  `mail_content` text NOT NULL,
  `subject` varchar(100) NOT NULL,
  `from_user` int(11) UNSIGNED NOT NULL,
  `to_user` int(11) UNSIGNED NOT NULL,
  `send_date` datetime DEFAULT current_timestamp(),
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `is_read` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mail_settings`
--

INSERT INTO `mail_settings` (`id`, `mail_content`, `subject`, `from_user`, `to_user`, `send_date`, `is_deleted`, `is_read`) VALUES
(9, '<p>Hi You application has been Approved</p>\r\n\r\n<p><img alt=\"frown\" height=\"23\" src=\"http://scholab.herdzo.com/la-assets/plugins/ckeditor/plugins/smiley/images/confused_smile.png\" title=\"frown\" width=\"23\" /></p>\r\n', 'Application Approval', 155, 167, '2021-04-02 10:17:03', 0, 0),
(10, '<p>Test mail content</p>\r\n', 'Test mail', 155, 298, '2022-10-14 13:35:43', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `manage_courses`
--

CREATE TABLE `manage_courses` (
  `id` int(11) NOT NULL,
  `course_name` varchar(150) NOT NULL,
  `course_description` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `university` int(10) NOT NULL,
  `country` int(10) NOT NULL,
  `intake` varchar(25) NOT NULL,
  `duration` decimal(5,2) NOT NULL,
  `course_type` varchar(10) NOT NULL,
  `tuition_fees` decimal(10,2) NOT NULL,
  `test_requirements` text NOT NULL,
  `entry_requirements` text NOT NULL,
  `application_fee` decimal(10,2) NOT NULL,
  `application_deadline` date DEFAULT NULL,
  `remarks` text NOT NULL,
  `university_logo` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_courses`
--

INSERT INTO `manage_courses` (`id`, `course_name`, `course_description`, `status`, `university`, `country`, `intake`, `duration`, `course_type`, `tuition_fees`, `test_requirements`, `entry_requirements`, `application_fee`, `application_deadline`, `remarks`, `university_logo`, `created_at`, `updated_at`) VALUES
(2, 'MBA', 'Business Administration', 1, 6, 4, 'Jan 2023', '3.50', 'PG', '50000.25', 'Sample requirements for test', 'Sample requirements for entry', '500.25', '2022-09-30', 'Sample remarks', 'http://localhost/maxglobal/admin/../files/zft1xbyi3lpunyamwbpj/java-script-img.jpg', NULL, '2022-09-24 00:11:54'),
(4, 'MCA', '', 1, 1, 2, 'Oct 2023', '2.00', 'PG', '45000.45', '', '', '500.00', '2022-09-30', '', 'http://localhost/maxglobal/admin/../files/fnphpnfhhieqnp7qowoa/SQL-Microsoft.jpg', NULL, '2022-09-29 02:14:44'),
(6, 'Biotechnology', '', 1, 5, 3, 'Jan 2023', '2.50', 'UG', '50000.25', 'Lorem ipsum dolor sit amet. Est officiis minus sit eaque quae sit odio internos. Aut laborum porro sit saepe ipsa et consequatur saepe ut illo laborum nam aperiam minus. Qui neque molestiae id dolorem rerum qui atque repellendus vel ipsa dolores. Sit eveniet obcaecati eum quos suscipit est sequi amet aut quibusdam perferendis est possimus voluptas.\r\n\r\nNon officiis minima quo amet perspiciatis eos quod molestiae eum molestias amet est harum consectetur aut provident nostrum? Et accusamus ducimus aut quis enim ut animi expedita qui voluptatem iusto eos illum omnis in laudantium consequatur aut praesentium mollitia.\r\n\r\nAut dolorum incidunt et perferendis totam et illo molestiae! Eum rerum esse et dignissimos vero est fuga assumenda quo voluptas ratione cum autem fugit. Aut reiciendis neque At dolores voluptates ut sunt excepturi ab laboriosam porro ad nulla minima.', 'Lorem ipsum dolor sit amet. Est officiis minus sit eaque quae sit odio internos. Aut laborum porro sit saepe ipsa et consequatur saepe ut illo laborum nam aperiam minus. Qui neque molestiae id dolorem rerum qui atque repellendus vel ipsa dolores. Sit eveniet obcaecati eum quos suscipit est sequi amet aut quibusdam perferendis est possimus voluptas.\r\n\r\nNon officiis minima quo amet perspiciatis eos quod molestiae eum molestias amet est harum consectetur aut provident nostrum? Et accusamus ducimus aut quis enim ut animi expedita qui voluptatem iusto eos illum omnis in laudantium consequatur aut praesentium mollitia.', '500.25', '2022-09-30', 'Sample remarks', 'http://localhost/maxglobal/admin/../files/vtnmkovfcdk9sgo5fwdj/img345.jpg', '2022-09-24 04:07:38', '2022-09-30 00:37:29'),
(7, 'hdfgsx', '', 1, 4, 4, 'Mar 2020', '3.00', 'UG', '456.00', 'afwa', 'dasf', '56.00', '2022-09-28', 'fwfw', 'http://localhost/maxglobal/admin/../files/7wvq8pof1yk4lh8m8vuv/testimonial-img.png', '2022-09-27 21:54:30', '2022-09-27 21:54:30'),
(8, 'Microbiology', '', 1, 7, 5, 'Mar 2023', '3.00', 'PG', '25000.00', 'Sample data', 'Test data', '230.00', '2022-10-21', 'Sample remarks', 'http://localhost/maxglobal/index.php/admin/../files/aa48jus4re0luaqfl7ze/nature101.jpg', '2022-09-29 07:04:50', '2022-09-29 07:04:50'),
(9, 'BBA', 'Lorem ipsum dolor sit amet. Ad sint odio non dolorem magnam qui accusamus molestias eum tempore rerum aut odio veniam vel placeat impedit! Est porro architecto et laboriosam quod non illum beatae non consequatur molestias. Et praesentium velit id voluptatem nihil ut sunt recusandae id quaerat saepe aut internos harum.\r\n\r\nEt placeat excepturi est provident obcaecati et repudiandae ipsam aut voluptas doloremque in asperiores labore qui quos quis est nobis quos. At omnis voluptatem ex inventore repellat vel voluptatem ipsa ea quis distinctio eum aliquam quos et expedita asperiores.\r\n\r\nA debitis eligendi in maxime culpa aut reprehenderit sint. In veritatis earum et voluptate nihil sed dolore voluptatum nam dolorem quia eos voluptates voluptas ea nihil nobis aut quasi explicabo. Quo internos dolorem aut rerum quae in quae omnis est quia commodi At deleniti et sint reprehenderit! Qui velit laborum sit suscipit provident ea laboriosam doloremque vel velit odit a animi sint eos alias earum.', 1, 6, 7, 'Jun 2023', '3.00', 'UG', '23999.00', 'dfsxfdszg', 'fbvxdcvz', '235.00', '2022-09-29', 'zfzsdfzf', 'http://localhost/maxglobal/admin/../files/9jfsvhdtg2jz9qtkwwu3/flower-img_350x350.jpg', '2022-09-29 07:13:11', '2022-10-06 03:52:16'),
(10, 'BTech', '', 1, 9, 5, 'Feb 2023', '3.00', 'UG', '35000.00', 'Test data', 'Test entry', '250.00', '2022-09-29', 'Test remarks', 'http://localhost/maxglobal/admin/../files/desnl1wf1dlcdcjesxxp/nature102.jpg', '2022-09-28 20:13:56', '2022-09-28 20:13:56'),
(11, 'MCom', '', 1, 15, 7, 'Aug 2023', '4.00', 'PG', '47000.00', 'Test data', 'Test values', '230.00', '2022-10-21', 'Test details', 'http://localhost/maxglobal/admin/../files/fyvru3todjhmr4yvrsra/ev-img01.jpg', '2022-09-28 20:15:46', '2022-09-28 20:16:33'),
(12, 'CA', '', 1, 5, 2, 'Jul 2023', '4.00', 'PG', '23000.00', '', '', '560.00', '2022-09-29', '', 'http://localhost/maxglobal/admin/../files/1tlppn18ywaipduznv9m/ev-img02.jpg', '2022-09-28 20:27:05', '2022-10-06 06:18:53'),
(13, 'MSW', '', 0, 8, 4, 'Sep 2023', '3.00', 'PG', '13000.00', 'xfvzdfvzf', 'vzczxvz', '460.00', '2022-09-29', 'xfcsdfzdg', 'http://localhost/maxglobal/admin/../files/p6xjyfzxpml6jsvox7sj/ev-img02.jpg', '2022-09-28 21:02:20', '2022-10-07 23:29:05'),
(14, 'BSW', '', 1, 11, 6, 'Aug 2023', '1.50', 'UG', '14000.00', 'Sample data', 'New data', '340.00', '2022-10-27', 'Sample details', '', '2022-09-30 00:26:47', '2022-09-30 00:26:47'),
(15, 'BHM', '', 1, 112, 2, 'Oct 2023', '3.00', 'UG', '24000.00', 'fjdsgefugseifhw feuwfriwhri gsdfsfs', 'xfcsdfsgd grdfedr gdsgd', '450.00', '2022-10-19', 'asdeas dfesd dhrfgde', '', '2022-10-01 05:17:47', '2022-10-01 05:17:47'),
(16, 'MSC Physics', 'dfydfywqgduwq wrgwugd bwerwrrrrrrrrrrr thy', 1, 143, 2, 'Feb 2023', '3.00', 'PG', '46000.00', 'fsdazsf dggsdg gsrgsxfxg', 'adsaf rgtdgtdctg gsgesg', '460.00', '2022-10-15', 'wasdeasdeas ew twet t4tet', '', '2022-10-06 01:48:10', '2022-10-06 01:48:38');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_db` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `view_col` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fa_icon` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `is_gen` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `label`, `name_db`, `view_col`, `model`, `controller`, `fa_icon`, `is_gen`, `created_at`, `updated_at`) VALUES
(1, 'Users', 'Users', 'users', 'name', 'User', 'UsersController', 'fa-group', 1, '2019-11-08 12:04:38', '2019-11-08 12:05:09'),
(2, 'Uploads', 'Uploads', 'uploads', 'name', 'Upload', 'UploadsController', 'fa-files-o', 1, '2019-11-08 12:04:40', '2019-11-08 12:05:09'),
(3, 'Departments', 'Departments', 'departments', 'name', 'Department', 'DepartmentsController', 'fa-tags', 1, '2019-11-08 12:04:41', '2019-11-08 12:05:09'),
(4, 'Employees', 'Employees', 'employees', 'type-user', 'Employee', 'EmployeesController', 'fa-group', 1, '2019-11-08 12:04:42', '2022-09-26 04:52:22'),
(5, 'Roles', 'Roles', 'roles', 'name', 'Role', 'RolesController', 'fa-user-plus', 1, '2019-11-08 12:04:44', '2019-11-08 12:05:09'),
(6, 'Organizations', 'Organizations', 'organizations', 'name', 'Organization', 'OrganizationsController', 'fa-university', 1, '2019-11-08 12:04:50', '2019-11-08 12:05:09'),
(7, 'Backups', 'Backups', 'backups', 'name', 'Backup', 'BackupsController', 'fa-hdd-o', 1, '2019-11-08 12:04:53', '2019-11-08 12:05:09'),
(8, 'Permissions', 'Permissions', 'permissions', 'name', 'Permission', 'PermissionsController', 'fa-magic', 1, '2019-11-08 12:04:55', '2019-11-08 12:05:09');

-- --------------------------------------------------------

--
-- Table structure for table `module_fields`
--

CREATE TABLE `module_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `colname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `module` int(10) UNSIGNED NOT NULL,
  `field_type` int(10) UNSIGNED NOT NULL,
  `unique` tinyint(1) NOT NULL DEFAULT 0,
  `defaultvalue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `minlength` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `maxlength` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `popup_vals` text COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `module_fields`
--

INSERT INTO `module_fields` (`id`, `colname`, `label`, `module`, `field_type`, `unique`, `defaultvalue`, `minlength`, `maxlength`, `required`, `popup_vals`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'name', 'Name', 1, 16, 0, '', 5, 250, 1, '', 0, '2019-11-08 12:04:38', '2019-11-08 12:04:38'),
(2, 'context_id', 'Context', 1, 13, 0, '0', 0, 0, 0, '', 0, '2019-11-08 12:04:38', '2019-11-08 12:04:38'),
(3, 'email', 'Email', 1, 8, 1, '', 0, 250, 0, '', 0, '2019-11-08 12:04:38', '2019-11-08 12:04:38'),
(4, 'password', 'Password', 1, 17, 0, '', 6, 250, 1, '', 0, '2019-11-08 12:04:38', '2019-11-08 12:04:38'),
(5, 'type', 'User Type', 1, 7, 0, 'Employee', 0, 0, 0, '[\"Employee\",\"Client\"]', 0, '2019-11-08 12:04:38', '2019-11-08 12:04:38'),
(6, 'name', 'Name', 2, 16, 0, '', 5, 250, 1, '', 0, '2019-11-08 12:04:40', '2019-11-08 12:04:40'),
(7, 'path', 'Path', 2, 19, 0, '', 0, 250, 0, '', 0, '2019-11-08 12:04:40', '2019-11-08 12:04:40'),
(8, 'extension', 'Extension', 2, 19, 0, '', 0, 20, 0, '', 0, '2019-11-08 12:04:40', '2019-11-08 12:04:40'),
(9, 'caption', 'Caption', 2, 19, 0, '', 0, 250, 0, '', 0, '2019-11-08 12:04:40', '2019-11-08 12:04:40'),
(10, 'user_id', 'Owner', 2, 7, 0, '1', 0, 0, 0, '@users', 0, '2019-11-08 12:04:40', '2019-11-08 12:04:40'),
(11, 'hash', 'Hash', 2, 19, 0, '', 0, 250, 0, '', 0, '2019-11-08 12:04:40', '2019-11-08 12:04:40'),
(12, 'public', 'Is Public', 2, 2, 0, '0', 0, 0, 0, '', 0, '2019-11-08 12:04:40', '2019-11-08 12:04:40'),
(16, 'name', 'Name', 4, 16, 0, '', 5, 250, 1, '', 0, '2019-11-08 12:04:42', '2019-11-08 12:04:42'),
(17, 'designation', 'Designation', 4, 19, 0, '', 0, 50, 1, '', 0, '2019-11-08 12:04:42', '2019-11-08 12:04:42'),
(18, 'gender', 'Gender', 4, 18, 0, 'Male', 0, 0, 0, '[\"Male\",\"Female\"]', 0, '2019-11-08 12:04:42', '2020-01-03 11:42:20'),
(19, 'mobile', 'Mobile', 4, 14, 0, '', 10, 20, 1, '', 0, '2019-11-08 12:04:42', '2019-11-08 12:04:42'),
(20, 'mobile2', 'Whatsapp', 4, 14, 0, '', 10, 20, 1, '', 0, '2019-11-08 12:04:42', '2021-04-12 17:27:09'),
(21, 'email', 'Email', 4, 8, 0, '', 5, 250, 1, '', 0, '2019-11-08 12:04:42', '2019-12-21 07:35:14'),
(22, 'dept', 'Department', 4, 7, 0, '0', 0, 0, 0, '@departments', 0, '2019-11-08 12:04:42', '2019-12-14 12:33:11'),
(23, 'city', 'City', 4, 19, 0, '', 0, 50, 0, '', 0, '2019-11-08 12:04:42', '2019-11-08 12:04:42'),
(24, 'address', 'Address', 4, 1, 0, '', 0, 1000, 0, '', 0, '2019-11-08 12:04:42', '2019-11-08 12:04:42'),
(25, 'about', 'About', 4, 19, 0, '', 0, 0, 0, '', 0, '2019-11-08 12:04:42', '2019-11-08 12:04:42'),
(26, 'date_birth', 'Date of Birth', 4, 4, 0, '1990-01-01', 0, 0, 0, '', 0, '2019-11-08 12:04:42', '2019-11-08 12:04:42'),
(30, 'name', 'Name', 5, 16, 1, '', 1, 250, 1, '', 0, '2019-11-08 12:04:44', '2019-11-08 12:04:44'),
(31, 'display_name', 'Display Name', 5, 19, 0, '', 0, 250, 1, '', 0, '2019-11-08 12:04:44', '2019-11-08 12:04:44'),
(32, 'description', 'Description', 5, 21, 0, '', 0, 1000, 0, '', 0, '2019-11-08 12:04:44', '2019-11-08 12:04:44'),
(33, 'parent', 'Parent Role', 5, 7, 0, '1', 0, 0, 0, '@roles', 0, '2019-11-08 12:04:44', '2019-11-08 12:04:44'),
(34, 'dept', 'Department', 5, 7, 0, '1', 0, 0, 0, '@departments', 0, '2019-11-08 12:04:45', '2019-11-08 12:04:45'),
(35, 'name', 'Name', 6, 16, 1, '', 5, 250, 1, '', 0, '2019-11-08 12:04:50', '2019-11-08 12:04:50'),
(36, 'email', 'Email', 6, 8, 1, '', 0, 250, 0, '', 0, '2019-11-08 12:04:51', '2019-11-08 12:04:51'),
(37, 'phone', 'Phone', 6, 14, 0, '', 0, 20, 0, '', 0, '2019-11-08 12:04:51', '2019-11-08 12:04:51'),
(38, 'website', 'Website', 6, 23, 0, 'http://', 0, 250, 0, '', 0, '2019-11-08 12:04:51', '2019-11-08 12:04:51'),
(39, 'assigned_to', 'Assigned to', 6, 7, 0, '0', 0, 0, 0, '@employees', 0, '2019-11-08 12:04:51', '2019-11-08 12:04:51'),
(40, 'connect_since', 'Connected Since', 6, 4, 0, 'date(\'Y-m-d\')', 0, 0, 0, '', 0, '2019-11-08 12:04:51', '2019-11-08 12:04:51'),
(41, 'address', 'Address', 6, 1, 0, '', 0, 1000, 1, '', 0, '2019-11-08 12:04:51', '2019-11-08 12:04:51'),
(42, 'city', 'City', 6, 19, 0, '', 0, 250, 1, '', 0, '2019-11-08 12:04:51', '2019-11-08 12:04:51'),
(43, 'description', 'Description', 6, 21, 0, '', 0, 1000, 0, '', 0, '2019-11-08 12:04:51', '2019-11-08 12:04:51'),
(44, 'profile_image', 'Profile Image', 6, 12, 0, '', 0, 250, 0, '', 0, '2019-11-08 12:04:51', '2019-11-08 12:04:51'),
(45, 'profile', 'Company Profile', 6, 9, 0, '', 0, 250, 0, '', 0, '2019-11-08 12:04:51', '2019-11-08 12:04:51'),
(46, 'name', 'Name', 7, 16, 1, '', 0, 250, 1, '', 0, '2019-11-08 12:04:53', '2019-11-08 12:04:53'),
(47, 'file_name', 'File Name', 7, 19, 1, '', 0, 250, 1, '', 0, '2019-11-08 12:04:53', '2019-11-08 12:04:53'),
(48, 'backup_size', 'File Size', 7, 19, 0, '0', 0, 10, 1, '', 0, '2019-11-08 12:04:53', '2019-11-08 12:04:53'),
(49, 'name', 'Name', 8, 16, 1, '', 1, 250, 1, '', 0, '2019-11-08 12:04:55', '2019-11-08 12:04:55'),
(50, 'display_name', 'Display Name', 8, 19, 0, '', 0, 250, 1, '', 0, '2019-11-08 12:04:55', '2019-11-08 12:04:55'),
(51, 'description', 'Description', 8, 21, 0, '', 0, 1000, 0, '', 0, '2019-11-08 12:04:55', '2019-11-08 12:04:55'),
(52, 'type-user', 'Student User', 4, 2, 0, '1', 0, 0, 0, '', 0, '2020-04-09 16:47:19', '2020-04-09 16:47:19'),
(53, 'nurse-user', 'Nurse user', 4, 2, 0, '', 0, 0, 0, '', 0, '2020-04-09 16:48:11', '2020-04-09 16:48:11');

-- --------------------------------------------------------

--
-- Table structure for table `module_field_types`
--

CREATE TABLE `module_field_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `module_field_types`
--

INSERT INTO `module_field_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Address', '2019-11-08 12:04:35', '2019-11-08 12:04:35'),
(2, 'Checkbox', '2019-11-08 12:04:35', '2019-11-08 12:04:35'),
(3, 'Currency', '2019-11-08 12:04:35', '2019-11-08 12:04:35'),
(4, 'Date', '2019-11-08 12:04:35', '2019-11-08 12:04:35'),
(5, 'Datetime', '2019-11-08 12:04:35', '2019-11-08 12:04:35'),
(6, 'Decimal', '2019-11-08 12:04:35', '2019-11-08 12:04:35'),
(7, 'Dropdown', '2019-11-08 12:04:35', '2019-11-08 12:04:35'),
(8, 'Email', '2019-11-08 12:04:35', '2019-11-08 12:04:35'),
(9, 'File', '2019-11-08 12:04:35', '2019-11-08 12:04:35'),
(10, 'Float', '2019-11-08 12:04:35', '2019-11-08 12:04:35'),
(11, 'HTML', '2019-11-08 12:04:35', '2019-11-08 12:04:35'),
(12, 'Image', '2019-11-08 12:04:35', '2019-11-08 12:04:35'),
(13, 'Integer', '2019-11-08 12:04:35', '2019-11-08 12:04:35'),
(14, 'Mobile', '2019-11-08 12:04:35', '2019-11-08 12:04:35'),
(15, 'Multiselect', '2019-11-08 12:04:35', '2019-11-08 12:04:35'),
(16, 'Name', '2019-11-08 12:04:35', '2019-11-08 12:04:35'),
(17, 'Password', '2019-11-08 12:04:35', '2019-11-08 12:04:35'),
(18, 'Radio', '2019-11-08 12:04:35', '2019-11-08 12:04:35'),
(19, 'String', '2019-11-08 12:04:36', '2019-11-08 12:04:36'),
(20, 'Taginput', '2019-11-08 12:04:36', '2019-11-08 12:04:36'),
(21, 'Textarea', '2019-11-08 12:04:36', '2019-11-08 12:04:36'),
(22, 'TextField', '2019-11-08 12:04:36', '2019-11-08 12:04:36'),
(23, 'URL', '2019-11-08 12:04:36', '2019-11-08 12:04:36'),
(24, 'Files', '2019-11-08 12:04:36', '2019-11-08 12:04:36');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_type` varchar(10) NOT NULL,
  `doc_type` varchar(20) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `comments` text DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `notif_dat` datetime DEFAULT current_timestamp(),
  `is_read` tinyint(1) NOT NULL DEFAULT 0,
  `approval_text` text DEFAULT NULL,
  `from_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `user_type`, `doc_type`, `status`, `comments`, `is_deleted`, `notif_dat`, `is_read`, `approval_text`, `from_user_id`) VALUES
(1, 222, '', '', NULL, 'There is change in the Courses Applied or Courses Preferred for the student Aswathy that has been done by Scholab Admin, Please check the details..', 0, '2022-05-19 13:04:13', 0, NULL, NULL),
(2, 155, '', '', NULL, 'There is change in the Courses Applied or Courses Preferred for the student Aswathy that has been done by Scholab Admin, Please check the details..', 0, '2022-05-19 13:04:13', 0, NULL, NULL),
(3, 222, '', '', NULL, 'There is change in the Courses Applied or Courses Preferred for the student Aswathy that has been done by Scholab Admin, Please check the details..', 0, '2022-05-19 13:04:31', 0, NULL, NULL),
(4, 155, '', '', NULL, 'There is change in the Courses Applied or Courses Preferred for the student Aswathy that has been done by Scholab Admin, Please check the details..', 0, '2022-05-19 13:04:31', 0, NULL, NULL),
(5, 222, '', '', NULL, 'There is change in the Courses Applied or Courses Preferred for the student Aswathy that has been done by Scholab Admin, Please check the details..', 0, '2022-05-19 13:04:41', 0, NULL, NULL),
(6, 155, '', '', NULL, 'There is change in the Courses Applied or Courses Preferred for the student Aswathy that has been done by Scholab Admin, Please check the details..', 0, '2022-05-19 13:04:41', 0, NULL, NULL),
(7, 106, '', '', NULL, 'Student -  has been assigned to you by Scholab Admin(Admin)', 0, '2022-05-24 09:15:08', 1, NULL, 155),
(8, 107, '', '', NULL, 'Student -  has been assigned to staff - Priyanka by Scholab Admin(Admin)', 0, '2022-05-24 09:15:08', 1, NULL, 155),
(9, 222, '', '', NULL, 'You are assigned to a staff - Priyanka by Scholab Admin(Admin)', 0, '2022-05-24 09:15:08', 0, NULL, 155),
(10, 96, '', '', NULL, 'Student -  has been assigned to staff - Priyanka by Scholab Admin(Counselor Staff)', 0, '2022-05-24 09:15:08', 1, NULL, 155),
(11, 126, '', '', NULL, 'Student - Ardra has been assigned to you by Scholab Admin(Admin)', 0, '2022-05-24 09:23:42', 0, NULL, 155),
(12, 214, '', '', NULL, 'You are assigned to a staff - Amaljith by Scholab Admin(Admin)', 0, '2022-05-24 09:23:42', 0, NULL, 155),
(13, 96, '', '', NULL, 'Student - Ardra has been assigned to staff - Amaljith by Scholab Admin(Admin)', 0, '2022-05-24 09:23:42', 1, NULL, 155),
(24, 222, '', '', NULL, 'Application form status of Aswathy has been changed to Approve by Scholab Admin', 0, '2022-05-25 08:48:52', 0, NULL, NULL),
(25, 107, '', '', NULL, 'Application form status of Aswathy has been changed to Approve by Scholab Admin', 0, '2022-05-25 08:48:52', 1, NULL, NULL),
(26, 106, '', '', NULL, 'Application form status of Aswathy has been changed to Approve by Scholab Admin', 0, '2022-05-25 08:48:52', 1, NULL, NULL),
(27, 96, '', '', NULL, 'Application form status of Aswathy has been changed to Approve by Scholab Admin', 0, '2022-05-25 08:48:52', 1, NULL, NULL),
(28, 222, '', '', NULL, 'Next follow up date of student Aswathy has been set to 2022-05-30 at 15:30 by Priyanka', 0, '2022-05-25 13:35:38', 0, NULL, NULL),
(29, 107, '', '', NULL, 'Next follow up date of student Aswathy has been set to 2022-05-30 at 15:30 by Priyanka', 0, '2022-05-25 13:35:38', 1, NULL, NULL),
(30, 106, '', '', NULL, 'Next follow up date of student Aswathy has been set to 2022-05-30 at 15:30 by Priyanka', 0, '2022-05-25 13:35:38', 1, NULL, NULL),
(31, 96, '', '', NULL, 'Next follow up date of student Aswathy has been set to 2022-05-30 at 15:30 by Priyanka', 0, '2022-05-25 13:35:38', 1, NULL, NULL),
(32, 96, '', '', NULL, 'Priyanka(Staff) added a new student - Anitha <a href=\"http://localhost/maxglobal/admin/students/226/edit\">. Click here to edit the student</a>', 0, '2022-05-25 13:44:11', 1, NULL, 175),
(33, 109, '', '', NULL, 'Student - Aswathy has been assigned to you by Priyanka(Staff)', 0, '2022-05-25 17:17:42', 1, NULL, 175),
(34, 107, '', '', NULL, 'Student - Aswathy has been assigned to university user - Karthik by Priyanka(Staff)', 0, '2022-05-25 17:17:42', 1, NULL, 175),
(35, 222, '', '', NULL, 'You are assigned to a university user - Karthik by Priyanka(Staff)', 0, '2022-05-25 17:17:42', 0, NULL, 175),
(36, 96, '', '', NULL, 'Student - Aswathy has been assigned to university user - Karthik by Priyanka(Staff)', 0, '2022-05-25 17:17:42', 1, NULL, 175),
(37, 96, '', '', NULL, 'Sneha(Agent) added a new student - Shiyas <a href=\"http://localhost/maxglobal/admin/students/232/edit\">. Click here to edit the student</a>', 0, '2022-05-26 16:19:36', 1, NULL, 176),
(38, 135, '', '', NULL, 'Sneha(Agent) added a new student - Shiyas <a href=\"http://localhost/maxglobal/admin/students/232/edit\">. Click here to edit the student</a>', 0, '2022-05-26 16:19:36', 0, NULL, 176),
(39, 126, '', '', NULL, 'Sneha(Agent) added a new student - Shiyas <a href=\"http://localhost/maxglobal/admin/students/232/edit\">. Click here to edit the student</a>', 0, '2022-05-26 16:19:36', 0, NULL, 176),
(40, 222, '', '', NULL, 'Next follow up date of student Aswathy has been set to 2022-06-05 at 10:30 by Priyanka', 0, '2022-05-27 08:21:20', 1, NULL, NULL),
(41, 107, '', '', NULL, 'Next follow up date of student Aswathy has been set to 2022-06-05 at 10:30 by Priyanka', 0, '2022-05-27 08:21:20', 1, NULL, NULL),
(42, 106, '', '', NULL, 'Next follow up date of student Aswathy has been set to 2022-06-05 at 10:30 by Priyanka', 0, '2022-05-27 08:21:20', 1, NULL, NULL),
(43, 109, '', '', NULL, 'Next follow up date of student Aswathy has been set to 2022-06-05 at 10:30 by Priyanka', 0, '2022-05-27 08:21:20', 1, NULL, NULL),
(44, 96, '', '', NULL, 'Next follow up date of student Aswathy has been set to 2022-06-05 at 10:30 by Priyanka', 0, '2022-05-27 08:21:20', 1, NULL, NULL),
(45, 235, 'NURSE', 'passport_upload', 1, 'Approved', 0, '2022-06-03 15:27:17', 0, 'Your Passport front and back image has been APPROVED', NULL),
(46, 235, 'NURSE', 'certificate_upload', 1, 'Approved', 0, '2022-06-03 15:44:38', 0, 'Your Qualification file has been APPROVED', NULL),
(47, 235, 'NURSE', 'evidence_upload', 0, 'Rejected', 0, '2022-06-03 15:44:54', 0, 'Your Evidence upload doc has been REJECTED', NULL),
(48, 235, 'NURSE', 'cv_file_upload', 1, 'CV Approved', 0, '2022-06-03 17:35:28', 0, 'Your CV file has been APPROVED', NULL),
(49, 235, 'NURSE', 'ielts_certificate', 1, 'Approved', 0, '2022-06-03 18:25:20', 0, 'Your IELTS Certificate has been APPROVED', NULL),
(50, 235, 'NURSE', 'ielts_file_upload', 1, 'Approved', 0, '2022-06-03 18:25:25', 0, 'Your IELTS file has been APPROVED', NULL),
(51, 235, 'NURSE', 'file_upload_nursing', 0, 'Rejected', 0, '2022-06-03 18:25:34', 0, 'Your NCC file has been REJECTED', NULL),
(52, 235, 'NURSE', 'offer_letter_file', 1, 'Approved', 0, '2022-06-03 19:40:23', 0, 'Your Offer letter has been APPROVED', NULL),
(53, 235, 'NURSE', 'passport_upload', 1, 'Approved', 0, '2022-06-04 11:42:55', 0, 'Your Passport front and back image has been APPROVED', NULL),
(54, 235, 'NURSE', 'addl_docs', 1, 'Approved', 0, '2022-06-04 11:49:25', 0, 'Your Additional documents has been APPROVED', NULL),
(55, 235, 'NURSE', 'scope_file_upload', 1, 'Approved', 0, '2022-06-04 12:17:31', 0, 'Your Scope File has been APPROVED', NULL),
(56, 235, 'NURSE', 'cbt_upload', 0, 'Rejected', 0, '2022-06-04 12:17:40', 0, 'Your CBT Score File has been REJECTED', NULL),
(57, 235, 'NURSE', 'cbt_upload', 0, 'Rejected', 0, '2022-06-04 12:22:13', 0, 'Your CBT Score File has been REJECTED', NULL),
(58, 235, 'NURSE', 'nmc_file_upload', 1, 'Approved', 0, '2022-06-04 12:22:20', 0, 'Your NMC File has been APPROVED', NULL),
(59, 235, 'NURSE', 'birth_cert_upload', 0, 'Approved', 0, '2022-06-04 12:22:27', 0, 'Your Birth Certificate has been REJECTED', NULL),
(60, 235, 'NURSE', 'scope_file_upload', 0, 'Rejected', 0, '2022-06-04 12:22:39', 0, 'Your Scope File has been REJECTED', NULL),
(61, 235, 'NURSE', 'spouse_passport', 1, 'Passport approved', 0, '2022-06-17 19:45:09', 0, 'Your Spouse passport front and back image has been APPROVED', NULL),
(62, 235, 'NURSE', 'appl_form', 1, 'Approved', 0, '2022-06-20 10:41:41', 0, 'Your Application form has been APPROVED', NULL),
(63, 235, 'NURSE', 'refer_file', 0, 'Rejected', 0, '2022-06-20 10:41:50', 0, 'Your Reference doc has been REJECTED', NULL),
(64, 235, 'NURSE', 'tb_file_upload', 0, 'Approved', 0, '2022-06-20 10:42:08', 0, 'Your TB doc has been REJECTED', NULL),
(65, 235, 'NURSE', 'spons_file_upload', 1, 'Approved', 0, '2022-06-20 10:42:12', 0, 'Your Sponsor doc has been APPROVED', NULL),
(66, 235, 'NURSE', 'visa_file', 1, 'Approved', 0, '2022-06-20 10:42:15', 0, 'Your Visa doc has been APPROVED', NULL),
(67, 235, 'NURSE', 'visa_appr_file', 1, 'Approved', 0, '2022-06-20 10:42:18', 0, 'Your Visa approval doc has been APPROVED', NULL),
(68, 235, 'NURSE', 'flight_ticket', 1, 'Approved', 0, '2022-06-20 10:42:21', 0, 'Your Flight ticket has been APPROVED', NULL),
(69, 235, 'NURSE', 'pcc_file_upload', 1, 'Approved', 0, '2022-06-20 10:45:08', 0, 'Your PCC certificate has been APPROVED', NULL),
(70, 123, '', '', NULL, 'Nurse - Sithara has been assigned to you by Scholab Admin(Admin)', 0, '2022-07-21 10:43:13', 1, NULL, 155),
(71, 235, '', '', NULL, 'You are assigned to a staff - Surya by Scholab Admin(Admin)', 0, '2022-07-21 10:43:13', 0, NULL, 155),
(72, 96, '', '', NULL, 'Nurse - Sithara has been assigned to staff - Surya by Scholab Admin(Admin)', 0, '2022-07-21 10:43:13', 1, NULL, 155),
(73, 123, '', '', NULL, 'Nurse - Anupama has been assigned to you by Scholab Admin(Admin)', 0, '2022-07-21 10:44:05', 1, NULL, 155),
(74, 217, '', '', NULL, 'You are assigned to a staff - Surya by Scholab Admin(Admin)', 0, '2022-07-21 10:44:05', 0, NULL, 155),
(75, 96, '', '', NULL, 'Nurse - Anupama has been assigned to staff - Surya by Scholab Admin(Admin)', 0, '2022-07-21 10:44:05', 1, NULL, 155),
(76, 126, '', '', NULL, 'Nurse - Keerthy has been assigned to you by Scholab Admin(Admin)', 0, '2022-07-21 10:44:15', 0, NULL, 155),
(77, 216, '', '', NULL, 'You are assigned to a staff - Amaljith by Scholab Admin(Admin)', 0, '2022-07-21 10:44:15', 0, NULL, 155),
(78, 96, '', '', NULL, 'Nurse - Keerthy has been assigned to staff - Amaljith by Scholab Admin(Admin)', 0, '2022-07-21 10:44:15', 1, NULL, 155),
(79, 123, '', '', NULL, 'Nurse - Karthik has been assigned to you by Scholab Admin(Admin)', 0, '2022-09-02 08:18:09', 1, NULL, 155),
(80, 253, '', '', NULL, 'You are assigned to a staff - Surya by Scholab Admin(Admin)', 0, '2022-09-02 08:18:09', 0, NULL, 155),
(81, 96, '', '', NULL, 'Nurse - Karthik has been assigned to staff - Surya by Scholab Admin(Admin)', 0, '2022-09-02 08:18:09', 1, NULL, 155),
(82, 96, '', '', NULL, 'There is change in the Courses Applied or Courses Preferred for the student Hareesh that has been done by Scholab Admin, Please check the details..', 0, '2022-09-02 12:05:54', 1, NULL, NULL),
(83, 96, '', '', NULL, 'Application form status of Hareesh has been changed to Processing by Scholab Admin', 0, '2022-09-02 12:50:18', 1, NULL, NULL),
(84, 107, '', '', NULL, 'Application form status of Shiyas A has been changed to Processing by Scholab Admin', 0, '2022-09-03 10:17:17', 1, NULL, NULL),
(85, 96, '', '', NULL, 'Application form status of Shiyas A has been changed to Processing by Scholab Admin', 0, '2022-09-03 10:17:17', 1, NULL, NULL),
(86, 96, '', '', NULL, 'Priyanka(Staff) added a new student - Sitara <a href=\"http://localhost/maxglobal/admin/students/260/edit\">. Click here to edit the student</a>', 0, '2022-09-06 05:18:56', 1, NULL, 175),
(87, 109, '', '', NULL, 'Student - Sitara has been assigned to you by Priyanka(Staff)', 0, '2022-09-06 05:20:11', 1, NULL, 175),
(88, 107, '', '', NULL, 'Student - Sitara has been assigned to university user - Karthik by Priyanka(Staff)', 0, '2022-09-06 05:20:11', 1, NULL, 175),
(89, 260, '', '', NULL, 'You are assigned to a university user - Karthik by Priyanka(Staff)', 0, '2022-09-06 05:20:11', 0, NULL, 175),
(90, 96, '', '', NULL, 'Student - Sitara has been assigned to university user - Karthik by Priyanka(Staff)', 0, '2022-09-06 05:20:11', 1, NULL, 175),
(91, 260, '', '', NULL, 'Application form status of Sitara has been changed to Processing by Priyanka', 0, '2022-09-06 05:20:22', 0, NULL, NULL),
(92, 107, '', '', NULL, 'Application form status of Sitara has been changed to Processing by Priyanka', 0, '2022-09-06 05:20:22', 1, NULL, NULL),
(93, 106, '', '', NULL, 'Application form status of Sitara has been changed to Processing by Priyanka', 0, '2022-09-06 05:20:22', 0, NULL, NULL),
(94, 109, '', '', NULL, 'Application form status of Sitara has been changed to Processing by Priyanka', 0, '2022-09-06 05:20:22', 1, NULL, NULL),
(95, 96, '', '', NULL, 'Application form status of Sitara has been changed to Processing by Priyanka', 0, '2022-09-06 05:20:22', 1, NULL, NULL),
(96, 260, '', '', NULL, 'Next follow up date of student Sitara has been set to 2022-09-14 at 14:30 by Priyanka', 0, '2022-09-06 05:20:43', 0, NULL, NULL),
(97, 107, '', '', NULL, 'Next follow up date of student Sitara has been set to 2022-09-14 at 14:30 by Priyanka', 0, '2022-09-06 05:20:43', 1, NULL, NULL),
(98, 106, '', '', NULL, 'Next follow up date of student Sitara has been set to 2022-09-14 at 14:30 by Priyanka', 0, '2022-09-06 05:20:43', 0, NULL, NULL),
(99, 109, '', '', NULL, 'Next follow up date of student Sitara has been set to 2022-09-14 at 14:30 by Priyanka', 0, '2022-09-06 05:20:43', 1, NULL, NULL),
(100, 96, '', '', NULL, 'Next follow up date of student Sitara has been set to 2022-09-14 at 14:30 by Priyanka', 0, '2022-09-06 05:20:43', 1, NULL, NULL),
(101, 96, '', '', NULL, 'Sneha(Agent) added a new student - Vijay <a href=\"http://localhost/maxglobal/admin/students/261/edit\">. Click here to edit the student</a>', 0, '2022-09-06 05:52:00', 1, NULL, 176),
(102, 135, '', '', NULL, 'Sneha(Agent) added a new student - Vijay <a href=\"http://localhost/maxglobal/admin/students/261/edit\">. Click here to edit the student</a>', 0, '2022-09-06 05:52:00', 0, NULL, 176),
(103, 126, '', '', NULL, 'Sneha(Agent) added a new student - Vijay <a href=\"http://localhost/maxglobal/admin/students/261/edit\">. Click here to edit the student</a>', 0, '2022-09-06 05:52:00', 0, NULL, 176),
(104, 107, '', '', NULL, 'Application form status of Vijay has been changed to Processing by Akhil', 0, '2022-09-06 13:35:14', 0, NULL, NULL),
(105, 96, '', '', NULL, 'Application form status of Vijay has been changed to Processing by Akhil', 0, '2022-09-06 13:35:14', 1, NULL, NULL),
(106, 107, '', '', NULL, 'Application form status of Vijay has been changed to Approve by Akhil', 0, '2022-09-06 13:36:46', 0, NULL, NULL),
(107, 96, '', '', NULL, 'Application form status of Vijay has been changed to Approve by Akhil', 0, '2022-09-06 13:36:46', 1, NULL, NULL),
(108, 107, '', '', NULL, 'Application form status of Vijay has been changed to Processing by Akhil', 0, '2022-09-06 13:37:10', 0, NULL, NULL),
(109, 96, '', '', NULL, 'Application form status of Vijay has been changed to Processing by Akhil', 0, '2022-09-06 13:37:10', 1, NULL, NULL),
(110, 260, '', '', NULL, 'Application form status of Sitara has been changed to Approve by Karthik', 0, '2022-09-06 13:54:21', 0, NULL, NULL),
(111, 107, '', '', NULL, 'Application form status of Sitara has been changed to Approve by Karthik', 0, '2022-09-06 13:54:21', 0, NULL, NULL),
(112, 106, '', '', NULL, 'Application form status of Sitara has been changed to Approve by Karthik', 0, '2022-09-06 13:54:21', 0, NULL, NULL),
(113, 109, '', '', NULL, 'Application form status of Sitara has been changed to Approve by Karthik', 0, '2022-09-06 13:54:21', 1, NULL, NULL),
(114, 96, '', '', NULL, 'Application form status of Sitara has been changed to Approve by Karthik', 0, '2022-09-06 13:54:21', 1, NULL, NULL),
(115, 260, '', '', NULL, 'Application form status of Sitara has been changed to Processing by Karthik', 0, '2022-09-06 13:54:27', 0, NULL, NULL),
(116, 107, '', '', NULL, 'Application form status of Sitara has been changed to Processing by Karthik', 0, '2022-09-06 13:54:27', 0, NULL, NULL),
(117, 106, '', '', NULL, 'Application form status of Sitara has been changed to Processing by Karthik', 0, '2022-09-06 13:54:27', 0, NULL, NULL),
(118, 109, '', '', NULL, 'Application form status of Sitara has been changed to Processing by Karthik', 0, '2022-09-06 13:54:27', 1, NULL, NULL),
(119, 96, '', '', NULL, 'Application form status of Sitara has been changed to Processing by Karthik', 0, '2022-09-06 13:54:27', 1, NULL, NULL),
(120, 106, '', '', NULL, 'Application form status of Anitha has been changed to Processing by Akhil', 0, '2022-09-07 06:26:07', 0, NULL, NULL),
(121, 96, '', '', NULL, 'Application form status of Anitha has been changed to Processing by Akhil', 0, '2022-09-07 06:26:07', 1, NULL, NULL),
(122, 260, '', '', NULL, 'Application form status of Sitara has been changed to Approve by Karthik', 0, '2022-09-07 06:27:47', 0, NULL, NULL),
(123, 107, '', '', NULL, 'Application form status of Sitara has been changed to Approve by Karthik', 0, '2022-09-07 06:27:47', 0, NULL, NULL),
(124, 106, '', '', NULL, 'Application form status of Sitara has been changed to Approve by Karthik', 0, '2022-09-07 06:27:47', 0, NULL, NULL),
(125, 109, '', '', NULL, 'Application form status of Sitara has been changed to Approve by Karthik', 0, '2022-09-07 06:27:47', 1, NULL, NULL),
(126, 96, '', '', NULL, 'Application form status of Sitara has been changed to Approve by Karthik', 0, '2022-09-07 06:27:47', 1, NULL, NULL),
(127, 260, '', '', NULL, 'Application form status of Sitara has been changed to Processing by Karthik', 0, '2022-09-07 06:27:53', 0, NULL, NULL),
(128, 107, '', '', NULL, 'Application form status of Sitara has been changed to Processing by Karthik', 0, '2022-09-07 06:27:53', 0, NULL, NULL),
(129, 106, '', '', NULL, 'Application form status of Sitara has been changed to Processing by Karthik', 0, '2022-09-07 06:27:53', 0, NULL, NULL),
(130, 109, '', '', NULL, 'Application form status of Sitara has been changed to Processing by Karthik', 0, '2022-09-07 06:27:53', 1, NULL, NULL),
(131, 96, '', '', NULL, 'Application form status of Sitara has been changed to Processing by Karthik', 0, '2022-09-07 06:27:53', 1, NULL, NULL),
(132, 107, '', '', NULL, 'Application form status of Vijay has been changed to Processing by Scholab Admin', 0, '2022-09-22 07:37:14', 0, NULL, NULL),
(133, 96, '', '', NULL, 'Application form status of Vijay has been changed to Processing by Scholab Admin', 0, '2022-09-22 07:37:14', 1, NULL, NULL),
(134, 261, '', '', NULL, 'Application form status of Vijay has been changed to Approve by Scholab Admin', 0, '2022-09-22 07:41:04', 0, NULL, NULL),
(135, 107, '', '', NULL, 'Application form status of Vijay has been changed to Approve by Scholab Admin', 0, '2022-09-22 07:41:04', 0, NULL, NULL),
(136, 96, '', '', NULL, 'Application form status of Vijay has been changed to Approve by Scholab Admin', 0, '2022-09-22 07:41:04', 1, NULL, NULL),
(137, 261, '', '', NULL, 'Application form status of Vijay has been changed to Processing by Scholab Admin', 0, '2022-09-22 07:43:07', 0, NULL, NULL),
(138, 107, '', '', NULL, 'Application form status of Vijay has been changed to Processing by Scholab Admin', 0, '2022-09-22 07:43:07', 0, NULL, NULL),
(139, 96, '', '', NULL, 'Application form status of Vijay has been changed to Processing by Scholab Admin', 0, '2022-09-22 07:43:07', 1, NULL, NULL),
(140, 261, '', '', NULL, 'Application form status of Vijay has been changed to Completed by Scholab Admin', 0, '2022-09-22 07:44:22', 0, NULL, NULL),
(141, 107, '', '', NULL, 'Application form status of Vijay has been changed to Completed by Scholab Admin', 0, '2022-09-22 07:44:22', 0, NULL, NULL),
(142, 96, '', '', NULL, 'Application form status of Vijay has been changed to Completed by Scholab Admin', 0, '2022-09-22 07:44:22', 1, NULL, NULL),
(143, 261, '', '', NULL, 'Application form status of Vijay has been changed to Processing by Scholab Admin', 0, '2022-09-22 07:45:52', 0, NULL, NULL),
(144, 107, '', '', NULL, 'Application form status of Vijay has been changed to Processing by Scholab Admin', 0, '2022-09-22 07:45:52', 0, NULL, NULL),
(145, 96, '', '', NULL, 'Application form status of Vijay has been changed to Processing by Scholab Admin', 0, '2022-09-22 07:45:52', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `nurse_dependant_det`
--

CREATE TABLE `nurse_dependant_det` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `relatiion_status` varchar(10) DEFAULT NULL,
  `spouse_prefix` varchar(7) DEFAULT NULL,
  `spouse_firstname` varchar(60) DEFAULT NULL,
  `spouse_middlename` varchar(26) DEFAULT NULL,
  `spouse_familyname` varchar(27) DEFAULT NULL,
  `spouse_birth_date` varchar(15) DEFAULT NULL,
  `current_live_status` varchar(10) NOT NULL DEFAULT 'No',
  `spouse_current_addr` varchar(50) DEFAULT NULL,
  `spouse_current_addr_line` varchar(59) DEFAULT NULL,
  `spouse_current_city` varchar(20) DEFAULT NULL,
  `spouse_current_state` varchar(20) DEFAULT NULL,
  `spouse_current_zipcode` varchar(20) DEFAULT NULL,
  `spouse_current_country` varchar(50) DEFAULT NULL,
  `spouse_passport_no` varchar(20) DEFAULT NULL,
  `spouse_passport` varchar(100) DEFAULT NULL,
  `spouse_passport_back` varchar(100) DEFAULT NULL,
  `spouse_passport_approval` tinyint(1) NOT NULL DEFAULT 0,
  `spouse_travel_uk` varchar(5) DEFAULT NULL,
  `childrens` varchar(6) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `birthdate` varchar(15) DEFAULT NULL,
  `relation` varchar(50) DEFAULT NULL,
  `current_live` varchar(6) DEFAULT NULL,
  `travel_to_uk` varchar(6) DEFAULT NULL,
  `json_data` text DEFAULT NULL,
  `mother_firstname` varchar(100) DEFAULT NULL,
  `mother_lastname` varchar(100) DEFAULT NULL,
  `mother_birth` varchar(15) DEFAULT NULL,
  `father_firstname` varchar(100) DEFAULT NULL,
  `father_lastname` varchar(100) DEFAULT NULL,
  `father_birth` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nurse_dependant_det`
--

INSERT INTO `nurse_dependant_det` (`id`, `user_id`, `relatiion_status`, `spouse_prefix`, `spouse_firstname`, `spouse_middlename`, `spouse_familyname`, `spouse_birth_date`, `current_live_status`, `spouse_current_addr`, `spouse_current_addr_line`, `spouse_current_city`, `spouse_current_state`, `spouse_current_zipcode`, `spouse_current_country`, `spouse_passport_no`, `spouse_passport`, `spouse_passport_back`, `spouse_passport_approval`, `spouse_travel_uk`, `childrens`, `name`, `birthdate`, `relation`, `current_live`, `travel_to_uk`, `json_data`, `mother_firstname`, `mother_lastname`, `mother_birth`, `father_firstname`, `father_lastname`, `father_birth`) VALUES
(1, 106, 'Single', 'Father.', 'Jji', 'M', 'PJ', 'Aug 26, 20', 'Married', 'Street', 'Street2', 'City', 'State', '888877', 'Anguilla', '9898989', 'http://maxglobal.herdzo.com/files/ckcyc2agmofudzojmvz2/passport.jpg?s=130', NULL, 0, 'YES', 'NO', '', '', 'Son', 'Yes', 'Yes', '[{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"},{\"name\":\"\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"0\",\"travel_to_uk\":\"0\"}]', 'Mother', NULL, 'Aug 21, 20', 'Father', NULL, 'Aug 29, 20'),
(2, 132, 'Single', 'Mr', '', '', '', '', 'Yes', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', 'Son', 'Yes', 'Yes', NULL, 'Geetha', NULL, 'Oct 07, 19', 'Ajay', NULL, 'Oct 07, 19'),
(3, 195, 'Married', 'Mrs.', 'Seena', 'J', 'Ijas', 'Sep 23, 1994', 'Yes', '', '', '', '', '', '', 'dsfsdf54366', 'http://localhost/maxglobal/files/syjxrgvyyuzzlyygxhgc/ev-img01.jpg?s=130', 'http://localhost/maxglobal/files/m4uls5bhwhhgtbzt9tig/ev-img02.jpg?s=130', 0, 'YES', 'YES', 'Irfan', 'Jan 13, 2016', 'Son', 'Yes', 'Yes', '{\"1\":{\"name\":\"Sameer\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"Yes\",\"travel_to_uk\":\"No\"},\"3\":{\"name\":\"Shijin\",\"birthdate\":\"\",\"relation\":\"Son\",\"current_live\":\"No\",\"travel_to_uk\":\"No\"}}', 'Ayisha', NULL, 'Apr 17, 1970', 'Riyas', NULL, 'Jul 23, 1965'),
(4, 235, 'Married', 'Mr', 'Hari', 'P', 'Vijay', 'Mar 07, 1975', 'No', 'St45678', 'St12345', 'Attingal', 'Kerala', '678456', 'Ireland', 'PT34242', 'http://localhost/maxglobal/files/uwrm770s5yrbryqqbpgk/ev-img01.jpg?s=130', 'http://localhost/maxglobal/files/yryynwjsm2fbeeii3cua/ev-img02.jpg?s=130', 1, 'YES', 'YES', 'Anu P', 'May 08, 2014', 'Daughter', 'Yes', 'No', '{\"1\":{\"name\":\"Arya\",\"birthdate\":\"Oct 15, 2020\",\"relation\":\"Daughter\",\"current_live\":\"Yes\",\"travel_to_uk\":\"No\"}}', 'Anitha', NULL, 'Mar 10, 1960', 'Rajiv', NULL, 'Jan 12, 1960');

-- --------------------------------------------------------

--
-- Table structure for table `nurse_employement_history`
--

CREATE TABLE `nurse_employement_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `job_title` varchar(50) DEFAULT NULL,
  `org_name` varchar(50) DEFAULT NULL,
  `ward` varchar(15) DEFAULT NULL,
  `working_status` varchar(15) DEFAULT NULL,
  `employement_start_date` varchar(15) DEFAULT NULL,
  `employement_end_date` varchar(15) DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `pers_statement` text DEFAULT NULL,
  `json_data` text DEFAULT NULL,
  `cv_file_upload` text DEFAULT NULL,
  `cv_file_upload_approval` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nurse_employement_history`
--

INSERT INTO `nurse_employement_history` (`id`, `user_id`, `job_title`, `org_name`, `ward`, `working_status`, `employement_start_date`, `employement_end_date`, `remarks`, `pers_statement`, `json_data`, `cv_file_upload`, `cv_file_upload_approval`) VALUES
(1, 106, 'Staff Nurse', 'Muthoot', '', 'Yes', 'Aug 20, 20', 'Aug 28, 20', 'Remarksss', ' Personame statement. Personame statement. Personame statement. Personame statement. v Personame statement. Personame statement.Personame statement.Personame statement.', '[{\"job_title\":\"Staff Nurse2\",\"org_name\":\"Mims\",\"ward\":\"\",\"employement_start_date\":\"Aug 19, 2020\",\"employement_end_date\":\"Aug 29, 2020\",\"remarks\":\"Remarks22\",\"working_status\":\"Yes\"}]', 'http://maxglobal.herdzo.com/files/tzewput57wsl0gzly37c/MaxGlobalConsultants_website_Final_doc (1).pd', 0),
(2, 132, 'Job1', 'ORG123', '', 'No', 'Mar 04, 20', 'Jun 12, 20', 'Remarks', 'Personal stmt', NULL, 'Tulips.jpg', 0),
(3, 195, 'Nurse', 'SNS', '', 'No', 'Jan 06, 2020', 'Jan 06, 2022', 'Good', 'Personal stmnt', '[{\"job_title\":\"Staff Nurse\",\"org_name\":\"SSN\",\"ward\":\"\",\"employement_start_date\":\"\",\"employement_end_date\":\"\",\"remarks\":\"\",\"working_status\":\"\"}]', 'http://localhost/maxglobal/files/p6xjks9oxdhk1fxoiyxg/A Sample PDF.pdf,http://localhost/maxglobal/files/tpqt4hqrr1job6hxzt01/Get_Started_With_Smallpdf.pdf', 0),
(4, 235, 'job2', 'SNS', 'Ward 1', 'No', 'May 01, 2022', 'May 03, 2022', 'Rmk1', 'Personal STmt', '[{\"job_title\":\"job 3\",\"org_name\":\"TGS\",\"ward\":\"Ward 1\",\"employement_start_date\":\"May 10, 2022\",\"employement_end_date\":\"May 13, 2022\",\"remarks\":\"Rmk 3\",\"working_status\":\"No\"},{\"job_title\":\"job 4\",\"org_name\":\"TKL\",\"ward\":\"\",\"employement_start_date\":\"May 19, 2022\",\"employement_end_date\":\"May 21, 2022\",\"remarks\":\"Rmk 4\",\"working_status\":\"Yes\"}]', 'http://localhost/maxglobal/files/pslz3feg1l91ve0snby2/dummy.pdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nurse_employer_details`
--

CREATE TABLE `nurse_employer_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `main_agent` varchar(150) DEFAULT NULL,
  `work_site` varchar(150) DEFAULT NULL,
  `mode_interview` varchar(150) DEFAULT NULL,
  `mode_interview_text` varchar(150) DEFAULT NULL,
  `interview_schedule_date` varchar(150) DEFAULT NULL,
  `interview_schedule_time` varchar(150) DEFAULT NULL,
  `interview_status` varchar(150) DEFAULT NULL,
  `offer_letter_status` varchar(15) DEFAULT NULL,
  `offer_letter_date` varchar(150) DEFAULT NULL,
  `offer_letter_signed` varchar(150) DEFAULT NULL,
  `offer_letter_file` text DEFAULT NULL,
  `offer_letter_file_approval` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nurse_employer_details`
--

INSERT INTO `nurse_employer_details` (`id`, `user_id`, `main_agent`, `work_site`, `mode_interview`, `mode_interview_text`, `interview_schedule_date`, `interview_schedule_time`, `interview_status`, `offer_letter_status`, `offer_letter_date`, `offer_letter_signed`, `offer_letter_file`, `offer_letter_file_approval`) VALUES
(1, 132, 'Abhiram', 'Care-home', 'WhatsApp', '', 'Oct 01, 2020', '06:24 PM', 'Passed', 'Received', 'Oct 03, 2020', 'Oct 04, 2020', 'Tulips.jpg', 0),
(2, 195, 'Sneha', 'Care-home', 'Face-to-face', '', 'Jan 04, 2022', '03:15 PM', 'Passed', 'Accepted', 'Jan 10, 2022', 'Jan 11, 2022', 'http://localhost/maxglobal/files/6qdngncsifhl5ksqz8ys/Get_Started_With_Smallpdf.pdf,http://localhost/maxglobal/files/onu0vfb8hzse3zivgbth/A Sample PDF.pdf', 0),
(3, 235, 'Sneha', 'Care-home', 'Zoom', '', 'May 25, 2022', '05:30 PM', 'Passed', 'Waiting', 'May 27, 2022', 'May 28, 2022', 'http://localhost/maxglobal/files/fjxzivssztd1ylscie4m/A Sample PDF.pdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nurse_english_language`
--

CREATE TABLE `nurse_english_language` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `eng_lang_evidence` varchar(100) DEFAULT NULL,
  `eng_lang_qualif` varchar(50) DEFAULT NULL,
  `test_sittings` tinyint(3) DEFAULT NULL,
  `ielts_passed_date` varchar(15) DEFAULT NULL,
  `ielts_number` varchar(10) DEFAULT NULL,
  `ielts_certificate` text DEFAULT NULL,
  `ielts_certificate_approval` tinyint(1) NOT NULL DEFAULT 0,
  `validity_date` varchar(15) DEFAULT NULL,
  `ielts_file_upload` text DEFAULT NULL,
  `ielts_file_upload_approval` tinyint(1) NOT NULL DEFAULT 0,
  `ielts_country` varchar(50) DEFAULT NULL,
  `work_start_date` varchar(12) DEFAULT NULL,
  `work_end_date` varchar(12) DEFAULT NULL,
  `employer_addresss` varchar(200) DEFAULT NULL,
  `employer_email` varchar(25) DEFAULT NULL,
  `file_upload_nursing` varchar(100) DEFAULT NULL,
  `file_upload_nursing_approval` tinyint(1) NOT NULL DEFAULT 0,
  `reference_letter` varchar(50) DEFAULT NULL,
  `json_data` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nurse_english_language`
--

INSERT INTO `nurse_english_language` (`id`, `user_id`, `eng_lang_evidence`, `eng_lang_qualif`, `test_sittings`, `ielts_passed_date`, `ielts_number`, `ielts_certificate`, `ielts_certificate_approval`, `validity_date`, `ielts_file_upload`, `ielts_file_upload_approval`, `ielts_country`, `work_start_date`, `work_end_date`, `employer_addresss`, `employer_email`, `file_upload_nursing`, `file_upload_nursing_approval`, `reference_letter`, `json_data`) VALUES
(1, 132, 'My Practice', 'IELTS', 2, 'Oct 04, 20', '534554', 'Tulips.jpg', 0, 'Oct 22, 20', 'Tulips.jpg', 0, 'Antigua and Barbuda', '', '', '', '', '', 0, '', NULL),
(2, 195, 'An accepted English language test', 'OET', 2, 'Dec 15, 2021', 'TS4636363', 'http://localhost/maxglobal/files/hpcad9d6yeczsax5kk9h/Get_Started_With_Smallpdf.pdf', 0, 'Apr 30, 2022', 'http://localhost/maxglobal/files/i6qctgdnlva0ecdtxxfy/A Sample PDF.pdf', 0, 'Antigua and Barbuda', '', '', '', '', '', 0, '', NULL),
(3, 235, 'An accepted English language test', 'Min_1year', 1, 'May 19, 2022', 'TS3255626', 'http://localhost/maxglobal/files/xtjuiacg2417vjmnzhvc/dummy.pdf', 1, 'Jul 22, 2022', 'http://localhost/maxglobal/files/agilljt8zu9segyafao5/A Sample PDF.pdf', 1, 'Anguilla', 'May 01, 2022', 'May 10, 2022', 'test address111', 'emp1@yopmail.com', 'http://localhost/maxglobal/files/8vwswatxu3x0gz3zn3xv/dummy.pdf', 0, 'ref letter1', '[{\"ielts_country\":\"Antigua and Barbuda\",\"work_start_date\":\"May 21, 2022\",\"work_end_date\":\"May 25, 2022\",\"employer_addresss\":\"test address444\",\"employer_email\":\"emp4@yopmail.com\",\"reference_letter\":\"ref letter4\",\"file_upload_nursing\":\"http:\\/\\/localhost\\/maxglobal\\/files\\/xwbe0zxvnol7lmtrerfl\\/A Sample PDF.pdf\"},{\"ielts_country\":\"United Kingdom\",\"work_start_date\":\"Apr 11, 2022\",\"work_end_date\":\"May 11, 2022\",\"employer_addresss\":\"Address 555\",\"employer_email\":\"emp555@yopmail.com\",\"reference_letter\":\"ref letter 5\",\"file_upload_nursing\":\"http:\\/\\/localhost\\/maxglobal\\/files\\/eo42sq0tluhdccsk1efb\\/pdf-sample.pdf\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `nurse_feedback`
--

CREATE TABLE `nurse_feedback` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `feedback` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nurse_feedback`
--

INSERT INTO `nurse_feedback` (`id`, `user_id`, `feedback`) VALUES
(1, 106, 'feedback about maxx'),
(2, 132, 'feedback'),
(3, 195, 'Excellent'),
(4, 235, 'Sample feedback');

-- --------------------------------------------------------

--
-- Table structure for table `nurse_joining`
--

CREATE TABLE `nurse_joining` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `joining_date` varchar(20) NOT NULL,
  `rebate_period_inp` varchar(100) NOT NULL,
  `rebate_period_select` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nurse_joining`
--

INSERT INTO `nurse_joining` (`id`, `user_id`, `joining_date`, `rebate_period_inp`, `rebate_period_select`) VALUES
(1, 106, 'Aug 10, 2020', 'rebate text', '3-months'),
(2, 132, 'Oct 10, 2020', '5', '3-days'),
(3, 195, 'Jan 31, 2022', '2', '3-days'),
(4, 235, 'Jun 16, 2022', '2', '3-months'),
(5, 217, '', '4', '3-days');

-- --------------------------------------------------------

--
-- Table structure for table `nurse_nmc`
--

CREATE TABLE `nurse_nmc` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `nmc_appl_status` varchar(30) DEFAULT NULL,
  `declaration_name` varchar(100) DEFAULT NULL,
  `declaration_org` varchar(100) DEFAULT NULL,
  `declaration_addr` text DEFAULT NULL,
  `declaration_contact` varchar(15) DEFAULT NULL,
  `declaration_email` varchar(150) DEFAULT NULL,
  `pcc_certificate` varchar(100) DEFAULT NULL,
  `pcc_certificate_approval` tinyint(1) NOT NULL DEFAULT 0,
  `scope_country` varchar(100) DEFAULT NULL,
  `scope_council` varchar(25) DEFAULT NULL,
  `scope_file_upload` text DEFAULT NULL,
  `scope_file_upload_approval` tinyint(1) NOT NULL DEFAULT 0,
  `letter_invitation` varchar(5) DEFAULT NULL,
  `cbt_recieved_date` varchar(12) DEFAULT NULL,
  `cbt_attempt_date` varchar(12) DEFAULT NULL,
  `cbt_attempt_status` varchar(12) DEFAULT NULL,
  `cbt_passed_date` varchar(13) DEFAULT NULL,
  `cbt_attempts` int(11) DEFAULT NULL,
  `cbt_upload` text DEFAULT NULL,
  `cbt_upload_approval` tinyint(1) DEFAULT 0,
  `cbt_validity` varchar(25) DEFAULT NULL,
  `nmc_appl_date` varchar(15) DEFAULT NULL,
  `nmc_file_upload` text DEFAULT NULL,
  `nmc_file_upload_approval` tinyint(1) NOT NULL DEFAULT 0,
  `birth_cert_upload` text DEFAULT NULL,
  `birth_cert_upload_approval` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nurse_nmc`
--

INSERT INTO `nurse_nmc` (`id`, `user_id`, `nmc_appl_status`, `declaration_name`, `declaration_org`, `declaration_addr`, `declaration_contact`, `declaration_email`, `pcc_certificate`, `pcc_certificate_approval`, `scope_country`, `scope_council`, `scope_file_upload`, `scope_file_upload_approval`, `letter_invitation`, `cbt_recieved_date`, `cbt_attempt_date`, `cbt_attempt_status`, `cbt_passed_date`, `cbt_attempts`, `cbt_upload`, `cbt_upload_approval`, `cbt_validity`, `nmc_appl_date`, `nmc_file_upload`, `nmc_file_upload_approval`, `birth_cert_upload`, `birth_cert_upload_approval`) VALUES
(1, 195, 'IELTS-OET', 'Aswin', 'TNS', 'Street 555', '9656774188', 'aswin33344@yopmail.com', NULL, 0, 'India', 'MS Council', 'http://localhost/maxglobal/files/lvfisbqhxkzhaxauivsb/A Sample PDF.pdf', 0, '1', 'Jan 04, 2022', 'Dec 20, 2021', 'Cleared', 'Dec 30, 2021', 2, 'http://localhost/maxglobal/files/jbmvouyvlwyyeypebnlg/Get_Started_With_Smallpdf.pdf', 0, 'Mar 31, 2022', 'Feb 25, 2022', 'http://localhost/maxglobal/files/xpojxh3f3u94pfkwh4tn/A Sample PDF.pdf,http://localhost/maxglobal/files/1htfplfrqjvk3xw4qwkq/Get_Started_With_Smallpdf.pdf', 0, 'http://localhost/maxglobal/files/wznwtukoyviktp6amjz4/Get_Started_With_Smallpdf.pdf,http://localhost/maxglobal/files/hjprg8fapfrdhwlbdmuy/A Sample PDF.pdf', 0),
(2, 235, 'IELTS-OET', 'Ajith', 'SNCS', 'test address 355', '9656774188', 'ajith6677@yopmail.com', NULL, 0, 'Canada', 'MS Council', 'http://localhost/maxglobal/files/o9b7qzhct9yfrzzzttiw/A Sample PDF.pdf', 0, '1', 'Apr 01, 2022', 'Apr 02, 2022', 'Cleared', 'Apr 03, 2022', 2, 'http://localhost/maxglobal/files/gzbog6n9vybpfd9gja7b/dummy.pdf', 0, 'May 31, 2022', 'Jun 10, 2022', 'http://localhost/maxglobal/files/dc8a0sg7mzxvmhwzgmoq/pdf-sample.pdf', 1, 'http://localhost/maxglobal/files/lj7hjuodgd13pfzthupe/A Sample PDF.pdf', 0);

-- --------------------------------------------------------

--
-- Table structure for table `nurse_payments`
--

CREATE TABLE `nurse_payments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `max_invoice_status` varchar(20) NOT NULL,
  `employer_pay` varchar(15) DEFAULT NULL,
  `outstanding_pay` varchar(200) DEFAULT NULL,
  `max_inv_empl` text DEFAULT NULL,
  `sub_agents` varchar(200) DEFAULT NULL,
  `inv_sub_agent` varchar(200) DEFAULT NULL,
  `sub_pay_status` varchar(10) DEFAULT NULL,
  `outstanding_pay_subagent` varchar(250) DEFAULT NULL,
  `addl_docs` text NOT NULL,
  `addl_docs_approval` tinyint(1) DEFAULT NULL,
  `last_communication` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nurse_payments`
--

INSERT INTO `nurse_payments` (`id`, `user_id`, `max_invoice_status`, `employer_pay`, `outstanding_pay`, `max_inv_empl`, `sub_agents`, `inv_sub_agent`, `sub_pay_status`, `outstanding_pay_subagent`, `addl_docs`, `addl_docs_approval`, `last_communication`) VALUES
(1, 106, 'Not-Submitted', 'Received', 'Aug 20, 2020', '', '0', '0', 'PAID', '', '', NULL, 'Last communication details'),
(2, 132, 'Submitted', 'Received', '1234', '32423424', 'Subcontractors', 'sub agent', 'PAID', 'payment', 'Tulips.jpg', NULL, 'communication'),
(3, 195, 'Submitted', 'Received', '700', 'cfvfxget43645', 'Not-applicable', '', 'PAID', '900', 'http://localhost/maxglobal/files/kak1nxpkujjpko0rmyi8/A Sample PDF.pdf', NULL, 'Aswin'),
(4, 235, 'Submitted', 'Received', '7000', 'MX3453', 'Not-applicable', 'INV342424', 'UNPAID', '9000', 'http://localhost/maxglobal/files/zwnv5hgfo1phrbqi6dvu/A Sample PDF.pdf', 1, 'Last communication'),
(5, 217, 'Not-Applicable', 'Not-Applicable', '', '', 'Not-applicable', '', '', '', '', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `nurse_personal_information`
--

CREATE TABLE `nurse_personal_information` (
  `id` int(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  `profile_image` varchar(150) DEFAULT NULL,
  `title` varchar(10) NOT NULL,
  `first_name` text NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `sur_name` varchar(100) DEFAULT NULL,
  `nationality` varchar(50) NOT NULL,
  `gender` enum('MALE','FEMALE') NOT NULL DEFAULT 'MALE',
  `dob` varchar(30) DEFAULT NULL,
  `home_address` text DEFAULT NULL,
  `address_2` text DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `zip_code` varchar(20) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `correspondence_address_flag` varchar(7) DEFAULT NULL,
  `correspondence_addr_1` text DEFAULT NULL,
  `correspondence_addr_2` text DEFAULT NULL,
  `correspondence_city` varchar(50) DEFAULT NULL,
  `correspondence_state` varchar(50) DEFAULT NULL,
  `correspondence_zip_code` varchar(20) DEFAULT NULL,
  `correspondence_country` varchar(50) DEFAULT NULL,
  `lived_othe_addr` varchar(5) DEFAULT NULL,
  `other_address_1` text DEFAULT NULL,
  `other_address_2` text DEFAULT NULL,
  `other_city` varchar(40) DEFAULT NULL,
  `other_state` int(50) DEFAULT NULL,
  `other_zip_code` int(20) DEFAULT NULL,
  `other_country` varchar(50) DEFAULT NULL,
  `start_living_addr` varchar(20) DEFAULT NULL,
  `stop_living_addr` varchar(20) DEFAULT NULL,
  `json_data` text DEFAULT NULL,
  `mobile` varchar(15) NOT NULL,
  `whatsapp` varchar(15) DEFAULT NULL,
  `imo_no` varchar(11) DEFAULT NULL,
  `btm_number` varchar(11) DEFAULT NULL,
  `skype_id` varchar(25) DEFAULT NULL,
  `skype_picture` varchar(100) DEFAULT NULL,
  `passport_no` varchar(25) DEFAULT NULL,
  `date_issue` varchar(25) DEFAULT NULL,
  `date_expiry` varchar(25) DEFAULT NULL,
  `passport_upload` varchar(100) DEFAULT NULL,
  `passport_upload_approval` tinyint(2) NOT NULL DEFAULT 0,
  `passport_back` varchar(100) DEFAULT NULL,
  `any_disability` varchar(7) DEFAULT NULL,
  `disability_comment` text DEFAULT NULL,
  `age` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nurse_personal_information`
--

INSERT INTO `nurse_personal_information` (`id`, `user_id`, `profile_image`, `title`, `first_name`, `middle_name`, `sur_name`, `nationality`, `gender`, `dob`, `home_address`, `address_2`, `city`, `state`, `zip_code`, `country`, `email`, `correspondence_address_flag`, `correspondence_addr_1`, `correspondence_addr_2`, `correspondence_city`, `correspondence_state`, `correspondence_zip_code`, `correspondence_country`, `lived_othe_addr`, `other_address_1`, `other_address_2`, `other_city`, `other_state`, `other_zip_code`, `other_country`, `start_living_addr`, `stop_living_addr`, `json_data`, `mobile`, `whatsapp`, `imo_no`, `btm_number`, `skype_id`, `skype_picture`, `passport_no`, `date_issue`, `date_expiry`, `passport_upload`, `passport_upload_approval`, `passport_back`, `any_disability`, `disability_comment`, `age`) VALUES
(1, 104, '', 'Mr', 'reshma', '', 'raju', 'Indian', 'MALE', 'Aug 19, 1964', 'edanjalil', 'koruthodu p.o', 'KOTTAYAM', 'KERALA', '686503', 'India', '', '0', '', '', '', '', '', '', '0', '', '', '', 0, 0, '', '', '', NULL, '2147483647', '2147483647', '2147483647', '2147483647', '07459124631', '', 'm988779', 'Aug 24, 2020', 'Aug 06, 2020', '', 0, NULL, '0', '', ''),
(2, 106, '', 'Mr', 'Sijo', 'New', 'Philip', 'Indian', 'MALE', 'Feb 23, 1979', 'Kakanad', 'Kakanad2', 'Kochi', 'Kerala', '889988i', 'Angola', '', '0', '', '', '', '', '', '', '1', 'Street', 'Street2', 'City', 0, 889988, 'Anguilla', 'Aug 27, 2021', 'Aug 29, 2020', NULL, '88998899', '2147483647', '87878787', '2147483647', 'Skype', 'http://maxglobal.herdzo.com/files/3j9wgsrzsak9fqcwwxya/skype.png?s=130', '880999', 'Aug 25, 2020', 'Aug 28, 2020', 'http://maxglobal.herdzo.com/files/qs9orbsxfrydw8p2bm4m/passport.jpg?s=130', 0, NULL, '0', '', ''),
(3, 112, '', 'Mr', 'herzo_nurse', 'herzo_nurse', 'herzo_nurse', '', 'MALE', 'Sep 26, 2020', 'cdccdc', 'cdc', 'cdcd', 'cdccd', 'cdcdcdc', 'Argentina', '', 'YES', '', '', '', '', '', '', '0', '', '', '', 0, 0, '', '', '', NULL, '0', '0', '0', '0', '', '', 'fddfdfd', 'Sep 06, 2020', 'Sep 26, 2020', '', 0, '', 'YES', 'dsdssdsd', ''),
(4, 132, '1.png', 'Dr.', 'Aswathy', 'N', 'Vijay', 'India', 'FEMALE', 'Oct 15, 1986', 'St 3333', 'St5555', 'Tvm', 'Kerala', '695581', 'India', 'aswathy@yopmail.com', 'YES', '', '', '', '', '', '', '0', '', '', '', 0, 0, '', '', '', NULL, '2147483647', '2147483647', '0', '0', 'aswathy777', '1.png', '5345435345435', 'Apr 09, 2020', 'Oct 15, 2020', 'Tulips.jpg', 0, 'Tulips.jpg', 'NO', '', ''),
(5, 133, '2.png', 'Mr', 'Akhil', 'S', 'Raj', 'India', 'MALE', 'Oct 10, 1995', 'St 222', 'St 777', 'Tvm', 'Kerala', '695318', 'India', 'akhil@yopmail.com', 'YES', '', '', '', '', '', '', '0', '', '', '', 0, 0, '', '', '', NULL, '2147483647', '2147483647', '0', '0', 'akhil345', '2.png', '3543654633', 'Oct 25, 2018', 'Oct 16, 2020', 'Tulips.jpg', 0, 'Tulips.jpg', 'NO', '', ''),
(6, 195, '', 'Mr', 'Ajmal', 'N', 'Khan', 'India', 'MALE', 'Jul 26, 1990', 'Lake View', 'Street 555', 'Attingal', 'Kerala', '695678', 'India', 'ajmal333@yopmail.com', 'YES', '', '', '', '', '', '', '0', '', '', '', 0, 0, '', '', '', NULL, '9656774188', '9447034555', '475862586', '56789', 'amir3456', 'http://localhost/maxglobal/files/ynha8sasodtdaa9yukoo/ev-img01.jpg?s=130', '5678/567/678', 'Jan 17, 2019', 'Mar 18, 2022', 'http://localhost/maxglobal/files/lnbfa93lt5zl9fcbauay/ev-img02.jpg?s=130', 0, 'http://localhost/maxglobal/files/9thprvxncakxrxqiqsz7/ev-img03.jpg?s=130', 'NO', '', ''),
(7, 216, NULL, '', 'Keerthy', NULL, NULL, '', 'MALE', 'Wed 01, 1992', NULL, NULL, 'Attingal', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9656774188', '9447034555', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '29'),
(8, 217, 'http://localhost/maxglobal/files/0pzwlvfo4gyyk62zxl33/avatar-img.jpg?s=130', 'Mr', 'Anupama', '', '', '', 'MALE', 'Jul 20, 1994', 'St56474', 'St46363', 'Attingal', 'kerala', '678978', 'Ireland', '', 'NO', 'St35542', 'St463437', 'Attingal', 'Kerala', '678567', 'Australia', '1', '', '', '', 0, 0, '', '', '', NULL, '9656774188', '9678678789', 'IMO24141', 'BT666666', 'SK3525', 'http://localhost/maxglobal/files/fygqpye36ejgvpnl5ony/ev-img01.jpg?s=130', 'PT3255', 'Apr 05, 2022', 'Aug 26, 2022', 'http://localhost/maxglobal/files/nckuohf4cbvbrmnjaht8/ev-img03.jpg?s=130', 0, 'http://localhost/maxglobal/files/5jl4p56sgqhquh36ny3v/flower-img.jpg?s=130', 'NO', '', '27'),
(9, 235, 'http://localhost/maxglobal/files/x56dgoa5fdc6vurigqak/avatar-img.jpg?s=130', 'Mr', 'Sithara', 'M', 'Raj', 'Indian', 'FEMALE', 'Jul 16, 1992', 'St111', 'St222', 'Attingal', 'Kerala', '666666', 'India', 'sithara345@yopmail.com', 'YES', 'St333', 'St444', 'Nagercoil', 'Tamil Nadu', '777777', 'India', '1', 'St555', 'St666', 'Varkala', 0, 888888, 'India', 'Mar 01, 2022', 'May 17, 2022', '{\"1\":{\"other_address_1\":\"\",\"other_address_2\":\"\",\"other_city\":\"\",\"other_state\":\"\",\"other_zip_code\":\"\",\"other_country\":\"\",\"start_living_addr\":\"\",\"stop_living_addr\":\"\"}}', '9656774188', '9447034555', 'IMO3525', 'BT23425', 'SK32255', 'http://localhost/maxglobal/files/5lnvqp7ulzwfbdv8f7vs/ev-img01.jpg?s=130', 'PT423255', 'Mar 14, 2022', 'Aug 18, 2022', 'http://localhost/maxglobal/files/3srt5rlwzthrkwerlxfv/ev-img02.jpg?s=130', 1, 'http://localhost/maxglobal/files/jgq4kh9h0tzgswirnrzi/ev-img03.jpg?s=130', 'NO', '', ''),
(10, 246, NULL, '', 'Swathy', NULL, NULL, '', 'MALE', 'May 09, 1990', NULL, NULL, 'Tvm', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7908967856', '7908967856', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '32'),
(11, 252, NULL, '', 'Sarath', NULL, NULL, '', 'MALE', 'Apr 07, 1990', NULL, NULL, 'Varkala', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9656774188', '9447034555', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '32'),
(12, 253, NULL, '', 'Karthik', NULL, NULL, '', 'MALE', 'May 07, 1990', NULL, NULL, 'Varkala', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9656774188', '9447034555', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '32'),
(13, 276, NULL, '', 'Anitha', NULL, NULL, '', 'MALE', 'Jun 30, 1994', NULL, NULL, 'Attingal', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9656774188', '9447034555', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '28'),
(14, 281, NULL, '', 'Simi', NULL, NULL, '', 'MALE', 'Nov 16, 1994', NULL, NULL, 'Attingal', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9656774188', '6789678967', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '27'),
(15, 282, NULL, '', 'Maya', NULL, NULL, '', 'MALE', 'Mar 15, 1994', NULL, NULL, 'Attingal', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9656774188', '6789078908', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '28'),
(16, 293, NULL, '', 'Soumya', NULL, NULL, '', 'MALE', 'Nov 22, 1984', NULL, NULL, 'Attingal', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9656774188', '7897895675', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '37'),
(17, 294, NULL, '', 'Seena', NULL, NULL, '', 'MALE', 'Oct 16, 1991', NULL, NULL, 'Attingal', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9656774188', '7890678545', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '30');

-- --------------------------------------------------------

--
-- Table structure for table `nurse_preferences`
--

CREATE TABLE `nurse_preferences` (
  `id` int(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  `preferred_sector` varchar(15) DEFAULT NULL,
  `preferred_designation` varchar(300) DEFAULT NULL,
  `preferred_location` varchar(200) NOT NULL,
  `preferred_salary` bigint(20) NOT NULL,
  `preferred_hours` varchar(20) NOT NULL,
  `preferred_shifts` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nurse_preferences`
--

INSERT INTO `nurse_preferences` (`id`, `user_id`, `preferred_sector`, `preferred_designation`, `preferred_location`, `preferred_salary`, `preferred_hours`, `preferred_shifts`) VALUES
(1, 106, 'NHS', 'Staff  Nurse', 'London', 1233000, '8', 'Day'),
(2, 132, 'NHS', 'Nurse', 'Tvm', 35000, '8', '2'),
(3, 195, 'NHS', 'Staff Nurse', 'Canada', 20, '9 hrs', '2'),
(4, 235, 'Nursing home', 'Staff Nurse', 'Canada', 20, '9 hrs', '2');

-- --------------------------------------------------------

--
-- Table structure for table `nurse_qualifications`
--

CREATE TABLE `nurse_qualifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `qualification` varchar(100) DEFAULT NULL,
  `date_awarded` varchar(15) DEFAULT NULL,
  `student_ref_no` varchar(20) DEFAULT NULL,
  `institution` varchar(100) DEFAULT NULL,
  `qualify_register` tinyint(1) NOT NULL DEFAULT 0,
  `certificate_upload` text DEFAULT NULL,
  `certificate_upload_name` varchar(50) NOT NULL,
  `certificate_upload_approval` tinyint(1) NOT NULL DEFAULT 0,
  `confirm_name` varchar(100) DEFAULT NULL,
  `json_data` text DEFAULT NULL,
  `different_first_name` varchar(20) DEFAULT NULL,
  `different_last_name` varchar(20) DEFAULT NULL,
  `reasons` text DEFAULT NULL,
  `evidence_upload` text DEFAULT NULL,
  `evidence_upload_approval` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nurse_qualifications`
--

INSERT INTO `nurse_qualifications` (`id`, `user_id`, `qualification`, `date_awarded`, `student_ref_no`, `institution`, `qualify_register`, `certificate_upload`, `certificate_upload_name`, `certificate_upload_approval`, `confirm_name`, `json_data`, `different_first_name`, `different_last_name`, `reasons`, `evidence_upload`, `evidence_upload_approval`) VALUES
(1, 106, 'BCOM', 'Aug 28, 2020', '777676', 'Anna University', 0, 'http://maxglobal.herdzo.com/files/6sdufeq8fp7ag354sc0u/Student Changes.odt,http://maxglobal.herdzo.c', 'BCOM Certificate', 0, 'BCOMM', NULL, 'Sijo', 'Ph', 'Certficate name are diiferent', 'http://maxglobal.herdzo.com/files/qm855xafbrvzpidrfhum/candidate_rearrange_order.odt,http://maxgloba', 0),
(2, 133, 'BSC', 'Oct 07, 2020', '23253453', 'TKM', 0, 'Tulips.jpg', 'Certificate', 0, 'BSC', NULL, 'Akhil', 'Raj', 'reasons', 'Tulips.jpg', 0),
(3, 195, 'MSC', 'Jan 03, 2022', 'ST5647535', 'SNC', 0, 'http://localhost/maxglobal/files/zo07coy7gftwoiyhthgb/Get_Started_With_Smallpdf.pdf,http://localhost', 'MSC Certificate', 0, 'MSC', NULL, 'Amir', 'Khan', 'Reason', 'http://localhost/maxglobal/files/ofb10mxmaqer51ualj5m/A Sample PDF.pdf', 0),
(4, 235, 'MSc', 'May 02, 2022', 'ST55555', 'SSN', 1, 'http://localhost/maxglobal/files/r9jhds0ucqgmuvdbgamy/A Sample PDF.pdf', 'MSC Certificate', 1, 'MSC', NULL, 'Sitara', 'R', 'Reasons', 'http://localhost/maxglobal/files/1rxfeozkblnpizdqxvrq/dummy.pdf', 0);

-- --------------------------------------------------------

--
-- Table structure for table `nurse_visa_imigration`
--

CREATE TABLE `nurse_visa_imigration` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `in_uk` varchar(5) DEFAULT NULL,
  `issued_uk_visa` varchar(5) DEFAULT NULL,
  `other_countries` varchar(5) DEFAULT NULL,
  `pc_country` varchar(70) DEFAULT NULL,
  `visit_reason` varchar(15) DEFAULT NULL,
  `pc_entry_date` varchar(15) DEFAULT NULL,
  `pc_exit_date` varchar(15) DEFAULT NULL,
  `crim_record_cert` varchar(5) DEFAULT NULL,
  `pcc_clearance_cert` varchar(100) DEFAULT NULL,
  `pcc_clearance_cert_approval` tinyint(1) NOT NULL DEFAULT 0,
  `json_data` text DEFAULT NULL,
  `visa_category` varchar(20) DEFAULT NULL,
  `appl_form` varchar(100) DEFAULT NULL,
  `appl_form_approval` tinyint(1) DEFAULT NULL,
  `refer_file` varchar(100) DEFAULT NULL,
  `refer_file_approval` tinyint(1) DEFAULT NULL,
  `pers_status` enum('Applicable','Not-Applicable') NOT NULL DEFAULT 'Not-Applicable',
  `pers_stat_text` text NOT NULL,
  `pers_stat_file` varchar(100) NOT NULL,
  `pers_stat_file_approval` int(11) NOT NULL,
  `pcc_status` varchar(10) DEFAULT NULL,
  `pcc_recieved_date` varchar(20) NOT NULL,
  `pcc_file_upload` varchar(100) NOT NULL,
  `pcc_file_upload_approval` tinyint(1) DEFAULT NULL,
  `tb_test_date` varchar(20) DEFAULT NULL,
  `tb_file_upload` varchar(100) DEFAULT NULL,
  `tb_file_upload_approval` tinyint(1) DEFAULT NULL,
  `spons_status` varchar(10) DEFAULT NULL,
  `spons_date` varchar(20) NOT NULL,
  `spons_file_upload` varchar(100) NOT NULL,
  `spons_file_upload_approval` tinyint(1) DEFAULT NULL,
  `visa_applied` varchar(5) DEFAULT NULL,
  `visa_status` varchar(20) DEFAULT NULL,
  `visa_appt_date` varchar(15) DEFAULT NULL,
  `visa_reciev_date` varchar(15) DEFAULT NULL,
  `visa_exp_date` varchar(15) DEFAULT NULL,
  `visa_file` varchar(100) DEFAULT NULL,
  `visa_file_approval` tinyint(1) NOT NULL DEFAULT 0,
  `visa_appr_file` varchar(100) DEFAULT NULL,
  `visa_appr_file_approval` tinyint(1) NOT NULL DEFAULT 0,
  `flight_tkt_status` varchar(12) DEFAULT NULL,
  `flight_ticket` varchar(100) DEFAULT NULL,
  `flight_ticket_approval` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nurse_visa_imigration`
--

INSERT INTO `nurse_visa_imigration` (`id`, `user_id`, `in_uk`, `issued_uk_visa`, `other_countries`, `pc_country`, `visit_reason`, `pc_entry_date`, `pc_exit_date`, `crim_record_cert`, `pcc_clearance_cert`, `pcc_clearance_cert_approval`, `json_data`, `visa_category`, `appl_form`, `appl_form_approval`, `refer_file`, `refer_file_approval`, `pers_status`, `pers_stat_text`, `pers_stat_file`, `pers_stat_file_approval`, `pcc_status`, `pcc_recieved_date`, `pcc_file_upload`, `pcc_file_upload_approval`, `tb_test_date`, `tb_file_upload`, `tb_file_upload_approval`, `spons_status`, `spons_date`, `spons_file_upload`, `spons_file_upload_approval`, `visa_applied`, `visa_status`, `visa_appt_date`, `visa_reciev_date`, `visa_exp_date`, `visa_file`, `visa_file_approval`, `visa_appr_file`, `visa_appr_file_approval`, `flight_tkt_status`, `flight_ticket`, `flight_ticket_approval`) VALUES
(1, 132, 'YES', 'YES', 'YES', 'Greenland', 'Work', 'Apr 16, 20', 'Jun 18, 20', 'Yes', 'Tulips.jpg', 0, NULL, 'CITIZENSHIP', 'Tulips.jpg', NULL, 'Tulips.jpg', NULL, 'Not-Applicable', '', '', 0, 'Received', 'Oct 01, 2020', 'Tulips.jpg', NULL, 'Oct 05, 2020', 'Tulips.jpg', NULL, 'Received', '', '', NULL, 'YES', 'Submitted Applicatio', 'Oct 05, 20', 'Oct 06, 20', 'Oct 22, 20', 'Tulips.jpg', 0, 'Tulips.jpg', 0, 'Received', 'Tulips.jpg', 0),
(2, 195, 'YES', 'YES', 'YES', 'Belarus', 'Tourism', 'Nov 03, 2020', 'Nov 30, 2020', 'Yes', 'http://localhost/maxglobal/files/mpm8ig6v2lwj5dyvkysa/A Sample PDF.pdf', 0, '[{\"pc_country\":\"Angola\",\"visit_reason\":\"\",\"pc_entry_date\":\"\",\"pc_exit_date\":\"\",\"crim_record_cert\":\"Yes\",\"pcc_clearance_cert\":\"http:\\/\\/localhost\\/maxglobal\\/files\\/to5p5gfbpqtarhbijtau\\/A Sample PDF.pdf\"},{\"pc_country\":\"Burundi\",\"visit_reason\":\"\",\"pc_entry_date\":\"\",\"pc_exit_date\":\"\",\"crim_record_cert\":\"Yes\",\"pcc_clearance_cert\":\"http:\\/\\/localhost\\/maxglobal\\/files\\/iiearsbx76hc0wbpcjj3\\/Get_Started_With_Smallpdf.pdf\"}]', '', '', NULL, '', NULL, '', '', '', 0, '', '', '', NULL, '', '', NULL, 'Received', '', '', NULL, '', '', '', '', '', '', 0, '', 0, '', '', 0),
(3, 235, 'NO', 'NO', 'NO', 'India', 'Work', 'May 01, 2022', 'May 03, 2022', 'Yes', 'http://localhost/maxglobal/files/9zlk09t8d8txc4vxdkvf/A Sample PDF.pdf', 0, '[{\"pc_country\":\"Canada\",\"visit_reason\":\"Tourism\",\"pc_entry_date\":\"May 05, 2022\",\"pc_exit_date\":\"May 10, 2022\",\"crim_record_cert\":\"Yes\",\"pcc_clearance_cert\":\"\"}]', 'SPONSORSHIP-Tier', 'http://localhost/maxglobal/files/uahcku31ufxzt3kyaxqt/pdf-sample.pdf', 1, 'http://localhost/maxglobal/files/yrf7ijsmko5xh7fhfggh/A Sample PDF.pdf', 0, 'Applicable', 'pstrfdgzfgxx', '', 0, 'Received', 'May 11, 2022', 'http://localhost/maxglobal/files/lejn1yaxmcdbcwlyrvpv/dummy.pdf', 1, 'May 13, 2022', 'http://localhost/maxglobal/files/a5yino50yvpiszzoggbs/pdf-sample.pdf', 0, 'Received', 'May 17, 2022', 'http://localhost/maxglobal/files/5frnkktoonia9fz6hdxr/A Sample PDF.pdf', 1, 'YES', 'Submitted Applicatio', 'May 23, 2022', 'May 20, 2022', 'Jun 30, 2022', 'http://localhost/maxglobal/files/x2lnjzfuruzfbstiry5l/dummy.pdf', 1, 'http://localhost/maxglobal/files/leymuby7m9knfmwg3cpc/pdf-sample.pdf', 1, 'Received', 'http://localhost/maxglobal/files/rnwergmclr0rqthw1mgd/A Sample PDF.pdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'http://',
  `assigned_to` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `connect_since` date NOT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `profile_image` int(11) NOT NULL,
  `profile` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('consultant@yopmail.com', '7129dbdfba054709becc820a3d63afbd85af9937b3cc77cdd24c03af50cb682b', '2020-01-14 17:29:36'),
('burhanam2050@gmail.com', 'fddf56be6163c6ff7b7c34dc5fc14a897e2acc84070e1eece18acae9b04e617f', '2020-01-30 16:46:04'),
('mainagent@yopmail.com', 'c9699ede7996145a240eae13d1375f2b97a19d6972f9c9fdc8bd4f432ea088c6', '2020-02-01 14:11:42'),
('rithunraj333@gmail.com', 'b13917b71dcb7cd00e435f58d193d119c961f7a929b8e1ae589ef23d28652704', '2020-02-02 12:38:53'),
('philipsijoo@gmail.com', '653592b5ffbbfcb5623430ba22f4ee3d412ceb88e8b07b8f5446faffcf8736fb', '2020-08-09 05:47:53'),
('abdul.samad086@gmail.com', '73d5d3ad5c65da97565b48b59a9f86ec71a3012da304a5d25a08113c1fb0d09a', '2020-08-23 06:02:50'),
('anvrnazr@gmail.com', '4f7c8b7418c403557d7458e0b8cd915b36bf9fa096f813dd70bf8828aa427eae', '2020-09-02 06:40:33'),
('amijijo@gmail.com', '4459d224db960983796d7778534c73bbdf7a1d11bd9efb0b5a9d83d9ac5e513e', '2021-03-20 17:16:34'),
('scholab.admin@yopmail.com', '18caa12765bdd865c4e31b0ba57ff60bc976cd3327ce3ea5eca2d0c416a1d779', '2022-01-07 23:28:04');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN_PANEL', 'Admin Panel', 'Admin Panel Permission', NULL, '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(2, 'aaaaaaaaaaa', 'test', 'test', NULL, '2019-11-10 06:20:08', '2019-11-10 06:20:08');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(2, 1),
(2, 3),
(2, 4),
(2, 5),
(2, 6);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `dept` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `parent`, `dept`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'SUPER_ADMIN', 'Super Admin', 'Full Access Role', 1, 1, NULL, '2019-11-08 12:05:03', '2019-11-08 12:05:03'),
(2, 'MANAGER', 'Manager', '', 1, 1, NULL, '2019-11-10 06:17:40', '2019-12-21 06:43:52'),
(3, 'CONSULTANTS', 'Consultants', '', 1, 1, NULL, '2019-12-14 09:23:48', '2020-01-01 08:37:27'),
(4, 'MAIN_AGENTS', 'MAIN_AGENTS', '', 1, 1, NULL, '2019-12-14 10:02:09', '2020-01-01 08:37:08'),
(5, 'UNIVERSITY_USER', 'UNIVERSITY_USER', '', 1, 1, NULL, '2019-12-14 11:38:10', '2020-01-01 08:36:57'),
(6, 'SUBCONTRACTORS', 'SUBCONTRACTORS', '', 1, 1, NULL, '2019-12-14 11:39:28', '2020-01-01 08:36:46'),
(7, 'EMPLOYER_ROLE', 'EMPLOYER_ROLE', '', 1, 1, NULL, '2020-01-01 08:36:25', '2020-01-01 08:36:25'),
(8, 'STUDENT', 'STUDENT', 'STUDENT', 1, 1, NULL, '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(9, 'NURSE', 'NURSE', 'NURSE', 1, 1, NULL, '2020-04-22 16:22:24', '2020-04-22 16:22:24');

-- --------------------------------------------------------

--
-- Table structure for table `role_module`
--

CREATE TABLE `role_module` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `acc_view` tinyint(1) NOT NULL,
  `acc_create` tinyint(1) NOT NULL,
  `acc_edit` tinyint(1) NOT NULL,
  `acc_delete` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_module`
--

INSERT INTO `role_module` (`id`, `role_id`, `module_id`, `acc_view`, `acc_create`, `acc_edit`, `acc_delete`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, '2019-11-08 12:05:04', '2019-11-08 12:05:04'),
(2, 1, 2, 1, 1, 1, 1, '2019-11-08 12:05:05', '2019-11-08 12:05:05'),
(3, 1, 3, 1, 1, 1, 1, '2019-11-08 12:05:07', '2019-11-08 12:05:07'),
(4, 1, 4, 1, 1, 1, 1, '2019-11-08 12:05:07', '2019-11-08 12:05:07'),
(5, 1, 5, 1, 1, 1, 1, '2019-11-08 12:05:07', '2019-11-08 12:05:07'),
(6, 1, 6, 1, 1, 1, 1, '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(7, 1, 7, 1, 1, 1, 1, '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(8, 1, 8, 1, 1, 1, 1, '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(9, 2, 1, 1, 1, 1, 1, '2019-11-10 06:17:40', '2019-11-10 06:17:40'),
(10, 2, 2, 1, 1, 1, 1, '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(11, 2, 3, 1, 1, 1, 1, '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(12, 2, 4, 1, 1, 1, 1, '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(13, 2, 5, 1, 1, 1, 1, '2019-11-10 06:17:42', '2019-11-10 06:17:42'),
(14, 2, 6, 1, 1, 1, 1, '2019-11-10 06:17:42', '2019-11-10 06:17:42'),
(15, 2, 7, 1, 1, 1, 1, '2019-11-10 06:17:43', '2019-11-10 06:17:43'),
(16, 2, 8, 1, 1, 1, 1, '2019-11-10 06:17:43', '2019-11-10 06:17:43'),
(17, 3, 1, 1, 1, 1, 1, '2019-12-14 09:23:48', '2019-12-14 09:23:48'),
(18, 3, 2, 1, 1, 1, 1, '2019-12-14 09:23:48', '2019-12-14 09:23:48'),
(19, 3, 3, 1, 1, 1, 1, '2019-12-14 09:23:49', '2019-12-14 09:23:49'),
(20, 3, 4, 1, 1, 1, 1, '2019-12-14 09:23:49', '2019-12-14 09:23:49'),
(21, 3, 5, 1, 1, 1, 1, '2019-12-14 09:23:50', '2019-12-14 09:23:50'),
(22, 3, 6, 1, 1, 1, 1, '2019-12-14 09:23:50', '2019-12-14 09:23:50'),
(23, 3, 7, 1, 1, 1, 1, '2019-12-14 09:23:51', '2019-12-14 09:23:51'),
(24, 3, 8, 1, 1, 1, 1, '2019-12-14 09:23:51', '2019-12-14 09:23:51'),
(25, 4, 1, 1, 1, 1, 1, '2019-12-14 10:02:09', '2019-12-14 10:02:09'),
(26, 4, 2, 1, 1, 1, 1, '2019-12-14 10:02:09', '2019-12-14 10:02:09'),
(27, 4, 3, 1, 1, 1, 1, '2019-12-14 10:02:10', '2019-12-14 10:02:10'),
(28, 4, 4, 1, 1, 1, 1, '2019-12-14 10:02:10', '2019-12-14 10:02:10'),
(29, 4, 5, 1, 1, 1, 1, '2019-12-14 10:02:11', '2019-12-14 10:02:11'),
(30, 4, 6, 1, 1, 1, 1, '2019-12-14 10:02:11', '2019-12-14 10:02:11'),
(31, 4, 7, 1, 1, 1, 1, '2019-12-14 10:02:12', '2019-12-14 10:02:12'),
(32, 4, 8, 1, 1, 1, 1, '2019-12-14 10:02:12', '2019-12-14 10:02:12'),
(33, 5, 1, 1, 1, 1, 1, '2019-12-14 11:38:10', '2019-12-14 11:38:10'),
(34, 5, 2, 1, 1, 1, 1, '2019-12-14 11:38:11', '2019-12-14 11:38:11'),
(35, 5, 3, 1, 1, 1, 1, '2019-12-14 11:38:12', '2019-12-14 11:38:12'),
(36, 5, 4, 1, 1, 1, 1, '2019-12-14 11:38:12', '2019-12-14 11:38:12'),
(37, 5, 5, 1, 1, 1, 1, '2019-12-14 11:38:13', '2019-12-14 11:38:13'),
(38, 5, 6, 1, 1, 1, 1, '2019-12-14 11:38:13', '2019-12-14 11:38:13'),
(39, 5, 7, 1, 1, 1, 1, '2019-12-14 11:38:14', '2019-12-14 11:38:14'),
(40, 5, 8, 1, 1, 1, 1, '2019-12-14 11:38:14', '2019-12-14 11:38:14'),
(41, 6, 1, 1, 1, 1, 1, '2019-12-14 11:39:28', '2019-12-14 11:39:28'),
(42, 6, 2, 1, 1, 1, 1, '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(43, 6, 3, 1, 1, 1, 1, '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(44, 6, 4, 1, 1, 1, 1, '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(45, 6, 5, 1, 1, 1, 1, '2019-12-14 11:39:30', '2019-12-14 11:39:30'),
(46, 6, 6, 1, 1, 1, 1, '2019-12-14 11:39:30', '2019-12-14 11:39:30'),
(47, 6, 7, 1, 1, 1, 1, '2019-12-14 11:39:31', '2019-12-14 11:39:31'),
(48, 6, 8, 1, 1, 1, 1, '2019-12-14 11:39:31', '2019-12-14 11:39:31'),
(49, 7, 1, 1, 1, 1, 1, '2020-01-01 08:36:25', '2020-01-01 08:36:25'),
(50, 7, 2, 1, 1, 1, 1, '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(51, 7, 3, 1, 1, 1, 1, '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(52, 7, 4, 1, 1, 1, 1, '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(53, 7, 5, 1, 1, 1, 1, '2020-01-01 08:36:27', '2020-01-01 08:36:27'),
(54, 7, 6, 1, 1, 1, 1, '2020-01-01 08:36:27', '2020-01-01 08:36:27'),
(55, 7, 7, 1, 1, 1, 1, '2020-01-01 08:36:28', '2020-01-01 08:36:28'),
(56, 7, 8, 1, 1, 1, 1, '2020-01-01 08:36:28', '2020-01-01 08:36:28'),
(57, 8, 1, 1, 1, 1, 1, '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(58, 8, 2, 1, 1, 1, 1, '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(59, 8, 3, 1, 1, 1, 1, '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(60, 8, 4, 1, 1, 1, 1, '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(61, 8, 5, 1, 1, 1, 1, '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(62, 8, 6, 1, 1, 1, 1, '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(63, 8, 7, 1, 1, 1, 1, '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(64, 8, 8, 1, 1, 1, 1, '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(65, 9, 1, 1, 1, 1, 1, '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(66, 9, 2, 1, 1, 1, 1, '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(67, 9, 3, 1, 1, 1, 1, '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(68, 9, 4, 1, 1, 1, 1, '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(69, 9, 5, 1, 1, 1, 1, '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(70, 9, 6, 1, 1, 1, 1, '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(71, 9, 7, 1, 1, 1, 1, '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(72, 9, 8, 1, 1, 1, 1, '2020-04-22 16:22:24', '2020-04-22 16:22:24');

-- --------------------------------------------------------

--
-- Table structure for table `role_module_fields`
--

CREATE TABLE `role_module_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `field_id` int(10) UNSIGNED NOT NULL,
  `access` enum('invisible','readonly','write') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_module_fields`
--

INSERT INTO `role_module_fields` (`id`, `role_id`, `field_id`, `access`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'write', '2019-11-08 12:05:04', '2019-11-08 12:05:04'),
(2, 1, 2, 'write', '2019-11-08 12:05:04', '2019-11-08 12:05:04'),
(3, 1, 3, 'write', '2019-11-08 12:05:04', '2019-11-08 12:05:04'),
(4, 1, 4, 'write', '2019-11-08 12:05:04', '2019-11-08 12:05:04'),
(5, 1, 5, 'write', '2019-11-08 12:05:04', '2019-11-08 12:05:04'),
(6, 1, 6, 'write', '2019-11-08 12:05:05', '2019-11-08 12:05:05'),
(7, 1, 7, 'write', '2019-11-08 12:05:05', '2019-11-08 12:05:05'),
(8, 1, 8, 'write', '2019-11-08 12:05:05', '2019-11-08 12:05:05'),
(9, 1, 9, 'write', '2019-11-08 12:05:05', '2019-11-08 12:05:05'),
(10, 1, 10, 'write', '2019-11-08 12:05:05', '2019-11-08 12:05:05'),
(11, 1, 11, 'write', '2019-11-08 12:05:05', '2019-11-08 12:05:05'),
(12, 1, 12, 'write', '2019-11-08 12:05:05', '2019-11-08 12:05:05'),
(16, 1, 16, 'write', '2019-11-08 12:05:07', '2019-11-08 12:05:07'),
(17, 1, 17, 'write', '2019-11-08 12:05:07', '2019-11-08 12:05:07'),
(18, 1, 18, 'write', '2019-11-08 12:05:07', '2019-11-08 12:05:07'),
(19, 1, 19, 'write', '2019-11-08 12:05:07', '2019-11-08 12:05:07'),
(20, 1, 20, 'write', '2019-11-08 12:05:07', '2019-11-08 12:05:07'),
(21, 1, 21, 'write', '2019-11-08 12:05:07', '2019-11-08 12:05:07'),
(22, 1, 22, 'write', '2019-11-08 12:05:07', '2019-11-08 12:05:07'),
(23, 1, 23, 'write', '2019-11-08 12:05:07', '2019-11-08 12:05:07'),
(24, 1, 24, 'write', '2019-11-08 12:05:07', '2019-11-08 12:05:07'),
(25, 1, 25, 'write', '2019-11-08 12:05:07', '2019-11-08 12:05:07'),
(26, 1, 26, 'write', '2019-11-08 12:05:07', '2019-11-08 12:05:07'),
(30, 1, 30, 'write', '2019-11-08 12:05:07', '2019-11-08 12:05:07'),
(31, 1, 31, 'write', '2019-11-08 12:05:07', '2019-11-08 12:05:07'),
(32, 1, 32, 'write', '2019-11-08 12:05:07', '2019-11-08 12:05:07'),
(33, 1, 33, 'write', '2019-11-08 12:05:07', '2019-11-08 12:05:07'),
(34, 1, 34, 'write', '2019-11-08 12:05:07', '2019-11-08 12:05:07'),
(35, 1, 35, 'write', '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(36, 1, 36, 'write', '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(37, 1, 37, 'write', '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(38, 1, 38, 'write', '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(39, 1, 39, 'write', '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(40, 1, 40, 'write', '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(41, 1, 41, 'write', '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(42, 1, 42, 'write', '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(43, 1, 43, 'write', '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(44, 1, 44, 'write', '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(45, 1, 45, 'write', '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(46, 1, 46, 'write', '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(47, 1, 47, 'write', '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(48, 1, 48, 'write', '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(49, 1, 49, 'write', '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(50, 1, 50, 'write', '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(51, 1, 51, 'write', '2019-11-08 12:05:08', '2019-11-08 12:05:08'),
(52, 2, 1, 'write', '2019-11-10 06:17:40', '2019-11-10 06:17:40'),
(53, 2, 2, 'write', '2019-11-10 06:17:40', '2019-11-10 06:17:40'),
(54, 2, 3, 'write', '2019-11-10 06:17:40', '2019-11-10 06:17:40'),
(55, 2, 4, 'write', '2019-11-10 06:17:40', '2019-11-10 06:17:40'),
(56, 2, 5, 'write', '2019-11-10 06:17:40', '2019-11-10 06:17:40'),
(57, 2, 6, 'write', '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(58, 2, 7, 'write', '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(59, 2, 8, 'write', '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(60, 2, 9, 'write', '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(61, 2, 10, 'write', '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(62, 2, 11, 'write', '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(63, 2, 12, 'write', '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(67, 2, 16, 'write', '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(68, 2, 17, 'invisible', '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(69, 2, 18, 'write', '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(70, 2, 19, 'write', '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(71, 2, 20, 'write', '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(72, 2, 21, 'write', '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(73, 2, 22, 'invisible', '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(74, 2, 23, 'write', '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(75, 2, 24, 'write', '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(76, 2, 25, 'write', '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(77, 2, 26, 'write', '2019-11-10 06:17:41', '2019-11-10 06:17:41'),
(81, 2, 30, 'readonly', '2019-11-10 06:17:42', '2019-11-10 06:17:42'),
(82, 2, 31, 'readonly', '2019-11-10 06:17:42', '2019-11-10 06:17:42'),
(83, 2, 32, 'readonly', '2019-11-10 06:17:42', '2019-11-10 06:17:42'),
(84, 2, 33, 'readonly', '2019-11-10 06:17:42', '2019-11-10 06:17:42'),
(85, 2, 34, 'readonly', '2019-11-10 06:17:42', '2019-11-10 06:17:42'),
(86, 2, 35, 'readonly', '2019-11-10 06:17:42', '2019-11-10 06:17:42'),
(87, 2, 36, 'readonly', '2019-11-10 06:17:42', '2019-11-10 06:17:42'),
(88, 2, 37, 'readonly', '2019-11-10 06:17:42', '2019-11-10 06:17:42'),
(89, 2, 38, 'readonly', '2019-11-10 06:17:42', '2019-11-10 06:17:42'),
(90, 2, 39, 'readonly', '2019-11-10 06:17:42', '2019-11-10 06:17:42'),
(91, 2, 40, 'readonly', '2019-11-10 06:17:42', '2019-11-10 06:17:42'),
(92, 2, 41, 'readonly', '2019-11-10 06:17:42', '2019-11-10 06:17:42'),
(93, 2, 42, 'readonly', '2019-11-10 06:17:42', '2019-11-10 06:17:42'),
(94, 2, 43, 'readonly', '2019-11-10 06:17:42', '2019-11-10 06:17:42'),
(95, 2, 44, 'readonly', '2019-11-10 06:17:42', '2019-11-10 06:17:42'),
(96, 2, 45, 'readonly', '2019-11-10 06:17:42', '2019-11-10 06:17:42'),
(97, 2, 46, 'readonly', '2019-11-10 06:17:43', '2019-11-10 06:17:43'),
(98, 2, 47, 'readonly', '2019-11-10 06:17:43', '2019-11-10 06:17:43'),
(99, 2, 48, 'readonly', '2019-11-10 06:17:43', '2019-11-10 06:17:43'),
(100, 2, 49, 'write', '2019-11-10 06:17:43', '2019-11-10 06:17:43'),
(101, 2, 50, 'write', '2019-11-10 06:17:43', '2019-11-10 06:17:43'),
(102, 2, 51, 'write', '2019-11-10 06:17:43', '2019-11-10 06:17:43'),
(103, 3, 1, 'write', '2019-12-14 09:23:48', '2019-12-14 09:23:48'),
(104, 3, 2, 'write', '2019-12-14 09:23:48', '2019-12-14 09:23:48'),
(105, 3, 3, 'write', '2019-12-14 09:23:48', '2019-12-14 09:23:48'),
(106, 3, 4, 'write', '2019-12-14 09:23:48', '2019-12-14 09:23:48'),
(107, 3, 5, 'write', '2019-12-14 09:23:48', '2019-12-14 09:23:48'),
(108, 3, 6, 'write', '2019-12-14 09:23:48', '2019-12-14 09:23:48'),
(109, 3, 7, 'write', '2019-12-14 09:23:48', '2019-12-14 09:23:48'),
(110, 3, 8, 'write', '2019-12-14 09:23:48', '2019-12-14 09:23:48'),
(111, 3, 9, 'write', '2019-12-14 09:23:48', '2019-12-14 09:23:48'),
(112, 3, 10, 'write', '2019-12-14 09:23:48', '2019-12-14 09:23:48'),
(113, 3, 11, 'write', '2019-12-14 09:23:48', '2019-12-14 09:23:48'),
(114, 3, 12, 'write', '2019-12-14 09:23:48', '2019-12-14 09:23:48'),
(118, 3, 16, 'write', '2019-12-14 09:23:49', '2019-12-14 09:23:49'),
(119, 3, 17, 'invisible', '2019-12-14 09:23:49', '2019-12-14 09:23:49'),
(120, 3, 18, 'write', '2019-12-14 09:23:49', '2019-12-14 09:23:49'),
(121, 3, 19, 'write', '2019-12-14 09:23:49', '2019-12-14 09:23:49'),
(122, 3, 20, 'write', '2019-12-14 09:23:49', '2019-12-14 09:23:49'),
(123, 3, 21, 'write', '2019-12-14 09:23:49', '2019-12-14 09:23:49'),
(124, 3, 22, 'invisible', '2019-12-14 09:23:49', '2019-12-14 09:23:49'),
(125, 3, 23, 'write', '2019-12-14 09:23:49', '2019-12-14 09:23:49'),
(126, 3, 24, 'write', '2019-12-14 09:23:49', '2019-12-14 09:23:49'),
(127, 3, 25, 'write', '2019-12-14 09:23:49', '2019-12-14 09:23:49'),
(128, 3, 26, 'write', '2019-12-14 09:23:49', '2019-12-14 09:23:49'),
(129, 3, 30, 'readonly', '2019-12-14 09:23:50', '2019-12-14 09:23:50'),
(130, 3, 31, 'readonly', '2019-12-14 09:23:50', '2019-12-14 09:23:50'),
(131, 3, 32, 'readonly', '2019-12-14 09:23:50', '2019-12-14 09:23:50'),
(132, 3, 33, 'readonly', '2019-12-14 09:23:50', '2019-12-14 09:23:50'),
(133, 3, 34, 'readonly', '2019-12-14 09:23:50', '2019-12-14 09:23:50'),
(134, 3, 35, 'readonly', '2019-12-14 09:23:50', '2019-12-14 09:23:50'),
(135, 3, 36, 'readonly', '2019-12-14 09:23:50', '2019-12-14 09:23:50'),
(136, 3, 37, 'readonly', '2019-12-14 09:23:50', '2019-12-14 09:23:50'),
(137, 3, 38, 'readonly', '2019-12-14 09:23:50', '2019-12-14 09:23:50'),
(138, 3, 39, 'readonly', '2019-12-14 09:23:50', '2019-12-14 09:23:50'),
(139, 3, 40, 'readonly', '2019-12-14 09:23:50', '2019-12-14 09:23:50'),
(140, 3, 41, 'readonly', '2019-12-14 09:23:50', '2019-12-14 09:23:50'),
(141, 3, 42, 'readonly', '2019-12-14 09:23:50', '2019-12-14 09:23:50'),
(142, 3, 43, 'readonly', '2019-12-14 09:23:50', '2019-12-14 09:23:50'),
(143, 3, 44, 'readonly', '2019-12-14 09:23:50', '2019-12-14 09:23:50'),
(144, 3, 45, 'readonly', '2019-12-14 09:23:50', '2019-12-14 09:23:50'),
(145, 3, 46, 'readonly', '2019-12-14 09:23:51', '2019-12-14 09:23:51'),
(146, 3, 47, 'readonly', '2019-12-14 09:23:51', '2019-12-14 09:23:51'),
(147, 3, 48, 'readonly', '2019-12-14 09:23:51', '2019-12-14 09:23:51'),
(148, 3, 49, 'write', '2019-12-14 09:23:51', '2019-12-14 09:23:51'),
(149, 3, 50, 'write', '2019-12-14 09:23:51', '2019-12-14 09:23:51'),
(150, 3, 51, 'write', '2019-12-14 09:23:51', '2019-12-14 09:23:51'),
(151, 4, 1, 'write', '2019-12-14 10:02:09', '2019-12-14 10:02:09'),
(152, 4, 2, 'write', '2019-12-14 10:02:09', '2019-12-14 10:02:09'),
(153, 4, 3, 'write', '2019-12-14 10:02:09', '2019-12-14 10:02:09'),
(154, 4, 4, 'write', '2019-12-14 10:02:09', '2019-12-14 10:02:09'),
(155, 4, 5, 'write', '2019-12-14 10:02:09', '2019-12-14 10:02:09'),
(156, 4, 6, 'write', '2019-12-14 10:02:09', '2019-12-14 10:02:09'),
(157, 4, 7, 'write', '2019-12-14 10:02:09', '2019-12-14 10:02:09'),
(158, 4, 8, 'write', '2019-12-14 10:02:09', '2019-12-14 10:02:09'),
(159, 4, 9, 'write', '2019-12-14 10:02:09', '2019-12-14 10:02:09'),
(160, 4, 10, 'write', '2019-12-14 10:02:09', '2019-12-14 10:02:09'),
(161, 4, 11, 'write', '2019-12-14 10:02:09', '2019-12-14 10:02:09'),
(162, 4, 12, 'write', '2019-12-14 10:02:09', '2019-12-14 10:02:09'),
(166, 4, 16, 'write', '2019-12-14 10:02:10', '2019-12-14 10:02:10'),
(167, 4, 17, 'invisible', '2019-12-14 10:02:10', '2019-12-14 10:02:10'),
(168, 4, 18, 'write', '2019-12-14 10:02:10', '2019-12-14 10:02:10'),
(169, 4, 19, 'write', '2019-12-14 10:02:10', '2019-12-14 10:02:10'),
(170, 4, 20, 'write', '2019-12-14 10:02:10', '2019-12-14 10:02:10'),
(171, 4, 21, 'write', '2019-12-14 10:02:10', '2019-12-14 10:02:10'),
(172, 4, 22, 'invisible', '2019-12-14 10:02:10', '2019-12-14 10:02:10'),
(173, 4, 23, 'write', '2019-12-14 10:02:10', '2019-12-14 10:02:10'),
(174, 4, 24, 'write', '2019-12-14 10:02:10', '2019-12-14 10:02:10'),
(175, 4, 25, 'write', '2019-12-14 10:02:10', '2019-12-14 10:02:10'),
(176, 4, 26, 'write', '2019-12-14 10:02:10', '2019-12-14 10:02:10'),
(177, 4, 30, 'readonly', '2019-12-14 10:02:11', '2019-12-14 10:02:11'),
(178, 4, 31, 'readonly', '2019-12-14 10:02:11', '2019-12-14 10:02:11'),
(179, 4, 32, 'readonly', '2019-12-14 10:02:11', '2019-12-14 10:02:11'),
(180, 4, 33, 'readonly', '2019-12-14 10:02:11', '2019-12-14 10:02:11'),
(181, 4, 34, 'readonly', '2019-12-14 10:02:11', '2019-12-14 10:02:11'),
(182, 4, 35, 'readonly', '2019-12-14 10:02:11', '2019-12-14 10:02:11'),
(183, 4, 36, 'readonly', '2019-12-14 10:02:11', '2019-12-14 10:02:11'),
(184, 4, 37, 'readonly', '2019-12-14 10:02:11', '2019-12-14 10:02:11'),
(185, 4, 38, 'readonly', '2019-12-14 10:02:11', '2019-12-14 10:02:11'),
(186, 4, 39, 'readonly', '2019-12-14 10:02:11', '2019-12-14 10:02:11'),
(187, 4, 40, 'readonly', '2019-12-14 10:02:11', '2019-12-14 10:02:11'),
(188, 4, 41, 'readonly', '2019-12-14 10:02:11', '2019-12-14 10:02:11'),
(189, 4, 42, 'readonly', '2019-12-14 10:02:11', '2019-12-14 10:02:11'),
(190, 4, 43, 'readonly', '2019-12-14 10:02:11', '2019-12-14 10:02:11'),
(191, 4, 44, 'readonly', '2019-12-14 10:02:11', '2019-12-14 10:02:11'),
(192, 4, 45, 'readonly', '2019-12-14 10:02:11', '2019-12-14 10:02:11'),
(193, 4, 46, 'readonly', '2019-12-14 10:02:12', '2019-12-14 10:02:12'),
(194, 4, 47, 'readonly', '2019-12-14 10:02:12', '2019-12-14 10:02:12'),
(195, 4, 48, 'readonly', '2019-12-14 10:02:12', '2019-12-14 10:02:12'),
(196, 4, 49, 'readonly', '2019-12-14 10:02:12', '2019-12-14 10:02:12'),
(197, 4, 50, 'write', '2019-12-14 10:02:12', '2019-12-14 10:02:12'),
(198, 4, 51, 'write', '2019-12-14 10:02:12', '2019-12-14 10:02:12'),
(199, 5, 1, 'readonly', '2019-12-14 11:38:10', '2019-12-14 11:38:10'),
(200, 5, 2, 'readonly', '2019-12-14 11:38:10', '2019-12-14 11:38:10'),
(201, 5, 3, 'readonly', '2019-12-14 11:38:10', '2019-12-14 11:38:10'),
(202, 5, 4, 'readonly', '2019-12-14 11:38:10', '2019-12-14 11:38:10'),
(203, 5, 5, 'readonly', '2019-12-14 11:38:10', '2019-12-14 11:38:10'),
(204, 5, 6, 'readonly', '2019-12-14 11:38:11', '2019-12-14 11:38:11'),
(205, 5, 7, 'readonly', '2019-12-14 11:38:11', '2019-12-14 11:38:11'),
(206, 5, 8, 'readonly', '2019-12-14 11:38:11', '2019-12-14 11:38:11'),
(207, 5, 9, 'readonly', '2019-12-14 11:38:11', '2019-12-14 11:38:11'),
(208, 5, 10, 'readonly', '2019-12-14 11:38:11', '2019-12-14 11:38:11'),
(209, 5, 11, 'readonly', '2019-12-14 11:38:11', '2019-12-14 11:38:11'),
(210, 5, 12, 'readonly', '2019-12-14 11:38:11', '2019-12-14 11:38:11'),
(214, 5, 16, 'write', '2019-12-14 11:38:12', '2019-12-14 11:38:12'),
(215, 5, 17, 'invisible', '2019-12-14 11:38:12', '2019-12-14 11:38:12'),
(216, 5, 18, 'write', '2019-12-14 11:38:12', '2019-12-14 11:38:12'),
(217, 5, 19, 'write', '2019-12-14 11:38:12', '2019-12-14 11:38:12'),
(218, 5, 20, 'write', '2019-12-14 11:38:12', '2019-12-14 11:38:12'),
(219, 5, 21, 'write', '2019-12-14 11:38:12', '2019-12-14 11:38:12'),
(220, 5, 22, 'invisible', '2019-12-14 11:38:12', '2019-12-14 11:38:12'),
(221, 5, 23, 'write', '2019-12-14 11:38:12', '2019-12-14 11:38:12'),
(222, 5, 24, 'write', '2019-12-14 11:38:12', '2019-12-14 11:38:12'),
(223, 5, 25, 'write', '2019-12-14 11:38:12', '2019-12-14 11:38:12'),
(224, 5, 26, 'write', '2019-12-14 11:38:12', '2019-12-14 11:38:12'),
(225, 5, 30, 'readonly', '2019-12-14 11:38:13', '2019-12-14 11:38:13'),
(226, 5, 31, 'readonly', '2019-12-14 11:38:13', '2019-12-14 11:38:13'),
(227, 5, 32, 'readonly', '2019-12-14 11:38:13', '2019-12-14 11:38:13'),
(228, 5, 33, 'readonly', '2019-12-14 11:38:13', '2019-12-14 11:38:13'),
(229, 5, 34, 'readonly', '2019-12-14 11:38:13', '2019-12-14 11:38:13'),
(230, 5, 35, 'readonly', '2019-12-14 11:38:13', '2019-12-14 11:38:13'),
(231, 5, 36, 'readonly', '2019-12-14 11:38:13', '2019-12-14 11:38:13'),
(232, 5, 37, 'readonly', '2019-12-14 11:38:13', '2019-12-14 11:38:13'),
(233, 5, 38, 'readonly', '2019-12-14 11:38:13', '2019-12-14 11:38:13'),
(234, 5, 39, 'readonly', '2019-12-14 11:38:13', '2019-12-14 11:38:13'),
(235, 5, 40, 'readonly', '2019-12-14 11:38:13', '2019-12-14 11:38:13'),
(236, 5, 41, 'readonly', '2019-12-14 11:38:13', '2019-12-14 11:38:13'),
(237, 5, 42, 'readonly', '2019-12-14 11:38:13', '2019-12-14 11:38:13'),
(238, 5, 43, 'readonly', '2019-12-14 11:38:13', '2019-12-14 11:38:13'),
(239, 5, 44, 'readonly', '2019-12-14 11:38:13', '2019-12-14 11:38:13'),
(240, 5, 45, 'readonly', '2019-12-14 11:38:13', '2019-12-14 11:38:13'),
(241, 5, 46, 'readonly', '2019-12-14 11:38:14', '2019-12-14 11:38:14'),
(242, 5, 47, 'readonly', '2019-12-14 11:38:14', '2019-12-14 11:38:14'),
(243, 5, 48, 'readonly', '2019-12-14 11:38:14', '2019-12-14 11:38:14'),
(244, 5, 49, 'readonly', '2019-12-14 11:38:14', '2019-12-14 11:38:14'),
(245, 5, 50, 'readonly', '2019-12-14 11:38:14', '2019-12-14 11:38:14'),
(246, 5, 51, 'readonly', '2019-12-14 11:38:14', '2019-12-14 11:38:14'),
(247, 6, 1, 'readonly', '2019-12-14 11:39:28', '2019-12-14 11:39:28'),
(248, 6, 2, 'readonly', '2019-12-14 11:39:28', '2019-12-14 11:39:28'),
(249, 6, 3, 'readonly', '2019-12-14 11:39:28', '2019-12-14 11:39:28'),
(250, 6, 4, 'readonly', '2019-12-14 11:39:28', '2019-12-14 11:39:28'),
(251, 6, 5, 'readonly', '2019-12-14 11:39:28', '2019-12-14 11:39:28'),
(252, 6, 6, 'readonly', '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(253, 6, 7, 'readonly', '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(254, 6, 8, 'readonly', '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(255, 6, 9, 'readonly', '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(256, 6, 10, 'readonly', '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(257, 6, 11, 'readonly', '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(258, 6, 12, 'readonly', '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(262, 6, 16, 'write', '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(263, 6, 17, 'invisible', '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(264, 6, 18, 'write', '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(265, 6, 19, 'write', '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(266, 6, 20, 'write', '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(267, 6, 21, 'write', '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(268, 6, 22, 'invisible', '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(269, 6, 23, 'write', '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(270, 6, 24, 'write', '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(271, 6, 25, 'write', '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(272, 6, 26, 'write', '2019-12-14 11:39:29', '2019-12-14 11:39:29'),
(273, 6, 30, 'readonly', '2019-12-14 11:39:30', '2019-12-14 11:39:30'),
(274, 6, 31, 'readonly', '2019-12-14 11:39:30', '2019-12-14 11:39:30'),
(275, 6, 32, 'readonly', '2019-12-14 11:39:30', '2019-12-14 11:39:30'),
(276, 6, 33, 'readonly', '2019-12-14 11:39:30', '2019-12-14 11:39:30'),
(277, 6, 34, 'readonly', '2019-12-14 11:39:30', '2019-12-14 11:39:30'),
(278, 6, 35, 'readonly', '2019-12-14 11:39:30', '2019-12-14 11:39:30'),
(279, 6, 36, 'readonly', '2019-12-14 11:39:30', '2019-12-14 11:39:30'),
(280, 6, 37, 'readonly', '2019-12-14 11:39:30', '2019-12-14 11:39:30'),
(281, 6, 38, 'readonly', '2019-12-14 11:39:30', '2019-12-14 11:39:30'),
(282, 6, 39, 'readonly', '2019-12-14 11:39:30', '2019-12-14 11:39:30'),
(283, 6, 40, 'readonly', '2019-12-14 11:39:30', '2019-12-14 11:39:30'),
(284, 6, 41, 'readonly', '2019-12-14 11:39:30', '2019-12-14 11:39:30'),
(285, 6, 42, 'readonly', '2019-12-14 11:39:30', '2019-12-14 11:39:30'),
(286, 6, 43, 'readonly', '2019-12-14 11:39:30', '2019-12-14 11:39:30'),
(287, 6, 44, 'readonly', '2019-12-14 11:39:30', '2019-12-14 11:39:30'),
(288, 6, 45, 'readonly', '2019-12-14 11:39:30', '2019-12-14 11:39:30'),
(289, 6, 46, 'readonly', '2019-12-14 11:39:31', '2019-12-14 11:39:31'),
(290, 6, 47, 'readonly', '2019-12-14 11:39:31', '2019-12-14 11:39:31'),
(291, 6, 48, 'readonly', '2019-12-14 11:39:31', '2019-12-14 11:39:31'),
(292, 6, 49, 'readonly', '2019-12-14 11:39:31', '2019-12-14 11:39:31'),
(293, 6, 50, 'readonly', '2019-12-14 11:39:31', '2019-12-14 11:39:31'),
(294, 6, 51, 'readonly', '2019-12-14 11:39:31', '2019-12-14 11:39:31'),
(295, 7, 1, 'readonly', '2020-01-01 08:36:25', '2020-01-01 08:36:25'),
(296, 7, 2, 'readonly', '2020-01-01 08:36:25', '2020-01-01 08:36:25'),
(297, 7, 3, 'readonly', '2020-01-01 08:36:25', '2020-01-01 08:36:25'),
(298, 7, 4, 'readonly', '2020-01-01 08:36:25', '2020-01-01 08:36:25'),
(299, 7, 5, 'readonly', '2020-01-01 08:36:25', '2020-01-01 08:36:25'),
(300, 7, 6, 'readonly', '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(301, 7, 7, 'readonly', '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(302, 7, 8, 'readonly', '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(303, 7, 9, 'readonly', '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(304, 7, 10, 'readonly', '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(305, 7, 11, 'readonly', '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(306, 7, 12, 'readonly', '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(307, 7, 16, 'write', '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(308, 7, 17, 'invisible', '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(309, 7, 18, 'write', '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(310, 7, 19, 'write', '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(311, 7, 20, 'write', '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(312, 7, 21, 'write', '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(313, 7, 22, 'invisible', '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(314, 7, 23, 'write', '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(315, 7, 24, 'write', '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(316, 7, 25, 'write', '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(317, 7, 26, 'write', '2020-01-01 08:36:26', '2020-01-01 08:36:26'),
(318, 7, 30, 'readonly', '2020-01-01 08:36:27', '2020-01-01 08:36:27'),
(319, 7, 31, 'readonly', '2020-01-01 08:36:27', '2020-01-01 08:36:27'),
(320, 7, 32, 'readonly', '2020-01-01 08:36:27', '2020-01-01 08:36:27'),
(321, 7, 33, 'readonly', '2020-01-01 08:36:27', '2020-01-01 08:36:27'),
(322, 7, 34, 'readonly', '2020-01-01 08:36:27', '2020-01-01 08:36:27'),
(323, 7, 35, 'readonly', '2020-01-01 08:36:27', '2020-01-01 08:36:27'),
(324, 7, 36, 'readonly', '2020-01-01 08:36:27', '2020-01-01 08:36:27'),
(325, 7, 37, 'readonly', '2020-01-01 08:36:27', '2020-01-01 08:36:27'),
(326, 7, 38, 'readonly', '2020-01-01 08:36:27', '2020-01-01 08:36:27'),
(327, 7, 39, 'readonly', '2020-01-01 08:36:27', '2020-01-01 08:36:27'),
(328, 7, 40, 'readonly', '2020-01-01 08:36:27', '2020-01-01 08:36:27'),
(329, 7, 41, 'readonly', '2020-01-01 08:36:27', '2020-01-01 08:36:27'),
(330, 7, 42, 'readonly', '2020-01-01 08:36:27', '2020-01-01 08:36:27'),
(331, 7, 43, 'readonly', '2020-01-01 08:36:27', '2020-01-01 08:36:27'),
(332, 7, 44, 'readonly', '2020-01-01 08:36:27', '2020-01-01 08:36:27'),
(333, 7, 45, 'readonly', '2020-01-01 08:36:27', '2020-01-01 08:36:27'),
(334, 7, 46, 'readonly', '2020-01-01 08:36:28', '2020-01-01 08:36:28'),
(335, 7, 47, 'readonly', '2020-01-01 08:36:28', '2020-01-01 08:36:28'),
(336, 7, 48, 'readonly', '2020-01-01 08:36:28', '2020-01-01 08:36:28'),
(337, 7, 49, 'readonly', '2020-01-01 08:36:28', '2020-01-01 08:36:28'),
(338, 7, 50, 'readonly', '2020-01-01 08:36:28', '2020-01-01 08:36:28'),
(339, 7, 51, 'readonly', '2020-01-01 08:36:28', '2020-01-01 08:36:28'),
(340, 1, 52, 'write', '2020-04-09 16:47:19', '2020-04-09 16:47:19'),
(341, 1, 53, 'write', '2020-04-09 16:48:12', '2020-04-09 16:48:12'),
(342, 7, 52, 'invisible', '2020-04-14 09:29:17', '2020-04-14 09:29:17'),
(343, 7, 53, 'invisible', '2020-04-14 09:29:17', '2020-04-14 09:29:17'),
(344, 8, 1, 'write', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(345, 8, 2, 'write', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(346, 8, 3, 'write', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(347, 8, 4, 'write', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(348, 8, 5, 'write', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(349, 8, 6, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(350, 8, 7, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(351, 8, 8, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(352, 8, 9, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(353, 8, 10, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(354, 8, 11, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(355, 8, 12, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(356, 8, 16, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(357, 8, 17, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(358, 8, 18, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(359, 8, 19, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(360, 8, 20, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(361, 8, 21, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(362, 8, 22, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(363, 8, 23, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(364, 8, 24, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(365, 8, 25, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(366, 8, 26, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(367, 8, 52, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(368, 8, 53, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(369, 8, 30, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(370, 8, 31, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(371, 8, 32, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(372, 8, 33, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(373, 8, 34, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(374, 8, 35, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(375, 8, 36, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(376, 8, 37, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(377, 8, 38, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(378, 8, 39, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(379, 8, 40, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(380, 8, 41, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(381, 8, 42, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(382, 8, 43, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(383, 8, 44, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(384, 8, 45, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(385, 8, 46, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(386, 8, 47, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(387, 8, 48, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(388, 8, 49, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(389, 8, 50, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(390, 8, 51, 'readonly', '2020-04-22 16:21:41', '2020-04-22 16:21:41'),
(391, 9, 1, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(392, 9, 2, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(393, 9, 3, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(394, 9, 4, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(395, 9, 5, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(396, 9, 6, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(397, 9, 7, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(398, 9, 8, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(399, 9, 9, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(400, 9, 10, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(401, 9, 11, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(402, 9, 12, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(403, 9, 16, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(404, 9, 17, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(405, 9, 18, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(406, 9, 19, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(407, 9, 20, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(408, 9, 21, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(409, 9, 22, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(410, 9, 23, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(411, 9, 24, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(412, 9, 25, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(413, 9, 26, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(414, 9, 52, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(415, 9, 53, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(416, 9, 30, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(417, 9, 31, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(418, 9, 32, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(419, 9, 33, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(420, 9, 34, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(421, 9, 35, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(422, 9, 36, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(423, 9, 37, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(424, 9, 38, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(425, 9, 39, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(426, 9, 40, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(427, 9, 41, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(428, 9, 42, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(429, 9, 43, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(430, 9, 44, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(431, 9, 45, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(432, 9, 46, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(433, 9, 47, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(434, 9, 48, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(435, 9, 49, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(436, 9, 50, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(437, 9, 51, 'readonly', '2020-04-22 16:22:24', '2020-04-22 16:22:24'),
(438, 2, 52, 'invisible', '2021-03-29 09:47:23', '2021-03-29 09:47:23'),
(439, 2, 53, 'invisible', '2021-03-29 09:47:23', '2021-03-29 09:47:23'),
(440, 3, 52, 'invisible', '2021-03-29 09:47:23', '2021-03-29 09:47:23'),
(441, 3, 53, 'invisible', '2021-03-29 09:47:23', '2021-03-29 09:47:23'),
(442, 4, 52, 'invisible', '2021-03-29 09:47:23', '2021-03-29 09:47:23'),
(443, 4, 53, 'invisible', '2021-03-29 09:47:23', '2021-03-29 09:47:23'),
(444, 5, 52, 'invisible', '2021-03-29 09:47:23', '2021-03-29 09:47:23'),
(445, 5, 53, 'invisible', '2021-03-29 09:47:23', '2021-03-29 09:47:23'),
(446, 6, 52, 'invisible', '2021-03-29 09:47:23', '2021-03-29 09:47:23'),
(447, 6, 53, 'invisible', '2021-03-29 09:47:23', '2021-03-29 09:47:23');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `employee_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, NULL, NULL),
(106, 2, 155, 96, NULL, NULL),
(108, 4, 157, 98, NULL, NULL),
(112, 3, 161, 99, NULL, NULL),
(113, 3, 162, 100, NULL, NULL),
(114, 8, 163, NULL, NULL, NULL),
(115, 8, 164, NULL, NULL, NULL),
(116, 8, 165, NULL, NULL, NULL),
(117, 8, 166, NULL, NULL, NULL),
(118, 3, 167, 101, NULL, NULL),
(119, 4, 168, 102, NULL, NULL),
(120, 4, 169, 103, NULL, NULL),
(121, 8, 170, NULL, NULL, NULL),
(122, 3, 171, 104, NULL, NULL),
(123, 3, 172, 105, NULL, NULL),
(124, 8, 173, NULL, NULL, NULL),
(126, 3, 175, 106, NULL, NULL),
(127, 4, 176, 107, NULL, NULL),
(128, 6, 177, 108, NULL, NULL),
(129, 5, 178, 109, NULL, NULL),
(130, 7, 179, 110, NULL, NULL),
(131, 6, 180, 111, NULL, NULL),
(132, 4, 181, 112, NULL, NULL),
(133, 3, 182, 113, NULL, NULL),
(134, 9, 183, NULL, NULL, NULL),
(135, 9, 184, NULL, NULL, NULL),
(138, 4, 187, 114, NULL, NULL),
(140, 6, 189, 115, NULL, NULL),
(141, 5, 190, 116, NULL, NULL),
(144, 8, 193, NULL, NULL, NULL),
(145, 9, 194, NULL, NULL, NULL),
(146, 9, 195, NULL, NULL, NULL),
(147, 8, 196, NULL, NULL, NULL),
(148, 4, 197, 117, NULL, NULL),
(149, 3, 198, 118, NULL, NULL),
(150, 4, 199, 119, NULL, NULL),
(151, 4, 200, 120, NULL, NULL),
(152, 4, 201, 121, NULL, NULL),
(153, 4, 202, 122, NULL, NULL),
(154, 3, 203, 123, NULL, NULL),
(155, 4, 204, 124, NULL, NULL),
(156, 4, 205, 125, NULL, NULL),
(157, 8, 206, NULL, NULL, NULL),
(158, 3, 207, 126, NULL, NULL),
(159, 3, 208, 127, NULL, NULL),
(160, 3, 209, 128, NULL, NULL),
(161, 6, 210, 129, NULL, NULL),
(162, 5, 211, 130, NULL, NULL),
(163, 7, 212, 131, NULL, NULL),
(165, 8, 214, NULL, NULL, NULL),
(167, 9, 216, NULL, NULL, NULL),
(168, 9, 217, NULL, NULL, NULL),
(170, 4, 219, 133, NULL, NULL),
(171, 3, 221, 135, NULL, NULL),
(172, 8, 222, NULL, NULL, NULL),
(174, 8, 224, NULL, NULL, NULL),
(175, 8, 225, NULL, NULL, NULL),
(176, 8, 226, NULL, NULL, NULL),
(177, 8, 227, NULL, NULL, NULL),
(178, 8, 228, NULL, NULL, NULL),
(179, 8, 229, NULL, NULL, NULL),
(180, 8, 230, NULL, NULL, NULL),
(181, 8, 231, NULL, NULL, NULL),
(185, 9, 235, NULL, NULL, NULL),
(186, 4, 236, 136, NULL, NULL),
(187, 4, 237, 137, NULL, NULL),
(188, 4, 238, 138, NULL, NULL),
(189, 4, 239, 139, NULL, NULL),
(194, 8, 244, NULL, NULL, NULL),
(196, 9, 246, NULL, NULL, NULL),
(197, 4, 247, 144, NULL, NULL),
(198, 4, 248, 145, NULL, NULL),
(199, 4, 249, 146, NULL, NULL),
(200, 4, 250, 147, NULL, NULL),
(201, 8, 251, NULL, NULL, NULL),
(202, 9, 252, NULL, NULL, NULL),
(203, 9, 253, NULL, NULL, NULL),
(204, 4, 254, 148, NULL, NULL),
(205, 4, 255, 149, NULL, NULL),
(206, 6, 256, 150, NULL, NULL),
(207, 6, 257, 151, NULL, NULL),
(208, 6, 258, 152, NULL, NULL),
(209, 5, 259, 153, NULL, NULL),
(210, 8, 260, NULL, NULL, NULL),
(211, 8, 261, NULL, NULL, NULL),
(212, 8, 262, NULL, NULL, NULL),
(213, 3, 263, 154, NULL, NULL),
(214, 3, 264, 155, NULL, NULL),
(216, 8, 266, NULL, NULL, NULL),
(217, 4, 267, 156, NULL, NULL),
(221, 8, 271, NULL, NULL, NULL),
(222, 4, 272, 157, NULL, NULL),
(223, 8, 273, NULL, NULL, NULL),
(224, 8, 274, NULL, NULL, NULL),
(226, 9, 276, NULL, NULL, NULL),
(227, 8, 277, NULL, NULL, NULL),
(228, 8, 278, NULL, NULL, NULL),
(229, 8, 279, NULL, NULL, NULL),
(230, 8, 280, NULL, NULL, NULL),
(231, 9, 281, NULL, NULL, NULL),
(232, 9, 282, NULL, NULL, NULL),
(233, 4, 283, 158, NULL, NULL),
(234, 4, 284, 159, NULL, NULL),
(235, 4, 285, 160, NULL, NULL),
(236, 4, 286, 161, NULL, NULL),
(237, 4, 287, 162, NULL, NULL),
(238, 4, 288, 163, NULL, NULL),
(239, 4, 289, 164, NULL, NULL),
(240, 8, 290, NULL, NULL, NULL),
(241, 8, 291, NULL, NULL, NULL),
(242, 8, 292, NULL, NULL, NULL),
(243, 9, 293, NULL, NULL, NULL),
(244, 9, 294, NULL, NULL, NULL),
(245, 3, 295, 165, NULL, NULL),
(246, 3, 296, 166, NULL, NULL),
(247, 4, 297, 167, NULL, NULL),
(248, 8, 298, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `staff_agents`
--

CREATE TABLE `staff_agents` (
  `id` int(10) NOT NULL,
  `staff_user_id` int(10) NOT NULL,
  `staff_emp_id` int(10) NOT NULL,
  `agent_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `staff_agents`
--

INSERT INTO `staff_agents` (`id`, `staff_user_id`, `staff_emp_id`, `agent_id`) VALUES
(3, 175, 106, 181),
(4, 175, 106, 199),
(10, 221, 135, 176),
(11, 221, 135, 181),
(12, 207, 126, 176),
(13, 207, 126, 187),
(15, 295, 165, 176),
(20, 296, 166, 176),
(21, 296, 166, 181);

-- --------------------------------------------------------

--
-- Table structure for table `stream`
--

CREATE TABLE `stream` (
  `id` int(11) NOT NULL,
  `stream_name` varchar(250) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `stream`
--

INSERT INTO `stream` (`id`, `stream_name`, `status`) VALUES
(1, 'Computer', 1),
(2, 'Maths', 1),
(3, 'Science', 1),
(4, 'Commerce', 1);

-- --------------------------------------------------------

--
-- Table structure for table `studentstatus`
--

CREATE TABLE `studentstatus` (
  `id` int(11) NOT NULL,
  `student_status` varchar(250) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `studentstatus`
--

INSERT INTO `studentstatus` (`id`, `student_status`, `status`) VALUES
(1, 'Approve', 1),
(2, 'Reject', 1),
(3, 'Processing', 1),
(5, 'Completed', 1);

-- --------------------------------------------------------

--
-- Table structure for table `syllabus`
--

CREATE TABLE `syllabus` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `syllabus`
--

INSERT INTO `syllabus` (`id`, `name`, `status`) VALUES
(1, 'ICSE', 1),
(2, 'CBSE', 1);

-- --------------------------------------------------------

--
-- Table structure for table `time_tracking_settings`
--

CREATE TABLE `time_tracking_settings` (
  `id` int(11) NOT NULL,
  `status` enum('ANYTIME','EVERYDAY','OFFICE-TIME') NOT NULL DEFAULT 'ANYTIME',
  `from_time` varchar(10) DEFAULT NULL,
  `to_time` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `time_tracking_settings`
--

INSERT INTO `time_tracking_settings` (`id`, `status`, `from_time`, `to_time`) VALUES
(8, 'OFFICE-TIME', '09:00 AM', '06:30 PM'),
(9, 'OFFICE-TIME', '09:00 AM', '06:00 PM'),
(10, 'OFFICE-TIME', '12:00 AM', '12:00 AM'),
(11, 'ANYTIME', '04:00 AM', '12:04 AM');

-- --------------------------------------------------------

--
-- Table structure for table `universities`
--

CREATE TABLE `universities` (
  `id` int(11) NOT NULL,
  `university` varchar(250) NOT NULL,
  `university_country` varchar(150) DEFAULT NULL,
  `university_location` varchar(200) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `universities`
--

INSERT INTO `universities` (`id`, `university`, `university_country`, `university_location`, `status`) VALUES
(1, '(INTO) University of Manchester', NULL, 'UK', 1),
(2, '(INTO) University of East of Anglia', NULL, 'UK', 1),
(3, '(STUDY GROUP) University of Leeds, Leeds', NULL, 'UK', 1),
(4, '(KAPLAN) University of Nottingham International College', NULL, 'UK', 1),
(5, '(KAPLAN) University of Glasgow International College', NULL, 'UK', 1),
(6, '(KAPLAN) University of Bristol', NULL, 'UK', 1),
(7, '(STUDY GROUP) The University of Sheffield International College', NULL, 'UK', 1),
(8, '(INTO) Newcastle University', NULL, 'UK', 1),
(9, '(KAPLAN) Queen Mary University of London', NULL, 'UK', 1),
(10, '(ONCAMPUS) Queen Mary University of London', NULL, 'UK', 1),
(11, 'Nottingham Trent University', NULL, 'UK', 1),
(12, '(KAPLAN) University of York16', NULL, 'UK', 1),
(13, 'University of Birmingham', NULL, 'UK', 1),
(14, '(STUDY GROUP) Cardiff University International Study Centre', NULL, 'UK', 1),
(15, '(KAPLAN) University of Liverpool International College', NULL, 'UK', 1),
(16, 'University of Essex', NULL, 'UK', 1),
(17, 'University of Bristol', NULL, 'UK', 1),
(18, 'University of Surrey', NULL, 'UK', 1),
(19, '(NAVITAS) University of Leicester Global Study Centre', NULL, 'UK', 1),
(20, '(STUDY GROUP) University of Sussex, Falmer', NULL, 'UK', 1),
(21, '(STUDY GROUP) University of Aberdeen International Study Centre', NULL, 'UK', 1),
(22, 'University of Glasgow', NULL, 'UK', 1),
(23, 'University of Portsmouth', NULL, 'UK', 1),
(24, '(STUDY GROUP) University of Surrey International Study Centre', NULL, 'UK', 1),
(25, '(STUDY GROUP) Durham University, Durham', NULL, 'UK', 1),
(26, 'University of East Anglia', NULL, 'UK', 1),
(27, 'Newcastle University', NULL, 'UK', 1),
(28, 'The University of Huddersfield', NULL, 'UK', 1),
(29, 'University of Dundee', NULL, 'UK', 1),
(30, '(STUDY GROUP) Lancaster University, Lancaster', NULL, 'UK', 1),
(31, '(NAVITAS) Lancaster University', NULL, 'UK', 1),
(32, '(ONCAMPUS) University of Reading', NULL, 'UK', 1),
(33, 'Northumbria University, Newcastle', NULL, 'UK', 1),
(34, '(STUDY GROUP) University of Strathclyde International Study Centre', NULL, 'UK', 1),
(35, 'University of Stirling', NULL, 'UK', 1),
(36, 'University of Gloucestershire', NULL, 'UK', 1),
(37, 'Oxford Brookes University', NULL, 'UK', 1),
(38, '(KAPLAN) University of Essex International College', NULL, 'UK', 1),
(39, '(NAVITAS) The College, Swansea University', NULL, 'UK', 1),
(40, 'Swansea University', NULL, 'UK', 1),
(41, 'Staffordshire University', NULL, 'UK', 1),
(42, 'Aberystwyth University', NULL, 'UK', 1),
(43, '(NAVITAS) International College of Portsmouth', NULL, 'UK', 1),
(44, '(QAHE) Northumbria University', NULL, 'UK', 1),
(45, 'Bangor University', NULL, 'UK', 1),
(46, 'University of West London', NULL, 'UK', 1),
(47, '(INTO) Manchester Metropolitan University', NULL, 'UK', 1),
(48, '(OXFORD INTERNATIONAL EDUCATION GROUP) Bangor University International College', NULL, 'UK', 1),
(49, 'University of the West of England, Bristol', NULL, 'UK', 1),
(50, '(KAPLAN) UWE Bristol', NULL, 'UK', 1),
(51, 'Queen`s University Belfast', NULL, 'UK', 1),
(52, 'University of Strathclyde', NULL, 'UK', 1),
(53, 'Manchester Metropolitan University', NULL, 'UK', 1),
(54, '(ONCAMPUS) Birkbeck, University of London', NULL, 'UK', 1),
(55, '(ONCAMPUS) University of Hull', NULL, 'UK', 1),
(56, 'Liverpool Hope University', NULL, 'UK', 1),
(57, 'University of Liverpool', NULL, 'UK', 1),
(58, '(KAPLAN) University of Brighton', NULL, 'UK', 1),
(59, 'University of Brighton', NULL, 'UK', 1),
(60, '(KAPLAN) Nottingham Trent International College', NULL, 'UK', 1),
(61, '(ONCAMPUS) Goldsmiths University of London', NULL, 'UK', 1),
(62, '(STUDY GROUP) Liverpool John Moores University, Liverpool', NULL, 'UK', 1),
(63, 'University of Leicester', NULL, 'UK', 1),
(64, 'Heriot Watt University', NULL, 'UK', 1),
(65, 'University of Plymouth', NULL, 'UK', 1),
(66, '(OXFORD INTERNATIONAL EDUCATION GROUP) De Montfort University International College', NULL, 'UK', 1),
(67, '(STUDY GROUP) Coventry University London International Study Centre', NULL, 'UK', 1),
(68, '(KAPLAN) Bournemouth University', NULL, 'UK', 1),
(69, 'Teesside University', NULL, 'UK', 1),
(70, 'Sheffield Hallam University', NULL, 'UK', 1),
(71, 'De Montfort University', NULL, 'UK', 1),
(72, '(STUDY GROUP) University of Huddersfield', NULL, 'UK', 1),
(73, '(ONCAMPUS) University of Central Lancashire', NULL, 'UK', 1),
(74, 'University of Bradford', NULL, 'UK', 1),
(75, '(STUDY GROUP) University of Kingston International Study Centre', NULL, 'UK', 1),
(76, '(STUDY GROUP) University of Lincoln, Lincoln', NULL, 'UK', 1),
(77, 'University of Central Lancashire', NULL, 'UK', 1),
(78, 'London South Bank University', NULL, 'UK', 1),
(79, 'University of Westminster', NULL, 'UK', 1),
(80, 'Queen Mary University of London', NULL, 'UK', 1),
(81, '(STUDY GROUP) Leeds Beckett University, Leeds', NULL, 'UK', 1),
(82, 'Royal Holloway University of London', NULL, 'UK', 1),
(83, '(STUDY GROUP) Royal Holloway, University of London International Study Centre', NULL, 'UK', 1),
(84, 'University of East London', NULL, 'UK', 1),
(85, 'University of Salford', NULL, 'UK', 1),
(86, 'University of Hertfordshire', NULL, 'UK', 1),
(87, '(ONCAMPUS) London South Bank University', NULL, 'UK', 1),
(88, 'Ulster University, London Campus', NULL, 'UK', 1),
(89, '(QAHE) University of Roehampton', NULL, 'UK', 1),
(90, 'Instituto Marangoni', NULL, 'UK', 1),
(91, 'University of Chester', NULL, 'UK', 1),
(92, '(INTO) City University of London', NULL, 'UK', 1),
(93, 'Glasgow Caledonian University', NULL, 'UK', 1),
(94, 'Birmingham City University', NULL, 'UK', 1),
(95, '(HOLMES EDUCATION GROUP) University of Bedfordshire International College', NULL, 'UK', 1),
(96, 'Cardiff Metropolitan University', NULL, 'UK', 1),
(97, 'University of Greenwich', NULL, 'UK', 1),
(98, '(ONCAMPUS) University of Sunderland', NULL, 'UK', 1),
(99, 'Anglia Ruskin University', NULL, 'UK', 1),
(100, '(NAVITAS) Robert Gordon University', NULL, 'UK', 1),
(101, 'Edinburgh Napier University', NULL, 'UK', 1),
(102, 'University of South Wales', NULL, 'UK', 1),
(103, 'University of Roehampton', NULL, 'UK', 1),
(104, 'The University of Northampton', NULL, 'UK', 1),
(105, 'Middlesex University', NULL, 'UK', 1),
(106, 'University of Bedfordshire', NULL, 'UK', 1),
(107, 'University of the West of Scotland', NULL, 'UK', 1),
(108, 'University of Wolverhampton', NULL, 'UK', 1),
(109, 'Leeds Beckett University', NULL, 'UK', 1),
(110, '(QAHE) Solent University', NULL, 'UK', 1),
(111, 'Solent University', NULL, 'UK', 1),
(112, '(HOLMES EDUCATION GROUP) University of Worcester International College', NULL, 'UK', 1),
(113, '(HOLMES EDUCATION GROUP) St Mary’s University London International College', NULL, 'UK', 1),
(114, 'Regent’s University London', NULL, 'UK', 1),
(115, 'University of Suffolk', NULL, 'UK', 1),
(116, 'University for the Creative Arts', NULL, 'UK', 1),
(117, '(HOLMES EDUCATION GROUP) University for the Creative Arts International College', NULL, 'UK', 1),
(118, 'Hult International Business School', NULL, 'UK', 1),
(119, 'BPP University', NULL, 'UK', 1),
(120, 'Le - Cordon Bleu', NULL, 'UK', 1),
(121, '(STUDY GROUP) Bellerbys College, Brighton', NULL, 'UK', 1),
(122, 'Brunel University London', NULL, 'UK', 1),
(123, 'Bournemouth University', NULL, 'UK', 1),
(124, '(NAVITAS) Birmingham City University- International College', NULL, 'UK', 1),
(125, 'Anglia Ruskin University College', NULL, 'UK', 1),
(126, '(NAVITAS) Hertfordshire International College', NULL, 'UK', 1),
(127, '(NAVITAS) London Brunel International College', NULL, 'UK', 1),
(128, '(NAVITAS) University of Plymouth International College', NULL, 'UK', 1),
(129, '(NAVITAS) University of Northampton International College', NULL, 'UK', 1),
(130, 'Kingston University', NULL, 'UK', 1),
(131, 'Ulster University, Birmingham Campus', NULL, 'UK', 1),
(132, 'Northumbria University, London', NULL, 'UK', 1),
(133, 'Coventry University, Coventry', NULL, 'UK', 1),
(134, 'Coventry University, London', NULL, 'UK', 1),
(135, 'University of Sunderland, Sunderland', NULL, 'UK', 1),
(136, 'University of Sunderland, London', NULL, 'UK', 1),
(137, '(INTO) Queen’s University Belfast', NULL, 'UK', 1),
(138, '(INTO) University of Exeter', NULL, 'UK', 1),
(139, '(INTO) University of Stirling', NULL, 'UK', 1),
(140, '(INTO) London', NULL, 'UK', 1),
(141, 'University of York', NULL, 'UK', 1),
(142, 'Cranfield University', NULL, 'UK', 1),
(143, '(KAPLAN) International College London', NULL, 'UK', 1),
(144, '(NCUK) University of Salford', NULL, 'UK', 1),
(145, '(OXFORD INTERNATIONAL EDUCATION GROUP) University of Greenwich International College', NULL, 'UK', 1),
(146, '(OXFORD INTERNATIONAL EDUCATION GROUP) International College Dundee', NULL, 'UK', 1),
(147, 'Durham University', NULL, 'UK', 1),
(148, 'The University of Sheffield', NULL, 'UK', 1),
(149, '(ONCAMPUS) The Courtauld University of Art', NULL, 'UK', 1),
(150, '(ONCAMPUS) Royal Veterinary College, University of London', NULL, 'UK', 1),
(151, '(ONCAMPUS) Aston Universit', NULL, 'UK', 1),
(152, 'University of Nottingham', NULL, 'UK', 1),
(153, '(ONCAMPUS) University of Southampton', NULL, 'UK', 1),
(154, 'University College Birmingham', NULL, 'UK', 1),
(155, 'Cambridge School of Visual & Performing Arts', NULL, 'UK', 1),
(156, 'University of Aberdeen', NULL, 'UK', 1),
(157, 'The University of Huddersfield, London', NULL, 'UK', 1);

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `hash` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `public` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `media` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`id`, `name`, `path`, `extension`, `caption`, `user_id`, `hash`, `public`, `deleted_at`, `created_at`, `updated_at`, `media`) VALUES
(6, 'Penguins.jpg', '/var/www/maxglobal/storage/uploads/2020-04-22-165421-Penguins.jpg', 'jpg', '', 1, 'ltxzavhofmhudtm3yajq', 0, NULL, '2020-04-22 16:54:21', '2020-04-26 09:05:34', 0),
(7, 'Jellyfish.jpg', '/var/www/maxglobal/storage/uploads/2020-04-22-165457-Jellyfish.jpg', 'jpg', '', 1, '908mbrivb1undw9sxsdh', 0, NULL, '2020-04-22 16:54:57', '2020-04-22 16:54:57', 0),
(8, 'Penguins.jpg', '/var/www/maxglobal/storage/uploads/2020-04-22-170604-Penguins.jpg', 'jpg', '', 1, 'bs05yx0sprwjk8covedt', 0, NULL, '2020-04-22 17:06:04', '2020-04-22 17:06:04', 0),
(9, 'Hydrangeas.jpg', '/var/www/maxglobal/storage/uploads/2020-04-25-063214-Hydrangeas.jpg', 'jpg', '', 1, 'nbdtvx5s9gwjgedrjqhf', 0, NULL, '2020-04-25 06:32:14', '2020-04-25 06:32:14', 0),
(10, 'wallpaper.jpg', '/var/www/maxglobal/storage/uploads/2020-04-26-091735-wallpaper.jpg', 'jpg', '', 1, 'vl8hpzv3pguokczqxonf', 0, NULL, '2020-04-26 09:17:35', '2020-04-26 09:17:35', 0),
(11, 'wallpaper.jpg', '/var/www/maxglobal/storage/uploads/2020-04-26-091848-wallpaper.jpg', 'jpg', '', 1, 'catiflmwygwuqjjc2yst', 0, NULL, '2020-04-26 09:18:48', '2020-04-26 09:18:48', 0),
(12, 'wallpaper.jpg', '/var/www/maxglobal/storage/uploads/2020-04-26-091935-wallpaper.jpg', 'jpg', '', 1, '8psz79nvjvniadyrpusg', 0, NULL, '2020-04-26 09:19:35', '2020-04-26 09:19:35', 0),
(13, 'wallpaper.jpg', '/var/www/maxglobal/storage/uploads/2020-04-26-091942-wallpaper.jpg', 'jpg', '', 1, 'krzwisrdlx1wsobrslfs', 0, NULL, '2020-04-26 09:19:42', '2020-04-26 09:19:42', 0),
(14, 'wallpaper.jpg', '/var/www/maxglobal/storage/uploads/2020-04-26-091953-wallpaper.jpg', 'jpg', '', 1, 'b7gchdy0olwxgij64slj', 0, NULL, '2020-04-26 09:19:53', '2020-04-26 09:19:53', 0),
(15, 'image6.png', '/var/www/maxglobal/storage/uploads/2020-05-16-174028-image6.png', 'png', '', 1, 'iqtvg4fjvdvyrksntode', 0, NULL, '2020-05-16 17:40:28', '2020-05-16 17:40:28', 0),
(16, '6f6p85he.jpg', '/var/www/maxglobal/storage/uploads/2020-06-02-151853-6f6p85he.jpg', 'jpg', '', 1, 'k4aqonjy01kqcrcimd9y', 0, NULL, '2020-06-02 15:18:53', '2020-06-02 15:18:53', 0),
(17, 'wallpaper.jpg', '/var/www/maxglobal/storage/uploads/2020-06-20-053546-wallpaper.jpg', 'jpg', '', 1, 'bq5yo4avho3bbnswvdnv', 0, NULL, '2020-06-20 05:35:46', '2020-06-20 05:35:46', 0),
(18, 'wallpaper.jpg', '/var/www/maxglobal/storage/uploads/2020-06-20-053621-wallpaper.jpg', 'jpg', '', 1, '3vhj9id80hjzetsglmga', 0, NULL, '2020-06-20 05:36:21', '2020-06-20 05:36:21', 0),
(19, 'wallpaper.jpg', '/var/www/maxglobal/storage/uploads/2020-06-20-053631-wallpaper.jpg', 'jpg', '', 1, 'faobtmczvaokn4q5ks3t', 0, NULL, '2020-06-20 05:36:31', '2020-06-20 05:36:31', 0),
(20, 'wallpaper.jpg', '/var/www/maxglobal/storage/uploads/2020-06-20-053900-wallpaper.jpg', 'jpg', '', 1, 'lapxqdcpodk2np4626rg', 0, NULL, '2020-06-20 05:39:00', '2020-06-20 05:39:00', 0),
(21, 'wallpaper.jpg', '/var/www/maxglobal/storage/uploads/2020-06-20-054033-wallpaper.jpg', 'jpg', '', 1, 'epmvowv2cukxu4ezbjnm', 0, NULL, '2020-06-20 05:40:33', '2021-04-11 11:14:10', 0),
(22, 'wallpaper.jpg', '/var/www/maxglobal/storage/uploads/2020-06-20-054059-wallpaper.jpg', 'jpg', '', 1, 'vptxphlwfgdtwuybpcxu', 0, NULL, '2020-06-20 05:40:59', '2020-06-20 05:40:59', 0),
(23, 'Nurse_Profile_Fields.docx', '/var/www/maxglobal/storage/uploads/2020-06-21-042447-Nurse_Profile_Fields.docx', 'docx', '', 1, 'yvo3jcfkqkshtoqs21ex', 0, NULL, '2020-06-21 04:24:47', '2020-06-21 04:24:47', 0),
(24, 'Nurse_Profile_Fields.docx', '/var/www/maxglobal/storage/uploads/2020-06-21-042707-Nurse_Profile_Fields.docx', 'docx', '', 1, 'phw4hx49t6enblnqa3jq', 0, NULL, '2020-06-21 04:27:07', '2020-06-21 04:27:07', 0),
(25, 'Nurse_Profile_Fields.docx', '/var/www/maxglobal/storage/uploads/2020-06-21-042834-Nurse_Profile_Fields.docx', 'docx', '', 1, 'cktd5jcuy61jaygzchsj', 0, NULL, '2020-06-21 04:28:34', '2020-06-21 04:28:34', 0),
(26, 'Nurse_Profile_Fields.docx', '/var/www/maxglobal/storage/uploads/2020-06-21-042942-Nurse_Profile_Fields.docx', 'docx', '', 1, 'tieechydtgndfif8tzg5', 0, NULL, '2020-06-21 04:29:42', '2020-06-21 04:29:42', 0),
(27, 'MaxGlobalConsultantApplicationDevelopment_StudentProfile_MainAgent_University_Access.odt', '/var/www/maxglobal/storage/uploads/2020-06-21-042958-MaxGlobalConsultantApplicationDevelopment_StudentProfile_MainAgent_University_Access.odt', 'odt', '', 1, 'da0y5vtbbsjb8cxzhqrp', 0, NULL, '2020-06-21 04:29:58', '2020-06-21 04:29:58', 0),
(28, 'MaxGlobalConsultantApplicationDevelopment_StudentProfile_SuperAdmin_Manager_Access.odt', '/var/www/maxglobal/storage/uploads/2020-06-21-043025-MaxGlobalConsultantApplicationDevelopment_StudentProfile_SuperAdmin_Manager_Access.odt', 'odt', '', 1, '9xtvwiwkl4uwtjxalc7p', 0, NULL, '2020-06-21 04:30:25', '2020-06-21 04:30:25', 0),
(29, 'wallpaper.jpg', '/var/www/maxglobal/storage/uploads/2020-06-21-065711-wallpaper.jpg', 'jpg', '', 1, 'xfdwka9rh31a9q18lrax', 0, NULL, '2020-06-21 06:57:11', '2020-06-21 06:57:11', 0),
(30, 'wallpaper.jpg', '/var/www/maxglobal/storage/uploads/2020-06-21-065734-wallpaper.jpg', 'jpg', '', 1, 'aezx8agclzcrg2ggud4d', 0, NULL, '2020-06-21 06:57:34', '2020-06-21 06:57:34', 0),
(31, 'wallpaper.jpg', '/var/www/maxglobal/storage/uploads/2020-06-21-065743-wallpaper.jpg', 'jpg', '', 1, 'xxh3f5z3rryiwywced9h', 0, NULL, '2020-06-21 06:57:43', '2020-06-21 06:57:43', 0),
(32, 'dp.jpg', '/var/www/maxglobal/storage/uploads/2020-06-21-073543-dp.jpg', 'jpg', '', 1, 'evalelq91gqpecmffbbk', 0, NULL, '2020-06-21 07:35:43', '2020-06-21 07:35:43', 0),
(33, 'wallpaper.jpg', '/var/www/maxglobal/storage/uploads/2020-06-21-073557-wallpaper.jpg', 'jpg', '', 1, '0nmt6ixp8qd0vvgfddns', 0, NULL, '2020-06-21 07:35:57', '2020-06-21 07:35:57', 0),
(34, 'passport.jpg', '/var/www/maxglobal/storage/uploads/2020-06-21-073729-passport.jpg', 'jpg', '', 1, 'anl9dwjqeumw0qzdt55l', 0, NULL, '2020-06-21 07:37:29', '2020-06-21 07:37:29', 0),
(35, 'passport.jpg', '/var/www/maxglobal/storage/uploads/2020-06-21-073737-passport.jpg', 'jpg', '', 1, 'xu45unmupgfb9wkkk40u', 0, NULL, '2020-06-21 07:37:37', '2020-06-21 07:37:37', 0),
(36, 'NHSmailLogonLog.txt', '/var/www/maxglobal/storage/uploads/2020-06-21-084328-NHSmailLogonLog.txt', 'txt', '', 1, 'zep4a9lyrlcddsbzoihy', 0, NULL, '2020-06-21 08:43:28', '2020-06-21 08:43:28', 0),
(37, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-06-21-085121-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, 'wv8efjxniykjmlaxh7qk', 0, NULL, '2020-06-21 08:51:21', '2020-06-21 08:51:21', 0),
(38, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-06-21-085702-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, 'qrkeyqmj1li8zhkexvmq', 0, NULL, '2020-06-21 08:57:02', '2020-06-21 08:57:02', 0),
(39, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-06-21-085747-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, 'utafpx0wq0ktrodmu0yn', 0, NULL, '2020-06-21 08:57:47', '2020-06-21 08:57:47', 0),
(40, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-06-21-091140-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, 'xvf6u59upm0tddqu9hmq', 0, NULL, '2020-06-21 09:11:40', '2020-06-21 09:11:40', 0),
(41, 'Sample _ Max Global weekly  REPORT15feb19new.xlsx', '/var/www/maxglobal/storage/uploads/2020-06-21-091159-Sample _ Max Global weekly  REPORT15feb19new.xlsx', 'xlsx', '', 1, 'iltp1xxwxzek825pukxx', 0, NULL, '2020-06-21 09:11:59', '2020-06-21 09:11:59', 0),
(42, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-06-21-103743-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, '66zjx9crbr532gf4wyz4', 0, NULL, '2020-06-21 10:37:43', '2020-06-21 10:37:43', 0),
(43, 'skype.png', '/var/www/maxglobal/storage/uploads/2020-06-21-104935-skype.png', 'png', '', 1, 'zts2b313s9jp2qexr1uv', 0, NULL, '2020-06-21 10:49:35', '2020-06-21 10:49:35', 0),
(44, 'Nurse_Profile_Fields.docx', '/var/www/maxglobal/storage/uploads/2020-06-21-113004-Nurse_Profile_Fields.docx', 'docx', '', 1, 'gtlrrkikipd6mf0qfcjk', 0, NULL, '2020-06-21 11:30:04', '2020-06-21 11:30:04', 0),
(45, 'Nurse_Profile_Fields.docx', '/var/www/maxglobal/storage/uploads/2020-06-21-121219-Nurse_Profile_Fields.docx', 'docx', '', 1, 'a1e5fh2xzkw1rbtd414n', 0, NULL, '2020-06-21 12:12:19', '2020-06-21 12:12:19', 0),
(46, 'Nurse_Profile_Fields.docx', '/var/www/maxglobal/storage/uploads/2020-06-21-121225-Nurse_Profile_Fields.docx', 'docx', '', 1, 'yfje0uw7ykeyscbv1zjk', 0, NULL, '2020-06-21 12:12:25', '2020-06-21 12:12:25', 0),
(47, 'MaxGlobalConsultants_Application_Development_Scope.odt', '/var/www/maxglobal/storage/uploads/2020-06-21-121247-MaxGlobalConsultants_Application_Development_Scope.odt', 'odt', '', 1, 'yb4hdarmyljgaqa3vkxk', 0, NULL, '2020-06-21 12:12:47', '2020-06-21 12:12:47', 0),
(48, 'Nurse_Profile_Fields.docx', '/var/www/maxglobal/storage/uploads/2020-06-21-121327-Nurse_Profile_Fields.docx', 'docx', '', 1, 'qyykmzloo47fwu2snpeb', 0, NULL, '2020-06-21 12:13:27', '2020-06-21 12:13:27', 0),
(49, 'receipt (1).pdf', '/var/www/maxglobal/storage/uploads/2020-06-22-150451-receipt (1).pdf', 'pdf', '', 1, 'kvbp6hqr6wdimqese66w', 0, NULL, '2020-06-22 15:04:51', '2020-06-22 15:04:51', 0),
(50, 'receipt (1).pdf', '/var/www/maxglobal/storage/uploads/2020-06-22-150502-receipt (1).pdf', 'pdf', '', 1, 'm9z3zv7iezrzsa8rtv98', 0, NULL, '2020-06-22 15:05:02', '2020-06-22 15:05:02', 0),
(51, '36.jpg', '/var/www/maxglobal/storage/uploads/2020-06-23-152752-36.jpg', 'jpg', '', 1, 'ds6ilnde7vjt9udqdyig', 0, NULL, '2020-06-23 15:27:52', '2020-06-23 15:27:52', 0),
(52, '86.jpg', '/var/www/maxglobal/storage/uploads/2020-06-23-152952-86.jpg', 'jpg', '', 1, 'karxo3cgkggr85kxc1fq', 0, NULL, '2020-06-23 15:29:52', '2020-06-23 15:29:52', 0),
(53, 'download.png', '/var/www/maxglobal/storage/uploads/2020-07-10-095626-download.png', 'png', '', 1, 'd5xdsgnsojmutxn0cr0u', 0, NULL, '2020-07-10 09:56:26', '2020-07-10 09:56:26', 0),
(54, 'download2.jpg', '/var/www/maxglobal/storage/uploads/2020-07-10-095706-download2.jpg', 'jpg', '', 1, 'wvxjkgi1qo6yrv9juukp', 0, NULL, '2020-07-10 09:57:06', '2020-07-10 09:57:06', 0),
(55, 'download3.jpg', '/var/www/maxglobal/storage/uploads/2020-07-10-095715-download3.jpg', 'jpg', '', 1, 'f29uceamprsyejpl0kyp', 0, NULL, '2020-07-10 09:57:15', '2020-07-10 09:57:15', 0),
(56, 'Document.rtf', '/var/www/maxglobal/storage/uploads/2020-07-10-100007-Document.rtf', 'rtf', '', 1, 'wqne3nmbrba6nr13gyvk', 0, NULL, '2020-07-10 10:00:07', '2020-07-10 10:00:07', 0),
(57, 'Document.rtf', '/var/www/maxglobal/storage/uploads/2020-07-10-100041-Document.rtf', 'rtf', '', 1, 'qeofaai4aa80ddauzho8', 0, NULL, '2020-07-10 10:00:41', '2020-07-10 10:00:41', 0),
(58, 'Document.rtf', '/var/www/maxglobal/storage/uploads/2020-07-10-100115-Document.rtf', 'rtf', '', 1, 'tptdqox1aecfmy1z4xxh', 0, NULL, '2020-07-10 10:01:15', '2020-07-10 10:01:15', 0),
(59, 'Document.rtf', '/var/www/maxglobal/storage/uploads/2020-07-10-100140-Document.rtf', 'rtf', '', 1, 'yz8jznw2bkdlndsrqnas', 0, NULL, '2020-07-10 10:01:40', '2020-07-10 10:01:40', 0),
(60, 'Document.rtf', '/var/www/maxglobal/storage/uploads/2020-07-10-103836-Document.rtf', 'rtf', '', 1, 'rgn91uvmsuaw7raprjkh', 0, NULL, '2020-07-10 10:38:36', '2020-07-10 10:38:36', 0),
(61, 'download3.jpg', '/var/www/maxglobal/storage/uploads/2020-07-10-103904-download3.jpg', 'jpg', '', 1, 'l4ygzltizfkh6rx2ae1p', 0, NULL, '2020-07-10 10:39:04', '2020-07-10 10:39:04', 0),
(62, 'skype.png', '/var/www/maxglobal/storage/uploads/2020-07-10-103931-skype.png', 'png', '', 1, 'gcikkibmlwqx44ak5mgp', 0, NULL, '2020-07-10 10:39:31', '2020-07-10 10:39:31', 0),
(63, 'download2.jpg', '/var/www/maxglobal/storage/uploads/2020-07-10-103945-download2.jpg', 'jpg', '', 1, 'ogqsa0t36vxzt59sxa0c', 0, NULL, '2020-07-10 10:39:45', '2020-07-10 10:39:45', 0),
(64, 'Document.rtf', '/var/www/maxglobal/storage/uploads/2020-07-10-104332-Document.rtf', 'rtf', '', 1, '5nxvdfpampf9ohp3cvli', 0, NULL, '2020-07-10 10:43:32', '2020-07-10 10:43:32', 0),
(65, 'skype.png', '/var/www/maxglobal/storage/uploads/2020-07-10-104337-skype.png', 'png', '', 1, 'bgycwtlemxkuoptcztbw', 0, NULL, '2020-07-10 10:43:37', '2020-07-10 10:43:37', 0),
(66, 'Document.rtf', '/var/www/maxglobal/storage/uploads/2020-07-10-104344-Document.rtf', 'rtf', '', 1, 'vbmn5btd2qy8za7gvei0', 0, NULL, '2020-07-10 10:43:44', '2020-07-10 10:43:44', 0),
(67, 'Document.rtf', '/var/www/maxglobal/storage/uploads/2020-07-10-104405-Document.rtf', 'rtf', '', 1, 'pdkztapvl359xwjj2aml', 0, NULL, '2020-07-10 10:44:05', '2020-07-10 10:44:05', 0),
(68, 'Document.rtf', '/var/www/maxglobal/storage/uploads/2020-07-10-104431-Document.rtf', 'rtf', '', 1, '4lickpgn9nzgjweoa2ft', 0, NULL, '2020-07-10 10:44:31', '2020-07-10 10:44:31', 0),
(69, 'image.jpeg', '/var/www/maxglobal/storage/uploads/2020-07-23-114929-image.jpeg', 'jpeg', '', 1, 'yfohmny7n2bgfzkczgdq', 0, NULL, '2020-07-23 11:49:29', '2020-07-23 11:49:29', 0),
(70, 'email1.png', '/var/www/maxglobal/storage/uploads/2020-07-23-115120-email1.png', 'png', '', 1, 'rebt0s95qjuksj60rf3g', 0, NULL, '2020-07-23 11:51:20', '2020-07-23 11:51:20', 0),
(71, '201_HD_Cam.jpg', '/var/www/maxglobal/storage/uploads/2020-07-23-115234-201_HD_Cam.jpg', 'jpg', '', 1, 'jydencyblchbndxnxz1t', 0, NULL, '2020-07-23 11:52:34', '2020-07-23 11:52:34', 0),
(72, 'dp.jpg', '/var/www/maxglobal/storage/uploads/2020-08-09-062224-dp.jpg', 'jpg', '', 1, 'dt9hp5hkyxcbnyztmru0', 0, NULL, '2020-08-09 06:22:24', '2020-08-09 06:22:24', 0),
(73, 'skype.png', '/var/www/maxglobal/storage/uploads/2020-08-09-071016-skype.png', 'png', '', 1, 'olbeuchzt1cmtt8rsj3i', 0, NULL, '2020-08-09 07:10:16', '2020-08-09 07:10:16', 0),
(74, 'passport.jpg', '/var/www/maxglobal/storage/uploads/2020-08-09-071107-passport.jpg', 'jpg', '', 1, 'cf3pco0xpyxgiklvdk44', 0, NULL, '2020-08-09 07:11:07', '2020-08-09 07:11:07', 0),
(75, 'dp.jpg', '/var/www/maxglobal/storage/uploads/2020-08-11-023015-dp.jpg', 'jpg', '', 1, 'noscqioynpi3zukwxbx2', 0, NULL, '2020-08-11 02:30:15', '2021-03-18 18:12:41', 0),
(76, 'passport.jpg', '/var/www/maxglobal/storage/uploads/2020-08-11-023954-passport.jpg', 'jpg', '', 1, 'hrpe2s5dtxr31irmxhjd', 0, NULL, '2020-08-11 02:39:54', '2020-08-11 02:39:54', 0),
(77, 'wallpaper.jpg', '/var/www/maxglobal/storage/uploads/2020-08-11-024109-wallpaper.jpg', 'jpg', '', 1, '7hdz7f7lfgcnulzj9ivx', 0, NULL, '2020-08-11 02:41:09', '2020-08-11 02:41:09', 0),
(78, 'passport.jpg', '/var/www/maxglobal/storage/uploads/2020-08-11-024122-passport.jpg', 'jpg', '', 1, 'bkyd2ngbovsdaowfzfh0', 0, NULL, '2020-08-11 02:41:22', '2020-08-11 02:41:22', 0),
(79, 'wallpaper.jpg', '/var/www/maxglobal/storage/uploads/2020-08-11-024139-wallpaper.jpg', 'jpg', '', 1, 'e6x88xswrnww6dynkhrz', 0, NULL, '2020-08-11 02:41:39', '2020-08-11 02:41:39', 0),
(80, 'passport.jpg', '/var/www/maxglobal/storage/uploads/2020-08-11-025017-passport.jpg', 'jpg', '', 1, 'bhvk38k20rgurotdh89z', 0, NULL, '2020-08-11 02:50:17', '2020-08-11 02:50:17', 0),
(81, 'passport.jpg', '/var/www/maxglobal/storage/uploads/2020-08-11-025048-passport.jpg', 'jpg', '', 1, 'butqb330enprs9qzykri', 0, NULL, '2020-08-11 02:50:48', '2020-08-11 02:50:48', 0),
(82, 'wallpaper.jpg', '/var/www/maxglobal/storage/uploads/2020-08-11-030635-wallpaper.jpg', 'jpg', '', 1, '4d1guhk1ycj6ts9ycn79', 0, NULL, '2020-08-11 03:06:35', '2020-08-11 03:06:35', 0),
(83, 'skype.png', '/var/www/maxglobal/storage/uploads/2020-08-11-030647-skype.png', 'png', '', 1, 'rvfquw4qor5gukleaxxf', 0, NULL, '2020-08-11 03:06:47', '2020-08-11 03:06:47', 0),
(84, 'skype.png', '/var/www/maxglobal/storage/uploads/2020-08-11-032638-skype.png', 'png', '', 1, 'mjqt7lqowcchx7lvwnqf', 0, NULL, '2020-08-11 03:26:38', '2020-08-11 03:26:38', 0),
(85, 'candidate_rearrange_order.odt', '/var/www/maxglobal/storage/uploads/2020-08-11-032700-candidate_rearrange_order.odt', 'odt', '', 1, 'gk0j52uyhtntwmenjyid', 0, NULL, '2020-08-11 03:27:00', '2020-08-11 03:27:00', 0),
(86, 'candidate_rearrange_order.odt', '/var/www/maxglobal/storage/uploads/2020-08-11-032922-candidate_rearrange_order.odt', 'odt', '', 1, 'j7jpeb7sqj8puz70okvc', 0, NULL, '2020-08-11 03:29:22', '2020-08-11 03:29:22', 0),
(87, 'dp.jpg', '/var/www/maxglobal/storage/uploads/2020-08-23-031837-dp.jpg', 'jpg', '', 1, 'fcqy8clfszhkwwimzpnm', 0, NULL, '2020-08-23 03:18:37', '2020-08-23 03:18:37', 0),
(88, 'skype.png', '/var/www/maxglobal/storage/uploads/2020-08-23-033213-skype.png', 'png', '', 1, '3j9wgsrzsak9fqcwwxya', 0, NULL, '2020-08-23 03:32:13', '2020-08-23 03:32:13', 0),
(89, 'passport.jpg', '/var/www/maxglobal/storage/uploads/2020-08-23-033229-passport.jpg', 'jpg', '', 1, 'qs9orbsxfrydw8p2bm4m', 0, NULL, '2020-08-23 03:32:29', '2020-08-23 03:32:29', 0),
(90, 'Student Changes.odt', '/var/www/maxglobal/storage/uploads/2020-08-23-033934-Student Changes.odt', 'odt', '', 1, '6sdufeq8fp7ag354sc0u', 0, NULL, '2020-08-23 03:39:34', '2020-08-23 03:39:34', 0),
(91, 'candidate_rearrange_order.odt', '/var/www/maxglobal/storage/uploads/2020-08-23-034140-candidate_rearrange_order.odt', 'odt', '', 1, 'qm855xafbrvzpidrfhum', 0, NULL, '2020-08-23 03:41:40', '2020-08-23 03:41:40', 0),
(92, 'Student Changes.odt', '/var/www/maxglobal/storage/uploads/2020-08-23-034213-Student Changes.odt', 'odt', '', 1, 'io46e5snndmajqgewfd9', 0, NULL, '2020-08-23 03:42:13', '2020-08-23 03:42:13', 0),
(93, 'MultipleDocumentUpload_Plan.png', '/var/www/maxglobal/storage/uploads/2020-08-23-034234-MultipleDocumentUpload_Plan.png', 'png', '', 1, 'rdllbonvle69mbrgj1uw', 0, NULL, '2020-08-23 03:42:34', '2020-08-23 03:42:35', 0),
(94, 'candidate_rearrange_order.odt', '/var/www/maxglobal/storage/uploads/2020-08-23-034256-candidate_rearrange_order.odt', 'odt', '', 1, 'gf4pdfinwvv7mejtbtxq', 0, NULL, '2020-08-23 03:42:56', '2020-08-23 03:42:56', 0),
(95, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-08-23-034701-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, 'tzewput57wsl0gzly37c', 0, NULL, '2020-08-23 03:47:01', '2020-08-23 03:47:01', 0),
(96, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-08-23-035203-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, 'zbrvaztsnfff1bwct3ib', 0, NULL, '2020-08-23 03:52:03', '2020-08-23 03:52:03', 0),
(97, 'candidate_rearrange_order.odt', '/var/www/maxglobal/storage/uploads/2020-08-23-035249-candidate_rearrange_order.odt', 'odt', '', 1, 'oeimt3yywbmgm8hmwdyk', 0, NULL, '2020-08-23 03:52:49', '2020-08-23 03:52:49', 0),
(98, 'candidate_rearrange_order.odt', '/var/www/maxglobal/storage/uploads/2020-08-23-035344-candidate_rearrange_order.odt', 'odt', '', 1, 'z4tilshtxblgdcw8t16x', 0, NULL, '2020-08-23 03:53:44', '2020-08-23 03:53:44', 0),
(99, 'candidate_rearrange_order.odt', '/var/www/maxglobal/storage/uploads/2020-08-23-043250-candidate_rearrange_order.odt', 'odt', '', 1, 'ejl3x8cbsfljjbvmrlek', 0, NULL, '2020-08-23 04:32:50', '2020-08-23 04:32:50', 0),
(100, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-08-23-043554-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, 'rhlypglc0u1y2uibgr7q', 0, NULL, '2020-08-23 04:35:54', '2020-08-23 04:35:54', 0),
(101, 'candidate_rearrange_order.odt', '/var/www/maxglobal/storage/uploads/2020-08-23-044026-candidate_rearrange_order.odt', 'odt', '', 1, 'k1tio7h1dstf6dkulmzy', 0, NULL, '2020-08-23 04:40:26', '2020-08-23 04:40:26', 0),
(102, 'candidate_rearrange_order.odt', '/var/www/maxglobal/storage/uploads/2020-08-23-044054-candidate_rearrange_order.odt', 'odt', '', 1, 'bblinm3i30xfreatsrz9', 0, NULL, '2020-08-23 04:40:54', '2020-08-23 04:40:54', 0),
(103, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-08-23-044111-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, '72rlos8pmz7kcjgilqyk', 0, NULL, '2020-08-23 04:41:11', '2020-08-23 04:41:11', 0),
(104, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-08-23-044122-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, 'pa9lq3tega1m3f6kj5gd', 0, NULL, '2020-08-23 04:41:22', '2020-08-23 04:41:22', 0),
(105, 'passport.jpg', '/var/www/maxglobal/storage/uploads/2020-08-23-045131-passport.jpg', 'jpg', '', 1, 'ckcyc2agmofudzojmvz2', 0, NULL, '2020-08-23 04:51:31', '2020-08-23 04:51:31', 0),
(106, 'candidate_rearrange_order.odt', '/var/www/maxglobal/storage/uploads/2020-08-23-050922-candidate_rearrange_order.odt', 'odt', '', 1, '3sw2fogek7kceuqqqspx', 0, NULL, '2020-08-23 05:09:22', '2020-08-23 05:09:22', 0),
(107, 'candidate_rearrange_order.odt', '/var/www/maxglobal/storage/uploads/2020-08-23-051017-candidate_rearrange_order.odt', 'odt', '', 1, 'nirbbyy7teg698vtuwoh', 0, NULL, '2020-08-23 05:10:17', '2020-08-23 05:10:17', 0),
(108, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-08-23-051027-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, '9hemx5hr1oitp23rnlfs', 0, NULL, '2020-08-23 05:10:27', '2020-08-23 05:10:27', 0),
(109, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-08-23-051050-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, 'uaiuqelltktpdqcrtije', 0, NULL, '2020-08-23 05:10:50', '2020-08-23 05:10:50', 0),
(110, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-08-23-051109-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, 'ashq52nynkqgzd1cqabk', 0, NULL, '2020-08-23 05:11:09', '2020-08-23 05:11:09', 0),
(111, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-08-23-051123-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, 'cssg0pvceqqb7job8ylj', 0, NULL, '2020-08-23 05:11:23', '2020-08-23 05:11:23', 0),
(112, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-08-23-051136-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, 'fyrhi3opjppv5chtw0so', 0, NULL, '2020-08-23 05:11:36', '2020-08-23 05:11:36', 0),
(113, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-08-23-051159-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, 'wy7j5nzjvgw8rhpraqhn', 0, NULL, '2020-08-23 05:11:59', '2020-08-23 05:11:59', 0),
(114, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-08-23-051208-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, 'jex3cjytcqbobpk9ezxu', 0, NULL, '2020-08-23 05:12:08', '2020-08-23 05:12:08', 0),
(115, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-08-23-051220-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, '3qu02ioydcbejajoew5t', 0, NULL, '2020-08-23 05:12:20', '2020-08-23 05:12:20', 0),
(116, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-08-23-051417-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, 'sv7lbvbte1agdagkmsuh', 0, NULL, '2020-08-23 05:14:17', '2020-08-23 05:14:17', 0),
(117, 'MaxGlobalConsultants_website_Final_doc (1).pdf', '/var/www/maxglobal/storage/uploads/2020-08-23-054610-MaxGlobalConsultants_website_Final_doc (1).pdf', 'pdf', '', 1, 'uy3myrqlb0jeccijiuie', 0, NULL, '2020-08-23 05:46:10', '2020-08-23 05:46:10', 0),
(118, 'candidate_rearrange_order.odt', '/var/www/maxglobal/storage/uploads/2020-08-23-054617-candidate_rearrange_order.odt', 'odt', '', 1, 'moojrwvqn7bh1vft3tjk', 0, NULL, '2020-08-23 05:46:17', '2020-08-23 05:46:17', 0),
(119, 'max.odt', '/var/www/maxglobal/storage/uploads/2020-08-23-054635-max.odt', 'odt', '', 1, 'qkfnc3wmhaiyakgfukgb', 0, NULL, '2020-08-23 05:46:35', '2020-08-23 05:46:35', 0),
(120, 'tabs-r.gif', '/var/www/maxglobal/storage/uploads/2020-10-06-045845-tabs-r.gif', 'gif', '', 1, '9iwaf6vodbu1bgpjlmgw', 0, NULL, '2020-10-06 04:58:45', '2020-10-06 04:58:45', 0),
(121, 'menu-right.gif', '/var/www/maxglobal/storage/uploads/2020-10-06-045902-menu-right.gif', 'gif', '', 1, 'cksf5yiehf9xuzhpkxxe', 0, NULL, '2020-10-06 04:59:02', '2020-10-06 04:59:02', 0),
(122, 'g-top-3.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-045942-g-top-3.jpg', 'jpg', '', 1, 'reayn0zbkh2smnyqu2lv', 0, NULL, '2020-10-06 04:59:42', '2020-10-06 04:59:42', 0),
(123, 'g2.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-062156-g2.jpg', 'jpg', '', 1, 'gdvhwl1h6zrgmvs1rv0w', 0, NULL, '2020-10-06 06:21:56', '2020-10-06 06:21:56', 0),
(124, 'g2.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-062756-g2.jpg', 'jpg', '', 1, 'zn4oxfc9ifrjigjty4pv', 0, NULL, '2020-10-06 06:27:56', '2020-10-06 06:27:56', 0),
(125, 'Tulips.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-063829-Tulips.jpg', 'jpg', '', 1, 'ckrx3sx7tvl4vfwlsh6x', 0, NULL, '2020-10-06 06:38:29', '2020-10-06 06:38:29', 0),
(126, 'Tulips.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-064300-Tulips.jpg', 'jpg', '', 1, 'go7p8vjy12qdmxo3azyv', 0, NULL, '2020-10-06 06:43:00', '2020-10-06 06:43:00', 0),
(127, 'Tulips.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-064424-Tulips.jpg', 'jpg', '', 1, 'grrfkcnorus6mhl258so', 0, NULL, '2020-10-06 06:44:24', '2020-10-06 06:44:24', 0),
(128, 'Tulips.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-064540-Tulips.jpg', 'jpg', '', 1, 'mrghl9nufcf09pgbgznb', 0, NULL, '2020-10-06 06:45:40', '2020-10-06 06:45:40', 0),
(129, 'Tulips.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-064603-Tulips.jpg', 'jpg', '', 1, 'z4mnkun8bpia4qkn86jl', 0, NULL, '2020-10-06 06:46:03', '2020-10-06 06:46:03', 0),
(130, 'Tulips.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-064637-Tulips.jpg', 'jpg', '', 1, 't0diar20fhaddbktjbco', 0, NULL, '2020-10-06 06:46:37', '2020-10-06 06:46:37', 0),
(131, 'Tulips.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-065002-Tulips.jpg', 'jpg', '', 1, 'bfabuliphwzk8fmrjqw3', 0, NULL, '2020-10-06 06:50:02', '2020-10-06 06:50:02', 0),
(132, 'Tulips.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-065157-Tulips.jpg', 'jpg', '', 1, 'kdre2aqybezjwqwt2egy', 0, NULL, '2020-10-06 06:51:57', '2020-10-06 06:51:57', 0),
(133, 'g2.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-071101-g2.jpg', 'jpg', '', 1, 'xofb0zzv1iwdol0v7451', 0, NULL, '2020-10-06 07:11:01', '2020-10-06 07:11:01', 0),
(134, 'Tulips.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-072523-Tulips.jpg', 'jpg', '', 1, 'vq6y2vumrggxxu2heorm', 0, NULL, '2020-10-06 07:25:23', '2020-10-06 07:25:23', 0),
(135, 'Tulips.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-072608-Tulips.jpg', 'jpg', '', 1, 'vwvzasy8xcflm8vqfilj', 0, NULL, '2020-10-06 07:26:08', '2020-10-06 07:26:08', 0),
(136, 'Tulips.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-072616-Tulips.jpg', 'jpg', '', 1, 'ngr8sieedavplvvehcfn', 0, NULL, '2020-10-06 07:26:16', '2020-10-06 07:26:16', 0),
(137, 'Tulips.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-072629-Tulips.jpg', 'jpg', '', 1, '2uaed6zj8zplqvsxkrsw', 0, NULL, '2020-10-06 07:26:29', '2020-10-06 07:26:29', 0),
(138, 'Tulips.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-072707-Tulips.jpg', 'jpg', '', 1, 'kddw0vqeaf3mrld677sd', 0, NULL, '2020-10-06 07:27:07', '2020-10-06 07:27:07', 0),
(139, 'Tulips.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-072718-Tulips.jpg', 'jpg', '', 1, 'a0acfw8iff21jrazoxgx', 0, NULL, '2020-10-06 07:27:18', '2020-10-06 07:27:18', 0),
(140, 'Tulips.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-072753-Tulips.jpg', 'jpg', '', 1, '2ulndwkfam3mhcosfiu9', 0, NULL, '2020-10-06 07:27:53', '2020-10-06 07:27:53', 0),
(141, 'Tulips.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-073354-Tulips.jpg', 'jpg', '', 1, '4a7serzam6sh23ihiqd4', 0, NULL, '2020-10-06 07:33:54', '2020-10-06 07:33:54', 0),
(142, 'Tulips.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-073508-Tulips.jpg', 'jpg', '', 1, 'vo7d5vmfbszfbxgkuqeo', 0, NULL, '2020-10-06 07:35:08', '2020-10-06 07:35:08', 0),
(143, 'Tulips.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-073609-Tulips.jpg', 'jpg', '', 1, 'io3nlnexq1qnqzyujgal', 0, NULL, '2020-10-06 07:36:09', '2020-10-06 07:36:09', 0),
(144, 'Tulips.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-073723-Tulips.jpg', 'jpg', '', 1, 'y2cuvekq5zz8hhgkhtbd', 0, NULL, '2020-10-06 07:37:23', '2020-10-06 07:37:23', 0),
(145, 'g2.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-074523-g2.jpg', 'jpg', '', 1, 'rok5paj9wlroobsd9zdy', 0, NULL, '2020-10-06 07:45:23', '2020-10-06 07:45:23', 0),
(146, '4.png', '/var/www/maxglobal/storage/uploads/2020-10-06-074539-4.png', 'png', '', 1, 'mhsujpnr41aqp36dgolq', 0, NULL, '2020-10-06 07:45:39', '2020-10-06 07:45:39', 0),
(147, 'g2.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-074556-g2.jpg', 'jpg', '', 1, 'p2dp797re3qybsypaaqi', 0, NULL, '2020-10-06 07:45:56', '2020-10-06 07:45:56', 0),
(148, 'g2.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-075330-g2.jpg', 'jpg', '', 1, 'w0r1dnefugdjywwivqv5', 0, NULL, '2020-10-06 07:53:30', '2020-10-06 07:53:30', 0),
(149, 'g2.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-075516-g2.jpg', 'jpg', '', 1, 'om4bzjnlfilz6amo6d73', 0, NULL, '2020-10-06 07:55:16', '2020-10-06 07:55:16', 0),
(150, 'g2.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-075610-g2.jpg', 'jpg', '', 1, 'kzxowdpcb9bttwklobl6', 0, NULL, '2020-10-06 07:56:10', '2020-10-06 07:56:10', 0),
(151, 'g2.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-080021-g2.jpg', 'jpg', '', 1, 'morgusejquyqe4hni6e8', 0, NULL, '2020-10-06 08:00:21', '2020-10-06 08:00:21', 0),
(152, '2.png', '/var/www/maxglobal/storage/uploads/2020-10-06-080340-2.png', 'png', '', 1, 'fp4jlzesnmmycpfs5smp', 0, NULL, '2020-10-06 08:03:40', '2020-10-06 08:03:40', 0),
(153, '2.png', '/var/www/maxglobal/storage/uploads/2020-10-06-080505-2.png', 'png', '', 1, 'pukbxm0nn0johz5hnchn', 0, NULL, '2020-10-06 08:05:05', '2020-10-06 08:05:05', 0),
(154, '1.png', '/var/www/maxglobal/storage/uploads/2020-10-06-080858-1.png', 'png', '', 1, '2spirnbndaq4bl39hmqh', 0, NULL, '2020-10-06 08:08:58', '2020-10-06 08:08:58', 0),
(155, '1.png', '/var/www/maxglobal/storage/uploads/2020-10-06-081027-1.png', 'png', '', 1, 'eaiziyah8qyjsok8blhx', 0, NULL, '2020-10-06 08:10:27', '2020-10-06 08:10:27', 0),
(156, '1.png', '/var/www/maxglobal/storage/uploads/2020-10-06-081228-1.png', 'png', '', 1, 'jg4yrvitnokdbobm7zab', 0, NULL, '2020-10-06 08:12:28', '2020-10-06 08:12:28', 0),
(157, 'g2.jpg', '/var/www/maxglobal/storage/uploads/2020-10-06-081447-g2.jpg', 'jpg', '', 1, 'v9mev34ljjtx5nqvhskd', 0, NULL, '2020-10-06 08:14:47', '2020-10-06 08:14:47', 0),
(158, '2.png', '/var/www/maxglobal/storage/uploads/2020-10-06-083055-2.png', 'png', '', 1, 'snpal8doknmzvbw04u3l', 0, NULL, '2020-10-06 08:30:55', '2020-10-06 08:30:55', 0),
(159, '2.png', '/var/www/maxglobal/storage/uploads/2020-10-06-083313-2.png', 'png', '', 1, 'watbslpolqvcn1j5ikhl', 0, NULL, '2020-10-06 08:33:13', '2020-10-06 08:33:13', 0),
(160, '1.png', '/var/www/maxglobal/storage/uploads/2020-10-06-091022-1.png', 'png', '', 1, 'vlqijbugrk2zknlg8hvy', 0, NULL, '2020-10-06 09:10:22', '2020-10-06 09:10:22', 0),
(161, '1.png', '/var/www/maxglobal/storage/uploads/2020-10-06-091134-1.png', 'png', '', 1, '5hm3qvthx4m596foeagj', 0, NULL, '2020-10-06 09:11:34', '2020-10-06 09:11:34', 0),
(162, '4.png', '/var/www/maxglobal/storage/uploads/2020-10-06-091331-4.png', 'png', '', 1, '1yei7td9igzp88ryvb0o', 0, NULL, '2020-10-06 09:13:31', '2020-10-06 09:13:31', 0),
(163, '4.png', '/var/www/maxglobal/storage/uploads/2020-10-06-091517-4.png', 'png', '', 1, 'ndclubkhemmq63gvov1f', 0, NULL, '2020-10-06 09:15:17', '2020-10-06 09:15:17', 0),
(164, '4.png', '/var/www/maxglobal/storage/uploads/2020-10-06-091600-4.png', 'png', '', 1, '5oi8s6v06vfvugw4098n', 0, NULL, '2020-10-06 09:16:00', '2020-10-06 09:16:00', 0),
(165, '2.png', '/var/www/maxglobal/storage/uploads/2020-10-06-091943-2.png', 'png', '', 1, 'qghzo34ix3kam65gej46', 0, NULL, '2020-10-06 09:19:43', '2020-10-06 09:19:43', 0),
(166, '4.png', '/var/www/maxglobal/storage/uploads/2020-10-06-092104-4.png', 'png', '', 1, 'iarvodzaf1rpdlcxplb2', 0, NULL, '2020-10-06 09:21:04', '2020-10-06 09:21:04', 0),
(167, '2.png', '/var/www/maxglobal/storage/uploads/2020-10-06-092207-2.png', 'png', '', 1, 'qmixiefqvmkbxbr3daza', 0, NULL, '2020-10-06 09:22:07', '2020-10-06 09:22:07', 0),
(168, '2.png', '/var/www/maxglobal/storage/uploads/2020-10-06-092323-2.png', 'png', '', 1, 'jqblx0agedgsduv7mnz2', 0, NULL, '2020-10-06 09:23:23', '2020-10-06 09:23:23', 0),
(169, '2.png', '/var/www/maxglobal/storage/uploads/2020-10-06-094101-2.png', 'png', '', 1, 'o5s1vtwsv1ltsxgys96i', 0, NULL, '2020-10-06 09:41:01', '2020-10-06 09:41:01', 0),
(170, '2.png', '/var/www/maxglobal/storage/uploads/2020-10-06-094657-2.png', 'png', '', 1, 'xjhywvnjt50snpcz46ga', 0, NULL, '2020-10-06 09:46:57', '2020-10-06 09:46:57', 0),
(171, '2.png', '/var/www/maxglobal/storage/uploads/2020-10-06-100321-2.png', 'png', '', 1, 'q0fzgvaans1dituv524v', 0, NULL, '2020-10-06 10:03:21', '2020-10-06 10:03:21', 0),
(172, '4.png', '/var/www/maxglobal/storage/uploads/2020-10-06-100911-4.png', 'png', '', 1, 'bp80otlucy6waz7owjji', 0, NULL, '2020-10-06 10:09:11', '2020-10-06 10:09:11', 0),
(173, '4.png', '/var/www/maxglobal/storage/uploads/2020-10-06-101215-4.png', 'png', '', 1, 'c1j5tyecitzdoxlyjncs', 0, NULL, '2020-10-06 10:12:15', '2020-10-06 10:12:15', 0),
(174, '1.png', '/var/www/maxglobal/storage/uploads/2020-10-06-115000-1.png', 'png', '', 1, '1dqo1gzv0ukb11yyaiw8', 0, NULL, '2020-10-06 11:50:00', '2020-10-06 11:50:00', 0),
(175, '1.png', '/var/www/maxglobal/storage/uploads/2020-10-06-115138-1.png', 'png', '', 1, 'fh6w8rouy2lhfzvifycm', 0, NULL, '2020-10-06 11:51:38', '2020-10-06 11:51:38', 0),
(176, '1.png', '/var/www/maxglobal/storage/uploads/2020-10-06-115256-1.png', 'png', '', 1, 'xr3wgnnc7lmkvmhjgtdm', 0, NULL, '2020-10-06 11:52:56', '2020-10-06 11:52:56', 0),
(177, '1.png', '/var/www/maxglobal/storage/uploads/2020-10-06-115554-1.png', 'png', '', 1, 'hx3k4ubymbb3sh4qopcp', 0, NULL, '2020-10-06 11:55:54', '2020-10-06 11:55:54', 0),
(178, '4.png', '/var/www/maxglobal/storage/uploads/2020-10-06-115910-4.png', 'png', '', 1, 'snnukcwgkhbi08uzrgyh', 0, NULL, '2020-10-06 11:59:10', '2020-10-06 11:59:10', 0),
(179, '4.png', '/var/www/maxglobal/storage/uploads/2020-10-06-120116-4.png', 'png', '', 1, 'njmimo9sdbgxeh5vihj2', 0, NULL, '2020-10-06 12:01:16', '2020-10-06 12:01:16', 0),
(180, '1.png', '/var/www/maxglobal/storage/uploads/2020-10-06-120223-1.png', 'png', '', 1, 'mmzt3fhx1sunsb9punnq', 0, NULL, '2020-10-06 12:02:23', '2020-10-06 12:02:23', 0),
(181, 'Chrysanthemum.jpg', '/var/www/maxglobal/storage/uploads/2021-03-18-181942-Chrysanthemum.jpg', 'jpg', '', 1, '57ujnvhrjrbfeh1a1ids', 0, NULL, '2021-03-18 18:19:42', '2021-03-18 18:19:42', 0),
(182, 'Desert.jpg', '/var/www/maxglobal/storage/uploads/2021-03-18-181958-Desert.jpg', 'jpg', '', 1, 'kpaykeztdmyx1vrklqlh', 0, NULL, '2021-03-18 18:19:58', '2021-03-18 18:19:58', 0),
(183, 'Koala.jpg', '/var/www/maxglobal/storage/uploads/2021-03-18-182009-Koala.jpg', 'jpg', '', 1, 'pya7ds10ntzidvzltwam', 0, NULL, '2021-03-18 18:20:09', '2021-03-18 18:20:09', 0),
(184, 'Lighthouse.jpg', '/var/www/maxglobal/storage/uploads/2021-03-18-182417-Lighthouse.jpg', 'jpg', '', 1, 'xnpdcxu6jxy03ep1hg42', 0, NULL, '2021-03-18 18:24:17', '2021-03-18 18:24:17', 0),
(185, 'Desert.jpg', '/var/www/maxglobal/storage/uploads/2021-03-18-182440-Desert.jpg', 'jpg', '', 1, 'ydqyguuhvym818swwlzn', 0, NULL, '2021-03-18 18:24:40', '2021-03-18 18:24:40', 0),
(186, 'Lighthouse.jpg', '/var/www/maxglobal/storage/uploads/2021-03-18-182503-Lighthouse.jpg', 'jpg', '', 1, 'is0wlfha4nbrtoqvd9gh', 0, NULL, '2021-03-18 18:25:03', '2021-03-18 18:25:03', 0),
(187, 'Chrysanthemum.jpg', '/var/www/maxglobal/storage/uploads/2021-03-18-182629-Chrysanthemum.jpg', 'jpg', '', 1, 'ipujum3tkjvhxhtmhgbj', 0, NULL, '2021-03-18 18:26:29', '2021-03-18 18:26:29', 0),
(188, 'Koala.jpg', '/var/www/maxglobal/storage/uploads/2021-03-18-182636-Koala.jpg', 'jpg', '', 1, 'tifwkmtxpdhz3utyssrn', 0, NULL, '2021-03-18 18:26:36', '2021-03-18 18:26:36', 0),
(189, 'Lighthouse.jpg', '/var/www/maxglobal/storage/uploads/2021-03-18-182643-Lighthouse.jpg', 'jpg', '', 1, 'd2jfyst4h94qjvo8gigz', 0, NULL, '2021-03-18 18:26:43', '2021-03-18 18:26:43', 0),
(190, 'Penguins.jpg', '/var/www/maxglobal/storage/uploads/2021-03-28-185433-Penguins.jpg', 'jpg', '', 1, 'alfoducpiegstufgzaef', 0, NULL, '2021-03-28 18:54:33', '2021-03-28 18:54:33', 0),
(191, 'Jellyfish.jpg', '/var/www/maxglobal/storage/uploads/2021-03-28-185500-Jellyfish.jpg', 'jpg', '', 1, 'ekmleely95fuarhevanj', 0, NULL, '2021-03-28 18:55:00', '2021-03-28 18:55:00', 0),
(192, 'avatar-img-01.jpg', '/var/www/maxglobal/storage/uploads/2021-03-29-044706-avatar-img-01.jpg', 'jpg', '', 1, 'b7hyppqk7ghi4tkrxrft', 0, NULL, '2021-03-29 04:47:06', '2021-03-29 04:47:07', 0),
(193, 'browser_logos-100734193-large.jpg', '/var/www/maxglobal/storage/uploads/2021-03-29-044912-browser_logos-100734193-large.jpg', 'jpg', '', 1, '3nvsjcktfrdacjgsomxm', 0, NULL, '2021-03-29 04:49:12', '2021-03-29 04:49:12', 0),
(194, 'M essi_Portrait.jpg', '/var/www/maxglobal/storage/uploads/2021-03-29-090722-M essi_Portrait.jpg', 'jpg', '', 1, 'pa385u2pty5xk5m1an1j', 0, NULL, '2021-03-29 09:07:22', '2021-03-29 09:07:22', 0),
(195, '33333333.png', '/var/www/maxglobal/storage/uploads/2021-03-29-090830-33333333.png', 'png', '', 1, 'ifpcaytnmbuvozgwmikw', 0, NULL, '2021-03-29 09:08:30', '2021-03-29 09:08:30', 0),
(196, 'Screenshot from 2020-05-12 09-22-14.png', '/var/www/maxglobal/storage/uploads/2021-03-29-090847-Screenshot from 2020-05-12 09-22-14.png', 'png', '', 1, 'bowxic4wnft10hatubsx', 0, NULL, '2021-03-29 09:08:47', '2021-03-29 09:08:47', 0),
(197, 'Screenshot from 2019-11-21 15-44-52.png', '/var/www/maxglobal/storage/uploads/2021-03-29-090930-Screenshot from 2019-11-21 15-44-52.png', 'png', '', 1, 'amo1tmkx0oucrwct5bgq', 0, NULL, '2021-03-29 09:09:30', '2021-03-29 09:09:30', 0),
(198, '111111111.png', '/var/www/maxglobal/storage/uploads/2021-03-29-090936-111111111.png', 'png', '', 1, 'nibnyy9bbybkmmtuahkl', 0, NULL, '2021-03-29 09:09:36', '2021-03-29 09:09:36', 0),
(199, 'bg.png', '/var/www/maxglobal/storage/uploads/2021-03-29-090953-bg.png', 'png', '', 1, 'bujinhsp1bkayrg5lewy', 0, NULL, '2021-03-29 09:09:53', '2021-03-29 09:09:53', 0),
(200, 'bg.png', '/var/www/maxglobal/storage/uploads/2021-03-29-090958-bg.png', 'png', '', 1, 'wya9fwqlfmlxmpswq0ct', 0, NULL, '2021-03-29 09:09:58', '2021-03-29 09:09:58', 0),
(201, 'bg.png', '/var/www/maxglobal/storage/uploads/2021-03-29-091004-bg.png', 'png', '', 1, 'no1zn4vvlysattkmcn2b', 0, NULL, '2021-03-29 09:10:04', '2021-03-29 09:10:04', 0),
(202, 'd 0012-43x43-bl.jpg', '/var/www/maxglobal/storage/uploads/2021-03-29-091025-d 0012-43x43-bl.jpg', 'jpg', '', 1, 'yyp7ydtk6ny2wzbc5xca', 0, NULL, '2021-03-29 09:10:25', '2021-03-29 09:10:25', 0),
(203, 'pexels-photo-248797.jpeg', '/var/www/maxglobal/storage/uploads/2021-03-29-091056-pexels-photo-248797.jpeg', 'jpeg', '', 1, 'gwnwokzoc7seccwyrllt', 0, NULL, '2021-03-29 09:10:56', '2021-03-29 09:10:56', 0),
(204, 'bg.png', '/var/www/maxglobal/storage/uploads/2021-03-29-091110-bg.png', 'png', '', 1, 'bkkabhb8f8x5uuxhvba6', 0, NULL, '2021-03-29 09:11:10', '2021-03-29 09:11:10', 0),
(205, 'd0012-43x43-bl.jpg', '/var/www/maxglobal/storage/uploads/2021-03-29-091121-d0012-43x43-bl.jpg', 'jpg', '', 1, 'spjcqdwyouj2dbjv0blu', 0, NULL, '2021-03-29 09:11:21', '2021-03-29 09:11:21', 0),
(206, 'd ownload11.jpeg', '/var/www/maxglobal/storage/uploads/2021-03-29-091133-d ownload11.jpeg', 'jpeg', '', 1, 'okis7votxddxm3x4aruh', 0, NULL, '2021-03-29 09:11:33', '2021-03-29 09:11:33', 0),
(207, 'aaaaaaaaaaaa.jpg', '/var/www/maxglobal/storage/uploads/2021-03-29-091327-aaaaaaaaaaaa.jpg', 'jpg', '', 1, 'w6umxwmvd0xkhuu9jz9v', 0, NULL, '2021-03-29 09:13:27', '2021-03-29 09:13:27', 0),
(208, 'imgpsh_fullsize_anim.jpeg', '/var/www/maxglobal/storage/uploads/2021-03-29-091706-imgpsh_fullsize_anim.jpeg', 'jpeg', '', 1, 'vemq3xxgooiwzogamibv', 0, NULL, '2021-03-29 09:17:06', '2021-03-29 09:17:06', 0),
(209, 'Screenshot.png', '/var/www/maxglobal/storage/uploads/2021-03-29-092749-Screenshot.png', 'png', '', 157, 'n1kfr8lpiz0jmuowlwhh', 0, NULL, '2021-03-29 09:27:49', '2021-03-29 09:27:49', 0),
(210, 'avatar-img-01.jpg', '/var/www/maxglobal/storage/uploads/2021-03-29-093221-avatar-img-01.jpg', 'jpg', '', 157, 'offbmuw2hnspxy8zfij6', 0, NULL, '2021-03-29 09:32:21', '2021-03-29 09:32:21', 0),
(211, 'avatar-img-01.jpg', '/var/www/maxglobal/storage/uploads/2021-03-29-093333-avatar-img-01.jpg', 'jpg', '', 157, 'ujcgxs6pxu0aseakebbh', 0, NULL, '2021-03-29 09:33:33', '2021-03-29 09:33:33', 0),
(212, 'avatar-img-01.jpg', '/var/www/maxglobal/storage/uploads/2021-03-29-093344-avatar-img-01.jpg', 'jpg', '', 157, 'lww68zpuhfcnpdx4oth5', 0, NULL, '2021-03-29 09:33:44', '2021-03-29 09:33:44', 0),
(213, 'avatar-img-01.jpg', '/var/www/maxglobal/storage/uploads/2021-03-29-093700-avatar-img-01.jpg', 'jpg', '', 157, 'mkkrlsbvmn81qxakcehr', 0, NULL, '2021-03-29 09:37:00', '2021-03-29 09:37:00', 0),
(214, 'avatar-img-01.jpg', '/var/www/maxglobal/storage/uploads/2021-03-29-093747-avatar-img-01.jpg', 'jpg', '', 157, 'n404vtgm3z8ihkoskfnr', 0, NULL, '2021-03-29 09:37:47', '2021-03-29 09:37:47', 0),
(215, 'student.png', '/var/www/maxglobal/storage/uploads/2021-03-29-101035-student.png', 'png', '', 155, 'awr0r4voawigpzue09x4', 0, NULL, '2021-03-29 10:10:35', '2021-03-29 10:10:35', 0),
(216, 'student.png', '/var/www/maxglobal/storage/uploads/2021-03-29-103528-student.png', 'png', '', 157, 'vmfxtnxefztgncaurw7n', 0, NULL, '2021-03-29 10:35:28', '2021-03-29 10:35:28', 0),
(217, 'avatar-img-01.jpg', '/var/www/maxglobal/storage/uploads/2021-03-29-104430-avatar-img-01.jpg', 'jpg', '', 155, '2ctbuqkd9uhybradhfpd', 0, NULL, '2021-03-29 10:44:30', '2021-03-29 10:44:30', 0),
(218, 'student.png', '/var/www/maxglobal/storage/uploads/2021-03-29-111109-student.png', 'png', '', 155, 'omgqxgppkku0cuitfga7', 0, NULL, '2021-03-29 11:11:09', '2021-03-29 11:11:09', 0),
(219, 'avatar-img-01.jpg', '/var/www/maxglobal/storage/uploads/2021-03-29-111135-avatar-img-01.jpg', 'jpg', '', 155, '5ths2ezhqcihdsrg5ctc', 0, NULL, '2021-03-29 11:11:35', '2021-03-29 11:11:35', 0),
(220, 'student.png', '/var/www/maxglobal/storage/uploads/2021-03-29-111352-student.png', 'png', '', 162, '8bvfatqt8u6nhteztfi8', 0, NULL, '2021-03-29 11:13:52', '2021-03-29 11:13:52', 0),
(221, 'avatar-img-01.jpg', '/var/www/maxglobal/storage/uploads/2021-03-29-111420-avatar-img-01.jpg', 'jpg', '', 162, 'cburk8yqwy30qhullawt', 0, NULL, '2021-03-29 11:14:20', '2021-03-29 11:14:20', 0),
(222, 'Registeronline_net_ad.jpg', '/var/www/maxglobal/storage/uploads/2021-03-29-111441-Registeronline_net_ad.jpg', 'jpg', '', 162, '8vimb9izl2gqhkpavfj4', 0, NULL, '2021-03-29 11:14:41', '2021-03-29 11:14:41', 0),
(223, 'student.png', '/var/www/maxglobal/storage/uploads/2021-03-29-112424-student.png', 'png', '', 162, 'dnjmtaqy0sfwuds2yu2n', 0, NULL, '2021-03-29 11:24:24', '2021-03-29 11:24:24', 0),
(224, 'Registeronline_net_ad.jpg', '/var/www/maxglobal/storage/uploads/2021-03-29-112446-Registeronline_net_ad.jpg', 'jpg', '', 162, 'pezpx59jeqjhqjbrxqim', 0, NULL, '2021-03-29 11:24:46', '2021-03-29 11:24:46', 0),
(225, 'TSD - gadgeon.pdf', '/var/www/maxglobal/storage/uploads/2021-03-29-112500-TSD - gadgeon.pdf', 'pdf', '', 162, 'sqo8lmyz025phqxah4uu', 0, NULL, '2021-03-29 11:25:00', '2021-03-29 11:25:00', 0),
(226, 'student.png', '/var/www/maxglobal/storage/uploads/2021-04-02-101141-student.png', 'png', '', 155, 'qethpvu4mybksxpxld5g', 0, NULL, '2021-04-02 10:11:41', '2021-04-02 10:11:41', 0),
(227, 'Age group-graph.png', '/var/www/maxglobal/storage/uploads/2021-04-02-101229-Age group-graph.png', 'png', '', 155, 'avzt5rcp7unkyjopzoih', 0, NULL, '2021-04-02 10:12:29', '2021-04-02 10:12:29', 0),
(228, 'brooke-cagle-_wYQnEAMAQk-unsplash.jpg', '/var/www/maxglobal/storage/uploads/2021-04-08-124138-brooke-cagle-_wYQnEAMAQk-unsplash.jpg', 'jpg', '', 172, 'u37ykgvp5a6zsddixvay', 0, NULL, '2021-04-08 12:41:38', '2021-04-08 12:41:38', 0),
(229, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-052548-avatar-img.jpg', 'jpg', '', 155, 'bloox2e0fsljghnc1rz2', 0, NULL, '2022-01-07 23:55:48', '2022-01-07 23:55:48', 0),
(230, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-052602-ev-img01.jpg', 'jpg', '', 155, 'utshets1rxwnftmhpwzd', 0, NULL, '2022-01-07 23:56:02', '2022-01-07 23:56:02', 0),
(231, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-052608-ev-img02.jpg', 'jpg', '', 155, 'lykxsf403td8sznpp2pb', 0, NULL, '2022-01-07 23:56:08', '2022-01-07 23:56:08', 0),
(232, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-052618-ev-img01.jpg', 'jpg', '', 155, 'k50e4o69chgmmtqtiyxg', 0, NULL, '2022-01-07 23:56:18', '2022-01-07 23:56:18', 0),
(233, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-052625-ev-img02.jpg', 'jpg', '', 155, 'ofbwdyomwhnpsmpuwyst', 0, NULL, '2022-01-07 23:56:25', '2022-01-07 23:56:25', 0),
(234, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-052838-avatar-img.jpg', 'jpg', '', 155, 'l5jhzgyoswschqle11dp', 0, NULL, '2022-01-07 23:58:38', '2022-01-07 23:58:38', 0),
(235, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-052919-ev-img01.jpg', 'jpg', '', 155, 'j2por8zsmzwldvltv9au', 0, NULL, '2022-01-07 23:59:19', '2022-01-07 23:59:19', 0),
(236, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-052925-ev-img02.jpg', 'jpg', '', 155, 'yi0hmht4aebkd8dacxwc', 0, NULL, '2022-01-07 23:59:25', '2022-01-07 23:59:25', 0),
(237, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-054718-A Sample PDF.pdf', 'pdf', '', 155, 'qtxalv6ss2fwtbun55qz', 0, NULL, '2022-01-08 00:17:18', '2022-01-08 00:17:18', 0),
(238, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-054837-A Sample PDF.pdf', 'pdf', '', 155, 'qbxtxfh7omiafl80tokn', 0, NULL, '2022-01-08 00:18:37', '2022-01-08 00:18:37', 0),
(239, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-055127-A Sample PDF.pdf', 'pdf', '', 155, 'm4m9bsupdcarb9hocc4e', 0, NULL, '2022-01-08 00:21:27', '2022-01-08 00:21:27', 0),
(240, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-055133-ev-img01.jpg', 'jpg', '', 155, 'gwvhfyu9lgw7pndanuin', 0, NULL, '2022-01-08 00:21:33', '2022-01-08 00:21:33', 0),
(241, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-055144-ev-img02.jpg', 'jpg', '', 155, 'tqfsgafa9tvptuomiatj', 0, NULL, '2022-01-08 00:21:44', '2022-01-08 00:21:44', 0),
(242, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-055156-ev-img03.jpg', 'jpg', '', 155, 'nzqnevhzy4amkc9ynorm', 0, NULL, '2022-01-08 00:21:56', '2022-01-08 00:21:56', 0),
(243, 'flower-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-055203-flower-img.jpg', 'jpg', '', 155, 'ifur81ckttl8c52u5efk', 0, NULL, '2022-01-08 00:22:03', '2022-01-08 00:22:03', 0),
(244, 'images555.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-055314-images555.jpg', 'jpg', '', 155, 'gg38n3q2dhg3i1haczcq', 0, NULL, '2022-01-08 00:23:14', '2022-01-08 00:23:14', 0),
(245, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-055319-A Sample PDF.pdf', 'pdf', '', 155, 'kygnwdrpefwm3d2qtqek', 0, NULL, '2022-01-08 00:23:19', '2022-01-08 00:23:19', 0),
(246, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-055908-A Sample PDF.pdf', 'pdf', '', 155, 'zvsnpjjqerylzsppzuc6', 0, NULL, '2022-01-08 00:29:08', '2022-01-08 00:29:08', 0),
(247, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-055913-A Sample PDF.pdf', 'pdf', '', 155, '8fuv1ygmc0p42vl66aov', 0, NULL, '2022-01-08 00:29:13', '2022-01-08 00:29:13', 0),
(248, 'flower-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-060236-flower-img.jpg', 'jpg', '', 155, 'c2qevyvbokhg9awjpjbo', 0, NULL, '2022-01-08 00:32:36', '2022-01-08 00:32:36', 0),
(249, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-061455-A Sample PDF.pdf', 'pdf', '', 155, 'g6pkeqzd1uaosunvkmlo', 0, NULL, '2022-01-08 00:44:55', '2022-01-08 00:44:55', 0),
(250, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-061500-A Sample PDF.pdf', 'pdf', '', 155, '70wj9npb0vfxso3n20nz', 0, NULL, '2022-01-08 00:45:00', '2022-01-08 00:45:00', 0),
(251, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-061517-A Sample PDF.pdf', 'pdf', '', 155, 'zxrjzmiwvdvnbewjfgf2', 0, NULL, '2022-01-08 00:45:17', '2022-01-08 00:45:17', 0),
(252, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-061522-A Sample PDF.pdf', 'pdf', '', 155, 'n8t1wvohtbdeobjvczaq', 0, NULL, '2022-01-08 00:45:22', '2022-01-08 00:45:22', 0),
(253, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-061526-A Sample PDF.pdf', 'pdf', '', 155, 'fskeabasbmkdefvqve2s', 0, NULL, '2022-01-08 00:45:26', '2022-01-08 00:45:26', 0),
(254, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-061531-A Sample PDF.pdf', 'pdf', '', 155, 'igzl5tmkglds539xschc', 0, NULL, '2022-01-08 00:45:31', '2022-01-08 00:45:31', 0),
(255, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-061604-A Sample PDF.pdf', 'pdf', '', 155, 'ohx4fhclmsoxqzsiba55', 0, NULL, '2022-01-08 00:46:04', '2022-01-08 00:46:04', 0),
(256, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-061608-A Sample PDF.pdf', 'pdf', '', 155, 'frw7sa8acdliy7zu1coo', 0, NULL, '2022-01-08 00:46:08', '2022-01-08 00:46:08', 0),
(257, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-061613-ev-img01.jpg', 'jpg', '', 155, 'u7cikajzaz6lk6oujcmj', 0, NULL, '2022-01-08 00:46:13', '2022-01-08 00:46:13', 0);
INSERT INTO `uploads` (`id`, `name`, `path`, `extension`, `caption`, `user_id`, `hash`, `public`, `deleted_at`, `created_at`, `updated_at`, `media`) VALUES
(258, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-061617-ev-img02.jpg', 'jpg', '', 155, 'g0zimhfz8dklwc5g2913', 0, NULL, '2022-01-08 00:46:17', '2022-01-08 00:46:17', 0),
(259, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-061640-A Sample PDF.pdf', 'pdf', '', 155, 'm9ojcoa3erwa7f8ybtsy', 0, NULL, '2022-01-08 00:46:40', '2022-01-08 00:46:40', 0),
(260, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-061645-A Sample PDF.pdf', 'pdf', '', 155, 'v3v1h4tioooayeci8zcl', 0, NULL, '2022-01-08 00:46:45', '2022-01-08 00:46:45', 0),
(261, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-061853-A Sample PDF.pdf', 'pdf', '', 155, 'lxjfbihc875t5ii0inv8', 0, NULL, '2022-01-08 00:48:53', '2022-01-08 00:48:53', 0),
(262, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-062007-ev-img02.jpg', 'jpg', '', 155, 'g3rcwtubvvidzqooleuc', 0, NULL, '2022-01-08 00:50:07', '2022-01-08 00:50:07', 0),
(263, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-062152-A Sample PDF.pdf', 'pdf', '', 155, 'vzor0fwwjog9mrlbfwrr', 0, NULL, '2022-01-08 00:51:52', '2022-01-08 00:51:52', 0),
(264, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-073048-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'vorinokvmpa8j1w4hztc', 0, NULL, '2022-01-08 02:00:48', '2022-01-08 02:00:48', 0),
(265, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-073112-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, '4hkvzbcd7obbn2eyw4o1', 0, NULL, '2022-01-08 02:01:12', '2022-01-08 02:01:12', 0),
(266, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-073117-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'jk6njnost2xyu7ounnpv', 0, NULL, '2022-01-08 02:01:17', '2022-01-08 02:01:17', 0),
(267, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-073123-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'qcnt30udupv4phqxqlwe', 0, NULL, '2022-01-08 02:01:23', '2022-01-08 02:01:23', 0),
(268, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-073133-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'ydgpxe33n9qerq0v52rp', 0, NULL, '2022-01-08 02:01:33', '2022-01-08 02:01:33', 0),
(269, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-073416-ev-img01.jpg', 'jpg', '', 155, 'vgb1u5vuj9y1w5bpbzzj', 0, NULL, '2022-01-08 02:04:16', '2022-01-08 02:04:16', 0),
(270, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-073447-ev-img02.jpg', 'jpg', '', 155, 'qqoeycn19ei36x8ih9yw', 0, NULL, '2022-01-08 02:04:47', '2022-01-08 02:04:47', 0),
(271, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-073843-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'toil2tnpatrgqymjhfb4', 0, NULL, '2022-01-08 02:08:43', '2022-01-08 02:08:43', 0),
(272, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-073853-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'wd2bhpcznzhbzo8edgvb', 0, NULL, '2022-01-08 02:08:53', '2022-01-08 02:08:53', 0),
(273, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-073901-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'stnxijkk17e00w6tkhe1', 0, NULL, '2022-01-08 02:09:01', '2022-01-08 02:09:01', 0),
(274, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-073908-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'z3ndodozws5vk1xihsl8', 0, NULL, '2022-01-08 02:09:08', '2022-01-08 02:09:08', 0),
(275, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-075552-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, '0yog9q1xj4cy155x2qom', 0, NULL, '2022-01-08 02:25:52', '2022-01-08 02:25:52', 0),
(276, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-090026-avatar-img.jpg', 'jpg', '', 155, 'wp9zqivf5ugo1xjwqltc', 0, NULL, '2022-01-08 03:30:26', '2022-01-08 03:30:26', 0),
(277, 'nouser.png', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-090120-nouser.png', 'png', '', 155, 'orrkjtdzo1cx6ozgm57h', 0, NULL, '2022-01-08 03:31:20', '2022-01-08 03:31:20', 0),
(278, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-090134-avatar-img.jpg', 'jpg', '', 155, 'aky0xmhipzuxzink2xax', 0, NULL, '2022-01-08 03:31:34', '2022-01-08 03:31:34', 0),
(279, 'nouser.png', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-090222-nouser.png', 'png', '', 155, 'csofmjgty21600hez1sj', 0, NULL, '2022-01-08 03:32:22', '2022-01-08 03:32:22', 0),
(280, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-090304-avatar-img.jpg', 'jpg', '', 155, '8zrr1dudub0cjlx3efle', 0, NULL, '2022-01-08 03:33:04', '2022-01-08 03:33:04', 0),
(281, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-091544-A Sample PDF.pdf', 'pdf', '', 155, 'hcwqzcokviydjqi7w5lf', 0, '2022-01-08 03:47:14', '2022-01-08 03:45:44', '2022-01-08 03:47:14', 0),
(282, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-091632-ev-img01.jpg', 'jpg', '', 155, 'kf8jxzgylry5ud8gkk38', 0, '2022-01-08 03:47:09', '2022-01-08 03:46:32', '2022-01-08 03:47:09', 0),
(283, 'nouser.png', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-091807-nouser.png', 'png', '', 155, 'qctxu9cwnvzp0ppruozn', 0, NULL, '2022-01-08 03:48:07', '2022-01-08 03:48:07', 0),
(284, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-091846-avatar-img.jpg', 'jpg', '', 155, 'nzj2vgi7lrdzauaode0g', 0, NULL, '2022-01-08 03:48:46', '2022-01-08 03:48:46', 0),
(285, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-092129-avatar-img.jpg', 'jpg', '', 155, 'sdbuqncxfgqnflbifiwq', 0, NULL, '2022-01-08 03:51:29', '2022-01-08 03:51:29', 0),
(286, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-092248-avatar-img.jpg', 'jpg', '', 155, 'wy1iwxdhi4y3atc6z7xp', 0, NULL, '2022-01-08 03:52:48', '2022-01-08 03:52:48', 0),
(287, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-092358-avatar-img.jpg', 'jpg', '', 155, 'j8ozju37pifqjlpsgdhx', 0, NULL, '2022-01-08 03:53:58', '2022-01-08 03:53:58', 0),
(288, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-092502-ev-img01.jpg', 'jpg', '', 155, 'izfjm1n7bdc9pkpvx6lg', 0, NULL, '2022-01-08 03:55:02', '2022-01-08 03:55:02', 0),
(289, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-092507-ev-img02.jpg', 'jpg', '', 155, '5vmhe3elsjeiqfvmcibi', 0, NULL, '2022-01-08 03:55:07', '2022-01-08 03:55:07', 0),
(290, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-092513-ev-img01.jpg', 'jpg', '', 155, 'jivbg4sq5i1zlgtryrur', 0, NULL, '2022-01-08 03:55:13', '2022-01-08 03:55:13', 0),
(291, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-092540-ev-img03.jpg', 'jpg', '', 155, 'wewummvsinoeh7s40vt8', 0, NULL, '2022-01-08 03:55:40', '2022-01-08 03:55:40', 0),
(292, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-092546-ev-img02.jpg', 'jpg', '', 155, 'oouwzk4ihuiiuk9lm3gf', 0, NULL, '2022-01-08 03:55:46', '2022-01-08 03:55:46', 0),
(293, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-092650-A Sample PDF.pdf', 'pdf', '', 155, 'ukkbabfrpfmoqynurthj', 0, NULL, '2022-01-08 03:56:50', '2022-01-08 03:56:50', 0),
(294, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-092739-A Sample PDF.pdf', 'pdf', '', 155, 'fztf0m5rsjwwkyrbxkig', 0, NULL, '2022-01-08 03:57:39', '2022-01-08 03:57:39', 0),
(295, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-100358-A Sample PDF.pdf', 'pdf', '', 155, '3lelv1cg37o2bzgnlpqw', 0, NULL, '2022-01-08 04:33:58', '2022-01-08 04:33:58', 0),
(296, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-100735-A Sample PDF.pdf', 'pdf', '', 155, 'i4zbvvq3mhvaplpklom1', 0, NULL, '2022-01-08 04:37:35', '2022-01-08 04:37:35', 0),
(297, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-100745-A Sample PDF.pdf', 'pdf', '', 155, 's9dz9p0lzdrli0mgccxd', 0, NULL, '2022-01-08 04:37:45', '2022-01-08 04:37:45', 0),
(298, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-104406-A Sample PDF.pdf', 'pdf', '', 155, 'gwbnevn6wckam12xibax', 0, NULL, '2022-01-08 05:14:06', '2022-01-08 05:14:06', 0),
(299, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-114326-avatar-img.jpg', 'jpg', '', 178, 'bpdgek2rrpxo9lj20pdk', 0, NULL, '2022-01-08 06:13:26', '2022-01-08 06:13:26', 0),
(300, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-114431-avatar-img.jpg', 'jpg', '', 178, 'pdomrcyo6nb8ofysefm7', 0, NULL, '2022-01-08 06:14:31', '2022-01-08 06:14:31', 0),
(301, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-115138-avatar-img.jpg', 'jpg', '', 177, 'bwzurss17qsvcowfojgw', 0, NULL, '2022-01-08 06:21:38', '2022-01-08 06:21:38', 0),
(302, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-122526-avatar-img.jpg', 'jpg', '', 175, 'v9ux5rpwr3bpwqsk0vpe', 0, NULL, '2022-01-08 06:55:26', '2022-01-08 06:55:26', 0),
(303, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-08-122708-avatar-img.jpg', 'jpg', '', 175, 'tp3q3thc0gep8avvi2ol', 0, NULL, '2022-01-08 06:57:08', '2022-01-08 06:57:08', 0),
(304, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-061432-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'osai9dcm7uixj0c2pncu', 0, NULL, '2022-01-11 00:44:32', '2022-01-11 00:44:32', 0),
(305, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-061442-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'rrplg8coyri32lvyyqgs', 0, NULL, '2022-01-11 00:44:42', '2022-01-11 00:44:42', 0),
(306, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-061449-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'yqcddmjlxglq81qz3zzn', 0, NULL, '2022-01-11 00:44:49', '2022-01-11 00:44:49', 0),
(307, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-061518-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'frrezpftwa8yn17x6j2d', 0, NULL, '2022-01-11 00:45:18', '2022-01-11 00:45:18', 0),
(308, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-061916-A Sample PDF.pdf', 'pdf', '', 155, 'sjsx2zmxshkx53kbbspq', 0, NULL, '2022-01-11 00:49:16', '2022-01-11 00:49:16', 0),
(309, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-062022-A Sample PDF.pdf', 'pdf', '', 155, 'lop24muwn3sz34sficwd', 0, NULL, '2022-01-11 00:50:22', '2022-01-11 00:50:22', 0),
(310, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-062053-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'vq32srmq84ln9npc4ycc', 0, NULL, '2022-01-11 00:50:53', '2022-01-11 00:50:53', 0),
(311, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-072313-avatar-img.jpg', 'jpg', '', 155, '1bsusmqldtbfe60akrp5', 0, NULL, '2022-01-11 01:53:13', '2022-01-11 01:53:13', 0),
(312, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-073535-avatar-img.jpg', 'jpg', '', 155, 'tuieogmvjerkivy7dmg2', 0, NULL, '2022-01-11 02:05:35', '2022-01-11 02:05:35', 0),
(313, 'image_addphoto.png', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-075722-image_addphoto.png', 'png', '', 155, 'jnksdhhlhbhxwivlfed7', 0, NULL, '2022-01-11 02:27:22', '2022-01-11 02:27:22', 0),
(314, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-075737-avatar-img.jpg', 'jpg', '', 155, 'a3n9pubj6ktu3sxasflo', 0, NULL, '2022-01-11 02:27:37', '2022-01-11 02:27:37', 0),
(315, 'bg-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-075823-bg-img.jpg', 'jpg', '', 155, 'rxoaubkmwtimnxrgqqxh', 0, NULL, '2022-01-11 02:28:23', '2022-01-11 02:28:23', 0),
(316, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-114138-avatar-img.jpg', 'jpg', '', 155, '2fe3chxslpt0ljofmvmu', 0, NULL, '2022-01-11 06:11:38', '2022-01-11 06:11:38', 0),
(317, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-114145-ev-img01.jpg', 'jpg', '', 155, 'rqtbgg5ianwh7zon4abp', 0, NULL, '2022-01-11 06:11:45', '2022-01-11 06:11:45', 0),
(318, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-114151-ev-img02.jpg', 'jpg', '', 155, 'vboq2xvxejqjqov8yswy', 0, NULL, '2022-01-11 06:11:51', '2022-01-11 06:11:51', 0),
(319, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-115416-avatar-img.jpg', 'jpg', '', 155, 'iw9uq5xs2tfdsqr2qzxz', 0, NULL, '2022-01-11 06:24:16', '2022-01-11 06:24:16', 0),
(320, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-115421-ev-img01.jpg', 'jpg', '', 155, 'tfwdilzkzjlrh18ufvwi', 0, NULL, '2022-01-11 06:24:21', '2022-01-11 06:24:21', 0),
(321, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-115426-ev-img02.jpg', 'jpg', '', 155, 'dtgdw4gvf858kwdiwtds', 0, NULL, '2022-01-11 06:24:26', '2022-01-11 06:24:26', 0),
(322, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-115625-avatar-img.jpg', 'jpg', '', 155, 'ek6oaazbf3fqn49skpev', 0, NULL, '2022-01-11 06:26:25', '2022-01-11 06:26:25', 0),
(323, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-115631-ev-img01.jpg', 'jpg', '', 155, '6nmum2vl38hf0uftgrzn', 0, NULL, '2022-01-11 06:26:31', '2022-01-11 06:26:31', 0),
(324, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-115636-ev-img02.jpg', 'jpg', '', 155, 'l7cgk88nsbyqyfg608b9', 0, NULL, '2022-01-11 06:26:36', '2022-01-11 06:26:36', 0),
(325, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-120320-avatar-img.jpg', 'jpg', '', 155, 'wk3nhuelo6l1lefrfiyt', 0, NULL, '2022-01-11 06:33:20', '2022-01-11 06:33:20', 0),
(326, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-120421-ev-img01.jpg', 'jpg', '', 155, 'cpboxyhr3d4swmezjl0v', 0, NULL, '2022-01-11 06:34:21', '2022-01-11 06:34:21', 0),
(327, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-120443-ev-img02.jpg', 'jpg', '', 155, 'qypb7pufdn9gae7duoky', 0, NULL, '2022-01-11 06:34:43', '2022-01-11 06:34:43', 0),
(328, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-120450-ev-img03.jpg', 'jpg', '', 155, 'oxkq5eyzvr6wqzokzia5', 0, NULL, '2022-01-11 06:34:50', '2022-01-11 06:34:50', 0),
(329, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-120645-avatar-img.jpg', 'jpg', '', 155, 'crczdp9ghfsy6fcbplbg', 0, NULL, '2022-01-11 06:36:45', '2022-01-11 06:36:45', 0),
(330, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-120737-ev-img01.jpg', 'jpg', '', 155, 'ynha8sasodtdaa9yukoo', 0, NULL, '2022-01-11 06:37:37', '2022-01-11 06:37:37', 0),
(331, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-120800-ev-img02.jpg', 'jpg', '', 155, 'lnbfa93lt5zl9fcbauay', 0, NULL, '2022-01-11 06:38:00', '2022-01-11 06:38:00', 0),
(332, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-11-120806-ev-img03.jpg', 'jpg', '', 155, '9thprvxncakxrxqiqsz7', 0, NULL, '2022-01-11 06:38:06', '2022-01-11 06:38:06', 0),
(333, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-081418-avatar-img.jpg', 'jpg', '', 155, 'zgr5do5r2gt4prcxbfm9', 0, NULL, '2022-01-12 02:44:18', '2022-01-12 02:44:18', 0),
(334, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-081426-ev-img01.jpg', 'jpg', '', 155, '9fprmlkqitm4nrclcyov', 0, NULL, '2022-01-12 02:44:26', '2022-01-12 02:44:26', 0),
(335, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-081430-ev-img02.jpg', 'jpg', '', 155, 'ipxv4zketknhozvcxnjy', 0, NULL, '2022-01-12 02:44:30', '2022-01-12 02:44:30', 0),
(336, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-105448-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'zo07coy7gftwoiyhthgb', 0, NULL, '2022-01-12 05:24:48', '2022-01-12 05:24:49', 0),
(337, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-105534-A Sample PDF.pdf', 'pdf', '', 155, 'ofb10mxmaqer51ualj5m', 0, NULL, '2022-01-12 05:25:34', '2022-01-12 05:25:34', 0),
(338, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-105629-A Sample PDF.pdf', 'pdf', '', 155, 'fspgpgkhketd2adirs2o', 0, NULL, '2022-01-12 05:26:29', '2022-01-12 05:26:29', 0),
(339, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-105730-A Sample PDF.pdf', 'pdf', '', 155, 'p6xjks9oxdhk1fxoiyxg', 0, NULL, '2022-01-12 05:27:30', '2022-01-12 05:27:30', 0),
(340, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-105805-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'ce1mamcklsqmfvu15f72', 0, NULL, '2022-01-12 05:28:05', '2022-01-12 05:28:05', 0),
(341, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-110123-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'tpqt4hqrr1job6hxzt01', 0, NULL, '2022-01-12 05:31:23', '2022-01-12 05:31:23', 0),
(342, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-110231-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'hpcad9d6yeczsax5kk9h', 0, NULL, '2022-01-12 05:32:31', '2022-01-12 05:32:31', 0),
(343, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-110248-A Sample PDF.pdf', 'pdf', '', 155, 'i6qctgdnlva0ecdtxxfy', 0, NULL, '2022-01-12 05:32:48', '2022-01-12 05:32:48', 0),
(344, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-110317-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'sevqq424rihkcmai6auo', 0, NULL, '2022-01-12 05:33:17', '2022-01-12 05:33:17', 0),
(345, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-111354-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, '6qdngncsifhl5ksqz8ys', 0, NULL, '2022-01-12 05:43:54', '2022-01-12 05:43:54', 0),
(346, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-111417-A Sample PDF.pdf', 'pdf', '', 155, 'gozgg5noqvykejykwx5j', 0, NULL, '2022-01-12 05:44:17', '2022-01-12 05:44:17', 0),
(347, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-111545-A Sample PDF.pdf', 'pdf', '', 155, 'onu0vfb8hzse3zivgbth', 0, NULL, '2022-01-12 05:45:45', '2022-01-12 05:45:45', 0),
(348, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-112029-A Sample PDF.pdf', 'pdf', '', 155, 'lvfisbqhxkzhaxauivsb', 0, NULL, '2022-01-12 05:50:29', '2022-01-12 05:50:29', 0),
(349, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-112101-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'jbmvouyvlwyyeypebnlg', 0, NULL, '2022-01-12 05:51:01', '2022-01-12 05:51:01', 0),
(350, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-112123-A Sample PDF.pdf', 'pdf', '', 155, 'xpojxh3f3u94pfkwh4tn', 0, NULL, '2022-01-12 05:51:23', '2022-01-12 05:51:23', 0),
(351, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-112130-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'wznwtukoyviktp6amjz4', 0, NULL, '2022-01-12 05:51:30', '2022-01-12 05:51:30', 0),
(352, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-112900-ev-img01.jpg', 'jpg', '', 155, 'syjxrgvyyuzzlyygxhgc', 0, NULL, '2022-01-12 05:59:00', '2022-01-12 05:59:00', 0),
(353, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-112905-ev-img02.jpg', 'jpg', '', 155, 'm4uls5bhwhhgtbzt9tig', 0, NULL, '2022-01-12 05:59:05', '2022-01-12 05:59:05', 0),
(354, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-113738-A Sample PDF.pdf', 'pdf', '', 155, 'kak1nxpkujjpko0rmyi8', 0, NULL, '2022-01-12 06:07:38', '2022-01-12 06:07:38', 0),
(355, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-114018-A Sample PDF.pdf', 'pdf', '', 155, 'mpm8ig6v2lwj5dyvkysa', 0, NULL, '2022-01-12 06:10:18', '2022-01-12 06:10:18', 0),
(356, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-114030-A Sample PDF.pdf', 'pdf', '', 155, 'zmevkuusqbiq43odqgfy', 0, NULL, '2022-01-12 06:10:30', '2022-01-12 06:10:30', 0),
(357, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-114035-A Sample PDF.pdf', 'pdf', '', 155, '4ifp1bf5eaynesnmchfb', 0, NULL, '2022-01-12 06:10:35', '2022-01-12 06:10:35', 0),
(358, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-114048-A Sample PDF.pdf', 'pdf', '', 155, 'evzlfggn6ligogjegyhw', 0, NULL, '2022-01-12 06:10:48', '2022-01-12 06:10:48', 0),
(359, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-114058-A Sample PDF.pdf', 'pdf', '', 155, '1t4uqhqldp9ghyky4bvt', 0, NULL, '2022-01-12 06:10:58', '2022-01-12 06:10:58', 0),
(360, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-114107-A Sample PDF.pdf', 'pdf', '', 155, 'xjp6sqzm7osii5fds7a8', 0, NULL, '2022-01-12 06:11:07', '2022-01-12 06:11:07', 0),
(361, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-114133-A Sample PDF.pdf', 'pdf', '', 155, 'mqc22fb6dzlbhmyxmwzs', 0, NULL, '2022-01-12 06:11:33', '2022-01-12 06:11:33', 0),
(362, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-114143-A Sample PDF.pdf', 'pdf', '', 155, 'oifzlmfnnrwi8q3yhhec', 0, NULL, '2022-01-12 06:11:43', '2022-01-12 06:11:43', 0),
(363, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-114150-A Sample PDF.pdf', 'pdf', '', 155, 'zc2yc6ydyvohlyzllerw', 0, NULL, '2022-01-12 06:11:50', '2022-01-12 06:11:50', 0),
(364, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-12-114244-A Sample PDF.pdf', 'pdf', '', 155, 'unwjapathsnojrjeh34j', 0, NULL, '2022-01-12 06:12:44', '2022-01-12 06:12:44', 0),
(365, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-055552-A Sample PDF.pdf', 'pdf', '', 155, 'khtoylygpudooiwlrr1i', 0, NULL, '2022-01-13 00:25:52', '2022-01-13 00:25:52', 0),
(366, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-065603-A Sample PDF.pdf', 'pdf', '', 155, 'xyialq2uhwesrhnisfmf', 0, NULL, '2022-01-13 01:26:03', '2022-01-13 01:26:03', 0),
(367, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-071917-A Sample PDF.pdf', 'pdf', '', 155, 'y2f6ho7tudkq7sjrnv3s', 0, NULL, '2022-01-13 01:49:17', '2022-01-13 01:49:17', 0),
(368, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-071931-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'zlxkoouqibipppupl64w', 0, NULL, '2022-01-13 01:49:31', '2022-01-13 01:49:31', 0),
(369, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-071953-A Sample PDF.pdf', 'pdf', '', 155, 'rwld0oqyprnmou07vull', 0, NULL, '2022-01-13 01:49:53', '2022-01-13 01:49:53', 0),
(370, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-072003-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'nafvmwxp7jmf605tdivi', 0, NULL, '2022-01-13 01:50:03', '2022-01-13 01:50:03', 0),
(371, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-072043-A Sample PDF.pdf', 'pdf', '', 155, 'eprcl6wfgzflrh7soqvm', 0, NULL, '2022-01-13 01:50:43', '2022-01-13 01:50:43', 0),
(372, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-072050-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'zdzxmgoizdryzmv5q0sx', 0, NULL, '2022-01-13 01:50:50', '2022-01-13 01:50:50', 0),
(373, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-072111-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'csol46o3xv351gbohw2l', 0, NULL, '2022-01-13 01:51:11', '2022-01-13 01:51:11', 0),
(374, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-072220-A Sample PDF.pdf', 'pdf', '', 155, 'fa9hzndyb70qg6tnwbun', 0, NULL, '2022-01-13 01:52:20', '2022-01-13 01:52:20', 0),
(375, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-072240-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, '6u2hk44cdwgfzfars3yt', 0, NULL, '2022-01-13 01:52:40', '2022-01-13 01:52:40', 0),
(376, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-075430-A Sample PDF.pdf', 'pdf', '', 155, 'ybdvpvdhmts1cgzzyoco', 0, NULL, '2022-01-13 02:24:30', '2022-01-13 02:24:30', 0),
(377, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-075436-A Sample PDF.pdf', 'pdf', '', 155, 'o5bjgtnycdia5pnrcnlt', 0, NULL, '2022-01-13 02:24:36', '2022-01-13 02:24:36', 0),
(378, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-075454-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'xyiio7vhwrh8d2f5z9mi', 0, NULL, '2022-01-13 02:24:54', '2022-01-13 02:24:54', 0),
(379, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-075500-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'jewztpj5tnalhn9ybvtf', 0, NULL, '2022-01-13 02:25:00', '2022-01-13 02:25:00', 0),
(380, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-075601-A Sample PDF.pdf', 'pdf', '', 155, 'phdi1oy4r8bpexavjk3c', 0, NULL, '2022-01-13 02:26:01', '2022-01-13 02:26:01', 0),
(381, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-075608-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'tf3crwiiudquc66wuexl', 0, NULL, '2022-01-13 02:26:08', '2022-01-13 02:26:08', 0),
(382, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-075707-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, '1htfplfrqjvk3xw4qwkq', 0, NULL, '2022-01-13 02:27:07', '2022-01-13 02:27:07', 0),
(383, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-075716-A Sample PDF.pdf', 'pdf', '', 155, 'hjprg8fapfrdhwlbdmuy', 0, NULL, '2022-01-13 02:27:16', '2022-01-13 02:27:16', 0),
(384, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-081227-A Sample PDF.pdf', 'pdf', '', 155, 's2rdvenzss1pgvzftrvv', 0, NULL, '2022-01-13 02:42:27', '2022-01-13 02:42:27', 0),
(385, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-094948-A Sample PDF.pdf', 'pdf', '', 155, 'f2bop640fk4pqrkq1gko', 0, NULL, '2022-01-13 04:19:48', '2022-01-13 04:19:48', 0),
(386, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-095136-A Sample PDF.pdf', 'pdf', '', 155, 'oumv62rsdydm5db5tmxf', 0, NULL, '2022-01-13 04:21:36', '2022-01-13 04:21:36', 0),
(387, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-095149-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'lf4ow5r27muoi4ohryc3', 0, NULL, '2022-01-13 04:21:49', '2022-01-13 04:21:49', 0),
(388, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-102301-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'mvyhoxwqshgky7yubxvl', 0, NULL, '2022-01-13 04:53:01', '2022-01-13 04:53:01', 0),
(389, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-102325-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'y6r87dhvrukdogtx4bcz', 0, NULL, '2022-01-13 04:53:25', '2022-01-13 04:53:25', 0),
(390, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-103159-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'jotm1gwhy8govgpprizk', 0, NULL, '2022-01-13 05:01:59', '2022-01-13 05:01:59', 0),
(391, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-124157-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'wihvckndfqrzxwpounza', 0, NULL, '2022-01-13 07:11:57', '2022-01-13 07:11:57', 0),
(392, 'evt2.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-124210-evt2.jpg', 'jpg', '', 155, '2eeq2buxszdvub3abdlv', 0, NULL, '2022-01-13 07:12:10', '2022-01-13 07:12:10', 0),
(393, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-130056-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, '2jc5bdth043n1aimax1l', 0, NULL, '2022-01-13 07:30:56', '2022-01-13 07:30:56', 0),
(394, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-132259-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'vpbh7c0tkf6o8zxpv8uj', 0, NULL, '2022-01-13 07:52:59', '2022-01-13 07:52:59', 0),
(395, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-132412-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, '33uwjvbwpil18jo8urio', 0, NULL, '2022-01-13 07:54:12', '2022-01-13 07:54:12', 0),
(396, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-133324-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'x0vewfv7chzp2kbv97ya', 0, NULL, '2022-01-13 08:03:24', '2022-01-13 08:03:24', 0),
(397, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-133344-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'u8dfsraa0pqvbdzuki12', 0, NULL, '2022-01-13 08:03:44', '2022-01-13 08:03:44', 0),
(398, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-133357-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, '8rpq6fwv0avl1qzfdhw7', 0, NULL, '2022-01-13 08:03:57', '2022-01-13 08:03:57', 0),
(399, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-134518-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'trwagvah3lq24ayly12g', 0, NULL, '2022-01-13 08:15:18', '2022-01-13 08:15:18', 0),
(400, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-134654-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'jul36zwacnlusc6onukg', 0, NULL, '2022-01-13 08:16:54', '2022-01-13 08:16:54', 0),
(401, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-135135-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'fkzog5iwr0xyjskcwcsx', 0, NULL, '2022-01-13 08:21:35', '2022-01-13 08:21:35', 0),
(402, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-135234-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'ymvgwifjii4gihoyc0yo', 0, NULL, '2022-01-13 08:22:34', '2022-01-13 08:22:34', 0),
(403, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-135644-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'mpz5svbo1ph5krlijczw', 0, NULL, '2022-01-13 08:26:44', '2022-01-13 08:26:44', 0),
(404, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-135833-A Sample PDF.pdf', 'pdf', '', 155, 'ta1llvj6nfyxqruvht19', 0, NULL, '2022-01-13 08:28:33', '2022-01-13 08:28:33', 0),
(405, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-135939-A Sample PDF.pdf', 'pdf', '', 155, 'xm2qz78rifxzbejlyyrf', 0, NULL, '2022-01-13 08:29:39', '2022-01-13 08:29:39', 0),
(406, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-140001-A Sample PDF.pdf', 'pdf', '', 155, 'to5p5gfbpqtarhbijtau', 0, NULL, '2022-01-13 08:30:01', '2022-01-13 08:30:01', 0),
(407, 'Get_Started_With_Smallpdf.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-13-140011-Get_Started_With_Smallpdf.pdf', 'pdf', '', 155, 'iiearsbx76hc0wbpcjj3', 0, NULL, '2022-01-13 08:30:11', '2022-01-13 08:30:11', 0),
(408, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-01-19-132806-avatar-img.jpg', 'jpg', '', 155, 'jf4ljavldhxyj6ln1fnb', 0, NULL, '2022-01-19 07:58:06', '2022-01-19 07:58:06', 0),
(409, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-05-055311-avatar-img.jpg', 'jpg', '', 155, 'mcfek68lqopynlm1wvwu', 0, NULL, '2022-05-05 00:23:11', '2022-05-05 00:23:11', 0),
(410, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-05-105328-avatar-img.jpg', 'jpg', '', 155, '80d9nzi2g00iwug1ypse', 0, NULL, '2022-05-05 05:23:28', '2022-05-05 05:23:28', 0),
(411, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-06-060342-avatar-img.jpg', 'jpg', '', 155, 'n6tggshbkgov0gn7m07v', 0, NULL, '2022-05-06 00:33:42', '2022-05-06 00:33:42', 0),
(412, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-06-065714-ev-img01.jpg', 'jpg', '', 155, 'robm1idx4pxj6rkr906h', 0, NULL, '2022-05-06 01:27:14', '2022-05-06 01:27:14', 0),
(413, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-06-073329-ev-img02.jpg', 'jpg', '', 155, 'u9hi9rtzixthwsdmh5la', 0, NULL, '2022-05-06 02:03:29', '2022-05-06 02:03:29', 0),
(414, 'flower-img_350x350.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-06-075428-flower-img_350x350.jpg', 'jpg', '', 155, '9o8tvto2idc1ptpg0fmz', 0, NULL, '2022-05-06 02:24:28', '2022-05-06 02:24:28', 0),
(415, 'ev-img03_1_540x300.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-06-105944-ev-img03_1_540x300.jpg', 'jpg', '', 155, 'nhvcqyed4c4hou7pcush', 0, NULL, '2022-05-06 05:29:44', '2022-05-06 05:29:44', 0),
(416, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-07-065416-avatar-img.jpg', 'jpg', '', 155, 'fabckyvkynfj0bs0w6xe', 0, NULL, '2022-05-07 01:24:16', '2022-05-07 01:24:16', 0),
(417, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-07-065903-avatar-img.jpg', 'jpg', '', 155, '9ldhc38eznkrlkuu6kcc', 0, NULL, '2022-05-07 01:29:03', '2022-05-07 01:29:03', 0),
(418, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-07-095050-avatar-img.jpg', 'jpg', '', 155, '18ebi8defqolvzmlacax', 0, NULL, '2022-05-07 04:20:50', '2022-05-07 04:20:50', 0),
(419, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-07-102055-avatar-img.jpg', 'jpg', '', 155, 'gihnbxvpe0j2v6mdrcjv', 0, NULL, '2022-05-07 04:50:55', '2022-05-07 04:50:55', 0),
(420, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-07-113941-avatar-img.jpg', 'jpg', '', 155, 'x0yfn0lt6mikngaikit9', 0, NULL, '2022-05-07 06:09:41', '2022-05-07 06:09:41', 0),
(421, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-12-134512-avatar-img.jpg', 'jpg', '', 155, 'mpm22uty8k0rr3sfo3ce', 0, NULL, '2022-05-12 08:15:12', '2022-05-12 08:15:12', 0),
(422, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-12-134746-avatar-img.jpg', 'jpg', '', 155, '7lfzmy562aazghj72yc6', 0, NULL, '2022-05-12 08:17:46', '2022-05-12 08:17:46', 0),
(423, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-14-052730-avatar-img.jpg', 'jpg', '', 155, 'luxdpgfwiwtc14wjlmxa', 0, NULL, '2022-05-13 23:57:30', '2022-05-13 23:57:30', 0),
(424, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-14-052742-ev-img01.jpg', 'jpg', '', 155, 'iljapukohcmjhqfn2u1d', 0, NULL, '2022-05-13 23:57:42', '2022-05-13 23:57:42', 0),
(425, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-14-052748-ev-img02.jpg', 'jpg', '', 155, 'ffx7mxy2an0zwzzc4nwy', 0, NULL, '2022-05-13 23:57:48', '2022-05-13 23:57:48', 0),
(426, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-17-082722-A Sample PDF.pdf', 'pdf', '', 155, 'ntj5jqhgopwjm0bac3ks', 0, NULL, '2022-05-17 02:57:22', '2022-05-17 02:57:22', 0),
(427, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-17-082829-A Sample PDF.pdf', 'pdf', '', 155, 'ygy0xzq35t2jcwn8978y', 0, NULL, '2022-05-17 02:58:29', '2022-05-17 02:58:29', 0),
(428, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-17-082859-A Sample PDF.pdf', 'pdf', '', 155, 'h3kxhm2smdwk3g1utjlr', 0, NULL, '2022-05-17 02:58:59', '2022-05-17 02:58:59', 0),
(429, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-17-083807-A Sample PDF.pdf', 'pdf', '', 155, 'r28x2ojgtkgcoekl6fog', 0, NULL, '2022-05-17 03:08:07', '2022-05-17 03:08:07', 0),
(430, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-17-083848-A Sample PDF.pdf', 'pdf', '', 155, '94ppv4savlqrbtwwlj9v', 0, NULL, '2022-05-17 03:08:48', '2022-05-17 03:08:48', 0),
(431, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-17-083907-A Sample PDF.pdf', 'pdf', '', 155, 'atdvlvhd2mwqyfs1noie', 0, NULL, '2022-05-17 03:09:07', '2022-05-17 03:09:07', 0),
(432, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-17-084147-A Sample PDF.pdf', 'pdf', '', 155, 'ee2b3mgvxndmvw7awcg5', 0, NULL, '2022-05-17 03:11:47', '2022-05-17 03:11:47', 0),
(433, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-17-084219-A Sample PDF.pdf', 'pdf', '', 155, '4fwtel33gs4qsvtnba41', 0, NULL, '2022-05-17 03:12:19', '2022-05-17 03:12:19', 0),
(434, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-17-084231-A Sample PDF.pdf', 'pdf', '', 155, 'vj45yeqmx0va8mzfdckn', 0, NULL, '2022-05-17 03:12:31', '2022-05-17 03:12:31', 0),
(435, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-17-102454-A Sample PDF.pdf', 'pdf', '', 155, 'dkyx0ozg7csyv4l9eyj7', 0, NULL, '2022-05-17 04:54:54', '2022-05-17 04:54:54', 0),
(436, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-17-103415-A Sample PDF.pdf', 'pdf', '', 155, 'utce1bpasw6vkv0i1epu', 0, NULL, '2022-05-17 05:04:15', '2022-05-17 05:04:15', 0),
(437, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-17-103525-A Sample PDF.pdf', 'pdf', '', 155, 'd6tu5jfqspczrnq1dqc1', 0, NULL, '2022-05-17 05:05:25', '2022-05-17 05:05:25', 0),
(438, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-17-103817-A Sample PDF.pdf', 'pdf', '', 155, 'gms2nf5tc9ctxjcrnarb', 0, NULL, '2022-05-17 05:08:17', '2022-05-17 05:08:17', 0),
(439, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-17-103845-A Sample PDF.pdf', 'pdf', '', 155, 'l4b27sk00srfhqp6ijg1', 0, NULL, '2022-05-17 05:08:45', '2022-05-17 05:08:45', 0),
(440, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-17-104249-A Sample PDF.pdf', 'pdf', '', 155, 'muflxe3isnbwtihze0zf', 0, NULL, '2022-05-17 05:12:49', '2022-05-17 05:12:49', 0),
(441, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-17-104317-A Sample PDF.pdf', 'pdf', '', 155, 'lwcc9vp7s9qyseutvp8x', 0, NULL, '2022-05-17 05:13:17', '2022-05-17 05:13:17', 0),
(442, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-17-104924-A Sample PDF.pdf', 'pdf', '', 155, 'hork4kb6srqwzljrwsic', 0, NULL, '2022-05-17 05:19:24', '2022-05-17 05:19:24', 0),
(443, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-17-104933-A Sample PDF.pdf', 'pdf', '', 155, 'vpbpawh2bmkonhciv8xl', 0, NULL, '2022-05-17 05:19:33', '2022-05-17 05:19:33', 0),
(444, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-17-104941-A Sample PDF.pdf', 'pdf', '', 155, '2z28gm7xpz4tdtsqehal', 0, NULL, '2022-05-17 05:19:41', '2022-05-17 05:19:41', 0),
(445, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-18-075758-avatar-img.jpg', 'jpg', '', 155, '4si0bztcdify51jsuheu', 0, NULL, '2022-05-18 02:27:58', '2022-05-18 02:27:58', 0),
(446, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-18-075806-ev-img01.jpg', 'jpg', '', 155, 'r5p55ar4tyinvbbhfy5t', 0, NULL, '2022-05-18 02:28:06', '2022-05-18 02:28:06', 0),
(447, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-18-075811-ev-img02.jpg', 'jpg', '', 155, '9dulpm0xncmr81sqteam', 0, NULL, '2022-05-18 02:28:11', '2022-05-18 02:28:11', 0),
(448, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-18-080003-A Sample PDF.pdf', 'pdf', '', 155, '7fyqol2kodunps8yaj9t', 0, NULL, '2022-05-18 02:30:03', '2022-05-18 02:30:03', 0),
(449, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-18-090734-A Sample PDF.pdf', 'pdf', '', 155, 'hhw9uegijpqv17mtlcsj', 0, NULL, '2022-05-18 03:37:34', '2022-05-18 03:37:34', 0),
(450, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-18-102000-A Sample PDF.pdf', 'pdf', '', 155, 'r5jvmqlpdtw5yg3e1zmw', 0, NULL, '2022-05-18 04:50:00', '2022-05-18 04:50:00', 0),
(451, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-18-102032-ev-img01.jpg', 'jpg', '', 155, 'yqcg5fczdm81wirkh97q', 0, NULL, '2022-05-18 04:50:32', '2022-05-18 04:50:32', 0),
(452, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-19-062213-ev-img01.jpg', 'jpg', '', 155, 'i4ia8mshig7hi01pk6zl', 0, NULL, '2022-05-19 00:52:13', '2022-05-19 00:52:13', 0),
(453, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-19-062447-ev-img02.jpg', 'jpg', '', 155, '26kacyxaaecixzjdvjoy', 0, NULL, '2022-05-19 00:54:47', '2022-05-19 00:54:47', 0),
(454, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-23-045056-A Sample PDF.pdf', 'pdf', '', 155, 'bdw3wziusmgrunl6c1zk', 0, NULL, '2022-05-22 23:20:56', '2022-05-22 23:20:56', 0),
(455, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-23-045117-A Sample PDF.pdf', 'pdf', '', 155, 'hrza7alobcte3d9r88w1', 0, NULL, '2022-05-22 23:21:17', '2022-05-22 23:21:17', 0),
(456, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-23-045124-A Sample PDF.pdf', 'pdf', '', 155, 'gkva0ktgkds2hqbxzvkn', 0, NULL, '2022-05-22 23:21:24', '2022-05-22 23:21:24', 0),
(457, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-23-045203-A Sample PDF.pdf', 'pdf', '', 155, 'dbwybec1krweujgia3m1', 0, NULL, '2022-05-22 23:22:03', '2022-05-22 23:22:03', 0),
(458, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-23-045209-A Sample PDF.pdf', 'pdf', '', 155, 'gybq9lqcekyneh4eqwn8', 0, NULL, '2022-05-22 23:22:09', '2022-05-22 23:22:09', 0),
(459, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-23-045233-A Sample PDF.pdf', 'pdf', '', 155, '6wfu6j8swa0qtqbb4ljo', 0, NULL, '2022-05-22 23:22:33', '2022-05-22 23:22:33', 0),
(460, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-23-073842-A Sample PDF.pdf', 'pdf', '', 155, 'aotkvqyudxhxr0dp53u5', 0, NULL, '2022-05-23 02:08:42', '2022-05-23 02:08:42', 0),
(461, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-23-073901-A Sample PDF.pdf', 'pdf', '', 155, 'lx6meyy4klwavsvq06zy', 0, NULL, '2022-05-23 02:09:01', '2022-05-23 02:09:01', 0),
(462, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-23-073905-A Sample PDF.pdf', 'pdf', '', 155, 'txdkbcrpvuuji3ntier4', 0, NULL, '2022-05-23 02:09:05', '2022-05-23 02:09:05', 0),
(463, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-23-073912-A Sample PDF.pdf', 'pdf', '', 155, 'bkfxvhyapnijvap4zzti', 0, NULL, '2022-05-23 02:09:12', '2022-05-23 02:09:12', 0),
(464, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-23-073933-A Sample PDF.pdf', 'pdf', '', 155, 'ps3rkfjxnlgv51onqerw', 0, NULL, '2022-05-23 02:09:33', '2022-05-23 02:09:33', 0),
(465, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-23-073938-A Sample PDF.pdf', 'pdf', '', 155, 'yf0zlktahftnzyq9jpuw', 0, NULL, '2022-05-23 02:09:38', '2022-05-23 02:09:38', 0),
(466, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-23-074001-A Sample PDF.pdf', 'pdf', '', 155, 'i8bq1deso84zndnjrgms', 0, NULL, '2022-05-23 02:10:01', '2022-05-23 02:10:01', 0),
(467, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-25-134328-avatar-img.jpg', 'jpg', '', 175, 'qalqj5bbjpvrlbtktc4f', 0, NULL, '2022-05-25 08:13:28', '2022-05-25 08:13:28', 0),
(468, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-25-134347-ev-img01.jpg', 'jpg', '', 175, 'hhccxnxf94awbzgecm26', 0, NULL, '2022-05-25 08:13:47', '2022-05-25 08:13:47', 0),
(469, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-25-134351-ev-img02.jpg', 'jpg', '', 175, 'cvkbbjlzgld7gvb7suoq', 0, NULL, '2022-05-25 08:13:51', '2022-05-25 08:13:51', 0),
(470, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-25-134717-avatar-img.jpg', 'jpg', '', 178, 've7tbbjojmbiiv3ictua', 0, NULL, '2022-05-25 08:17:17', '2022-05-25 08:17:17', 0),
(471, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-25-134743-ev-img03.jpg', 'jpg', '', 178, 'nww4jnuybm39uxl4l8bk', 0, NULL, '2022-05-25 08:17:43', '2022-05-25 08:17:43', 0),
(472, 'flower-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-25-134747-flower-img.jpg', 'jpg', '', 178, 'mcoxgfsbkgqzcuznbpcu', 0, NULL, '2022-05-25 08:17:47', '2022-05-25 08:17:47', 0),
(473, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-25-140209-avatar-img.jpg', 'jpg', '', 178, 'hedic6a7galawq0qfdms', 0, NULL, '2022-05-25 08:32:09', '2022-05-25 08:32:09', 0),
(474, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-25-140214-ev-img01.jpg', 'jpg', '', 178, 'ndtf5mu6x1ho0wpc0dlp', 0, NULL, '2022-05-25 08:32:14', '2022-05-25 08:32:14', 0),
(475, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-25-140218-ev-img02.jpg', 'jpg', '', 178, 'bol6iob3jewdqlnsyuto', 0, NULL, '2022-05-25 08:32:18', '2022-05-25 08:32:18', 0),
(476, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-25-140757-avatar-img.jpg', 'jpg', '', 178, 'ke18n3h9m2afpahhkntq', 0, NULL, '2022-05-25 08:37:57', '2022-05-25 08:37:57', 0),
(477, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-25-141045-avatar-img.jpg', 'jpg', '', 178, 'paevjvjcef6s3qxjsxhf', 0, NULL, '2022-05-25 08:40:45', '2022-05-25 08:40:45', 0),
(478, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-25-141909-avatar-img.jpg', 'jpg', '', 178, '7bneysjdrl4rmokkb6ys', 0, NULL, '2022-05-25 08:49:09', '2022-05-25 08:49:09', 0),
(479, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-25-142713-avatar-img.jpg', 'jpg', '', 178, 'csydccifzjduxqnngufz', 0, NULL, '2022-05-25 08:57:13', '2022-05-25 08:57:13', 0),
(480, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-125906-A Sample PDF.pdf', 'pdf', '', 155, 'rckvkvuufqblx13xnmlb', 0, NULL, '2022-05-31 07:29:06', '2022-05-31 07:29:06', 0),
(481, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-130004-A Sample PDF.pdf', 'pdf', '', 155, 'afpean05nq51yidxuakd', 0, NULL, '2022-05-31 07:30:04', '2022-05-31 07:30:04', 0),
(482, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-151340-A Sample PDF.pdf', 'pdf', '', 155, 'qi54nxukuo43gzdboge9', 0, NULL, '2022-05-31 09:43:40', '2022-05-31 09:43:40', 0),
(483, 'flower-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-151909-flower-img.jpg', 'jpg', '', 155, '0dtgjpn2tzrsl9k7huep', 0, NULL, '2022-05-31 09:49:09', '2022-05-31 09:49:09', 0),
(484, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-151942-avatar-img.jpg', 'jpg', '', 155, '0yaczhtzqja70dmxilov', 0, NULL, '2022-05-31 09:49:42', '2022-05-31 09:49:42', 0),
(485, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-152001-ev-img03.jpg', 'jpg', '', 155, 'ijnvsjunv4olz5552vyt', 0, NULL, '2022-05-31 09:50:01', '2022-05-31 09:50:01', 0),
(486, 'nature102.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-152023-nature102.jpg', 'jpg', '', 155, 'evl9hyrwlpa7rcvrw3xt', 0, NULL, '2022-05-31 09:50:23', '2022-05-31 09:50:23', 0),
(487, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-152345-A Sample PDF.pdf', 'pdf', '', 155, 'gt7gsno00lwmutkuynq9', 0, NULL, '2022-05-31 09:53:45', '2022-05-31 09:53:45', 0),
(488, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-152428-A Sample PDF.pdf', 'pdf', '', 155, 'rb0ioglgiyrktupesoqx', 0, NULL, '2022-05-31 09:54:28', '2022-05-31 09:54:28', 0),
(489, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-152511-A Sample PDF.pdf', 'pdf', '', 155, 'iwmb9kteih19ltw1cnok', 0, NULL, '2022-05-31 09:55:11', '2022-05-31 09:55:11', 0),
(490, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-152616-avatar-img.jpg', 'jpg', '', 155, '7tq8ioyf07exctbclef9', 0, NULL, '2022-05-31 09:56:16', '2022-05-31 09:56:16', 0),
(491, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-152648-ev-img03.jpg', 'jpg', '', 155, '8jf4rvhmm5klywzxxv5p', 0, NULL, '2022-05-31 09:56:48', '2022-05-31 09:56:48', 0);
INSERT INTO `uploads` (`id`, `name`, `path`, `extension`, `caption`, `user_id`, `hash`, `public`, `deleted_at`, `created_at`, `updated_at`, `media`) VALUES
(492, 'flower-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-152657-flower-img.jpg', 'jpg', '', 155, 'uwxwwbrjzc8olujn7axz', 0, NULL, '2022-05-31 09:56:57', '2022-05-31 09:56:57', 0),
(493, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-152709-ev-img01.jpg', 'jpg', '', 155, 'ri7zfbjplzxu9nwrmwsw', 0, NULL, '2022-05-31 09:57:09', '2022-05-31 09:57:09', 0),
(494, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-152750-ev-img03.jpg', 'jpg', '', 155, 'kvu5l0g4s9fbq5zheoss', 0, NULL, '2022-05-31 09:57:50', '2022-05-31 09:57:50', 0),
(495, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-152814-ev-img01.jpg', 'jpg', '', 155, 'kpeaczpaaqnjp4pqcfcw', 0, NULL, '2022-05-31 09:58:14', '2022-05-31 09:58:14', 0),
(496, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-153439-A Sample PDF.pdf', 'pdf', '', 155, '3bfku7jo3mmdsd99fudi', 0, NULL, '2022-05-31 10:04:39', '2022-05-31 10:04:39', 0),
(497, 'pdf-sample.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-153538-pdf-sample.pdf', 'pdf', '', 155, 'ntcgtqozh5szlgose3gk', 0, NULL, '2022-05-31 10:05:38', '2022-05-31 10:05:38', 0),
(498, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-153627-A Sample PDF.pdf', 'pdf', '', 155, 'cwkm31kyp8i7rukqklyq', 0, NULL, '2022-05-31 10:06:27', '2022-05-31 10:06:27', 0),
(499, 'pdf-sample.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-153645-pdf-sample.pdf', 'pdf', '', 155, '95dshbfcqusweo3kcddw', 0, NULL, '2022-05-31 10:06:45', '2022-05-31 10:06:45', 0),
(500, 'pdf-sample.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-153711-pdf-sample.pdf', 'pdf', '', 155, '44nc75chknmx2u9unbbm', 0, NULL, '2022-05-31 10:07:11', '2022-05-31 10:07:11', 0),
(501, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-153739-A Sample PDF.pdf', 'pdf', '', 155, 'zghgbxnzspk58mqroneh', 0, NULL, '2022-05-31 10:07:39', '2022-05-31 10:07:39', 0),
(502, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-153844-dummy.pdf', 'pdf', '', 155, 'x60qc2aqts7ctjhdt5ka', 0, NULL, '2022-05-31 10:08:44', '2022-05-31 10:08:44', 0),
(503, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-154540-dummy.pdf', 'pdf', '', 155, 'nbhjlbf9p4cqyk3yjk11', 0, NULL, '2022-05-31 10:15:40', '2022-05-31 10:15:40', 0),
(504, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-154931-dummy.pdf', 'pdf', '', 155, 'ljwu8xyhmzdzfac07rym', 0, NULL, '2022-05-31 10:19:31', '2022-05-31 10:19:31', 0),
(505, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-155019-dummy.pdf', 'pdf', '', 155, '4jvhhmevwlfa8cxxlg1a', 0, NULL, '2022-05-31 10:20:19', '2022-05-31 10:20:19', 0),
(506, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-05-31-155039-A Sample PDF.pdf', 'pdf', '', 155, 'abrfhoeoo7cfep00bjit', 0, NULL, '2022-05-31 10:20:39', '2022-05-31 10:20:39', 0),
(507, 'flower-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-050614-flower-img.jpg', 'jpg', '', 155, 'thcdf2gfvyrezqadxbox', 0, NULL, '2022-05-31 23:36:14', '2022-05-31 23:36:14', 0),
(508, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-050635-ev-img01.jpg', 'jpg', '', 155, 'vq29jp896tjgzo2064xq', 0, NULL, '2022-05-31 23:36:35', '2022-05-31 23:36:35', 0),
(509, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-050847-ev-img01.jpg', 'jpg', '', 155, 'z7lg3tpceup2ww0hvzud', 0, NULL, '2022-05-31 23:38:47', '2022-05-31 23:38:47', 0),
(510, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-050856-ev-img02.jpg', 'jpg', '', 155, 'hgcrbpilx4u3e9pxiozo', 0, NULL, '2022-05-31 23:38:56', '2022-05-31 23:38:56', 0),
(511, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-052530-ev-img01.jpg', 'jpg', '', 155, 'nnr20ndruailyhufdc3h', 0, NULL, '2022-05-31 23:55:30', '2022-05-31 23:55:30', 0),
(512, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-052541-ev-img02.jpg', 'jpg', '', 155, 'q5rtrffeog7r16ra2wnx', 0, NULL, '2022-05-31 23:55:41', '2022-05-31 23:55:41', 0),
(513, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-053024-A Sample PDF.pdf', 'pdf', '', 155, '4oy0eyw3ql0ettpz1dhm', 0, NULL, '2022-06-01 00:00:24', '2022-06-01 00:00:24', 0),
(514, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-055055-A Sample PDF.pdf', 'pdf', '', 155, '2vz4vwfx3h80lfycdjtl', 0, NULL, '2022-06-01 00:20:55', '2022-06-01 00:20:55', 0),
(515, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-055106-A Sample PDF.pdf', 'pdf', '', 155, 'dx91vs4cdz2wdgnro9rs', 0, NULL, '2022-06-01 00:21:06', '2022-06-01 00:21:06', 0),
(516, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-055113-A Sample PDF.pdf', 'pdf', '', 155, 'wybk0fhltun3ke2ry0ea', 0, NULL, '2022-06-01 00:21:13', '2022-06-01 00:21:13', 0),
(517, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-055131-dummy.pdf', 'pdf', '', 155, 'a0mxi6cn5hk7mmkvdx98', 0, NULL, '2022-06-01 00:21:31', '2022-06-01 00:21:31', 0),
(518, 'pdf-sample.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-055139-pdf-sample.pdf', 'pdf', '', 155, 'kghr80tppqcr2mllusgu', 0, NULL, '2022-06-01 00:21:39', '2022-06-01 00:21:39', 0),
(519, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-055146-dummy.pdf', 'pdf', '', 155, 'pvuboo6jorhmmk6oom66', 0, NULL, '2022-06-01 00:21:46', '2022-06-01 00:21:46', 0),
(520, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-060220-A Sample PDF.pdf', 'pdf', '', 155, 'gzcldpahjbmekztyvk6k', 0, NULL, '2022-06-01 00:32:20', '2022-06-01 00:32:20', 0),
(521, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-060230-dummy.pdf', 'pdf', '', 155, 'fkb3qtx0gflsvcsms2li', 0, NULL, '2022-06-01 00:32:30', '2022-06-01 00:32:30', 0),
(522, 'pdf-sample.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-060247-pdf-sample.pdf', 'pdf', '', 155, 'h7g7n2ofwychdhx1rtf5', 0, NULL, '2022-06-01 00:32:47', '2022-06-01 00:32:47', 0),
(523, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-061408-dummy.pdf', 'pdf', '', 155, '5rjxlkab9qgz1pivav3x', 0, NULL, '2022-06-01 00:44:08', '2022-06-01 00:44:08', 0),
(524, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-061420-dummy.pdf', 'pdf', '', 155, 'ormavzxypirjfwzqmxfe', 0, NULL, '2022-06-01 00:44:20', '2022-06-01 00:44:20', 0),
(525, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-073054-A Sample PDF.pdf', 'pdf', '', 155, 'mlsgg8ikgzhosngblzgd', 0, NULL, '2022-06-01 02:00:54', '2022-06-01 02:00:54', 0),
(526, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-074420-ev-img01.jpg', 'jpg', '', 155, 'cnsk74svkeeikabuxm1b', 0, NULL, '2022-06-01 02:14:20', '2022-06-01 02:14:20', 0),
(527, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-074757-avatar-img.jpg', 'jpg', '', 155, '68kuvys0yfmfimwxz9ck', 0, NULL, '2022-06-01 02:17:57', '2022-06-01 02:17:57', 0),
(528, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-074819-ev-img01.jpg', 'jpg', '', 155, 'uasdgtncfsnajhw9gg7y', 0, NULL, '2022-06-01 02:18:19', '2022-06-01 02:18:19', 0),
(529, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-074825-ev-img02.jpg', 'jpg', '', 155, '6azwt6fzw3obpunxw0n0', 0, NULL, '2022-06-01 02:18:25', '2022-06-01 02:18:25', 0),
(530, 'flower-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-074839-flower-img.jpg', 'jpg', '', 155, '6thudpnlxbgjqb3lapzq', 0, NULL, '2022-06-01 02:18:39', '2022-06-01 02:18:39', 0),
(531, 'flower-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-081448-flower-img.jpg', 'jpg', '', 155, 'he27oehpu8wfr0nt4o9w', 0, NULL, '2022-06-01 02:44:48', '2022-06-01 02:44:48', 0),
(532, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-083548-ev-img03.jpg', 'jpg', '', 155, 'irxwpe6x1k6cy9cnjvhc', 0, NULL, '2022-06-01 03:05:48', '2022-06-01 03:05:48', 0),
(533, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-083616-ev-img02.jpg', 'jpg', '', 155, 'c24ccjee1jjtbrjzpiky', 0, NULL, '2022-06-01 03:06:16', '2022-06-01 03:06:16', 0),
(534, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-084438-avatar-img.jpg', 'jpg', '', 155, '9lowau9fpugubylrglbs', 0, NULL, '2022-06-01 03:14:38', '2022-06-01 03:14:38', 0),
(535, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-084638-ev-img01.jpg', 'jpg', '', 155, 'x31mvfiewqgnyfgjdvu5', 0, NULL, '2022-06-01 03:16:38', '2022-06-01 03:16:38', 0),
(536, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-092538-ev-img02.jpg', 'jpg', '', 155, 'kx1snwqe2hfclt6ceenu', 0, NULL, '2022-06-01 03:55:38', '2022-06-01 03:55:38', 0),
(537, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-101610-ev-img03.jpg', 'jpg', '', 155, 'eitguszsg67hmgwvq3fk', 0, NULL, '2022-06-01 04:46:10', '2022-06-01 04:46:10', 0),
(538, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-103012-ev-img02.jpg', 'jpg', '', 155, 'vfjs7uopxgaardu6sbfs', 0, NULL, '2022-06-01 05:00:12', '2022-06-01 05:00:12', 0),
(539, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-103329-avatar-img.jpg', 'jpg', '', 155, 'ebaqletmecthkpumgeev', 0, NULL, '2022-06-01 05:03:29', '2022-06-01 05:03:29', 0),
(540, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-103342-ev-img01.jpg', 'jpg', '', 155, '2vpcczjbq9goeixamrfz', 0, NULL, '2022-06-01 05:03:42', '2022-06-01 05:03:42', 0),
(541, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-103349-ev-img02.jpg', 'jpg', '', 155, '5mdgisavwp2dcvd3zux3', 0, NULL, '2022-06-01 05:03:49', '2022-06-01 05:03:49', 0),
(542, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-113317-avatar-img.jpg', 'jpg', '', 155, 'd8tdgtlmgguote23tr3k', 0, NULL, '2022-06-01 06:03:17', '2022-06-01 06:03:17', 0),
(543, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-114032-avatar-img.jpg', 'jpg', '', 155, 'pdpmfptpovgolw5ajhx9', 0, NULL, '2022-06-01 06:10:32', '2022-06-01 06:10:32', 0),
(544, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-124006-avatar-img.jpg', 'jpg', '', 155, '6sabpno9bwdir7yzncps', 0, NULL, '2022-06-01 07:10:06', '2022-06-01 07:10:06', 0),
(545, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-124019-ev-img01.jpg', 'jpg', '', 155, 'ln51wdkzen60cgpsvxaf', 0, NULL, '2022-06-01 07:10:19', '2022-06-01 07:10:19', 0),
(546, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-124025-ev-img02.jpg', 'jpg', '', 155, 'q3p3td66dsmsq2nzqyhv', 0, NULL, '2022-06-01 07:10:25', '2022-06-01 07:10:25', 0),
(547, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-124826-dummy.pdf', 'pdf', '', 155, 'rvgnon0vtkir7fnljee5', 0, NULL, '2022-06-01 07:18:26', '2022-06-01 07:18:26', 0),
(548, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-124835-A Sample PDF.pdf', 'pdf', '', 155, 'brt829kcrowodvtiok3j', 0, NULL, '2022-06-01 07:18:35', '2022-06-01 07:18:35', 0),
(549, 'pdf-sample.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-125025-pdf-sample.pdf', 'pdf', '', 155, '76o34tizdmwnaxu2mres', 0, NULL, '2022-06-01 07:20:25', '2022-06-01 07:20:25', 0),
(550, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-125145-A Sample PDF.pdf', 'pdf', '', 155, '8jqior1m5oncamkc1fgc', 0, NULL, '2022-06-01 07:21:45', '2022-06-01 07:21:45', 0),
(551, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-125151-dummy.pdf', 'pdf', '', 155, '45crsnsskzx5y4oevhpu', 0, NULL, '2022-06-01 07:21:51', '2022-06-01 07:21:51', 0),
(552, 'pdf-sample.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-125159-pdf-sample.pdf', 'pdf', '', 155, 'ggskdzmq7me49mi0zg0h', 0, NULL, '2022-06-01 07:21:59', '2022-06-01 07:21:59', 0),
(553, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-125205-A Sample PDF.pdf', 'pdf', '', 155, 'pkrrhiquq1p1dacfvpnz', 0, NULL, '2022-06-01 07:22:05', '2022-06-01 07:22:05', 0),
(554, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-131144-A Sample PDF.pdf', 'pdf', '', 155, 'ayawll3klffapmriry6q', 0, NULL, '2022-06-01 07:41:44', '2022-06-01 07:41:44', 0),
(555, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-131227-dummy.pdf', 'pdf', '', 155, 'j9th1doptewyofccdsvm', 0, NULL, '2022-06-01 07:42:27', '2022-06-01 07:42:27', 0),
(556, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-131243-A Sample PDF.pdf', 'pdf', '', 155, 'tsxkiqbidl9kapeijzcm', 0, NULL, '2022-06-01 07:42:43', '2022-06-01 07:42:43', 0),
(557, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-131330-A Sample PDF.pdf', 'pdf', '', 155, 'bgppo3agqgqxmvz2ojjr', 0, NULL, '2022-06-01 07:43:30', '2022-06-01 07:43:30', 0),
(558, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-131438-dummy.pdf', 'pdf', '', 155, 'urufbe9oduwkwhjfgsog', 0, NULL, '2022-06-01 07:44:38', '2022-06-01 07:44:38', 0),
(559, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-131452-dummy.pdf', 'pdf', '', 155, 'sojyer0wdxj1krvee0pc', 0, NULL, '2022-06-01 07:44:52', '2022-06-01 07:44:52', 0),
(560, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-132140-A Sample PDF.pdf', 'pdf', '', 155, 'agilljt8zu9segyafao5', 0, NULL, '2022-06-01 07:51:40', '2022-06-01 07:51:40', 0),
(561, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-132151-dummy.pdf', 'pdf', '', 155, '8vwswatxu3x0gz3zn3xv', 0, NULL, '2022-06-01 07:51:51', '2022-06-01 07:51:51', 0),
(562, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-132710-avatar-img.jpg', 'jpg', '', 155, '7xvnystncmdqmvgblrkc', 0, NULL, '2022-06-01 07:57:10', '2022-06-01 07:57:10', 0),
(563, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-132722-A Sample PDF.pdf', 'pdf', '', 155, 'xbbe4kqozlnwhgbavqxr', 0, NULL, '2022-06-01 07:57:22', '2022-06-01 07:57:22', 0),
(564, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-132729-ev-img01.jpg', 'jpg', '', 155, 'hep56cf9bmgsysmahotb', 0, NULL, '2022-06-01 07:57:29', '2022-06-01 07:57:29', 0),
(565, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-132743-avatar-img.jpg', 'jpg', '', 155, 'x56dgoa5fdc6vurigqak', 0, NULL, '2022-06-01 07:57:43', '2022-06-01 07:57:43', 0),
(566, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-132752-ev-img01.jpg', 'jpg', '', 155, 'nmzj2dzue6m1vwonrpgi', 0, NULL, '2022-06-01 07:57:52', '2022-06-01 07:57:52', 0),
(567, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-132759-ev-img02.jpg', 'jpg', '', 155, '40t4ltoeizyqdh2xlacg', 0, NULL, '2022-06-01 07:57:59', '2022-06-01 07:57:59', 0),
(568, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-132804-ev-img03.jpg', 'jpg', '', 155, 'az2wogd915jvunwfztbq', 0, NULL, '2022-06-01 07:58:04', '2022-06-01 07:58:04', 0),
(569, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-132824-A Sample PDF.pdf', 'pdf', '', 155, 'r9jhds0ucqgmuvdbgamy', 0, NULL, '2022-06-01 07:58:24', '2022-06-01 07:58:24', 0),
(570, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-132831-dummy.pdf', 'pdf', '', 155, '1rxfeozkblnpizdqxvrq', 0, NULL, '2022-06-01 07:58:31', '2022-06-01 07:58:31', 0),
(571, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-132854-dummy.pdf', 'pdf', '', 155, 'pslz3feg1l91ve0snby2', 0, NULL, '2022-06-01 07:58:54', '2022-06-01 07:58:54', 0),
(572, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-133014-A Sample PDF.pdf', 'pdf', '', 155, 'fjxzivssztd1ylscie4m', 0, NULL, '2022-06-01 08:00:14', '2022-06-01 08:00:14', 0),
(573, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-133216-A Sample PDF.pdf', 'pdf', '', 155, 'o9b7qzhct9yfrzzzttiw', 0, NULL, '2022-06-01 08:02:16', '2022-06-01 08:02:16', 0),
(574, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-133222-dummy.pdf', 'pdf', '', 155, 'gzbog6n9vybpfd9gja7b', 0, NULL, '2022-06-01 08:02:22', '2022-06-01 08:02:22', 0),
(575, 'pdf-sample.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-133229-pdf-sample.pdf', 'pdf', '', 155, 'dc8a0sg7mzxvmhwzgmoq', 0, NULL, '2022-06-01 08:02:29', '2022-06-01 08:02:29', 0),
(576, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-133243-A Sample PDF.pdf', 'pdf', '', 155, 'lj7hjuodgd13pfzthupe', 0, NULL, '2022-06-01 08:02:43', '2022-06-01 08:02:43', 0),
(577, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-135747-A Sample PDF.pdf', 'pdf', '', 155, 'zwnv5hgfo1phrbqi6dvu', 0, NULL, '2022-06-01 08:27:47', '2022-06-01 08:27:47', 0),
(578, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-140106-A Sample PDF.pdf', 'pdf', '', 155, '9zlk09t8d8txc4vxdkvf', 0, NULL, '2022-06-01 08:31:06', '2022-06-01 08:31:06', 0),
(579, 'pdf-sample.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-140118-pdf-sample.pdf', 'pdf', '', 155, 'uahcku31ufxzt3kyaxqt', 0, NULL, '2022-06-01 08:31:18', '2022-06-01 08:31:18', 0),
(580, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-140124-A Sample PDF.pdf', 'pdf', '', 155, 'yrf7ijsmko5xh7fhfggh', 0, NULL, '2022-06-01 08:31:24', '2022-06-01 08:31:24', 0),
(581, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-140130-dummy.pdf', 'pdf', '', 155, 'lejn1yaxmcdbcwlyrvpv', 0, NULL, '2022-06-01 08:31:30', '2022-06-01 08:31:30', 0),
(582, 'pdf-sample.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-140137-pdf-sample.pdf', 'pdf', '', 155, 'a5yino50yvpiszzoggbs', 0, NULL, '2022-06-01 08:31:37', '2022-06-01 08:31:37', 0),
(583, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-140143-A Sample PDF.pdf', 'pdf', '', 155, '5frnkktoonia9fz6hdxr', 0, NULL, '2022-06-01 08:31:43', '2022-06-01 08:31:43', 0),
(584, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-140149-dummy.pdf', 'pdf', '', 155, 'x2lnjzfuruzfbstiry5l', 0, NULL, '2022-06-01 08:31:49', '2022-06-01 08:31:49', 0),
(585, 'pdf-sample.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-140156-pdf-sample.pdf', 'pdf', '', 155, 'leymuby7m9knfmwg3cpc', 0, NULL, '2022-06-01 08:31:56', '2022-06-01 08:31:56', 0),
(586, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-01-140201-A Sample PDF.pdf', 'pdf', '', 155, 'rnwergmclr0rqthw1mgd', 0, NULL, '2022-06-01 08:32:01', '2022-06-01 08:32:01', 0),
(587, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-03-060542-ev-img01.jpg', 'jpg', '', 155, 'y5hkatzpzdr9ot9rjrrb', 0, NULL, '2022-06-03 00:35:42', '2022-06-03 00:35:42', 0),
(588, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-03-060547-ev-img02.jpg', 'jpg', '', 155, 'gtjhyi6uobpel4wxecv1', 0, NULL, '2022-06-03 00:35:47', '2022-06-03 00:35:47', 0),
(589, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-03-070705-ev-img01.jpg', 'jpg', '', 155, 'uwrm770s5yrbryqqbpgk', 0, NULL, '2022-06-03 01:37:05', '2022-06-03 01:37:05', 0),
(590, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-03-070711-ev-img02.jpg', 'jpg', '', 155, 'yryynwjsm2fbeeii3cua', 0, NULL, '2022-06-03 01:37:11', '2022-06-03 01:37:11', 0),
(591, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-03-092431-ev-img01.jpg', 'jpg', '', 155, '0dvujyzuvga4nsut6t1v', 0, NULL, '2022-06-03 03:54:31', '2022-06-03 03:54:31', 0),
(592, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-03-092447-ev-img02.jpg', 'jpg', '', 155, '8tpl85o3wcfobwgvrkxo', 0, NULL, '2022-06-03 03:54:47', '2022-06-03 03:54:47', 0),
(593, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-03-092453-ev-img03.jpg', 'jpg', '', 155, 't0m6nd1ozjhm2rn4u6mr', 0, NULL, '2022-06-03 03:54:53', '2022-06-03 03:54:53', 0),
(594, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-03-092820-ev-img01.jpg', 'jpg', '', 155, 'fygqpye36ejgvpnl5ony', 0, NULL, '2022-06-03 03:58:20', '2022-06-03 03:58:20', 0),
(595, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-03-092827-ev-img03.jpg', 'jpg', '', 155, 'nckuohf4cbvbrmnjaht8', 0, NULL, '2022-06-03 03:58:27', '2022-06-03 03:58:27', 0),
(596, 'flower-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-03-092831-flower-img.jpg', 'jpg', '', 155, '5jl4p56sgqhquh36ny3v', 0, NULL, '2022-06-03 03:58:31', '2022-06-03 03:58:31', 0),
(597, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-03-093000-ev-img01.jpg', 'jpg', '', 155, '5lnvqp7ulzwfbdv8f7vs', 0, NULL, '2022-06-03 04:00:00', '2022-06-03 04:00:00', 0),
(598, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-03-093014-ev-img02.jpg', 'jpg', '', 155, '3srt5rlwzthrkwerlxfv', 0, NULL, '2022-06-03 04:00:14', '2022-06-03 04:00:14', 0),
(599, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-03-093019-ev-img03.jpg', 'jpg', '', 155, 'jgq4kh9h0tzgswirnrzi', 0, NULL, '2022-06-03 04:00:19', '2022-06-03 04:00:19', 0),
(600, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-03-123736-dummy.pdf', 'pdf', '', 155, 'xtjuiacg2417vjmnzhvc', 0, NULL, '2022-06-03 07:07:36', '2022-06-03 07:07:36', 0),
(601, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-03-133630-A Sample PDF.pdf', 'pdf', '', 155, 'xwbe0zxvnol7lmtrerfl', 0, NULL, '2022-06-03 08:06:30', '2022-06-03 08:06:30', 0),
(602, 'pdf-sample.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-03-133641-pdf-sample.pdf', 'pdf', '', 155, 'eo42sq0tluhdccsk1efb', 0, NULL, '2022-06-03 08:06:41', '2022-06-03 08:06:41', 0),
(604, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-20-142233-ev-img01.jpg', 'jpg', '', 155, '6omhsndbfkjkn0emalaz', 0, NULL, '2022-06-20 08:52:33', '2022-06-20 08:52:33', 1),
(607, 'pdf-sample.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-091811-pdf-sample.pdf', 'pdf', '', 155, 'ilzw7zirqlvgbelyn551', 0, NULL, '2022-06-30 03:48:11', '2022-06-30 03:48:11', 1),
(608, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-102500-ev-img02.jpg', 'jpg', '', 155, 'wcxzwwsbls1p2tfmry8c', 0, NULL, '2022-06-30 04:55:00', '2022-06-30 04:55:00', 1),
(609, 'flower-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-102540-flower-img.jpg', 'jpg', '', 155, 'f1quvectbiwcuamf2vks', 0, NULL, '2022-06-30 04:55:40', '2022-06-30 04:55:40', 1),
(610, 'nature101.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-103038-nature101.jpg', 'jpg', '', 155, 'gqwa8z3xzd9abx7k2i7f', 0, NULL, '2022-06-30 05:00:38', '2022-06-30 05:00:38', 1),
(611, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-103119-ev-img03.jpg', 'jpg', '', 155, '9upftsxwo62djhrcsnqi', 0, NULL, '2022-06-30 05:01:19', '2022-06-30 05:01:19', 1),
(612, 'nature102.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-103628-nature102.jpg', 'jpg', '', 155, 'cgh6dqh30lkdi86thzxl', 0, NULL, '2022-06-30 05:06:28', '2022-06-30 05:06:28', 1),
(613, 'nouser.png', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-103656-nouser.png', 'png', '', 155, 'o9gn1wtxfjw8tauret8i', 0, NULL, '2022-06-30 05:06:56', '2022-06-30 05:06:56', 1),
(614, 'nature103.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-103840-nature103.jpg', 'jpg', '', 155, 'fn0m6z1gz3c7rvq0uilc', 0, NULL, '2022-06-30 05:08:40', '2022-06-30 05:08:40', 1),
(615, 'pdf-sample.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-103904-pdf-sample.pdf', 'pdf', '', 155, 'a980kjzmyqc5kcbj2tcy', 0, NULL, '2022-06-30 05:09:04', '2022-06-30 05:09:04', 1),
(616, 'img345.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-121418-img345.jpg', 'jpg', '', 155, 'gwk9fw79zfszry6pvnj3', 0, NULL, '2022-06-30 06:44:18', '2022-06-30 06:44:18', 1),
(617, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-121432-dummy.pdf', 'pdf', '', 155, 'k9cmxy08as9bat2suroj', 0, NULL, '2022-06-30 06:44:32', '2022-06-30 06:44:32', 1),
(618, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-121540-ev-img03.jpg', 'jpg', '', 155, 'n6lach8artqninjhz5fq', 0, NULL, '2022-06-30 06:45:40', '2022-06-30 06:45:40', 1),
(619, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-121833-dummy.pdf', 'pdf', '', 155, 'wvyo55ckqo8e0uwwbulq', 0, NULL, '2022-06-30 06:48:33', '2022-06-30 06:48:33', 1),
(620, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-122409-ev-img01.jpg', 'jpg', '', 155, 'nj0b3wpd4ew0kwnsvnue', 0, NULL, '2022-06-30 06:54:09', '2022-06-30 06:54:09', 1),
(621, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-122454-ev-img01.jpg', 'jpg', '', 155, 'l3lwjjh9bnuqfyot1ueb', 0, NULL, '2022-06-30 06:54:54', '2022-06-30 06:54:54', 0),
(622, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-134228-avatar-img.jpg', 'jpg', '', 155, 'buetyaalwpxbmtnvmfsk', 0, NULL, '2022-06-30 08:12:28', '2022-06-30 08:12:28', 0),
(623, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-140655-avatar-img.jpg', 'jpg', '', 155, '5krvnnpossp4u1vlishx', 0, NULL, '2022-06-30 08:36:55', '2022-06-30 08:36:55', 0),
(624, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-140746-avatar-img.jpg', 'jpg', '', 155, '7tl7igzao48nkzezyx4r', 0, NULL, '2022-06-30 08:37:46', '2022-06-30 08:37:46', 0),
(625, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-140914-ev-img01.jpg', 'jpg', '', 155, 'efxgrkijxvih0m5wbnae', 0, NULL, '2022-06-30 08:39:14', '2022-06-30 08:39:14', 0),
(626, 'flower-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-140948-flower-img.jpg', 'jpg', '', 155, 'tharhotcisdg4knrxqkx', 0, NULL, '2022-06-30 08:39:48', '2022-06-30 08:39:48', 0),
(627, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-141003-ev-img01.jpg', 'jpg', '', 155, 'f6n2ydb7afebyzyyd6bo', 0, NULL, '2022-06-30 08:40:03', '2022-06-30 08:40:03', 0),
(628, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-141026-ev-img03.jpg', 'jpg', '', 155, '3u4uljyaz4dxnj7hzmh6', 0, NULL, '2022-06-30 08:40:26', '2022-06-30 08:40:26', 0),
(629, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-141251-avatar-img.jpg', 'jpg', '', 155, 're28oyowcj6oznmri7ec', 0, NULL, '2022-06-30 08:42:51', '2022-06-30 08:42:51', 0),
(630, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-141302-ev-img01.jpg', 'jpg', '', 155, '2tvwue1jfa5nspyieire', 0, NULL, '2022-06-30 08:43:02', '2022-06-30 08:43:02', 0),
(631, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-141310-ev-img03.jpg', 'jpg', '', 155, 'ohjej2aftwyj5yqmbvco', 0, NULL, '2022-06-30 08:43:10', '2022-06-30 08:43:10', 0),
(632, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-144031-ev-img01.jpg', 'jpg', '', 155, 'k9tq4hb9bh2wpjwjkgwh', 0, NULL, '2022-06-30 09:10:31', '2022-06-30 09:10:31', 0),
(633, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-144313-ev-img01.jpg', 'jpg', '', 155, 'qikdtpdytkmliv43mlti', 0, NULL, '2022-06-30 09:13:13', '2022-06-30 09:13:13', 0),
(634, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-145031-ev-img01.jpg', 'jpg', '', 155, '2yibo5cxujzcqrlrfgpr', 0, NULL, '2022-06-30 09:20:31', '2022-06-30 09:20:31', 0),
(635, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-145633-A Sample PDF.pdf', 'pdf', '', 155, '1ttcajkgyrr0gpnzphzs', 0, NULL, '2022-06-30 09:26:33', '2022-06-30 09:26:33', 0),
(636, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-145722-dummy.pdf', 'pdf', '', 155, 'tfkshyi70harlom5csyf', 0, NULL, '2022-06-30 09:27:22', '2022-06-30 09:27:22', 0),
(637, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-145928-A Sample PDF.pdf', 'pdf', '', 155, 'wue1svfvxtlotfghltmz', 0, NULL, '2022-06-30 09:29:28', '2022-06-30 09:29:28', 0),
(638, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-145938-dummy.pdf', 'pdf', '', 155, 'x5qa7rbpc1mfa6lq8esf', 0, NULL, '2022-06-30 09:29:38', '2022-06-30 09:29:38', 0),
(639, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-145948-dummy.pdf', 'pdf', '', 155, 'elrfpj5pmzqy0zkejrgu', 0, NULL, '2022-06-30 09:29:48', '2022-06-30 09:29:48', 0),
(640, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-150008-A Sample PDF.pdf', 'pdf', '', 155, 'anjnyqyovzb0voekxxol', 0, NULL, '2022-06-30 09:30:08', '2022-06-30 09:30:08', 0),
(641, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-150020-dummy.pdf', 'pdf', '', 155, 'dwmekhy3n2eybwpcu51a', 0, NULL, '2022-06-30 09:30:20', '2022-06-30 09:30:20', 0),
(642, 'pdf-sample.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-150030-pdf-sample.pdf', 'pdf', '', 155, 'dct2veeozg92fwvqfmv9', 0, NULL, '2022-06-30 09:30:30', '2022-06-30 09:30:30', 0),
(643, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-150040-A Sample PDF.pdf', 'pdf', '', 155, 'yhjcw89jzujde4opgdqr', 0, NULL, '2022-06-30 09:30:40', '2022-06-30 09:30:40', 0),
(644, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-150117-dummy.pdf', 'pdf', '', 155, 'xtok3itoiyhqt2rxjl75', 0, NULL, '2022-06-30 09:31:17', '2022-06-30 09:31:17', 0),
(645, 'pdf-sample.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-150148-pdf-sample.pdf', 'pdf', '', 155, 'obvdjbbumlmyoij1ngkm', 0, NULL, '2022-06-30 09:31:48', '2022-06-30 09:31:48', 0),
(646, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-150215-dummy.pdf', 'pdf', '', 155, 'mwpbkb6mrtcl3d9axwgt', 0, NULL, '2022-06-30 09:32:15', '2022-06-30 09:32:15', 0),
(647, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-150238-A Sample PDF.pdf', 'pdf', '', 155, 'qxyhg3bic2q5xng3abww', 0, NULL, '2022-06-30 09:32:38', '2022-06-30 09:32:38', 0),
(648, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-150624-ev-img01.jpg', 'jpg', '', 155, 'xymm3a2newdvnpfi8s8a', 0, NULL, '2022-06-30 09:36:24', '2022-06-30 09:36:24', 0),
(649, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-06-30-150645-ev-img02.jpg', 'jpg', '', 155, 'h9qqtjjejrfsw9x10fpb', 0, NULL, '2022-06-30 09:36:45', '2022-06-30 09:36:45', 0),
(650, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-07-01-064352-A Sample PDF.pdf', 'pdf', '', 155, 'gohorakvhjevcn1aijad', 0, NULL, '2022-07-01 01:13:52', '2022-07-01 01:13:52', 0),
(651, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-07-01-064407-dummy.pdf', 'pdf', '', 155, 'conm5mywnrvri24xht5y', 0, NULL, '2022-07-01 01:14:07', '2022-07-01 01:14:07', 0),
(652, 'flower-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-07-01-065000-flower-img.jpg', 'jpg', '', 155, 'nd8bpc2hu7gsnhzmjrhw', 0, NULL, '2022-07-01 01:20:00', '2022-07-01 01:20:00', 0),
(653, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-07-01-065513-avatar-img.jpg', 'jpg', '', 155, 'hisnoejasqassvof97yh', 0, NULL, '2022-07-01 01:25:13', '2022-07-01 01:25:13', 0),
(654, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-07-01-065527-avatar-img.jpg', 'jpg', '', 155, '0pzwlvfo4gyyk62zxl33', 0, NULL, '2022-07-01 01:25:27', '2022-07-01 01:25:27', 0),
(655, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-06-050644-avatar-img.jpg', 'jpg', '', 175, 'srylael3p3cz1h0pqgwc', 0, NULL, '2022-09-05 23:36:44', '2022-09-05 23:36:44', 0),
(656, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-06-051003-avatar-img.jpg', 'jpg', '', 155, 'uiwk1rryvhbpj9qiajut', 0, NULL, '2022-09-05 23:40:03', '2022-09-05 23:40:03', 0),
(657, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-06-051218-avatar-img.jpg', 'jpg', '', 175, 'akjlcbtznxfluorhtd1q', 0, NULL, '2022-09-05 23:42:18', '2022-09-05 23:42:18', 0),
(658, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-06-051334-avatar-img.jpg', 'jpg', '', 175, 'lulthuo6jiug0t6a6ofd', 0, NULL, '2022-09-05 23:43:34', '2022-09-05 23:43:34', 0),
(659, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-06-051429-ev-img01.jpg', 'jpg', '', 175, 'mgcphy29vimbjq8xpwrl', 0, NULL, '2022-09-05 23:44:29', '2022-09-05 23:44:29', 0),
(660, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-06-051604-ev-img01.jpg', 'jpg', '', 175, 'xcrdjrg7qqjbxw8wygfk', 0, NULL, '2022-09-05 23:46:04', '2022-09-05 23:46:04', 0),
(661, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-06-051717-avatar-img.jpg', 'jpg', '', 175, 'hy64eg4de9gcdzjj06pv', 0, NULL, '2022-09-05 23:47:17', '2022-09-05 23:47:17', 0),
(662, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-06-051926-avatar-img.jpg', 'jpg', '', 175, 'qmu9wt9ohhfzc3kkz6ya', 0, NULL, '2022-09-05 23:49:26', '2022-09-05 23:49:26', 0),
(663, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-06-133618-avatar-img.jpg', 'jpg', '', 177, 'xmnckqzbu8tewcqxo3on', 0, NULL, '2022-09-06 08:06:18', '2022-09-06 08:06:18', 0),
(664, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-07-062824-ev-img01.jpg', 'jpg', '', 178, '5fjghiamfnpf2rx7cqcj', 0, NULL, '2022-09-07 00:58:24', '2022-09-07 00:58:24', 0),
(665, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-07-062831-ev-img02.jpg', 'jpg', '', 178, 'jawuqeqchylerrhssaqf', 0, NULL, '2022-09-07 00:58:31', '2022-09-07 00:58:31', 0),
(666, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-07-100734-ev-img01.jpg', 'jpg', '', 155, 'ln6khiuj6uq3lkadq2gi', 0, NULL, '2022-09-07 04:37:34', '2022-09-07 04:37:34', 0),
(667, 'A Sample PDF.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-07-102115-A Sample PDF.pdf', 'pdf', '', 155, '2zqagfxndmdlyueivkk7', 0, NULL, '2022-09-07 04:51:15', '2022-09-07 04:51:15', 0),
(668, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-07-102123-dummy.pdf', 'pdf', '', 155, 'zkrra8ndfgf2ui6inh83', 0, NULL, '2022-09-07 04:51:23', '2022-09-07 04:51:23', 0),
(669, 'pdf-sample.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-07-102132-pdf-sample.pdf', 'pdf', '', 155, 'vcpmxd9a21qxn9qkk4ua', 0, NULL, '2022-09-07 04:51:32', '2022-09-07 04:51:32', 0),
(670, 'dummy.pdf', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-07-102138-dummy.pdf', 'pdf', '', 155, 'qopvbzddswrse0rrw50v', 0, NULL, '2022-09-07 04:51:38', '2022-09-07 04:51:38', 0),
(671, 'nature103.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-23-073803-nature103.jpg', 'jpg', '', 155, '2soeftziqmii7d1ewoia', 0, NULL, '2022-09-23 02:08:03', '2022-09-23 02:08:03', 1),
(672, 'nature101.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-23-073829-nature101.jpg', 'jpg', '', 155, 'n02uozqx6huttra0nyjm', 0, NULL, '2022-09-23 02:08:29', '2022-09-23 02:08:29', 1),
(673, '7159433.png', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-122342-7159433.png', 'png', '', 155, 'ohayiw3au2k8yba8liaq', 0, NULL, '2022-09-28 06:53:42', '2022-09-28 06:53:42', 0),
(674, '9-2-winner-png-clipart.png', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-124906-9-2-winner-png-clipart.png', 'png', '', 155, 'awytsjn4dzahxv2catvo', 0, NULL, '2022-09-28 07:19:06', '2022-09-28 07:19:06', 1),
(675, '9-2-winner-png-clipart.png', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-125512-9-2-winner-png-clipart.png', 'png', '', 155, '5mxovf3apl55spkukw1y', 0, NULL, '2022-09-28 07:25:12', '2022-09-28 07:25:12', 1),
(676, '9-2-winner-png-clipart.png', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-131723-9-2-winner-png-clipart.png', 'png', '', 155, 'azn7ijeiisfhg29bhw6z', 0, NULL, '2022-09-28 07:47:23', '2022-09-28 07:47:23', 1),
(677, 'images (1).png', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-132056-images (1).png', 'png', '', 155, '7d5w4vjt2mud81wr4rar', 0, NULL, '2022-09-28 07:50:56', '2022-09-28 07:50:56', 1),
(678, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-145236-avatar-img.jpg', 'jpg', '', 155, 'jmxv2qox3gz0r4aaqwny', 0, NULL, '2022-09-28 09:22:36', '2022-09-28 09:22:36', 0),
(679, 'nature101.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-150640-nature101.jpg', 'jpg', '', 155, 'qbkgqptafyrgrovo08fg', 0, NULL, '2022-09-28 09:36:40', '2022-09-28 09:36:40', 0),
(680, 'flower-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-151959-flower-img.jpg', 'jpg', '', 155, 'txqsztf7pbjeeu0minqm', 0, NULL, '2022-09-28 09:49:59', '2022-09-28 09:49:59', 0),
(681, 'flower-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-152151-flower-img.jpg', 'jpg', '', 155, 'ypsgn2vhytxmtlwlw2yj', 0, NULL, '2022-09-28 09:51:51', '2022-09-28 09:51:51', 1),
(682, 'ev-img02_760x400.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-164547-ev-img02_760x400.jpg', 'jpg', '', 155, 'thn06kpsxsosjvw5uer5', 0, NULL, '2022-09-28 11:15:47', '2022-09-28 11:15:47', 0),
(683, 'flower-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-170024-flower-img.jpg', 'jpg', '', 155, 'fci6mjjodjtmglq0glcg', 0, NULL, '2022-09-28 11:30:24', '2022-09-28 11:30:24', 0),
(684, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-170432-ev-img03.jpg', 'jpg', '', 155, 'sgwvsfbgfkjzycwn1fb8', 0, NULL, '2022-09-28 11:34:32', '2022-09-28 11:34:32', 0),
(685, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-170531-ev-img03.jpg', 'jpg', '', 155, 'rwekmhmvvwqpkue7mh3c', 0, NULL, '2022-09-28 11:35:31', '2022-09-28 11:35:31', 0),
(686, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-170734-ev-img03.jpg', 'jpg', '', 155, 'l3l0d2j8gkrcchj1eb5e', 0, NULL, '2022-09-28 11:37:34', '2022-09-28 11:37:34', 0),
(687, 'flower-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-170959-flower-img.jpg', 'jpg', '', 155, 'h2rkzmje62b8dg545jc3', 0, NULL, '2022-09-28 11:39:59', '2022-09-28 11:39:59', 0),
(688, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-171130-ev-img03.jpg', 'jpg', '', 155, '8fepq5zotuucing2yqvv', 0, NULL, '2022-09-28 11:41:30', '2022-09-28 11:41:30', 0),
(689, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-172300-ev-img03.jpg', 'jpg', '', 155, 'xffrfwgfi8spzwoezz0m', 0, NULL, '2022-09-28 11:53:00', '2022-09-28 11:53:00', 0),
(690, 'flower-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-185901-flower-img.jpg', 'jpg', '', 155, 'cbatnxi9tlgksnosxty9', 0, NULL, '2022-09-28 13:29:01', '2022-09-28 13:29:01', 0),
(691, 'ev-img02_760x400.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-190351-ev-img02_760x400.jpg', 'jpg', '', 155, 'tggttqpgrjpxqtdiuhww', 0, NULL, '2022-09-28 13:33:51', '2022-09-28 13:33:51', 0),
(692, 'ev-img02_760x400.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-190550-ev-img02_760x400.jpg', 'jpg', '', 155, 'bq76rxgtbng3tz7wfugt', 0, NULL, '2022-09-28 13:35:50', '2022-09-28 13:35:50', 0),
(693, 'ev-img03_1_540x300.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-28-190728-ev-img03_1_540x300.jpg', 'jpg', '', 155, 'hglgrul8yehr7gg8pjmj', 0, NULL, '2022-09-28 13:37:28', '2022-09-28 13:37:28', 0),
(694, 'flower-img_350x350.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-062526-flower-img_350x350.jpg', 'jpg', '', 155, 't0guegfmlivxsdwglmwx', 0, NULL, '2022-09-29 00:55:26', '2022-09-29 00:55:26', 0),
(695, '9-2-winner-png-clipart.png', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-092429-9-2-winner-png-clipart.png', 'png', '', 155, '37jbhxemo44fcdqlphjx', 0, NULL, '2022-09-29 03:54:29', '2022-09-29 03:54:29', 0),
(696, '7159433.png', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-092455-7159433.png', 'png', '', 155, 'viixvznk4ht85m8tgmqc', 0, NULL, '2022-09-29 03:54:55', '2022-09-29 03:54:55', 0),
(697, 'java-script-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-092517-java-script-img.jpg', 'jpg', '', 155, 'upbrc6h4bccnjqx1dhms', 0, NULL, '2022-09-29 03:55:17', '2022-09-29 03:55:17', 0),
(698, 'mysql-img_900x500.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-092531-mysql-img_900x500.jpg', 'jpg', '', 155, 'jjbylk4hjhiwvuvqu1yi', 0, NULL, '2022-09-29 03:55:31', '2022-09-29 03:55:31', 0),
(699, 'java-script-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-092602-java-script-img.jpg', 'jpg', '', 155, 'zft1xbyi3lpunyamwbpj', 0, NULL, '2022-09-29 03:56:02', '2022-09-29 03:56:02', 0),
(700, 'SQL-Microsoft.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-092618-SQL-Microsoft.jpg', 'jpg', '', 155, 'fnphpnfhhieqnp7qowoa', 0, NULL, '2022-09-29 03:56:18', '2022-09-29 03:56:18', 0),
(701, 'testimonial-img.png', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-092637-testimonial-img.png', 'png', '', 155, '7wvq8pof1yk4lh8m8vuv', 0, NULL, '2022-09-29 03:56:37', '2022-09-29 03:56:37', 0),
(702, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-124347-ev-img03.jpg', 'jpg', '', 155, '3f8gioinpiimla7xbrrp', 0, NULL, '2022-09-29 07:13:47', '2022-09-29 07:13:47', 0),
(703, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-134619-ev-img02.jpg', 'jpg', '', 155, 'cvdn1rrrvdh12gs2o5cu', 0, NULL, '2022-09-29 08:16:19', '2022-09-29 08:16:19', 0),
(704, 'nature101.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-134859-nature101.jpg', 'jpg', '', 155, 'aa48jus4re0luaqfl7ze', 0, NULL, '2022-09-29 08:18:59', '2022-09-29 08:18:59', 0),
(705, 'nature103.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-135606-nature103.jpg', 'jpg', '', 155, 'e3rx7sbbkmktj5ncpj5b', 0, NULL, '2022-09-29 08:26:06', '2022-09-29 08:26:06', 0),
(706, 'ev-img03.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-135859-ev-img03.jpg', 'jpg', '', 155, 'hqadpnchoalkhkyhkclg', 0, NULL, '2022-09-29 08:28:59', '2022-09-29 08:28:59', 0),
(707, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-135908-ev-img02.jpg', 'jpg', '', 155, '1tlppn18ywaipduznv9m', 0, NULL, '2022-09-29 08:29:08', '2022-09-29 08:29:08', 0),
(708, 'ev-img02.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-143533-ev-img02.jpg', 'jpg', '', 155, 'p6xjyfzxpml6jsvox7sj', 0, NULL, '2022-09-29 09:05:33', '2022-09-29 09:05:33', 0),
(709, 'img345.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-144559-img345.jpg', 'jpg', '', 155, 'vtnmkovfcdk9sgo5fwdj', 0, NULL, '2022-09-29 09:15:59', '2022-09-29 09:15:59', 0),
(710, 'nature103.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-145439-nature103.jpg', 'jpg', '', 155, 'etecvqvc59d7lfu5ngym', 0, NULL, '2022-09-29 09:24:39', '2022-09-29 09:24:39', 0),
(711, 'img111.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-145824-img111.jpg', 'jpg', '', 155, 'qbuxdihtflh4j2uqchkw', 0, NULL, '2022-09-29 09:28:24', '2022-09-29 09:28:24', 0),
(712, 'nature102.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-150757-nature102.jpg', 'jpg', '', 155, 'i5rbxb1is9vnivsdrkjz', 0, NULL, '2022-09-29 09:37:57', '2022-09-29 09:37:57', 0),
(713, 'img111.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-150919-img111.jpg', 'jpg', '', 155, '1mrxiwhnuhzynbfk7ngw', 0, NULL, '2022-09-29 09:39:19', '2022-09-29 09:39:19', 0),
(714, 'nature102.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-151010-nature102.jpg', 'jpg', '', 155, 'desnl1wf1dlcdcjesxxp', 0, NULL, '2022-09-29 09:40:10', '2022-09-29 09:40:10', 0),
(715, 'ev-img01.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-29-151025-ev-img01.jpg', 'jpg', '', 155, 'fyvru3todjhmr4yvrsra', 0, NULL, '2022-09-29 09:40:25', '2022-09-29 09:40:25', 0),
(716, 'flower-img_350x350.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-09-30-055513-flower-img_350x350.jpg', 'jpg', '', 155, '9jfsvhdtg2jz9qtkwwu3', 0, NULL, '2022-09-30 00:25:13', '2022-09-30 00:25:13', 0),
(717, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-10-14-065445-avatar-img.jpg', 'jpg', '', 155, 'vlwajdnoatiyslhddnd9', 0, NULL, '2022-10-14 01:24:45', '2022-10-14 01:24:45', 0),
(718, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-10-14-070808-avatar-img.jpg', 'jpg', '', 155, 'gzav0uxe5y0qkfc7m8q4', 0, NULL, '2022-10-14 01:38:08', '2022-10-14 01:38:08', 0),
(719, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-10-14-070916-avatar-img.jpg', 'jpg', '', 155, 'wumf0mdxwyvxupa1l9l5', 0, NULL, '2022-10-14 01:39:16', '2022-10-14 01:39:16', 0),
(720, 'avatar-img.jpg', 'C:\\xampp\\htdocs\\maxglobal\\storage\\uploads\\2022-10-14-071510-avatar-img.jpg', 'jpg', '', 155, 'vwjjzm0mfxwpasixxcus', 0, NULL, '2022-10-14 01:45:10', '2022-10-14 01:45:10', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `context_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Employee',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_approved` tinyint(1) DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `assigned_to_agents` int(11) DEFAULT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT 0,
  `user_status` varchar(250) COLLATE utf8_unicode_ci DEFAULT 'Pending',
  `enable_disable` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `context_id`, `email`, `password`, `type`, `remember_token`, `deleted_at`, `created_at`, `updated_at`, `is_approved`, `assigned_to`, `assigned_to_agents`, `verified`, `user_status`, `enable_disable`) VALUES
(1, 'Super Admin', 1, 'info@herdzo.com', '$2y$10$wRKJ3xPg2AaOBsWgpw7vG.TGWjgF3MrxTmWHoONUCxvmk9YR2oMtq', 'SUPER_ADMIN', 'TZOH9xZXg3immfgkhj23kBfpfzydkc2o4U5xZWC4xy2J6KNYCMip4xXzgj5I', NULL, '2019-11-08 12:06:31', '2022-09-27 05:35:38', NULL, NULL, NULL, 0, '', 1),
(155, 'Scholab Admin', 96, 'scholab.admin@yopmail.com', '$2y$10$wRKJ3xPg2AaOBsWgpw7vG.TGWjgF3MrxTmWHoONUCxvmk9YR2oMtq', 'MANAGER', 'nckyOQZfot8vMAfaOIS6AAmMGQhYG2foNC7VnICnLcTCjz1ruqdNyTaKu1pV', NULL, '2021-03-29 08:47:02', '2022-10-18 04:19:22', NULL, NULL, NULL, 0, 'Pending', 1),
(157, 'Sample', 98, 'sample.agent@yopmail.com', '$2y$10$EW.Q22qaw8bYUb2LujMO1.esTbEO8Dgvvf9z7pV7tq5v7fYyxeRmS', 'MAIN_AGENTS', 'wpw28fOTBsrsKk1YXizq8qUcOBcblsPzqMseIcEuTQY3wRaWYohYcp5z56p3', NULL, '2021-03-29 08:56:56', '2021-04-13 09:09:26', NULL, NULL, NULL, 0, 'Pending', 1),
(161, 'Sample Staff', 99, 'staff.scholab@yopmail.com', '$2y$10$exdINwxL7unJN0f1u3ewLe2FMowgI8Fui16Zqxafw.O3IsAxixN56', 'CONSULTANTS', NULL, NULL, '2021-03-29 10:44:32', '2021-03-29 10:44:32', NULL, NULL, NULL, 0, 'Pending', 1),
(162, 'Sample Staff', 100, 'staff.scholab.1@yopmail.com', '$2y$10$ya2SF5JiP1BmDp4//ZgsGeCxbe9JKGjdfT2ukpHwTG3CbCLRLeg3.', 'CONSULTANTS', '8I99ANfHfV7jvAsBPE9yoEegfNM1P2O7izv7oGvVIweiX1S2jOubH8fGS7fH', NULL, '2021-03-29 11:11:37', '2021-04-12 17:24:15', NULL, NULL, NULL, 0, 'Pending', 1),
(163, 'Mohammed', 1617016516, 'mail@iqbalkhan.in', '$2y$10$UMu3.3wLGCPvsVwGUfJTduClb6QLbEbSNVPK9KfpX9dz/iyZD9RoK', 'STUDENT', NULL, '2021-04-07 17:56:28', '2021-03-29 11:15:16', '2021-03-29 11:15:16', NULL, 172, NULL, 0, 'APPROVE', 1),
(164, 'Sample', 1617016877, 'samplestudent@yopmail.com', '$2y$10$EM1cHCLdWR/Qp2FWiYNhhejC4WpWdvQ/AEaqaPNt1CKtWkICer32C', 'STUDENT', NULL, NULL, '2021-03-29 11:21:17', '2021-03-29 11:21:17', NULL, 172, NULL, 1, 'REJECT', 1),
(165, 'ajeesh', 1617028776, 'ajeeshbabu789@gmail.com', '$2y$10$fe3B1V4KW/7LkSS5AO/w0.Se5JRMKYufBUnDMHclSIDSNM/8qwDJS', 'STUDENT', 'cUYxIjrbqmB2bOEtbYXNgUaNMujCVu02D6k3LQ5A3ImTGOFP1k7Nl5Q9IOrZ', '2021-04-07 17:56:24', '2021-03-29 14:39:36', '2021-04-09 11:06:12', NULL, 172, NULL, 0, 'Pending', 1),
(166, 'Iqbal ', 1617358363, 'mail.test@yopmail.com', '$2y$10$k7JZZ51VCkrlU/ms7xNqROn4pf4EGI7k1.ANvDCFhyztAdWnwBXja', 'STUDENT', 'MUMSBLV94V74jiqQZfq6aAiKTteL9MPdnojz1TC5LzHaxZU0vlESerqETwU0', NULL, '2021-04-02 10:12:43', '2021-04-02 10:23:58', NULL, 172, NULL, 0, 'IELTS processing', 1),
(167, 'Councillor 1 ', 101, 'staff.scholab.2@yopmail.com', '$2y$10$HKc2Ltg5snlxybbeLMDrKu7PgTG3gXrHTiNdZWyuRHmH1RT3cKYL.', 'CONSULTANTS', NULL, NULL, '2021-04-02 10:15:10', '2021-04-02 10:15:10', NULL, NULL, NULL, 0, 'Pending', 1),
(168, 'Farshad', 102, 'doc@scholabedu.com', '$2y$10$XB/MT01dB1kCkuPBEbEHCOqp5l3TbKqlr7QOwyoVtz1is2G3k4tXq', 'MAIN_AGENTS', '3QUnrAKJu51YAvcpwqR2RCnYcacRvOYccx4pQENd6DEIqfE6X1dimQIkLpeP', NULL, '2021-04-07 12:06:38', '2021-04-08 13:45:31', NULL, NULL, NULL, 0, 'Pending', 1),
(169, 'Ajeesh', 103, 'abacreationz@gmail.com', '$2y$10$4X9XEmWmwEO.lw1dtlNuz.nD2NKYWllQEQh7562dpz/ETQeQDCmkK', 'MAIN_AGENTS', 'fAQ36vRzlpNa1Q0scNQwht5i4lGiFwMJzd4QYzRIjKEZ9nThW04jVZyssQR9', NULL, '2021-04-07 12:27:22', '2021-04-09 11:03:36', NULL, NULL, NULL, 0, 'Pending', 1),
(170, 'Ajeesh', 1617809068, 'ajcreationz786@gmail.com', '$2y$10$iH1VagWTp80f2VJcU3w3Kep3g4bvT99RTAqk.LMWrBwpBMIotQyy6', 'STUDENT', '28wSPZnyi1oSdk6AcjCZpf3mhcJzNyAHDiIpr7BOIzWHl6VOkkB8ASbMPXBj', '2021-04-07 17:56:16', '2021-04-07 15:24:29', '2021-04-09 11:06:30', NULL, 172, NULL, 0, 'Passport processing', 1),
(171, 'qqqqqq', 104, 'axlcreationz@gmail.com', '$2y$10$51UpFYufedL4ywpqg4qnKOhaX0kXMoOGZdnABSM7hTu1dplheB.uq', 'CONSULTANTS', NULL, NULL, '2021-04-07 17:43:38', '2021-04-07 17:43:38', NULL, NULL, NULL, 0, 'Pending', 1),
(172, 'qqqqq', 105, 'ajcreationz111@gmail.com', '$2y$10$Ym38EHz6cw6JkR19XQF5E.0ULXDLF0xMPdQ5DtAnu08bB1mPZzDWS', 'CONSULTANTS', 'eAS2c90Ec3S7tK8Uaz1kWVWFI39rg2W9sR8kvrRKaGR0uBQBYLOQr6u7xgs2', NULL, '2021-04-07 17:45:21', '2021-04-13 09:14:24', NULL, NULL, NULL, 0, 'Pending', 1),
(173, 'dsd', 1617817709, 'dsdsds22sd33@yopmail.com', '$2y$10$Y3xGgbqqgSQ/yG4bhB0wUeCs2jeTz2NvJLTN4WbQNKle6.MLidmey', 'STUDENT', NULL, NULL, '2021-04-07 17:48:29', '2021-04-07 17:48:29', NULL, NULL, NULL, 0, 'Pending', 1),
(175, 'Priyanka', 106, 'priyanka333@yopmail.com', '$2y$10$2WMhhKfGDDFdVVZ33xIoouhY4hRW4bIrZtCl7MvrQLVvhRlXPeH4e', 'CONSULTANTS', 'q5AHU4xX8155VI5U2t0Hlt4BIUGa3NMdC0Jy6aH6RGNQG8latWzju2GcvLre', NULL, '2022-01-08 03:30:30', '2022-09-06 07:51:33', NULL, NULL, NULL, 0, 'Pending', 1),
(176, 'Sneha', 107, 'sneha333@yopmail.com', '$2y$10$ap1jK8Ol1ygG5cRPmqLXHeTzMWyDsYR1tdi.XjSDFF.5q5qDElb9S', 'MAIN_AGENTS', 'r2IHcSZzldVabjqYtPiWrgOHWuuINXIiqHWW3h4Up8zwYfCfccwkYGSBm01D', NULL, '2022-01-08 03:32:29', '2022-10-15 09:01:33', NULL, NULL, NULL, 0, 'Pending', 1),
(177, 'Akhil', 108, 'akhil333@yopmail.com', '$2y$10$Nla0QFfbkt3Pw9mECkZT1OIbt5gGH/baNvp4ibCkjuNayrnrn5rIW', 'SUBCONTRACTORS', 'me1BkyEYeVvSLnHi9EUzLzPg3JBF9ehNv5y4HpMMbVxZF0tiEraCLmNu0xpw', NULL, '2022-01-08 03:35:55', '2022-09-07 00:56:42', NULL, NULL, NULL, 0, 'Pending', 1),
(178, 'Karthik', 109, 'karthik333@yopmail.com', '$2y$10$U1UcN10XVzsxPUHvqDqXUuo0hxaHFJmtH39xKktAxMAbmy/2v6VK.', 'UNIVERSITY_USER', 'xMDRdZCjQ4liz3YdvjDqn4yrHLwHY2zq2fQTRVQdpIcnz6nBRLrEHsCWlUya', NULL, '2022-01-08 03:37:06', '2022-09-07 00:55:32', NULL, NULL, NULL, 0, 'Pending', 1),
(179, 'Kiran', 110, 'kiran333@yopmail.com', '$2y$10$jU7t8mPhodg5pnapQty24.B5SjFZvPRUT3A.n2T6sZUq.SUDuLv7C', 'EMPLOYER_ROLE', 'DvgguBtiXunJ1ZWhepg8HJqcULrbZxZtXaDl6kWZJLpdEpM7sgdMZjyukmfE', NULL, '2022-01-08 03:48:26', '2022-08-29 11:41:02', NULL, NULL, NULL, 0, 'Pending', 1),
(180, 'Adarsh', 111, 'adarsh333@yopmail.com', '$2y$10$qxSSLOR9Iato/b2wfYRJROMaMp5h5l7e1RPq9qq6Q/Dhi6jLdap0W', 'SUBCONTRACTORS', NULL, NULL, '2022-01-08 03:50:22', '2022-01-08 03:50:22', NULL, NULL, NULL, 0, 'Pending', 1),
(181, 'Praveen', 112, 'praveen333@yopmail.com', '$2y$10$xUUU/snUI8LmG9US4ZM1pu/OUJZQbfjG.xjCksAFZX8Zrr7ydB73S', 'MAIN_AGENTS', NULL, NULL, '2022-01-08 03:51:45', '2022-01-08 03:51:45', NULL, NULL, NULL, 0, 'Pending', 1),
(182, 'Afsal', 113, 'afsal333@yopmail.com', '$2y$10$iyc0oF8I0CsTUzY/uXYsHOQSixG6OwMwxJ73TaeTs6X59ajeWUKpC', 'CONSULTANTS', NULL, NULL, '2022-01-08 03:52:51', '2022-01-08 03:52:51', NULL, NULL, NULL, 0, 'Pending', 1),
(183, 'Aswin', 1641633951, 'aswin333@yopmail.com', '$2y$10$ZA85rak/QpqSYrPsCG646uFlbw4FT75eYA5nsiEzBDhV2XjAe2iB.', 'NURSE', NULL, NULL, '2022-01-08 03:55:51', '2022-01-08 03:55:51', NULL, NULL, NULL, 0, 'Pending', 1),
(184, 'Athulya', 1641642051, 'athulya333@yopmail.com', '$2y$10$AJzkErOSGQmUUu0ntgUUO.11Cp6HikWyHPe5ETAq/CH.r5WXM6cge', 'NURSE', NULL, NULL, '2022-01-08 06:10:51', '2022-01-08 06:10:51', NULL, NULL, NULL, 0, 'Pending', 1),
(187, 'Ranjith', 114, 'ranjith333@yopmail.com', '$2y$10$/k9G5iDNfzq2.Sh.Eb1.suJi3s62XGLiAPPx5zGy16Ny.pd1XwmYa', 'MAIN_AGENTS', NULL, NULL, '2022-01-08 06:55:37', '2022-01-08 06:55:37', NULL, NULL, NULL, 0, 'Pending', 1),
(189, 'Vivek', 115, 'vivek333@yopmail.com', '$2y$10$xvHTQgPm8v8sMIID4jQGFeR2UouiagVzq.6EfGGAEKkZXmLhcA3OK', 'SUBCONTRACTORS', NULL, NULL, '2022-01-11 01:53:25', '2022-01-11 01:53:25', NULL, NULL, NULL, 0, 'Pending', 1),
(190, 'Anwar', 116, 'anwar333@yopmail.com', '$2y$10$IcIUkIeb/TVr.Yzd9yp6deAJDLHFhAQbLODbx5gOrfzhQU2J.u8Oi', 'UNIVERSITY_USER', NULL, NULL, '2022-01-11 02:05:38', '2022-01-11 02:05:38', NULL, NULL, NULL, 0, 'Pending', 1),
(193, 'Advik', 1641902209, 'advik333@yopmail.com', '$2y$10$QlpNZbupxn2X4vvdbesb1uCyda8plVnCZSMGE62pIaRoOaT6Bh0y2', 'STUDENT', NULL, NULL, '2022-01-11 06:26:49', '2022-01-11 06:26:49', NULL, NULL, NULL, 0, 'Pending', 1),
(194, 'Sreya', 1641902697, 'sreya333@yopmail.com', '$2y$10$WS1B78KbHbKuLP3PMczuF.glR1Ng.6GvBvQwGtCH4TWECzw83pikC', 'NURSE', NULL, NULL, '2022-01-11 06:34:57', '2022-01-11 06:34:57', NULL, NULL, NULL, 0, 'Pending', 1),
(195, 'Ajmal', 1641902889, 'ajmal333@yopmail.com', '$2y$10$AClP6zIx.FGqqVrqluLx.e5aEBUNUUV9zHjweh5GVLQb1OPU4/oD.', 'NURSE', NULL, NULL, '2022-01-11 06:38:09', '2022-01-11 06:38:09', NULL, NULL, NULL, 0, 'Pending', 1),
(196, 'Manu', 1641975318, 'manu333@yopmail.com', '$2y$10$5nFX64iDSlxOOAs267j4V.ppLNbBPEXlanxg3nYeNytb26avafLjW', 'STUDENT', NULL, NULL, '2022-01-12 02:45:18', '2022-01-12 02:45:18', NULL, NULL, NULL, 0, 'Pending', 1),
(197, 'Vivek', 117, 'vivek123@yopmail.com', '$2y$10$Q/HP2UGyR5f6Wk8FE47C3OCdVKR4iL1erhpK2PbX3VwhbNEUp9Cva', 'MAIN_AGENTS', NULL, NULL, '2022-01-14 00:47:32', '2022-01-14 00:47:32', NULL, NULL, NULL, 0, 'Pending', 1),
(198, 'Gayathri', 118, 'gayathri555@yopmail.com', '$2y$10$UZeRN4m9e4lfpmb0qG8C0OuMZKEgsB99PzTviBY3tBDq/fxf9grZS', 'CONSULTANTS', NULL, NULL, '2022-05-05 00:23:17', '2022-05-05 00:23:17', NULL, NULL, NULL, 0, 'Pending', 1),
(199, 'Sameer', 119, 'sameer456@yopmail.com', '$2y$10$wRKJ3xPg2AaOBsWgpw7vG.TGWjgF3MrxTmWHoONUCxvmk9YR2oMtq', 'MAIN_AGENTS', '2CcncuuZoU1boBgFDyLIyKdeFFQ04v1dMniKNCA4bzMZc3BhCPo4KLfPp0dz', NULL, '2022-05-05 05:23:51', '2022-07-22 23:20:06', NULL, NULL, NULL, 0, 'Pending', 1),
(200, 'Afsal', 120, 'afsal345@yopmail.com', '$2y$10$YXTiWYozoSrqsJ.A//i7Puj5lsYTfl1oxWwJHK4ZSpiuDvGTHLWpu', 'MAIN_AGENTS', NULL, NULL, '2022-05-05 05:29:59', '2022-05-05 05:29:59', NULL, NULL, NULL, 0, 'Pending', 1),
(201, 'Remya', 121, 'remya345@yopmail.com', '$2y$10$AUk8a.1sjyKrDukvTBa55eTFKPL2hK9faWjhSU7Un/ygLvx7wN72C', 'MAIN_AGENTS', NULL, NULL, '2022-05-05 05:46:47', '2022-05-05 05:46:47', NULL, NULL, NULL, 0, 'Pending', 1),
(202, 'Anoop', 122, 'anoop345@yopmail.com', '$2y$10$dPb3XYJVmJCHP8F4ifotdOFK892s1X0ucUQ47JXAwWNTa3GKCYsrS', 'MAIN_AGENTS', NULL, NULL, '2022-05-05 05:47:27', '2022-05-05 05:47:27', NULL, NULL, NULL, 0, 'Pending', 1),
(203, 'Surya', 123, 'surya345@yopmail.com', '$2y$10$wRKJ3xPg2AaOBsWgpw7vG.TGWjgF3MrxTmWHoONUCxvmk9YR2oMtq', 'CONSULTANTS', 'qQsKBCQAWS6AbggGMAvAMRlVFMf8ikVwJfwcKUuh35yrnJnt7wyUEBdax91U', NULL, '2022-05-05 05:48:33', '2022-09-03 04:46:21', NULL, NULL, NULL, 0, 'Pending', 1),
(204, 'Ajith', 124, 'ajith345@yopmail.com', '$2y$10$zsXBqDY8Ty3YzzEPE6ndM.aFnUl6beS2M5/QkJEY7Eyc3qyTwnqvK', 'MAIN_AGENTS', NULL, NULL, '2022-05-05 06:02:43', '2022-05-05 06:02:43', NULL, NULL, NULL, 0, 'Pending', 1),
(205, 'Kiran', 125, 'kiran345@yopmail.com', '$2y$10$lqhIQ3AzoyQRMJDMiRHhhOxbp/NqKK0vJGXWrMyKCf7b0.tqVwmq6', 'MAIN_AGENTS', NULL, NULL, '2022-05-05 06:03:21', '2022-05-05 06:03:21', NULL, NULL, NULL, 0, 'Pending', 1),
(206, 'Karan', 1651817046, 'karan45@yopmail.com', '$2y$10$4Zp7D.wtZ/Y.b8xN5cVy1.QNf50a4rA3hdiM2WvY.S3jr03pdCD/y', 'STUDENT', NULL, NULL, '2022-05-06 00:34:07', '2022-05-06 00:34:07', NULL, 175, NULL, 0, 'Pending', 1),
(207, 'Amaljith', 126, 'amaljith345@yopmail.com', '$2y$10$YnnxnW148fH/YdCsBc44YeoBAYqcrA8z8M5djnEl6AaXyjJoEHoui', 'CONSULTANTS', NULL, NULL, '2022-05-07 01:24:22', '2022-05-07 01:24:22', NULL, NULL, NULL, 0, 'Pending', 1),
(208, 'Riyas', 127, 'riyas345@yopmail.com', '$2y$10$MIqzLhLCUHZiGtdYqbqeGOGuvRrRP2nEV3Gz1dPxiazfwYhKa3c2C', 'CONSULTANTS', NULL, NULL, '2022-05-07 01:29:05', '2022-05-07 01:29:05', NULL, NULL, NULL, 0, 'Pending', 1),
(209, 'Saranya', 128, 'saranya345@yopmail.com', '$2y$10$Ez0PnlYGSm.3hEqV.HF.qOfEb8/msB6/JdCERZmnrL62ZHc1Jsfrm', 'CONSULTANTS', NULL, NULL, '2022-05-07 01:31:36', '2022-05-07 01:31:36', NULL, NULL, NULL, 0, 'Pending', 1),
(210, 'Tharun', 129, 'tharun345@yopmail.com', '$2y$10$wRKJ3xPg2AaOBsWgpw7vG.TGWjgF3MrxTmWHoONUCxvmk9YR2oMtq', 'SUBCONTRACTORS', 'yFLXlmo91esa0c9eLXwhW6bMKgocIBmfRsXMuRJh90yoHP1ydcttVHat8gVX', NULL, '2022-05-07 04:20:53', '2022-07-22 23:22:53', NULL, NULL, NULL, 0, 'Pending', 1),
(211, 'Archana', 130, 'archana3345@yopmail.com', '$2y$10$.0aLb7SSk4M3IpAhjDEqUed3pJSfsmPp5Bj27QpDDjrlwgOofY0cC', 'UNIVERSITY_USER', NULL, NULL, '2022-05-07 04:50:57', '2022-05-07 04:50:57', NULL, NULL, NULL, 0, 'Pending', 1),
(212, 'Anupama', 131, 'anupama345@yopmail.com', '$2y$10$9Jv1DOouXI/KZVkQXn7jA..WcnyDQ.W02C6NcNkKdp4btZSqfqZLa', 'EMPLOYER_ROLE', NULL, NULL, '2022-05-07 06:09:42', '2022-05-07 06:09:42', NULL, NULL, NULL, 0, 'Pending', 1),
(214, 'Ardra', 1652168405, 'ardra123@yopmail.com', '$2y$10$DbHzsu1StlZq2sRgKbw7EeUGkSIk2kPL6lAz2V4TxrXdPPz41JJR2', 'STUDENT', NULL, NULL, '2022-05-10 02:10:05', '2022-05-10 02:10:05', NULL, 207, NULL, 1, 'Pending', 1),
(216, 'Keerthy', 1652175520, 'keerthy123@yopmail.com', '$2y$10$lEk6XrMX4BWjXp4ebEUb3OXqiiqkESgUygpPwyyYWUUXYeOF89SL6', 'NURSE', NULL, '2022-09-02 02:49:27', '2022-05-10 04:08:40', '2022-05-10 04:08:40', NULL, 207, NULL, 1, 'Pending', 1),
(217, 'Anupama', 1652176015, 'anupama567@yopmail.com', '$2y$10$BLFmUADyBWwhpl4eqaX19.F.G7dWAHiKq4G4YScQRXGOnpst1.hiu', 'NURSE', NULL, NULL, '2022-05-10 04:16:55', '2022-05-10 04:16:55', NULL, 203, NULL, 1, 'Pending', 1),
(219, 'Athul', 133, 'athul345@yopmail.com', '$2y$10$LlP/.aAA4Ml48BLKNk0E6OPJlPt9PGwXsOvhdvmw4Ha6CnlyQSvny', 'MAIN_AGENTS', NULL, NULL, '2022-05-10 06:48:08', '2022-05-10 06:48:08', NULL, NULL, NULL, 0, 'Pending', 0),
(221, 'Sreeja', 135, 'sreeja345@yopmail.com', '$2y$10$0ccaBOjDInfvFRmUkNIWGeIAMkdwK8pE.DZQC7pvY6.8bcCTXuEfK', 'CONSULTANTS', NULL, NULL, '2022-05-12 08:18:04', '2022-05-12 08:18:04', NULL, NULL, NULL, 0, 'Pending', 1),
(222, 'Aswathy', 1652506080, 'aswathy543@yopmail.com', '$2y$10$vSBEth06Uk6rr4/ftsszK.UOqvHk9Wqx8twx0siAdumF.Fk2y7PXK', 'STUDENT', 'DVfNfZDzYiDMYMk8kGdXzWvdEHcEpr5tWyZF29Zz9wiBzYKiN1uq9CEV25si', NULL, '2022-05-13 23:58:00', '2022-05-27 03:05:49', NULL, 175, 178, 0, 'Approve', 1),
(224, 'Anagha', 1653304851, 'anagha567@yopmail.com', '$2y$10$pbB25UVMDRX5tpBKZA231Of0pX/pTUTsclBzA8H7t.CjHknrMTDRq', 'STUDENT', NULL, NULL, '2022-05-23 05:50:51', '2022-05-23 05:50:51', NULL, NULL, NULL, 1, 'Pending', 1),
(225, 'Aneesh', 1653305092, 'aneesh567@yopmail.com', '$2y$10$yvBszrhDljNOuBL2mwfz5uO.OXQmzvob4NihZtsmuETU2DENAsdoS', 'STUDENT', NULL, NULL, '2022-05-23 05:54:52', '2022-05-23 05:54:52', NULL, NULL, NULL, 1, 'Pending', 1),
(226, 'Anitha', 1653486251, 'anitha456@yopmail.com', '$2y$10$AL9TVQ8nEBWANFOvdNQjQOZRFgLGg5vkc8kfUzyBNw84Pm2aMoB4a', 'STUDENT', NULL, NULL, '2022-05-25 08:14:11', '2022-05-25 08:14:11', NULL, 175, NULL, 0, 'Processing', 1),
(227, 'Rihan', 1653486470, 'rihan567@yopmail.com', '$2y$10$qCZLjy8GXbU4LAhfbyE6e.invf4IcphgjoWt9w9xVUlBc1L7Dluvm', 'STUDENT', NULL, NULL, '2022-05-25 08:17:50', '2022-05-25 08:17:50', NULL, NULL, 178, 0, 'Pending', 1),
(228, 'Abhilash', 1653487353, 'abhilash456@yopmail.com', '$2y$10$A0JNoMI76NyQLqGTewBR.eO.gu3sV.6A/r8qpt88ehaMPVhBXKcM2', 'STUDENT', NULL, NULL, '2022-05-25 08:32:33', '2022-05-25 08:32:33', NULL, NULL, 178, 0, 'Pending', 1),
(229, 'Rahul', 1653487688, 'rahul789@yopmail.com', '$2y$10$wkavoWmpB66m42sxtvA19uvU1rSY62/jMxmgyjPm6hXIRNiKcpHdK', 'STUDENT', NULL, NULL, '2022-05-25 08:38:08', '2022-05-25 08:38:08', NULL, NULL, 178, 0, 'Pending', 1),
(230, 'Dhanya', 1653488365, 'dhanya345@yopmail.com', '$2y$10$DRA4IDcb3fIMgG/MZyDAm.coXx25/JybRR89ic.fIfHmiAgf8ChZ.', 'STUDENT', NULL, NULL, '2022-05-25 08:49:25', '2022-05-25 08:49:25', NULL, NULL, 178, 0, 'Pending', 1),
(231, 'Shivani', 1653488845, 'shivani678@yopmail.com', '$2y$10$Wzchih.rO.vyEHJA0ANTceprZwU.kkHt4FUpaUeRqEXZKP7BnF8k.', 'STUDENT', NULL, NULL, '2022-05-25 08:57:25', '2022-05-25 08:57:25', NULL, NULL, 178, 0, 'Pending', 1),
(235, 'Sithara', 1653746547, 'sithara345@yopmail.com', '$2y$10$KHTEyiCO97YJzgSkf1QCXOwe.u10fjO4d2KUql5tiwy4eufgl/pBy', 'NURSE', NULL, NULL, '2022-05-28 08:32:27', '2022-05-28 08:32:27', NULL, 203, NULL, 0, 'Pending', 1),
(236, 'Nithin', 136, 'nithin778@yopmail.com', '$2y$10$kmF.jcm6Qpv/l0QQ1QxgY.GUBRMAhpWdVsjEljH1U4AuVfUQN3Amm', 'MAIN_AGENTS', NULL, NULL, '2022-08-16 07:12:52', '2022-08-16 07:12:52', NULL, NULL, NULL, 0, 'Pending', 1),
(237, 'Krishna', 137, 'krishna777@yopmail.com', '$2y$10$hqGNNk/rzuinFXiLz.wGW.59Rx2751EhD2YcQ84BV2pBTWn0m9KAm', 'MAIN_AGENTS', NULL, NULL, '2022-08-16 07:55:04', '2022-08-16 07:55:04', NULL, NULL, NULL, 0, 'Pending', 1),
(238, 'Anil', 138, 'anil777@yopmail.com', '$2y$10$jqSdPcNa/g5IeIUzer62UOUsVkjaS/VhEEkAszo9.Uv4CTHbaFl9e', 'MAIN_AGENTS', NULL, NULL, '2022-08-16 08:02:31', '2022-08-16 08:02:31', NULL, NULL, NULL, 0, 'Pending', 1),
(239, 'Anila', 139, 'anila777@yopmail.com', '$2y$10$FnoPYrH.JqHwJNWy7S7CXuMkQpoFnjWo9juRBYcbJfBbCtr0h8/vy', 'MAIN_AGENTS', NULL, NULL, '2022-08-16 08:05:08', '2022-08-16 08:05:08', NULL, NULL, NULL, 0, 'Pending', 1),
(244, 'Deepak', 1660659089, 'deepak333@yopmail.com', '$2y$10$CLenCSaIfd0l.52HiFpwQeZ6ONT5RLvtSNOq7FAVoL5VQtptX/U/.', 'STUDENT', 'CFJcR7aBUVz2GDxhtG7YGDrAuieOl2JMy8FBYbCmV48A5ltGirkUlraxVLYU', NULL, '2022-08-16 08:41:29', '2022-08-16 09:17:06', NULL, NULL, NULL, 0, 'Pending', 1),
(246, 'Swathy', 1660659684, 'swathy555@yopmail.com', '$2y$10$YHyxnDwXwGB2jLE/h9EweuvHiQeTF8CzY.ampddmwV4SgOAf1/XFe', 'NURSE', 'mhepV2u7CLBUM5RdllmTzD9KDygh4zVC2RpXlqe78oPIDoWEvpaP4EmB8VCz', NULL, '2022-08-16 08:51:24', '2022-08-16 09:16:53', NULL, NULL, NULL, 0, 'Pending', 1),
(247, 'Sreeja', 144, 'sreeja333@yopmail.com', '$2y$10$U1Wi10Si64aGh6LRNE61Xu4/0zklGZexcWHpaSwSlAnQIlhZH0F8a', 'MAIN_AGENTS', NULL, NULL, '2022-08-16 09:23:57', '2022-08-16 09:23:57', NULL, NULL, NULL, 0, 'Pending', 1),
(248, 'Saranya', 145, 'saranya567@yopmail.com', '$2y$10$wRKJ3xPg2AaOBsWgpw7vG.TGWjgF3MrxTmWHoONUCxvmk9YR2oMtq', 'MAIN_AGENTS', 'OCJnOL0oXUvOx6d78ZOZkSuTUSC6gkiikCA05lPEVSoxUzTHjr2vZNmGbDbh', NULL, '2022-08-16 22:58:54', '2022-08-16 23:00:42', NULL, NULL, NULL, 0, 'Pending', 1),
(249, 'Akshara', 146, 'akshara567@yopmail.com', '$2y$10$wRKJ3xPg2AaOBsWgpw7vG.TGWjgF3MrxTmWHoONUCxvmk9YR2oMtq', 'MAIN_AGENTS', 'qOvVKdAO81nIoAK29XYNpNv9uFzlGk51oJP0TKc399pi4KjVZaywjps1Xro6', NULL, '2022-08-16 23:07:43', '2022-08-17 01:54:27', NULL, NULL, NULL, 0, 'Pending', 1),
(250, 'Sreethu', 147, 'sreethu567@yopmail.com', '$2y$10$wRKJ3xPg2AaOBsWgpw7vG.TGWjgF3MrxTmWHoONUCxvmk9YR2oMtq', 'MAIN_AGENTS', 'qPI2QkU3h2lfFxW7q2btqoubiakJVv5WzsVSt6lCgxiVnC1Q2CfQIDRTq05G', NULL, '2022-08-16 23:10:16', '2022-08-16 23:15:49', NULL, NULL, NULL, 0, 'Pending', 1),
(251, 'Hareesh', 1661776285, 'hareesh444@yopmail.com', '$2y$10$2MpuS20ED24xqKOcW8JrCuRklmKDowsAFTpzUgGoy4hseG9ceKhli', 'STUDENT', '57hibGDf1H7bnaEPqLeZkJ3AFueIB3muv3BTx3KWWP1ilM0d8a1s4xhD2VBZ', NULL, '2022-08-29 07:01:25', '2022-08-29 07:02:06', NULL, NULL, NULL, 0, 'Processing', 1),
(252, 'Sarath', 1661776393, 'sarath444@yopmail.com', '$2y$10$5CkvrPrgrEGuJGJOZm6JfObn/RqoRqMaAQ6BPjjefmsZd/nd6FHXu', 'NURSE', 'abPbYjxIQK2kx5XPeomyJ10dr2NhQg9QNuuKJakWa7GsMKeiz6AvH8epJd6D', NULL, '2022-08-29 07:03:14', '2022-08-29 07:03:56', 0, NULL, NULL, 0, 'Pending', 1),
(253, 'Karthik', 1661776493, 'karthik444@yopmail.com', '$2y$10$0zmIsb6td8i7plxS9fUqpeo75.bVuQH1OziyrofJ2ySZZYX7GO4qO', 'NURSE', 'hDgOr6EJqBN8Q8Lsi2sAGfS6esTD633AAOF5BdvwTzrpmq2kSiAiqnAqEpMk', NULL, '2022-08-29 07:04:53', '2022-08-29 07:09:48', 1, 203, NULL, 0, 'Pending', 1),
(254, 'Sameer', 148, 'sameer444@yopmail.com', '$2y$10$g6F8rDpc3YOw3yI1kqT31eBB1sixFYNfBcNzg5vjUncACELf/t0L6', 'MAIN_AGENTS', '7fIrktQoeqJocK99sFewHRzY2k2J9wec6xgcQqtQDR1qtscCyjGRtHw0VkIL', NULL, '2022-08-29 07:06:41', '2022-08-29 07:17:29', NULL, NULL, NULL, 0, 'Pending', 1),
(255, 'Vijay', 149, 'vijay444@yopmail.com', '$2y$10$Rll.GwDNNfLWPR29vyvTK.fiXalaHDP0R54UnlhUiT9nXm7Ihx0Gm', 'MAIN_AGENTS', 'qqwTbnJcjxJzx6QVSMPyzgmAoKEL7A8IH8NfDzaHTROthB7NCIdA31dvlXti', NULL, '2022-08-29 07:21:43', '2022-10-15 07:03:43', NULL, NULL, NULL, 0, 'Pending', 1),
(256, 'Anoop', 150, 'anoop789@yopmail.com', '$2y$10$KFSpA1GxkMy/dKSaFwydyewtsF/D1dCrVD2o7QXP3Lpc10dAAPKOq', 'SUBCONTRACTORS', NULL, NULL, '2022-09-05 23:38:01', '2022-09-05 23:38:01', NULL, NULL, NULL, 0, 'Pending', 1),
(257, 'Rajiv', 151, 'rajiv578@yopmail.com', '$2y$10$PEgzsdUbAo.F0gILwOD3dOibMF5Ue4qtsmoGG2MAUZy6m8FNmiRHu', 'SUBCONTRACTORS', NULL, NULL, '2022-09-05 23:40:33', '2022-09-05 23:40:33', NULL, NULL, NULL, 0, 'Pending', 1),
(258, 'Deepak', 152, 'deepak689@yopmail.com', '$2y$10$gWf0HifvWciS9alNcki6fuiQ7gFQTKq3wzb9W3vlLEMZ3F6LA/HSi', 'SUBCONTRACTORS', NULL, NULL, '2022-09-05 23:42:46', '2022-09-05 23:42:46', NULL, NULL, NULL, 0, 'Pending', 1),
(259, 'Ashwin', 153, 'ashwin578@yopmail.com', '$2y$10$UQ5W0lCQrQf89ZijckAYOutq5m9wpOGMsYh12a6iK9qJXUuoH/ZLu', 'UNIVERSITY_USER', NULL, NULL, '2022-09-05 23:44:00', '2022-09-05 23:44:00', NULL, NULL, NULL, 0, 'Pending', 1),
(260, 'Sitara', 1662441535, 'sitara467@yopmail.com', '$2y$10$AiLw11IjB3ZIv7qC/dtAWOpRfFD69K7Dr8ZFSLAOhSVCjPnoKzdmq', 'STUDENT', NULL, NULL, '2022-09-05 23:48:56', '2022-09-05 23:48:56', NULL, 175, 178, 0, 'Processing', 1),
(261, 'Vijay', 1662443520, 'vijay578@yopmail.com', '$2y$10$/iz.pin/jrdyfoPu0OdcRe82J4s017CeDwO/eAM.XgKYFS2V.u1xW', 'STUDENT', NULL, NULL, '2022-09-06 00:22:00', '2022-09-06 00:22:00', NULL, NULL, NULL, 0, 'Processing', 1),
(262, 'Amal', 1662532163, 'amal568@yopmail.com', '$2y$10$4kaVkJLkVF..FxV8lWXyJuQqZwqv.mkj2xeLqFt5WvRm2UETZNvXm', 'STUDENT', NULL, NULL, '2022-09-07 00:59:23', '2022-09-07 00:59:23', NULL, NULL, 178, 0, 'Pending', 1),
(263, 'Vineetha', 154, 'vineetha345@yopmail.com', '$2y$10$3z9gqkfgb8caCjJffbs9keGqUT53FTAxd/53jPH02dEN5mt.Ddr4G', 'CONSULTANTS', NULL, NULL, '2022-09-17 04:47:21', '2022-09-17 04:47:21', NULL, NULL, NULL, 0, 'Pending', 1),
(264, 'Sajin', 155, 'sajin345@yopmail.com', '$2y$10$xEn6dCr0rwrbBHw1egQEmeflE4tbwM.znfT9.a1YHf3plDotMUBc2', 'CONSULTANTS', NULL, NULL, '2022-09-17 04:47:55', '2022-09-17 04:47:55', NULL, NULL, NULL, 0, 'Pending', 1),
(266, 'Sarath', 1664198619, 'sarath789@yopmail.com', '$2y$10$qDk9ChzY9E7SL84lRw3RoOBI1CGm5PBTZBusBrIfnsfDU.kqPG4w6', 'STUDENT', 'XZa8H2hus7jvouIEtzaNVmh5oNy25s55B0jNQIhJLS8WAUkZrCGVTnFDIXJt', NULL, '2022-09-26 07:53:39', '2022-09-26 07:54:43', NULL, NULL, NULL, 0, 'Pending', 1),
(267, 'Hridya', 156, 'hridya567@yopmail.com', '$2y$10$aK2deYhg8nHDqXrMkIUA6uqQqIoM8zmh7Hbf2aS99ftTjxhclDKDa', 'MAIN_AGENTS', 'jFBeTrpQ8WYHcrsHdCWS5ul3g0YqsOBJjoho1h359AbLfECDCiLg1acFF9am', NULL, '2022-10-07 02:19:09', '2022-10-07 02:20:44', NULL, NULL, NULL, 0, 'Pending', 1),
(271, 'Syam', 1665132265, 'syam567@yopmail.com', '$2y$10$vaoFebe4aajLP8urx8QLT.gH9QICGiykk3kxlftiA3wwZYlW0/5uO', 'STUDENT', NULL, NULL, '2022-10-07 03:14:26', '2022-10-07 03:14:26', NULL, NULL, NULL, 1, 'Pending', 1),
(272, 'Riyad', 157, 'riyad567@yopmail.com', '$2y$10$Ckw2JcoJqg408UxOgNYumub8dhIReiKfZ9sCHP0Qm7Si5PO702kDC', 'MAIN_AGENTS', NULL, NULL, '2022-10-07 03:16:09', '2022-10-07 03:16:09', NULL, NULL, NULL, 0, 'Pending', 0),
(273, 'Revathy', 1665133220, 'revathy567@yopmail.com', '$2y$10$FOLQ8mFcTqpPFzRqMaXQP.wOKzK1JYhCW9PYAmDnetP7ruwz1JuIy', 'STUDENT', 'W10tXOn9Mef23eZPiLJPCmHjPZnnjWizCqeQQBLgzGV0OSCrY18HRbvolhRH', NULL, '2022-10-07 03:30:20', '2022-10-07 03:35:54', NULL, NULL, NULL, 0, 'Pending', 1),
(274, 'Riyas', 1665135662, 'riyas567@yopmail.com', '$2y$10$jCTRYKnbRFIU4w6nnzRrtOnlZdbAYd8diyNwSlH2Plq9Z.h8uG6Dy', 'STUDENT', NULL, NULL, '2022-10-07 04:11:02', '2022-10-07 04:11:02', NULL, NULL, NULL, 1, 'Pending', 1),
(276, 'Anitha', 1665136302, 'anitha567@yopmail.com', '$2y$10$OrtCB4vyJ0wZEkYykJnuuO9R9nJDKzRzN7TdEJp4rJ5T3M3HuMzLm', 'NURSE', 'ZRO4N4EJRHcfhSBs37uH719TFLnfarUy13RlribkGI10fkP3sGcVBZ2TlEL1', NULL, '2022-10-07 04:21:42', '2022-10-07 04:23:11', NULL, NULL, NULL, 0, 'Pending', 1),
(277, 'Kanika', 1665140924, 'kanika345@yopmail.com', '$2y$10$YIARgvi5w6Eq6LdVItvwfeh/QavcBkg1tDWkOOPl2h4etcMk7aYqy', 'STUDENT', NULL, NULL, '2022-10-07 05:38:44', '2022-10-07 05:38:44', NULL, NULL, NULL, 1, 'Pending', 1),
(278, 'Rejin', 1665312613, 'rejin456@yopmail.com', '$2y$10$LG.Q8CTfEkcvd.qZE1rxnuN850iSCwcVuX1NcneJEYCG8VAsswUO.', 'STUDENT', NULL, NULL, '2022-10-09 05:20:13', '2022-10-09 05:20:13', NULL, NULL, NULL, 1, 'Pending', 1),
(279, 'Ajmal', 1665312799, 'ajmal567@yopmail.com', '$2y$10$9jShhnURGfLl0J4Sc1P3EOPu.VzwwwbzvM1TC0UcPdJQuUJgSmoGC', 'STUDENT', NULL, NULL, '2022-10-09 05:23:20', '2022-10-09 05:23:20', NULL, NULL, NULL, 1, 'Pending', 1),
(280, 'Anandhu', 1665312987, 'anandhu456@yopmail.com', '$2y$10$s/N3ucafQki4pC1FBQsU1udCJ0FYtL9RDc36CofJOt7.w/qa.LPIe', 'STUDENT', NULL, NULL, '2022-10-09 05:26:27', '2022-10-09 05:26:27', NULL, NULL, NULL, 1, 'Pending', 1),
(281, 'Simi', 1665313595, 'simi567@yopmail.com', '$2y$10$8DAQL8P6tyLHhzDQa66qIeoegvtcMc47LKHT3k1ARTrurhGfl3Z8y', 'NURSE', NULL, NULL, '2022-10-09 05:36:35', '2022-10-09 05:36:35', NULL, NULL, NULL, 1, 'Pending', 1),
(282, 'Maya', 1665313659, 'maya456@yopmail.com', '$2y$10$QI6K8KotGcHhdx8TrUFtcOEKU.Lku/RMLivlV9jM42F7wO7Xic.L6', 'NURSE', NULL, NULL, '2022-10-09 05:37:39', '2022-10-09 05:37:39', NULL, NULL, NULL, 1, 'Pending', 1),
(283, 'Neeraja', 158, 'neeraja567@yopmail.com', '$2y$10$WhfM9Kc3P5WOpT1/JgKb9uRwqFodXo5h9GIpdiimDthhQQKAZYkja', 'MAIN_AGENTS', NULL, NULL, '2022-10-09 05:40:50', '2022-10-09 05:40:50', NULL, NULL, NULL, 0, 'Pending', 0),
(284, 'Saniya', 159, 'saniya345@yopmail.com', '$2y$10$rS3qpVCoUXkEyIfckCdo7e5rJPQTXYAUldBaxBHE9RvaHo3PIHd12', 'MAIN_AGENTS', NULL, NULL, '2022-10-09 06:00:29', '2022-10-09 06:00:29', NULL, NULL, NULL, 0, 'Pending', 0),
(285, 'Amjith', 160, 'amjith567@yopmail.com', '$2y$10$gxeh34IS./l4V/XiRpQZwO9IugOANqFeEzy2zndMBKQqAkcdYTaBW', 'MAIN_AGENTS', 'vGp2Z9xZj96vPFBOFTr3I79OokH2fjoE51T76TtZhNvZYUVJB27OJYKGr7X5', NULL, '2022-10-09 23:07:52', '2022-10-09 23:08:40', NULL, NULL, NULL, 0, 'Pending', 0),
(286, 'Sanya', 161, 'sanya456@yopmail.com', '$2y$10$Y0Nzt.XvkUUDHzZvYpqw9eXIDB4NcVfNUdFhbT4Lsbugm1Qs2fzmu', 'MAIN_AGENTS', NULL, NULL, '2022-10-09 23:17:47', '2022-10-09 23:17:47', NULL, NULL, NULL, 0, 'Pending', 0),
(287, 'Krishna', 162, 'krishna478@yopmail.com', '$2y$10$AKFTWU2pd3OC6CCe2qIauORH6U5H.zateLgx/Iue.u/8Ye2yOxlXO', 'MAIN_AGENTS', NULL, NULL, '2022-10-09 23:20:17', '2022-10-09 23:20:17', NULL, NULL, NULL, 0, 'Pending', 0),
(288, 'Anusha', 163, 'anusha678@yopmail.com', '$2y$10$Qxv4mz3HUxSP0USqdpb/r.hTlOLPEKKf5vZcZP.CMF/h7UPKxW3pW', 'MAIN_AGENTS', NULL, NULL, '2022-10-10 09:38:33', '2022-10-10 09:38:33', NULL, NULL, NULL, 0, 'Pending', 1),
(289, 'Keerthi', 164, 'keerthi678@yopmail.com', '$2y$10$lBXyVaSt/qeH0JeEgqxOzuOxm4YTc0E8yQojw1LveYFjQLJDwgQ.m', 'MAIN_AGENTS', 'v4qqB0KoBgxfVRZt0J2hoeT6xPiUVrWNNbHvYfBaZ2DQcOQelfdWmaAyC7vU', NULL, '2022-10-10 09:41:00', '2022-10-10 10:22:03', NULL, NULL, NULL, 0, 'Pending', 1),
(290, 'Parvathy', 1665415190, 'parvathy678@yopmail.com', '$2y$10$K5kVZr25eS8B8v9yXHnIf.fgxGjmY.atWPeitr5YsvH./vZ7HvUgu', 'STUDENT', 'wib2vNvKNOC2PDnu5hYKP8VxjGtPQhLWnRXJYSAZiTAilIth9t56norbqcej', NULL, '2022-10-10 09:49:50', '2022-10-10 10:37:53', NULL, NULL, NULL, 0, 'Pending', 1),
(291, 'Krithi', 1665461815, 'krithi678@yopmail.com', '$2y$10$LoB/xfpe/JqYt6Bm6wYVu.ZL3OnPVLsgTeLM1..Rm2U64k9isn0Jy', 'STUDENT', 'IIrKVdWTxbHgtNr924czdFZSaOdiIVl3Ysjh7jQyZooCmxi6h8Tne7KGrpZo', NULL, '2022-10-10 22:46:56', '2022-10-10 22:49:05', NULL, NULL, NULL, 0, 'Pending', 1),
(292, 'Sreeja', 1665463071, 'sreeja456@yopmail.com', '$2y$10$7mQUcHaZ2KvNUHFMsknVluphv3V/HyQ2LVxu7cpXpq8MRIBqC9Ge2', 'STUDENT', 'o0KXFrhpAJ41y9vXQLUqHJ5G1tkbRue3knqSVZBcmOxcN98Xnc9sueBeGNX8', NULL, '2022-10-10 23:07:51', '2022-10-10 23:08:45', NULL, NULL, NULL, 0, 'Pending', 1),
(293, 'Soumya', 1665469758, 'soumya678@yopmail.com', '$2y$10$pkTqEP7xDI2Q3p6LKh2V6.S81ZBZMqQvQbRWPlFd.QbYKGEaiU.Qu', 'NURSE', '3957Z9uCjNM62anzrbo5mbtvhKGmNFAdOnAFjpXBr5i3EJ7x85rCvzCmZ7uL', NULL, '2022-10-11 00:59:18', '2022-10-11 01:33:20', NULL, NULL, NULL, 0, 'Pending', 1),
(294, 'Seena', 1665472413, 'seena678@yopmail.com', '$2y$10$kCR6cV765Xys7rfSXyIrj.4RliaRy4dyXRc/HhmDXWL7QOW25WzRS', 'NURSE', '2ngP7KHkoYrqstdlJ8eT4eyBL7VXFQGOmUyXNcPwQ3UZd72i6x9UZFCXT9nP', NULL, '2022-10-11 01:43:33', '2022-10-11 01:44:22', NULL, NULL, NULL, 0, 'Pending', 1),
(295, 'Shivani', 165, 'shivani777@yopmail.com', '$2y$10$5uQF1l4Dfb.ELKa6RF9uQe0PhHSx3Q1qAonDGTeS1w0EkrlHcwnwG', 'CONSULTANTS', 'cXN7JTeA8WU98kVY42YsqhiBuusMDWgAi1ynizoLEYw35OO3TrIJphBuUYth', NULL, '2022-10-14 01:24:50', '2022-10-14 01:26:45', NULL, NULL, NULL, 0, 'Pending', 1),
(296, 'Smitha', 166, 'smitha455@yopmail.com', '$2y$10$V0U9Pj/sF.LvIMXF6uuGoejOGZOp5au46RqPR8XgmDkfpb/r1c1za', 'CONSULTANTS', NULL, NULL, '2022-10-14 01:37:46', '2022-10-14 01:37:46', NULL, NULL, NULL, 0, 'Pending', 1),
(297, 'Anjitha', 167, 'anjitha257@yopmail.com', '$2y$10$CbN9C2bZq3hZ2S5dDoHnUer9yBjhQli3XOTON6dFRo1sHR3GWM4.W', 'MAIN_AGENTS', NULL, NULL, '2022-10-14 01:38:50', '2022-10-14 01:38:50', NULL, NULL, NULL, 0, 'Pending', 1),
(298, 'Adarsh', 1665731715, 'adarsh545@yopmail.com', '$2y$10$zyFKsDC27vu1klw0Zbx.XOW973siqFX/gtNFRghFteSpvNox883HS', 'STUDENT', 'lTb1HDcRmiDGU9aZSM1PYwPnD5240pLF4lbbv5KYDxoezovNhXNRj3FvE3kv', NULL, '2022-10-14 01:45:15', '2022-10-15 09:33:47', NULL, NULL, NULL, 0, 'Pending', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_time_track`
--

CREATE TABLE `user_time_track` (
  `id` int(11) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `employee_id` int(11) UNSIGNED NOT NULL,
  `track_date` varchar(10) DEFAULT NULL,
  `from_time` varchar(10) DEFAULT NULL,
  `to_time` varchar(10) DEFAULT NULL,
  `reason` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `verify_users`
--

CREATE TABLE `verify_users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `verify_users`
--

INSERT INTO `verify_users` (`id`, `user_id`, `token`, `created_at`, `updated_at`) VALUES
(1, 99, '32e4f29f408262fd099138af61d8f11e42f5f4d3', NULL, NULL),
(2, 100, '4045b803a6dadecf9f78a7ca20b9f6f5741f6a9c', NULL, NULL),
(3, 105, '86a99e6681259ee5924396e7d73de8459407aa20', NULL, NULL),
(4, 111, '786642912fabbc59e56d1dbea38251fe025ffd54', NULL, NULL),
(5, 112, 'c0c1ea73ea174fda96536866c9cf87f41f0b32c9', NULL, NULL),
(6, 142, '6307342781483859ea9f38320003ffb02a902a62', NULL, NULL),
(7, 144, 'b0599859c41a764c6e58182f3f82da144bfa93a9', NULL, NULL),
(8, 164, '2ffa6cef6477c1530642a037fe0ff465804d3db9', NULL, NULL),
(9, 213, '1b2c310f8e92551d36fa426b997453b28db57641', NULL, NULL),
(10, 214, 'dd4001b07f0718c3d6a5ce4b4ae895d709210683', NULL, NULL),
(11, 215, '8da274c68f20838c4d237c189e25ac332dacf268', NULL, NULL),
(12, 216, '612263f91ecd479abde10655a436b186cbb62547', NULL, NULL),
(13, 217, 'b0754284b6192a8a2ffc55839d242e7f9d503d30', NULL, NULL),
(14, 219, '6e5f7925ef10f609f2b9cc91a9efffab55112663', NULL, NULL),
(15, 223, '3d085d8e9e7f144d4a9f893ed1bc486e1dfb51e8', NULL, NULL),
(16, 224, '2586ac8dcd3ffd8df9b57f9edae37b47da35fbdf', NULL, NULL),
(17, 225, '7f0d30d1fed040c481312938ae672813bbdf515a', NULL, NULL),
(18, 236, '84c934c2d29ab71aca528ebcb4896762aa9e4f83', NULL, NULL),
(19, 237, 'c2876676a860a740bc8856a2ee84ca9dfafda265', NULL, NULL),
(20, 238, 'c34e49ff569c1e6a0fe5cffe74fcd47c7e0e93fd', NULL, NULL),
(21, 239, '6a1dc0eb64e067ec4e606deae08bff5cc087b77e', NULL, NULL),
(22, 240, '747528fa39c6b3956c1c806ddb0cb08a2987e6d9', NULL, NULL),
(23, 241, '54bb45033cc27ce3d7c19c678f53fb7fd68bb818', NULL, NULL),
(24, 242, '1f766556f6cc27896dcfc02f0b24358c4e0d3491', NULL, NULL),
(25, 243, 'c13473fffc14359d00ef307f9047b6ffe3c670b6', NULL, NULL),
(26, 244, 'b079d177b3cf31cd1137dbd778cd9808327e9985', NULL, NULL),
(27, 245, 'd17bad9bcb751741ceca3ab3dee8566a7f7d4c40', NULL, NULL),
(28, 246, '10dc0099aca2ec2dce77f2af6034f7ed00e008a1', NULL, NULL),
(29, 251, 'afe3944e35cae9c84f89f5941d1d9ea30f4d7f02', NULL, NULL),
(30, 252, '4517402277cb72714d0a1824793e7fa673a63618', NULL, NULL),
(31, 253, 'f59a553d1af1d75aa104987b303130b9920ccf4d', NULL, NULL),
(32, 254, '74ab5b988599524223a25a05f0bd3d44ae5a4e78', NULL, NULL),
(33, 255, 'ade8bc43f7754137229bc4bbdd6d8f18be4a7379', NULL, NULL),
(34, 265, '44d50d032fd35978c45e1f2952d2fa8930c26846', NULL, NULL),
(35, 266, '65b79ff6fa23652680df1056343d9fbb7d3d14d8', NULL, NULL),
(36, 267, '435ac97cf29d1a77a1fc076a6a7ee3be46ec10e7', NULL, NULL),
(37, 268, '1f402e76013748454ba400541a206f850b6d42cc', NULL, NULL),
(38, 269, '09ad3b60fee9bbfa3110f2df7af7cee57b01c46e', NULL, NULL),
(39, 270, '0a367debc361a8db99f6404c47f4a94f3876ebdf', NULL, NULL),
(40, 271, '7064855c9f1738d7e5276690414077458aa6d3b6', NULL, NULL),
(41, 272, '0fd1ee14e8d4f005ea0ed23c0d67b7cf691f8bba', NULL, NULL),
(42, 273, 'dcfb0adb5e2a4e8c79bc4c9eb65da068d4f7213f', NULL, NULL),
(43, 274, '528b3646a1020b59c1c52e3f8cd6a65a576c61b4', NULL, NULL),
(44, 275, 'f57c1c40f246d2e53bca91f4a2dc8f1364447401', NULL, NULL),
(45, 276, '408f5a4e3f4dc462101e4339add136dece53cff1', NULL, NULL),
(46, 277, '41bc053a70d69f366edc6eca0fd442759004bd12', NULL, NULL),
(47, 278, 'f57536cc361fe42224a800efba58bc0584862f85', NULL, NULL),
(48, 279, 'b79480c4b6b49a15ede8add15e419c1574e30f59', NULL, NULL),
(49, 280, '4d2e37f55d8185b551b3212093ccfd148c83bff7', NULL, NULL),
(50, 281, 'c0de22a56335844f30a5e13075a19aac5b7f41ba', NULL, NULL),
(51, 282, '71bfdc10c41844c5f261a24dd3a38c0b0940c340', NULL, NULL),
(52, 283, '2bf4ee0b178f35d14db449ac9899a54af98b8db4', NULL, NULL),
(53, 284, '26709d65c9a5dae2333caead8c62280dc7bfe9f3', NULL, NULL),
(54, 285, '34041670dd8d3f1ec0734d54f83e4b9dc25c37a2', NULL, NULL),
(55, 286, '8ec1201da867e92f0814942625dd2c23c4d3ce1c', NULL, NULL),
(56, 287, 'f098e614a61b06a03ece8017606c1a730a693243', NULL, NULL),
(57, 288, 'fca6cc4871a232fedc780525ea4bbc1dd2f6d3dd', NULL, NULL),
(58, 289, '93e00c6c134975974ff67925c607f6cb39c125d7', NULL, NULL),
(59, 290, '151e18954ccf6ea73de3ff5e087f0e7ef3850c76', NULL, NULL),
(60, 291, '9e4e6d5a62e59d56c800d66433e47ddcbcd4d737', NULL, NULL),
(61, 292, 'e876affd931d8304b4af2248517256c30484642d', NULL, NULL),
(62, 293, '22aa536279d0c48f610d6b5d6569e354b965d754', NULL, NULL),
(63, 294, '2227b906a5e13b694cecf11d909867861b392329', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applynow`
--
ALTER TABLE `applynow`
  ADD PRIMARY KEY (`apply_id`);

--
-- Indexes for table `backups`
--
ALTER TABLE `backups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `backups_name_unique` (`name`),
  ADD UNIQUE KEY `backups_file_name_unique` (`file_name`);

--
-- Indexes for table `campus`
--
ALTER TABLE `campus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_courses_applied`
--
ALTER TABLE `data_courses_applied`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_courses_applied_new`
--
ALTER TABLE `data_courses_applied_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_dependant_det`
--
ALTER TABLE `data_dependant_det`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_employement_history`
--
ALTER TABLE `data_employement_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_eng_lang_proficiency`
--
ALTER TABLE `data_eng_lang_proficiency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_follow_up`
--
ALTER TABLE `data_follow_up`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_further_details`
--
ALTER TABLE `data_further_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_immigration_history`
--
ALTER TABLE `data_immigration_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_log`
--
ALTER TABLE `data_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_parent_det`
--
ALTER TABLE `data_parent_det`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_personal_information`
--
ALTER TABLE `data_personal_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_personal_stat`
--
ALTER TABLE `data_personal_stat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_school`
--
ALTER TABLE `data_school`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_student_lead`
--
ALTER TABLE `data_student_lead`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `departments_name_unique` (`name`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_email_unique` (`email`),
  ADD KEY `employees_dept_foreign` (`dept`);

--
-- Indexes for table `form_data`
--
ALTER TABLE `form_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `la_configs`
--
ALTER TABLE `la_configs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `la_menus`
--
ALTER TABLE `la_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mail_settings`
--
ALTER TABLE `mail_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from_user_relation` (`from_user`),
  ADD KEY `to_user_relation` (`to_user`);

--
-- Indexes for table `manage_courses`
--
ALTER TABLE `manage_courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_fields`
--
ALTER TABLE `module_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_fields_module_foreign` (`module`),
  ADD KEY `module_fields_field_type_foreign` (`field_type`);

--
-- Indexes for table `module_field_types`
--
ALTER TABLE `module_field_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nurse_dependant_det`
--
ALTER TABLE `nurse_dependant_det`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nurse_employement_history`
--
ALTER TABLE `nurse_employement_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nurse_employer_details`
--
ALTER TABLE `nurse_employer_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nurse_english_language`
--
ALTER TABLE `nurse_english_language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nurse_feedback`
--
ALTER TABLE `nurse_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nurse_joining`
--
ALTER TABLE `nurse_joining`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nurse_nmc`
--
ALTER TABLE `nurse_nmc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nurse_payments`
--
ALTER TABLE `nurse_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nurse_personal_information`
--
ALTER TABLE `nurse_personal_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nurse_preferences`
--
ALTER TABLE `nurse_preferences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nurse_qualifications`
--
ALTER TABLE `nurse_qualifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nurse_visa_imigration`
--
ALTER TABLE `nurse_visa_imigration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `organizations_name_unique` (`name`),
  ADD UNIQUE KEY `organizations_email_unique` (`email`),
  ADD KEY `organizations_assigned_to_foreign` (`assigned_to`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`),
  ADD KEY `roles_parent_foreign` (`parent`),
  ADD KEY `roles_dept_foreign` (`dept`);

--
-- Indexes for table `role_module`
--
ALTER TABLE `role_module`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_module_role_id_foreign` (`role_id`),
  ADD KEY `role_module_module_id_foreign` (`module_id`);

--
-- Indexes for table `role_module_fields`
--
ALTER TABLE `role_module_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_module_fields_role_id_foreign` (`role_id`),
  ADD KEY `role_module_fields_field_id_foreign` (`field_id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`),
  ADD KEY `employee_id` (`employee_id`);

--
-- Indexes for table `staff_agents`
--
ALTER TABLE `staff_agents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stream`
--
ALTER TABLE `stream`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `studentstatus`
--
ALTER TABLE `studentstatus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `syllabus`
--
ALTER TABLE `syllabus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `time_tracking_settings`
--
ALTER TABLE `time_tracking_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `universities`
--
ALTER TABLE `universities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uploads_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_time_track`
--
ALTER TABLE `user_time_track`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_relation` (`user_id`),
  ADD KEY `employee_relation` (`employee_id`);

--
-- Indexes for table `verify_users`
--
ALTER TABLE `verify_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applynow`
--
ALTER TABLE `applynow`
  MODIFY `apply_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `backups`
--
ALTER TABLE `backups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `campus`
--
ALTER TABLE `campus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=210;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `data_courses_applied`
--
ALTER TABLE `data_courses_applied`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `data_courses_applied_new`
--
ALTER TABLE `data_courses_applied_new`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_dependant_det`
--
ALTER TABLE `data_dependant_det`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `data_employement_history`
--
ALTER TABLE `data_employement_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `data_eng_lang_proficiency`
--
ALTER TABLE `data_eng_lang_proficiency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `data_follow_up`
--
ALTER TABLE `data_follow_up`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_further_details`
--
ALTER TABLE `data_further_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `data_immigration_history`
--
ALTER TABLE `data_immigration_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `data_log`
--
ALTER TABLE `data_log`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `data_parent_det`
--
ALTER TABLE `data_parent_det`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `data_personal_information`
--
ALTER TABLE `data_personal_information`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `data_personal_stat`
--
ALTER TABLE `data_personal_stat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `data_school`
--
ALTER TABLE `data_school`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `data_student_lead`
--
ALTER TABLE `data_student_lead`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;

--
-- AUTO_INCREMENT for table `form_data`
--
ALTER TABLE `form_data`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `la_configs`
--
ALTER TABLE `la_configs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `la_menus`
--
ALTER TABLE `la_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `mail_settings`
--
ALTER TABLE `mail_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `manage_courses`
--
ALTER TABLE `manage_courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `module_fields`
--
ALTER TABLE `module_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `module_field_types`
--
ALTER TABLE `module_field_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT for table `nurse_dependant_det`
--
ALTER TABLE `nurse_dependant_det`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `nurse_employement_history`
--
ALTER TABLE `nurse_employement_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `nurse_employer_details`
--
ALTER TABLE `nurse_employer_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `nurse_english_language`
--
ALTER TABLE `nurse_english_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `nurse_feedback`
--
ALTER TABLE `nurse_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `nurse_joining`
--
ALTER TABLE `nurse_joining`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `nurse_nmc`
--
ALTER TABLE `nurse_nmc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `nurse_payments`
--
ALTER TABLE `nurse_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `nurse_personal_information`
--
ALTER TABLE `nurse_personal_information`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `nurse_preferences`
--
ALTER TABLE `nurse_preferences`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `nurse_qualifications`
--
ALTER TABLE `nurse_qualifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `nurse_visa_imigration`
--
ALTER TABLE `nurse_visa_imigration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `role_module`
--
ALTER TABLE `role_module`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `role_module_fields`
--
ALTER TABLE `role_module_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=448;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=249;

--
-- AUTO_INCREMENT for table `staff_agents`
--
ALTER TABLE `staff_agents`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `stream`
--
ALTER TABLE `stream`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `studentstatus`
--
ALTER TABLE `studentstatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `syllabus`
--
ALTER TABLE `syllabus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `time_tracking_settings`
--
ALTER TABLE `time_tracking_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `universities`
--
ALTER TABLE `universities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=721;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=299;

--
-- AUTO_INCREMENT for table `user_time_track`
--
ALTER TABLE `user_time_track`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `verify_users`
--
ALTER TABLE `verify_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_dept_foreign` FOREIGN KEY (`dept`) REFERENCES `departments` (`id`);

--
-- Constraints for table `mail_settings`
--
ALTER TABLE `mail_settings`
  ADD CONSTRAINT `from_user_relation` FOREIGN KEY (`from_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `to_user_relation` FOREIGN KEY (`to_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `module_fields`
--
ALTER TABLE `module_fields`
  ADD CONSTRAINT `module_fields_field_type_foreign` FOREIGN KEY (`field_type`) REFERENCES `module_field_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `module_fields_module_foreign` FOREIGN KEY (`module`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `organizations`
--
ALTER TABLE `organizations`
  ADD CONSTRAINT `organizations_assigned_to_foreign` FOREIGN KEY (`assigned_to`) REFERENCES `employees` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_dept_foreign` FOREIGN KEY (`dept`) REFERENCES `departments` (`id`),
  ADD CONSTRAINT `roles_parent_foreign` FOREIGN KEY (`parent`) REFERENCES `roles` (`id`);

--
-- Constraints for table `role_module`
--
ALTER TABLE `role_module`
  ADD CONSTRAINT `role_module_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_module_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_module_fields`
--
ALTER TABLE `role_module_fields`
  ADD CONSTRAINT `role_module_fields_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `module_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_module_fields_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `uploads`
--
ALTER TABLE `uploads`
  ADD CONSTRAINT `uploads_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_time_track`
--
ALTER TABLE `user_time_track`
  ADD CONSTRAINT `employee_relation` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_relation` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
