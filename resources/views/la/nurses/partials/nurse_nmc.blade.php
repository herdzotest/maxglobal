<div id="tab_7" class="tab-pane " >
        
<div class="content-head">
                                                <h4>NMC Registration</h4>
                                                <!-- <div class="id">Nurse ID : <b>#12675</b></div> -->
                                </div>
                                {!! Form::open(['action' => 'LA\NursesController@store', 'id' => '']) !!}
               
                                <div class="pane">

								<div class="row">
										<!--
                                        <div class=" col s6 ">
                                                <div class="input-field">
														<select name="employer" id="employer">
															<option value="">Select</option>
                                                            <option value="Agent1">Trained in the UK</option>
                                                            <option value="Agent2">Trained in the EU/EEE</option>
                                                            <option value="Not Applicable">Trained outside the EU/EEE</option>
														</select>
														<label for="employer">Register as Nurse / Mid Wife</label>
                                                </div>


                                        </div>
										-->
                                        <div class=" col s6 ">
                                        <div class="input-field">
                                                        <select name="nmc_appl_status" id="nmc_appl_status" class="formSelect">
															<option value="">Select</option>
                                                            <option value="IELTS-OET">IELTS/OET</option>
                                                            <option value="English-speaking">From majority English speaking country</option>
                                                            <option value="invitation">Letter of invitation</option>
															<option value="CBT-Completed">CBT Completed</option>
															<option value="CBT-Passed">CBT Passed</option>
															<option value="Not-Started">Not Yet Started</option>
														</select>												
													<label for="nmc_appl_status">NMC Registration Status </label>
											</div>
                                        </div>
                                </div>
								<h4 class="sub_title">Supporting declaration about your health from your medical practitioner or occupation health</h4>

							<div class="row">
								<div class=" col s6 ipfield">
										<div class="input-field">
														<input name="declaration_name" id="declaration_name" type="text">
														<label for="declaration_name">Name</label>
										   
										</div>
								</div>
								<div class=" col s6 ipfield">
                                        <div class="input-field">
                                                        <input name="declaration_org" id="declaration_org"  type="text">
                                                        <label for="declaration_org">Organization</label>
                                           
                                        </div>
                                </div>

							</div>
							<div class="row">
								<div class=" col s6 ipfield">
										<div class="input-field">
														<input name="declaration_addr" id="declaration_addr" type="text">
														<label for="declaration_addr">Address</label>
										   
										</div>
								</div>
								<div class=" col s6 ipfield">
                                        <div class="input-field">
                                                        <input name="declaration_contact" id="declaration_contact"  type="text">
                                                        <label for="declaration_contact">Contact Number</label>
                                           
                                        </div>
                                </div>

							</div>

							<div class="row">
								<div class=" col s6 ipfield">
										<div class="input-field">
														<input name="declaration_email" id="declaration_email" type="email">
														<label for="declaration_email">Email Id</label>
										   
										</div>
								</div>
								<!--
								<div class=" col s6 ipfield">
                                        <div class="input-field">
                                                        <input name="pc_dbs_certificate" id="pc_dbs_certificate"  type="text">
                                                        <label for="pc_dbs_certificate">Police Clearance Certificates / DBS certificate</label>
                                           
                                        </div>
                                </div>
								-->

							</div>

							<h4 class="sub_title">Pcc Scope of Practice</h4><br>

							<div class="row">
							<div class="input-field col s6">
								<select name="scope_country" id="scope_country" class="searchSelect">
								<option value=""> Please Select </option>
								<?php
								foreach($countries as $countriesData) {
								echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
								}
								?>
								</select>							   
							   <label for="" style="margin-top: -40px;font-size: 13px;">Country</label>
							</div>	
								<div class=" col s6 ipfield">
                                        <div class="input-field">
                                                        <input name="scope_council" id="scope_council"  type="text">
                                                        <label for="scope_council">Council</label>
                                           
                                        </div>
                                </div>

							</div>



							<div class="row">
										<div class="col s6">
												<label>File Upload</label>
												<div class="file-field input-field">


																<div class="btn7">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
																<div class="file-path-wrapper">
																		<input class="file-path validate" name="scope_file_upload"  type="text">
																	  

																</div>
												</div>
											  
												<div class="uploaded-file" id="scope_file_upload" data-type="file"></div>
										</div>

							</div>



                                        <div class="row">
                                                <div class=" col s6 ">
                                                    <p>Have You Received Letter of invitation?</p>
							<p>
								<label>
									<input name="letter_invitation" type="radio" value="1" />
										<span>Yes</span>
						                </label>
								<label>
									<input name="letter_invitation" type="radio"  value="0" checked/>
										<span>No</span>
									</label>
							</p>
                                                                   
                                                </div>
        

												<div class="input-field col s6 invitation-wrapper hide">

													<input type="text" id="cbt_recieved_date" name="cbt_recieved_date" class="datepicker">
							   						<label for="cbt_recieved_date">Received Date</label>
												</div>

                                         </div> 

										<div class="row invitation-wrapper hide">


						



                                                <div class="input-field col s6 ">

													<input type="text" id="cbt_attempt_date" name="cbt_attempt_date" class="datepicker">
							   						<label for="cbt_attempt_date">CBT Attempt Date</label>
                                                </div>

												<div class="col s6">
													
												
													<div class="input-field">
                                                        <select name="cbt_attempt_status" id="cbt_attempt_status" class="formSelect">
															<option value="">Select</option>
                                                            <option value="Not-Cleared">Not Cleared</option>
															<option value="Cleared">Cleared</option>
														</select>												
													<label for="cbt_attempt_status">CBT Attempt Status</label>
													</div>

												</div>

                                        </div>

										<div class="row invitation-wrapper hide">
                                                <div class="input-field col s6 ">

													<input type="text" id="cbt_passed_date" name="cbt_passed_date" class="datepicker">
							   						<label for="cbt_passed_date">CBT Passed Date</label>
                                                </div>

												<div class="col s6">
													
												
													<div class="input-field">
                                                        <select name="cbt_attempts" id="cbt_attempts" class="formSelect">
															<option value="">Select</option>
                                                            <option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
														</select>												
													<label for="cbt_attempts">CBT Number of Attempts</label>
													</div>

												</div>

                                        </div>
										<div class="row">
                                                <div class=" col s6 invitation-wrapper hide">

												<label>CBT Score Upload</label>
												<div class="file-field input-field">

																<div class="btn7">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
																<div class="file-path-wrapper">
																		<input class="file-path validate" name="cbt_upload"  type="text">
																	  

																</div>
												</div>
											  
												<div class="uploaded-file" id="cbt_upload" data-type="file"></div>

													
                                                </div>


        

												<div class=" input-field col s6">
												
													<input type="text" id="cbt_validity" name="cbt_validity" class="datepicker">
							   						<label for="cbt_validity">CBT validity</label>

												</div>

                                        </div>
										<div class="row">
                                                <div class="input-field col s6 ">

													<input type="text" id="nmc_appl_date" name="nmc_appl_date" class="datepicker">
							   						<label for="nmc_appl_date">NMC Full Application Submission Date </label>

													
                                                </div>

												<div class="  col s6">
												
												<label>NMC status screenshot/decision letter File Upload </label>
												<div class="file-field input-field">

																<div class="btn7">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
																<div class="file-path-wrapper">
																		<input class="file-path validate" name="nmc_file_upload"  type="text">
																	  

																</div>
												</div>
											  
												<div class="uploaded-file" id="nmc_file_upload" data-type="file"></div>

													

												</div>

                                        </div>

										<div class="row">
                                                <div class=" col s6 ">

												<label>Birth Certificate File Upload</label>
												<div class="file-field input-field">

																<div class="btn7">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
																<div class="file-path-wrapper">
																		<input class="file-path validate" name="birth_cert_upload"  type="text">
																	  

																</div>
												</div>
											  
												<div class="uploaded-file" id="birth_cert_upload" data-type="file"></div>

													
                                                </div>

												<div class=" input-field col s6">
												
													

												</div>

                                        </div>
									
                                </div>
								
                                <div class="controls">
                                                <a class="mgbtn2">Back</a>
                                                <input id="form_name" type="hidden" class="validate" name="type_of" value="nmc">
                                                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                                                {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
                        

</div>

                