<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Google fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Outlined" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />

  <!-- Stylesheets -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/vendors.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/main.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/custom.css">

  <title>Course Finder</title>
  <link rel="icon" type="image/x-icon" href="la-assets/coursefinder/img/coursesCards/icons/zodocs_fav.png">
</head>

<body class="preloader-visible" data-barba="wrapper">

  <!-- preloader start -->
  <div class="preloader js-preloader">
    <div class="preloader__bg"></div>
  </div>
  <!-- preloader end -->

  <!-- barba container start -->
  <div class="barba-container" data-barba="container">


    <main class="main-content  ">

      <header data-anim="fade" data-add-bg="bg-white" class="header -type-5 js-header">

        <div class="d-flex items-center bg-white py-10 border-bottom-light">
          <div class="header__container">
            <div class="row y-gap-5 justify-between items-center">
              <div class="col-auto">
                <div class="d-flex x-gap-40 y-gap-10 items-center">
                  <div class="d-flex items-center text-dark-1 md:d-none">
                    <div class="fa fa-phone mr-10"></div>
                    <div class="text-13 lh-1">+91 9745 746 250</div>
                  </div>
                  <div class="d-flex items-center text-dark-1">
                    <div class="fa fa-envelope mr-10"></div>
                    <div class="text-13 lh-1">inbox@labgex.com</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div class="header__container py-10">
          <div class="row justify-between items-center">

            <div class="col-auto">
              <div class="header-left d-flex items-center">

                <div class="header__logo ">
                  <a data-barba href="<?php echo url('/coursefinder'); ?>">
                    <img src="la-assets/coursefinder/img/general/logo1.png" alt="logo">
                  </a>
                </div>

                <div class="header-menu js-mobile-menu-toggle ">
                  <div class="header-menu__content">
                    <div class="mobile-bg js-mobile-bg"></div>

                    <div class="d-none xl:d-flex items-center px-20 py-20 border-bottom-light">
                      <?php 
                      if(isset(Auth::user()->type)) { ?>
                        <a href="<?php echo url('/logout'); ?>" class="text-dark-1">Log out</a>
                        <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="text-dark-1 ml-30">My Account</a>
                      <?php } else { ?>
                        <a href="<?php echo url('/userlogin'); ?>" class="text-dark-1">Log in</a>
                        <a href="<?php echo url('/signup'); ?>" class="text-dark-1 ml-30">Sign up</a>
                      <?php } ?>
                    </div>

                    <div class="menu js-navList">
                      <ul class="menu__nav text-dark-1 ml-50 xl:ml-30 -is-active">
                        <li class="menu-item-has-children">
                      <a data-barba href="#">Home</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Home</a>
                        </li>
                        <li><a href="<?php echo url('/coursefinder'); ?>">Home 1</a></li>
                        <li><a href="<?php echo url('/coursefinder2'); ?>">Home 2</a></li>
                        <li><a href="<?php echo url('/coursefinder3'); ?>">Home 3</a></li>
                        <li><a href="<?php echo url('/coursefinder4'); ?>">Home 4</a></li>
                        <li><a href="<?php echo url('/coursefinder5'); ?>">Home 5</a></li>
                        <li><a href="<?php echo url('/coursefinder6'); ?>">Home 6</a></li>
                        <li><a href="<?php echo url('/coursefinder7'); ?>">Home 7</a></li>
                        <li><a href="<?php echo url('/coursefinder8'); ?>">Home 8</a></li>
                        <li><a href="<?php echo url('/coursefinder9'); ?>">Home 9</a></li>
                        <li><a href="<?php echo url('/coursefinder10'); ?>">Home 10</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/aboutus'); ?>">
                        About Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li class="menu-item-has-children">
                      <a data-barba href="#">Courses</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Courses</a>
                        </li>
                        <li><a href="<?php echo url('/coursesearch'); ?>">Course 1</a></li>
                        <li><a href="<?php echo url('/coursesearch2'); ?>">Course 2</a></li>
                        <li><a href="<?php echo url('/coursesearch3'); ?>">Course 3</a></li>
                        <li><a href="<?php echo url('/coursesearch4'); ?>">Course 4</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/blog'); ?>">
                        Blog <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/topcourses'); ?>">
                        Top Courses <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/contactus'); ?>">
                        Contact Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>
                      </ul>
                    </div>
                  </div>

                  <div class="header-menu-close" data-el-toggle=".js-mobile-menu-toggle">
                    <div class="size-40 d-flex items-center justify-center rounded-full bg-white">
                      <i class="fa fa-close"></i>
                    </div>
                  </div>

                  <div class="header-menu-bg"></div>
                </div>

              </div>
            </div>


            <div class="col-auto">
              <div class="header-right d-flex items-center">
                <div class="header-right__icons text-white d-flex items-center">

                  <div class="d-none xl:d-block ml-20">
                    <button class="text-dark-1 items-center" data-el-toggle=".js-mobile-menu-toggle">
                      <i class="fa fa-bars"></i>
                    </button>
                  </div>

                </div>

                <div class="header-right__buttons d-flex items-center ml-30 xl:ml-20 lg:d-none">
                  <?php 
                  if(isset(Auth::user()->type)) { ?>
                    <a href="<?php echo url('/logout'); ?>" class="button -underline text-dark-1">Log out</a>
                    <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="button px-25 h-50 -dark-1 text-white ml-20">My Account</a>
                  <?php } else { ?>
                    <a href="<?php echo url('/userlogin'); ?>" class="button -underline text-dark-1">Log in</a>
                    <a href="<?php echo url('/signup'); ?>" class="button px-25 h-50 -dark-1 text-white ml-20">Sign up</a>
                  <?php } ?>
                </div>
              </div>
            </div>

          </div>
        </div>
      </header>


      <div class="content-wrapper  js-content-wrapper">

        <section data-anim-wrap class="masthead -type-5">
          <div class="masthead__bg"></div>

          <div class="masthead__container">
            <div class="row y-gap-50 items-center">
              <div class="col-lg-6">
                <div class="masthead__content">
                  <div data-anim-child="slide-up delay-2" class="text-17 lh-15 text-orange-1 fw-500 mb-10">
                    Zodocs CRM software is highly rated in the study abroad education industry
                  </div>
                  <h1 data-anim-child="slide-up delay-2" class="masthead__title">
                    Process of Overseas Education 
                    Visa CRM with Zodocs
                  </h1>
                  <p data-anim-child="slide-up delay-3" class="mt-5">
                  </p>
                  <div data-anim-child="slide-up delay-4" class="row items-center x-gap-20 y-gap-20 pt-20">
                    <div class="col-auto">
                      <a href="<?php echo url('/signup'); ?>" class="button -md -orange-1 text-white">Join For Free</a>
                    </div>
                    <div class="col-auto">
                      <a href="<?php echo url('/coursesearch'); ?>" class="button -md -outline-light-5 text-dark-1">Find Courses</a>
                    </div>
                  </div>

                  <div data-anim-child="slide-up delay-5" class="row x-gap-20 y-gap-20 items-center pt-60 lg:pt-30">

                    <div class="col-xl-4 col-auto">
                      <div class="text-dark-1">
                        <div class="mr-10">
                          <img src="la-assets/coursefinder/img/home-8/hero/icons/1.svg" alt="icon">
                        </div>
                        <div class="fw-500 lh-11 mt-10">Over 12 million<br> students</div>
                      </div>
                    </div>

                    <div class="col-xl-4 col-auto">
                      <div class="text-dark-1">
                        <div class="mr-10">
                          <img src="la-assets/coursefinder/img/home-8/hero/icons/2.svg" alt="icon">
                        </div>
                        <div class="fw-500 lh-11 mt-10">More than<br> 60,000 courses</div>
                      </div>
                    </div>

                    <div class="col-xl-4 col-auto">
                      <div class="text-dark-1">
                        <div class="mr-10">
                          <img src="la-assets/coursefinder/img/home-8/hero/icons/3.svg" alt="icon">
                        </div>
                        <div class="fw-500 lh-11 mt-10">Learn anything<br> online</div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>

              <div class="col-lg-6">
                <div data-anim-child="slide-up delay-6" class="masthead__image">
                  <img src="la-assets/coursefinder/img/home-8/hero/image.png" alt="image">
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="layout-pt-md layout-pb-lg">
          <div data-anim-wrap class="container">
            <div class="row justify-center text-center">
              <div class="col-auto">
                <div class="sectionTitle ">
                  <h2 class="sectionTitle__title ">Our Courses</h2>
                </div>
                <div class="d-flex x-gap-20 text-left pt-60 lg:pt-40">
                  <div>
                  </div>
                  <div>
                  </div>
                  <div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row y-gap-30 justify-center pt-60 lg:pt-40">
              <!--Course list -->
              <?php 
                if(count($courses) > 0) { 
                  foreach($courses as $result) {
              ?>
              <div class="col-lg-3 col-md-6">
                <div data-anim-child="slide-up delay-1">

                  <a href="<?php echo url('/coursedetails/'.Crypt::encrypt($result->id)); ?>" class="coursesCard -type-1 border-light rounded-8">
                    <div class="relative">
                      <div class="coursesCard__image overflow-hidden rounded-top-8">
                        <?php if($result->university_logo!='') { ?>
                          <img class="w-1/1 course_logo" src="<?php echo $result->university_logo; ?>" alt="image">
                        <?php } else { ?>
                          <img class="w-1/1 course_logo" src="la-assets/coursefinder/img/coursesCards/no-image.png" alt="image">
                        <?php } ?>
                        <div class="coursesCard__image_overlay rounded-top-8"></div>
                      </div>
                      <div class="d-flex justify-between py-10 px-10 absolute-full-center z-3">

                      </div>
                    </div>

                    <div class="h-100 pt-15 pb-10 px-20">
                      <div class="d-flex items-center">
                        <div class="text-14 lh-1 text-yellow-1 mr-10"><?php echo $result->university_name; ?></div>
                      </div>

                      <div class="text-17 lh-15 fw-500 text-dark-1 mt-10"><?php echo $result->course_name; ?></div>

                      <div class="d-flex x-gap-10 items-center pt-10">

                        <div class="d-flex items-center">
                          <div class="mr-8">
                            <img src="la-assets/coursefinder/img/coursesCards/icons/location-icon.png" alt="icon">
                          </div>
                          <div class="text-14 lh-1"><?php echo $result->country_name; ?></div>
                        </div>

                        <div class="d-flex items-center">
                          <div class="mr-8">
                            <img src="la-assets/coursefinder/img/coursesCards/icons/2.svg" alt="icon">
                          </div>
                          <div class="text-14 lh-1"><?php echo $result->intake; ?></div>
                        </div>

                        <div class="d-flex items-center">
                          <div class="mr-8">
                            <img src="la-assets/coursefinder/img/coursesCards/icons/1.svg" alt="icon">
                          </div>
                          <div class="text-14 lh-1"><?php echo $result->course_type; ?></div>
                        </div>

                      </div>

                      <div class="coursesCard-footer">
                        <div class="coursesCard-footer__author">
                          <div>Annual Tuition Fees</div>
                        </div>

                        <div class="coursesCard-footer__price">
                          <div></div>
                          <div>&#8377; <?php echo $result->tuition_fees; ?></div>
                        </div>
                      </div>
                    </div>
                  </a>

                </div>
              </div>
              <?php }} ?>
              <!-- End Course list -->
            </div>

            <div class="row justify-center pt-60 lg:pt-40">
              <div class="col-auto">
                <a href="<?php echo url('/coursesearch'); ?>" class="button -md -outline-light-5 text-dark-1">
                  View All Courses
                </a>
              </div>
            </div>
          </div>
        </section>

        <section class="layout-pt-lg layout-pb-lg bg-dark-5">
          <div data-anim-wrap class="container">
            <div class="row justify-center text-center">
              <div data-anim-child="slide-up delay-1" class="col-auto">

                <div class="sectionTitle ">

                  <h2 class="sectionTitle__title text-white">Why learn with our courses?</h2>

                  <p class="sectionTitle__text text-white">Lorem ipsum dolor sit amet, consectetur.</p>

                </div>

              </div>
            </div>

            <div class="row y-gap-30 pt-50">

              <div data-anim-child="slide-up delay-2" class="col-lg-4 col-md-6">
                <div class="stepCard -type-1 -stepCard-hover">
                  <div class="stepCard__content">
                    <div class="stepCard__icon">
                      <i class="fa fa-university text-64 text-green-1"></i>
                    </div>
                    <h4 class="stepCard__title">01. Learn</h4>
                    <p class="stepCard__text"> Lorem ipsum dolor sit amet, consectetur dolorili adipiscing elit. Felis donec massa aliqua.</p>
                  </div>
                </div>
              </div>

              <div data-anim-child="slide-up delay-3" class="col-lg-4 col-md-6">
                <div class="stepCard -type-1 -stepCard-hover">
                  <div class="stepCard__content">
                    <div class="stepCard__icon">
                      <i class="fa fa-graduation-cap text-64 text-green-1"></i>
                    </div>
                    <h4 class="stepCard__title">02. Graduate</h4>
                    <p class="stepCard__text"> Lorem ipsum dolor sit amet, consectetur dolorili adipiscing elit. Felis donec massa aliqua.</p>
                  </div>
                </div>
              </div>

              <div data-anim-child="slide-up delay-4" class="col-lg-4 col-md-6">
                <div class="stepCard -type-1 -stepCard-hover">
                  <div class="stepCard__content">
                    <div class="stepCard__icon">
                      <i class="fa fa-tasks text-64 text-green-1"></i>
                    </div>
                    <h4 class="stepCard__title">03. Work</h4>
                    <p class="stepCard__text"> Lorem ipsum dolor sit amet, consectetur dolorili adipiscing elit. Felis donec massa aliqua.</p>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </section>

        <section class="layout-pt-lg layout-pb-lg">
          <div class="container">
            <div class="row justify-center text-center">
              <div class="col-xl-6 col-lg-7">

                <div class="sectionTitle ">

                  <h2 class="sectionTitle__title ">What is ZODOCS?</h2>

                  <p class="sectionTitle__text ">Zodocs CRM software is highly rated in the study abroad education industry.</p>

                </div>

              </div>
            </div>

            <div data-anim-wrap class="row y-gap-30 justify-between items-center pt-60 lg:pt-40">
              <div data-anim-child="slide-up" class="col-lg-6">
                <img class="w-1/1" src="la-assets/coursefinder/img/home-8/what/1.png" alt="image">
              </div>

              <div class="col-xl-5 col-lg-6 col-md-9">
                <div class="d-flex flex-column y-gap-30">

                  <div data-anim-child="slide-up delay-2" class="d-flex">
                    <div class="d-flex justify-center items-center size-70 bg-orange-5 rounded-full">
                      <img src="la-assets/coursefinder/img/home-8/what/icons/1.svg" alt="icon">
                    </div>
                    <div class="ml-20">
                      <h5 class="text-18 lh-11 text-dark-1 fw-500">Industry expert agents</h5>
                      <p class="text-dark-1 mt-5">Lorem ipsum dolor sit amet, consectetur dolorili adipiscing elit. Felis donec massa aliquam id dolor .</p>
                    </div>
                  </div>

                  <div data-anim-child="slide-up delay-3" class="d-flex">
                    <div class="d-flex justify-center items-center size-70 bg-orange-5 rounded-full">
                      <img src="la-assets/coursefinder/img/home-8/what/icons/2.svg" alt="icon">
                    </div>
                    <div class="ml-20">
                      <h5 class="text-18 lh-11 text-dark-1 fw-500">Up-to-date course content</h5>
                      <p class="text-dark-1 mt-5">Lorem ipsum dolor sit amet, consectetur dolorili adipiscing elit. Felis donec massa aliquam id dolor .</p>
                    </div>
                  </div>

                  <div data-anim-child="slide-up delay-4" class="d-flex">
                    <div class="d-flex justify-center items-center size-70 bg-orange-5 rounded-full">
                      <img src="la-assets/coursefinder/img/home-8/what/icons/3.svg" alt="icon">
                    </div>
                    <div class="ml-20">
                      <h5 class="text-18 lh-11 text-dark-1 fw-500">Students community</h5>
                      <p class="text-dark-1 mt-5">Lorem ipsum dolor sit amet, consectetur dolorili adipiscing elit. Felis donec massa aliquam id dolor .</p>
                    </div>
                  </div>

                </div>

                <div data-anim-child="slide-up delay-5" class="d-inline-block mt-30">
                  <a href="<?php echo url('/signup'); ?>" class="button -md -orange-1 text-white">Join For Free </a>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="section-bg pt-90 pb-90 lg:pt-50 lg:pb-50">
          <div class="section-bg__item -full">
            <div class="bg-image js-lazy" data-bg="la-assets/coursefinder/img/home-8/grow/bg.png"></div>
          </div>

          <div class="container">
            <div data-anim-wrap class="row y-gap-30 justify-between items-center">
              <div data-anim-child="slide-up delay-3" class="col-xl-4 col-lg-5 col-md-8">
                <h2 class="text-30 lh-15 text-white">Grow your career today with the Education courses</h2>
                <p class="text-white mt-15">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                <div class="d-inline-block mt-25">
                  <a href="<?php echo url('/coursesearch'); ?>" class="button -md -green-1 text-dark-1">Explore Courses</a>
                </div>
              </div>

              <div data-anim-child="slide-up delay-1" class="col-lg-auto">
                <div class="composition -type-6">
                  <div class="-el-1">
                    <div class="bg-white rounded-16 px-60 py-50 sm:px-30 sm:py-40 text-center">
                      <div class="text-55 fw-700 text-dark-1">9/10</div>
                      <div class="mt-10">Overall courses satisfaction score</div>
                    </div>
                  </div>

                  <div class="-el-2">
                    <div class="bg-white rounded-16 px-60 py-50 sm:px-30 sm:py-40 text-center">
                      <div class="text-55 fw-700 text-dark-1">10K+</div>
                      <div class="mt-10">Happy Students Worldwide</div>
                    </div>
                  </div>

                  <div class="-el-3">
                    <div class="bg-white rounded-16 px-60 py-50 sm:px-30 sm:py-40 text-center">
                      <div class="text-55 fw-700 text-dark-1">96%</div>
                      <div class="mt-10">Completition Rate On All Courses</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="layout-pt-lg layout-pb-lg">
          <div class="container">
            <div class="row y-gap-30 justify-between items-center">
              <div class="col-xl-4 col-lg-5 col-md-9">
                <h2 class="text-30 lh-13">What Our Students<br> Have To <span class="text-orange-1"> Say</span></h2>
                
                <div class="pt-60 lg:pt-50 pr-5 overflow-hidden js-section-slider" data-gap="30" data-pagination data-slider-cols="xl-1" data-anim-wrap>
                  <div class="swiper-wrapper">

                    <div class="swiper-slide">
                      <div data-anim="slide-left" class="pt-40 pb-30 px-40 border-light rounded-8">
                        <div class="testimonials__content">
                          <h4 class="text-18 fw-500 text-orange-1">Lorem ipsum</h4>
                          <p class="lh-2 fw-500 mt-15 mb-30"> “Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.”</p>

                          <div class="row x-gap-20 y-gap-20 items-center border-top-light pt-15">
                            <div class="col-auto">
                              <img src="la-assets/coursefinder/img/testimonials/1.png" alt="image">
                            </div>

                            <div class="col-auto">
                              <div class="lh-12 fw-500 text-dark-1">Lorem ipsum</div>
                              <div class="text-13 lh-1 mt-5">Lorem ipsum, Consetetur sadipscing</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="swiper-slide">
                      <div data-anim="slide-left" class="pt-40 pb-30 px-40 border-light rounded-8">
                        <div class="testimonials__content">
                          <h4 class="text-18 fw-500 text-orange-1">Lorem ipsum</h4>
                          <p class="lh-2 fw-500 mt-15 mb-30"> “Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.”</p>

                          <div class="row x-gap-20 y-gap-20 items-center border-top-light pt-15">
                            <div class="col-auto">
                              <img src="la-assets/coursefinder/img/testimonials/2.png" alt="image">
                            </div>

                            <div class="col-auto">
                              <div class="lh-12 fw-500 text-dark-1">Lorem ipsum</div>
                              <div class="text-13 lh-1 mt-5">Lorem ipsum, Consetetur sadipscing</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="swiper-slide">
                      <div data-anim="slide-left" class="pt-40 pb-30 px-40 border-light rounded-8">
                        <div class="testimonials__content">
                          <h4 class="text-18 fw-500 text-orange-1">Lorem ipsum</h4>
                          <p class="lh-2 fw-500 mt-15 mb-30"> “Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.”</p>

                          <div class="row x-gap-20 y-gap-20 items-center border-top-light pt-15">
                            <div class="col-auto">
                              <img src="la-assets/coursefinder/img/testimonials/3.png" alt="image">
                            </div>

                            <div class="col-auto">
                              <div class="lh-12 fw-500 text-dark-1">Lorem ipsum</div>
                              <div class="text-13 lh-1 mt-5">Lorem ipsum, Consetetur sadipscing</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>

                  <div class="d-flex x-gap-15 items-center pt-30">
                    <div class="col-auto">
                      <button class="d-flex items-center text-24 arrow-left-hover js-prev">
                        <i class="fa fa-arrow-left"></i>
                      </button>
                    </div>
                    <div class="col-auto">
                      <div class="pagination -arrows js-pagination"></div>
                    </div>
                    <div class="col-auto">
                      <button class="d-flex items-center text-24 arrow-right-hover js-next">
                        <i class="fa fa-arrow-right"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-lg-6">
                <div class="composition -type-7">
                  <div class="-el-1">
                    <img class="w-1/1" src="la-assets/coursefinder/img/home-8/testimonials/1.png" alt="image">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        
        <footer class="footer -type-5 pt-60 bg-dark-2">
          <div class="container">
            <div class="py-30 border-top-light-15">
              <div class="row justify-between items-center y-gap-20">
                <div class="col-auto">
                  <div class="d-flex items-center h-100 text-white">
                    © 2022 ZODOCS. All Right Reserved.
                  </div>
                </div>

                <div class="col-auto">
                  <div class="d-flex x-gap-20 y-gap-20 items-center flex-wrap">
                    <div>
                      <div class="d-flex x-gap-15 text-white">
                        Powered By<a href="http://demo.zodocs.com/" target="_blank" class="ft_txt1">ZODOCS</a>
                        . Solution By<a href="https://www.labgex.com/" target="_blank" class="ft_txt1">LabGex Innovations (P) Ltd</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>


      </div>
    </main>
  </div>
  <!-- barba container end -->

  <!-- JavaScript -->
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
  <script src="la-assets/coursefinder/js/vendors.js"></script>
  <script src="la-assets/coursefinder/js/main.js"></script>

</body>

</html>