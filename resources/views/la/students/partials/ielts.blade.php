<div id="tab_4" class="tab-pane" >
        
<div class="content-head">
                                        
                                        <h4>English Language Proficiency</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                        </div>
                        {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'school-add-form']) !!}
               
                <div class="pane">
                        <div class="row">
                                <div class=" col s12 ">


                                        <p>
                                                Was English the language of instruction in your previous institution  ?
                                        </p>
                                        <p>
                                                        <label>
                                                                        <input  name="eng_lang_check" type="radio" value="1" checked/>
                                                                        <span>Yes</span>
                                                                      </label>
                                                                      <label>
                                                                                <input name="eng_lang_check" type="radio"  value="0" />
                                                                                <span>No</span>
                                                                              </label>
                                                                      <label>
                                                                                <input name="eng_lang_check" type="radio"  value="2" />
                                                                                <span>Partly</span>
                                                                              </label>																			  
                                                      </p>
                                </div>
                        </div>
						


                        <div id="ielts-wrapper" class="eng-lang-wrapper"> <!-- Yes -->

						<div class="row">
							<div class="col s6">
									<div class="input-field ">
													<input id="yrs_english" name="yrs_english" type="number">
													<label for="yrs_english">Number of years you have studied in English</label>
											</div>
							</div>
							<div class="col s6">
										<div class="input-field ">
											<select name="qualifications" id="qualifications" class="formSelect">
												<option value="">Select</option>
												<option value="IELTS">IELTS</option>
												<option value="PTE">PTE</option>
												<option value="TOEFL">TOEFL</option>
												<option value="OET">OET</option>
												<option value="OTHERS">OTHERS</option>
											</select>	
											<label for="qualifications">I hold the following qualifications</label>
										</div>
							</div>
						</div>
						<div class="row hide" id="other-qualifications-wrapper">
							<div class="col s6">
								<div class="input-field ">
								<input id="other_qualifications" name="other_qualifications" type="text" class="validate">
								<label for="other_qualifications"></label>
								<label for="qualifications">Other qualifications</label>
								</div>
									
							</div>
						</div>
						
						<h4 class="sub_title">Score (if known)</h4>
						<div class="row">
								<div class="col s6">
									<div class="input-field ">
										<input id="reading_score" name="reading_score" type="number" class="validate" step="0.1" min="0">
										<label for="reading_score">Reading Score</label>
									</div>
								</div>	
                                <div class="col s6">
                                        <div class="input-field ">
                                                        <input id="writing_score" name="writing_score" type="number" class="validate" step="0.1" min="0">
                                                        <label for="writing_score">Writing Score</label>
                                                </div>
                                </div>
								
						</div>
                        <div class="row">
                                <div class="col s6">
                                                <div class="input-field ">
                                                                <input id="speaking_score" name="speaking_score" type="number" class="validate" step="0.1" min="0">
                                                                <label for="speaking_score">Speaking Score</label>

                                                        </div>
                                </div>						
								<div class="col s6">
									<div class="input-field ">
										<input id="listening_score" name="listening_score" type="number" class="validate" step="0.1" min="0">
										<label for="listening_score">Listening Score</label>
									</div>
								</div>
                        </div>
                        <div class="row">
                                <div class="col s6">
                                                <div class="input-field ">
                                                        <input id="over_all_score" name="over_all_score" type="number" class="validate" step="0.1" min="0">
                                                        <label for="over_all_score">Over all Score</label>

                                                </div>
                                </div>						
				<div class="col s6">
					<div class="input-field ">
						<input id="test_date" name="test_date" type="text" class="testdatepicker">
						<label for="test_date">Date of test </label>
					</div>
				</div>								
                        </div>



                </div> <!-- End Yes -->

                <div id="ielts-test-wrapper" class="hide eng-lang-wrapper"> <!-- N0 -->
			<h4 class="sub_title">If test is yet to be completed, please state date and time to be taken</h4>
			<div class="row">
				<div class="col s6">
					<div class="input-field ">
						<input id="test_date_time" name="test_date_time" type="text" class="futuredatepicker">
						<label for="test_date_time">Date</label>
					</div>
				</div>
				<div class="col s6">
					<div class="input-field ">
						<input id="test_time" name="test_time" type="text" class="timepicker">
						<label for="test_time">Time</label>
					</div>
				</div>								
			</div>
		</div> <!-- End No -->

		<!-- pte scores  -->
					<div class="row">
							<div class=" col s12 ">
									<p>PTE completed ?</p>
									<p>
										<label>
											<input  name="pte" type="radio" value="1" checked/>
											<span>Yes</span>
										</label>
										<label>
											<input name="pte" type="radio"  value="0" />
											<span>No</span>
										</label>																		  
									</p>
							</div>
					</div>
					<div id="pte-wrapper" class="hide eng-lang-wrapper">
						<h4 class="sub_title">Score (if known)</h4>
						<div class="row">
								<div class="col s6">
												<div class="input-field ">
																<input id="pte_reading" name="pte_reading" type="number" step="0.1" min="0">
																<label for="pte_reading">Reading Score</label>

														</div>
								</div>	
								<div class="col s6">
										<div class="input-field ">
														<input id="pte_writing" name="pte_writing" type="number" step="0.1" min="0">
														<label for="pte_writing">Writing Score</label>
												</div>
								</div>
								
						</div>
						<div class="row">
								<div class="col s6">
												<div class="input-field ">
																<input id="pte_speaking" name="pte_speaking" type="number" step="0.1" min="0">
																<label for="pte_speaking">Speaking Score</label>

														</div>
								</div>						
								<div class="col s6">
										<div class="input-field ">
														<input id="pte_listening" name="pte_listening" type="number" step="0.1" min="0">
														<label for="pte_listening">Listening Score</label>
												</div>
								</div>
						</div>
						<div class="row">
								<div class="col s6">
												<div class="input-field ">
																<input id="pte_overall_score" name="pte_overall_score" type="number" step="0.1" min="0">
																<label for="pte_overall_score">Over all Score</label>

														</div>
								</div>						
								<div class="col s6">
										<div class="input-field ">
														<input id="pte_date_exam" name="pte_date_exam" type="text" class="testdatepicker">
														<label for="pte_date_exam">Date of test </label>
											</div>
								</div>								
						</div>
					</div>

		<!-- Duolingo scores  -->
					<div class="row">
							<div class=" col s12 ">
									<p>Duolingo completed ?</p>
									<p>
										<label>
											<input  name="duolingo" type="radio" value="1" checked/>
											<span>Yes</span>
										</label>
										<label>
											<input name="duolingo" type="radio"  value="0" />
											<span>No</span>
										</label>																		  
									</p>
							</div>
					</div>
					<div id="duolingo-wrapper" class="hide eng-lang-wrapper">
						<h4 class="sub_title">Score (if known)</h4>
						<div class="row">
								<div class="col s6">
												<div class="input-field ">
																<input id="duolingo_reading" name="duolingo_reading" type="number" step="0.1" min="0">
																<label for="duolingo_reading">Reading Score</label>

														</div>
								</div>	
								<div class="col s6">
										<div class="input-field ">
														<input id="duolingo_writing" name="duolingo_writing" type="number" step="0.1" min="0">
														<label for="duolingo_writing">Writing Score</label>
												</div>
								</div>
								
						</div>
						<div class="row">
								<div class="col s6">
												<div class="input-field ">
																<input id="duolingo_speaking" name="duolingo_speaking" type="number" step="0.1" min="0">
																<label for="duolingo_speaking">Speaking Score</label>

														</div>
								</div>						
								<div class="col s6">
										<div class="input-field ">
														<input id="duolingo_listening" name="duolingo_listening" type="number" step="0.1" min="0">
														<label for="duolingo_listening">Listening Score</label>
												</div>
								</div>
						</div>
						<div class="row">
								<div class="col s6">
												<div class="input-field ">
																<input id="duolingo_overall_score" name="duolingo_overall_score" type="number" step="0.1" min="0">
																<label for="duolingo_overall_score">Over all Score</label>

														</div>
								</div>						
								<div class="col s6">
										<div class="input-field ">
														<input id="duolingo_date_exam" name="duolingo_date_exam" type="text" class="testdatepicker">
														<label for="duolingo_date_exam">Date of test </label>
											</div>
								</div>								
						</div>
					</div>

		<!-- TOEFL scores  -->
					<div class="row">
							<div class=" col s12 ">
									<p>TOEFL completed ?</p>
									<p>
										<label>
											<input  name="toefl" type="radio" value="1" checked/>
											<span>Yes</span>
										</label>
										<label>
											<input name="toefl" type="radio"  value="0" />
											<span>No</span>
										</label>																		  
									</p>
							</div>
					</div>
					<div id="TOEFL-wrapper" class="hide eng-lang-wrapper">
						<h4 class="sub_title">Score (if known)</h4>
						<div class="row">
								<div class="col s6">
												<div class="input-field ">
																<input id="toefl_reading" name="toefl_reading" type="number" step="0.1" min="0">
																<label for="toefl_reading">Reading Score</label>

														</div>
								</div>	
								<div class="col s6">
										<div class="input-field ">
														<input id="toefl_writing" name="toefl_writing" type="number" step="0.1" min="0">
														<label for="toefl_writing">Writing Score</label>
												</div>
								</div>
								
						</div>
						<div class="row">
								<div class="col s6">
												<div class="input-field ">
																<input id="toefl_speaking" name="toefl_speaking" type="number" step="0.1" min="0">
																<label for="toefl_speaking">Speaking Score</label>

														</div>
								</div>						
								<div class="col s6">
										<div class="input-field ">
														<input id="toefl_listening" name="toefl_listening" type="number" step="0.1" min="0">
														<label for="toefl_listening">Listening Score</label>
												</div>
								</div>
						</div>
						<div class="row">
								<div class="col s6">
												<div class="input-field ">
																<input id="toefl_overall_score" name="toefl_overall_score" type="number" step="0.1" min="0">
																<label for="toefl_overall_score">Over all Score</label>

														</div>
								</div>						
								<div class="col s6">
										<div class="input-field ">
														<input id="toefl_date_exam" name="toefl_date_exam" type="text" class="testdatepicker">
														<label for="toefl_date_exam">Date of test </label>
											</div>
								</div>								
						</div>
					</div>


					


                </div>
                <div class="controls">
                                <a class="mgbtn2 back_button">Back</a>
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="eng_lang_proficiency">
                                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                                                {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
       

</div>