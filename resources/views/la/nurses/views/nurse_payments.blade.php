<div id="tab_12" class="tab-pane " >
        
<div class="content-head">
                                                <h4>Post Deployement Section </h4>
                                                <!--<div class="id">Nurse ID : <b>#12675</b></div>-->
                                </div>
                                <div class="pane">
									<div class="academic">
									 <div class="mgv-field">
											<div class="name">
											Max Invoice Status To Employer
											</div>
											<div class="value" id="max_invoice_status"></div>
									 </div>               		
									 <div class="mgv-field">
											<div class="name">
											Employer Payment Status
											</div>
											<div class="value" id="employer_pay"></div>
									 </div> 
									 <div class="mgv-field">
											<div class="name">
											Outstanding Payment From Employer
											</div>
											<div class="value" id="outstanding_pay"></div>
									 </div>
									 <div class="mgv-field">
											<div class="name">
											Max Invoice To Employer
											</div>
											<div class="value" id="max_inv_empl"></div>
									 </div>
									 <div class="mgv-field">
											<div class="name">
											Sub Agents
											</div>
											<div class="value" id="sub_agents"></div>
									 </div>
									 <div class="mgv-field">
											<div class="name">
											Invoice From Sub Agent
											</div>
											<div class="value" id="inv_sub_agent"></div>
									 </div>
									 <div class="mgv-field">
											<div class="name">
											Sub Agent Payment Status
											</div>
											<div class="value" id="sub_pay_status"></div>
									 </div>  
									 <div class="mgv-field">
											<div class="name">
											Out Standing Payment
											</div>
											<div class="value" id="outstanding_pay_subagent"></div>
									 </div> 
									 <div class="mgv-field">
											<div class="name">
											Additional Documents
											</div>
											<div class="value">
												<div class="uploaded-file" id="addl_docs" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
												<div class="file-approve-buttons">
													<!-- <a href="#" class="btn btn-danger mx-approve-doc" data-type="addl_doc" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
													<a href="#" class="btn btn-success mx-approve-doc" data-type="addl_doc" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a> -->
													<a href="#" class="btn btn-danger mx-approve-doc" data-type="addl_docs" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
													<a href="#" class="btn btn-success mx-approve-doc" data-type="addl_docs" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
												</div>  											
											</div>
									 </div>
									 <div class="mgv-field">
											<div class="name">
											Last Communication with Candidate
											</div>
											<div class="value" id="last_communication"></div>
									 </div>    									 
									</div>									
							
                                </div>									
</div>

                