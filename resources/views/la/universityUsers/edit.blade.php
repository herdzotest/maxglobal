@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/employees') }}">University user</a> :
@endsection
@section("contentheader_description", $employee->$view_col)
@section("section", "University user")
@section("section_url", url(config('laraadmin.adminRoute') . '/university_users'))
@section("sub_section", "Edit")

@section("htmlheader_title", "University user Edit : ".$employee->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($employee, ['route' => [config('laraadmin.adminRoute') . '.university_users.update', $employee->id ], 'method'=>'PUT', 'id' => 'employee-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'designation')
					@la_input($module, 'gender')
					@la_input($module, 'mobile')
					@la_input($module, 'mobile2')
					@la_input($module, 'email')
					@la_input($module, 'dept')
					@la_input($module, 'city')
					@la_input($module, 'address')
					@la_input($module, 'about')
					@la_input($module, 'date_birth')
					--}}
					<div class="form-group">
						<label for="Profile image" style="display:block;">Profile image :</label>
						<input class="form-control" placeholder="Enter Profile image" id="profile-pic" name="profilePic" type="hidden" value="<?php echo $employee->profilePic; ?>">
						<a class="btn btn-default" data-url="<?php echo url('/'); ?>" id="mx_upload_image" file_type="image" selecter="Profile image">Upload <i class="fa fa-cloud-upload"></i></a>
						<br/>
						<div class="uploaded_image <?php echo (str_replace(' ','',$employee->profilePic)) == '' ? 'hide' : '' ?>" id="view_upload_image">
							<a href="<?php echo str_replace('?s=130','',$employee->profilePic); ?>" target="_blank"><img src="<?php echo $employee->profilePic; ?>"></a>
							<i title="Remove Image" class="fa fa-times" id="profile-remove-img"></i>
						</div>
					</div>					
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/university_users/'.$employee->id) }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@include('la.common.imageUpload')
@endsection

@push('scripts')
<script src="{{ asset('la-assets/js/common.js') }}"></script>
<script>
$(function () {
	$("#employee-edit-form").validate({
		
	});
});
</script>
@endpush
