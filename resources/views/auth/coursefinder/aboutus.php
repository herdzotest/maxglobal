<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Google fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Outlined" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />

  <!-- Stylesheets -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/vendors.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/main.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/custom.css">

  <title>About Us</title>
  <link rel="icon" type="image/x-icon" href="la-assets/coursefinder/img/coursesCards/icons/zodocs_fav.png">
</head>

<body class="preloader-visible" data-barba="wrapper">

  <!-- preloader start -->
  <div class="preloader js-preloader">
    <div class="preloader__bg"></div>
  </div>
  <!-- preloader end -->

  <!-- barba container start -->
  <div class="barba-container" data-barba="container">


    <main class="main-content  ">

      <header data-anim="fade" data-add-bg="bg-dark-1" class="header -type-1 js-header">


        <div class="header__container">
          <div class="row justify-between items-center">

            <div class="col-auto">
              <div class="header-left">

                <div class="header__logo ">
                  <a data-barba href="<?php echo url('/coursefinder'); ?>">
                    <img src="la-assets/coursefinder/img/general/logo.png" alt="logo">
                  </a>
                </div>

              </div>
            </div>


            <div class="header-menu js-mobile-menu-toggle ">
              <div class="header-menu__content">
                <div class="mobile-bg js-mobile-bg"></div>

                <div class="d-none xl:d-flex items-center px-20 py-20 border-bottom-light">
                  
                  <?php 
                  if(isset(Auth::user()->type)) { ?>
                    <a href="<?php echo url('/logout'); ?>" class="text-dark-1">Log out</a>
                    <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="text-dark-1 ml-30">My Account</a>
                  <?php } else { ?>
                    <a href="<?php echo url('/userlogin'); ?>" class="text-dark-1">Log in</a>
                    <a href="<?php echo url('/signup'); ?>" class="text-dark-1 ml-30">Sign up</a>
                  <?php } ?>

                </div>

                <div class="menu js-navList">
                  <ul class="menu__nav text-white -is-active">

                    <li class="menu-item-has-children">
                      <a data-barba href="#">Home</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Home</a>
                        </li>
                        <li><a href="<?php echo url('/coursefinder'); ?>">Home 1</a></li>
                        <li><a href="<?php echo url('/coursefinder2'); ?>">Home 2</a></li>
                        <li><a href="<?php echo url('/coursefinder3'); ?>">Home 3</a></li>
                        <li><a href="<?php echo url('/coursefinder4'); ?>">Home 4</a></li>
                        <li><a href="<?php echo url('/coursefinder5'); ?>">Home 5</a></li>
                        <li><a href="<?php echo url('/coursefinder6'); ?>">Home 6</a></li>
                        <li><a href="<?php echo url('/coursefinder7'); ?>">Home 7</a></li>
                        <li><a href="<?php echo url('/coursefinder8'); ?>">Home 8</a></li>
                        <li><a href="<?php echo url('/coursefinder9'); ?>">Home 9</a></li>
                        <li><a href="<?php echo url('/coursefinder10'); ?>">Home 10</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/aboutus'); ?>">
                        About Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li class="menu-item-has-children">
                      <a data-barba href="#">Courses</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Courses</a>
                        </li>
                        <li><a href="<?php echo url('/coursesearch'); ?>">Course 1</a></li>
                        <li><a href="<?php echo url('/coursesearch2'); ?>">Course 2</a></li>
                        <li><a href="<?php echo url('/coursesearch3'); ?>">Course 3</a></li>
                        <li><a href="<?php echo url('/coursesearch4'); ?>">Course 4</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/blog'); ?>">
                        Blog <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/topcourses'); ?>">
                        Top Courses <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/contactus'); ?>">
                        Contact Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>


                  </ul>
                </div>

                
              </div>

            </div>


            <div class="col-auto">
              <div class="header-right d-flex items-center">

                <div class="header-right__icons text-white d-flex items-center">

                  <div class="d-none xl:d-block ml-20">
                    <button class="text-white items-center" data-el-toggle=".js-mobile-menu-toggle">
                      <i class="fa fa-bars"></i>
                    </button>
                  </div>

                </div>
                
                <div class="header-right__buttons d-flex items-center ml-30 md:d-none">
                  
                  <?php 
                  if(isset(Auth::user()->type)) { ?>
                    <a href="<?php echo url('/logout'); ?>" class="button -underline text-white">Log out</a>
                    <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="button -sm -white text-dark-1 ml-30">My Account</a>
                  <?php } else { ?>
                    <a href="<?php echo url('/userlogin'); ?>" class="button -underline text-white">Log in</a>
                    <a href="<?php echo url('/signup'); ?>" class="button -sm -white text-dark-1 ml-30">Sign up</a>
                  <?php } ?>

                </div>
              </div>
            </div>

          </div>
        </div>
      </header>


      <div class="content-wrapper  js-content-wrapper">


        <section data-anim="fade" class="breadcrumbs ">
          <div class="container">
            <div class="row">
              <div class="col-auto">
                <div class="breadcrumbs__content">

                  <div class="breadcrumbs__item ">
                    <a href="<?php echo url('/coursefinder'); ?>">Home</a>
                  </div>

                  <div class="breadcrumbs__item ">
                    About Us
                  </div>

                </div>
              </div>
            </div>
          </div>
        </section>


        <section class="page-header -type-1">
          <div class="container">
            <div class="page-header__content">
              <div class="row justify-center text-center">
                <div class="col-auto">
                  <div data-anim="slide-up delay-1">

                    <h1 class="page-header__title">About Us</h1>

                  </div>

                  <div data-anim="slide-up delay-2">

                    <p class="page-header__text">We’re on a mission to deliver engaging, curated courses at a reasonable price.</p>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>


        <section class="layout-pt-md layout-pb-md">
          <div data-anim-wrap class="container">
            <div class="row y-gap-50 justify-between items-center">
              <div class="col-lg-6 pr-50 sm:pr-15">
                <div class="composition -type-8">
                  <div class="-el-1"><img src="la-assets/coursefinder/img/about-1/1.png" alt="image"></div>
                  <div class="-el-2"><img src="la-assets/coursefinder/img/about-1/2.png" alt="image"></div>
                  <div class="-el-3"><img src="la-assets/coursefinder/img/about-1/3.png" alt="image"></div>
                </div>
              </div>

              <div class="col-lg-5">
                <h2 class="text-30 lh-16">Welcome to ZODOCS. Enhance your skills with best Online courses</h2>
                <p class="text-dark-1 mt-30">You can start and finish one of these popular courses in under a day – for free! Check out the list below.. Take the course for free</p>
                <p class="pr-50 lg:pr-0 mt-25">We believe that choosing the right university is a crucial step in shaping your future, and our platform is here to provide the support, resources, and tools you need to succeed in this important journey. Let this be your trusted guide as you navigate the path to higher education.</p>
                <div class="d-inline-block">
                  <a href="<?php echo url('/coursesearch'); ?>" class="button -md -purple-1 text-white mt-30">Start Learning For Free </a>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="layout-pt-md layout-pb-lg">
          <div class="container">
            <div class="row y-gap-20 justify-center text-center">
              <div class="col-auto">

                <div class="sectionTitle ">

                  <h2 class="sectionTitle__title ">How it works?</h2>

                  <p class="sectionTitle__text ">Overseas Education Workflow: Plan, Apply, Learn</p>

                </div>

              </div>
            </div>

            <div class="row y-gap-30 justify-between pt-60 lg:pt-40">

              <div class="col-xl-2 col-lg-3 col-md-6">
                <div class="d-flex flex-column items-center text-center">
                  <div class="relative size-120 d-flex justify-center items-center rounded-full bg-light-4">
                    <img src="la-assets/coursefinder/img/home-3/works/1.svg" alt="image">
                    <div class="side-badge">
                      <div class="size-35 d-flex justify-center items-center rounded-full bg-dark-1 -dark-bg-purple-1">
                        <span class="text-14 fw-500 text-white">01</span>
                      </div>
                    </div>
                  </div>
                  <div class="text-17 fw-500 text-dark-1 mt-30">Explore Courses from World-Class Universities</div>
                </div>
              </div>


              <div class="col-auto xl:d-none">
                <div class="pt-30">
                  <img src="la-assets/coursefinder/img/misc/lines/1.svg" alt="icon">
                </div>
              </div>


              <div class="col-xl-2 col-lg-3 col-md-6">
                <div class="d-flex flex-column items-center text-center">
                  <div class="relative size-120 d-flex justify-center items-center rounded-full bg-light-4">
                    <img src="la-assets/coursefinder/img/home-3/works/2.svg" alt="image">
                    <div class="side-badge">
                      <div class="size-35 d-flex justify-center items-center rounded-full bg-dark-1 -dark-bg-purple-1">
                        <span class="text-14 fw-500 text-white">02</span>
                      </div>
                    </div>
                  </div>
                  <div class="text-17 fw-500 text-dark-1 mt-30">Seamless Online Application for Your Overseas Education Journey</div>
                </div>
              </div>


              <div class="col-auto xl:d-none">
                <div class="pt-30">
                  <img src="la-assets/coursefinder/img/misc/lines/2.svg" alt="icon">
                </div>
              </div>


              <div class="col-xl-2 col-lg-3 col-md-6">
                <div class="d-flex flex-column items-center text-center">
                  <div class="relative size-120 d-flex justify-center items-center rounded-full bg-light-4">
                    <img src="la-assets/coursefinder/img/home-3/works/3.svg" alt="image">
                    <div class="side-badge">
                      <div class="size-35 d-flex justify-center items-center rounded-full bg-dark-1 -dark-bg-purple-1">
                        <span class="text-14 fw-500 text-white">03</span>
                      </div>
                    </div>
                  </div>
                  <div class="text-17 fw-500 text-dark-1 mt-30">TYou're All Set! Start learning right away.</div>
                </div>
              </div>


            </div>
          </div>
        </section>

        <section class="section-bg layout-pt-lg layout-pb-md">
          <div class="section-bg__item -full -height-half bg-dark-5"></div>

          <div data-anim-wrap class="container">
            <div class="row justify-center text-center">
              <div class="col-auto">

                <div class="sectionTitle ">

                  <h2 class="sectionTitle__title text-white">Start your Learning Journey Today!</h2>

                  <p class="sectionTitle__text text-white">Lorem ipsum dolor sit amet, consectetur.</p>

                </div>

              </div>
            </div>

            <div data-anim-wrap class="row y-gap-30 justify-between pt-60 lg:pt-50">

              <div data-anim-child="slide-up delay-1" class="col-lg-3 col-md-6">
                <div class="coursesCard -type-2 text-center pt-50 pb-40 px-30 rounded-8 bg-white shadow-2">
                  <div class="coursesCard__image">
                    <img src="la-assets/coursefinder/img/home-5/learning/1.svg" alt="image">
                  </div>
                  <div class="coursesCard__content mt-30">
                    <h5 class="coursesCard__title text-18 lh-1 fw-500">Learn with Experts</h5>
                    <p class="coursesCard__text text-14 mt-10">Learn from experienced professionals and experts.



</p>
                  </div>
                </div>
              </div>

              <div data-anim-child="slide-up delay-2" class="col-lg-3 col-md-6">
                <div class="coursesCard -type-2 text-center pt-50 pb-40 px-30 rounded-8 bg-white shadow-2">
                  <div class="coursesCard__image">
                    <img src="la-assets/coursefinder/img/home-5/learning/2.svg" alt="image">
                  </div>
                  <div class="coursesCard__content mt-30">
                    <h5 class="coursesCard__title text-18 lh-1 fw-500">Learn Anything</h5>
                    <p class="coursesCard__text text-14 mt-10">Master any skill at your own pace.</p>
                  </div>
                </div>
              </div>

              <div data-anim-child="slide-up delay-3" class="col-lg-3 col-md-6">
                <div class="coursesCard -type-2 text-center pt-50 pb-40 px-30 rounded-8 bg-white shadow-2">
                  <div class="coursesCard__image">
                    <img src="la-assets/coursefinder/img/home-5/learning/3.svg" alt="image">
                  </div>
                  <div class="coursesCard__content mt-30">
                    <h5 class="coursesCard__title text-18 lh-1 fw-500">Flexible Learning</h5>
                    <p class="coursesCard__text text-14 mt-10">Learn at your own pace, anytime, anywhere.</p>
                  </div>
                </div>
              </div>

              <div data-anim-child="slide-up delay-4" class="col-lg-3 col-md-6">
                <div class="coursesCard -type-2 text-center pt-50 pb-40 px-30 rounded-8 bg-white shadow-2">
                  <div class="coursesCard__image">
                    <img src="la-assets/coursefinder/img/home-5/learning/4.svg" alt="image">
                  </div>
                  <div class="coursesCard__content mt-30">
                    <h5 class="coursesCard__title text-18 lh-1 fw-500">Industrial Standard</h5>
                    <p class="coursesCard__text text-14 mt-10">Courses that meet industry standards.</p>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </section>

        <section class="layout-pt-md layout-pb-md">
          <div class="container">
            <div class="row y-gap-20 justify-center text-center">
              <div class="col-auto">

                <div class="sectionTitle ">

                  <h2 class="sectionTitle__title ">Testimonials</h2>

                  <p class="sectionTitle__text ">Lorem ipsum dolor sit amet</p>

                </div>

              </div>
            </div>

            <div class="row justify-center pt-60">
              <div class="col-xl-6 col-lg-8 col-md-10">
                <div class="overflow-hidden js-testimonials-slider">
                  <div class="swiper-wrapper">

                    <div class="swiper-slide h-100">
                      <div data-anim="slide-up" class="testimonials -type-2 text-center">
                        <div class="testimonials__icon">
                          <img src="la-assets/coursefinder/img/misc/quote.svg" alt="quote">
                        </div>
                        <div class="testimonials__text md:text-20 fw-500 text-dark-1">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.</div>
                        <div class="testimonials__author">
                          <h5 class="text-17 lh-15 fw-500">Lorem ipsum</h5>
                          <div class="mt-5">Lorem ipsum, Consetetur sadipscing</div>
                        </div>
                      </div>
                    </div>

                    <div class="swiper-slide h-100">
                      <div data-anim="slide-up" class="testimonials -type-2 text-center">
                        <div class="testimonials__icon">
                          <img src="la-assets/coursefinder/img/misc/quote.svg" alt="quote">
                        </div>
                        <div class="testimonials__text md:text-20 fw-500 text-dark-1">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.</div>
                        <div class="testimonials__author">
                          <h5 class="text-17 lh-15 fw-500">Lorem ipsum</h5>
                          <div class="mt-5">Lorem ipsum, Consetetur sadipscing</div>
                        </div>
                      </div>
                    </div>

                    <div class="swiper-slide h-100">
                      <div data-anim="slide-up" class="testimonials -type-2 text-center">
                        <div class="testimonials__icon">
                          <img src="la-assets/coursefinder/img/misc/quote.svg" alt="quote">
                        </div>
                        <div class="testimonials__text md:text-20 fw-500 text-dark-1">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.</div>
                        <div class="testimonials__author">
                          <h5 class="text-17 lh-15 fw-500">Lorem ipsum</h5>
                          <div class="mt-5">Lorem ipsum, Consetetur sadipscing</div>
                        </div>
                      </div>
                    </div>

                    <div class="swiper-slide h-100">
                      <div data-anim="slide-up" class="testimonials -type-2 text-center">
                        <div class="testimonials__icon">
                          <img src="la-assets/coursefinder/img/misc/quote.svg" alt="quote">
                        </div>
                        <div class="testimonials__text md:text-20 fw-500 text-dark-1">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.</div>
                        <div class="testimonials__author">
                          <h5 class="text-17 lh-15 fw-500">Lorem ipsum</h5>
                          <div class="mt-5">Lorem ipsum, Consetetur sadipscing</div>
                        </div>
                      </div>
                    </div>

                    <div class="swiper-slide h-100">
                      <div data-anim="slide-up" class="testimonials -type-2 text-center">
                        <div class="testimonials__icon">
                          <img src="la-assets/coursefinder/img/misc/quote.svg" alt="quote">
                        </div>
                        <div class="testimonials__text md:text-20 fw-500 text-dark-1">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.</div>
                        <div class="testimonials__author">
                          <h5 class="text-17 lh-15 fw-500">Lorem ipsum</h5>
                          <div class="mt-5">Lorem ipsum, Consetetur sadipscing</div>
                        </div>
                      </div>
                    </div>

                  </div>

                  <div class="pt-60 lg:pt-40">
                    <div class="pagination -avatars row x-gap-40 y-gap-20 justify-center js-testimonials-pagination">

                      <div class="col-auto">
                        <div class="pagination__item is-active">
                          <img src="la-assets/coursefinder/img/home-3/testimonials/1.png" alt="image">
                        </div>
                      </div>

                      <div class="col-auto">
                        <div class="pagination__item ">
                          <img src="la-assets/coursefinder/img/home-3/testimonials/2.png" alt="image">
                        </div>
                      </div>

                      <div class="col-auto">
                        <div class="pagination__item ">
                          <img src="la-assets/coursefinder/img/home-3/testimonials/3.png" alt="image">
                        </div>
                      </div>

                      <div class="col-auto">
                        <div class="pagination__item ">
                          <img src="la-assets/coursefinder/img/home-3/testimonials/4.png" alt="image">
                        </div>
                      </div>

                      <div class="col-auto">
                        <div class="pagination__item ">
                          <img src="la-assets/coursefinder/img/home-3/testimonials/5.png" alt="image">
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="layout-pt-md layout-pb-md">
          <div class="container">
            <div class="row y-gap-30 justify-between items-center">
              <div class="col-xl-5 col-lg-6 col-md-9 lg:order-2">
                <h3 class="text-45 md:text-30 lh-12"><span class="text-purple-1">Learn</span> new skills when<br class="lg:d-none"> and where you like.</h3>
                <p class="mt-20">Access a wide range of courses that fit your schedule. Whether at home or on the go, you can learn at your own pace and convenience, making skill development easy and flexible.
<br class="lg:d-none">.</p>
                <div class="d-inline-block mt-30">
                  <a href="<?php echo url('/signup'); ?>" class="button -md -dark-1 text-white">Join Free</a>
                </div>
              </div>

              <div class="col-lg-6 lg:order-1">
                <div class="composition -type-3">
                  <div class="-el-1">
                    <div class="bg-dark-1 py-50 px-30 rounded-8">
                      <div class="y-gap-20 pt-25">

                        <div class="d-flex items-center">
                          <div class="d-flex items-center justify-center size-25 rounded-full bg-purple-1 mr-15">
                            <i class="text-white size-12" data-feather="check"></i>
                          </div>
                          <div class="fw-500 text-white">Hand-picked authors</div>
                        </div>

                        <div class="d-flex items-center">
                          <div class="d-flex items-center justify-center size-25 rounded-full bg-purple-1 mr-15">
                            <i class="text-white size-12" data-feather="check"></i>
                          </div>
                          <div class="fw-500 text-white">Easy to follow curriculum</div>
                        </div>

                        <div class="d-flex items-center">
                          <div class="d-flex items-center justify-center size-25 rounded-full bg-purple-1 mr-15">
                            <i class="text-white size-12" data-feather="check"></i>
                          </div>
                          <div class="fw-500 text-white">Free courses</div>
                        </div>

                        <div class="d-flex items-center">
                          <div class="d-flex items-center justify-center size-25 rounded-full bg-purple-1 mr-15">
                            <i class="text-white size-12" data-feather="check"></i>
                          </div>
                          <div class="fw-500 text-white">Money-back guarantee</div>
                        </div>

                      </div>
                    </div>
                  </div>
                  <div class="-el-2">
                    <img class="w-1/1" src="la-assets/coursefinder/img/home-6/learn/1.png" alt="image">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <footer class="footer -type-1 bg-dark-1 -green-links">
          <div class="container">
            
            <div class="py-30 border-top-light-15">
              <div class="row justify-between items-center y-gap-20">
                <div class="col-auto">
                  <div class="d-flex items-center h-100 text-white">
                    © 2022 ZODOCS. All Right Reserved.
                  </div>
                </div>

                <div class="col-auto">
                  <div class="d-flex x-gap-20 y-gap-20 items-center flex-wrap">
                    <div>Powered By
                      <a href="http://demo.zodocs.com/" class="text-white" target="_blank">
                        <span class="text-15">ZODOCS</span>
                      </a>. Solution By
                      <a href="https://www.labgex.com/" class="text-white" target="_blank">
                        <span class="text-15">LabGex Innovations (P) Ltd</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>


      </div>
    </main>
  </div>
  <!-- barba container end -->

  <!-- JavaScript -->
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
  <script src="la-assets/coursefinder/js/vendors.js"></script>
  <script src="la-assets/coursefinder/js/main.js"></script>

</body>

</html>