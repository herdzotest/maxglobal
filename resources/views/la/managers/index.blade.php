@extends("la.layouts.app")

@section("contentheader_title", "Managers")
@section("contentheader_description", "Managers listing")
@section("section", "Managers")
@section("sub_section", "Listing")
@section("htmlheader_title", "Managers Listing")

@section("headerElems")
@la_access("Employees", "create")
	<button class="btn btn-success btn-sm pull-right addbtn" data-toggle="modal" data-target="#AddModal"><i class="icon-user-follow"></i> Add Manager</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-hover mg-table mgt2">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			@endforeach
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Employees", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog mgmodal-dialog" role="document">
		<div class="modal-content mgmodal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Manager</h4>
			</div>
			{!! Form::open(['action' => 'LA\ManagersController@store', 'id' => 'employee-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
                    @la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'gender')
					@la_input($module, 'mobile')
					@la_input($module, 'mobile2')
					@la_input($module, 'email')
					@la_input($module, 'city')
					@la_input($module, 'address')
					@la_input($module, 'about')
					@la_input($module, 'date_birth')
					--}}
					<div class="form-group">
						<label for="Profile image" style="display:block;">Profile image :</label>
						<input class="form-control" placeholder="Enter Profile image" id="profile-pic" name="profilePic" type="hidden" value="">
						<a class="btn btn-default" id="mx_upload_image" file_type="image" selecter="Profile image">Upload <i class="fa fa-cloud-upload"></i></a>
						<br/>
						<div class="uploaded_image hide" id="view_upload_image">
							<a href="#" target="_blank"><img src=""></a>
							<i title="Remove Image" class="fa fa-times"></i>
						</div>
					</div>	
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default mgbtn2" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn mgbtn']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@include('la.common.imageUpload')
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>

@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/js/common.js') }}"></script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
		ajax: {
			url: "{{ url(config('laraadmin.adminRoute') . '/managers_dt_ajax/') }}?type=<?php echo $type; ?>",
		},
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#employee-add-form").validate({	
	});
});
</script>
@endpush
