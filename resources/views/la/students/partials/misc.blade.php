<div id="tab_6" class="tab-pane" >
        
<div class="content-head">

<h4>Further Details</h4>
<!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
</div>
{!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'misc-add-form']) !!}
               
<div class="pane">

	<div class="row">
			<div class="col s6">
					<div class="input-field ">
						<select name="aboutUs" id="aboutUs" class="formSelect">
							<option value="">Select</option>
							<option value="Friend"> Friend </option>
							<option value="Website"> Website </option>
							<option value="social media"> social media </option>
						</select>
						<label for="aboutUs">How Did You Hear About Us?</label>
					</div>
			</div>
	</div>
	
	<h4>Next of Kin (Name and contact no.of family or friend to be contacted in emergency)</h4>

	<div class="row">
		<div class="col s4">
			<div class="input-field ">
				<input id="nxt_kin_name" name="nxt_kin_name" type="text" maxlength="100" class="">
				<label for="nxt_kin_name">Name</label>
			</div>
		</div>
		<div class="col s3">
			<div class="input-field ">
				<input id="nxt_kin_country_code" name="nxt_kin_country_code" type="number" maxlength="4" class="">
				<label for="nxt_kin_country_code">Country code</label>
			</div>
		</div>
		<div class="col s5">
			<div class="input-field ">
				<input id="nxt_kin_phone" name="nxt_kin_phone" type="number" maxlength="15" class="">
				<label for="nxt_kin_phone">Phone number</label>
			</div>
		</div>		
	</div>	
	<div class="row">
			<div class="col s6">
				<div class="input-field ">
					<input id="skype_id" name="skype_id" type="text" maxlength="15" class="">
					<label for="nxt_kin_phone">Skype ID</label>
				</div>
			</div>
			<div class="col s6">
							<label for="">Skype profile screen shot</label>
							<div class="file-field input-field">
									<div class="btn8">
											<span>Upload</span>
											<input type="file" multiple > <i class="fa fa-cloud-upload"></i>
									</div>
									<div class="file-path-wrapper">
											<input class="file-path validate" name="skpe_screenshot" type="text" placeholder = "Upload file" >
									</div>
							</div>
							<div class="uploaded-file" id="skpe_screenshot" data-type="file"></div>
			</div>
	</div>
	<div class="row">
			<div class="col s6">
				<div class="input-field ">
					<select name="country_permanent" id="country_permanent" class="searchSelect">
						<option value="">Select</option>
						<?php
						foreach($countries as $countriesData) {
							echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
						}
						?>				
					</select>
					<label for="nxt_kin_country_code" style="margin-top: -40px;font-size: 13px;">Country of permanent residence</label>
				</div>
			</div>
			<div class="col s6">
				<div class="input-field ">
					<select name="country_residence" id="country_residence" class="searchSelect">
						<option value="">Select</option>
						<?php
						foreach($countries as $countriesData) {
							echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
						}
						?>
					</select>
					<label for="nxt_kin_phone" style="margin-top: -40px;font-size: 13px;">Country of birth</label>
				</div>
			</div>		
	</div>	

	<div class="row">
			<div class="col s6">
				<div class="input-field ">
                    <input id="birth_place" name="birth_place" type="text" class="">
					<label for="birth_place">Place of birth</label>
				</div>
			</div>	
			<div class="col s6">
				<div class="input-field ">
					<select name="Nationality" id="Nationality" class="searchSelect">
          				<option value="">Select</option>
            <option value="Afghan"> Afghan </option>
            <option value="Albanian"> Albanian </option>
            <option value="Algerian"> Algerian </option>
            <option value="American"> American </option>
            <option value="Andorran"> Andorran </option>
            <option value="Angolan"> Angolan </option>
            <option value="Anguillan"> Anguillan </option>
            <option value="Citizen of Antigua and Barbuda"> Citizen of Antigua and Barbuda </option>
            <option value="Argentine"> Argentine </option>
            <option value="Armenian"> Armenian </option>
            <option value="Australian"> Australian </option>
            <option value="Austrian"> Austrian </option>
            <option value="Azerbaijani"> Azerbaijani </option>
            <option value="Bahamian"> Bahamian </option>
            <option value="Bahraini"> Bahraini </option>
            <option value="Bangladeshi"> Bangladeshi </option>
            <option value="Barbadian"> Barbadian </option>
            <option value="Belarusian"> Belarusian </option>
            <option value="Belgian"> Belgian </option>
            <option value="Belizean"> Belizean </option>
            <option value="Beninese"> Beninese </option>
            <option value="Bermudian"> Bermudian </option>
            <option value="Bhutanese"> Bhutanese </option>
            <option value="Bolivian"> Bolivian </option>
            <option value="Citizen of Bosnia and Herzegovina"> Citizen of Bosnia and Herzegovina </option>
            <option value="Botswanan"> Botswanan </option>
            <option value="Brazilian"> Brazilian </option>
            <option value="British"> British </option>
            <option value="British-Virgin-Islander"> British Virgin Islander </option>
            <option value="Bruneian"> Bruneian </option>
            <option value="Bulgarian"> Bulgarian </option>
            <option value="Burkinan	"> Burkinan </option>
            <option value="Burmese"> Burmese </option>
            <option value="Burundian"> Burundian </option>
            <option value="Cambodian"> Cambodian </option>
            <option value="Cameroonian"> Cameroonian </option>
            <option value="Canadian	Cape Verdean"> Canadian Cape Verdean </option>
            <option value="Cayman Islander	"> Cayman Islander </option>
            <option value="Central African	"> Central African </option>
            <option value="Chadian	"> Chadian </option>
            <option value="Chilean"> Chilean </option>
            <option value="Chinese	"> Chinese </option>
            <option value="Colombian	"> Colombian </option>
            <option value="Comoran	Congolese (Congo)"> Comoran Congolese (Congo) </option>
            <option value="Congolese (DRC)	"> Congolese (DRC) </option>
            <option value="Cook Islander	"> Cook Islander </option>
            <option value="Costa Rican	"> Costa Rican </option>
            <option value="Croatian"> Croatian </option>
            <option value="Cuban	"> Cuban </option>
            <option value="Cymraes	"> Cymraes </option>
            <option value="Cymro	"> Cymro </option>
            <option value="Cypriot"> Cypriot </option>
            <option value="Czech	 	 	 "> Czech </option>
            <option value="Danish	"> Danish </option>
            <option value="Djiboutian	"> Djiboutian </option>
            <option value="Dominican	"> Dominican </option>
            <option value="Citizen of the Dominican Republic"> Citizen of the Dominican Republic </option>
            <option value="Dutch	 	 	 "> Dutch </option>
            <option value="East Timorese	"> East Timorese </option>
            <option value="Ecuadorean	"> Ecuadorean </option>
            <option value="Egyptian"> Egyptian </option>
            <option value="Emirati"> Emirati </option>
            <option value="English	"> English </option>
            <option value="Equatorial Guinean"> Equatorial Guinean </option>
            <option value="Eritrean	"> Eritrean </option>
            <option value="Estonian"> Estonian </option>
            <option value="Ethiopian	 	 	 "> Ethiopian </option>
            <option value="Faroese	"> Faroese </option>
            <option value="Fijian	"> Fijian </option>
            <option value="Filipino	"> Filipino </option>
            <option value="Finnish"> Finnish </option>
            <option value="French	 	 	 "> French </option>
            <option value="Gabonese	"> Gabonese </option>
            <option value="Gambian	"> Gambian </option>
            <option value="Georgian	"> Georgian </option>
            <option value="German"> German </option>
            <option value="Ghanaian	"> Ghanaian </option>
            <option value="Gibraltarian	"> Gibraltarian </option>
            <option value="Greek	"> Greek </option>
            <option value="Greenlandic"> Greenlandic </option>
            <option value="Grenadian"> Grenadian </option>
            <option value="Guamanian	"> Guamanian </option>
            <option value="Guatemalan	"> Guatemalan </option>
            <option value="Citizen of Guinea-Bissau"> Citizen of Guinea-Bissau </option>
            <option value="Guinean	Guyanese	 	 "> Guinean Guyanese </option>
            <option value="Haitian	"> Haitian </option>
            <option value="Honduran	"> Honduran </option>
            <option value="Hong Konger	"> Hong Konger </option>
            <option value="Hungarian"> Hungarian </option>
            <option value="Icelandic"> Icelandic </option>
            <option value="Indian	"> Indian </option>
            <option value="Indonesian	"> Indonesian </option>
            <option value="Iranian"> Iranian </option>
            <option value="Iraqi	"> Iraqi </option>
            <option value="Irish	"> Irish </option>
            <option value="Israeli	"> Israeli </option>
            <option value="Italian"> Italian </option>
            <option value="Ivorian	 	 	 "> Ivorian </option>
            <option value="Jamaican	"> Jamaican </option>
            <option value="Japanese	"> Japanese </option>
            <option value="Jordanian"> Jordanian </option>
            <option value="Kazakh	"> Kazakh </option>
            <option value="Kenyan	"> Kenyan </option>
            <option value="Kittitian	"> Kittitian </option>
            <option value="Citizen of Kiribati"> Citizen of Kiribati </option>
            <option value="Kosovan	"> Kosovan </option>
            <option value="Kuwaiti	"> Kuwaiti </option>
            <option value="Kyrgyz	 "> Kyrgyz </option>
            <option value="Lao	"> Lao </option>
            <option value="Latvian	"> Latvian </option>
            <option value="Lebanese"> Lebanese </option>
            <option value="Liberian"> Liberian </option>
            <option value="Libyan	"> Libyan </option>
            <option value="Liechtenstein citizen	"> Liechtenstein citizen </option>
            <option value="Lithuanian	"> Lithuanian </option>
            <option value="Luxembourger"> Luxembourger </option>
            <option value="Macanese	"> Macanese </option>
            <option value="Macedonian	"> Macedonian </option>
            <option value="Malagasy	"> Malagasy </option>
            <option value="Malawian"> Malawian </option>
            <option value="Malaysian	"> Malaysian </option>
            <option value="Maldivian	"> Maldivian </option>
            <option value="Malian	"> Malian </option>
            <option value="Maltese"> Maltese </option>
            <option value="Marshallese	"> Marshallese </option>
            <option value="Martiniquais	"> Martiniquais </option>
            <option value="Mauritanian	"> Mauritanian </option>
            <option value="Mauritian"> Mauritian </option>
            <option value="Mexican	"> Mexican </option>
            <option value="Micronesian	"> Micronesian </option>
            <option value="Moldovan	"> Moldovan </option>
            <option value="Monegasque"> Monegasque </option>
            <option value="Mongolian	"> Mongolian </option>
            <option value="Montenegrin	"> Montenegrin </option>
            <option value="Montserratian	"> Montserratian </option>
            <option value="Moroccan"> Moroccan </option>
            <option value="Mosotho	"> Mosotho </option>
            <option value="Mozambican	 	 "> Mozambican </option>
            <option value="Namibian	"> Namibian </option>
            <option value="Nauruan	"> Nauruan </option>
            <option value="Nepalese	"> Nepalese </option>
            <option value="New Zealander"> New Zealander </option>
            <option value="Nicaraguan	"> Nicaraguan </option>
            <option value="Nigerian	"> Nigerian </option>
            <option value="Nigerien	"> Nigerien </option>
            <option value="Niuean"> Niuean </option>
            <option value="North Korean"> North Korean </option>
            <option value="Northern Irish	"> Northern Irish </option>
            <option value="Norwegian	 "> Norwegian </option>
            <option value="Omani"> Omani </option>
            <option value="Pakistani	"> Pakistani </option>
            <option value="Palauan	"> Palauan </option>
            <option value="Palestinian	"> Palestinian </option>
            <option value="Panamanian"> Panamanian </option>
            <option value="Papua New Guinean	"> Papua New Guinean </option>
            <option value="Paraguayan	"> Paraguayan </option>
            <option value="Peruvian	"> Peruvian </option>
            <option value="Pitcairn Islander"> Pitcairn Islander </option>
            <option value="Polish	"> Polish </option>
            <option value="Portuguese	"> Portuguese </option>
            <option value="Prydeinig	Puerto Rican"> Prydeinig Puerto Rican </option>
            <option value="Qatari"> Qatari </option>
            <option value="Romanian	"> Romanian </option>
            <option value="Russian"> Russian </option>
            <option value="Rwandan"> Rwandan </option>
            <option value="Salvadorean"> Salvadorean </option>
            <option value="Sammarinese"> Sammarinese </option>
            <option value="Samoan	"> Samoan </option>
            <option value="Sao Tomean"> Sao Tomean </option>
            <option value="Saudi Arabian	"> Saudi Arabian </option>
            <option value="Scottish	"> Scottish </option>
            <option value="Senegalese	"> Senegalese </option>
            <option value="Serbian"> Serbian </option>
            <option value="Citizen of Seychelles	"> Citizen of Seychelles </option>
            <option value="Sierra Leonean	"> Sierra Leonean </option>
            <option value="Singaporean	"> Singaporean </option>
            <option value="Slovak"> Slovak </option>
            <option value="Slovenian	Solomon Islander	"> Slovenian Solomon Islander </option>
            <option value="Somali	"> Somali </option>
            <option value="South African"> South African </option>
            <option value="South Korean	"> South Korean </option>
            <option value="South Sudanese	"> South Sudanese </option>
            <option value="Spanish	"> Spanish </option>
            <option value="Sri Lankan"> Sri Lankan </option>
            <option value="St Helenian	"> St Helenian </option>
            <option value="St Lucian	"> St Lucian </option>
            <option value="Stateless	"> Stateless </option>
            <option value="Sudanese"> Sudanese </option>
            <option value="Surinamese	"> Surinamese </option>
            <option value="Swazi	"> Swazi </option>
            <option value="Swedish	"> Swedish </option>
            <option value="Swiss"> Swiss </option>
            <option value="Syrian	 	 	 "> Syrian </option>
            <option value="Taiwanese	"> Taiwanese </option>
            <option value="Tajik	"> Tajik </option>
            <option value="Tanzanian	"> Tanzanian </option>
            <option value="Thai"> Thai </option>
            <option value="Togolese	"> Togolese </option>
            <option value="Tongan	"> Tongan </option>
            <option value="Trinidadian	"> Trinidadian </option>
            <option value="Tristanian"> Tristanian </option>
            <option value="Tunisian	"> Tunisian </option>
            <option value="Turkish	"> Turkish </option>
            <option value="Turkmen	"> Turkmen </option>
            <option value="Turks and Caicos Islander"> Turks and Caicos Islander </option>
            <option value="Tuvaluan	 	 	 "> Tuvaluan </option>
            <option value="Ugandan	"> Ugandan </option>
            <option value="Ukrainian	"> Ukrainian </option>
            <option value="Uruguayan	"> Uruguayan </option>
            <option value="Uzbek"> Uzbek </option>
            <option value="Vatican citizen	Citizen of Vanuatu	"> Vatican citizen Citizen of Vanuatu </option>
            <option value="Venezuelan	"> Venezuelan </option>
            <option value="Vietnamese"> Vietnamese </option>
            <option value="Vincentian	 	 	 "> Vincentian </option>
            <option value="Wallisian"> Wallisian </option>
            <option value="Welsh"> Welsh </option>
            <option value="Yemeni"> Yemeni </option>
            <option value="Zambian	"> Zambian </option>
            <option value="Zimbabwean"> Zimbabwean </option>
					</select>	
					<label for="nxt_kin_phone" style="margin-top: -40px;font-size: 13px;">Nationality</label>
				</div>
			</div>				
	</div>	
	<div class="row">
		<div class="col s6">
			<div class="input-field ">
				<input name="prev_study_sector" type="text" id="prev_study_sector" value="" />
				<label for="prev_study_sector">Previous Study Sector</label>
			</div>
		</div>
	</div>
	
	<div class="row">
			<div class="col s6">
					<p>Have you also made an application to any University through UCAS</p>
					<p>
							<label>
									<input name="ucas_application" type="radio" value="0" checked/>
									<span>Yes</span>
							</label>
							<label>
									<input name="ucas_application" type="radio"
											value="1" />
									<span>No</span>
							</label>
					</p>
			</div>
			<div class="col s6">
				<div class="input-field ">
                    <input name="ucas_id" type="text" id="ucas_id" maxlength="25" value=""/>
					<label for="prev_study_sector">UCAS ID</label>
				</div>
			</div>	
	</div>
	<div class="row">
			<div class=" col s12 ">
					<p>Have you lived at any other address in past 2 years?</p>
					<p>
							<label>
									<input name="lived_other_address" type="radio" value="0" checked/>
									<span>Yes</span>
							</label>
							<label>
									<input name="lived_other_address" type="radio" value="1" />
									<span>No</span>
							</label>
					</p>
			</div>
	</div>
	<div id="lived_other_address_wrapper">
		<div class="row">
			<div class="input-field col s6">
				<input type="text" id="street_address" name="street_address">
				<label for="address">Street Address</label>
			</div>
			<div class="input-field col s6">
				<input type="text" id="street_address_line" name="street_address_line">
				<label for="address_2">Street Address Line2</label>
			</div>						
		</div>
		<div class="row">
			<div class="input-field col s6">
			   <input type="text" id="city_other_address" name="city_other_address">
			   <label for="city">City</label>
			</div>
			<div class="input-field col s6">
			   <input type="text" id="state_other_address" name="state_other_address">
			   <label for="state">State / Province</label>
			</div>						
		</div>
		<div class="row">
			<div class="input-field col s6">
			   <input type="text" id="zip_code_other_address" name="zip_code_other_address">
			   <label for="zip_code">Postal / Zip code</label>
			</div>
			<div class="input-field col s6">
				<select name="country_other_address" id="country_other_address" class="searchSelect">
						<option value="">Select</option>
						<?php
						foreach($countries as $countriesData) {
							echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
						}
						?>
				</select>							   
			   <label for="country" style="margin-top: -40px;font-size: 13px;">Country</label>
			</div>						
		</div>
		<div class="row">
			<div class="input-field col s8">
			   <input type="text" id="living_addr_start_date" name="living_addr_start_date" class="datepicker">
			   <label for="living_addr_start_date">When did you start living at this address?</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s8">
			   <input type="text" id="living_addr_stop_date" name="living_addr_stop_date" class="datepicker">
			   <label for="living_addr_stop_date">When did you stop living at this address?</label>
			</div>					
		</div>	
	</div>	

	<div class="row">
		<div class="input-field col s4">
			<select name="relatiion_status" id="relatiion_status" class="formSelect">
				<option value="Single">Single</option>
				<option value="Married">Married</option>
				<option value="Civil-partnership">Civil partnership</option>
			</select>			   
		   <label for="living_addr_start_date">Relationship Status</label>
		</div>				
	</div>	
	<div id="spouse-details-wrapper" class="hide">
		<div class="row">
				<div class=" col s3 ipfield">
						<div class="input-field">
							<select name="spouse_prefix" id="spouse_prefix" class="formSelect">
								<option value="Mr">Mr.</option>
								<option value="Mrs">Mrs.</option>
							</select>
							<label for="spouse_prefix" style="margin-top: -10px;font-size: 13px;">Prefix</label>
						</div>
				</div>
				<div class=" col s3 ipfield">
						<div class="input-field">
										<input name="spouse_firstname" id="spouse_firstname" type="text">
										<label for="spouse_firstname">First Name</label>
						</div>


				</div>
				<div class=" col s3 ipfield">
								<div class="input-field">
												<input id="spouse_middlename" name="spouse_middlename" type="text" class="validate">
												<label for="spouse_middlename">Middle Name</label>
								</div>


						</div>
						<div class=" col s3 ipfield">
										<div class="input-field">
														<input id="spouse_familyname" name="spouse_familyname" type="text" class="validate">
														<label for="spouse_familyname">Family Name</label>
										</div>
								</div>
		</div>						
		<div class="row">
				<div class=" col s6 ipfield">
						<div class="input-field">
							<input name="spouse_birth_date" class="datepicker" id="spouse_birth_date" type="text">
							<label for="spouse_birth_date">Date of birth of spouse</label>
						</div>
				</div>
				<div class="col s6 ipfield">
					<div class="input-field">
						<select name="current_live_status" id="current_live_status" class="formSelect">
							<option value="Yes">Yes</option>
							<option value="No">No</option>
						</select>
						<label for="current_live_status" style="margin-top: -10px;font-size: 13px;">Do they currently live with you?</label>
					</div>
				</div>
		</div>			
		
		<div id="spouse-adrress-details-wrapper" class="hide">
			<h4 class="sub_title">If No, provide the current address </h4>
			<div class="row">
				<div class="input-field col s6">
				   <input type="text" id="spouse_current_addr" name="spouse_current_addr">
				   <label for="spouse_current_addr">Street Address</label>
				</div>
				<div class="input-field col s6">
				   <input type="text" id="spouse_current_addr_line" name="spouse_current_addr_line">
				   <label for="spouse_current_addr_line">Street Address Line2</label>
				</div>						
			</div>

			<div class="row">
				<div class="input-field col s6">
				   <input type="text" id="spouse_current_city" name="spouse_current_city">
				   <label for="spouse_current_city">City</label>
				</div>
				<div class="input-field col s6">
				   <input type="text" id="spouse_current_state" maxlength="50" name="spouse_current_state">
				   <label for="spouse_current_state">State / Province</label>
				</div>						
			</div>
			
			<div class="row">
				<div class="input-field col s6">
				   <input type="text" id="spouse_current_zipcode" maxlength="20" name="spouse_current_zipcode">
				   <label for="spouse_current_zipcode">Postal / Zip code</label>
				</div>
				<div class="input-field col s6">
					<select name="spouse_current_country" id="spouse_current_country" class="searchSelect">
							<option value="">Select</option>
							<?php
							foreach($countries as $countriesData) {
								echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
							}
							?>
					</select>							   
				   <label for="country" style="margin-top: -40px;font-size: 13px;">Country</label>
				</div>						
			</div>	
		</div>
		
		<div class="row">
				<div class=" col s6 ipfield">
						<div class="input-field">
							<input name="spouse_passport_no" class="" id="spouse_passport_no" maxlength="20" type="text">
							<label for="spouse_birth_date">Passport number of spouse</label>
						</div>
				</div>
				<div class=" col s6 ipfield">
						<div class="input-field">
							<select name="spouse_travel_uk" id="spouse_travel_uk" class="formSelect">
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
							<label for="current_live_status" style="margin-top: -10px;font-size: 13px;">Will they be travelling with you to the UK?</label>
						</div>
				</div>
		</div>
		
		<div class="row">
			<div class="col s6">
					<label>Passport of spouse</label>
					<div class="file-field input-field">
									<div class="btn">
											<span>Upload</span>
											<input type="file" multiple > <i class="fa fa-cloud-upload"></i>
									</div>
									<div class="file-path-wrapper">
											<input class="file-path validate" name="spouse_passport" type="text" placeholder = "Upload file" >
									</div>
					</div>
					<div class="uploaded-file" id="spouse_passport" data-type="file"></div>	
			</div>
		</div>	
	</div>

</div>
<div class="controls">
<a class="mgbtn2 back_button">Back</a>
                <input id="form_name" type="hidden" class="validate" name="type_of" value="further_details">
                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">                 
                                                      {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
											  
        

</div>