<div id="tab_4" class="tab-pane" >
		<div class="content-head">
		
						<h4>Preferences</h4>
					   <!-- <div class="id">Nurse ID : <b>#12675</b></div>-->
		</div>
	<div class="pane">
		<div class="academic">
			 <div class="mgv-field">
					<div class="name">
							 Preferred Sector 
					</div>
					<div class="value" id="preferred_sector"></div>
			 </div>
			 <div class="mgv-field">
					<div class="name">
						Preferred Designation
					</div>
					<div class="value" id="preferred_designation">
					</div>
			 </div>
			 <div class="mgv-field">
					<div class="name">
						Preferred location
					</div>
					<div class="value" id="preferred_location">
					</div>
			 </div>
			 <div class="mgv-field">
					<div class="name">
						Preferred salary
					</div>
					<div class="value" id="preferred_salary">
					</div>
			 </div>
			 <div class="mgv-field">
					<div class="name">
						Preferred hours
					</div>
					<div class="value" id="preferred_hours">
					</div>
			 </div>
			 <div class="mgv-field">
					<div class="name">
						Preferred shifts
					</div>
					<div class="value" id="preferred_shifts">
					</div>
			 </div>				 
		</div>                               									

	</div>
</div>