@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/employees') }}">Application Form</a> :
@endsection
@section("contentheader_description", $applyDetails[0]->name)
@section("section", "Applications")
@section("section_url", url(config('laraadmin.adminRoute') . '/coursefind'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Applications Edit : ".$applyDetails[0]->name)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-12"><h4>General Info</h4></div>
				{!! Form::model($applyDetails, ['route' => [config('laraadmin.adminRoute') . '.coursefind.update', $applyDetails[0]->apply_id ], 'method'=>'PUT', 'id' => 'employee-edit-form']) !!}
					
				<div class="col-md-8 col-md-offset-2">
					<div class="form-group">
						<label for="name">Name :</label>
						<input class="form-control" placeholder="Name" id="name" name="name" type="text" value="<?php echo $applyDetails[0]->name; ?>" required>
					</div>
					<div class="form-group">
						<label for="name">Email :</label>
						<input class="form-control" placeholder="Email" id="email" name="email" type="email" value="<?php echo $applyDetails[0]->email; ?>" required>
					</div>
					<div class="form-group">
						<label for="name">Mobile :</label>
						<input class="form-control" placeholder="Mobile" id="mobile" name="mobile" type="text" value="<?php echo $applyDetails[0]->mobile; ?>" required>
					</div>
					<div class="form-group">
						<label for="name">Country :</label>
						<select name="country" id="country" class="form-control" required>
                            <option value="">Select</option>
                            <?php 
                                foreach($countries as $countriesData) {
                                    echo '<option value="'.$countriesData->id.'"'.($countriesData->id === $applyDetails[0]->country ? ' selected="selected"' : '').'>'.$countriesData->country.'</option>';
                                }
                            ?>
                        </select>
					</div>
					<div class="form-group">
						<label for="name">State :</label>
						<input class="form-control" placeholder="State" id="state" name="state" type="text" value="<?php echo $applyDetails[0]->state; ?>">
					</div>
					<div class="form-group">
						<label for="name">Date of Birth :</label>
						<input class="form-control" placeholder="Date of Birth" id="dob" name="dob" type="date" value="<?php echo $applyDetails[0]->dob; ?>">
					</div></div>
                    <br>
                    <div class="col-md-12"><h4>Intake</h4></div>
                    <div class="col-md-8 col-md-offset-2">

                    </div>
                    <div class="col-md-8 col-md-offset-2">
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/coursefind/'.$applyDetails[0]->apply_id) }}">Cancel</a></button>
					</div></div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@include('la.common.imageUpload')

@endsection

@push('scripts')
<script src="{{ asset('la-assets/js/common.js') }}"></script>
<script>
$(function () {
	$("#employee-edit-form").validate({
		
	});
});
</script>
@endpush
