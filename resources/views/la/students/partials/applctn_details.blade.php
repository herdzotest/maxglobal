<div id="tab_9" class="tab-pane " >
        
<div class="content-head">
                                        <h4>Immigration History</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->

                                </div>
                                {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'application-add-form']) !!}
	
                                <div class="pane">
								
										<div class="row">
											<div class="col s6">
													<p>Have you been to the UK in the past 10 years?</p>
													<p>
															<label>
																	<input name="in_uk" type="radio" value="0" checked/>
																	<span>Yes</span>
															</label>
															<label>
																	<input name="in_uk" type="radio"
																			value="1" />
																	<span>No</span>
															</label>
													</p>
											</div>	
										</div>

										<div class="row">
											<div class="col s12">
													<p>Have you previously studied in any country on either a Tier 4 (General) visa or a Pre-Tier 4 student visa?</p>
													<p>
															<label>
																	<input name="studied_uk" type="radio" value="0" checked/>
																	<span>Yes</span>
															</label>
															<label>
																	<input name="studied_uk" type="radio"
																			value="1" />
																	<span>No</span>
															</label>
													</p>
											</div>	
										</div>	
										
										<div id="prev-studied-country-wrapper">
										<div id="prev-studied-wrapper">
											<div class="stdhst">
													<div class="index">1</div>
													<div class="col-md-6">
														<div class="mg-input-field ">
															<label for="studied_country">Country</label>
												<select id="studied_country" name="studied_country" class="searchSelect">											<option value="">Select</option>
														<?php
														foreach($countries as $countriesData) {
															echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
														}
														?>
												</select>
	
														</div>
													</div>
													
													<div class="col-md-6">
														<div class="mg-input-field ">
															<label for="reason">Visa start date</label>
															<input class="form-control datepicker" id="studied_visa_start_date" name="studied_visa_start_date" type="text">						
														</div>
													</div>
													<div class="col-md-6">
															<div class="mg-input-field ">
																	<label for="visite_date">Visa end date</label>
																	<input class="form-control datepicker" id="studied_visa_end_date" name="studied_visa_end_date" type="text">
															</div>
													</div>

													<div class="col-md-6">
															<div class="mg-input-field ">
																<label for="studied_status">Status</label>
																	<select id="studied_status" name="studied_status" class="formSelect">
																		<option value="">Select</option>
																		<option value="Completed">Completed</option>
																		<option value="Not-completed">Not completed</option>
																	</select>
															</div>
													</div>

													<div class="col-md-6">
															<div class="mg-input-field ">
																<label for="studied_university">University</label>
																<!-- <input class="form-control" id="studied_university" name="studied_university" type="text"> --> 
																	<select class="searchSelect" name="studied_university" id="studied_university">
																		<option value="">Select</option>
																		<?php
																		foreach($university as $universityData) {
																			echo '<option value="'.$universityData->university.'">'.$universityData->university.'</option>';
																		}
																		?>													
																	</select>
															</div>
													</div>													
													<div class="col-md-6">
															<div class="mg-input-field ">
																	<label for="studied_course_title">Course title</label>
																	<!-- <input class="form-control" id="studied_course_title" name="studied_course_title" type="text"> -->
																	<select class="searchSelect" id="studied_course_title" name="studied_course_title">
																		<option value="">Select</option>
																		<?php
																		foreach($courses as $courseData) {
																			echo '<option value="'.$courseData->course_name.'">'.$courseData->course_name.'</option>';
																		}
																		?>											
																	</select>
															</div>
													</div>
													<!-- <div class="col-md-6">
															<div class="mg-input-field ">
																<label for="studied_status">Status</label>
																	<select id="studied_status" name="studied_status" class="formSelect">
																		<option value="">Select</option>
																		<option value="Completed">Completed</option>
																		<option value="Not-completed">Not completed</option>
																	</select>
															</div>
													</div> -->	
													<div class="col-md-6">
															<div class="mg-input-field ">
																	<label for="studied_cas_start_date" style="margin-top: 15px;">CAS start date</label>
																	<input class="form-control datepicker" id="studied_cas_start_date" name="studied_cas_start_date" type="text">
															</div>
													</div>
													<div class="col-md-6">
															<div class="mg-input-field ">
																	<label for="studied_cas_start_date" style="margin-top: 15px;">CAS end date</label>
																	<input class="form-control datepicker" id="studied_cas_end_date" name="studied_cas_end_date" type="text">
															</div>
													</div>
													<div class="col-md-6">
															<div class="mg-input-field ">
																	<label for="studied_cas_start_date">CAS/Visa copy/transcripts</label>
																	<input class="form-control" id="cas_transcripts" name="cas_transcripts" type="text">
															</div>
													</div>													
													<div class="clearfix"></div>
											</div>
											<input type="hidden" value="imigration_studied" name="formtype_studied" class="formtype_studied">
											<input type="hidden" value="1" class="wrapper-count-studied">	
										</div>
										<div class="btn addshbtn" data-type="formtype_studied"> <span class="glyphicon glyphicon-plus"></span></div>										
										</div>
										
										<div class="row">
											<div class="col s12">
													<p>Have you been issued with a UK visa in the past 10 years?</p>
													<p>
															<label>
																	<input name="uk_visa_issued" type="radio" value="0" checked/>
																	<span>Yes</span>
															</label>
															<label>
																	<input name="uk_visa_issued" type="radio"
																			value="1" />
																	<span>No</span>
															</label>
													</p>
											</div>	
										</div>
										<div class="row">
											<div class="col s12">
													<p>Have you been to any other countries in the past 10 years?</p>
													<p>
															<label>
																	<input name="other_countries" type="radio" value="0" checked/>
																	<span>Yes</span>
															</label>
															<label>
																	<input name="other_countries" type="radio"
																			value="1" />
																	<span>No</span>
															</label>
													</p>
											</div>	
										</div>										
										
										<div id="other-countrier-details-wrapper">
											<div id="imigration-wrapper">
												<div class="stdhst">
														<div class="index">1</div>
														<div class="col-md-6">
															<div class="mg-input-field ">
																<label for="other_country_name">Country</label>
													<select id="other_country_name" name="other_country_name" class="searchSelect">
														<option value="">Select</option>
														<?php
														foreach($countries as $countriesData) {
															echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
														}
														?>
													</select>
															</div>
														</div>
														
														<div class="col-md-6">
															<div class="mg-input-field ">
																<label for="reason">Reason for your visit</label>
																<select id="reason" name="reason" class="formSelect">
																	  <option value="">Select</option>
																	  <option value="Work"> Work </option>
																	  <option value="Tourism"> Tourism </option>
																	  <option value="Others"> Others </option>											
																</select>							
															</div>
														</div>
														<div class="col-md-6">
																<div class="mg-input-field">
																		<label for="visite_date">Date you entered</label>
																		<input class="form-control datepicker" id="visite_date" name="visite_date" type="text">
																</div>
														</div>
														<div class="col-md-6">
																<div class="mg-input-field">
																		<label for="leave_date">Date you left</label>
																		<input class="form-control datepicker" id="leave_date" name="leave_date" type="text">
																</div>
														</div>
														
														<div class="clearfix"></div>
												</div>
												<input type="hidden" value="imigration" name="formtype" class="formtype">
												<input type="hidden" value="1" class="wrapper-count">	
											</div>
											<div class="btn addshbtn"> <span class="glyphicon glyphicon-plus"></span></div>	

											<br/><br/>
										</div>
										
                                        <div class="row">
                                                <div class=" col s6 ">
                                                        <div class="input-field">
															<select name="course" id="course" class="searchSelect">
																<option value="" disabled>Choose your Course</option>
																<?php
																foreach($courses as $coursename) {
																?>
																	<option value="<?php echo $coursename->course_name; ?>"><?php echo $coursename->course_name; ?></option>
																<?php
																}
																?>
															</select>
															<label for="" style="margin-top: -40px;font-size: 13px;">Selected Course</label>
                                                        </div>
                                                </div>
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                <select name="selected_university_imigration" id="selected_university_imigration" class="searchSelect">
                                                                        <option value="">Select</option>
                                                                        <?php
																		foreach($university as $universityData) {
																			echo '<option value="'.$universityData->university.'">'.$universityData->university.'</option>';
																		}
																		?>
                                                                </select>
                                                                <label for="course" style="margin-top: -40px;font-size: 13px;">Selected University</label>
                                                        </div>
                                                </div>									
                                        </div>
                                        <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                <input id="tution_fee" name="tution_fee" type="text" class="validate">
                                                                <label for="tution_fee">Tuition Fee</label>
                                                        </div>
                                                </div>
                                               <div class="col s6">
                                                        <div class="input-field ">
                                                                <select name="scholarship_stat" id="scholarship_stat" class="formSelect">
                                                                        <option value="" disabled>Your status</option>
                                                                        <option value="OFFERED" selected>OFFERED </option>
                                                                        <option value="NOT-RECEIVED">NOT RECEIVED</option>
                                                                        <option value="RECEIVED">RECEIVED</option>
                                                                        <option value="NOT-APPLICABLE">NOT APPLICABLE</option>

                                                                </select>
                                                                <label for="">Scholarship Status</label>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                <select name="uni_aplctn_stat" id="uni_aplctn_stat" class="formSelect">
                                                                        <option value="" disabled>Your status</option>
                                                                        <option value="SUBMITED" selected>SUBMITED </option>
                                                                        <option value="REFUSED">REFUSED</option>
                                                                        <option value="ACCEPTED">ACCEPTED</option>
                                                                        <option value="NOT-APPLICABLE">NOT APPLICABLE</option>
                                                                </select>
                                                                <label for="">University Application Status</label>

                                                        </div>
												</div>
												<div class="col s6">
                                                        <div class="input-field ">
                                                                <select name="offer_stat" id="offer_stat" class="formSelect">
                                                                        <option value="" disabled selected>Your offer letter status</option>
                                                                        <option value="RECEIVED">RECEIVED </option>
                                                                        <option value="WAITING">WAITING</option>
                                                                        <option value="NOT-APPLICABLE">NOT APPLICABLE</option>
                                                                </select>
                                                                <label for="">Offer Letter Status</label>
                                                        </div>														
                                                </div>
                                        </div>
                                        <div class="row" id="ofl-upload" style="display: none;">
                                                <div class="col s6">
													<label>Upload Offer Letter File</label>
													<div class="file-field input-field">
															<div class="btn">
																	<span>Upload</span>
																	<input type="file" multiple>
															</div>
															<div class="file-path-wrapper">
																	<input class="file-path validate"
																			type="text" name="offerletter_proof" 
																			placeholder="Upload file">
															</div>
													</div>
													<div class="uploaded-file" id="offerletter_proof" data-type="file"></div>
                                                </div>
                                        </div>

                                        <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                <select name="tuitionfeecheck" id="tuitionfeecheck" class="formSelect">
                                                                        <option value="" disabled selected>Your payment status</option>
                                                                        <option value="PAID">PAID </option>
                                                                        <option value="NOT-PAID">NOT PAID</option>
                                                                </select>
                                                                <label for="tuitionfeecheck">Tuition Fee Status</label>
                                                        </div>
                                                </div>
												<div class="col s6">
														<div class="input-field">
															<input id="tution_paid" name="tution_paid" type="text" class="validate">
															<label for="">Amount Paid</label>
														</div>
												</div>
                                        </div>
										
										<div class="row">
												<div class="col s6">
														<div class="input-field ">
																<input id="tution_balance" name="tution_balance" type="text" class="validate">
																<label for="">Balance Amount to be Paid</label>
														</div>
												</div>
												<div class="col s6">
														<div class="input-field ">
                                                                <select name="mode_mainatanence" id="mode_mainatanence" class="formSelect">
                                                                        <option value="">Select</option>
                                                                        <option value="Bank-Statement">Bank Statement</option>
                                                                        <option value="Student-Loan-Letter">Student Loan Letter </option>
                                                                </select>
																<label for="">Mode of Maintenance Fund proof</label>
														</div>
												</div>											
										</div>
												
                                        <div class="row">
                                                <div class="col s6">
                                                        <label>Proof of Maintenance Fund (Eg : Bank Statement, Student
                                                                Loan Letter etc.)</label>
                                                        <div class="file-field input-field">
                                                                <div class="btn">
                                                                        <span>Upload</span>
                                                                        <input type="file" multiple>
                                                                </div>
                                                                <div class="file-path-wrapper">
                                                                        <input class="file-path validate" name="maintenance_proof" type="text"
                                                                                placeholder="Upload file">
                                                                </div>
                                                        </div>
														<div class="uploaded-file" id="maintenance_proof" data-type="file"></div>
                                                </div>
                                                <div class="col s6">
                                                        <label> TB Screening Test Report File Upload</label>
                                                        <div class="file-field input-field">
                                                                <div class="btn">
                                                                        <span>Upload</span>
                                                                        <input type="file" multiple>
                                                                </div>
                                                                <div class="file-path-wrapper">
                                                                        <input class="file-path validate" name="TB_proof" type="text"
                                                                                placeholder="Upload file">
                                                                </div>
                                                        </div>
														<div class="uploaded-file" id="TB_proof" data-type="file"></div>
                                                </div>												
                                        </div>
										
										
                                        <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                <select name="cas_stat" id="cas_stat" class="formSelect">
                                                                        <option value="" disabled selected>Your CAS status</option>
                                                                        <option value="RECEIVED">RECEIVED </option>
                                                                        <option value="PENDING">PENDING</option>
                                                                        <option value="NOT-APPLICABLE">NOT APPLICABLE</option>
                                                                </select>
                                                                <label for="">CAS Status</label>
                                                        </div>
												</div>
												<div class="col s8" id="cas-rcd" style="display: none">
														<label>Upload CAS Files</label>
														<div class="file-field input-field">
																<div class="btn">
																		<span>Upload</span>
																		<input type="file" multiple>
																</div>
																<div class="file-path-wrapper">
																		<input class="file-path validate"
																				type="text" name="cas_proof"
																				placeholder="Upload file">
																</div>
														</div>
														<div class="uploaded-file" id="cas_proof" data-type="file"></div>
												</div>
                                        </div>
                                        <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                <select name="visa_aplctn_stat" id="visa_aplctn_stat" class="formSelect">
                                                                        <option value="" disabled selected>Your visa application status</option>
                                                                        <option value="SUBMITTED">SUBMITTED </option>
                                                                        <option value="NOT-SUBMITTED">NOT SUBMITTED</option>
                                                                </select>
                                                                <label for="visa_aplctn_stat">Visa Application Status</label>
                                                        </div>
                                                </div>
										</div>
										<div id="visa-application-details" class="hide">
											<div class="row">
												<div class="col s6">
													<div class="input-field ">
														<input id="visa_apntmnt_date" name="visa_apntmnt_date" type="text" class="datepicker">
														<label for="visa_apntmnt_date">Date</label>
													</div>
												</div>
												<div class="col s6">
													<div class="input-field ">
														<input id="visa_apntmnt_time" name="visa_apntmnt_time" type="text" class="timepicker">
														<label for="visa_apntmnt_time">Time</label>
													</div>
												</div>										
											</div>
											<div class="row">
												<div class="col s6">
													<div class="input-field ">
														<input id="visa_apntmnt_place" name="visa_apntmnt_place" maxlength="100" type="text" class="">
														<label for="visa_apntmnt_place">Centre</label>
													</div>
												</div>
												<div class="col s6">
													<div class="input-field ">
														<input id="visastatus" name="visastatus" maxlength="25" type="text" class="">
														<label for="visastatus">Status</label>
													</div>
												</div>										
											</div>
											<div class="row">
												<div class="col s6">
                                                        <label>Upload Visa Files</label>
														<div class="file-field input-field">
															<div class="btn">
																<span>Upload</span>
																<input type="file" multiple>
															</div>
															<div class="file-path-wrapper">
																<input class="file-path validate" type="text" name="visa_file" placeholder="Upload file">
															</div>
														</div>
														<div class="uploaded-file" id="visa_file" data-type="file"></div>										
												</div>										
											</div>												
										</div>
										
										<div class="row">
												<div class="col s6">
                                                        <label>Upload Flight Ticket</label>
                                                        <div class="file-field input-field">
                                                                <div class="btn">
                                                                        <span>Upload</span>
                                                                        <input type="file" multiple>
                                                                </div>
                                                                <div class="file-path-wrapper">
                                                                        <input class="file-path validate" name="ticket_proof" type="text"
                                                                                placeholder="Upload file">
                                                                </div>
                                                        </div>
														<div class="uploaded-file" id="ticket_proof" data-type="file"></div>
												</div>												
                                        </div>
                                        <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                <select name="enrollment_stat" id="enrollment_stat" class="formSelect">
                                                                        <option value="" disabled selected>Your Enrollment Status</option>
                                                                        <option value="ENROLLED">ENROLLED </option>
                                                                        <option value="NOT-ENROLLED">NOT ENROLLED</option>

                                                                </select>
                                                                <label for="">Enrollment Status</label>
                                                        </div>
												</div>
                                                 <div id="enrolled" class="col s6" style="display: none">
													<div class="input-field ">
															<input id="enrollment_num" type="text" name="enrollment_num" class="validate">
															<label for="">Enrollment Number</label>

													</div>
                                                </div>
                                        </div>
                                        <div class="row">
                                                <div class="col s12">
                                                        <div class="input-field ">
                                                                <select name="placement_through" id="placement_through" class="formSelect">
                                                                        <option value="" disabled selected>Your Placement Agent</option>
                                                                        <option value="1">AGENT 1 </option>
                                                                        <option value="2">AGENT 2</option>
                                                                        <option value="3">AGENT 3</option>
                                                                </select>
                                                                <label for="">Placement Through</label>
                                                        </div>


                                                </div>
                                        </div>
                                        <div class="row">
                                                <div class="col s12">
                                                        <div class="input-field ">
                                                                <select name="invoice_stat" id="invoice_stat" class="formSelect">
                                                                        <option value="" disabled selected>Your Invoice Status</option>
                                                                        <option value="SUBMITTED">SUBMITTED </option>
                                                                        <option value="NOT-SUBMITTED">NOT SUBMITTED</option>



                                                                </select>
                                                                <label for="">Invoice Status</label>
                                                        </div>
                                                        <div id="invoiced" style="display: none">
                                                                <br>

                                                                <label>Upload Invoice</label>
                                                                <div class="file-field input-field">
                                                                        <div class="btn">
                                                                                <span>Upload</span>
                                                                                <input type="file"  multiple>
                                                                        </div>
                                                                        <div class="file-path-wrapper">
                                                                                <input class="file-path validate"
                                                                                        type="text" name="invoice_proof"
                                                                                        placeholder="Upload file">
                                                                        </div>
                                                                </div>
																<div class="uploaded-file" id="invoice_proof" data-type="file"></div>

                                                        </div>

                                                </div>
                                        </div>
                                        <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
                                                                        <select name="payment_stat" id="payment_stat" class="formSelect">
                                                                                <option value="" disabled selected>Your Payment Status</option>
                                                                                <option value="RECEIVED">RECEIVED </option>
                                                                                <option value="NOT-RECEIVED">NOT RECEIVED</option>
                                                                        </select>
                                                                        <label for="">Payment Received</label>
                                                                </div>
        
        
                                                        </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
                                                                        <select name="sub_agent" id="sub_agent" class="formSelect">
                                                                                <option value="" disabled selected>Select Sub Agent</option>
                                                                                <option value="1">SUB AGENT 1 </option>
                                                                                <option value="2">SUB AGENT 2</option>
                                                                                <option value="3">SUB AGENT 3</option>
                                                                                <option value="4">NO SUB AGENT </option>

                                                                        </select>
                                                                        <label for="">Sub Agent</label>
                                                                </div>
        
        
                                                        </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
                                                                        <select name="sub_agent_invoice" id="sub_agent_invoice" class="formSelect">
                                                                                <option value="" disabled selected>Select Sub Agent Invoice Status</option>
                                                                                <option value="RECEIVED">RECEIVED  </option>
                                                                                <option value="NOT-RECEIVED">NOT RECEIVED</option>
                                                                                <option value="NOT-APPLICABLE">NOT APPLICABLE</option>

                                                                        </select>
                                                                        <label for="">Invoice from Sub Agent</label>
                                                                </div>
        
        
                                                        </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field">
                                                                        <select name="sub_agent_paymnt_stat" id="sub_agent_paymnt_stat" class="formSelect">
                                                                                <option value="" disabled selected>Select SubAgent Payment Status</option>
                                                                                <option value="PAID">PAID  </option>
                                                                                <option value="NOT-PAID">NOT PAID</option>
                                                                                <option value="NOT-APPLICABLE">NOT APPLICABLE</option>

                                                                        </select>
                                                                        <label for="">SubAgent Payment Status</label>
                                                                </div>
        
        
                                                        </div>
                                                </div>

                                                <div class="row">
                                                        <div class="col s12">
                                                                <label>Additional Documents File Upload</label>
                                                                <div class="file-field input-field">
                                                                        <div class="btn">
                                                                                <span>Upload</span>
                                                                                <input type="file" multiple>
                                                                        </div>
                                                                        <div class="file-path-wrapper">
                                                                                <input class="file-path validate"
                                                                                        type="text" name="additional_proofs"
                                                                                        placeholder="Upload file">
                                                                        </div>
                                                                </div>
																<div class="uploaded-file" id="additional_proofs" data-type="file"></div>
        
                                                        </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
                                                                                <textarea id="last_communication" name="last_communication" class="materialize-textarea"></textarea>
                                                                                <label>Last Communication with Candidate</label>

                                                                </div>
        
                                                        </div>
                                                </div>
                                </div>
                                <div class="controls">
                                        <a class="mgbtn2  back_button">Back</a>
                                                        <input id="form_name" type="hidden" class="validate" name="type_of" value="immigration_history">
                                                        <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                                                      {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
  
                        
</div>

                