<div id="tab_1" class="tab-pane active" >
                        
<div class="content-head">
                                        
                                        <h4>Personal Information</h4>
                                        <!-- <div class="id" id="nurse_id">Nurse ID : <b>#12675</b></div> -->
                        </div>
                        {!! Form::open(['action' => 'LA\NursesController@store', 'id' => 'personal-add-form']) !!}
                <div class="pane" >

				<div class="row">
                                        <div class="input-field  col s3">
										<select name="title" class="formSelect">
											<option value="Mr"> Mr. </option>
											<option value="Mrs."> Mrs. </option>
											<option value="Ms."> Ms. </option>
											<option value="Miss."> Miss. </option>
											<option value="Dr."> Dr. </option>
											<option value="Professor."> Professor. </option>
											<option value="Sister."> Sister. </option>
											<option value="Father."> Father. </option>
											<option value="Major."> Major. </option>
											<option value="Reverend."> Reverend. </option>
											<option value="Pastor."> Pastor. </option>
											<option value="Captain."> Captain. </option>
											<option value="Rabbi."> Rabbi. </option>
											<option value="Mx."> Mx. </option>
										</select>
											<label for="title">Prefix</label>
                                        </div>
                                        <div class="input-field col s3">
                                                        <!-- <i class="material-icons prefix">account_circle</i> -->
                                                        <input name="first_name" required id="first_name" type="text">
                                                        <label for="first_name">First Name</label>
                                           
                                                        <span class="helper-text" data-error="wrong"
                                                        data-success="right">As per Passport</span>
                                        </div>


                                                <div class="input-field col s3">
                                                                <input id="middle_name" name="middle_name" type="text" class="validate">
                                                                <label for="middle_name">Middle Name</label>
                                                                <span class="helper-text" data-error="wrong"
                                                                data-success="right">As per Passport</span>
                                                </div>


                                                        <div class="input-field col s3 ">
                                                                        <input id="sur_name" name="sur_name" type="text" class="validate">
                                                                        <label for="sur_name">Sur Name</label>
                                                                        <span class="helper-text" data-error="wrong"
                                                                        data-success="right">As per Passport</span>
                                                        </div>



                        </div>
                        
                        <div class="row">
							<div class="col s6 ipfield">
								<label>Upload profile image</label>
								<div class="file-field input-field">
									<div class="btn9">
											<span>Browse</span>
											<input type="file"><i class="fa fa-cloud-upload"></i>
									</div>
									<div class="file-path-wrapper">
										<input class="file-path validate" name="profile_image" type="text">
										<!-- <input class="file-path validate" name="profile_image" type="text" id="nurse-pro-img"> -->

											<div>
											</div>
							
									</div>
								</div>
								<div class="uploaded-file" id="profile_image" data-type="file"></div>
								<!-- <i title="Remove Image" class="remv-icon fa fa-times" id="nurse-profile-remove-img"></i> -->
							</div>


							


							<div class=" col s6 ipfield">
                                        <div class="input-field"> 
						<select name="nationality" id="nationality" class="searchSelect">
						    <option value="">Select </option>
						    <option value="Afghan"> Afghan </option>
	                                            <option value="Albanian"> Albanian </option>
	                                            <option value="Algerian"> Algerian </option>
	                                            <option value="American"> American </option>
	                                            <option value="Andorran"> Andorran </option>
	                                            <option value="Angolan"> Angolan </option>
	                                            <option value="Anguillan"> Anguillan </option>
	                                            <option value="Citizen of Antigua and Barbuda"> Citizen of Antigua and Barbuda </option>
	                                            <option value="Argentine"> Argentine </option>
	                                            <option value="Armenian"> Armenian </option>
	                                            <option value="Australian"> Australian </option>
	                                            <option value="Austrian"> Austrian </option>
	                                            <option value="Azerbaijani"> Azerbaijani </option>
	                                            <option value="Bahamian"> Bahamian </option>
	                                            <option value="Bahraini"> Bahraini </option>
	                                            <option value="Bangladeshi"> Bangladeshi </option>
	                                            <option value="Barbadian"> Barbadian </option>
	                                            <option value="Belarusian"> Belarusian </option>
	                                            <option value="Belgian"> Belgian </option>
	                                            <option value="Belizean"> Belizean </option>
	                                            <option value="Beninese"> Beninese </option>
	                                            <option value="Bermudian"> Bermudian </option>
	                                            <option value="Bhutanese"> Bhutanese </option>
	                                            <option value="Bolivian"> Bolivian </option>
	                                            <option value="Citizen of Bosnia and Herzegovina"> Citizen of Bosnia and Herzegovina </option>
	                                            <option value="Botswanan"> Botswanan </option>
	                                            <option value="Brazilian"> Brazilian </option>
	                                            <option value="British"> British </option>
	                                            <option value="British-Virgin-Islander"> British Virgin Islander </option>
	                                            <option value="Bruneian"> Bruneian </option>
	                                            <option value="Bulgarian"> Bulgarian </option>
	                                            <option value="Burkinan"> Burkinan </option>
	                                            <option value="Burmese"> Burmese </option>
	                                            <option value="Burundian"> Burundian </option>
	                                            <option value="Cambodian"> Cambodian </option>
	                                            <option value="Cameroonian"> Cameroonian </option>
	                                            <option value="Canadian Cape Verdean"> Canadian Cape Verdean </option>
	                                            <option value="Cayman Islander"> Cayman Islander </option>
	                                            <option value="Central African"> Central African </option>
	                                            <option value="Chadian"> Chadian </option>
	                                            <option value="Chilean"> Chilean </option>
	                                            <option value="Chinese"> Chinese </option>
	                                            <option value="Colombian"> Colombian </option>
	                                            <option value="Comoran Congolese (Congo)"> Comoran Congolese (Congo) </option>
	                                            <option value="Congolese (DRC)"> Congolese (DRC) </option>
	                                            <option value="Cook Islander"> Cook Islander </option>
	                                            <option value="Costa Rican"> Costa Rican </option>
	                                            <option value="Croatian"> Croatian </option>
	                                            <option value="Cuban"> Cuban </option>
	                                            <option value="Cymraes"> Cymraes </option>
	                                            <option value="Cymro"> Cymro </option>
	                                            <option value="Cypriot"> Cypriot </option>
	                                            <option value="Czech"> Czech </option>
	                                            <option value="Danish"> Danish </option>
	                                            <option value="Djiboutian"> Djiboutian </option>
	                                            <option value="Dominican"> Dominican </option>
	                                            <option value="Citizen of the Dominican Republic"> Citizen of the Dominican Republic </option>
	                                            <option value="Dutch"> Dutch </option>
	                                            <option value="East Timorese"> East Timorese </option>
	                                            <option value="Ecuadorean"> Ecuadorean </option>
	                                            <option value="Egyptian"> Egyptian </option>
	                                            <option value="Emirati"> Emirati </option>
	                                            <option value="English"> English </option>
	                                            <option value="Equatorial Guinean"> Equatorial Guinean </option>
	                                            <option value="Eritrean"> Eritrean </option>
	                                            <option value="Estonian"> Estonian </option>
	                                            <option value="Ethiopian"> Ethiopian </option>
	                                            <option value="Faroese"> Faroese </option>
	                                            <option value="Fijian"> Fijian </option>
	                                            <option value="Filipino"> Filipino </option>
	                                            <option value="Finnish"> Finnish </option>
	                                            <option value="French"> French </option>
	                                            <option value="Gabonese"> Gabonese </option>
	                                            <option value="Gambian"> Gambian </option>
	                                            <option value="Georgian"> Georgian </option>
	                                            <option value="German"> German </option>
	                                            <option value="Ghanaian"> Ghanaian </option>
	                                            <option value="Gibraltarian"> Gibraltarian </option>
	                                            <option value="Greek"> Greek </option>
	                                            <option value="Greenlandic"> Greenlandic </option>
	                                            <option value="Grenadian"> Grenadian </option>
	                                            <option value="Guamanian"> Guamanian </option>
	                                            <option value="Guatemalan"> Guatemalan </option>
	                                            <option value="Citizen of Guinea-Bissau"> Citizen of Guinea-Bissau </option>
	                                            <option value="Guinean Guyanese"> Guinean Guyanese </option>
	                                            <option value="Haitian"> Haitian </option>
	                                            <option value="Honduran"> Honduran </option>
	                                            <option value="Hong Konger"> Hong Konger </option>
	                                            <option value="Hungarian"> Hungarian </option>
	                                            <option value="Icelandic"> Icelandic </option>
	                                            <option value="Indian"> Indian </option>
	                                            <option value="Indonesian"> Indonesian </option>
	                                            <option value="Iranian"> Iranian </option>
	                                            <option value="Iraqi"> Iraqi </option>
	                                            <option value="Irish"> Irish </option>
	                                            <option value="Israeli"> Israeli </option>
	                                            <option value="Italian"> Italian </option>
	                                            <option value="Ivorian"> Ivorian </option>
	                                            <option value="Jamaican"> Jamaican </option>
	                                            <option value="Japanese"> Japanese </option>
	                                            <option value="Jordanian"> Jordanian </option>
	                                            <option value="Kazakh"> Kazakh </option>
	                                            <option value="Kenyan"> Kenyan </option>
	                                            <option value="Kittitian"> Kittitian </option>
	                                            <option value="Citizen of Kiribati"> Citizen of Kiribati </option>
	                                            <option value="Kosovan"> Kosovan </option>
	                                            <option value="Kuwaiti"> Kuwaiti </option>
	                                            <option value="Kyrgyz"> Kyrgyz </option>
	                                            <option value="Lao"> Lao </option>
	                                            <option value="Latvian"> Latvian </option>
	                                            <option value="Lebanese"> Lebanese </option>
	                                            <option value="Liberian"> Liberian </option>
	                                            <option value="Libyan"> Libyan </option>
	                                            <option value="Liechtenstein citizen"> Liechtenstein citizen </option>
	                                            <option value="Lithuanian"> Lithuanian </option>
	                                            <option value="Luxembourger"> Luxembourger </option>
	                                            <option value="Macanese"> Macanese </option>
	                                            <option value="Macedonian"> Macedonian </option>
	                                            <option value="Malagasy"> Malagasy </option>
	                                            <option value="Malawian"> Malawian </option>
	                                            <option value="Malaysian"> Malaysian </option>
	                                            <option value="Maldivian"> Maldivian </option>
	                                            <option value="Malian"> Malian </option>
	                                            <option value="Maltese"> Maltese </option>
	                                            <option value="Marshallese"> Marshallese </option>
	                                            <option value="Martiniquais"> Martiniquais </option>
	                                            <option value="Mauritanian"> Mauritanian </option>
	                                            <option value="Mauritian"> Mauritian </option>
	                                            <option value="Mexican"> Mexican </option>
	                                            <option value="Micronesian"> Micronesian </option>
	                                            <option value="Moldovan"> Moldovan </option>
	                                            <option value="Monegasque"> Monegasque </option>
	                                            <option value="Mongolian"> Mongolian </option>
	                                            <option value="Montenegrin"> Montenegrin </option>
	                                            <option value="Montserratian"> Montserratian </option>
	                                            <option value="Moroccan"> Moroccan </option>
	                                            <option value="Mosotho"> Mosotho </option>
	                                            <option value="Mozambican"> Mozambican </option>
	                                            <option value="Namibian"> Namibian </option>
	                                            <option value="Nauruan"> Nauruan </option>
	                                            <option value="Nepalese"> Nepalese </option>
	                                            <option value="New Zealander"> New Zealander </option>
	                                            <option value="Nicaraguan"> Nicaraguan </option>
	                                            <option value="Nigerian"> Nigerian </option>
	                                            <option value="Nigerien"> Nigerien </option>
	                                            <option value="Niuean"> Niuean </option>
	                                            <option value="North Korean"> North Korean </option>
	                                            <option value="Northern Irish"> Northern Irish </option>
	                                            <option value="Norwegian"> Norwegian </option>
	                                            <option value="Omani"> Omani </option>
	                                            <option value="Pakistani"> Pakistani </option>
	                                            <option value="Palauan"> Palauan </option>
	                                            <option value="Palestinian"> Palestinian </option>
	                                            <option value="Panamanian"> Panamanian </option>
	                                            <option value="Papua New Guinean"> Papua New Guinean </option>
	                                            <option value="Paraguayan"> Paraguayan </option>
	                                            <option value="Peruvian"> Peruvian </option>
	                                            <option value="Pitcairn Islander"> Pitcairn Islander </option>
	                                            <option value="Polish"> Polish </option>
	                                            <option value="Portuguese"> Portuguese </option>
	                                            <option value="Prydeinig Puerto Rican"> Prydeinig Puerto Rican </option>
	                                            <option value="Qatari"> Qatari </option>
	                                            <option value="Romanian"> Romanian </option>
	                                            <option value="Russian"> Russian </option>
	                                            <option value="Rwandan"> Rwandan </option>
	                                            <option value="Salvadorean"> Salvadorean </option>
	                                            <option value="Sammarinese"> Sammarinese </option>
	                                            <option value="Samoan"> Samoan </option>
	                                            <option value="Sao Tomean"> Sao Tomean </option>
	                                            <option value="Saudi Arabian"> Saudi Arabian </option>
	                                            <option value="Scottish"> Scottish </option>
	                                            <option value="Senegalese"> Senegalese </option>
	                                            <option value="Serbian"> Serbian </option>
	                                            <option value="Citizen of Seychelles"> Citizen of Seychelles </option>
	                                            <option value="Sierra Leonean"> Sierra Leonean </option>
	                                            <option value="Singaporean"> Singaporean </option>
	                                            <option value="Slovak"> Slovak </option>
	                                            <option value="Slovenian Solomon Islander"> Slovenian Solomon Islander </option>
	                                            <option value="Somali"> Somali </option>
	                                            <option value="South African"> South African </option>
	                                            <option value="South Korean"> South Korean </option>
	                                            <option value="South Sudanese"> South Sudanese </option>
	                                            <option value="Spanish"> Spanish </option>
	                                            <option value="Sri Lankan"> Sri Lankan </option>
	                                            <option value="St Helenian"> St Helenian </option>
	                                            <option value="St Lucian"> St Lucian </option>
	                                            <option value="Stateless"> Stateless </option>
	                                            <option value="Sudanese"> Sudanese </option>
	                                            <option value="Surinamese"> Surinamese </option>
	                                            <option value="Swazi"> Swazi </option>
	                                            <option value="Swedish"> Swedish </option>
	                                            <option value="Swiss"> Swiss </option>
	                                            <option value="Syrian"> Syrian </option>
	                                            <option value="Taiwanese"> Taiwanese </option>
	                                            <option value="Tajik"> Tajik </option>
	                                            <option value="Tanzanian"> Tanzanian </option>
	                                            <option value="Thai"> Thai </option>
	                                            <option value="Togolese"> Togolese </option>
	                                            <option value="Tongan"> Tongan </option>
	                                            <option value="Trinidadian"> Trinidadian </option>
	                                            <option value="Tristanian"> Tristanian </option>
	                                            <option value="Tunisian"> Tunisian </option>
	                                            <option value="Turkish"> Turkish </option>
	                                            <option value="Turkmen"> Turkmen </option>
	                                            <option value="Turks and Caicos Islander"> Turks and Caicos Islander </option>
	                                            <option value="Tuvaluan"> Tuvaluan </option>
	                                            <option value="Ugandan"> Ugandan </option>
	                                            <option value="Ukrainian"> Ukrainian </option>
	                                            <option value="Uruguayan"> Uruguayan </option>
	                                            <option value="Uzbek"> Uzbek </option>
	                                            <option value="Vatican citizen Citizen of Vanuatu"> Vatican citizen Citizen of Vanuatu </option>
	                                            <option value="Venezuelan"> Venezuelan </option>
	                                            <option value="Vietnamese"> Vietnamese </option>
	                                            <option value="Vincentian"> Vincentian </option>
	                                            <option value="Wallisian"> Wallisian </option>
	                                            <option value="Welsh"> Welsh </option>
	                                            <option value="Yemeni"> Yemeni </option>
	                                            <option value="Zambian"> Zambian </option>
	                                            <option value="Zimbabwean"> Zimbabwean </option>	
						</select>		
						<label for="nationality" style="margin-top: -40px;font-size: 13px;">Nationality </label>
                                           
                                        </div>
                                </div>
						</div>			
						<div class="row">
							<div class="col s6">
							<p>Gender</p>
								<p>
									<label>
										<input name="gender" type="radio" value="MALE" checked/>
										<span>MALE</span>
									</label>
									<label>
										<input name="gender" type="radio"  value="FEMALE" />
										<span>FEMALE</span>
									</label>
							  </p>
							</div>	  
							<div class="input-field col s6">
                                               <!--<i class="material-icons prefix">calendar_today</i>-->

                                        <!-- <input id="dob" required  name="dob" type="text" class="datepicker" /> -->
                                        <input id="dob" name="dob" type="text" class="dateofbirth">
                                        <label for="dob">Date of Birth</label>
                                        <span class="helper-text" data-error="wrong"   data-success="right">As per Passport</span>
                        	</div>

                         </div>

						 <h4 class="sub_title">Home Address As Per Passport</h4>
						
					    <div class="row">
							<div class="input-field col s6">
							   <input type="text" id="home_address" name="home_address" required>
							   <label for="address">Street Address</label>
							</div>
							<div class="input-field col s6">
							   <input type="text" id="address_2" name="address_2" required>
							   <label for="address_2">Street Address Line2</label>
							</div>						
						</div>
					
					    <div class="row">
							<div class="input-field col s6">
							   <input type="text" id="city" name="city" required>
							   <label for="city">City</label>
							</div>
							<div class="input-field col s6">
							   <input type="text" id="state" name="state" required>
							   <label for="state">State / Province</label>
							</div>						
						</div>
						
					    <div class="row">
							<div class="input-field col s6">
							   <input type="text" id="zip_code" name="zip_code" required>
							   <label for="zip_code">Postal / Zip code</label>
							</div>
							<div class="input-field col s6">
								<select name="country" id="country" class="searchSelect" required>
								  <option value=""> Please Select </option>
								  	<?php
									foreach($countries as $countriesData) {
									echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
									}
									?>
								</select>							   
							   <label for="country" style="margin-top: -40px;font-size: 13px;">Country</label>
							</div>						
						</div>
						<div class="row">
							
							<div class="col s12">
							<p>Is this your correspondence address?</p>
								<p>
									<label>
										<input name="correspondence_address_flag" type="radio" value="YES" checked/>
										<span>YES</span>
									</label>
									<label>
										<input name="correspondence_address_flag" type="radio"  value="NO" />
										<span>NO</span>
									</label>
							  </p>
							</div>	  


						</div>
						
					    <div class="row" id="row_corres_1" style="display:none" >
							<h4 class="sub_title" style="margin-bottom: 39px;">Correspondence address</h4>
							<div class="input-field col s6">
							   <input type="text" id="correspondence_addr_1" name="correspondence_addr_1" >
							   <label for="correspondence_addr_1">Street Address</label>
							</div>
							<div class="input-field col s6">
							   <input type="text" id="correspondence_addr_2" name="correspondence_addr_2" >
							   <label for="correspondence_addr_2">Street Address Line2</label>
							</div>						
						</div>
					
					    <div class="row" id="row_corres_2" style="display:none">
							<div class="input-field col s6">
							   <input type="text" id="correspondence_city" name="correspondence_city" >
							   <label for="city">City</label>
							</div>
							<div class="input-field col s6">
							   <input type="text" id="correspondence_state" name="correspondence_state" >
							   <label for="state">State / Province</label>
							</div>						
						</div>
						
					    <div class="row"  id="row_corres_3" style="display:none" >
							<div class="input-field col s6">
							   <input type="text" id="correspondence_zip_code" name="correspondence_zip_code" >
							   <label for="zip_code">Postal / Zip code</label>
							</div>
							<div class="input-field col s6">
								<select name="correspondence_country" id="correspondence_country" class="searchSelect">
								<option value=""> Please Select </option>
								<?php
								foreach($countries as $countriesData) {
								echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
								}
								?>
								</select>							   
							   <label for="country" style="margin-top: -40px;font-size: 13px;">Country</label>
							</div>						
						</div>

						<div class="row">
							<div class="col s8">
								<div class="input-field">
									<select name="lived_othe_addr" id="lived_othe_addr" class="formSelect">
										<option value="0">NO</option>
										<option value="1">YES</option>
									</select>											
									<label for="lived_othe_addr">Have you lived at any other address in past 2 years ? </label>
								</div>
							</div>											
						</div>		

						<div id="other-addr-wrapp">
							<div class="row" id="row_other_1" style="display:none" >
								<div class="input-field col s6">
								   <input type="text" id="other_address_1" name="other_address_1" >
								   <label for="other_address_1">Street Address</label>
								</div>
								<div class="input-field col s6">
								   <input type="text" id="other_address_2" name="other_address_2" >
								   <label for="other_address_2">Street Address Line2</label>
								</div>						
							</div>
						
							<div class="row" id="row_other_2" style="display:none">
								<div class="input-field col s6">
								   <input type="text" id="other_city" name="other_city" >
								   <label for="other_city">City</label>
								</div>
								<div class="input-field col s6">
								   <input type="text" id="other_state" name="other_state" >
								   <label for="other_state">State / Province</label>
								</div>						
							</div>
							
							<div class="row"  id="row_other_3" style="display:none" >
								<div class="input-field col s6">
								   <input type="text" id="other_zip_code" name="other_zip_code" >
								   <label for="other_zip_code">Postal / Zip code</label>
								</div>
								<div class="input-field col s6">
									<select name="other_country" id="other_country" class="searchSelect">
									  <option value=""> Please Select </option>
									  	<?php
										foreach($countries as $countriesData) {
										echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
										}
										?>
									</select>							   
								   <label for="other_country" style="margin-top: -40px;font-size: 13px;">Country</label>
								</div>						
							</div>
							<div class="row" id="row_other_4" style="display:none">
									<div class=" col s8 ipfield">
											<div class="input-field">
															<input name="start_living_addr" id="start_living_addr" type="text" data-status="1" data-id="0" data-count="0" class="start_living_addr0 datepicker living_addr">
															<label for="start_living_addr">When did you start living at this address ?  </label>
											   
											</div>
									</div>
									<div class=" col s8 ipfield">
											<div class="input-field">
															<input name="stop_living_addr" id="stop_living_addr" data-id="0" type="text" class="stop_living_addr0 datepicker living_addr" >
															<label for="stop_living_addr">When did you stop living at this address ? </label>
											   
											</div>
									</div>
							</div>
							<input type="hidden" value="otheraddress" name="formtype" class="formtype">
							<!-- <input type="hidden" value="0" class="wrapper-count-living"> -->
							<input type="hidden" value="<?php echo $wrp_count_living;  ?>" class="wrapper-count-living">								
						</div>
						<div class="row">
								<?php
								if($studentId == 0 || $studentId == '') {
								?>
								<div class="col s6 ipfield">
									<div class="input-field">
										<!-- <i class="material-icons prefix">mail</i> -->
										<?php
										if($studentId == 0 || $studentId == '') {
										?>
											<input name="email" id="email" required type="text">
										<?php } else { ?>
											<input type="text" value="" readonly>
										<?php } ?>
										<label for="email">Email</label>
									</div>
								</div>
								<?php } ?>
								<div class=" col s6 ipfield">
                                        <div class="input-field">
                                                        <!-- <input name="mobile_phone_text" id="mobile_phone_text" type="text"> -->
                                                        <input name="mobile" id="mobile" type="number">
                                                        <label for="mobile">Mobile number </label>
                                           
                                        </div>
                                </div>

						</div>
						<div class="row">
								<div class=" col s6 ipfield">
										<div class="input-field">
														<!--<i class="material-icons prefix">mail</i>-->
														<!-- <input name="whats_app_no" id="whats_app_no"  type="text"> -->
														<input id="whatsapp" name="whatsapp" type="number" required>
														<label for="whats_app_no">WhatsApp Number</label>
										   
										</div>
								</div>
								<div class=" col s6 ipfield">
                                        <div class="input-field">
                                                        <input name="imo_no" id="imo_no"  type="text">
                                                        <label for="imo_no">IMO Number</label>
                                           
                                        </div>
                                </div>

						</div>
						<div class="row">
								<div class=" col s6 ipfield">
										<div class="input-field">
														<!--<i class="material-icons prefix">mail</i>-->
														<input name="btm_number" id="btm_number"  type="text">
														<label for="btm_number">Botim Number</label>
										   
										</div>
								</div>
								<div class=" col s6 ipfield">
                                        <div class="input-field">
                                                        <input name="skype_id" id="skype_id"  type="text">
                                                        <label for="skype_id">Skype ID</label>
                                           
                                        </div>
                                </div>

						</div>
						<div class="row">
							<div class="col s6 ipfield">
								<label>Skype ID Profile Screenshots Upload</label>
								<div class="file-field input-field">
														<div class="btn9">
																<span>Upload</span>
																<input type="file"><i class="fa fa-cloud-upload"></i> 
														</div>
												<div class="file-path-wrapper">
														<input class="file-path" name="skype_picture" type="text">

														<div>
														</div>
										
												</div>
								</div>

								<div class="uploaded-file" id="skype_picture" data-type="file"></div>								
								
								
							</div>
							<div class=" col s6 ipfield">
                                        <div class="input-field">
                                                        <input name="passport_no" id="passport_no" required type="text">
                                                        <label for="passport_no">Passport Number </label>
                                           
                                        </div>
                            </div>
						</div>			
						<div class="row">
							<div class=" col s6 ipfield">
                                        <div class="input-field">

										<input id="date_issue" required  name="date_issue" type="text" class="datepicker" />
                                        <label for="date_issue">Date of Issue</label>

										</div>
							</div>
							<div class=" col s6 ipfield">
                                        <div class="input-field">
										<input id="date_expiry" required  name="date_expiry" type="text" class="datepicker" />
                                        <label for="dob">Date of Expiry</label>

										</div>
							</div>

						</div>

						<div class="row">
								<div class="col s6">
										<label>Upload Passport Front Page</label>
										<div class="file-field input-field">
																<div class="btn9">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
														<div class="file-path-wrapper">
																<input class="file-path validate" name="passport_upload" type="text">

																<div>
																</div>
												
														</div>
										</div>

										<div class="uploaded-file" id="passport_upload" data-type="file"></div>
								</div>
								
								<div class="col s6">
										<label>Upload Passport Back Page</label>
										<div class="file-field input-field">
																<div class="btn9">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
														<div class="file-path-wrapper">
																<input class="file-path validate" name="passport_back" type="text">

																<div>
																</div>
												
														</div>
										</div>

										<div class="uploaded-file" id="passport_back" data-type="file"></div>
								</div>
						</div>	
						


						<div class="row">
							<div class="col s12">
							<p>Do you have any disability or other condition which might necessitate special
arrangement or facilities ?</p>
								<p>
									<label>
										<input name="any_disability" type="radio" value="YES" />
										<span>YES</span>
									</label>
									<label>
										<input name="any_disability" type="radio"  value="NO" checked />
										<span>NO</span>
									</label>
							  </p>
							</div>	  
							
                         </div>


						 <div class="row" id="row_disability" style="display:none" >
							<div class=" col s12 ">
                                        <div class="input-field">

										<textarea name="disability_comment" class="materialize-textarea" id="disability_comment"></textarea>
                                        <label for="disability_comment" class="">Disability Comment</label>

										</div>
							</div>
							
						</div>

											 
                </div>
                <div class="controls">
                                <a class="mgbtn2">Back</a>
                                <!-- <a class="waves-effect waves-light btn" type="submit"><i class="material-icons left">save</i>SAVE</a> -->
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="personal_information">
                                 <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                              
                                {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                        </div>
                        {{ Form::close() }}
</div>