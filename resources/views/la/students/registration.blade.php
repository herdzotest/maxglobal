<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Varela+Round&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
		<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/css/bootstrap.css') }}"/>
		<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/css/AdminLTE.css') }}"/>
		<!-- Date Picker -->
		<link rel="stylesheet" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}"/>
		
        <title>Zodocs</title>
</head>
<body class="hold-transition login-page mg-login ">
    <div class="login-banner regbanner">
        <span>
            <h3>
                STUDY. <br>
                WORK. <br>
                LIVE.
            </h3>
        </span>
    </div>
    <div class="login-box regbox">
    <img class="prop"  src="{{ asset('la-assets/img/prop1.png') }}" alt="">
        <div class="login-wrap">
            
        <div class="login-logo">
            <a href="{{ url('/home') }}"><img src="{{asset('la-assets/img/zodocs_logo.png')}}" alt=""></a>
        </div>

        </div>


        @if (count($errors) > 0)
                <div class="alert alert-danger mt20">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                                @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                @endforeach
                        </ul>
                </div>
        @endif
        @if (session('status'))
                <div class="alert alert-success mt20">{{ session('status') }}</div>
        @endif

        <div class="login-box-body reg-box-body">
                 <p class="login-box-msg">Welcome to Zodocs</p>
                 <p class="loginp2">Register Now</p>

				<form action="" method="post" id="registration-add-form">
				{{ csrf_field() }}
               <div class="pane clearfix">
                       <!-- <div class="row">
                               <div class=" col-md-6 pr5">
                                       <div class="form-group">
                                       <label for="first_name">First Name</label>
                                        <input id="first_name" name="first_name" type="text" class="form-control" required class="validate" value="" >
                                       </div>
                               </div>
                               <div class=" col-md-4 pl5 pr5">
                                        <div class="form-group">
                                        <label for="middle_name">Middle Name</label>
                                        <input id="middle_name" name="middle_name" type="text" class="form-control" class="validate" value="" >
                                        </div>
                                </div>
                                <div class=" col-md-4 pl5">
                                        <div class="form-group">
                                        <label for="sur_name">Sur Name</label>
                                        <input id="sur_name" name="sur_name" type="text" class="form-control" class="validate" >
                                        </div>
                                </div>
                       </div> -->

                        <div class="row">
                               <div class=" col-md-6 pr5">
                                       <div class="form-group">
                                       <label for="first_name">Name *</label>
                                        <input id="first_name" autocomplete="off" name="first_name" type="text" class="form-control" required class="validate" value="" >
                                       </div>
                               </div>
                               <div class=" col-md-6 pl5">
                                        <div class="form-group">
                                        <label for="dob">Date of Birth</label>
                                        <input id="dob" autocomplete="off" name="dob" type="text" class="form-control datepicker1" >
                                        </div>
                                </div>
                        </div>
                       <div class="row">               
                                <div class=" col-md-6 pr5">
                                        <div class="form-group">        
                                        <label for="email">Email *</label>
                                        <input id="email" autocomplete="off" required name="email" type="email" class="form-control" class="validate" >
                                        </div>
                                </div>
                                <div class=" col-md-6 pl5">
                                        <div class="form-group">
                                        <label for="mobile">Mobile *</label>
                                        <input id="mobile" autocomplete="off" min="10" name="mobile" type="number" class="form-control validate" required>
                                        </div>
                               </div>
                       </div>
                       <div class="row">
                               <div class=" col-md-6 pr5">
                                        <div class="form-group">
                                        <label for="whatsapp">Whatsapp *</label>
                                        <input id="whatsapp" autocomplete="off" min="10" name="whatsapp" type="number" class="form-control validate" required>
                                        </div>
                               </div>
                               <div class=" col-md-6 pl5">
                                        <div class="form-group">
                                        <label for="city">City / Place</label>
                                        <input id="city" autocomplete="off" name="city" type="text" class="form-control">
                                        </div>
                                </div>
                        </div>
                        <div class="row">
                               <div class=" col-md-6 pr5">
                                       <div class="form-group">
                                       <label for="pwd">Password *</label>
                                            <input id="pwd" required name="password" type="password" class="form-control validate" >
                                            <span toggle="#pwd" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                       </div>
                               </div>
                               <div class="col-md-6 pl5">
                                        <div class="form-group">
                                                <label for="email">I am *</label>
                                                <select id="user_type" required class="form-control">
                                                        <option value="">Select</option>
                                                        <option value="STUDENT">STUDENT</option>
                                                        <option value="MAIN_AGENTS">AGENT</option>
                                                        <option value="NURSE">NURSE</option>
                                                </select>
                                        </div>
                               </div>                                                      
                       </div>

                       <!--<div class="row">
                               <div class="col-md-6 pr5">
                               <div class="form-group">

                               <label for="dob">Date of Birth</label>
                                       <input id="dob" name="dob" type="text" class="form-control datepicker" >  
                               </div>
                               </div>
                               <div class=" col-md-6 pl5">
                                       <div class="form-group">
                                       <label for="email">E mail</label>

                                                       <input id="email" required name="email" type="email" class="form-control" class="validate" >
                                       </div>
                               </div>
                               <div class=" col-md-6 pr5">
                                       <div class="form-group">
                                       <label for="pwd">Password</label>
                                            <input id="pwd" required name="password" type="password" class="form-control" class="validate" >
                                       </div>
                               </div>
                               <div class="col-md-6 pl5">
									<div class="form-group">
										<label for="email">Iam</label>
										<select id="user_type" required class="form-control">
											<option value="">Select</option>
											<option value="STUDENT">STUDENT</option>
											<option value="MAIN_AGENTS">AGENT</option>
											<option value="NURSE">NURSE</option>
										</select>
									</div>
                               </div>							   

                       </div> -->

               </div>
               <div class="controls">
                       <div class="row">
                               <div class="col-md-3">
                               <input id="form_name" type="hidden" class="validate" name="type_of" value="personal_information">
				
                                <?php if(isset($_GET['id']) && $_GET['id'] != '') { ?>
                                        <input id="form_name" type="hidden" name="std_agent_id" value="<?php echo $_GET['id']; ?>">     
                                <?php } else { ?>
                                        <input id="form_name" type="hidden" name="std_agent_id" value="">
                                <?php } ?>	

                                <button type="submit" class=" btn btn-primary btn-block btn-flat loginbtn regbtn2" value="REGISTER">REGISTER</button>
                               </div>
                       </div>

                       </div>
                </form> 

        </div>
        <div class=" rstlink"  >
          Already Registered ? <a class="" href="{{ url('/login') }}">Login Now</a>

            </div>

    </div>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script>
$(function () {
        $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
                input.attr("type", "text");
        } else {
                input.attr("type", "password");
        }
        });

        $('.datepicker1').datepicker({
                autoclose: true,
                endDate: new Date(),
                format: 'yyyy-mm-dd'
        });

	$('.datepicker').datepicker({
		autoclose: true
	});
	$('body').on('change', '#user_type', function() {
		
		var value = $(this).val();
		if(value != '') {
			var saveUrl = (value == 'STUDENT') ? '{{ url("/registration_save") }}' : ((value == 'MAIN_AGENTS') ? '{{ url("/agents_save") }}' : '{{ url("/nurse_save") }}'); //alert(saveUrl); die();
			$("#registration-add-form").attr('action',saveUrl);
		}
	});		
});
</script>

</html>