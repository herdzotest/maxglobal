<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Google fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Outlined" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />

  <!-- Stylesheets -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/vendors.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/main.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/custom.css">

  <title>Course Finder</title>
  <link rel="icon" type="image/x-icon" href="la-assets/coursefinder/img/coursesCards/icons/zodocs_fav.png">
</head>

<body class="preloader-visible" data-barba="wrapper">

  <!-- preloader start -->
  <div class="preloader js-preloader">
    <div class="preloader__bg"></div>
  </div>
  <!-- preloader end -->

  <!-- barba container start -->
  <div class="barba-container" data-barba="container">


    <main class="main-content  ">

      <header data-anim="fade" data-add-bg="" class="header -type-4 -shadow bg-white js-header">


        <div class="header__container border-bottom-light py-10">
          <div class="row justify-between items-center">

            <div class="col-auto">
              <div class="header-left d-flex items-center">

                <div class="header__logo pr-30 xl:pr-20 md:pr-0">
                  <a data-barba href="<?php echo url('/coursefinder'); ?>">
                    <img src="la-assets/coursefinder/img/general/logo1.png" alt="logo">
                  </a>
                </div>

                <div class="header-menu js-mobile-menu-toggle pl-30 xl:pl-20">
                  <div class="header-menu__content">
                    <div class="mobile-bg js-mobile-bg"></div>

                    <div class="d-none xl:d-flex items-center px-20 py-20 border-bottom-light">
                      
                      <?php 
                      if(isset(Auth::user()->type)) { ?>
                        <a href="<?php echo url('/logout'); ?>" class="text-dark-1">Log out</a>
                        <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="text-dark-1 ml-30">My Account</a>
                      <?php } else { ?>
                        <a href="<?php echo url('/userlogin'); ?>" class="text-dark-1">Log in</a>
                        <a href="<?php echo url('/signup'); ?>" class="text-dark-1 ml-30">Sign up</a>
                      <?php } ?>

                    </div>

                    <div class="menu js-navList">
                      <ul class="menu__nav text-dark-1 -is-active">
                    
                    <li class="menu-item-has-children">
                      <a data-barba href="#">Home</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Home</a>
                        </li>
                        <li><a href="<?php echo url('/coursefinder'); ?>">Home 1</a></li>
                        <li><a href="<?php echo url('/coursefinder2'); ?>">Home 2</a></li>
                        <li><a href="<?php echo url('/coursefinder3'); ?>">Home 3</a></li>
                        <li><a href="<?php echo url('/coursefinder4'); ?>">Home 4</a></li>
                        <li><a href="<?php echo url('/coursefinder5'); ?>">Home 5</a></li>
                        <li><a href="<?php echo url('/coursefinder6'); ?>">Home 6</a></li>
                        <li><a href="<?php echo url('/coursefinder7'); ?>">Home 7</a></li>
                        <li><a href="<?php echo url('/coursefinder8'); ?>">Home 8</a></li>
                        <li><a href="<?php echo url('/coursefinder9'); ?>">Home 9</a></li>
                        <li><a href="<?php echo url('/coursefinder10'); ?>">Home 10</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/aboutus'); ?>">
                        About Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li class="menu-item-has-children">
                      <a data-barba href="#">Courses</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Courses</a>
                        </li>
                        <li><a href="<?php echo url('/coursesearch'); ?>">Course 1</a></li>
                        <li><a href="<?php echo url('/coursesearch2'); ?>">Course 2</a></li>
                        <li><a href="<?php echo url('/coursesearch3'); ?>">Course 3</a></li>
                        <li><a href="<?php echo url('/coursesearch4'); ?>">Course 4</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/blog'); ?>">
                        Blog <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/topcourses'); ?>">
                        Top Courses <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/contactus'); ?>">
                        Contact Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>
                      </ul>
                    </div>
                  </div>

                  <div class="header-menu-close" data-el-toggle=".js-mobile-menu-toggle">
                    <div class="size-40 d-flex items-center justify-center rounded-full bg-white">
                      <i class="fa fa-close"></i>
                    </div>
                  </div>

                  <div class="header-menu-bg"></div>
                </div>

              </div>
            </div>


            <div class="col-auto">
              <div class="header-right d-flex items-center">
                <div class="header-right__icons text-white d-flex items-center">
                  <div class="d-none xl:d-block pl-20 sm:pl-15">
                    <button class="text-dark-1 items-center" data-el-toggle=".js-mobile-menu-toggle">
                      <i class="fa fa-bars"></i>
                    </button>
                  </div>
                </div>

                <div class="header-right__buttons d-flex items-center ml-30 xl:ml-20 lg:d-none">
                  <!-- <a href="<?php //echo url('/userlogin'); ?>" class="button -underline text-purple-1">Log in</a>
                  <a href="<?php //echo url('/signup'); ?>" class="button h-50 px-30 -purple-1 -rounded text-white ml-20">Sign up</a> -->

                  <?php 
                  if(isset(Auth::user()->type)) { ?>
                    <a href="<?php echo url('/logout'); ?>" class="button -underline text-purple-1">Log out</a>
                    <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="button h-50 px-30 -purple-1 -rounded text-white ml-20">My Account</a>
                  <?php } else { ?>
                    <a href="<?php echo url('/userlogin'); ?>" class="button -underline text-purple-1">Log in</a>
                    <a href="<?php echo url('/signup'); ?>" class="button h-50 px-30 -purple-1 -rounded text-white ml-20">Sign up</a>
                  <?php } ?>

                </div>
              </div>
            </div>

          </div>
        </div>
      </header>


      <div class="content-wrapper  js-content-wrapper">


        <section data-anim-wrap class="masthead -type-4">
          <div class="container">
            <div class="row y-gap-50 justify-center items-center">
              <div class="col-xl-5 col-lg-6">
                <div class="masthead__content">
                  <h1 data-anim-child="slide-up delay-3" class="masthead__title">
                    Process of Overseas Education<br>
                    Visa CRM with <span class="text-purple-1 underline">ZODOCS</span>
                  </h1>
                  <p data-anim-child="slide-up delay-4" class="masthead__text pt-15">
                    Zodocs CRM software is highly rated in the <br>
                    study abroad education industry
                  </p>
                  <div data-anim-child="slide-up delay-5" class="masthead__button row x-gap-20 y-gap-20 pt-30">
                    <div class="col-auto"><a href="<?php echo url('/signup'); ?>" class="button -md -purple-1 -rounded text-white">Join For Free</a></div>
                    <div class="col-auto"><a href="<?php echo url('/coursesearch'); ?>" class="button -md -outline-dark-1 -rounded text-dark-1">Find Courses</a></div>
                  </div>
                </div>
              </div>

              <div class="col-xl-6 col-lg-6">
                <div data-anim-child="slide-up delay-6" class="masthead__image">
                  <img src="la-assets/coursefinder/img/home-5/masthead/bg.svg" alt="image">
                </div>
              </div>
            </div>
          </div>
        </section>

        

        <section class="layout-pt-lg layout-pb-md">
          <div data-anim-wrap class="container">
            <div class="tabs -pills js-tabs">
              <div class="row y-gap-20 justify-between items-end">
                <div class="col-auto">

                  <div class="sectionTitle ">
                    <h2 class="sectionTitle__title ">Our Courses</h2>
                  </div>

                </div>

              </div>

              <div class="tabs__content pt-60 lg:pt-50 js-tabs-content">

                <div class="tabs__pane -tab-item-1 is-active">
                  <div class="overflow-hidden js-section-slider" data-gap="30" data-slider-cols="xl-4 lg-3 md-2 sm-2">
                    <div class="swiper-wrapper">

                      <!--Course list -->
                      <?php 
                        if(count($courses) > 0) { 
                          foreach($courses as $result) {
                      ?>

                      <div class="swiper-slide">
                        <div data-anim-child="slide-up delay-1">

                          <a href="<?php echo url('/coursedetails/'.Crypt::encrypt($result->id)); ?>" class="coursesCard -type-1 ">
                            <div class="relative">
                              <div class="coursesCard__image overflow-hidden rounded-8">
                                <?php if($result->university_logo!='') { ?>
                                  <img class="w-1/1 course_logo" src="<?php echo $result->university_logo; ?>" alt="image">
                                <?php } else { ?>
                                  <img class="w-1/1 course_logo" src="la-assets/coursefinder/img/coursesCards/no-image.png" alt="image">
                                <?php } ?>
                                <div class="coursesCard__image_overlay rounded-8"></div>
                              </div>
                              <div class="d-flex justify-between py-10 px-10 absolute-full-center z-3">

                              </div>
                            </div>

                            <div class="h-100 pt-15">
                              <div class="d-flex items-center">
                                <div class="text-14 lh-1 text-yellow-1 mr-10"><?php echo $result->university_name; ?></div>
                              </div>

                              <div class="text-17 lh-15 fw-500 text-dark-1 mt-10"><?php echo $result->course_name; ?></div>

                              <div class="d-flex x-gap-10 items-center pt-10">

                                <div class="d-flex items-center">
                                  <div class="mr-8">
                                    <img src="la-assets/coursefinder/img/coursesCards/icons/location-icon.png" alt="icon">
                                  </div>
                                  <div class="text-14 lh-1"><?php echo $result->country_name; ?></div>
                                </div>

                                <div class="d-flex items-center">
                                  <div class="mr-8">
                                    <img src="la-assets/coursefinder/img/coursesCards/icons/2.svg" alt="icon">
                                  </div>
                                  <div class="text-14 lh-1"><?php echo $result->intake; ?></div>
                                </div>

                                <div class="d-flex items-center">
                                  <div class="mr-8">
                                    <img src="la-assets/coursefinder/img/coursesCards/icons/1.svg" alt="icon">
                                  </div>
                                  <div class="text-14 lh-1"><?php echo $result->course_type; ?></div>
                                </div>

                              </div>

                              <div class="coursesCard-footer">
                                <div class="coursesCard-footer__author">
                                  <div>Annual Tuition Fees</div>
                                </div>

                                <div class="coursesCard-footer__price">
                                  <div></div>
                                  <div>&#8377; <?php echo $result->tuition_fees; ?></div>
                                </div>
                              </div>
                            </div>
                          </a>

                        </div>
                      </div>

                      <?php }} ?>
                      <!-- End Course list -->
                    </div>

                    <button class="section-slider-nav -prev -dark-bg-dark-2 -white -absolute size-70 rounded-full shadow-5 js-prev">
                      <i class="fa fa-arrow-left text-24"></i>
                    </button>

                    <button class="section-slider-nav -next -dark-bg-dark-2 -white -absolute size-70 rounded-full shadow-5 js-next">
                      <i class="fa fa-arrow-right text-24"></i>
                    </button>

                  </div>
                </div>

              </div>
            </div>
          </div>
        </section>

        <section class="layout-pt-lg layout-pb-lg">
          <div data-anim-wrap class="container">
            <div class="row justify-center text-center">
              <div class="col-auto">

                <div class="sectionTitle ">

                  <h2 class="sectionTitle__title ">Start your Learning Journey Today!</h2>

                  <p class="sectionTitle__text ">Lorem ipsum dolor sit amet, consectetur.</p>

                </div>

              </div>
            </div>

            <div class="row y-gap-30 justify-between pt-60 lg:pt-50">

              <div class="col-lg-3 col-md-6">
                <div class="coursesCard -type-2 text-center pt-50 pb-40 px-30 bg-white rounded-8">
                  <div class="coursesCard__image">
                    <img src="la-assets/coursefinder/img/home-5/learning/1.svg" alt="image">
                  </div>
                  <div class="coursesCard__content mt-30">
                    <h5 class="coursesCard__title text-18 lh-1 fw-500">Learn with Experts</h5>
                    <p class="coursesCard__text text-14 mt-10">Grursus mal suada faci lisis that ipsum ameti consecte.</p>
                  </div>
                </div>
              </div>

              <div class="col-lg-3 col-md-6">
                <div class="coursesCard -type-2 text-center pt-50 pb-40 px-30 bg-white rounded-8">
                  <div class="coursesCard__image">
                    <img src="la-assets/coursefinder/img/home-5/learning/2.svg" alt="image">
                  </div>
                  <div class="coursesCard__content mt-30">
                    <h5 class="coursesCard__title text-18 lh-1 fw-500">Learn Anything</h5>
                    <p class="coursesCard__text text-14 mt-10">Grursus mal suada faci lisis that ipsum ameti consecte.</p>
                  </div>
                </div>
              </div>

              <div class="col-lg-3 col-md-6">
                <div class="coursesCard -type-2 text-center pt-50 pb-40 px-30 bg-white rounded-8">
                  <div class="coursesCard__image">
                    <img src="la-assets/coursefinder/img/home-5/learning/3.svg" alt="image">
                  </div>
                  <div class="coursesCard__content mt-30">
                    <h5 class="coursesCard__title text-18 lh-1 fw-500">Flexible Learning</h5>
                    <p class="coursesCard__text text-14 mt-10">Grursus mal suada faci lisis that ipsum ameti consecte.</p>
                  </div>
                </div>
              </div>

              <div class="col-lg-3 col-md-6">
                <div class="coursesCard -type-2 text-center pt-50 pb-40 px-30 bg-white rounded-8">
                  <div class="coursesCard__image">
                    <img src="la-assets/coursefinder/img/home-5/learning/4.svg" alt="image">
                  </div>
                  <div class="coursesCard__content mt-30">
                    <h5 class="coursesCard__title text-18 lh-1 fw-500">Industrial Standart</h5>
                    <p class="coursesCard__text text-14 mt-10">Grursus mal suada faci lisis that ipsum ameti consecte.</p>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </section>

        <footer class="footer -type-1 bg-purple-1">
          <div class="container">
            
            <div class="py-30 border-top-light-15">
              <div class="row justify-between items-center y-gap-20">
                <div class="col-auto">
                  <div class="d-flex items-center h-100 text-white">
                    © 2022 ZODOCS. All Right Reserved.
                  </div>
                </div>

                <div class="col-auto">
                  <div class="d-flex x-gap-20 y-gap-20 items-center flex-wrap">
                    <div>
                      <div class="d-flex x-gap-15 text-white">
                        Powered By<a href="http://demo.zodocs.com/" target="_blank" class="ft_txt1">ZODOCS</a>
                        . Solution By<a href="https://www.labgex.com/" target="_blank" class="ft_txt1">LabGex Innovations (P) Ltd</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>


      </div>
    </main>
  </div>
  <!-- barba container end -->

  <!-- JavaScript -->
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
  <script src="la-assets/coursefinder/js/vendors.js"></script>
  <script src="la-assets/coursefinder/js/main.js"></script>

</body>

</html>