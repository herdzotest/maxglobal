<div class="modal fade" id="AddModalUpload" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">Upload Image</div>
			<div class="modal-body">
				<form action="{{ url(config('laraadmin.adminRoute') . '/upload_files') }}" id="fm_dropzone_main" enctype="multipart/form-data" method="POST">
					{{ csrf_field() }}
					<a id="closeDZ1"><i class="fa fa-times"></i></a>
					<div class="dz-message"><i class="fa fa-cloud-upload"></i><br>Drop files here to upload</div>
				</form>
			</div>
		</div>
	</div>
</div>