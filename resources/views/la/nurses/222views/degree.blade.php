<div id="tab_5" class="tab-pane " >

        
<div class="content-head">
                                        
                                        <h4> Employment History  </h4>

                                        <!--  <div class="id">Sutdent ID : <b>#12675</b></div>-->
                                </div>
                <div class="pane">
                        
		<div id="employement-wrapper">
            <div class="academic">      
                 <div class="mgv-field">
                        <div class="name">
                        Name of employer/organisation
                        </div>
                        <div class="value" id="employer_name"></div>
                 </div>
                 
                 <div class="mgv-field">
                        <div class="name">
                        Nature of work 	
                        </div>
                        <div class="value" id="work_nature"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                                 From Date
                        </div>
                        <div class="value" id="from_date"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                                 To Date
                        </div>
                        <div class="value" id="to_date"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                        Mode of Work 
                        </div>
                        <div class="value" id="work_mode"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">Experience letter</div>
                        <div class="value uploaded-file" id="experience_letter" data-type="file"></div>
						<div class="file-approve-buttons hide">
							<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="misc-exper-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
							<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="misc-exper-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
						</div>	
                 </div>         
			</div> 
		</div>
    </div>  
</div>