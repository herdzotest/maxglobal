Dear {{ $staff }},<br><br>

Application form of student: {{ $user }} has been assigned to you. <br><br>

Please login to zodocs to view the details 
<a href="{{ url('/userlogin') }}">{{ str_replace("http://", "", url('/userlogin')) }}</a><br><br>

Best Regards,<br>
Zodocs

