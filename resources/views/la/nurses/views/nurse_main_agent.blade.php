<div id="tab_4" class="tab-pane " >
                                
                                <div class="content-head">
                                                <h4>Section 4</h4>
                                                <div class="id">Nurse ID : <b>#12675</b></div>
                                        </div>
                        <div class="pane">
                                
                                <div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
													<input name="main_agent" id="main_agent" type="text">												
													<label for="interview_schedule_date">Employer/ Main Agent  </label>														
                                                </div>
                                        </div>
                                        <div class=" col s6 ">
                                                <div class="input-field">
														<input name="work_site" id="work_site" type="text">
														<label for="work_site">Work Site </label>															
                                                </div>
                                        </div>
                                </div>

                                <div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
														<input name="mode_interview" id="mode_interview" type="text">
														<label for="mode_interview">Mode of Interview  </label>
                                                </div>


                                        </div>
										
										<div class="col s6">
												<input name="interview_status" id="interview_status" type="text">
												<label for="interview_status">Interview status  </label>
										</div>	
                                </div>
								
								<div id="offer-letter-wrapper" style="display:none;">
									<div class="row">
										<div class="col s6">
											<div class="input-field">
													<input name="offer_letter_date"  required id="offer_letter_date" type="text">												
													<label for="offer_letter_date">Offer Letter Received Date  </label>
											</div>
										</div>
										<div class=" col s6 ">
												<div class="input-field">
														<input name="offer_letter_signed" id="offer_letter_signed" type="text">												
														<label for="offer_letter_signed">Offer Letter Signed Date   </label>
												</div>
										</div>
									</div>
									
								 <div class="row">
											<div class="col s12">
													<label>Offer Letter File Upload   </label>
													<div class="file-field input-field"></div>
													<div class="uploaded-file" id="offer_letter_file" data-type="file"></div>
													<div class="file-approve-buttons hide">
														<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="offer_letter_file" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
														<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="offer_letter_file" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
													</div>	
											</div>											
									</div>
								</div>								
								
								
                                <div class="row">
                                                <div class="col s6">
													<div class="input-field">
															<input name="interview_schedule_time"  required id="interview_schedule_time" type="text">												
															<label for="interview_schedule_time">Interview Schedule Time  </label>
													</div>
                                                </div>
												<div class=" col s6 ">
														<div class="input-field">
																<input name="interview_schedule_date" id="interview_schedule_date" type="text">												
																<label for="interview_schedule_date">Interview Schedule Date  </label>
														</div>
												</div>
                                        </div>

                        </div>
                </div>

                