<div id="tab_8" class="tab-pane" >
        
<div class="content-head">

<h4> Parent / Guardian's Details </h4>
<!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
</div>              
<div class="pane">
        <div class="academic">
                 
                <div class="mgv-field">
                        <div class="name">
                        Mother's name 
                        </div>
                        <div class="value">
							<span id="mother_prefix"></span>&nbsp;
							<span id="mother_firstname"></span>&nbsp;
							<span id="mother_middlename"></span>&nbsp;
							<span id="mother_lastname"></span>
                        </div>
                 </div>                 
                <div class="mgv-field">
                        <div class="name">
                        Mother's Date of birth
                        </div>
                        <div class="value" id="mother_birth"></div>
                 </div>  
                <div class="mgv-field">
                        <div class="name">
                              Mother's Nationality
                        </div>
                        <div class="value" id="mother_nation"></div>
                 </div>				 
                <div class="mgv-field">
                        <div class="name">
                        Father's name 

                        </div>
                        <div class="value">
							<span id="father_prefix"></span>&nbsp;
							<span id="father_firstname"></span>&nbsp;
							<span id="father_middlename"></span>&nbsp;
							<span id="father_lastname"></span>
                        </div>
                 </div>             
                <div class="mgv-field">
                        <div class="name">
                        Father's Date of birth
                        </div>
                        <div class="value" id="father_birth"></div>
                 </div> 				 
                <div class="mgv-field">
                        <div class="name">
                            Father's Nationality
                        </div>
                        <div class="value" id="father_nation"></div>
                 </div>
		</div>
</div>
        
</div>