<div id="tab_8" class="tab-pane">
        
<div class="content-head">
                                                <h4>Dependant Details</h4>
                                                <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                                </div>

                                
            <div class="pane">
				<div class="academic">
                 <div class="mgv-field">
                        <div class="name">
                        Relationship Status 
                        </div>
                        <div class="value" id="relatiion_status"></div>
                 </div> 
				<div id="spouse-details-wrapper" class="hide">
                 <div class="mgv-field">
                        <div class="name">
                        Name of spouse 
                        </div>
                        <div class="value">
							<span id="spouse_prefix"></span>&nbsp;
							<span id="spouse_firstname"></span>&nbsp;
							<span id="spouse_middlename"></span>&nbsp;
							<span id="spouse_familyname"></span>
						</div>
                 </div>                
                 <div class="mgv-field">
                        <div class="name">
                        Date Of Birth of spouse  
                        </div>
                        <div class="value" id="spouse_birth_date"></div>
                 </div>                 
                 <div class="mgv-field">
                        <div class="name">
                        Do they currently live with you? 
                        </div>
                        <div class="value" id="current_live_status"></div>
                 </div>  
				<div id="spouse-adrress-details-wrapper" class="hide">
                 <div class="mgv-field">
                        <div class="name">
                        Spouse Address
                        </div>
                        <div class="value">
                                <span id="spouse_current_addr"></span><br>
                                <span id="spouse_current_addr_line"></span><br>
                                <span id="spouse_current_city"></span><br>
                                <span id="spouse_current_state"></span><br>
                                <span id="spouse_current_zipcode"></span>
								<span id="spouse_current_country"></span>
                        </div>
                 </div>  
				</div>
                 <div class="mgv-field">
                        <div class="name">
                        Passport number of spouse 
                        </div>
                        <div class="value" id="spouse_passport_no"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                        Will they be travelling with you to the UK?
                        </div>
                        <div class="value" id="spouse_travel_uk"></div>
                 </div> 				 
                 <div class="mgv-field">
                        <div class="name">Passport of spouse</div>
                                <div class="value">
					<div class="uploaded-file" id="spouse_passport" data-type="file"></div>
					<div class="uploaded-file" id="spouse_passport_back" data-type="file"></div>

                                        <div class="file-approve-buttons">                            
                                                <a href="#" class="btn btn-danger mx-approve-doc" data-type="spouse_passport" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
                                                <a href="#" class="btn btn-success mx-approve-doc" data-type="spouse_passport" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
                                        </div>
                                </div> 

                 </div>
				</div>
				
                <div class="mgv-field nb">
                        <div class="name">
                        Do you have children ?
                        </div>
                        <div class="value" id="childrens"></div>
                 </div>
				 
                <div id="childrens-wrapper" class="hide">
		<div class="academic">
                        <div class="mgv-field">
                                <div class="name">
                                Name (First Name+Family Name) 
                                </div>
                                <div class="value" id="name"></div>
                        </div>
                        <div class="mgv-field">
                                <div class="name">
                                Date of birth 
                                </div>
                                <div class="value" id="birthdate"></div>
                        </div>
                        <div class="mgv-field">
                                <div class="name">
                                Relationship to you? 
                                </div>
                                <div class="value" id="relation"></div>
                        </div>
                        <div class="mgv-field">
                                <div class="name">
                                Does this person currently live with you? 
                                </div>
                                <div class="value" id="current_live"></div>
                        </div>
                        <div class="mgv-field">
                                <div class="name">
                                Is this person travelling with you to the UK? 
                                </div>
                                <div class="value" id="travel_to_uk"></div>
                        </div>
                         
                 </div>
				</div>	
				
                <div class="mgv-field">
                        <div class="name">
                        Mother's name 
                        </div>
                        <div class="value">
							<span id="mother_firstname"></span>&nbsp;
							<span id="mother_lastname"></span>
                        </div>
                 </div>                 
                <div class="mgv-field">
                        <div class="name">
                        Mother's Date of birth
                        </div>
                        <div class="value" id="mother_birth"></div>
                 </div>  			 
                <div class="mgv-field">
                        <div class="name">
                        Father's name 

                        </div>
                        <div class="value">
							<span id="father_firstname"></span>&nbsp;
							<span id="father_lastname"></span>
                        </div>
                 </div>             
                <div class="mgv-field">
                        <div class="name">
                        Father's Date of birth
                        </div>
                        <div class="value" id="father_birth"></div>
                 </div> 				 			
				
			</div>
            </div>             
</div>

               