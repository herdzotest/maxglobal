<div id="tab_7" class="tab-pane" >
        
<div class="content-head">
                                        
                                        <h4>English Language Proficiency</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                        </div>
                        {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'school-add-form']) !!}
               
                <div class="pane">
                        <div class="row">
                                <div class=" col s12 ">


                                        <p>
                                                Was English the language of instruction in your previous institution  ?
                                        </p>
                                        <p>
                                                        <label>
                                                                        <input  name="eng_lang_check" type="radio" value="0" checked/>
                                                                        <span>Yes</span>
                                                                      </label>
                                                                      <label>
                                                                                <input name="eng_lang_check" type="radio"  value="1" />
                                                                                <span>No</span>
                                                                              </label>
                                                                      <label>
                                                                                <input name="eng_lang_check" type="radio"  value="2" />
                                                                                <span>Partly</span>
                                                                              </label>																			  
                                                      </p>
                                </div>
                        </div>
						<div class="row">
							<div class="col s4">
									<div class="input-field ">
													<input id="yrs_english" name="yrs_english" type="number">
													<label for="yrs_english">Number of years you have studied in English</label>
											</div>
							</div>
							<div class="col s4">
										<div class="input-field ">
											<select name="qualifications" id="qualifications">
												<option value="">Select</option>
												<option value="IELTS">IELTS</option>
												<option value="PTE">PTE</option>
												<option value="TOEFL">TOEFL</option>
												<option value="OET">OET</option>
												<option value="OTHERS">OTHERS</option>
											</select>	
											<label for="qualifications">I hold the following qualifications</label>
										</div>
							</div>
							<div class="col s4">
											<div class="input-field ">
															<input id="other_qualifications" name="other_qualifications" type="text" class="validate">
															<label for="other_qualifications"></label>

													</div>
							</div>
						</div>
						
						<h4 class="sub_title">Score (if known)</h4>
						<div class="row">
								<div class="col s6">
										<div class="input-field ">
														<input id="listening_score" name="listening_score" type="text" class="validate">
														<label for="listening_score">Listerning Score</label>
												</div>
								</div>

								<div class="col s6">
												<div class="input-field ">
																<input id="reading_score" name="reading_score" type="text" class="validate">
																<label for="reading_score">Reading Score</label>

														</div>
								</div>


						</div>
                        <div class="row">
                                <div class="col s6">
                                        <div class="input-field ">
                                                        <input id="writing_score" name="writing_score" type="text" class="validate">
                                                        <label for="writing_score">Writing Score</label>
                                                </div>
                                </div>

                                <div class="col s6">
                                                <div class="input-field ">
                                                                <input id="speaking_score" name="speaking_score" type="text" class="validate">
                                                                <label for="speaking_score">Speaking Score</label>

                                                        </div>
                                </div>
                        </div>
						<div class="row">
								<div class="col s6">
										<div class="input-field ">
														<input id="test_date" name="test_date" type="text" class="datepicker">
														<label for="test_date">Date of test </label>
											</div>
								</div>

								<div class="col s6">
												<div class="input-field ">
																<input id="test_date_time" name="test_date_time" type="text" class="datepicker">
																<label for="test_date_time">If test is yet to be completed, please state date and time to be taken</label>
														</div>
								</div>
						</div>
                </div>
                <div class="controls">
                                <a class="mgbtn2">Back</a>
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="ielts">
                                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                                                {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
       

</div>