@extends("la.layouts.app")

@section("contentheader_title", "students")
@section("contentheader_description", "students section")
@section("section", "students")
@section("sub_section", "section")
@section("htmlheader_title", "students data")

@section("headerElems")

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('status_success'))
    <div class="alert alert-success">{{ session('status_success') }}</div>
@endif  

<style>
#mx_students_view .modal-dialog {
	width: 600px !important; 
    margin: 60px auto !important;
}
#mx_students_view .file-field input[type=file] {
	width:20%
}
#mx_students_view #AddModal .modal-content {
	Top: 0px
}
.user-header,.user-body { float:none; }
.user-footer a { margin: 0px !important; }
</style>

<div class="box box-success studentbox">

	<!--<div class="box-header"></div>-->
	<div class="box-body">

	<input type="hidden" value="<?php echo $studentId; ?>" id="student_id">
    <input type="hidden" value="<?php echo Auth::user()->type; ?>" id="user_type">


    <?php if(Auth::user()->type == "MAIN_AGENTS"){ ?> <!-- Main agent --> 
    <style>
    #mx_students_view { margin-left: 20px; }
    </style>
    <main class="clearfix">
    <div class="contents tab-content" id="mx_students_view">
        @include('la.students.partials.studentdata')
    </div>
    </main>
    <?php } 
    else { ?>
	
    <main class="clearfix">
                        <div class="form_sidebar ">

                                <ul >
                                        <li class="active">
                                                <a  class="active" href="#tab_1" data-toggle="tab" aria-expanded="true">
                                                        <span> 1 </span>
                                                        <div>Personal Information</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_2" data-toggle="tab" aria-expanded="false">
                                                        <span> 2 </span>
                                                        <div>Courses Preferred</div>
                                                </a>
                                        </li>										
                                        <li>
                                                <a  href="#tab_3" data-toggle="tab" aria-expanded="false">
                                                        <span> 3 </span>
                                                        <div>Academic Details</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_4" data-toggle="tab" aria-expanded="false">
                                                        <span>4</span>
                                                        <div>English Language</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_5" data-toggle="tab" aria-expanded="false">
                                                        <span>5</span>
                                                        <div>Employement History</div>
                                                </a>
                                        </li>										
                                        <li>
                                                <a  href="#tab_6" data-toggle="tab" aria-expanded="false">
                                                        <span>6</span>
                                                        <div>Further details</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_7" data-toggle="tab" aria-expanded="false">
                                                        <span>7</span>
                                                        <div>Dependant details</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_8" data-toggle="tab" aria-expanded="false">
                                                        <span>8</span>
                                                        <div>Parent's Details</div>
                                                </a>
                                        </li>											
                                        <li>
                                                <a  href="#tab_9" data-toggle="tab" aria-expanded="false">
                                                        <span>9</span>
                                                        <div>Immigration history</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_10" data-toggle="tab" aria-expanded="false">
                                                        <span>10</span>
                                                        <div>Personal statement</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_11" data-toggle="tab" aria-expanded="false">
                                                        <span>11</span>
                                                        <div>Courses Applied</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_12" data-toggle="tab" aria-expanded="false">
                                                        <span>12</span>
                                                        <div>Student Lead Details</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_13" data-toggle="tab" aria-expanded="false">
                                                        <span>13</span>
                                                        <div>Follow Up</div>
                                                </a>
                                        </li>
                                </ul>
                        </div>
                        <br>
                        
                        <div class="contents tab-content" id="mx_students_view">
                        @include('la.students.partials.personal')
						@include('la.students.partials.courses_applied')
                        @include('la.students.partials.sslc')
                        @include('la.students.partials.ielts')
						@include('la.students.partials.employement_history')
                        @include('la.students.partials.misc')
						@include('la.students.partials.dependant_details')
						@include('la.students.partials.parent_details')
                        @include('la.students.partials.applctn_details')
						@include('la.students.partials.personal_statement')
                        @include('la.students.partials.courses_applied_new')
                        @include('la.students.partials.student_lead_details')
                        @include('la.students.partials.follow_up_details')
						
						@la_access("Employees", "create")
						<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">     
								<div class="modal-content">
									<div class="modal-header hd-text">
							Upload Image <br><div class="file_error_text"></div>
									</div>
									<div class="modal-body">       <div class="image-upload-wrap">
									
										<form action="{{ url(config('laraadmin.adminRoute') . '/upload_files') }}" id="fm_dropzone_main" enctype="multipart/form-data" method="POST">
							{{ csrf_field() }}
							<a id="closeDZ1"><i class="fa fa-times"></i></a>
							<div class="dz-message drag-text"><i class="fa fa-cloud-upload"></i><br><h3>Drop files here to upload</h3></div>
						</form>
										</div>            </div>
								</div>
							</div>               
						</div>



                        <div class="modal fade" id="AddModalImg" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">     
                                <div class="modal-content">
                                    <div class="modal-header hd-text">
                            Upload Image  <br><div class="file_error_text_img"></div>
                                    </div>

                                    <div class="modal-body">       <div class="image-upload-wrap">
                                    
                                        <form action="{{ url(config('laraadmin.adminRoute') . '/upload_files') }}" id="fm_dropzone_main_img" enctype="multipart/form-data" method="POST">
                            {{ csrf_field() }}
                            <a id="closeDZ1Img"><i class="fa fa-times"></i></a>
                            <div class="dz-message drag-text"><i class="fa fa-cloud-upload"></i><br><h3>Drop files here to upload</h3></div>
                        </form>
                                        </div>            </div>
                                </div>
                            </div>               
                        </div>


						@endla_access
						
                        </div>
                </main>

        <?php } ?>

	</div>
</div>
@endsection

@push('styles')

<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Varela+Round&display=swap" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/stdentsform/css/normalize.css') }}"/>
<link rel="stylesheet" href="{{ asset('la-assets/stdentsform/css/materialize.css') }}">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/stdentsform/css/main.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/stdentsform/js/main.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<style> .uploaded-file { width: 150px; height: 150px;}</style>
<script>
var bsurl = $('body').attr("bsurl");
var fm_dropzone_main = null;    var fm_dropzone_main_img = null;
var cntFiles = null;
var count=null;
var elem;
$(function () {





    /*get student data on edit section */ // Relocate content
    var studentId = $("#student_id").val(); 
    $(".uploaded-file").addClass("hide");
    
    $('body').off('click', '.form_sidebar li a');
    $('body').on('click', '.form_sidebar li a', function() {
        
        type = $(this).attr('href');
        getStdentData(studentId,type);
    }); 
    //if(studentId != '0' && ((localStorage.getItem('activeTab') == null) || (localStorage.getItem('activeTab') == 'tab_1')) ) {
    if(studentId != '0' && ((localStorage.getItem('activeTab') == null) || (localStorage.getItem('activeTab') == 'tab_1') || ($('#mx-simple-stud-form').length > 0)) ) {
        //getStdentData(studentId,'tab_1');

        var userType = $('#user_type').val();       
        if(userType == "MAIN_AGENTS") {         
            getStdentData(studentId,'std_form');
        } else{
            getStdentData(studentId,'tab_1');
        }

        $("#mx_students_view input,#mx_students_view textarea").prop("readonly", false);
    } else {
        //var activeTab = localStorage.getItem('activeTab');
        var activeTab = (studentId == '0') ? 'tab_1' : localStorage.getItem('activeTab');
        $(".studentbox").find('.form_sidebar li a[href="#'+activeTab+'"]').trigger("click");

        if(activeTab == 'tab_11') { 
            $('#mx_students_view').css('overflow','visible');
        }  else {
            $('#mx_students_view').removeAttr('style');
        }

    } // End Relocate content






	$('#AddModal').modal({
		dismissible: true
	});



    $('#AddModalImg').modal({
        dismissible: true
    });

    var userType = $('#user_type').val();

	


    @la_access("Uploads", "create")
	fm_dropzone_main = new Dropzone("#fm_dropzone_main", {
        maxFilesize: 2,
        //acceptedFiles: "image/*,application/pdf",
        
        accept: function(file, done) {
            console.log(file);

            /*if (file.type != "application/pdf") {
                $(".file_error_text").html("<p class='file_err'>Error! Files of this type are not accepted</p>");
                this.removeFile(file);
            } 
            else {
                done();
            }*/

            // file extension check
            var ext = file.name.split('.').pop();
            if(ext=="pdf" || ext=="docx" || ext=="doc" || ext=="PDF" || ext=="jpg" || ext=="jpeg" || ext=="png" || ext=="gif" || ext=="bmp"   ){
                done();
            } else {
              $(".file_error_text").html("<p class='file_err'>Error! Files of this type are not accepted</p>");
              this.removeFile(file);  
            }

        },

        init: function() {
                this.on('addedfile', function(file) {
                if (this.files.length > 1) {
                    alert("Only 1 file allowed")
                this.removeFile(this.files[0]);
                }
                });

            this.on("complete", function(file) {
                this.removeFile(file);  $(".file_error_text").html("");
            });
            this.on("success", function(file) {
                $('#AddModal').modal('open');   $(".file_error_text").html("");
                loadUploadedFiles();
            });
        }
    });




	$("#mx_students_view").on("click", ".btn input", function(e) {
        e.preventDefault();
		elem=$(this).closest('.file-field').find(".file-path-wrapper input");
		$('#AddModal').modal('open');
    });
    $("#closeDZ1").on("click", function() {
        $('#AddModal').modal('close');
    });


// Image Upload
    fm_dropzone_main_img = new Dropzone("#fm_dropzone_main_img", {
        maxFilesize: 2,
        //acceptedFiles: "image/*,application/pdf",

        accept: function(file, done) {
            console.log(file);
            if (file.type != "image/jpeg" && file.type != "image/jpg" && file.type != "image/png" && file.type != "image/gif" && file.type != "image/bmp") {
                $(".file_error_text_img").html("<p class='file_err'>Error! Files of this type are not accepted</p>");
                this.removeFile(file);
            }
            else {
                done();
            }
        },

        init: function() {
                this.on('addedfile', function(file) {
                if (this.files.length > 1) {
                    alert("Only 1 file allowed")
                this.removeFile(this.files[0]);
                }
                });

            this.on("complete", function(file) {    $('#AddModalImg').modal('close');
                this.removeFile(file);  $(".file_error_text_img").html("");
            });
            this.on("success", function(file) {
                $('#AddModalImg').modal('open');    $(".file_error_text_img").html("");
                loadUploadedFiles();
            });
        }
    });

    $("#mx_students_view").on("click", ".btn8 input", function(e) {
        e.preventDefault();
        elem=$(this).closest('.file-field').find(".file-path-wrapper input");
        $('#AddModalImg').modal('open');
    });
    $("#closeDZ1Img").on("click", function() {
        $('#AddModalImg').modal('close');
    });
// End Image Upload


	@endla_access
	
    $("body").on("click", "ul.files_container .fm_file_sel", function() {
        var upload = $(this).attr("upload");
        upload = JSON.parse(upload);
        $("#EditFileModal .modal-title").html("File: "+upload.name);
        $(".file-info-form input[name=file_id]").val(upload.id);
        $(".file-info-form input[name=filename]").val(upload.name);
        $(".file-info-form input[name=url]").val(bsurl+'/files/'+upload.hash+'/'+upload.name);
        $(".file-info-form input[name=caption]").val(upload.caption);
        $("#EditFileModal #downFileBtn").attr("href", bsurl+'/files/'+upload.hash+'/'+upload.name+"?download");
        

        @if(!config('laraadmin.uploads.private_uploads'))
        if(upload.public == "1") {
            $(".file-info-form input[name=public]").attr("checked", !0);
            $(".file-info-form input[name=public]").next().removeClass("On").addClass("Off");
        } else {
            $(".file-info-form input[name=public]").attr("checked", !1);
            $(".file-info-form input[name=public]").next().removeClass("Off").addClass("On");
        }
        @endif

        $("#EditFileModal .fileObject").empty();
        if($.inArray(upload.extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
            $("#EditFileModal .fileObject").append('<img src="'+bsurl+'/files/'+upload.hash+'/'+upload.name+'">');
            $("#EditFileModal .fileObject").css("padding", "15px 0px");
        } else {
            switch (upload.extension) {
                case "pdf":
                    // TODO: Object PDF
                    $("#EditFileModal .fileObject").append('<object width="100%" height="325" data="'+bsurl+'/files/'+upload.hash+'/'+upload.name+'"></object>');
                    $("#EditFileModal .fileObject").css("padding", "0px");
                    break;
                default:
                    $("#EditFileModal .fileObject").append('<i class="fa fa-file-text-o"></i>');
                    $("#EditFileModal .fileObject").css("padding", "15px 0px");
                    break;
            }
        }
        $("#EditFileModal").modal('show');
    });
    @if(!config('laraadmin.uploads.private_uploads') && Module::hasFieldAccess("Uploads", "public", "write"))
    $('#EditFileModal .Switch.Ajax').click(function() {
        $.ajax({
            url: "{{ url(config('laraadmin.adminRoute') . '/uploads_update_public') }}",
            method: 'POST',
            data: $("form.file-info-form").serialize(),
            success: function( data ) {
               loadUploadedFiles();
            }
        });
    });
    @endif
    loadUploadedFiles(); 




	
	/* get student data on edit section */
	/*var studentId = $("#student_id").val(); 
	$(".uploaded-file").addClass("hide");
	
	$('body').off('click', '.form_sidebar li a');
	$('body').on('click', '.form_sidebar li a', function() {
		
		type = $(this).attr('href');
		getStdentData(studentId,type);
	});	
	//if(studentId != '0' && ((localStorage.getItem('activeTab') == null) || (localStorage.getItem('activeTab') == 'tab_1')) ) {
    if(studentId != '0' && ((localStorage.getItem('activeTab') == null) || (localStorage.getItem('activeTab') == 'tab_1') || ($('#mx-simple-stud-form').length > 0)) ) {
		//getStdentData(studentId,'tab_1');

        var userType = $('#user_type').val();       
        if(userType == "MAIN_AGENTS") {         
            getStdentData(studentId,'std_form');
        } else{
            getStdentData(studentId,'tab_1');
        }

		$("#mx_students_view input,#mx_students_view textarea").prop("readonly", false);
	} else {
		//var activeTab = localStorage.getItem('activeTab');
        var activeTab = (studentId == '0') ? 'tab_1' : localStorage.getItem('activeTab');
		$(".studentbox").find('.form_sidebar li a[href="#'+activeTab+'"]').trigger("click");

        if(activeTab == 'tab_11') { 
            $('#mx_students_view').css('overflow','visible');
        }  else {
            $('#mx_students_view').removeAttr('style');
        }

	}*/




	// prevent navigation on student create
	$('body').on('click', '.form_sidebar li', function(event) {
		if(studentId == '0') {
			event.stopPropagation();
		} else {
			/*setTimeout(function(){ 
				var obj = $(".form_sidebar li.active a");
				var activeTab = obj.attr('href').replace("#",'');
				localStorage.setItem("activeTab",activeTab);				
			}, 800);*/
            var activeTab = $(this).find('a').attr('href').replace("#",'');
            localStorage.setItem("activeTab",activeTab);
            if(activeTab == 'tab_11') {
                $('#mx_students_view').css('overflow','visible');
            }  else {
                $('#mx_students_view').removeAttr('style');
            }
		}
	});
	
	// add more tab functionality
	$('body').on('click', '.addshbtn', function(event) {

		var formTypeStudied = $(this).attr('data-type');
		if(formTypeStudied == 'formtype_studied') {
			var wrapperCount = $("#mx_students_view .tab-pane.active .wrapper-count-studied").val();
			var formType = $("#mx_students_view .tab-pane.active .formtype_studied").val();			
		} else {
			var wrapperCount = $("#mx_students_view .tab-pane.active .wrapper-count").val();
			var formType = $("#mx_students_view .tab-pane.active .formtype").val();			
		}
		var indexVal = parseInt(wrapperCount)-1;

        var yr = new Date().getFullYear();
        var yr1 = new Date().getFullYear() + 1;
        var yr2 = new Date().getFullYear() + 2;

		switch(formType) {
          case 'courses':
            $("#course-wrapper").append('<div class="stdhst wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button>      <div class="col-md-6"> <div class="mg-input-field "> <label for="course_country">Preferred Country</label> <select class="searchSelect" name="courses[' + indexVal + '][course_country]" id=""> <option value="">Select</option> <?php foreach($countries as $countriesData) { echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>'; } ?> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="level_study">Preferred Study Level</label> <select class="searchSelect" name="courses[' + indexVal + '][level_study]" id=""> <option value="">Select</option> <option value="Diploma"> Diploma </option> <option value="Bachelors"> Bachelors </option> <option value="Masters"> Masters </option> <option value="Doctoral"> Doctoral </option> </select> </div> </div><div class="col-md-6"> <div class="mg-input-field "> <label for="course_university">Preferred University</label> <select name="courses[' + indexVal + '][course_university]" class="searchSelect" id=""> <option value="">Select</option> <?php foreach($university as $universityData) { echo '<option value="'.$universityData->university.'">'.$universityData->university.'</option>'; } ?> </select> </div></div> <div class="col-md-6"> <div class="mg-input-field "> <label for="course_title">Preferred Course</label> <select class="searchSelect" id="" name="courses[' + indexVal + '][course_title]"> <option value="">Select</option> <?php foreach($courses as $courseData) { echo '<option value="'.$courseData->course_name.'">'.$courseData->course_name.'</option>'; } ?> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="mode_study">Mode of study</label> <select class="searchSelect" name="courses[' + indexVal + '][mode_study]" id=""> <option value="">Select</option> <option value="Full time"> Full time </option> <option value="Part time"> Part time </option> <option value="Distance Learning"> Distance Learning </option> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="campus_stage">Preferred Campus</label> <select class="searchSelect" id="" name="courses[' + indexVal + '][campus_stage]"> <option value="">Select</option> <?php foreach($campus as $campusData) { echo '<option value="'.$campusData->campus_name.'">'.$campusData->campus_name.'</option>'; } ?> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="stage_name">Preferred Stage</label> <select class="searchSelect" name="courses[' + indexVal + '][stage_name]" id=""> <option value="">Select</option> <option value="Year-1"> Year 1 </option> <option value="Year-2"> Year 2 </option> <option value="Year-3"> Year 3 </option> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="entry_date">Preferred Intake</label> <select class="searchSelect" name="courses[' + indexVal + '][entry_date]" id=""> <option value="">Select</option><?php $curr_yr = date('Y'); $curr_yr2 = date('Y', strtotime('-2 year')); $nxt_yr2 = date('Y', strtotime('+2 year')); $curr_mon = date('m'); $months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec'); for($i=$curr_yr2; $i<=$nxt_yr2;$i++) { foreach ($months as $key => $value) { /*if($i==$curr_yr && $key<$curr_mon) { } else { */ echo '<option value="'.$value.' '.$i.'">'.$value.' '.$i.'</option>'; /*}*/ } } ?></select> </div> </div><div class="clearfix"></div></div>');
            break;
          case 'courses_new':
          $("#course-wrapper-new").append('<div class="stdhst wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button>      <div class="col-md-6"> <div class="mg-input-field "> <label for="course_country_new">Country</label> <select class="searchSelect" name="courses_new[' + indexVal + '][course_country_new]" id=""> <option value="">Select</option> <?php foreach($countries as $countriesData) { echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>'; } ?> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="level_study_new">Level of Study</label> <select class="formSelect"    name="courses_new[' + indexVal + '][level_study_new]" id=""> <option value="">Select</option> <option value="Diploma"> Diploma </option> <option value="Bachelors"> Bachelors </option> <option value="Masters"> Masters </option> <option value="Doctoral"> Doctoral </option> </select> </div> </div>  <div class="col-md-6"> <div class="mg-input-field "> <label for="course_title_new">Course Name</label> <select id="" name="courses_new[' + indexVal + '][course_title_new]" class="searchSelect"> <option value="">Select</option> <?php foreach($courses as $courseData) { echo '<option value="'.$courseData->course_name.'">'.$courseData->course_name.'</option>'; } ?> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="mode_study_new">Mode of Study</label> <select name="courses_new[' + indexVal + '][mode_study_new]" id="" class="formSelect"> <option value="">Select</option> <option value="Full-time"> Full time </option> <option value="Distance"> Distance </option> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="campus_stage_new">Campus</label> <select id=""  class="searchSelect" name="courses_new[' + indexVal + '][campus_stage_new]"> <option value="">Select</option> <?php foreach($campus as $campusData) { echo '<option value="'.$campusData->campus_name.'">'.$campusData->campus_name.'</option>'; } ?> </select> </div> </div>  <div class="col-md-6"> <div class="mg-input-field "> <label for="stage_name_new">Stage (in months)</label><input class="form-control" id="" name="courses_new[' + indexVal + '][stage_name_new]" type="number" min="1"></div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="entry_date_new">Intake</label> <select name="courses_new[' + indexVal + '][entry_date_new]" id="" class="searchSelect"> <option value="">Select</option><?php $curr_yr = date('Y'); $curr_yr2 = date('Y', strtotime('-2 year')); $nxt_yr2 = date('Y', strtotime('+2 year')); $curr_mon = date('m'); $months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec'); for($i=$curr_yr2; $i<=$nxt_yr2;$i++) { foreach ($months as $key => $value) { /*if($i==$curr_yr && $key<$curr_mon) { } else { */ echo '<option value="'.$value.' '.$i.'">'.$value.' '.$i.'</option>'; /*} */ } } ?></select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="course_university_new">University Name</label> <select class="searchSelect"  name="courses_new[' + indexVal + '][course_university_new]" id=""> <option value="">Select</option> <?php foreach($university as $universityData) { echo '<option value="'.$universityData->university.'">'.$universityData->university.'</option>'; } ?> </select> </div></div>  <div class="col-md-6"> <div class="mg-input-field "> <label for="status_new">Status</label> <select class="searchSelect" name="courses_new[' + indexVal + '][status_new]" id=""> <option value="">Select</option> <option value="Received Application at Scholab">Received Application at Scholab</option> <option value="Application in Progress">Application in Progress</option> <option value="Application on hold by Scholab team">Application on hold by Scholab team</option> <option value="Application submitted to the University">Application submitted to the University</option> <option value="Conditional Offer Received">Conditional Offer Received</option> <option value="Unconditional Offer Received">Unconditional Offer Received</option> <option value="Rejected by University">Rejected by University</option> <option value="Application on hold by University">Application on hold by University</option> <option value="Case Closed - Student Not Qualified">Case Closed - Student Not Qualified</option> <option value="Case Closed - Program Closed">Case Closed - Program Closed</option> <option value="Case Closed - Offer Received - Student not interested to pay">Case Closed - Offer Received - Student not interested to pay</option> <option value="Tuition Fee Received">Tuition Fee Received</option> <option value="CAS Requested">CAS Requested</option> <option value="CAS Received">CAS Received</option> <option value="Visa In Process">Visa In Process</option> <option value="Visa Received">Visa Received</option> <option value="Visa Rejected">Visa Rejected</option> <option value="Student Deferred">Student Deferred</option> <option value="Refund Request Initiated">Refund Request Initiated</option> </select></div> </div> <div class="col-md-8"> <div class="mg-input-field "> <label for="status_note">Representative Note</label> <textarea class="form-control" id="" name="courses_new[' + indexVal + '][status_note][0]"></textarea>  </div> </div>    <div class="clearfix"></div></div>');
            break;
		  case 'employement':
			$("#employement-wrapper").append('<div class="stdhst wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button> <div class="col-md-6"> <div class="mg-input-field "> <label for="employer_name">Name of employer/organisation</label> <input class="form-control" id="" name="employement[' + indexVal + '][employer_name]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="job_position">Job Position</label> <input class="form-control" id="" name="employement[' + indexVal + '][job_position]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="from_date">From</label> <select name="employement[' + indexVal + '][from_date]" class="searchSelect" id=""> <option value="">Select</option> <?php $curr_yr = date('Y'); $curr_mon = date('m'); $months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec'); for($i=1990; $i<=$curr_yr;$i++) { foreach ($months as $key => $value) { if($i==$curr_yr && $key>$curr_mon) { } else { echo '<option value="'.$value.' '.$i.'">'.$value.' '.$i.'</option>'; } } } ?> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="to_date">To</label> <select name="employement[' + indexVal + '][to_date]" class="searchSelect" id=""> <option value="">Select</option> <?php for($i=1990; $i<=$curr_yr;$i++) { foreach ($months as $key => $value) { if($i==$curr_yr && $key>$curr_mon) { } else { echo '<option value="'.$value.' '.$i.'">'.$value.' '.$i.'</option>'; } } } ?> </select> </div> </div>        <div class="col-md-6"> <div class="mg-input-field "> <label for="experience">Year of experience</label> <input class="form-control" id="" name="employement[' + indexVal + '][experience]" type="number" step=".01" min=1> </div> </div>     <div class="col-md-6"> <div class="mg-input-field "> <label for="work_mode">Mode of work</label> <select id="" name="employement[' + indexVal + '][work_mode]" class="formSelect"> <option value="">Select</option> <option value="Full-time"> Full time </option> <option value="Part-time"> Part-time </option> </select> </div> </div> <div class="col-md-6"> <label>Experience letter</label> <div class="file-field input-field"> <div class="btn"> <span>Upload</span> <input type="file" multiple > <i class="fa fa-cloud-upload"></i> </div> <div class="file-path-wrapper"> <input class="file-path validate" name="employement[' + indexVal + '][experience_letter]" type="text" placeholder = "Upload file" > </div> </div> <div class="uploaded-file hide" id="employement[' + indexVal + '][experience_letter]" data-type="file"></div> </div> <div class="clearfix"></div> </div>');
			//$("#mx_students_view .tab-pane.active .uploaded-file").addClass("hide");
			break;
		  case 'children':
			$("#childrens-wrapper").append('<div class="stdhst wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button>   <div class="col-md-6"> <div class="mg-input-field "> <label for="name">Name (First Name + Family Name)</label> <input class="form-control" id="" name="children[' + indexVal + '][name]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="birthdate">Date of birth</label> <input class="form-control datepicker" id="" name="children[' + indexVal + '][birthdate]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="from_date">Relationship to you?</label> <select name="children[' + indexVal + '][relation]" id="" class="formSelect"> <option value="Son">Son</option> <option value="Daughter">Daughter</option> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="current_live">Does this person currently live with you?</label> <select name="children[' + indexVal + '][current_live]" id="" class="formSelect"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div><div class="col-md-6"> <div class="mg-input-field "> <label for="travel_to_uk">Is this person travelling with you to the UK?</label> <select name="children[' + indexVal + '][travel_to_uk]" id="" class="formSelect"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="clearfix"></div> </div>');
			break;
		  case 'imigration':
            $("#imigration-wrapper").append('<div class="stdhst wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button>  <div class="col-md-6"> <div class="mg-input-field "> <label for="other_country_name">Country</label> <select class="searchSelect" name="imigration[' + indexVal + '][other_country_name]" id=""> <option value="">Select</option> <?php foreach($countries as $countriesData) { echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>'; } ?> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="reason">Reason for your visit</label> <select id="" name="imigration[' + indexVal + '][reason]" class="formSelect"> <option value="">Select</option> <option value="Work"> Work </option> <option value="Tourism"> Tourism </option> <option value="Others"> Others </option> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="visite_date">Date you entered</label> <input class="form-control datepicker" id="" name="imigration[' + indexVal + '][visite_date]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="leave_date">Date you left</label> <input class="form-control datepicker" id="" name="imigration[' + indexVal + '][leave_date]" type="text"> </div> </div> <div class="clearfix"></div>  </div>');
			break;
		  case 'imigration_studied':
			$("#prev-studied-wrapper").append('<div class="stdhst wrapper-' + wrapperCount + '"> <div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button> <div class="col-md-6"> <div class="mg-input-field "> <label for="studied_country">Country</label>  <select class="searchSelect" name="imigration_studied[' + indexVal + '][studied_country]" id=""> <option value="">Select</option> <?php foreach($countries as $countriesData) { echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>'; } ?> </select>  </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="reason">Visa start date</label> <input class="form-control datepicker" id="" name="imigration_studied[' + indexVal + '][studied_visa_start_date]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="visite_date">Visa end date</label> <input class="form-control datepicker" id="" name="imigration_studied[' + indexVal + '][studied_visa_end_date]" type="text"> </div> </div>  <div class="col-md-6"> <div class="mg-input-field "> <label for="studied_status">Status</label> <select id="" name="imigration_studied[' + indexVal + '][studied_status]" class="formSelect"> <option value="">Select</option> <option value="Completed">Completed</option> <option value="Not-completed">Not completed</option> </select> </div> </div>  <div class="col-md-6"> <div class="mg-input-field "> <label for="studied_university">University</label> <select class="searchSelect"  name="imigration_studied[' + indexVal + '][studied_university]" id=""> <option value="">Select</option> <?php foreach($university as $universityData) { echo '<option value="'.$universityData->university.'">'.$universityData->university.'</option>'; } ?> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="studied_course_title">Course title</label> <select id="" name="imigration_studied[' + indexVal + '][studied_course_title]" class="searchSelect"> <option value="">Select</option> <?php foreach($courses as $courseData) { echo '<option value="'.$courseData->course_name.'">'.$courseData->course_name.'</option>'; } ?> </select> </div> </div>  <div class="col-md-6"> <div class="mg-input-field "> <label for="studied_cas_start_date" style="margin-top: 15px;">CAS start date</label> <input class="form-control datepicker" id="" name="imigration_studied[' + indexVal + '][studied_cas_start_date]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="studied_cas_start_date" style="margin-top: 15px;">CAS end date</label> <input class="form-control datepicker" id="" name="imigration_studied[' + indexVal + '][studied_cas_end_date]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="studied_cas_start_date">CAS/Visa copy/transcripts</label> <input class="form-control" id="" name="imigration_studied[' + indexVal + '][cas_transcripts]" type="text"> </div> </div> <div class="clearfix"></div> </div>');
			break;
          case 'school':
            $("#school-wrapper").append('<div class="mgv-wrap wrapper-' + wrapperCount + '"><h4 class="sub_title">Additional Qualification</h4><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button><div class="row"><div class=" col s6 "> <div class="input-field "> <label for="UG_board_name">Course Name</label> <select name="school[' + indexVal + '][UG_board_name]" id="" class="searchSelect"> <option value="">Select</option> <?php foreach($courses as $courseData) { echo '<option value="'.$courseData->course_name.'">'.$courseData->course_name.'</option>'; } ?> </select> </div> </div><div class=" col s6 "> <div class="input-field "> <label for="ug_university">University Name</label> <select name="school[' + indexVal + '][ug_university]" id="" class="searchSelect"> <option value="">Select</option> <?php foreach($university as $universityData) { echo '<option value="'.$universityData->university.'">'.$universityData->university.'</option>'; } ?> </select> </div> </div> </div><div class="row"><div class=" col s6 "> <div class="input-field "> <label for="sslcgrade" class="active">University Percentage(%)</label> <input class="form-control" id="" name="school[' + indexVal + '][UG_grade]" type="number" step=".01" min=1>  </div> </div><div class=" col s6 "> <div class="input-field "> <label for="UG_end_year">Pass Out Year</label> <select name="school[' + indexVal + '][UG_end_year]" id="" class="searchSelect"> <option value="">Select</option> <?php foreach(range(date('Y'), 1990) as $x) {  echo '<option value="'.$x.'"'.($x === date('Y') ? ' selected="selected"' : '').'>'.$x.'</option>'; } ?> </select>  </div> </div> </div><div class="row"><div class=" col s6 "> <div class="input-field "> <label for="UG_backlogs" class="active">Backlogs</label> <input class="form-control" id="" name="school[' + indexVal + '][UG_backlogs]" type="number">  </div> </div><div class=" col s6 "> <div class="input-field "> <label for="UG_mode_study" style="margin-top:-40px;">Mode of study</label> <select id="" name="school[' + indexVal + '][UG_mode_study]" class="formSelect"> <option value="">Select</option> <option value="Full-time"> Full time </option> <option value="Distance"> Distance </option> </select> </div> </div>       <div class="row"><div class="col s6"><label>Upload Documents</label><div class="file-field input-field"><div class="btn"><span>Upload</span><input type="file" multiple > <i class="fa fa-cloud-upload"></i></div><div class="file-path-wrapper"><input class="file-path validate" name="school[' + indexVal + '][UG_document]" type="text" placeholder = "Upload file" ></div></div><div class="uploaded-file hide" id="school[' + indexVal + '][UG_document]" data-type="file"></div>  </div></div>      </div><div class="clearfix"></div></div>');
            break;
          case 'follow_up':
            $("#follow_up-wrapper").append('<div class="stdhst wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button><div class="col-md-6"> <div class="mg-input-field "> <label for="follow_date">Date for next follow up</label> <input class="futuredatepicker" id="" name="follow_up[' + indexVal + '][follow_date]" type="text">  </div> </div><div class="col-md-6"> <div class="mg-input-field "> <label for="test_time">Time for next follow up</label> <input class="timepicker" id="" name="follow_up[' + indexVal + '][follow_time]" type="text"> </div> </div><div class="col-md-12"> <div class="mg-input-field "> <label for="prefix">Representative Note</label> <textarea class="materialize-textarea" id="" name="follow_up[' + indexVal + '][representative_note]" maxlength="500"> </textarea> </div> </div><div class="clearfix"></div> </div>');
            break;		
		  default:
			break;
		}
		/*$('select').formSelect();
		$('.datepicker').datepicker({selectMonths: true,selectYears: 30});*/	

        $('.formSelect').formSelect();
        $('.searchSelect').select2();
        $('.datepicker').datepicker({selectMonths: true,selectYears: 30});
        $('.futuredatepicker').datepicker({yearRange: [2021,2025],minDate: new Date(),selectMonths: true});
        $('.timepicker').timepicker();

		if(formTypeStudied == 'formtype_studied') {
			$("#mx_students_view .tab-pane.active .wrapper-count-studied").val(parseInt(wrapperCount)+1);
		} else {
			$("#mx_students_view .tab-pane.active .wrapper-count").val(parseInt(wrapperCount)+1);
            $("#school-wrapper .select2-container--default").parent().find('label').css('margin-top','-40px');
		}
	});	
	// remove tab functionality
	$('body').on('click', '.xclose', function(event) {
		$(this).parent().remove();
		var wrapperCount = $("#mx_students_view .tab-pane.active .wrapper-count").val();
		$("#mx_students_view .tab-pane.active .wrapper-count").val(parseInt(wrapperCount)-1);
	});	
	// back button functionality
	$('#mx_students_view').on('click', '.back_button', function() {
		
		var id = $("#mx_students_view .tab-pane.active").attr('id');
		var backtab = '#tab_' + (parseInt(id.replace('tab_','')) - 1);
		$("a[href$=" + backtab + "]").trigger('click');
	});		
	$('#mx_students_view').on('change', '.living_addr', function() {
		
	});	

    // checklist
    $('#mx_students_view').on('change', '.checklist', function() {
        
        var checked  = ($(this).is(":checked")) ? 1 : 0;
        var idName = $(this).attr('id');
        $("."+idName).val(checked);
    });
    // end checklist

});

// get all the student details based on tabs
function getStdentData(studentId,type) {
	type = type.replace('#','');
	var url = '{{ url(config('laraadmin.adminRoute')) }}/students_data/' + studentId + '/' + type;

	$.ajax({
		url: url,
		data: {},
		success: function(response){
			var details = JSON.parse(response);

            if(details.length == 0 && type == 'tab_4') {
                $('.eng-lang-wrapper').removeClass('hide');
            }

			$.each(details, function(key, value) {
				element = $("#"+key); elementName = $("input[name='" + key + "']");

				if(element.length > 0) {
					if(element.attr('data-type') == 'file') {
						if(value != null && value != '') {
							
							element.removeClass("hide"); // remove hide class of frame to show image 
							$(".appendedImages").remove();
							$("input[name='" + key + "']").val(value);
							var images = value.split(",");
							var extension = images[0].split("?")[0].substr( (images[0].split("?")[0].lastIndexOf('.') +1) );
							if($.inArray(extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
								element.css("background-image", "url(" + images[0] + ")");
								element.wrap('<a target="_blank" href="' + images[0].split("?")[0] + '"></a>');
							} else {
								switch (extension) {
									case "pdf":
										element.html('<i class="fa fa-file-pdf-o"></i>');
										break;
									default:
										element.html('<i class="fa fa-file-text-o"></i>');
										break;
								}
								element.wrap('<a target="_blank" href="' + images[0] + '"></a>');
							}
							if(images.length > 1) { //alert(images.length);
								for(i=1;i<=(images.length)-1;i++) {
                                //for(i=1;i<(images.length);i++) {
									
									var extension = images[i].split("?")[0].substr( (images[i].split("?")[0].lastIndexOf('.') +1) );
									if($.inArray(extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
										element.parent().parent().append('<a target="_blank" class="appendedImages" href="' + images[i].split("?")[0] + '"><div style="background-image: url(' + images[i] + ')" class="uploaded-file" data-type="file"></div></a>');
									} else {
										switch (extension) {
											case "pdf":
												var appendedHtml = '<i class="fa fa-file-pdf-o"></i>';
												break;
											default:
												var appendedHtml = '<i class="fa fa-file-text-o"></i>';
												break;
										}
										element.parent().parent().append('<a target="_blank" class="appendedImages" href="' + images[i] + '"><div class="uploaded-file" data-type="file">' + appendedHtml +'</div></a>');
									}									
								}
							}
						}
					} else {
						/*if(element.prop('type') == 'checkbox' && value == '1') {
							element.prop('checked', true);
						}*/
                        if(key == 'age') {
                            element.prop('readOnly', true);
                        }
                        // checklist
                        if(element.prop('type') == 'checkbox') {
                            if(value == '1') {
                                element.prop('checked', true);  
                            } else { element.prop('checked', false); }
                                $("."+key).val(value);
                        }
                        // end checklist

                        else {

                            if(key == 'status_note') {
                            var statusNote = JSON.parse(value);
                            $(".course-repr-note .mg-input-field .status-note-wrapper").remove();
                            $(".course-repr-note .mg-input-field").append('<div class="status-note-wrapper"></div>')
                            var statusNoteLength = statusNote.length;
                            $.each(statusNote, function(key, value) {
                                if(key == 0) {
                                    element.val(value);
                                } else {
                                    $(".course-repr-note .mg-input-field  .status-note-wrapper").append("<textarea name='status_note["+key+"]'>"+value+"</textarea>");
                                }
                            });
                            if(statusNoteLength > 0) {
                                $(".course-repr-note .mg-input-field .status-note-wrapper").append("<textarea name='status_note["+statusNoteLength+"]'></textarea>");
                            }
                        } else {

    							element.val(value);
    							element.next().addClass('active');
    							if(element.prop('type') == 'select-one') {
    								//element.formSelect();
                                    if(element.hasClass('searchSelect'))
                                        element.select2();
                                    else
                                        element.formSelect();
    							}

                            }
						}
						switch(key) {
							case 'visa_aplctn_stat':
								if(value == 'SUBMITTED') {
									$('#visa-application-details').removeClass('hide');
								}
								break;
							case 'offer_stat':
								if(value == 'RECEIVED') {
									$('#ofl-upload').show();
								}
								break;
							case 'cas_stat':
								if(value == 'RECEIVED') {
									$('#cas-rcd').show();
								}
							case 'enrollment_stat':
								if(value == 'ENROLLED') {
									$('#enrolled').show();
								}
								break;
							case 'qualifications':
								if(value == 'OTHERS') {
									$('#other-qualifications-wrapper').removeClass('hide');
								}
								break;
							case 'relatiion_status':
								if(value == 'Married' || value == 'Civil-partnership') {
									$('#spouse-details-wrapper').removeClass('hide');
								}
								break;
							case 'current_live_status':
								if(value == 'No') {
									$('#spouse-adrress-details-wrapper').removeClass('hide');
								}
								break;
							case 'childrens':
								if(value == 'Yes') {
									$('#childrens-wrapper').removeClass('hide');
								}
								break;
                            case 'lead_from':
                                if(value == 'Agents') {
                                    $('#lead_agents').removeClass('hide'); 
                                }
                                break;						
							default:
								break;							
						}
					}
				} else if (elementName.attr('type') == 'radio') {
					$("input[name='" + key + "'][value='" + value + "']").prop('checked', true);
						switch(key) {
                            // English Lang Check
                            case 'eng_lang_check':
                            if(value == '1') {
                                $('#ielts-wrapper').removeClass('hide');
                                $('#ielts-test-wrapper').addClass('hide');
                            } else {
                                $('#ielts-wrapper').addClass('hide');
                                $('#ielts-test-wrapper').removeClass('hide');
                            }
                                break;
                            // End English Lang Check  
							case 'lived_other_address':
								if(value == '0') {
									$('#lived_other_address_wrapper').removeClass('hide');
								} else {
									$('#lived_other_address_wrapper').addClass('hide');
								}
								break;	
                            case 'studied_uk':
                            if(value == '0') {
                                $('#prev-studied-country-wrapper').removeClass('hide');
                            } else {
                                $('#prev-studied-country-wrapper').addClass('hide');
                            }               
                            break;
							case 'other_countries':
								if(value == '0') {
									$('#other-countrier-details-wrapper').removeClass('hide');
								} else {
									$('#other-countrier-details-wrapper').addClass('hide');
								}
							/*case 'studied_uk':
								if(value == '0') {
									$('#prev-studied-country-wrapper').removeClass('hide');
								} else {
									$('#prev-studied-country-wrapper').addClass('hide');
								}						
								break;*/
                            case 'pte':
                                if(value == '1') {
                                    $('#pte-wrapper').removeClass('hide');
                                } else {
                                    $('#pte-wrapper').addClass('hide');
                                }               
                                break;
                            case 'duolingo':
                                if(value == '1') {
                                    $('#duolingo-wrapper').removeClass('hide');
                                } else {
                                    $('#duolingo-wrapper').addClass('hide');
                                }               
                                break;
                            case 'toefl':
                                if(value == '1') {
                                    $('#TOEFL-wrapper').removeClass('hide');
                                } else {
                                    $('#TOEFL-wrapper').addClass('hide');
                                }               
                                break;  							
							default:
								break;						
						}
				} //else if(key == 'json_data' && value != null) {
                    else if(key == 'json_data' && value != null && value != '') {

                    var selectelement = selectelementSearch = datepickerelement = datepickerelement1 = timepickerelement = '';

					var jsonData = JSON.parse(value);
					$(".common-wrapper").remove();
					
                    var yr = new Date().getFullYear();
                    var yr1 = new Date().getFullYear() + 1;
                    var yr2 = new Date().getFullYear() + 2;

                    var wc=0; // Wrapper count

					$.each(jsonData, function(key, value) {
						//wrapperCount = key+1; indexVal = key;

						// Wrapper count
                        wrapperCount = parseInt(wc)+1;
                        indexVal = parseInt(wc);
                        wc = parseInt(wc)+1;
                        // End wrapper count

						switch(type) {
                            case 'tab_2':
                                $("#course-wrapper").append('<div class="stdhst common-wrapper wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button>    <div class="col-md-6"> <div class="mg-input-field "> <label for="course_country">Preferred Country</label> <select name="courses[' + indexVal + '][course_country]" class="searchSelect" id=""> <option value="">Select</option> <?php foreach($countries as $countriesData) { echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>'; } ?> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="level_study">Preferred Study Level</label> <select  class="searchSelect" name="courses[' + indexVal + '][level_study]" id=""> <option value="">Select</option> <option value="Diploma"> Diploma </option> <option value="Bachelors"> Bachelors </option> <option value="Masters"> Masters </option> <option value="Doctoral"> Doctoral </option> </select> </div> </div><div class="col-md-6"> <div class="mg-input-field "> <label for="course_university">Preferred University</label> <select class="searchSelect" name="courses[' + indexVal + '][course_university]" id=""> <option value="">Select</option> <?php foreach($university as $universityData) { echo '<option value="'.$universityData->university.'">'.$universityData->university.'</option>'; } ?> </select> </div></div> <div class="col-md-6"> <div class="mg-input-field "> <label for="course_title">Preferred Course</label> <select id="" name="courses[' + indexVal + '][course_title]" class="searchSelect"> <option value="">Select</option> <?php foreach($courses as $courseData) { echo '<option value="'.$courseData->course_name.'">'.$courseData->course_name.'</option>'; } ?> </select> </div> </div>    <div class="col-md-6"> <div class="mg-input-field "> <label for="mode_study">Mode of study</label> <select  class="searchSelect" name="courses[' + indexVal + '][mode_study]" id=""> <option value="">Select</option> <option value="Full time"> Full time </option> <option value="Part time"> Part time </option> <option value="Distance Learning"> Distance Learning </option> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="campus_stage">Preferred Campus</label> <select name="courses[' + indexVal + '][campus_stage]" class="searchSelect" id=""> <option value="">Select</option> <?php foreach($campus as $campusData) { echo '<option value="'.$campusData->campus_name.'">'.$campusData->campus_name.'</option>'; } ?> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="stage_name">Preferred Stage</label> <select  class="searchSelect" name="courses[' + indexVal + '][stage_name]" id=""> <option value="">Select</option> <option value="Year-1"> Year 1 </option> <option value="Year-2"> Year 2 </option> <option value="Year-3"> Year 3 </option> </select> </div> </div>    <div class="col-md-6"> <div class="mg-input-field "> <label for="entry_date">Preferred Intake</label> <select name="courses[' + indexVal + '][entry_date]" id="" class="searchSelect"> <option value="">Select</option><?php $curr_yr = date('Y'); $curr_yr2 = date('Y', strtotime('-2 year')); $nxt_yr2 = date('Y', strtotime('+2 year')); $curr_mon = date('m'); $months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec'); for($i=$curr_yr2; $i<=$nxt_yr2;$i++) { foreach ($months as $key => $value) { /*if($i==$curr_yr && $key<$curr_mon) { } else { */ echo '<option value="'.$value.' '.$i.'">'.$value.' '.$i.'</option>'; /*} */ } } ?></select> </div> </div><div class="clearfix"></div></div>');
                                $( "select[name*='courses[" + indexVal + "][course_country]']").val(value.course_country);
                                $( "select[name*='courses[" + indexVal + "][level_study]']").val(value.level_study);
                                $( "select[name*='courses[" + indexVal + "][course_university]']").val(value.course_university);
                                $( "select[name*='courses[" + indexVal + "][mode_study]']").val(value.mode_study);
                                $( "select[name*='courses[" + indexVal + "][course_title]']").val(value.course_title);
                                $( "select[name*='courses[" + indexVal + "][mode_study]']").val(value.mode_study);
                                $( "select[name*='courses[" + indexVal + "][campus_stage]']").val(value.campus_stage);
                                $( "select[name*='courses[" + indexVal + "][stage_name]']").val(value.stage_name);
                                $( "select[name*='courses[" + indexVal + "][entry_date]']").val(value.entry_date);
                                selectelement = $("#course-wrapper .formSelect");
                                selectelementSearch = $("#course-wrapper .searchSelect");
                                datepickerelement = $("#course-wrapper .datepicker");
                                break;
                            case 'tab_3':
                                $("#school-wrapper").append('<div class="mgv-wrap common-wrapper wrapper-' + wrapperCount + '"><h4 class="sub_title">Additional Qualification</h4><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button><div class="row"><div class=" col s6 "> <div class="input-field "> <label for="UG_board_name">Course Name</label> <select name="school[' + indexVal + '][UG_board_name]" id="" class="searchSelect"> <option value="">Select</option> <?php foreach($courses as $courseData) { echo '<option value="'.$courseData->course_name.'">'.$courseData->course_name.'</option>'; } ?> </select> </div> </div><div class=" col s6 "> <div class="input-field "> <label for="ug_university">University Name</label> <select name="school[' + indexVal + '][ug_university]" id="" class="searchSelect"> <option value="">Select</option> <?php foreach($university as $universityData) { echo '<option value="'.$universityData->university.'">'.$universityData->university.'</option>'; } ?> </select> </div> </div> </div><div class="row"><div class=" col s6 "> <div class="input-field "> <label for="sslcgrade" class="active">University Percentage(%)</label> <input class="form-control" id="" name="school[' + indexVal + '][UG_grade]" type="number" step=".01" min=1>  </div> </div><div class=" col s6 "> <div class="input-field "> <label for="UG_end_year">Pass Out Year</label> <select name="school[' + indexVal + '][UG_end_year]" id="" class="searchSelect"> <option value="">Select</option> <?php foreach(range(date('Y'), 1990) as $x) {  echo '<option value="'.$x.'"'.($x === date('Y') ? ' selected="selected"' : '').'>'.$x.'</option>'; } ?> </select>  </div> </div> </div><div class="row"><div class=" col s6 "> <div class="input-field "> <label for="UG_backlogs" class="active">Backlogs</label> <input class="form-control" id="" name="school[' + indexVal + '][UG_backlogs]" type="number">  </div> </div><div class=" col s6 "> <div class="input-field "> <label for="UG_mode_study" style="margin-top:-40px;">Mode of study</label> <select id="" name="school[' + indexVal + '][UG_mode_study]" class="formSelect"> <option value="">Select</option> <option value="Full-time"> Full time </option> <option value="Distance"> Distance </option> </select> </div> </div>   <div class="row"><div class="col s6"><label>Upload Documents</label><div class="file-field input-field"><div class="btn"><span>Upload</span><input type="file" multiple > <i class="fa fa-cloud-upload"></i></div><div class="file-path-wrapper"><input class="file-path validate" name="school[' + indexVal + '][UG_document]" type="text" placeholder = "Upload file" ></div></div><div class="uploaded-file" id="school[' + indexVal + '][UG_document]" data-type="file"></div></div></div>      </div> <div class="clearfix"></div></div>');
                                $( "select[name*='school[" + indexVal + "][UG_board_name]']").val(value.UG_board_name);
                                $( "select[name*='school[" + indexVal + "][ug_university]']").val(value.ug_university);
                                $( "input[name*='school[" + indexVal + "][UG_grade]']").val(value.UG_grade);
                                $( "select[name*='school[" + indexVal + "][UG_end_year]']").val(value.UG_end_year);
                                $( "input[name*='school[" + indexVal + "][UG_backlogs]']").val(value.UG_backlogs);
                                $( "select[name*='school[" + indexVal + "][UG_mode_study]']").val(value.UG_mode_study);
                                //$( "select[name*='school[" + indexVal + "][UG_document]']").val(value.UG_document);
                                selectelement = $("#school-wrapper .formSelect");
                                selectelementSearch = $("#school-wrapper .searchSelect");
                                datepickerelement = $("#school-wrapper .datepicker");

                                valueUG_document = value.UG_document;
                                if(valueUG_document != null && valueUG_document != '') { // display the uploaded files
                                    element = $('#school-' + indexVal + '-UG_document');
                                    key = 'school[' + indexVal + '][UG_document]';
                                    displayUploadedFiles(element,key,valueUG_document);
                                }

                                break;
							case 'tab_5':
								$("#employement-wrapper").append('<div class="stdhst common-wrapper wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button> <div class="col-md-6"> <div class="mg-input-field "> <label for="employer_name">Name of employer/organisation</label> <input class="form-control" id="" name="employement[' + indexVal + '][employer_name]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="job_position">Job Position</label> <input class="form-control" id="" name="employement[' + indexVal + '][job_position]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="from_date">From</label> <select name="employement[' + indexVal + '][from_date]" class="searchSelect" id=""> <option value="">Select</option> <?php $curr_yr = date('Y'); $curr_mon = date('m'); $months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec'); for($i=1990; $i<=$curr_yr;$i++) { foreach ($months as $key => $value) { if($i==$curr_yr && $key>$curr_mon) { } else { echo '<option value="'.$value.' '.$i.'">'.$value.' '.$i.'</option>'; } } } ?> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="to_date">To</label> <select name="employement[' + indexVal + '][to_date]" class="searchSelect" id=""> <option value="">Select</option> <?php for($i=1990; $i<=$curr_yr;$i++) { foreach ($months as $key => $value) { if($i==$curr_yr && $key>$curr_mon) { } else { echo '<option value="'.$value.' '.$i.'">'.$value.' '.$i.'</option>'; } } } ?> </select> </div> </div>      <div class="col-md-6"> <div class="mg-input-field "> <label for="experience">Year of Experience</label> <input class="form-control" id="" name="employement[' + indexVal + '][experience]" type="number" step=".01" min=1> </div> </div>     <div class="col-md-6"> <div class="mg-input-field "> <label for="work_mode">Mode of work</label> <select id="" name="employement[' + indexVal + '][work_mode]" class="formSelect"> <option value="">Select</option> <option value="Full-time"> Full time </option> <option value="Part-time"> Part time </option> </select> </div> </div>       <div class="col-md-6"> <!--<div class="mg-input-field "> --> <label>Experience letter</label> <div class="file-field input-field"> <div class="btn"> <span>Upload</span> <input type="file"><i class="fa fa-cloud-upload"></i> </div> <div class="file-path-wrapper"> <input class="file-path validate" name="employement[' + indexVal + '][experience_letter]" type="text"> <!--<div> </div>--> </div> </div> <div class="uploaded-file" id="employement[' + indexVal + '][experience_letter]" data-type="file"></div> </div> <!--</div> -->    <div class="clearfix"></div> </div>');
								
								//$( "input[name*='employement[" + key + "][employer_name]']").val(value.employer_name);
                                $( "input[name*='employement[" + indexVal + "][employer_name]']").val(value.employer_name);
								$( "input[name*='employement[" + indexVal + "][job_position]']").val(value.job_position);
                                $( "input[name*='employement[" + indexVal + "][experience]']").val(value.experience);
								$( "select[name*='employement[" + indexVal + "][from_date]']").val(value.from_date);
                                $( "select[name*='employement[" + indexVal + "][to_date]']").val(value.to_date);
								$( "select[name*='employement[" + indexVal + "][work_mode]']").val(value.work_mode);

								//selectelement = $("#employement-wrapper select");
                                selectelement = $("#employement-wrapper .formSelect");
                                selectelementSearch = $("#employement-wrapper .searchSelect");
								datepickerelement = $("#employement-wrapper .datepicker");
								valueExperience = value.experience_letter;
								if(valueExperience != null && valueExperience != '') { // display the uploaded files
									element = $('#employement-' + indexVal + '-experience_letter');
									key = 'employement[' + indexVal + '][experience_letter]';
									displayUploadedFiles(element,key,valueExperience);
								}
								break;
							case 'tab_7':
								$("#childrens-wrapper").append('<div class="stdhst common-wrapper wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button>   <div class="col-md-6"> <div class="mg-input-field "> <label for="name">Name (First Name + Family Name)</label> <input class="form-control" id="" name="children[' + indexVal + '][name]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="birthdate">Date of birth</label> <input class="form-control datepicker" id="" name="children[' + indexVal + '][birthdate]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="from_date">Relationship to you?</label> <select name="children[' + indexVal + '][relation]" id="" class="formSelect"> <option value="Son">Son</option> <option value="Daughter">Daughter</option> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="current_live">Does this person currently live with you?</label> <select name="children[' + indexVal + '][current_live]" id="" class="formSelect"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div><div class="col-md-6"> <div class="mg-input-field "> <label for="travel_to_uk">Is this person travelling with you to the UK?</label> <select name="children[' + indexVal + '][travel_to_uk]" id="" class="formSelect"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="clearfix"></div> </div>');
								
								//$( "input[name*='children[" + key + "][name]']").val(value.name);
                                $( "input[name*='children[" + indexVal + "][name]']").val(value.name);
								$( "input[name*='children[" + indexVal + "][birthdate]']").val(value.birthdate);
								$( "select[name*='children[" + indexVal + "][relation]']").val(value.relation);
								$( "select[name*='children[" + indexVal + "][current_live]']").val(value.current_live);
								$( "select[name*='children[" + indexVal + "][travel_to_uk]']").val(value.travel_to_uk);
								
								//selectelement = $("#childrens-wrapper select");
								//datepickerelement = $("#childrens-wrapper .datepicker");

                                selectelement = $("#childrens-wrapper .formSelect");
                                selectelementSearch = $("#childrens-wrapper .searchSelect");
                                datepickerelement = $("#childrens-wrapper .datepicker");							
								break;
							case 'tab_9':
								$("#imigration-wrapper").append('<div class="stdhst common-wrapper wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button>  <div class="col-md-6"> <div class="mg-input-field "> <label for="other_country_name">Country</label> <select name="imigration[' + indexVal + '][other_country_name]" class="searchSelect" id=""> <option value="">Select</option> <?php foreach($countries as $countriesData) { echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>'; } ?> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="reason">Reason for your visit</label> <select id="" name="imigration[' + indexVal + '][reason]" class="formSelect"> <option value="">Select</option> <option value="Work"> Work </option> <option value="Tourism"> Tourism </option> <option value="Others"> Others </option> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="visite_date">Date you entered</label> <input class="form-control datepicker" id="" name="imigration[' + indexVal + '][visite_date]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="leave_date">Date you left</label> <input class="form-control datepicker" id="" name="imigration[' + indexVal + '][leave_date]" type="text"> </div> </div> <div class="clearfix"></div>  </div>');

								//$( "input[name*='imigration[" + key + "][other_country_name]']").val(value.other_country_name);
                                $( "select[name*='imigration[" + indexVal + "][other_country_name]']").val(value.other_country_name);
								$( "select[name*='imigration[" + indexVal + "][reason]']").val(value.reason);
								$( "input[name*='imigration[" + indexVal + "][visite_date]']").val(value.visite_date);
								$( "input[name*='imigration[" + indexVal + "][leave_date]']").val(value.leave_date);
								
								//selectelement = $("#imigration-wrapper select");
								//datepickerelement = $("#imigration-wrapper .datepicker");

                                selectelement = $("#imigration-wrapper .formSelect");
                                selectelementSearch = $("#imigration-wrapper .searchSelect");
                                datepickerelement = $("#imigration-wrapper .datepicker");							
								break;

                                case 'tab_11':
                                $("#course-wrapper-new").append('<div class="stdhst common-wrapper wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button>    <div class="col-md-6"> <div class="mg-input-field "> <label for="course_country_new">Country</label> <select name="courses_new[' + indexVal + '][course_country_new]" id="" class="searchSelect"> <option value="">Select</option> <?php foreach($countries as $countriesData) { echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>'; } ?> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="level_study_new">Level of Study</label> <select name="courses_new[' + indexVal + '][level_study_new]" id="" class="formSelect"> <option value="">Select</option> <option value="Diploma"> Diploma </option> <option value="Bachelors"> Bachelors </option> <option value="Masters"> Masters </option> <option value="Doctoral"> Doctoral </option> </select> </div> </div>  <div class="col-md-6"> <div class="mg-input-field "> <label for="course_title_new">Course Name</label> <select id="" name="courses_new[' + indexVal + '][course_title_new]" class="searchSelect"> <option value="">Select</option> <?php foreach($courses as $courseData) { echo '<option value="'.$courseData->course_name.'">'.$courseData->course_name.'</option>'; } ?> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="mode_study_new">Mode of Study</label> <select name="courses_new[' + indexVal + '][mode_study_new]" id="" class="formSelect"> <option value="">Select</option> <option value="Full-time"> Full time </option> <option value="Distance"> Distance </option> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="campus_stage_new">Campus</label> <select id="" name="courses_new[' + indexVal + '][campus_stage_new]" class="searchSelect"> <option value="">Select</option> <?php foreach($campus as $campusData) { echo '<option value="'.$campusData->campus_name.'">'.$campusData->campus_name.'</option>'; } ?> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="stage_name_new">Stage (in months)</label><input class="form-control" id="" name="courses_new[' + indexVal + '][stage_name_new]" type="number" min="1"></div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="entry_date_new">Intake</label> <select name="courses_new[' + indexVal + '][entry_date_new]" id="" class="searchSelect"> <option value="">Select</option><?php $curr_yr = date('Y'); $curr_yr2 = date('Y', strtotime('-2 year')); $nxt_yr2 = date('Y', strtotime('+2 year')); $curr_mon = date('m'); $months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec'); for($i=$curr_yr2; $i<=$nxt_yr2;$i++) { foreach ($months as $key => $value) { /*if($i==$curr_yr && $key<$curr_mon) { } else { */ echo '<option value="'.$value.' '.$i.'">'.$value.' '.$i.'</option>'; /*} */ } } ?></select> </div> </div>  <div class="col-md-6"> <div class="mg-input-field "> <label for="course_university_new">University Name</label> <select name="courses_new[' + indexVal + '][course_university_new]" id="" class="searchSelect"> <option value="">Select</option> <?php foreach($university as $universityData) { echo '<option value="'.$universityData->university.'">'.$universityData->university.'</option>'; } ?> </select> </div></div>    <div class="col-md-6"> <div class="mg-input-field "> <label for="status_new">Status</label> <select name="courses_new[' + indexVal + '][status_new]" id="" class="searchSelect"> <option value="">Select</option> <option value="Received Application at Scholab">Received Application at Scholab</option> <option value="Application in Progress">Application in Progress</option> <option value="Application on hold by Scholab team">Application on hold by Scholab team</option> <option value="Application submitted to the University">Application submitted to the University</option> <option value="Conditional Offer Received">Conditional Offer Received</option> <option value="Unconditional Offer Received">Unconditional Offer Received</option> <option value="Rejected by University">Rejected by University</option> <option value="Application on hold by University">Application on hold by University</option> <option value="Case Closed - Student Not Qualified">Case Closed - Student Not Qualified</option> <option value="Case Closed - Program Closed">Case Closed - Program Closed</option> <option value="Case Closed - Offer Received - Student not interested to pay">Case Closed - Offer Received - Student not interested to pay</option> <option value="Tuition Fee Received">Tuition Fee Received</option> <option value="CAS Requested">CAS Requested</option> <option value="CAS Received">CAS Received</option> <option value="Visa In Process">Visa In Process</option> <option value="Visa Received">Visa Received</option> <option value="Visa Rejected">Visa Rejected</option> <option value="Student Deferred">Student Deferred</option> <option value="Refund Request Initiated">Refund Request Initiated</option> </select> </div> </div> <div class="col-md-8"> <div class="mg-input-field "> <label for="status_note">Representative Notes</label> <textarea class="" id="" name="courses_new[' + indexVal + '][status_note][0]"></textarea> <div class="rec-statNote-wrapp' + indexVal + '"></div> </div> </div><div class="clearfix"></div></div>'); 
                                $( "select[name*='courses_new[" + indexVal + "][course_country_new]']").val(value.course_country_new);
                                $( "select[name*='courses_new[" + indexVal + "][level_study_new]']").val(value.level_study_new);
                                $( "select[name*='courses_new[" + indexVal + "][course_university_new]']").val(value.course_university_new);
                                $( "select[name*='courses_new[" + indexVal + "][course_title_new]']").val(value.course_title_new);
                                $( "select[name*='courses_new[" + indexVal + "][mode_study_new]']").val(value.mode_study_new);
                                $( "select[name*='courses_new[" + indexVal + "][campus_stage_new]']").val(value.campus_stage_new);
                                $( "input[name*='courses_new[" + indexVal + "][stage_name_new]']").val(value.stage_name_new);
                                $( "select[name*='courses_new[" + indexVal + "][entry_date_new]']").val(value.entry_date_new);
                                $( "select[name*='courses_new[" + indexVal + "][status_new]']").val(value.status_new);

                                $('.rec-statNote-wrapp'+ indexVal).html('');
                                var statusNoteLn = value.status_note.length;
                                $.each(value.status_note, function(key, value) { // representative note field
                                    if(key == 0) {
                                        $( "textarea[name*='courses_new[" + indexVal + "][status_note][0]']").val(value);
                                    } else {
                                        $('.rec-statNote-wrapp'+ indexVal).append("<textarea name='courses_new[" + indexVal + "][status_note]["+key+"]' >"+value+"</textarea>");
                                    }
                                });
                                if(statusNoteLn > 0 && value.status_note[statusNoteLn-1] != '') {
                                    $('.rec-statNote-wrapp'+ indexVal).append("<textarea name='courses_new[" + indexVal + "][status_note]["+statusNoteLn+"]' ></textarea>");
                                }

                                selectelement = $("#course-wrapper-new .formSelect");
                                selectelementSearch = $("#course-wrapper-new .searchSelect");
                                datepickerelement = $("#course-wrapper-new .datepicker");                           
                                break;

                            case 'tab_13':
                                $("#follow_up-wrapper").append('<div class="stdhst common-wrapper wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button><div class="col-md-6"> <div class="mg-input-field "> <label for="follow_date">Date for next follow up</label>  <input class="futuredatepicker" id="" name="follow_up[' + indexVal + '][follow_date]" type="text">  </div> </div><div class="col-md-6"> <div class="mg-input-field "> <label for="test_time">Time for next follow up</label> <input class="timepicker" id="" name="follow_up[' + indexVal + '][follow_time]" type="text"> </div> </div><div class="col-md-12"> <div class="mg-input-field "> <label for="prefix">Representative Note</label> <textarea class="materialize-textarea" id="" name="follow_up[' + indexVal + '][representative_note]" maxlength="500"> </textarea> </div> </div><div class="clearfix"></div></div>');
                                $( "input[name*='follow_up[" + indexVal + "][follow_date]']").val(value.follow_date);
                                $( "input[name*='follow_up[" + indexVal + "][follow_time]']").val(value.follow_time);
                                $( "textarea[name*='follow_up[" + indexVal + "][representative_note]']").val(value.representative_note);
                                datepickerelement1 = $("#follow_up-wrapper .futuredatepicker");
                                timepickerelement = $("#follow_up-wrapper .timepicker");
                                break;

							default:
								break;
						}
					});
					/*selectelement.formSelect();
					datepickerelement.datepicker({yearRange: [1960,2022],changeMonth: true,changeYear: true});*/

                    if(selectelement != '')
                        selectelement.formSelect();
                    if(selectelementSearch != '')
                        selectelementSearch.select2();
                    if(datepickerelement != '')
                        datepickerelement.datepicker({yearRange: [1960,2022],changeMonth: true,changeYear: true});
                    if(datepickerelement1 != '')
                        datepickerelement1.datepicker({yearRange: [2021,2025],minDate: new Date(),selectMonths: true});
                    if(timepickerelement != '')
                        timepickerelement.timepicker();

                    $("#school-wrapper .select2-container--default").parent().find('label').css('margin-top','-40px');  
					$("#mx_students_view .tab-pane.active .wrapper-count").val(parseInt(wrapperCount)+1);
				} else if(key == 'json_data_studied' && value != null) {
					
                    var selectelement = selectelementSearch = datepickerelement = datepickerelement1 = timepickerelement = '';

					var jsonDataStudied = JSON.parse(value);
					$(".common-wrapper-studied").remove();

                    var wc=0; // Wrapper count
                    
					$.each(jsonDataStudied, function(key, value) {
						//wrapperCount = key+1; indexVal = key;

                        // Wrapper count
                        wrapperCount = parseInt(wc)+1;
                        indexVal = parseInt(wc);
                        wc = parseInt(wc)+1;
                        // End wrapper count

						$("#prev-studied-wrapper").append('<div class="stdhst common-wrapper-studied wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button> <div class="col-md-6"> <div class="mg-input-field "> <label for="studied_country">Country</label> <select name="imigration_studied[' + indexVal + '][studied_country]" id="" class="searchSelect"> <option value="">Select</option> <?php foreach($countries as $countriesData) { echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>'; } ?> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="reason">Visa start date</label> <input class="form-control datepicker" id="" name="imigration_studied[' + indexVal + '][studied_visa_start_date]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="visite_date">Visa end date</label> <input class="form-control datepicker" id="" name="imigration_studied[' + indexVal + '][studied_visa_end_date]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="studied_status">Status</label> <select id="" name="imigration_studied[' + indexVal + '][studied_status]" class="formSelect"> <option value="">Select</option> <option value="Completed">Completed</option> <option value="Not-completed">Not completed</option> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="studied_university">University</label> <select class="searchSelect" name="imigration_studied[' + indexVal + '][studied_university]" id=""> <option value="">Select</option> <?php foreach($university as $universityData) { echo '<option value="'.$universityData->university.'">'.$universityData->university.'</option>'; } ?> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="studied_course_title">Course title</label> <select id="" name="imigration_studied[' + indexVal + '][studied_course_title]" class="searchSelect"> <option value="">Select</option> <?php foreach($courses as $courseData) { echo '<option value="'.$courseData->course_name.'">'.$courseData->course_name.'</option>'; } ?> </select> </div> </div>  <div class="col-md-6"> <div class="mg-input-field "> <label for="studied_cas_start_date" style="margin-top: 15px;">CAS start date</label> <input class="form-control datepicker" id="" name="imigration_studied[' + indexVal + '][studied_cas_start_date]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="studied_cas_start_date" style="margin-top: 15px;">CAS end date</label> <input class="form-control datepicker" id="" name="imigration_studied[' + indexVal + '][studied_cas_end_date]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="studied_cas_start_date">CAS/Visa copy/transcripts</label> <input class="form-control" id="" name="imigration_studied[' + indexVal + '][cas_transcripts]" type="text"> </div> </div>  <div class="clearfix"></div>  </div>');

						//$( "select[name*='imigration_studied[" + key + "][studied_country]']").val(value.studied_country);
                        $( "select[name*='imigration_studied[" + indexVal + "][studied_country]']").val(value.studied_country);
						$( "input[name*='imigration_studied[" + indexVal + "][studied_visa_start_date]']").val(value.studied_visa_start_date);
						$( "input[name*='imigration_studied[" + indexVal + "][studied_visa_end_date]']").val(value.studied_visa_end_date);
						$( "select[name*='imigration_studied[" + indexVal + "][studied_university]']").val(value.studied_university);
						$( "select[name*='imigration_studied[" + indexVal + "][studied_course_title]']").val(value.studied_course_title);
						$( "select[name*='imigration_studied[" + indexVal + "][studied_status]']").val(value.studied_status);
						$( "input[name*='imigration_studied[" + indexVal + "][studied_cas_start_date]']").val(value.studied_cas_start_date);
						$( "input[name*='imigration_studied[" + indexVal + "][studied_cas_end_date]']").val(value.studied_cas_end_date);
						$( "input[name*='imigration_studied[" + indexVal + "][cas_transcripts]']").val(value.cas_transcripts);
						
						//selectelement = $("#prev-studied-wrapper select");
						//datepickerelement = $("#prev-studied-wrapper .datepicker");

                        selectelement = $("#prev-studied-wrapper .formSelect");
                        selectelementSearch = $("#prev-studied-wrapper .searchSelect");
                        datepickerelement = $("#prev-studied-wrapper .datepicker");

					});
					/*selectelement.formSelect();
					datepickerelement.datepicker({yearRange: [1960,2022],changeMonth: true,changeYear: true});*/

                    if(selectelement != '')
                        selectelement.formSelect();
                    if(selectelementSearch != '')
                        selectelementSearch.select2();
                    if(datepickerelement != '')
                        datepickerelement.datepicker({yearRange: [1960,2022],changeMonth: true,changeYear: true});
                    if(datepickerelement1 != '')
                        datepickerelement1.datepicker({yearRange: [2021,2025],minDate: new Date(),selectMonths: true});
                    if(timepickerelement != '')
                        timepickerelement.timepicker();

					$("#mx_students_view .tab-pane.active .wrapper-count-studied").val(parseInt(wrapperCount)+1);					
				}
			});
		}
	});			
}

//display the uploaded files
function displayUploadedFiles(element,key,value) { 
	
	element.removeClass("hide"); // remove hide class of frame to show image 
	//$(".appendedImages").remove();
	$("input[name='" + key + "']").val(value);
	var images = value.split(",");
	var extension = images[0].split("?")[0].substr( (images[0].split("?")[0].lastIndexOf('.') +1) );
	if($.inArray(extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
		element.css("background-image", "url(" + images[0] + ")");
		element.wrap('<a target="_blank" href="' + images[0].split("?")[0] + '"></a>');
	} else {
		switch (extension) {
			case "pdf":
				element.html('<i class="fa fa-file-pdf-o"></i>');
				break;
			default:
				element.html('<i class="fa fa-file-text-o"></i>');
				break;
		}
		element.wrap('<a target="_blank" href="' + images[0] + '"></a>');
	}
	if(images.length > 1) {
		for(i=1;i<=(images.length)-1;i++) {
			
			var extension = images[i].split("?")[0].substr( (images[i].split("?")[0].lastIndexOf('.') +1) );
			if($.inArray(extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
				element.parent().parent().append('<a target="_blank" class="appendedImages" href="' + images[i].split("?")[0] + '"><div style="background-image: url(' + images[i] + ')" class="uploaded-file" data-type="file"></div></a>');
			} else {
				switch (extension) {
					case "pdf":
						var appendedHtml = '<i class="fa fa-file-pdf-o"></i>';
						break;
					default:
						var appendedHtml = '<i class="fa fa-file-text-o"></i>';
						break;
				}
				element.parent().parent().append('<a target="_blank" class="appendedImages" href="' + images[i] + '"><div class="uploaded-file" data-type="file">' + appendedHtml +'</div></a>');
			}									
		}
	}	
}

function loadUploadedFiles() {
    // load folder files
    $.ajax({
        dataType: 'json',
        url: "{{ url(config('laraadmin.adminRoute') . '/uploaded_files') }}",
        success: function ( json ) {
            cntFiles = json.uploads;
            $("ul.files_container").empty();
            if(cntFiles.length) {
				//for (var index = 0; index < cntFiles.length; index++) {
                    var element = cntFiles[(cntFiles.length)-1];
                    var li = formatFile(element);
					if($.inArray(element.extension, ["jpg", "jpeg", "png", "gif", "bmp"]) == -1) {
						switch (element.extension) {
							case "pdf":
								image = '<i class="fa fa-file-pdf-o"></i>';
								break;
							default:
								image = '<i class="fa fa-file-text-o"></i>';
								break;
						}						
					} else{
						image = '<img src='+li+'>';
					}
					if($(elem).parent().prev().find('input').attr("multiple") == undefined) {
						$(elem).closest('div').find('img').remove();
					}
                    $(elem).closest('div').append(image);
					
					var elementNameObj = $('input[name="'+$(elem).attr('name')+'"]');
					currentValue = elementNameObj.val();
					li = (currentValue != '') ? currentValue + ',' + li : li;
                    elementNameObj.val(li);
                //}
            } else {
                $("ul.files_container").html("<div class='text-center text-danger' style='margin-top:40px;'>No Files</div>");
            }
			$('#AddModal').modal('close');
        }
    });
}
function formatFile(upload) {
    var image = '';
    var url='';
    if($.inArray(upload.extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
		url=bsurl+'/files/'+upload.hash+'/'+upload.name+'?s=130';
        image = '<img src='+url+'>';
    } else {
		url=bsurl+'/files/'+upload.hash+'/'+upload.name;
		/*
        switch (upload.extension) {
            case "pdf":
                image = '<i class="fa fa-file-pdf-o"></i>';
                break;
            default:
                image = '<i class="fa fa-file-text-o"></i>';
                break;
        }
		*/
    }
	
    return url;
}

</script>
@endpush



