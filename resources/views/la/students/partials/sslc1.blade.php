<div id="tab_3" class="tab-pane " >
                                
                                <div class="content-head">
                                                <h4>School / College & University education <small>(from age 11, starting with the most recent)</small> </h4>
                                                <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                                        </div>
                                        {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'school-add-form']) !!}
               
                        <div class="pane">
                            <div class="mgv-wrap">
								<h4 class="sub_title">GCSE / 10th Board</h4>
                                <div class="row">
									<div class=" col s6 ">
										<div class="input-field">
											<input id="board_name"  name="board_name" type="text" class="validate">
											<label for="board_name">Name of Institution</label>
										</div>
									</div>
									<div class=" col s6 ">
										<div class="input-field">
											<select name="country_sslc" id="country_sslc">
												  <option value="">Select</option>
												  <option value="Afghanistan"> Afghanistan </option>
												  <option value="Albania"> Albania </option>
												  <option value="Algeria"> Algeria </option>
												  <option value="Andorra"> Andorra </option>
												  <option value="Angola"> Angola </option>
												  <option value="Antigua and Barbuda"> Antigua and Barbuda </option>
												  <option value="Argentina"> Argentina </option>
												  <option value="Armenia"> Armenia </option>
												  <option value="Australia"> Australia </option>
												  <option value="Austria"> Austria </option>
												  <option value="Azerbaijan"> Azerbaijan </option>
												  <option value="Bahamas"> Bahamas </option>
												  <option value="Bahrain"> Bahrain </option>
												  <option value="Bangladesh"> Bangladesh </option>
												  <option value="Barbados"> Barbados </option>
												  <option value="Belarus"> Belarus </option>
												  <option value="Belgium"> Belgium </option>
												  <option value="Belize"> Belize </option>
												  <option value="Benin"> Benin </option>
												  <option value="Bhutan"> Bhutan </option>
												  <option value="Bolivia"> Bolivia </option>
												  <option value="Bosnia and Herzegovina"> Bosnia and Herzegovina </option>
												  <option value="Botswana"> Botswana </option>
												  <option value="Brazil"> Brazil </option>
												  <option value="Brunei"> Brunei </option>
												  <option value="Bulgaria"> Bulgaria </option>
												  <option value="Burkina Faso"> Burkina Faso </option>
												  <option value="Burundi"> Burundi </option>
												  <option value="Cabo Verde"> Cabo Verde </option>
												  <option value="Cambodia"> Cambodia </option>
												  <option value="Cameroon"> Cameroon </option>
												  <option value="Canada"> Canada </option>
												  <option value="Central African Republic (CAR)"> Central African Republic (CAR) </option>
												  <option value="Chad"> Chad </option>
												  <option value="Chile"> Chile </option>
												  <option value="China"> China </option>
												  <option value="Colombia"> Colombia </option>
												  <option value="Comoros"> Comoros </option>
												  <option value="Congo, Democratic Republic of the"> Congo, Democratic Republic of the </option>
												  <option value="Congo, Republic of the"> Congo, Republic of the </option>
												  <option value="Costa Rica"> Costa Rica </option>
												  <option value="Cote d'Ivoire"> Cote d'Ivoire </option>
												  <option value="Croatia"> Croatia </option>
												  <option value="Cuba"> Cuba </option>
												  <option value="Cyprus"> Cyprus </option>
												  <option value="Czechia"> Czechia </option>
												  <option value="Denmark"> Denmark </option>
												  <option value="Djibouti"> Djibouti </option>
												  <option value="Dominica"> Dominica </option>
												  <option value="Dominican Republic"> Dominican Republic </option>
												  <option value="Ecuador"> Ecuador </option>
												  <option value="Egypt"> Egypt </option>
												  <option value="El Salvador"> El Salvador </option>
												  <option value="Equatorial Guinea"> Equatorial Guinea </option>
												  <option value="Eritrea"> Eritrea </option>
												  <option value="Estonia"> Estonia </option>
												  <option value="Eswatini (formerly Swaziland)"> Eswatini (formerly Swaziland) </option>
												  <option value="Ethiopia"> Ethiopia </option>
												  <option value="Fiji"> Fiji </option>
												  <option value="Finland"> Finland </option>
												  <option value="France"> France </option>
												  <option value="Gabon"> Gabon </option>
												  <option value="Gambia"> Gambia </option>
												  <option value="Georgia"> Georgia </option>
												  <option value="Germany"> Germany </option>
												  <option value="Ghana"> Ghana </option>
												  <option value="Greece"> Greece </option>
												  <option value="Grenada"> Grenada </option>
												  <option value="Guatemala"> Guatemala </option>
												  <option value="Guinea"> Guinea </option>
												  <option value="Guinea-Bissau"> Guinea-Bissau </option>
												  <option value="Guyana"> Guyana </option>
												  <option value="Haiti"> Haiti </option>
												  <option value="Honduras"> Honduras </option>
												  <option value="Hungary"> Hungary </option>
												  <option value="Iceland"> Iceland </option>
												  <option value="India"> India </option>
												  <option value="Indonesia"> Indonesia </option>
												  <option value="Iran"> Iran </option>
												  <option value="Iraq"> Iraq </option>
												  <option value="Ireland"> Ireland </option>
												  <option value="Israel"> Israel </option>
												  <option value="Italy"> Italy </option>
												  <option value="Jamaica"> Jamaica </option>
												  <option value="Japan"> Japan </option>
												  <option value="Jordan"> Jordan </option>
												  <option value="Kazakhstan"> Kazakhstan </option>
												  <option value="Kenya"> Kenya </option>
												  <option value="Kiribati"> Kiribati </option>
												  <option value="Kosovo"> Kosovo </option>
												  <option value="Kuwait"> Kuwait </option>
												  <option value="Kyrgyzstan"> Kyrgyzstan </option>
												  <option value="Laos"> Laos </option>
												  <option value="Latvia"> Latvia </option>
												  <option value="Lebanon"> Lebanon </option>
												  <option value="Lesotho"> Lesotho </option>
												  <option value="Liberia"> Liberia </option>
												  <option value="Libya"> Libya </option>
												  <option value="Liechtenstein"> Liechtenstein </option>
												  <option value="Lithuania"> Lithuania </option>
												  <option value="Luxembourg"> Luxembourg </option>
												  <option value="Madagascar"> Madagascar </option>
												  <option value="Malawi"> Malawi </option>
												  <option value="Malaysia"> Malaysia </option>
												  <option value="Maldives"> Maldives </option>
												  <option value="Mali"> Mali </option>
												  <option value="Malta"> Malta </option>
												  <option value="Marshall Islands"> Marshall Islands </option>
												  <option value="Mauritania"> Mauritania </option>
												  <option value="Mauritius"> Mauritius </option>
												  <option value="Mexico"> Mexico </option>
												  <option value="Micronesia"> Micronesia </option>
												  <option value="Moldova"> Moldova </option>
												  <option value="Monaco"> Monaco </option>
												  <option value="Mongolia"> Mongolia </option>
												  <option value="Montenegro"> Montenegro </option>
												  <option value="Morocco"> Morocco </option>
												  <option value="Mozambique"> Mozambique </option>
												  <option value="Myanmar (formerly Burma)"> Myanmar (formerly Burma) </option>
												  <option value="Namibia"> Namibia </option>
												  <option value="Nauru"> Nauru </option>
												  <option value="Nepal"> Nepal </option>
												  <option value="Netherlands"> Netherlands </option>
												  <option value="New Zealand"> New Zealand </option>
												  <option value="Nicaragua"> Nicaragua </option>
												  <option value="Niger"> Niger </option>
												  <option value="Nigeria"> Nigeria </option>
												  <option value="North Korea"> North Korea </option>
												  <option value="North Macedonia (formerly Macedonia)"> North Macedonia (formerly Macedonia) </option>
												  <option value="Norway"> Norway </option>
												  <option value="Oman"> Oman </option>
												  <option value="Pakistan"> Pakistan </option>
												  <option value="Palau"> Palau </option>
												  <option value="Palestine"> Palestine </option>
												  <option value="Panama"> Panama </option>
												  <option value="Papua New Guinea"> Papua New Guinea </option>
												  <option value="Paraguay"> Paraguay </option>
												  <option value="Peru"> Peru </option>
												  <option value="Philippines"> Philippines </option>
												  <option value="Poland"> Poland </option>
												  <option value="Portugal"> Portugal </option>
												  <option value="Qatar"> Qatar </option>
												  <option value="Romania"> Romania </option>
												  <option value="Russia"> Russia </option>
												  <option value="Rwanda"> Rwanda </option>
												  <option value="Saint Kitts and Nevis"> Saint Kitts and Nevis </option>
												  <option value="Saint Lucia"> Saint Lucia </option>
												  <option value="Saint Vincent and the Grenadines"> Saint Vincent and the Grenadines </option>
												  <option value="Samoa"> Samoa </option>
												  <option value="San Marino"> San Marino </option>
												  <option value="Sao Tome and Principe"> Sao Tome and Principe </option>
												  <option value="Saudi Arabia"> Saudi Arabia </option>
												  <option value="Senegal"> Senegal </option>
												  <option value="Serbia"> Serbia </option>
												  <option value="Seychelles"> Seychelles </option>
												  <option value="Sierra Leone"> Sierra Leone </option>
												  <option value="Singapore"> Singapore </option>
												  <option value="Slovakia"> Slovakia </option>
												  <option value="Slovenia"> Slovenia </option>
												  <option value="Solomon Islands"> Solomon Islands </option>
												  <option value="Somalia"> Somalia </option>
												  <option value="South Africa"> South Africa </option>
												  <option value="South Korea"> South Korea </option>
												  <option value="South Sudan"> South Sudan </option>
												  <option value="Spain"> Spain </option>
												  <option value="Sri Lanka"> Sri Lanka </option>
												  <option value="Sudan"> Sudan </option>
												  <option value="Suriname"> Suriname </option>
												  <option value="Sweden"> Sweden </option>
												  <option value="Switzerland"> Switzerland </option>
												  <option value="Syria"> Syria </option>
												  <option value="Taiwan"> Taiwan </option>
												  <option value="Tajikistan"> Tajikistan </option>
												  <option value="Tanzania"> Tanzania </option>
												  <option value="Thailand"> Thailand </option>
												  <option value="Timor-Leste"> Timor-Leste </option>
												  <option value="Togo"> Togo </option>
												  <option value="Tonga"> Tonga </option>
												  <option value="Trinidad and Tobago"> Trinidad and Tobago </option>
												  <option value="Tunisia"> Tunisia </option>
												  <option value="Turkey"> Turkey </option>
												  <option value="Turkmenistan"> Turkmenistan </option>
												  <option value="Tuvalu"> Tuvalu </option>
												  <option value="Uganda"> Uganda </option>
												  <option value="Ukraine"> Ukraine </option>
												  <option value="United Arab Emirates (UAE)"> United Arab Emirates (UAE) </option>
												  <option value="United Kingdom (UK)"> United Kingdom (UK) </option>
												  <option value="United States of America (USA)"> United States of America (USA) </option>
												  <option value="Uruguay"> Uruguay </option>
												  <option value="Uzbekistan"> Uzbekistan </option>
												  <option value="Vanuatu"> Vanuatu </option>
												  <option value="Vatican City (Holy See)"> Vatican City (Holy See) </option>
												  <option value="Venezuela"> Venezuela </option>
												  <option value="Vietnam"> Vietnam </option>
												  <option value="Yemen"> Yemen </option>
												  <option value="Zambia"> Zambia </option>
												  <option value="Zimbabwe"> Zimbabwe </option>
											</select>											
											<label for="country">Country</label>
										</div>
									</div>
                                </div>

                                <div class="row">
                                        <div class="col s6">
                                                <div class="input-field ">
                                                                <input id="start_year" name="start_year" type="text" class="validate datepicker">
                                                                <label for="start_year">Start Year</label>
                                                                <!-- <span class="helper-text" data-error="wrong"
                                                                data-success="right">As per Passport</span> -->
                                                        </div>
                                        </div>
                                        <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="end_year" name="end_year" type="text" class="validate datepicker">
                                                                        <label for="end_year">Completion Year</label>

                                                                </div>
                                        </div>
                                </div>
                                <div class="row">
									<div class=" col s6 ">
										<div class="input-field">
											<select name="mode_study_school" id="mode_study_school">
												<option value="Full-time">Full time</option>
												<option value="Part-time">Part time</option>
												<option value="Distance-learning">Distance Learning</option>
											</select>	
											<label for="mode_study_school">Mode of study</label>
										</div>
									</div>
									<div class=" col s6 ">
										<div class="input-field">
											<input id="qualification"  name="qualification" type="text" class="validate">										
											<label for="qualification">Qualification eg.BA(Hons)</label>
										</div>
									</div>
                                </div>								
                                <div class="row">
                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="grade" name="grade" type="text" class="validate">
                                                                                <label for="grade">Percentage / Grade Obtained</label>

                                                                        </div>
                                                </div>								
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="medium_of_instr" name="medium_of_instr" type="text" class="validate">
                                                                        <label for="language">Medium of Instruction</label>

                                                                </div>
                                                </div>
								</div>

								<div class="row">
                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="eng_score" name="eng_score" type="text" class="validate">
                                                                                <label for="eng_score">Marks Obtained in English</label>

                                                                        </div>
                                                </div>


                                </div>
                                <div class="row">
                                        <div class="col s6">
                                                <label>Upload Documents</label>
                                                <div class="file-field input-field">
                                                                <div class="btn">
                                                                        <span>Upload</span>
                                                                        <input type="file" multiple > <i class="fa fa-cloud-upload"></i>
                                                                </div>
                                                                <div class="file-path-wrapper">
                                                                        <input class="file-path validate" name="document" type="text" placeholder = "Upload file" >
                                                                </div>
                                                </div>
												<div class="uploaded-file" id="document" data-type="file"></div>	
                                        </div>
								</div>
                        </div>
						<div class="mgv-wrap">

					
								<h4 class="sub_title">A level / 12th Board</h4>
                                <div class="row">
									<div class=" col s6 ">
										<div class="input-field">
											<input id="hse_board_name"  name="hse_board_name" type="text" class="validate">
											<label for="hse_board_name">Name of Institution</label>
										</div>
									</div>
									<div class=" col s6 ">
										<div class="input-field">
											<select name="hse_country" id="hse_country">
												  <option value="">Select</option>
												  <option value="Afghanistan"> Afghanistan </option>
												  <option value="Albania"> Albania </option>
												  <option value="Algeria"> Algeria </option>
												  <option value="Andorra"> Andorra </option>
												  <option value="Angola"> Angola </option>
												  <option value="Antigua and Barbuda"> Antigua and Barbuda </option>
												  <option value="Argentina"> Argentina </option>
												  <option value="Armenia"> Armenia </option>
												  <option value="Australia"> Australia </option>
												  <option value="Austria"> Austria </option>
												  <option value="Azerbaijan"> Azerbaijan </option>
												  <option value="Bahamas"> Bahamas </option>
												  <option value="Bahrain"> Bahrain </option>
												  <option value="Bangladesh"> Bangladesh </option>
												  <option value="Barbados"> Barbados </option>
												  <option value="Belarus"> Belarus </option>
												  <option value="Belgium"> Belgium </option>
												  <option value="Belize"> Belize </option>
												  <option value="Benin"> Benin </option>
												  <option value="Bhutan"> Bhutan </option>
												  <option value="Bolivia"> Bolivia </option>
												  <option value="Bosnia and Herzegovina"> Bosnia and Herzegovina </option>
												  <option value="Botswana"> Botswana </option>
												  <option value="Brazil"> Brazil </option>
												  <option value="Brunei"> Brunei </option>
												  <option value="Bulgaria"> Bulgaria </option>
												  <option value="Burkina Faso"> Burkina Faso </option>
												  <option value="Burundi"> Burundi </option>
												  <option value="Cabo Verde"> Cabo Verde </option>
												  <option value="Cambodia"> Cambodia </option>
												  <option value="Cameroon"> Cameroon </option>
												  <option value="Canada"> Canada </option>
												  <option value="Central African Republic (CAR)"> Central African Republic (CAR) </option>
												  <option value="Chad"> Chad </option>
												  <option value="Chile"> Chile </option>
												  <option value="China"> China </option>
												  <option value="Colombia"> Colombia </option>
												  <option value="Comoros"> Comoros </option>
												  <option value="Congo, Democratic Republic of the"> Congo, Democratic Republic of the </option>
												  <option value="Congo, Republic of the"> Congo, Republic of the </option>
												  <option value="Costa Rica"> Costa Rica </option>
												  <option value="Cote d'Ivoire"> Cote d'Ivoire </option>
												  <option value="Croatia"> Croatia </option>
												  <option value="Cuba"> Cuba </option>
												  <option value="Cyprus"> Cyprus </option>
												  <option value="Czechia"> Czechia </option>
												  <option value="Denmark"> Denmark </option>
												  <option value="Djibouti"> Djibouti </option>
												  <option value="Dominica"> Dominica </option>
												  <option value="Dominican Republic"> Dominican Republic </option>
												  <option value="Ecuador"> Ecuador </option>
												  <option value="Egypt"> Egypt </option>
												  <option value="El Salvador"> El Salvador </option>
												  <option value="Equatorial Guinea"> Equatorial Guinea </option>
												  <option value="Eritrea"> Eritrea </option>
												  <option value="Estonia"> Estonia </option>
												  <option value="Eswatini (formerly Swaziland)"> Eswatini (formerly Swaziland) </option>
												  <option value="Ethiopia"> Ethiopia </option>
												  <option value="Fiji"> Fiji </option>
												  <option value="Finland"> Finland </option>
												  <option value="France"> France </option>
												  <option value="Gabon"> Gabon </option>
												  <option value="Gambia"> Gambia </option>
												  <option value="Georgia"> Georgia </option>
												  <option value="Germany"> Germany </option>
												  <option value="Ghana"> Ghana </option>
												  <option value="Greece"> Greece </option>
												  <option value="Grenada"> Grenada </option>
												  <option value="Guatemala"> Guatemala </option>
												  <option value="Guinea"> Guinea </option>
												  <option value="Guinea-Bissau"> Guinea-Bissau </option>
												  <option value="Guyana"> Guyana </option>
												  <option value="Haiti"> Haiti </option>
												  <option value="Honduras"> Honduras </option>
												  <option value="Hungary"> Hungary </option>
												  <option value="Iceland"> Iceland </option>
												  <option value="India"> India </option>
												  <option value="Indonesia"> Indonesia </option>
												  <option value="Iran"> Iran </option>
												  <option value="Iraq"> Iraq </option>
												  <option value="Ireland"> Ireland </option>
												  <option value="Israel"> Israel </option>
												  <option value="Italy"> Italy </option>
												  <option value="Jamaica"> Jamaica </option>
												  <option value="Japan"> Japan </option>
												  <option value="Jordan"> Jordan </option>
												  <option value="Kazakhstan"> Kazakhstan </option>
												  <option value="Kenya"> Kenya </option>
												  <option value="Kiribati"> Kiribati </option>
												  <option value="Kosovo"> Kosovo </option>
												  <option value="Kuwait"> Kuwait </option>
												  <option value="Kyrgyzstan"> Kyrgyzstan </option>
												  <option value="Laos"> Laos </option>
												  <option value="Latvia"> Latvia </option>
												  <option value="Lebanon"> Lebanon </option>
												  <option value="Lesotho"> Lesotho </option>
												  <option value="Liberia"> Liberia </option>
												  <option value="Libya"> Libya </option>
												  <option value="Liechtenstein"> Liechtenstein </option>
												  <option value="Lithuania"> Lithuania </option>
												  <option value="Luxembourg"> Luxembourg </option>
												  <option value="Madagascar"> Madagascar </option>
												  <option value="Malawi"> Malawi </option>
												  <option value="Malaysia"> Malaysia </option>
												  <option value="Maldives"> Maldives </option>
												  <option value="Mali"> Mali </option>
												  <option value="Malta"> Malta </option>
												  <option value="Marshall Islands"> Marshall Islands </option>
												  <option value="Mauritania"> Mauritania </option>
												  <option value="Mauritius"> Mauritius </option>
												  <option value="Mexico"> Mexico </option>
												  <option value="Micronesia"> Micronesia </option>
												  <option value="Moldova"> Moldova </option>
												  <option value="Monaco"> Monaco </option>
												  <option value="Mongolia"> Mongolia </option>
												  <option value="Montenegro"> Montenegro </option>
												  <option value="Morocco"> Morocco </option>
												  <option value="Mozambique"> Mozambique </option>
												  <option value="Myanmar (formerly Burma)"> Myanmar (formerly Burma) </option>
												  <option value="Namibia"> Namibia </option>
												  <option value="Nauru"> Nauru </option>
												  <option value="Nepal"> Nepal </option>
												  <option value="Netherlands"> Netherlands </option>
												  <option value="New Zealand"> New Zealand </option>
												  <option value="Nicaragua"> Nicaragua </option>
												  <option value="Niger"> Niger </option>
												  <option value="Nigeria"> Nigeria </option>
												  <option value="North Korea"> North Korea </option>
												  <option value="North Macedonia (formerly Macedonia)"> North Macedonia (formerly Macedonia) </option>
												  <option value="Norway"> Norway </option>
												  <option value="Oman"> Oman </option>
												  <option value="Pakistan"> Pakistan </option>
												  <option value="Palau"> Palau </option>
												  <option value="Palestine"> Palestine </option>
												  <option value="Panama"> Panama </option>
												  <option value="Papua New Guinea"> Papua New Guinea </option>
												  <option value="Paraguay"> Paraguay </option>
												  <option value="Peru"> Peru </option>
												  <option value="Philippines"> Philippines </option>
												  <option value="Poland"> Poland </option>
												  <option value="Portugal"> Portugal </option>
												  <option value="Qatar"> Qatar </option>
												  <option value="Romania"> Romania </option>
												  <option value="Russia"> Russia </option>
												  <option value="Rwanda"> Rwanda </option>
												  <option value="Saint Kitts and Nevis"> Saint Kitts and Nevis </option>
												  <option value="Saint Lucia"> Saint Lucia </option>
												  <option value="Saint Vincent and the Grenadines"> Saint Vincent and the Grenadines </option>
												  <option value="Samoa"> Samoa </option>
												  <option value="San Marino"> San Marino </option>
												  <option value="Sao Tome and Principe"> Sao Tome and Principe </option>
												  <option value="Saudi Arabia"> Saudi Arabia </option>
												  <option value="Senegal"> Senegal </option>
												  <option value="Serbia"> Serbia </option>
												  <option value="Seychelles"> Seychelles </option>
												  <option value="Sierra Leone"> Sierra Leone </option>
												  <option value="Singapore"> Singapore </option>
												  <option value="Slovakia"> Slovakia </option>
												  <option value="Slovenia"> Slovenia </option>
												  <option value="Solomon Islands"> Solomon Islands </option>
												  <option value="Somalia"> Somalia </option>
												  <option value="South Africa"> South Africa </option>
												  <option value="South Korea"> South Korea </option>
												  <option value="South Sudan"> South Sudan </option>
												  <option value="Spain"> Spain </option>
												  <option value="Sri Lanka"> Sri Lanka </option>
												  <option value="Sudan"> Sudan </option>
												  <option value="Suriname"> Suriname </option>
												  <option value="Sweden"> Sweden </option>
												  <option value="Switzerland"> Switzerland </option>
												  <option value="Syria"> Syria </option>
												  <option value="Taiwan"> Taiwan </option>
												  <option value="Tajikistan"> Tajikistan </option>
												  <option value="Tanzania"> Tanzania </option>
												  <option value="Thailand"> Thailand </option>
												  <option value="Timor-Leste"> Timor-Leste </option>
												  <option value="Togo"> Togo </option>
												  <option value="Tonga"> Tonga </option>
												  <option value="Trinidad and Tobago"> Trinidad and Tobago </option>
												  <option value="Tunisia"> Tunisia </option>
												  <option value="Turkey"> Turkey </option>
												  <option value="Turkmenistan"> Turkmenistan </option>
												  <option value="Tuvalu"> Tuvalu </option>
												  <option value="Uganda"> Uganda </option>
												  <option value="Ukraine"> Ukraine </option>
												  <option value="United Arab Emirates (UAE)"> United Arab Emirates (UAE) </option>
												  <option value="United Kingdom (UK)"> United Kingdom (UK) </option>
												  <option value="United States of America (USA)"> United States of America (USA) </option>
												  <option value="Uruguay"> Uruguay </option>
												  <option value="Uzbekistan"> Uzbekistan </option>
												  <option value="Vanuatu"> Vanuatu </option>
												  <option value="Vatican City (Holy See)"> Vatican City (Holy See) </option>
												  <option value="Venezuela"> Venezuela </option>
												  <option value="Vietnam"> Vietnam </option>
												  <option value="Yemen"> Yemen </option>
												  <option value="Zambia"> Zambia </option>
												  <option value="Zimbabwe"> Zimbabwe </option>
											</select>											
											<label for="hse_country">Country</label>
										</div>
									</div>
                                </div>

                                <div class="row">
                                        <div class="col s6">
                                                <div class="input-field ">
                                                                <input id="hse_start_year" name="hse_start_year" type="text" class="validate datepicker">
                                                                <label for="hse_start_year">Start Year</label>
                                                                <!-- <span class="helper-text" data-error="wrong"
                                                                data-success="right">As per Passport</span> -->
                                                        </div>
                                        </div>
                                        <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="hse_end_year" name="hse_end_year" type="text" class="validate datepicker">
                                                                        <label for="hse_end_year">Completion Year</label>

                                                                </div>
                                        </div>
                                </div>
                                <div class="row">
									<div class=" col s6 ">
										<div class="input-field">
											<select name="hse_mode_study" id="hse_mode_study">
												<option value="Full-time">Full time</option>
												<option value="Part-time">Part time</option>
												<option value="Distance-learning">Distance Learning</option>
											</select>	
											<label for="hse_mode_study">Mode of study</label>
										</div>
									</div>
									<div class=" col s6 ">
										<div class="input-field">
											<input id="hse_qualification"  name="hse_qualification" type="text" class="validate">										
											<label for="hse_qualification">Qualification eg.BA(Hons)</label>
										</div>
									</div>
                                </div>								
                                <div class="row">
                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="hse_grade" name="hse_grade" type="text" class="validate">
                                                                                <label for="sslcgrade">Percentage / Grade Obtained</label>

                                                                        </div>
                                                </div>								
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="hse_medium_of_instr" name="hse_medium_of_instr" type="text" class="validate">
                                                                        <label for="hse_language">Medium of Instruction</label>

                                                                </div>
                                                </div>
								</div>
	
								<div class="row">
                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="hse_eng_score" name="hse_eng_score" type="text" class="validate">
                                                                                <label for="hse_eng_score">Marks Obtained in English</label>

                                                                        </div>
                                                </div>


                                </div>
                                <div class="row">
                                        <div class="col s6">
                                                <label>Upload Documents</label>
                                                <div class="file-field input-field">
                                                                <div class="btn">
                                                                        <span>Upload</span>
                                                                        <input type="file" multiple > <i class="fa fa-cloud-upload"></i>
                                                                </div>
                                                                <div class="file-path-wrapper">
                                                                        <input class="file-path validate" name="hse_document" type="text" placeholder = "Upload file" >
                                                                </div>
                                                </div>
												<div class="uploaded-file" id="hse_document" data-type="file"></div>	
                                        </div>
                                </div>								
								</div>
							<div class="mgv-wrap">
								<h4 class="sub_title">Bachelors / UG</h4>
                                <div class="row">
									<div class=" col s6 ">
										<div class="input-field">
											<input id="UG_board_name"  name="UG_board_name" type="text" class="validate">
											<label for="UG_board_name">Name of Institution</label>
										</div>
									</div>
									<div class=" col s6 ">
										<div class="input-field">
											<select name="UG_country" id="UG_country">
												  <option value="">Select</option>
												  <option value="Afghanistan"> Afghanistan </option>
												  <option value="Albania"> Albania </option>
												  <option value="Algeria"> Algeria </option>
												  <option value="Andorra"> Andorra </option>
												  <option value="Angola"> Angola </option>
												  <option value="Antigua and Barbuda"> Antigua and Barbuda </option>
												  <option value="Argentina"> Argentina </option>
												  <option value="Armenia"> Armenia </option>
												  <option value="Australia"> Australia </option>
												  <option value="Austria"> Austria </option>
												  <option value="Azerbaijan"> Azerbaijan </option>
												  <option value="Bahamas"> Bahamas </option>
												  <option value="Bahrain"> Bahrain </option>
												  <option value="Bangladesh"> Bangladesh </option>
												  <option value="Barbados"> Barbados </option>
												  <option value="Belarus"> Belarus </option>
												  <option value="Belgium"> Belgium </option>
												  <option value="Belize"> Belize </option>
												  <option value="Benin"> Benin </option>
												  <option value="Bhutan"> Bhutan </option>
												  <option value="Bolivia"> Bolivia </option>
												  <option value="Bosnia and Herzegovina"> Bosnia and Herzegovina </option>
												  <option value="Botswana"> Botswana </option>
												  <option value="Brazil"> Brazil </option>
												  <option value="Brunei"> Brunei </option>
												  <option value="Bulgaria"> Bulgaria </option>
												  <option value="Burkina Faso"> Burkina Faso </option>
												  <option value="Burundi"> Burundi </option>
												  <option value="Cabo Verde"> Cabo Verde </option>
												  <option value="Cambodia"> Cambodia </option>
												  <option value="Cameroon"> Cameroon </option>
												  <option value="Canada"> Canada </option>
												  <option value="Central African Republic (CAR)"> Central African Republic (CAR) </option>
												  <option value="Chad"> Chad </option>
												  <option value="Chile"> Chile </option>
												  <option value="China"> China </option>
												  <option value="Colombia"> Colombia </option>
												  <option value="Comoros"> Comoros </option>
												  <option value="Congo, Democratic Republic of the"> Congo, Democratic Republic of the </option>
												  <option value="Congo, Republic of the"> Congo, Republic of the </option>
												  <option value="Costa Rica"> Costa Rica </option>
												  <option value="Cote d'Ivoire"> Cote d'Ivoire </option>
												  <option value="Croatia"> Croatia </option>
												  <option value="Cuba"> Cuba </option>
												  <option value="Cyprus"> Cyprus </option>
												  <option value="Czechia"> Czechia </option>
												  <option value="Denmark"> Denmark </option>
												  <option value="Djibouti"> Djibouti </option>
												  <option value="Dominica"> Dominica </option>
												  <option value="Dominican Republic"> Dominican Republic </option>
												  <option value="Ecuador"> Ecuador </option>
												  <option value="Egypt"> Egypt </option>
												  <option value="El Salvador"> El Salvador </option>
												  <option value="Equatorial Guinea"> Equatorial Guinea </option>
												  <option value="Eritrea"> Eritrea </option>
												  <option value="Estonia"> Estonia </option>
												  <option value="Eswatini (formerly Swaziland)"> Eswatini (formerly Swaziland) </option>
												  <option value="Ethiopia"> Ethiopia </option>
												  <option value="Fiji"> Fiji </option>
												  <option value="Finland"> Finland </option>
												  <option value="France"> France </option>
												  <option value="Gabon"> Gabon </option>
												  <option value="Gambia"> Gambia </option>
												  <option value="Georgia"> Georgia </option>
												  <option value="Germany"> Germany </option>
												  <option value="Ghana"> Ghana </option>
												  <option value="Greece"> Greece </option>
												  <option value="Grenada"> Grenada </option>
												  <option value="Guatemala"> Guatemala </option>
												  <option value="Guinea"> Guinea </option>
												  <option value="Guinea-Bissau"> Guinea-Bissau </option>
												  <option value="Guyana"> Guyana </option>
												  <option value="Haiti"> Haiti </option>
												  <option value="Honduras"> Honduras </option>
												  <option value="Hungary"> Hungary </option>
												  <option value="Iceland"> Iceland </option>
												  <option value="India"> India </option>
												  <option value="Indonesia"> Indonesia </option>
												  <option value="Iran"> Iran </option>
												  <option value="Iraq"> Iraq </option>
												  <option value="Ireland"> Ireland </option>
												  <option value="Israel"> Israel </option>
												  <option value="Italy"> Italy </option>
												  <option value="Jamaica"> Jamaica </option>
												  <option value="Japan"> Japan </option>
												  <option value="Jordan"> Jordan </option>
												  <option value="Kazakhstan"> Kazakhstan </option>
												  <option value="Kenya"> Kenya </option>
												  <option value="Kiribati"> Kiribati </option>
												  <option value="Kosovo"> Kosovo </option>
												  <option value="Kuwait"> Kuwait </option>
												  <option value="Kyrgyzstan"> Kyrgyzstan </option>
												  <option value="Laos"> Laos </option>
												  <option value="Latvia"> Latvia </option>
												  <option value="Lebanon"> Lebanon </option>
												  <option value="Lesotho"> Lesotho </option>
												  <option value="Liberia"> Liberia </option>
												  <option value="Libya"> Libya </option>
												  <option value="Liechtenstein"> Liechtenstein </option>
												  <option value="Lithuania"> Lithuania </option>
												  <option value="Luxembourg"> Luxembourg </option>
												  <option value="Madagascar"> Madagascar </option>
												  <option value="Malawi"> Malawi </option>
												  <option value="Malaysia"> Malaysia </option>
												  <option value="Maldives"> Maldives </option>
												  <option value="Mali"> Mali </option>
												  <option value="Malta"> Malta </option>
												  <option value="Marshall Islands"> Marshall Islands </option>
												  <option value="Mauritania"> Mauritania </option>
												  <option value="Mauritius"> Mauritius </option>
												  <option value="Mexico"> Mexico </option>
												  <option value="Micronesia"> Micronesia </option>
												  <option value="Moldova"> Moldova </option>
												  <option value="Monaco"> Monaco </option>
												  <option value="Mongolia"> Mongolia </option>
												  <option value="Montenegro"> Montenegro </option>
												  <option value="Morocco"> Morocco </option>
												  <option value="Mozambique"> Mozambique </option>
												  <option value="Myanmar (formerly Burma)"> Myanmar (formerly Burma) </option>
												  <option value="Namibia"> Namibia </option>
												  <option value="Nauru"> Nauru </option>
												  <option value="Nepal"> Nepal </option>
												  <option value="Netherlands"> Netherlands </option>
												  <option value="New Zealand"> New Zealand </option>
												  <option value="Nicaragua"> Nicaragua </option>
												  <option value="Niger"> Niger </option>
												  <option value="Nigeria"> Nigeria </option>
												  <option value="North Korea"> North Korea </option>
												  <option value="North Macedonia (formerly Macedonia)"> North Macedonia (formerly Macedonia) </option>
												  <option value="Norway"> Norway </option>
												  <option value="Oman"> Oman </option>
												  <option value="Pakistan"> Pakistan </option>
												  <option value="Palau"> Palau </option>
												  <option value="Palestine"> Palestine </option>
												  <option value="Panama"> Panama </option>
												  <option value="Papua New Guinea"> Papua New Guinea </option>
												  <option value="Paraguay"> Paraguay </option>
												  <option value="Peru"> Peru </option>
												  <option value="Philippines"> Philippines </option>
												  <option value="Poland"> Poland </option>
												  <option value="Portugal"> Portugal </option>
												  <option value="Qatar"> Qatar </option>
												  <option value="Romania"> Romania </option>
												  <option value="Russia"> Russia </option>
												  <option value="Rwanda"> Rwanda </option>
												  <option value="Saint Kitts and Nevis"> Saint Kitts and Nevis </option>
												  <option value="Saint Lucia"> Saint Lucia </option>
												  <option value="Saint Vincent and the Grenadines"> Saint Vincent and the Grenadines </option>
												  <option value="Samoa"> Samoa </option>
												  <option value="San Marino"> San Marino </option>
												  <option value="Sao Tome and Principe"> Sao Tome and Principe </option>
												  <option value="Saudi Arabia"> Saudi Arabia </option>
												  <option value="Senegal"> Senegal </option>
												  <option value="Serbia"> Serbia </option>
												  <option value="Seychelles"> Seychelles </option>
												  <option value="Sierra Leone"> Sierra Leone </option>
												  <option value="Singapore"> Singapore </option>
												  <option value="Slovakia"> Slovakia </option>
												  <option value="Slovenia"> Slovenia </option>
												  <option value="Solomon Islands"> Solomon Islands </option>
												  <option value="Somalia"> Somalia </option>
												  <option value="South Africa"> South Africa </option>
												  <option value="South Korea"> South Korea </option>
												  <option value="South Sudan"> South Sudan </option>
												  <option value="Spain"> Spain </option>
												  <option value="Sri Lanka"> Sri Lanka </option>
												  <option value="Sudan"> Sudan </option>
												  <option value="Suriname"> Suriname </option>
												  <option value="Sweden"> Sweden </option>
												  <option value="Switzerland"> Switzerland </option>
												  <option value="Syria"> Syria </option>
												  <option value="Taiwan"> Taiwan </option>
												  <option value="Tajikistan"> Tajikistan </option>
												  <option value="Tanzania"> Tanzania </option>
												  <option value="Thailand"> Thailand </option>
												  <option value="Timor-Leste"> Timor-Leste </option>
												  <option value="Togo"> Togo </option>
												  <option value="Tonga"> Tonga </option>
												  <option value="Trinidad and Tobago"> Trinidad and Tobago </option>
												  <option value="Tunisia"> Tunisia </option>
												  <option value="Turkey"> Turkey </option>
												  <option value="Turkmenistan"> Turkmenistan </option>
												  <option value="Tuvalu"> Tuvalu </option>
												  <option value="Uganda"> Uganda </option>
												  <option value="Ukraine"> Ukraine </option>
												  <option value="United Arab Emirates (UAE)"> United Arab Emirates (UAE) </option>
												  <option value="United Kingdom (UK)"> United Kingdom (UK) </option>
												  <option value="United States of America (USA)"> United States of America (USA) </option>
												  <option value="Uruguay"> Uruguay </option>
												  <option value="Uzbekistan"> Uzbekistan </option>
												  <option value="Vanuatu"> Vanuatu </option>
												  <option value="Vatican City (Holy See)"> Vatican City (Holy See) </option>
												  <option value="Venezuela"> Venezuela </option>
												  <option value="Vietnam"> Vietnam </option>
												  <option value="Yemen"> Yemen </option>
												  <option value="Zambia"> Zambia </option>
												  <option value="Zimbabwe"> Zimbabwe </option>
											</select>											
											<label for="UG_country">Country</label>
										</div>
									</div>
                                </div>

                                <div class="row">
                                        <div class="col s6">
                                                <div class="input-field ">
                                                                <input id="UG_start_year" name="UG_start_year" type="text" class="validate datepicker">
                                                                <label for="UG_start_year">Start Year</label>
                                                        </div>
                                        </div>
                                        <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="UG_end_year" name="UG_end_year" type="text" class="validate datepicker">
                                                                        <label for="UG_end_year">Completion Year</label>

                                                                </div>
                                        </div>
                                </div>
                                <div class="row">
									<div class=" col s6 ">
										<div class="input-field">
											<select name="UG_mode_study" id="UG_mode_study">
												<option value="Full-time">Full time</option>
												<option value="Part-time">Part time</option>
												<option value="Distance-learning">Distance Learning</option>
											</select>	
											<label for="UG_mode_study">Mode of study</label>
										</div>
									</div>
									<div class=" col s6 ">
										<div class="input-field">
											<input id="UG_qualification"  name="UG_qualification" type="text" class="validate">										
											<label for="UG_qualification">Qualification eg.BA(Hons)</label>
										</div>
									</div>
                                </div>								
                                <div class="row">
                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="UG_grade" name="UG_grade" type="text" class="validate">
                                                                                <label for="sslcgrade">Percentage / Grade Obtained</label>

                                                                        </div>
                                                </div>								
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="UG_medium_of_instr" name="UG_medium_of_instr" type="text" class="validate">
                                                                        <label for="UG_language">Medium of Instruction</label>

                                                                </div>
                                                </div>
								</div>
		
								<div class="row">
                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="UG_eng_score" name="UG_eng_score" type="text" class="validate">
                                                                                <label for="UG_eng_score">Marks Obtained in English</label>

                                                                        </div>
                                                </div>


                                </div>
                                <div class="row">
                                        <div class="col s6">
                                                <label>Upload Documents</label>
                                                <div class="file-field input-field">
                                                                <div class="btn">
                                                                        <span>Upload</span>
                                                                        <input type="file" multiple > <i class="fa fa-cloud-upload"></i>
                                                                </div>
                                                                <div class="file-path-wrapper">
                                                                        <input class="file-path validate" name="UG_document" type="text" placeholder = "Upload file" >
                                                                </div>
                                                </div>
												<div class="uploaded-file" id="UG_document" data-type="file"></div>	
                                        </div>
                                </div>	
							</div>	  
							<div class="mgv-wrap">

							
								<h4 class="sub_title">Masters / PG</h4>
                                <div class="row">
									<div class=" col s6 ">
										<div class="input-field">
											<input id="PG_board_name"  name="PG_board_name" type="text" class="validate">
											<label for="PG_board_name">Name of Institution</label>
										</div>
									</div>
									<div class=" col s6 ">
										<div class="input-field">
											<select name="PG_country" id="PG_country">
												  <option value="">Select</option>
												  <option value="Afghanistan"> Afghanistan </option>
												  <option value="Albania"> Albania </option>
												  <option value="Algeria"> Algeria </option>
												  <option value="Andorra"> Andorra </option>
												  <option value="Angola"> Angola </option>
												  <option value="Antigua and Barbuda"> Antigua and Barbuda </option>
												  <option value="Argentina"> Argentina </option>
												  <option value="Armenia"> Armenia </option>
												  <option value="Australia"> Australia </option>
												  <option value="Austria"> Austria </option>
												  <option value="Azerbaijan"> Azerbaijan </option>
												  <option value="Bahamas"> Bahamas </option>
												  <option value="Bahrain"> Bahrain </option>
												  <option value="Bangladesh"> Bangladesh </option>
												  <option value="Barbados"> Barbados </option>
												  <option value="Belarus"> Belarus </option>
												  <option value="Belgium"> Belgium </option>
												  <option value="Belize"> Belize </option>
												  <option value="Benin"> Benin </option>
												  <option value="Bhutan"> Bhutan </option>
												  <option value="Bolivia"> Bolivia </option>
												  <option value="Bosnia and Herzegovina"> Bosnia and Herzegovina </option>
												  <option value="Botswana"> Botswana </option>
												  <option value="Brazil"> Brazil </option>
												  <option value="Brunei"> Brunei </option>
												  <option value="Bulgaria"> Bulgaria </option>
												  <option value="Burkina Faso"> Burkina Faso </option>
												  <option value="Burundi"> Burundi </option>
												  <option value="Cabo Verde"> Cabo Verde </option>
												  <option value="Cambodia"> Cambodia </option>
												  <option value="Cameroon"> Cameroon </option>
												  <option value="Canada"> Canada </option>
												  <option value="Central African Republic (CAR)"> Central African Republic (CAR) </option>
												  <option value="Chad"> Chad </option>
												  <option value="Chile"> Chile </option>
												  <option value="China"> China </option>
												  <option value="Colombia"> Colombia </option>
												  <option value="Comoros"> Comoros </option>
												  <option value="Congo, Democratic Republic of the"> Congo, Democratic Republic of the </option>
												  <option value="Congo, Republic of the"> Congo, Republic of the </option>
												  <option value="Costa Rica"> Costa Rica </option>
												  <option value="Cote d'Ivoire"> Cote d'Ivoire </option>
												  <option value="Croatia"> Croatia </option>
												  <option value="Cuba"> Cuba </option>
												  <option value="Cyprus"> Cyprus </option>
												  <option value="Czechia"> Czechia </option>
												  <option value="Denmark"> Denmark </option>
												  <option value="Djibouti"> Djibouti </option>
												  <option value="Dominica"> Dominica </option>
												  <option value="Dominican Republic"> Dominican Republic </option>
												  <option value="Ecuador"> Ecuador </option>
												  <option value="Egypt"> Egypt </option>
												  <option value="El Salvador"> El Salvador </option>
												  <option value="Equatorial Guinea"> Equatorial Guinea </option>
												  <option value="Eritrea"> Eritrea </option>
												  <option value="Estonia"> Estonia </option>
												  <option value="Eswatini (formerly Swaziland)"> Eswatini (formerly Swaziland) </option>
												  <option value="Ethiopia"> Ethiopia </option>
												  <option value="Fiji"> Fiji </option>
												  <option value="Finland"> Finland </option>
												  <option value="France"> France </option>
												  <option value="Gabon"> Gabon </option>
												  <option value="Gambia"> Gambia </option>
												  <option value="Georgia"> Georgia </option>
												  <option value="Germany"> Germany </option>
												  <option value="Ghana"> Ghana </option>
												  <option value="Greece"> Greece </option>
												  <option value="Grenada"> Grenada </option>
												  <option value="Guatemala"> Guatemala </option>
												  <option value="Guinea"> Guinea </option>
												  <option value="Guinea-Bissau"> Guinea-Bissau </option>
												  <option value="Guyana"> Guyana </option>
												  <option value="Haiti"> Haiti </option>
												  <option value="Honduras"> Honduras </option>
												  <option value="Hungary"> Hungary </option>
												  <option value="Iceland"> Iceland </option>
												  <option value="India"> India </option>
												  <option value="Indonesia"> Indonesia </option>
												  <option value="Iran"> Iran </option>
												  <option value="Iraq"> Iraq </option>
												  <option value="Ireland"> Ireland </option>
												  <option value="Israel"> Israel </option>
												  <option value="Italy"> Italy </option>
												  <option value="Jamaica"> Jamaica </option>
												  <option value="Japan"> Japan </option>
												  <option value="Jordan"> Jordan </option>
												  <option value="Kazakhstan"> Kazakhstan </option>
												  <option value="Kenya"> Kenya </option>
												  <option value="Kiribati"> Kiribati </option>
												  <option value="Kosovo"> Kosovo </option>
												  <option value="Kuwait"> Kuwait </option>
												  <option value="Kyrgyzstan"> Kyrgyzstan </option>
												  <option value="Laos"> Laos </option>
												  <option value="Latvia"> Latvia </option>
												  <option value="Lebanon"> Lebanon </option>
												  <option value="Lesotho"> Lesotho </option>
												  <option value="Liberia"> Liberia </option>
												  <option value="Libya"> Libya </option>
												  <option value="Liechtenstein"> Liechtenstein </option>
												  <option value="Lithuania"> Lithuania </option>
												  <option value="Luxembourg"> Luxembourg </option>
												  <option value="Madagascar"> Madagascar </option>
												  <option value="Malawi"> Malawi </option>
												  <option value="Malaysia"> Malaysia </option>
												  <option value="Maldives"> Maldives </option>
												  <option value="Mali"> Mali </option>
												  <option value="Malta"> Malta </option>
												  <option value="Marshall Islands"> Marshall Islands </option>
												  <option value="Mauritania"> Mauritania </option>
												  <option value="Mauritius"> Mauritius </option>
												  <option value="Mexico"> Mexico </option>
												  <option value="Micronesia"> Micronesia </option>
												  <option value="Moldova"> Moldova </option>
												  <option value="Monaco"> Monaco </option>
												  <option value="Mongolia"> Mongolia </option>
												  <option value="Montenegro"> Montenegro </option>
												  <option value="Morocco"> Morocco </option>
												  <option value="Mozambique"> Mozambique </option>
												  <option value="Myanmar (formerly Burma)"> Myanmar (formerly Burma) </option>
												  <option value="Namibia"> Namibia </option>
												  <option value="Nauru"> Nauru </option>
												  <option value="Nepal"> Nepal </option>
												  <option value="Netherlands"> Netherlands </option>
												  <option value="New Zealand"> New Zealand </option>
												  <option value="Nicaragua"> Nicaragua </option>
												  <option value="Niger"> Niger </option>
												  <option value="Nigeria"> Nigeria </option>
												  <option value="North Korea"> North Korea </option>
												  <option value="North Macedonia (formerly Macedonia)"> North Macedonia (formerly Macedonia) </option>
												  <option value="Norway"> Norway </option>
												  <option value="Oman"> Oman </option>
												  <option value="Pakistan"> Pakistan </option>
												  <option value="Palau"> Palau </option>
												  <option value="Palestine"> Palestine </option>
												  <option value="Panama"> Panama </option>
												  <option value="Papua New Guinea"> Papua New Guinea </option>
												  <option value="Paraguay"> Paraguay </option>
												  <option value="Peru"> Peru </option>
												  <option value="Philippines"> Philippines </option>
												  <option value="Poland"> Poland </option>
												  <option value="Portugal"> Portugal </option>
												  <option value="Qatar"> Qatar </option>
												  <option value="Romania"> Romania </option>
												  <option value="Russia"> Russia </option>
												  <option value="Rwanda"> Rwanda </option>
												  <option value="Saint Kitts and Nevis"> Saint Kitts and Nevis </option>
												  <option value="Saint Lucia"> Saint Lucia </option>
												  <option value="Saint Vincent and the Grenadines"> Saint Vincent and the Grenadines </option>
												  <option value="Samoa"> Samoa </option>
												  <option value="San Marino"> San Marino </option>
												  <option value="Sao Tome and Principe"> Sao Tome and Principe </option>
												  <option value="Saudi Arabia"> Saudi Arabia </option>
												  <option value="Senegal"> Senegal </option>
												  <option value="Serbia"> Serbia </option>
												  <option value="Seychelles"> Seychelles </option>
												  <option value="Sierra Leone"> Sierra Leone </option>
												  <option value="Singapore"> Singapore </option>
												  <option value="Slovakia"> Slovakia </option>
												  <option value="Slovenia"> Slovenia </option>
												  <option value="Solomon Islands"> Solomon Islands </option>
												  <option value="Somalia"> Somalia </option>
												  <option value="South Africa"> South Africa </option>
												  <option value="South Korea"> South Korea </option>
												  <option value="South Sudan"> South Sudan </option>
												  <option value="Spain"> Spain </option>
												  <option value="Sri Lanka"> Sri Lanka </option>
												  <option value="Sudan"> Sudan </option>
												  <option value="Suriname"> Suriname </option>
												  <option value="Sweden"> Sweden </option>
												  <option value="Switzerland"> Switzerland </option>
												  <option value="Syria"> Syria </option>
												  <option value="Taiwan"> Taiwan </option>
												  <option value="Tajikistan"> Tajikistan </option>
												  <option value="Tanzania"> Tanzania </option>
												  <option value="Thailand"> Thailand </option>
												  <option value="Timor-Leste"> Timor-Leste </option>
												  <option value="Togo"> Togo </option>
												  <option value="Tonga"> Tonga </option>
												  <option value="Trinidad and Tobago"> Trinidad and Tobago </option>
												  <option value="Tunisia"> Tunisia </option>
												  <option value="Turkey"> Turkey </option>
												  <option value="Turkmenistan"> Turkmenistan </option>
												  <option value="Tuvalu"> Tuvalu </option>
												  <option value="Uganda"> Uganda </option>
												  <option value="Ukraine"> Ukraine </option>
												  <option value="United Arab Emirates (UAE)"> United Arab Emirates (UAE) </option>
												  <option value="United Kingdom (UK)"> United Kingdom (UK) </option>
												  <option value="United States of America (USA)"> United States of America (USA) </option>
												  <option value="Uruguay"> Uruguay </option>
												  <option value="Uzbekistan"> Uzbekistan </option>
												  <option value="Vanuatu"> Vanuatu </option>
												  <option value="Vatican City (Holy See)"> Vatican City (Holy See) </option>
												  <option value="Venezuela"> Venezuela </option>
												  <option value="Vietnam"> Vietnam </option>
												  <option value="Yemen"> Yemen </option>
												  <option value="Zambia"> Zambia </option>
												  <option value="Zimbabwe"> Zimbabwe </option>
											</select>											
											<label for="PG_country">Country</label>
										</div>
									</div>
                                </div>

                                <div class="row">
                                        <div class="col s6">
                                                <div class="input-field ">
                                                                <input id="PG_start_year" name="PG_start_year" type="text" class="validate datepicker">
                                                                <label for="PG_start_year">Start Year</label>
                                                        </div>
                                        </div>
                                        <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="PG_end_year" name="PG_end_year" type="text" class="validate datepicker">
                                                                        <label for="PG_end_year">Completion Year</label>

                                                                </div>
                                        </div>
                                </div>
                                <div class="row">
									<div class=" col s6 ">
										<div class="input-field">
											<select name="PG_mode_study" id="PG_mode_study">
												<option value="Full-time">Full time</option>
												<option value="Part-time">Part time</option>
												<option value="Distance-learning">Distance Learning</option>
											</select>	
											<label for="PG_mode_study">Mode of study</label>
										</div>
									</div>
									<div class=" col s6 ">
										<div class="input-field">
											<input id="PG_qualification"  name="PG_qualification" type="text" class="validate">										
											<label for="PG_qualification">Qualification eg.BA(Hons)</label>
										</div>
									</div>
                                </div>								
                                <div class="row">
                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="PG_grade" name="PG_grade" type="text" class="validate">
                                                                                <label for="PG_sslcgrade">Percentage / Grade Obtained</label>

                                                                        </div>
                                                </div>								
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="PG_medium_of_instr" name="PG_medium_of_instr" type="text" class="validate">
                                                                        <label for="PG_language">Medium of Instruction</label>

                                                                </div>
                                                </div>
								</div>
	
								<div class="row">
                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="PG_eng_score" name="PG_eng_score" type="text" class="validate">
                                                                                <label for="PG_eng_score">Marks Obtained in English</label>

                                                                        </div>
                                                </div>


                                </div>
                                <div class="row">
                                        <div class="col s6">
                                                <label>Upload Documents</label>
                                                <div class="file-field input-field">
                                                                <div class="btn">
                                                                        <span>Upload</span>
                                                                        <input type="file" multiple > <i class="fa fa-cloud-upload"></i>
                                                                </div>
                                                                <div class="file-path-wrapper">
                                                                        <input class="file-path validate" name="PG_document" type="text" placeholder = "Upload file" >
                                                                </div>
                                                </div>
												<div class="uploaded-file" id="PG_document" data-type="file"></div>	
                                        </div>
                                </div>												  


						</div>
						</div>
                        <div class="controls">
                                        <a class="mgbtn2 back_button">Back</a>
                                        <input id="form_name" type="hidden" class="validate" name="type_of" value="school">
                                        <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                        {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
                </div>

                