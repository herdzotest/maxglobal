<div id="tab_7" class="tab-pane" >
        
<div class="content-head">
                                        
                                        <h4>Dependant details</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                        </div>
                <div class="pane">

                <div class="mgv-field nb">
                        <div class="name">
                        Do you have children ?
                        </div>
                        <div class="value" id="childrens"></div>
                 </div>
                 <div id="childrens-wrapper" class="hide">
					<div class="academic">
                        <div class="mgv-field">
                                <div class="name">
                                Name (First Name+Family Name) 
                                </div>
                                <div class="value" id="name"></div>
                        </div>
                        <div class="mgv-field">
                                <div class="name">
                                Date of birth 
                                </div>
                                <div class="value" id="birthdate"></div>
                        </div>
                        <div class="mgv-field">
                                <div class="name">
                                Relationship to you? 
                                </div>
                                <div class="value" id="relation"></div>
                        </div>
                        <div class="mgv-field">
                                <div class="name">
                                Does this person currently live with you? 
                                </div>
                                <div class="value" id="current_live"></div>
                        </div>
                        <div class="mgv-field">
                                <div class="name">
                                Is this person travelling with you to the UK? 
                                </div>
                                <div class="value" id="travel_to_uk"></div>
                        </div>
                         
                 </div>
				</div>
            </div>

</div>