<div id="tab_9" class="tab-pane " >
        
<div class="content-head">
                                                <h4>Visa Details </h4>
                                                <!--<div class="id">Nurse ID : <b>#12675</b></div>-->
                                </div>
                                <div class="pane">
									<div class="academic">
									 <div class="mgv-field">
											<div class="name">
											Have you been to the UK in the past 10 years?  
											</div>
											<div class="value" id="in_uk"></div>
									 </div>               
									 <div class="mgv-field">
											<div class="name">
											Have you been issued with a UK visa in the past 10 years? 
											</div>
											<div class="value" id="issued_uk_visa"></div>
									 </div>                 
									 <div class="mgv-field">
											<div class="name">
											Have you been to any other countries in the past 10 years?
											</div>
											<div class="value" id="other_countries"></div>
									 </div>

									<div id="imigration-wrapper">
										
									</div>
									 
									<div class="mgv-field">
											<div class="name">
											Visa Category
											</div>
											<div class="value" id="visa_category"></div>
									 </div>
									<div class="mgv-field">
											<div class="name">
											Application Form (NHS / Nursing Home)
											</div>
											<div class="value">
											<div class="uploaded-file" id="appl_form" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
											<div class="file-approve-buttons">
												<a href="#" class="btn btn-danger mx-approve-doc" data-type="appl_form" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
												<a href="#" class="btn btn-success mx-approve-doc" data-type="appl_form" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
											</div> </div> 
									</div>
									<div class="mgv-field">
											<div class="name">
											References 
											</div>
											<div class="value">
											<div class="uploaded-file" id="refer_file" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
											<div class="file-approve-buttons">
												<a href="#" class="btn btn-danger mx-approve-doc" data-type="refer_file" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
												<a href="#" class="btn btn-success mx-approve-doc" data-type="refer_file" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
											</div> </div>  
									</div>
									<div class="mgv-field">
											<div class="name">
											Personal Statement 
											</div>
											<div class="value" id="pers_status">
												<span id="pers_status"></span><br/>
												<span id="pers_stat_text"></span><br/>
												<div class="uploaded-file" id="pers_stat_file" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
												<div class="file-approve-buttons">
													<a href="#" class="btn btn-danger mx-approve-doc" data-type="" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
													<a href="#" class="btn btn-success mx-approve-doc" data-type="" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
												</div>  												
											</div>
									</div>
									<div class="mgv-field">
											<div class="name">
											Police Clearance Certificate (PCC) details 
											</div>
											<div class="value">
												<span id="pers_status"></span><br/>
												<span id="pcc_recieved_date"></span><br/>
												<div class="uploaded-file" id="pcc_file_upload" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
												<div class="file-approve-buttons">
													<a href="#" class="btn btn-danger mx-approve-doc" data-type="pcc_file_upload" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
													<a href="#" class="btn btn-success mx-approve-doc" data-type="pcc_file_upload" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
												</div>  												
											</div>
									 </div>
									<div class="mgv-field">
											<div class="name">
											Medical or TB Screening Test details 
											</div>
											<div class="value">
												<span id="tb_test_date"></span>
												<div class="uploaded-file" id="tb_file_upload" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
												<div class="file-approve-buttons">
													<a href="#" class="btn btn-danger mx-approve-doc" data-type="tb_file_upload" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
													<a href="#" class="btn btn-success mx-approve-doc" data-type="tb_file_upload" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
												</div>  												
											</div>
									 </div>  									 
									<div class="mgv-field">
											<div class="name">
											Certificate of Sponsorship details 
											</div>
											<div class="value">
												<span id="spons_status"></span><br/>
												<span id="spons_date"></span><br/>
												<div class="uploaded-file" id="spons_file_upload" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
												<div class="file-approve-buttons">
													<a href="#" class="btn btn-danger mx-approve-doc" data-type="spons_file_upload" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
													<a href="#" class="btn btn-success mx-approve-doc" data-type="spons_file_upload" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
												</div>  												
											</div>
									 </div> 			 
									<div class="mgv-field">
											<div class="name">
											Have You Applied For Visa ?
											</div>
											<div class="value" id="visa_applied">
											</div>
									 </div>
									<div class="mgv-field">
											<div class="name">
											Visa Status
											</div>
											<div class="value" id="visa_status"></div>
									 </div>									 
									<div class="mgv-field">
											<div class="name">
											Visa Appointment Date
											</div>
											<div class="value" id="visa_appt_date"></div>
									 </div> 				 			
									<div class="mgv-field">
											<div class="name">
											Visa Received Date
											</div>
											<div class="value" id="visa_reciev_date"></div>
									 </div> 
									<div class="mgv-field">
											<div class="name">
											Visa Expiry Date
											</div>
											<div class="value" id="visa_exp_date"></div>
									 </div> 
									<div class="mgv-field">
											<div class="name">
											Visa Copy File
											</div>
											<div class="value">
												<div class="uploaded-file" id="visa_file" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
												<div class="file-approve-buttons">
													<a href="#" class="btn btn-danger mx-approve-doc" data-type="visa_file" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
													<a href="#" class="btn btn-success mx-approve-doc" data-type="visa_file" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
												</div> 											
											</div>
									 </div>
									<div class="mgv-field">
											<div class="name">
											Visa Approval Letter
											</div>
											<div class="value">
												<div class="uploaded-file" id="visa_appr_file" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
												<div class="file-approve-buttons">
													<a href="#" class="btn btn-danger mx-approve-doc" data-type="visa_appr_file" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
													<a href="#" class="btn btn-success mx-approve-doc" data-type="visa_appr_file" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
												</div> 											
											</div>
									 </div>
									<div class="mgv-field">
											<div class="name">
											Flight Ticket details 
											</div>
											<div class="value">
												<span id="flight_tkt_status"></span><br/>
												<div class="uploaded-file" id="flight_ticket" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
												<div class="file-approve-buttons">
													<a href="#" class="btn btn-danger mx-approve-doc" data-type="flight_ticket" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
													<a href="#" class="btn btn-success mx-approve-doc" data-type="flight_ticket" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
												</div>  												
											</div>
									 </div>   									 
								</div>								
					</div>									
</div>

                