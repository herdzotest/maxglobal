@extends("la.layouts.app")

@section("contentheader_title", "Studentstatus")
@section("contentheader_description", "Studentstatus listing")
@section("section", "Studentstatus")
@section("sub_section", "Listing")
@section("htmlheader_title", "Studentstatus Listing")

@section("headerElems")
@la_access("Employees", "create")
	<button class="btn btn-success btn-sm pull-right addbtn" data-toggle="modal" id="addCourseModal" data-target="#AddModal"> <i class="icon-plus"></i> Add Student Status</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-hover mg-table ">
		<thead>
			<tr class="success">
				<th>Status Name</th>
				<th>Status</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
		<?php 
		if(count($studentstatus) > 0) { 
			foreach($studentstatus as $result) {
		?>
			<tr role="row" class="odd">
				<td><a href="javascript:void(0);"><?php echo $result->student_status; ?></a></td>
				<td>
					<?php 
					$status = ($result->status == '1') ? "Active" : "InActive";
					echo $status; 
					?>
				</td>
				<td>
				<?php if($result->student_status != 'Approve') { ?>
					<a href="#" class="btn btn-warning btn-xs mx-studentstatus-edit" data-status="<?php echo $result->status; ?>" data-title="<?php echo $result->student_status; ?>" data-id="<?php echo $result->id; ?>"><i class="fa fa-edit"></i></a>
					<?php
					$output  = Form::open(['route' => [config('laraadmin.adminRoute') . '.studentstatus.destroy', $result->id], 'method' => 'delete', 'style'=>'display:inline','onSubmit'=>"return confirm('Are you sure you wish to delete?');"]);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
					echo $output;
					?>
				<?php } ?>
				</td>
			</tr>	
		<?php 
			}
		} else { ?>
			<td>No entry found!!</td>
		<?php 
		}
		?>			
		</tbody>
		</table>
	</div>
</div>

@endsection

@la_access("Employees", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Student Status</h4>
			</div>
			{!! Form::open(['action' => 'LA\StudentstatusController@store', 'id' => 'studentstatus-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
					<input type="hidden" value="" id="studentstatus_id" name="studentstatus_id">
					<div class="form-group">
						<label for="name">Student Status* :</label>
						<input class="form-control" placeholder="Enter student status" required id="student_status" name="student_status" type="text" value="">
					</div>
					<div class="form-group">
						<label for="status">Status :</label>
						<select name="status" id="studentstatus_status" class="form-control">
							<option value="1">Active</option>
							<option value="0">InActive</option>
						</select>
					</div>					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success">Submit</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@push('scripts')
<script>
$(function () {
	$("#studentstatus-add-form").validate({
	});
	$('body').off('click', '.mx-studentstatus-edit');
	$('body').on('click', '.mx-studentstatus-edit', function() {
		
		var studentstatus_id = $(this).attr('data-id');
		var title = $(this).attr('data-title');
		var status = $(this).attr('data-status');
		$("#studentstatus_id").val(studentstatus_id);
		$("#student_status").val(title);
		$("#studentstatus_status").val(status)
		$('#AddModal').modal('show');
	});
	$("#AddModal").on('hide.bs.modal', function(){
		$('#AddModal input,#AddModal textarea').val('');
		$('#AddModal select').val(1);
	});
});
</script>
@endpush
