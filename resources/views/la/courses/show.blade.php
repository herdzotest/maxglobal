@extends('la.layouts.app')

@section('htmlheader_title')
	Course View
@endsection


@section('main-content')
<div id="page-content" class="profile2">
	<div class="bg-primary clearfix">
		<div class="col-md-7">
			<div class="row">
				<div class="col-md-2">
					<?php if($courseDetails[0]->university_logo != '') { ?>
						<img class="profile-image" src="<?php echo $courseDetails[0]->university_logo; ?>" alt="">
					<?php } else {  ?>
						<!-- <img class="profile-image" src="{{ asset('la-assets/img/course_icon.png') }}" alt=""> -->
						<img class="profile-image" src="{{ asset('la-assets/coursefinder/img/coursesCards/no-image.png') }}" alt="">
					<?php } ?>
				</div>
				<div class="col-md-10 prname">
					<h4 class="name">{{ $courseDetails[0]->course_name }}</h4>
					<div class="name">{{ $courseDetails[0]->course_type }}</div>
				</div>
			</div>
		</div>
		
		
		<div class="col-md-4"></div>
		
		<div class="col-md-1 actions">
				{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.courses.destroy', $courseDetails[0]->id], 'method' => 'delete', 'style'=>'display:inline','onSubmit'=>"return confirm('Are you sure you wish to delete?');"]) }}
					<button class="btn btn-default btn-delete btn-xs" type="submit"><i class="fa fa-times"></i></button>
				{{ Form::close() }}
		</div>

		<div class="row">
			<div class="col-md-12 prdts-prof vhbottom"><br>
				<div class="dats1"><i class="fa fa-university mr5"></i> {{ $courseDetails[0]->university }} <span>|</span><i class="fa fa-map-marker mr5"></i> {{ $courseDetails[0]->country }} </div>
			</div>
		</div>
		
	</div>

	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		<li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> General Info</a></li>
		<li class=""><a role="tab" data-toggle="tab" href="#tab-account-settings" data-target="#tab-account-settings"><i class="fa fa-bars"></i> Test Requirements</a></li>
		<li class=""><a role="tab" data-toggle="tab" href="#tab-account-settime" data-target="#tab-account-settime"><i class="fa fa-bars"></i> Entry Requirements</a></li>
		
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>General Info</h4>
					</div><br>
					<div class="form-group"><label for="name" class="col-md-2">Course :</label><div class="col-md-10 fvalue">{{ $courseDetails[0]->course_name }}</div></div>
					<div class="form-group"><label for="name" class="col-md-2">Course Description :</label><div class="col-md-10 fvalue">{{ $courseDetails[0]->course_description }}</div></div>
					<div class="form-group"><label for="name" class="col-md-2">Course Type :</label><div class="col-md-10 fvalue">{{ $courseDetails[0]->course_type }}</div></div>
					<div class="form-group"><label for="name" class="col-md-2">University :</label><div class="col-md-10 fvalue">{{ $courseDetails[0]->university }}</div></div>
					<div class="form-group"><label for="name" class="col-md-2">Country :</label><div class="col-md-10 fvalue">{{ $courseDetails[0]->country }}</div></div>
					<div class="form-group"><label for="name" class="col-md-2">Intake :</label><div class="col-md-10 fvalue">{{ $courseDetails[0]->intake }}</div></div>
					<div class="form-group"><label for="name" class="col-md-2">Duration :</label><div class="col-md-10 fvalue">{{ $courseDetails[0]->duration }}</div></div>
					<div class="form-group"><label for="name" class="col-md-2">Tuition Fees :</label><div class="col-md-10 fvalue">&#8377; {{ $courseDetails[0]->tuition_fees }}</div></div>
					<div class="form-group"><label for="name" class="col-md-2">Application Fee :</label><div class="col-md-10 fvalue">&#8377; {{ $courseDetails[0]->application_fee }}</div></div>
					<?php
					$application_deadline = date("d M Y", strtotime($courseDetails[0]->application_deadline));
					?>
					<div class="form-group"><label for="name" class="col-md-2">Application Deadline :</label><div class="col-md-10 fvalue">{{ $application_deadline }}</div></div>
					<div class="form-group"><label for="name" class="col-md-2">Remarks :</label><div class="col-md-10 fvalue">{{ $courseDetails[0]->remarks }}</div></div>
				</div>
			</div>
		</div>
		
		<div role="tabpanel" class="tab-pane fade" id="tab-account-settings">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>Test Requirements</h4>
					</div><br>
				<div class="form-group"><div class="col-md-10 fvalue">{{ $courseDetails[0]->test_requirements }}</div></div>
				</div>
			</div>
		</div>

		<div role="tabpanel" class="tab-pane fade" id="tab-account-settime">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>Entry Requirements</h4>
					</div><br>
				<div class="form-group"><div class="col-md-10 fvalue">{{ $courseDetails[0]->entry_requirements }}</div></div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>
	<meta name="csrf-token" content="{{ csrf_token() }}" />
</div>
@endsection

@push('scripts')
<script>

</script>
@endpush

