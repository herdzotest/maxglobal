<?php

namespace App\Http\Controllers\Coursefinder;

use App\User;
use App\Models\Employee;
use App\Role;
use Validator;
use Eloquent;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Dwij\Laraadmin\Models\Module;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;

use \Crypt;

class CoursefinderController extends Controller
{

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        
    }
   
    public function coursefinder()
    {
        $courses = DB::table('manage_courses AS c')
        ->select('c.*')->addselect('u.university AS university_name')->addselect('ct.country AS country_name')
        ->leftJoin('universities AS u', 'c.university', '=', 'u.id')
        ->leftJoin('countries AS ct', 'c.country', '=', 'ct.id')
        ->orderBy('c.course_name')->where('c.status',1)->limit(20)->get();
        $countries = DB::table('countries')->select('country')->addSelect('id')->orderBy('country')->where('status', 1)->get();
        $university = DB::table('universities')->select('university')->addSelect('id')->orderBy('university')->where('status', 1)->get();
        return View('auth.coursefinder.index', [
            'courses' => $courses,
            'countries' => $countries,
            'university' => $university
        ]);
    }

    public function coursefinder2()
    {
        $courses = DB::table('manage_courses AS c')
        ->select('c.*')->addselect('u.university AS university_name')->addselect('ct.country AS country_name')
        ->leftJoin('universities AS u', 'c.university', '=', 'u.id')
        ->leftJoin('countries AS ct', 'c.country', '=', 'ct.id')
        ->orderBy('c.course_name')->where('c.status',1)->limit(20)->get();
        $countries = DB::table('countries')->select('country')->addSelect('id')->orderBy('country')->where('status', 1)->get();
        $university = DB::table('universities')->select('university')->addSelect('id')->orderBy('university')->where('status', 1)->get();
        return View('auth.coursefinder.coursefinder2', [
            'courses' => $courses,
            'countries' => $countries,
            'university' => $university
        ]);
    }

    public function coursefinder3()
    {
        $courses = DB::table('manage_courses AS c')
        ->select('c.*')->addselect('u.university AS university_name')->addselect('ct.country AS country_name')
        ->leftJoin('universities AS u', 'c.university', '=', 'u.id')
        ->leftJoin('countries AS ct', 'c.country', '=', 'ct.id')
        ->orderBy('c.course_name')->where('c.status',1)->limit(20)->get();
        $countries = DB::table('countries')->select('country')->addSelect('id')->orderBy('country')->where('status', 1)->get();
        $university = DB::table('universities')->select('university')->addSelect('id')->orderBy('university')->where('status', 1)->get();
        return View('auth.coursefinder.coursefinder3', [
            'courses' => $courses,
            'countries' => $countries,
            'university' => $university
        ]);
    }

    public function coursefinder4()
    {
        $courses = DB::table('manage_courses AS c')
        ->select('c.*')->addselect('u.university AS university_name')->addselect('ct.country AS country_name')
        ->leftJoin('universities AS u', 'c.university', '=', 'u.id')
        ->leftJoin('countries AS ct', 'c.country', '=', 'ct.id')
        ->orderBy('c.course_name')->where('c.status',1)->limit(20)->get();
        $countries = DB::table('countries')->select('country')->addSelect('id')->orderBy('country')->where('status', 1)->get();
        $university = DB::table('universities')->select('university')->addSelect('id')->orderBy('university')->where('status', 1)->get();
        return View('auth.coursefinder.coursefinder4', [
            'courses' => $courses,
            'countries' => $countries,
            'university' => $university
        ]);
    }

    public function coursefinder5()
    {
        $courses = DB::table('manage_courses AS c')
        ->select('c.*')->addselect('u.university AS university_name')->addselect('ct.country AS country_name')
        ->leftJoin('universities AS u', 'c.university', '=', 'u.id')
        ->leftJoin('countries AS ct', 'c.country', '=', 'ct.id')
        ->orderBy('c.course_name')->where('c.status',1)->limit(20)->get();
        $countries = DB::table('countries')->select('country')->addSelect('id')->orderBy('country')->where('status', 1)->get();
        $university = DB::table('universities')->select('university')->addSelect('id')->orderBy('university')->where('status', 1)->get();
        return View('auth.coursefinder.coursefinder5', [
            'courses' => $courses,
            'countries' => $countries,
            'university' => $university
        ]);
    }

    public function coursefinder6()
    {
        $courses = DB::table('manage_courses AS c')
        ->select('c.*')->addselect('u.university AS university_name')->addselect('ct.country AS country_name')
        ->leftJoin('universities AS u', 'c.university', '=', 'u.id')
        ->leftJoin('countries AS ct', 'c.country', '=', 'ct.id')
        ->orderBy('c.course_name')->where('c.status',1)->limit(20)->get();
        $countries = DB::table('countries')->select('country')->addSelect('id')->orderBy('country')->where('status', 1)->get();
        $university = DB::table('universities')->select('university')->addSelect('id')->orderBy('university')->where('status', 1)->get();
        return View('auth.coursefinder.coursefinder6', [
            'courses' => $courses,
            'countries' => $countries,
            'university' => $university
        ]);
    }

    public function coursefinder7()
    {
        $courses = DB::table('manage_courses AS c')
        ->select('c.*')->addselect('u.university AS university_name')->addselect('ct.country AS country_name')
        ->leftJoin('universities AS u', 'c.university', '=', 'u.id')
        ->leftJoin('countries AS ct', 'c.country', '=', 'ct.id')
        ->orderBy('c.course_name')->where('c.status',1)->limit(20)->get();
        $countries = DB::table('countries')->select('country')->addSelect('id')->orderBy('country')->where('status', 1)->get();
        $university = DB::table('universities')->select('university')->addSelect('id')->orderBy('university')->where('status', 1)->get();
        return View('auth.coursefinder.coursefinder7', [
            'courses' => $courses,
            'countries' => $countries,
            'university' => $university
        ]);
    }

    public function coursefinder8()
    {
        $courses = DB::table('manage_courses AS c')
        ->select('c.*')->addselect('u.university AS university_name')->addselect('ct.country AS country_name')
        ->leftJoin('universities AS u', 'c.university', '=', 'u.id')
        ->leftJoin('countries AS ct', 'c.country', '=', 'ct.id')
        ->orderBy('c.course_name')->where('c.status',1)->limit(20)->get();
        $countries = DB::table('countries')->select('country')->addSelect('id')->orderBy('country')->where('status', 1)->get();
        $university = DB::table('universities')->select('university')->addSelect('id')->orderBy('university')->where('status', 1)->get();
        return View('auth.coursefinder.coursefinder8', [
            'courses' => $courses,
            'countries' => $countries,
            'university' => $university
        ]);
    }

    public function coursefinder9()
    {
        $courses = DB::table('manage_courses AS c')
        ->select('c.*')->addselect('u.university AS university_name')->addselect('ct.country AS country_name')
        ->leftJoin('universities AS u', 'c.university', '=', 'u.id')
        ->leftJoin('countries AS ct', 'c.country', '=', 'ct.id')
        ->orderBy('c.course_name')->where('c.status',1)->limit(20)->get();
        $countries = DB::table('countries')->select('country')->addSelect('id')->orderBy('country')->where('status', 1)->get();
        $university = DB::table('universities')->select('university')->addSelect('id')->orderBy('university')->where('status', 1)->get();
        return View('auth.coursefinder.coursefinder9', [
            'courses' => $courses,
            'countries' => $countries,
            'university' => $university
        ]);
    }

    public function coursefinder10()
    {
        $courses = DB::table('manage_courses AS c')
        ->select('c.*')->addselect('u.university AS university_name')->addselect('ct.country AS country_name')
        ->leftJoin('universities AS u', 'c.university', '=', 'u.id')
        ->leftJoin('countries AS ct', 'c.country', '=', 'ct.id')
        ->orderBy('c.course_name')->where('c.status',1)->limit(20)->get();
        $countries = DB::table('countries')->select('country')->addSelect('id')->orderBy('country')->where('status', 1)->get();
        $university = DB::table('universities')->select('university')->addSelect('id')->orderBy('university')->where('status', 1)->get();
        return View('auth.coursefinder.coursefinder10', [
            'courses' => $courses,
            'countries' => $countries,
            'university' => $university
        ]);
    }

    public function aboutus()
    {
        return View('auth.coursefinder.aboutus');
    }

    public function blog()
    {
        return View('auth.coursefinder.blog');
    }

    public function blogdetails()
    {
        return View('auth.coursefinder.blogdetails');
    }
    public function blogdetails1()
    {
        return View('auth.coursefinder.blogdetails1');
    }

    public function blogdetails2()
    {
        return View('auth.coursefinder.blogdetails2');
    }

    public function blogdetails3()
    {
        return View('auth.coursefinder.blogdetails3');
    }

    public function blogdetails4()
    {
        return View('auth.coursefinder.blogdetails4');
    }

    public function contactus()
    {
        return View('auth.coursefinder.contactus');
    }

    public function topcourses()
    {   
        $query = "SELECT c.*,u.university AS university_name,ct.country AS country_name FROM manage_courses AS c 
            LEFT JOIN universities AS u ON c.university = u.id 
            LEFT JOIN countries AS ct ON c.country = ct.id
            WHERE c.status = 1
            ORDER BY c.course_name ASC LIMIT 9";
        $courses = DB::select($query);
        
        return View('auth.coursefinder.topcourses', [
            'courses' => $courses
        ]);

        //return View('auth.coursefinder.topcourses');
    }

    public function coursesearch()
    {
        if(isset($_GET['course_country'])){ $course_country = $_GET['course_country']; } else {$course_country = ''; }
        if(isset($_GET['course_university'])){ $course_university = $_GET['course_university']; } else {$course_university = ''; }
        if(isset($_GET['course_intake'])){ $course_intake = $_GET['course_intake']; } else {$course_intake = ''; }
        if(isset($_GET['course_name'])){ $course_name = $_GET['course_name']; } else {$course_name = ''; }
        if(isset($_GET['course_price'])){ $course_price = $_GET['course_price']; } else {$course_price = ''; }
        $conditions = "";
        if ($course_country != '') { 
            $conditions .= " AND (ct.country = '$course_country') "; 
        }
        if ($course_university != '') { 
            $conditions .= " AND (u.university = '$course_university') "; 
        }
        if ($course_intake != '') { 
            $conditions .= " AND (c.intake = '$course_intake') ";
        }
        if ($course_name != '') { 
            $conditions .= " AND (c.course_name = '$course_name') ";
        }
        if ($course_price != '') { 
            if($course_price == '10,000 - 30,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 10000 AND 30000) ";
            } else if($course_price == '31,000 - 60,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 31000 AND 60000) ";
            } else if($course_price == '61,000 - 90,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 61000 AND 90000) ";
            } else if($course_price == '91,000 and Above') {
                $conditions .= " AND (c.tuition_fees >= 91000) ";
            }
        }
        $query = "SELECT c.*,u.university AS university_name,ct.country AS country_name FROM manage_courses AS c 
            LEFT JOIN universities AS u ON c.university = u.id 
            LEFT JOIN countries AS ct ON c.country = ct.id
            WHERE c.status = 1
            $conditions 
            ORDER BY c.course_name ASC";
        $courses = DB::select($query);
        $count = count($courses);
        $countries = DB::table('countries')->select('country')->addSelect('id')->orderBy('country')->where('status', 1)->get();
        $university = DB::table('universities')->select('university')->addSelect('id')->orderBy('university')->where('status', 1)->get();
        $coursess = DB::table('manage_courses')->select('course_name')->addSelect('id')->orderBy('course_name')->where('status', 1)->get();
        return View('auth.coursefinder.courses', [
            'courses' => $courses,
            'countries' => $countries,
            'university' => $university,
            'course_country' => $course_country,
            'course_university' => $course_university,
            'course_intake' => $course_intake,
            'course_name' => $course_name,
            'course_price' => $course_price,
            'coursess' => $coursess,
            'count' => $count
        ]);
    }

    public function coursesearch2()
    {   
        if(isset($_GET['course_country'])){ $course_country = $_GET['course_country']; } else {$course_country = ''; }
        if(isset($_GET['course_university'])){ $course_university = $_GET['course_university']; } else {$course_university = ''; }
        if(isset($_GET['course_intake'])){ $course_intake = $_GET['course_intake']; } else {$course_intake = ''; }
        if(isset($_GET['course_name'])){ $course_name = $_GET['course_name']; } else {$course_name = ''; }
        if(isset($_GET['course_price'])){ $course_price = $_GET['course_price']; } else {$course_price = ''; }
        $conditions = "";
        if ($course_country != '') { 
            $conditions .= " AND (ct.country = '$course_country') "; 
        }
        if ($course_university != '') { 
            $conditions .= " AND (u.university = '$course_university') "; 
        }
        if ($course_intake != '') { 
            $conditions .= " AND (c.intake = '$course_intake') ";
        }
        if ($course_name != '') { 
            $conditions .= " AND (c.course_name = '$course_name') ";
        }
        if ($course_price != '') { 
            if($course_price == '10,000 - 30,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 10000 AND 30000) ";
            } else if($course_price == '31,000 - 60,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 31000 AND 60000) ";
            } else if($course_price == '61,000 - 90,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 61000 AND 90000) ";
            } else if($course_price == '91,000 and Above') {
                $conditions .= " AND (c.tuition_fees >= 91000) ";
            }
        }
        $query = "SELECT c.*,u.university AS university_name,ct.country AS country_name FROM manage_courses AS c 
            LEFT JOIN universities AS u ON c.university = u.id 
            LEFT JOIN countries AS ct ON c.country = ct.id
            WHERE c.status = 1
            $conditions 
            ORDER BY c.course_name ASC";
        $courses = DB::select($query);
        $count = count($courses);
        $countries = DB::table('countries')->select('country')->addSelect('id')->orderBy('country')->where('status', 1)->get();
        $university = DB::table('universities')->select('university')->addSelect('id')->orderBy('university')->where('status', 1)->get();
        $coursess = DB::table('manage_courses')->select('course_name')->addSelect('id')->orderBy('course_name')->where('status', 1)->get();
        return View('auth.coursefinder.courses2', [
            'courses' => $courses,
            'countries' => $countries,
            'university' => $university,
            'course_country' => $course_country,
            'course_university' => $course_university,
            'course_intake' => $course_intake,
            'course_name' => $course_name,
            'course_price' => $course_price,
            'coursess' => $coursess,
            'count' => $count
        ]);
    }

    public function coursesearch3()
    {   
        if(isset($_GET['course_country'])){ $course_country = $_GET['course_country']; } else {$course_country = ''; }
        if(isset($_GET['course_university'])){ $course_university = $_GET['course_university']; } else {$course_university = ''; }
        if(isset($_GET['course_intake'])){ $course_intake = $_GET['course_intake']; } else {$course_intake = ''; }
        if(isset($_GET['course_name'])){ $course_name = $_GET['course_name']; } else {$course_name = ''; }
        if(isset($_GET['course_price'])){ $course_price = $_GET['course_price']; } else {$course_price = ''; }
        $conditions = "";
        if ($course_country != '') { 
            $conditions .= " AND (ct.country = '$course_country') "; 
        }
        if ($course_university != '') { 
            $conditions .= " AND (u.university = '$course_university') "; 
        }
        if ($course_intake != '') { 
            $conditions .= " AND (c.intake = '$course_intake') ";
        }
        if ($course_name != '') { 
            $conditions .= " AND (c.course_name = '$course_name') ";
        }
        if ($course_price != '') { 
            if($course_price == '10,000 - 30,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 10000 AND 30000) ";
            } else if($course_price == '31,000 - 60,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 31000 AND 60000) ";
            } else if($course_price == '61,000 - 90,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 61000 AND 90000) ";
            } else if($course_price == '91,000 and Above') {
                $conditions .= " AND (c.tuition_fees >= 91000) ";
            }
        }
        $query = "SELECT c.*,u.university AS university_name,ct.country AS country_name FROM manage_courses AS c 
            LEFT JOIN universities AS u ON c.university = u.id 
            LEFT JOIN countries AS ct ON c.country = ct.id
            WHERE c.status = 1
            $conditions 
            ORDER BY c.course_name ASC";
        $courses = DB::select($query);
        $count = count($courses);
        $countries = DB::table('countries')->select('country')->addSelect('id')->orderBy('country')->where('status', 1)->get();
        $university = DB::table('universities')->select('university')->addSelect('id')->orderBy('university')->where('status', 1)->get();
        $coursess = DB::table('manage_courses')->select('course_name')->addSelect('id')->orderBy('course_name')->where('status', 1)->get();
        return View('auth.coursefinder.courses3', [
            'courses' => $courses,
            'countries' => $countries,
            'university' => $university,
            'course_country' => $course_country,
            'course_university' => $course_university,
            'course_intake' => $course_intake,
            'course_name' => $course_name,
            'course_price' => $course_price,
            'coursess' => $coursess,
            'count' => $count
        ]);
    }

    public function coursesearch4()
    {   
        if(isset($_GET['course_country'])){ $course_country = $_GET['course_country']; } else {$course_country = ''; }
        if(isset($_GET['course_university'])){ $course_university = $_GET['course_university']; } else {$course_university = ''; }
        if(isset($_GET['course_intake'])){ $course_intake = $_GET['course_intake']; } else {$course_intake = ''; }
        if(isset($_GET['course_name'])){ $course_name = $_GET['course_name']; } else {$course_name = ''; }
        if(isset($_GET['course_price'])){ $course_price = $_GET['course_price']; } else {$course_price = ''; }
        $conditions = "";
        if ($course_country != '') { 
            $conditions .= " AND (ct.country = '$course_country') "; 
        }
        if ($course_university != '') { 
            $conditions .= " AND (u.university = '$course_university') "; 
        }
        if ($course_intake != '') { 
            $conditions .= " AND (c.intake = '$course_intake') ";
        }
        if ($course_name != '') { 
            $conditions .= " AND (c.course_name = '$course_name') ";
        }
        if ($course_price != '') { 
            if($course_price == '10,000 - 30,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 10000 AND 30000) ";
            } else if($course_price == '31,000 - 60,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 31000 AND 60000) ";
            } else if($course_price == '61,000 - 90,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 61000 AND 90000) ";
            } else if($course_price == '91,000 and Above') {
                $conditions .= " AND (c.tuition_fees >= 91000) ";
            }
        }
        $query = "SELECT c.*,u.university AS university_name,ct.country AS country_name FROM manage_courses AS c 
            LEFT JOIN universities AS u ON c.university = u.id 
            LEFT JOIN countries AS ct ON c.country = ct.id
            WHERE c.status = 1
            $conditions 
            ORDER BY c.course_name ASC";
        $courses = DB::select($query);
        $count = count($courses);
        $countries = DB::table('countries')->select('country')->addSelect('id')->orderBy('country')->where('status', 1)->get();
        $university = DB::table('universities')->select('university')->addSelect('id')->orderBy('university')->where('status', 1)->get();
        $coursess = DB::table('manage_courses')->select('course_name')->addSelect('id')->orderBy('course_name')->where('status', 1)->get();
        return View('auth.coursefinder.courses4', [
            'courses' => $courses,
            'countries' => $countries,
            'university' => $university,
            'course_country' => $course_country,
            'course_university' => $course_university,
            'course_intake' => $course_intake,
            'course_name' => $course_name,
            'course_price' => $course_price,
            'coursess' => $coursess,
            'count' => $count
        ]);
    }

    public function coursesearch5()
    {   
        if(isset($_GET['course_country'])){ $course_country = $_GET['course_country']; } else {$course_country = ''; }
        if(isset($_GET['course_university'])){ $course_university = $_GET['course_university']; } else {$course_university = ''; }
        if(isset($_GET['course_intake'])){ $course_intake = $_GET['course_intake']; } else {$course_intake = ''; }
        if(isset($_GET['course_name'])){ $course_name = $_GET['course_name']; } else {$course_name = ''; }
        if(isset($_GET['course_price'])){ $course_price = $_GET['course_price']; } else {$course_price = ''; }
        $conditions = "";
        if ($course_country != '') { 
            $conditions .= " AND (ct.country = '$course_country') "; 
        }
        if ($course_university != '') { 
            $conditions .= " AND (u.university = '$course_university') "; 
        }
        if ($course_intake != '') { 
            $conditions .= " AND (c.intake = '$course_intake') ";
        }
        if ($course_name != '') { 
            $conditions .= " AND (c.course_name = '$course_name') ";
        }
        if ($course_price != '') { 
            if($course_price == '10,000 - 30,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 10000 AND 30000) ";
            } else if($course_price == '31,000 - 60,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 31000 AND 60000) ";
            } else if($course_price == '61,000 - 90,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 61000 AND 90000) ";
            } else if($course_price == '91,000 and Above') {
                $conditions .= " AND (c.tuition_fees >= 91000) ";
            }
        }
        $query = "SELECT c.*,u.university AS university_name,ct.country AS country_name FROM manage_courses AS c 
            LEFT JOIN universities AS u ON c.university = u.id 
            LEFT JOIN countries AS ct ON c.country = ct.id
            WHERE c.status = 1
            $conditions 
            ORDER BY c.course_name ASC";
        $courses = DB::select($query);
        $count = count($courses);
        $countries = DB::table('countries')->select('country')->addSelect('id')->orderBy('country')->where('status', 1)->get();
        $university = DB::table('universities')->select('university')->addSelect('id')->orderBy('university')->where('status', 1)->get();
        $coursess = DB::table('manage_courses')->select('course_name')->addSelect('id')->orderBy('course_name')->where('status', 1)->get();
        return View('auth.coursefinder.courses5', [
            'courses' => $courses,
            'countries' => $countries,
            'university' => $university,
            'course_country' => $course_country,
            'course_university' => $course_university,
            'course_intake' => $course_intake,
            'course_name' => $course_name,
            'course_price' => $course_price,
            'coursess' => $coursess,
            'count' => $count
        ]);
    }

    public function coursesearch6()
    {   
        if(isset($_GET['course_country'])){ $course_country = $_GET['course_country']; } else {$course_country = ''; }
        if(isset($_GET['course_university'])){ $course_university = $_GET['course_university']; } else {$course_university = ''; }
        if(isset($_GET['course_intake'])){ $course_intake = $_GET['course_intake']; } else {$course_intake = ''; }
        if(isset($_GET['course_name'])){ $course_name = $_GET['course_name']; } else {$course_name = ''; }
        if(isset($_GET['course_price'])){ $course_price = $_GET['course_price']; } else {$course_price = ''; }
        $conditions = "";
        if ($course_country != '') { 
            $conditions .= " AND (ct.country = '$course_country') "; 
        }
        if ($course_university != '') { 
            $conditions .= " AND (u.university = '$course_university') "; 
        }
        if ($course_intake != '') { 
            $conditions .= " AND (c.intake = '$course_intake') ";
        }
        if ($course_name != '') { 
            $conditions .= " AND (c.course_name = '$course_name') ";
        }
        if ($course_price != '') { 
            if($course_price == '10,000 - 30,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 10000 AND 30000) ";
            } else if($course_price == '31,000 - 60,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 31000 AND 60000) ";
            } else if($course_price == '61,000 - 90,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 61000 AND 90000) ";
            } else if($course_price == '91,000 and Above') {
                $conditions .= " AND (c.tuition_fees >= 91000) ";
            }
        }
        $query = "SELECT c.*,u.university AS university_name,ct.country AS country_name FROM manage_courses AS c 
            LEFT JOIN universities AS u ON c.university = u.id 
            LEFT JOIN countries AS ct ON c.country = ct.id
            WHERE c.status = 1
            $conditions 
            ORDER BY c.course_name ASC";
        $courses = DB::select($query);
        $count = count($courses);
        $countries = DB::table('countries')->select('country')->addSelect('id')->orderBy('country')->where('status', 1)->get();
        $university = DB::table('universities')->select('university')->addSelect('id')->orderBy('university')->where('status', 1)->get();
        $coursess = DB::table('manage_courses')->select('course_name')->addSelect('id')->orderBy('course_name')->where('status', 1)->get();
        return View('auth.coursefinder.courses6', [
            'courses' => $courses,
            'countries' => $countries,
            'university' => $university,
            'course_country' => $course_country,
            'course_university' => $course_university,
            'course_intake' => $course_intake,
            'course_name' => $course_name,
            'course_price' => $course_price,
            'coursess' => $coursess,
            'count' => $count
        ]);
    }

    public function coursedetails($id)
    {   
        $id = Crypt::decrypt($id);

        $courseDetails = DB::table('manage_courses')->select('manage_courses.*')
            ->addSelect('unv.id AS university_id')->addSelect('unv.university')->addSelect('cnt.country')
            ->leftJoin('universities AS unv', 'unv.id', '=', 'manage_courses.university')
            ->leftJoin('countries AS cnt', 'cnt.id', '=', 'manage_courses.country')
            ->where('manage_courses.id','=',$id)->get();
        $unv_id = $courseDetails[0]->university_id;
        $courseunvDetails = DB::table('manage_courses')->select('manage_courses.*')
            ->addSelect('unv.id AS university_id')->addSelect('unv.university')->addSelect('cnt.country')
            ->leftJoin('universities AS unv', 'unv.id', '=', 'manage_courses.university')
            ->leftJoin('countries AS cnt', 'cnt.id', '=', 'manage_courses.country')
            ->orderBy('manage_courses.course_name')->where('manage_courses.university','=',$unv_id)->where('manage_courses.status',1)->get();
        return View('auth.coursefinder.course_details', [
            'courses' => $courseDetails,
            'unvcourses' => $courseunvDetails
        ]);
    }

    public function coursedetails2($id)
    {   
        $id = Crypt::decrypt($id);

        $courseDetails = DB::table('manage_courses')->select('manage_courses.*')
            ->addSelect('unv.id AS university_id')->addSelect('unv.university')->addSelect('cnt.country')
            ->leftJoin('universities AS unv', 'unv.id', '=', 'manage_courses.university')
            ->leftJoin('countries AS cnt', 'cnt.id', '=', 'manage_courses.country')
            ->where('manage_courses.id','=',$id)->get();
        $unv_id = $courseDetails[0]->university_id;
        $courseunvDetails = DB::table('manage_courses')->select('manage_courses.*')
            ->addSelect('unv.id AS university_id')->addSelect('unv.university')->addSelect('cnt.country')
            ->leftJoin('universities AS unv', 'unv.id', '=', 'manage_courses.university')
            ->leftJoin('countries AS cnt', 'cnt.id', '=', 'manage_courses.country')
            ->orderBy('manage_courses.course_name')->where('manage_courses.university','=',$unv_id)->where('manage_courses.status',1)->get();
        return View('auth.coursefinder.course_details2', [
            'courses' => $courseDetails,
            'unvcourses' => $courseunvDetails
        ]);
    }

    public function coursedetails3($id)
    {   
        $id = Crypt::decrypt($id);

        $courseDetails = DB::table('manage_courses')->select('manage_courses.*')
            ->addSelect('unv.id AS university_id')->addSelect('unv.university')->addSelect('cnt.country')
            ->leftJoin('universities AS unv', 'unv.id', '=', 'manage_courses.university')
            ->leftJoin('countries AS cnt', 'cnt.id', '=', 'manage_courses.country')
            ->where('manage_courses.id','=',$id)->get();
        $unv_id = $courseDetails[0]->university_id;
        $courseunvDetails = DB::table('manage_courses')->select('manage_courses.*')
            ->addSelect('unv.id AS university_id')->addSelect('unv.university')->addSelect('cnt.country')
            ->leftJoin('universities AS unv', 'unv.id', '=', 'manage_courses.university')
            ->leftJoin('countries AS cnt', 'cnt.id', '=', 'manage_courses.country')
            ->orderBy('manage_courses.course_name')->where('manage_courses.university','=',$unv_id)->where('manage_courses.status',1)->get();
        return View('auth.coursefinder.course_details3', [
            'courses' => $courseDetails,
            'unvcourses' => $courseunvDetails
        ]);
    }

    public function coursedetails4($id)
    {   
        $id = Crypt::decrypt($id);

        $courseDetails = DB::table('manage_courses')->select('manage_courses.*')
            ->addSelect('unv.id AS university_id')->addSelect('unv.university')->addSelect('cnt.country')
            ->leftJoin('universities AS unv', 'unv.id', '=', 'manage_courses.university')
            ->leftJoin('countries AS cnt', 'cnt.id', '=', 'manage_courses.country')
            ->where('manage_courses.id','=',$id)->get();
        $unv_id = $courseDetails[0]->university_id;
        $courseunvDetails = DB::table('manage_courses')->select('manage_courses.*')
            ->addSelect('unv.id AS university_id')->addSelect('unv.university')->addSelect('cnt.country')
            ->leftJoin('universities AS unv', 'unv.id', '=', 'manage_courses.university')
            ->leftJoin('countries AS cnt', 'cnt.id', '=', 'manage_courses.country')
            ->orderBy('manage_courses.course_name')->where('manage_courses.university','=',$unv_id)->where('manage_courses.status',1)->get();
        return View('auth.coursefinder.course_details4', [
            'courses' => $courseDetails,
            'unvcourses' => $courseunvDetails
        ]);
    }

    public function coursedetails5($id)
    {   
        $id = Crypt::decrypt($id);

        $courseDetails = DB::table('manage_courses')->select('manage_courses.*')
            ->addSelect('unv.id AS university_id')->addSelect('unv.university')->addSelect('cnt.country')
            ->leftJoin('universities AS unv', 'unv.id', '=', 'manage_courses.university')
            ->leftJoin('countries AS cnt', 'cnt.id', '=', 'manage_courses.country')
            ->where('manage_courses.id','=',$id)->get();
        $unv_id = $courseDetails[0]->university_id;
        $courseunvDetails = DB::table('manage_courses')->select('manage_courses.*')
            ->addSelect('unv.id AS university_id')->addSelect('unv.university')->addSelect('cnt.country')
            ->leftJoin('universities AS unv', 'unv.id', '=', 'manage_courses.university')
            ->leftJoin('countries AS cnt', 'cnt.id', '=', 'manage_courses.country')
            ->orderBy('manage_courses.course_name')->where('manage_courses.university','=',$unv_id)->where('manage_courses.status',1)->get();
        return View('auth.coursefinder.course_details5', [
            'courses' => $courseDetails,
            'unvcourses' => $courseunvDetails
        ]);
    }

    public function coursedetails6($id)
    {   
        $id = Crypt::decrypt($id);

        $courseDetails = DB::table('manage_courses')->select('manage_courses.*')
            ->addSelect('unv.id AS university_id')->addSelect('unv.university')->addSelect('cnt.country')
            ->leftJoin('universities AS unv', 'unv.id', '=', 'manage_courses.university')
            ->leftJoin('countries AS cnt', 'cnt.id', '=', 'manage_courses.country')
            ->where('manage_courses.id','=',$id)->get();
        $unv_id = $courseDetails[0]->university_id;
        $courseunvDetails = DB::table('manage_courses')->select('manage_courses.*')
            ->addSelect('unv.id AS university_id')->addSelect('unv.university')->addSelect('cnt.country')
            ->leftJoin('universities AS unv', 'unv.id', '=', 'manage_courses.university')
            ->leftJoin('countries AS cnt', 'cnt.id', '=', 'manage_courses.country')
            ->orderBy('manage_courses.course_name')->where('manage_courses.university','=',$unv_id)->where('manage_courses.status',1)->get();
        return View('auth.coursefinder.course_details6', [
            'courses' => $courseDetails,
            'unvcourses' => $courseunvDetails
        ]);
    }

    public function signup()
    {
        return View('auth.coursefinder.signup');
    }

    public function userlogin()
    {
        return View('auth.coursefinder.login');
    }

    public function applynow($id)
    {   
        $id = Crypt::decrypt($id);

        $countries = DB::table('countries')->select('country')->addSelect('id')->orderBy('country')->where('status', 1)->get();
        $courseDetails = DB::table('manage_courses AS c')->select('c.*')
            ->addSelect('unv.id AS university_id')->addSelect('unv.university AS university_name')
            ->leftJoin('universities AS unv', 'unv.id', '=', 'c.university')
            ->where('c.id','=',$id)->get();
        return View('auth.coursefinder.applynow', [
            'countries' => $countries,
            'courseDetails' => $courseDetails
        ]);
    }

    public function saveapplication(Request $request)
    { 
        if(count($request->all()) > 0) {
            $postedData = $request->all();

            $rules = Module::validateRules("Employees", $request);
            $validator = Validator::make($request->all(), $rules);
            $this->emailId = $request->all()['email'];
            $validator->after(function($validator) {
                if ($this->emailValidation() > 0) {
                      $validator->errors()->add('email', 'Email already exists');
                }
            });
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }


            DB::table('applynow')->insert([
                    'name' => $postedData['person_name'],
                    'email' => $postedData['email'],
                    'mobile' => $postedData['mobile'],
                    'country' => $postedData['country'],
                    'state' => $postedData['state'],
                    'dob' => $postedData['dob'],
                    'gender' => $postedData['gender'],
                    'intake_year' => $postedData['intake_year'],
                    'intake_month' => $postedData['intake_month'],
                    'leap_year' => $postedData['leap_year'],
                    'marital_status' => $postedData['marital_status'],
                    'visa_reject' => $postedData['visa_reject'],
                    'education_level' => $postedData['education_level'],
                    'language_test' => $postedData['language_test'],
                    'created_at' => date("Y-m-d h:i:sa"),
                    'updated_at' => date("Y-m-d h:i:sa"),
                    'course' => $postedData['course_id'],
                    'university' => $postedData['university_id']
            ]);
            \Session::flash('success_message', 'Your application form has been submitted succesfully');
            //return redirect('/applynow/'.$postedData['course_id']);
            $cid = Crypt::encrypt($postedData['course_id']);
            return redirect('/applynow/'.$cid);
        }
    }

    public function emailValidation()
    {
        $email = DB::table('applynow')
        ->where('email', $this->emailId)
        ->get();
        return count($email);
    }

	public function authenticated(Request $request,$user)
    {   
        $pathvalue = $request->all()['pathvalue'];
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
			if(Auth::user()->type == 'CONSULTANTS') {

				$timeSettings = DB::table('time_tracking_settings')->first();
				if(is_array($timeSettings) && count($timeSettings) > 0) {
					$currentDate = new \DateTime(date("d-m-Y"));
					$timeTrack = DB::table('user_time_track')->where('employee_id', Auth::user()->context_id)->get();
					$datetime2 = new \DateTime(str_replace('/','-',$timeTrack[0]->track_date));
					$interval = $currentDate->diff($datetime2);
						
					if(count($timeTrack) > 0 && $interval->format('%a') == '0') {
						if(time() < strtotime($timeTrack->from_time) || time() > strtotime($timeTrack->to_time)) {
							Auth::logout();
						}
					} else if($timeSettings->status != 'ANYTIME') {
						$currentDay = date("N"); $daysArray = array(6,7);
						if( in_array($currentDay, $daysArray) || time() < strtotime($timeSettings->from_time) || time() > strtotime($timeSettings->to_time) ) {
							Auth::logout();
						}
					}
				}
                // New code
                else { 
                    // check whether the user email is verified / user account is enabled
                    $isverified = Auth::user()->verified;
                    if($isverified == '1' || Auth::user()->enable_disable == '0') {

                        $message = ($isverified == '1') ? "Your email is not confirmed yet, please check your mail.." : "Your login is blocked due to one or more reasons, please contact administrator.";
                        Auth::logout();
                        if($pathvalue=="userlogincf"){
                            return redirect('/userlogin')->with('loginfail', $message);
                        } else {
                            return redirect('/login')->with('loginfail', $message);
                        }
                    }
                }
                // End new code


			} else if(in_array(Auth::user()->type,['STUDENT','NURSE']) && Auth::user()->verified == '1') {
                $isverified = Auth::user()->verified; // new
                $message = ($isverified == '1') ? "Your email is not confirmed yet, please check your mail.." : "Your login is blocked due to one or more reasons, please contact administrator."; // new
				Auth::logout();
                if($pathvalue=="userlogincf"){ 
                    return redirect('/userlogin')->with('loginfail', $message); //new
                } else {
                    return redirect('/login')->with('loginfail', $message); //new
                }
			}


            // New code
            else { 
                // check whether the user email is verified / user account is enabled
                $isverified = Auth::user()->verified;
                if($isverified == '1' || Auth::user()->enable_disable == '0') {

                    $message = ($isverified == '1') ? "Your email is not confirmed yet, please check your mail.." : "Your login is blocked due to one or more reasons, please contact administrator.";
                    Auth::logout();
                    if($pathvalue=="userlogincf"){
                        return redirect('/userlogin')->with('loginfail', $message);
                    } else {
                       return redirect('/login')->with('loginfail', $message); 
                   }
                }
            }
            // End new code
			
            // Authentication passed...
            return redirect()->intended('/admin/dashboard');
        }
    }
	
    public function showRegistrationForm()
    {
        $roleCount = Role::count();
		if($roleCount != 0) {
			$userCount = User::count();
			if($userCount == 0) {
				return view('auth.register');
			} else {
				return redirect('login');
			}
		} else {
			return view('errors.error', [
				'title' => 'Migration not completed',
				'message' => 'Please run command <code>php artisan db:seed</code> to generate required table data.',
			]);
		}
    }
    
    public function showLoginForm()
    {
		$roleCount = Role::count();
		if($roleCount != 0) {
			$userCount = User::count();
			if($userCount == 0) {
				return redirect('register');
			} else {
				return view('auth.login');
			}
		} else {
			return view('errors.error', [
				'title' => 'Migration not completed',
				'message' => 'Please run command <code>php artisan db:seed</code> to generate required table data.',
			]);
		}
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        // TODO: This is Not Standard. Need to find alternative
        Eloquent::unguard();
        
        $employee = Employee::create([
            'name' => $data['name'],
            'designation' => "Super Admin",
            'mobile' => "8888888888",
            'mobile2' => "",
            'email' => $data['email'],
            'gender' => 'Male',
            'dept' => "1",
            'city' => "Pune",
            'address' => "Karve nagar, Pune 411030",
            'about' => "About user / biography",
            'date_birth' => date("Y-m-d"),
            'date_hire' => date("Y-m-d"),
            'date_left' => date("Y-m-d"),
            'salary_cur' => 0,
        ]);
        
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'context_id' => $employee->id,
            'type' => "Employee",
        ]);
        $role = Role::where('name', 'SUPER_ADMIN')->first();
        $user->attachRole($role);
    
        return $user;
    }

    public function applynow1($id)
    {
        $id = Crypt::decrypt($id);

        $countries = DB::table('countries')->select('country')->addSelect('id')->orderBy('country')->where('status', 1)->get();
        $courseDetails = DB::table('manage_courses AS c')->select('c.*')
            ->addSelect('unv.id AS university_id')->addSelect('unv.university AS university_name')
            ->leftJoin('universities AS unv', 'unv.id', '=', 'c.university')
            ->where('c.id','=',$id)->get();
        return View('auth.coursefinder.applynow1', [
            'countries' => $countries,
            'courseDetails' => $courseDetails
        ]);
    }

    public function saveapplications(Request $request)
    { 
        if(count($request->all()) > 0) {
            $postedData = $request->all();

            $rules = Module::validateRules("Employees", $request);
            $validator = Validator::make($request->all(), $rules);
            $this->emailId = $request->all()['email'];
            $validator->after(function($validator) {
                if ($this->emailValidation() > 0) {
                      $validator->errors()->add('email', 'Email already exists');
                }
            });
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }


            DB::table('applynow')->insert([
                    'name' => $postedData['person_name'],
                    'email' => $postedData['email'],
                    'mobile' => $postedData['mobile'],
                    'country' => $postedData['country'],
                    'state' => $postedData['state'],
                    'dob' => $postedData['dob'],
                    'gender' => $postedData['gender'],
                    'intake_year' => $postedData['intake_year'],
                    'intake_month' => $postedData['intake_month'],
                    'leap_year' => $postedData['leap_year'],
                    'marital_status' => $postedData['marital_status'],
                    'visa_reject' => $postedData['visa_reject'],
                    'education_level' => $postedData['education_level'],
                    'language_test' => $postedData['language_test'],
                    'created_at' => date("Y-m-d h:i:sa"),
                    'updated_at' => date("Y-m-d h:i:sa"),
                    'course' => $postedData['course_id'],
                    'university' => $postedData['university_id']
            ]);
            \Session::flash('success_message', 'Your application form has been submitted succesfully');
            //return redirect('/applynow1/'.$postedData['course_id']);
            $cid = Crypt::encrypt($postedData['course_id']);
            return redirect('/applynow/'.$cid);
        }
    }

    public function coursesearch1()
    {   
        if(isset($_GET['course_country'])){ $course_country = $_GET['course_country']; } else {$course_country = ''; }
        if(isset($_GET['course_university'])){ $course_university = $_GET['course_university']; } else {$course_university = ''; }
        if(isset($_GET['course_intake'])){ $course_intake = $_GET['course_intake']; } else {$course_intake = ''; }
        if(isset($_GET['course_name'])){ $course_name = $_GET['course_name']; } else {$course_name = ''; }
        if(isset($_GET['course_price'])){ $course_price = $_GET['course_price']; } else {$course_price = ''; }
        $conditions = "";
        if ($course_country != '') { 
            $conditions .= " AND (ct.country = '$course_country') "; 
        }
        if ($course_university != '') { 
            $conditions .= " AND (u.university = '$course_university') "; 
        }
        if ($course_intake != '') { 
            $conditions .= " AND (c.intake = '$course_intake') ";
        }
        if ($course_name != '') { 
            $conditions .= " AND (c.course_name = '$course_name') ";
        }
        if ($course_price != '') { 
            if($course_price == '10,000 - 30,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 10000 AND 30000) ";
            } else if($course_price == '31,000 - 60,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 31000 AND 60000) ";
            } else if($course_price == '61,000 - 90,000') {
                $conditions .= " AND (c.tuition_fees BETWEEN 61000 AND 90000) ";
            } else if($course_price == '91,000 and Above') {
                $conditions .= " AND (c.tuition_fees >= 91000) ";
            }
        }
        $query = "SELECT c.*,u.university AS university_name,ct.country AS country_name FROM manage_courses AS c 
            LEFT JOIN universities AS u ON c.university = u.id 
            LEFT JOIN countries AS ct ON c.country = ct.id
            WHERE c.status = 1
            $conditions 
            ORDER BY c.course_name ASC";
        $courses = DB::select($query);
        $count = count($courses);
        $countries = DB::table('countries')->select('country')->addSelect('id')->orderBy('country')->where('status', 1)->get();
        $university = DB::table('universities')->select('university')->addSelect('id')->orderBy('university')->where('status', 1)->get();
        $coursess = DB::table('manage_courses')->select('course_name')->addSelect('id')->orderBy('course_name')->where('status', 1)->get();
        return View('auth.coursefinder.courses1', [
            'courses' => $courses,
            'countries' => $countries,
            'university' => $university,
            'course_country' => $course_country,
            'course_university' => $course_university,
            'course_intake' => $course_intake,
            'course_name' => $course_name,
            'course_price' => $course_price,
            'coursess' => $coursess,
            'count' => $count
        ]);
    }

}
