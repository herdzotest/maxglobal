<div id="tab_10" class="tab-pane " >
        
<div class="content-head">
                                        <h4>Personal statement</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->

                                </div>
                                {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'application-add-form']) !!}
									
                                <div class="pane">
									<div class="row">
                                        <div class="input-field  col s12">
											<textarea id="personal_statement" name="personal_statement" class="materialize-textarea" maxlength="500"></textarea>
											<label for="prefix">Personal statement</label>
                                            <span class="helper-text">Please use a minimum of 250 words in English detailing your reasons for applying, explaining your choice of course at this university, what you hope to do in your future career and how the course is relevant to your plans.</span>											
                                        </div>	
									</div>
									<div class="row">
										<div class="col s6">
											<label>
												<!-- <input name="certificates" id="certificates" type="checkbox" value="1"/> -->
												<input class="checklist" id="certificates" type="checkbox"/>
												<span>Transcripts/Certificates</span>
											</label>
											<input name="certificates" class="certificates" type="hidden" value="0"/>
										</div>	
									</div>
									<div class="row">
										<div class="col s6">
											<label>
												<!-- <input name="eng_qualification" id="eng_qualification" type="checkbox" value="1"/> -->
												<input class="checklist" id="eng_qualification" type="checkbox"/>
												<span>English Language qualification</span>
											</label>
											<input name="eng_qualification" class="eng_qualification" type="hidden" value="0"/>
										</div>	
									</div>
									<div class="row">
										<div class="col s6">
											<label>
												<!-- <input name="resume" id="resume" type="checkbox"/> -->
												<input class="checklist" id="resume" type="checkbox"/>
												<span>CV/Resumé</span>
											</label>
											<input name="resume" class="resume" type="hidden" value="0"/>
										</div>	
									</div>	
									<div class="row">
										<div class="col s6">
											<label>
												<!-- <input name="passport" id="passport" type="checkbox" value="1"/> -->
												<input class="checklist" id="passport" type="checkbox"/>
												<span>Passport personal details page</span>
											</label>
											<input name="passport" class="passport" type="hidden" value="0"/>
										</div>	
									</div>	
									<div class="row">
										<div class="col s6">
											<label>
												<!-- <input name="reference" id="reference" type="checkbox" value="1"/> -->
												<input class="checklist" id="reference" type="checkbox"/>
												<span>Reference(s)</span>
											</label>
											<input name="reference" class="reference" type="hidden" value="0"/>
										</div>	
									</div>	
									<div class="row">
										<div class="col s6">
											<label>
												<!-- <input name="statement" id="statement" type="checkbox" value="1"/> -->
												<input class="checklist" id="statement" type="checkbox"/>
												<span>Statement of purpose</span>
											</label>
											<input name="statement" class="statement" type="hidden" value="0"/>
										</div>	
									</div>	
									<div class="row">
										<div class="col s12">
											<label>
												<!-- <input name="imigration" id="imigration" type="checkbox" value="1"/> -->
												<input class="checklist" id="imigration" type="checkbox"/>
												<span>Confirmation of immigration history (if applicable): to include copies of any previous Tier 4 (General) visas,and if applying for your visa from within the UK a copyof your current Tier 4 (General) visa.</span>
											</label>
											<input name="imigration" class="imigration" type="hidden" value="0"/>
										</div>	
									</div>	
									<div class="row">
										<div class="col s6">
											<label>
												<!-- <input name="skype_profile" id="skype_profile"  type="checkbox" value="1"/> -->
												<input class="checklist" id="skype_profile" type="checkbox"/>
												<span>Skype profile screen shot.</span>
											</label>
											<input name="skype_profile" class="skype_profile" type="hidden" value="0"/>
										</div>	
									</div>	
									<div class="row">
										<div class="col s6">
											<label>
												<!-- <input name="tb_screenshot" id="tb_screenshot" type="checkbox" value="1"/> -->
												<input class="checklist" id="tb_screenshot" type="checkbox"/>
												<span>TB screening Test Report</span>
											</label>
											<input name="tb_screenshot" class="tb_screenshot" type="hidden" value="0"/>
										</div>	
									</div>	
									<div class="row">
										<div class="col s6">
											<label>
												<!-- <input name="fund_proof" id="fund_proof" type="checkbox" value="1"/> -->
												<input class="checklist" id="fund_proof" type="checkbox"/>
												<span>Maintenance fund proof</span>
											</label>
											<input name="fund_proof" class="fund_proof" type="hidden" value="0"/>
										</div>	
									</div>
									<div class="row">
										<div class="col s6">
											<label>
												<!-- <input name="exp_cert" id="exp_cert" type="checkbox" value="1"/> -->
												<input class="checklist" id="exp_cert" type="checkbox"/>
												<span>Experience certificates</span>
											</label>
											<input name="exp_cert" class="exp_cert" type="hidden" value="0"/>
										</div>	
									</div>										
                                </div>
                                <div class="controls">
                                        <a class="mgbtn2 back_button">Back</a>
                                                        <input id="form_name" type="hidden" class="validate" name="type_of" value="personal_stat">
                                                        <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                                                      {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
  
                        
</div>

                