<div id="tab_9" class="tab-pane " >
        
<div class="content-head">
                                        <h4>Immigration History</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->

                                </div>


                                <div class="pane">   
									<div class="academic">
                                        <div class="mgv-field">
                                                <div class="name">
                                                Have you been to the UK in the past 10 years?
                                                </div>
                                                <div class="value" id="in_uk" data-type="radio"></div>
                                        </div>                 
                                        <div class="mgv-field ">
                                                <div class="name">
                                                Have you previously studied in any country on either a Tier 4 (General) visa or a Pre-Tier 4 student visa? 
                                                </div>
                                                <div class="value" id="studied_uk" data-type="radio"></div>
                                        </div>
					



					<div class="mgv-row ">
						<table class="mgv-table    hide prev-studied-country-wrapper" id="prev-studied-wrapper">
						        <tr>
						                <th>Country</th>
						                <th>Visa start date</th>
						                <th>Visa end date</th>
						                <th>University</th>
						                <th>Course title</th>
						                <th>Status</th>
								<th>CAS start date</th>
						                <th>CAS end date</th>
						                <th>CAS/Visa copy/transcripts</th>
						        </tr>
						        <tr>
						                <td id="studied_country"></td>
						                <td id="studied_visa_start_date"></td>
						                <td id="studied_visa_end_date"></td>
						                <td id="studied_university"></td>
						                <td id="studied_course_title"></td>
						                <td id="studied_status"></td>
								<td id="studied_cas_start_date"></td>
								<td id="studied_cas_end_date"></td>
						                <td id="cas_transcripts"></td>
						        </tr>
						</table>
                 			</div>



					<!--<div id="prev-studied-wrapper" class="hide prev-studied-country-wrapper">
						<div class="academic">

													<div class="mgv-field">
															<div class="name">
														   Country
															</div>
															<div class="value" id="studied_country"></div>
													</div>                 
													<div class="mgv-field">
															<div class="name">
														   Visa start date
															</div>
															<div class="value" id="studied_visa_start_date"></div>
													</div>  
													<div class="mgv-field">
															<div class="name">
														   Visa end date
															</div>
															<div class="value" id="studied_visa_end_date"></div>
													</div> 												
													<div class="mgv-field">
															<div class="name">
														   University
															</div>
															<div class="value" id="studied_university"></div>
													</div>
													<div class="mgv-field">
															<div class="name">
														   Course title
															</div>
															<div class="value" id="studied_course_title"></div>
													</div>  
													<div class="mgv-field">
															<div class="name">
														   Status
															</div>
															<div class="value" id="studied_status"></div>
													</div>  
													<div class="mgv-field">
															<div class="name">
														   CAS start date
															</div>
															<div class="value" id="studied_cas_start_date"></div>
													</div>  
													<div class="mgv-field">
															<div class="name">
														   CAS end date
															</div>
															<div class="value" id="studied_cas_end_date"></div>
													</div>  
													<div class="mgv-field">
															<div class="name">
														   CAS/Visa copy/transcripts
															</div>
															<div class="value" id="cas_transcripts"></div>
													</div>  													
											</div>
										</div>	-->									
										
                                        <div class="mgv-field ">
                                                <div class="name">
                                                Have you been issued with a UK visa in the past 10 years? 
                                                </div>
                                                <div class="value" id="uk_visa_issued" data-type="radio"></div>
                                        </div>
                                        <div class="mgv-field ">
                                                <div class="name">
                                                Have you been to any other countries in the past 10 years? 
                                                </div>
                                                <div class="value" id="other_countries" data-type="radio"></div>
                                        </div>

                                        <div class="mgv-row ">
						<table class="mgv-table other-countrier-details-wrapper hide" id="imigration-wrapper">
						        <tr>
						                <th>Country</th>
						                <th>Reason for your visit</th>
						                <th>Date you entered</th>
						                <th>Date you left</th>
						        </tr>
						        <tr>
						                <td id="other_country_name"></td>
						                <td id="reason"></td>
						                <td id="visite_date"></td>
						                <td id="leave_date"></td>
						        </tr>
						</table>
                 			</div>										

										<!--<div id="imigration-wrapper" class="other-countrier-details-wrapper hide">
											<div class="academic">                 
													<div class="mgv-field">
															<div class="name">
														   Country
															</div>
															<div class="value" id="other_country_name"></div>
													</div>                 
													<div class="mgv-field">
															<div class="name">
														   Reason for your visit
															</div>
															<div class="value" id="reason"></div>
													</div>  
													<div class="mgv-field">
															<div class="name">
														   Date you entered
															</div>
															<div class="value" id="visite_date"></div>
													</div> 												
													<div class="mgv-field">
															<div class="name">
														   Date you left
															</div>
															<div class="value" id="leave_date"></div>
													</div>  
											</div>
										</div> -->
                                                         
                                        <div class="mgv-field">
                                                <div class="name">
                                                Selected Course
                                                </div>
                                                <div class="value" id="course"></div>
                                        </div>    
                                        <div class="mgv-field">
                                                <div class="name">
                                                Selected University
                                                </div>
                                                <div class="value" id="selected_university_imigration"></div>
                                        </div>    
                                        <div class="mgv-field">
                                                <div class="name">
                                                Tuition Fee
                                                </div>
                                                <div class="value" id="tution_fee"></div>
                                        </div>    
                                        <div class="mgv-field">
                                                <div class="name">
                                                Scholarship Status
                                                </div>
                                                <div class="value" id="scholarship_stat"></div>
                                        </div>    
                                        <div class="mgv-field">
                                                <div class="name">
                                                University Application Status
                                                </div>
                                                <div class="value" id="uni_aplctn_stat"></div>
                                        </div>                                           
										<div class="mgv-field">
                                                <div class="name">
                                                Offer Letter Status
                                                </div>
                                                <div class="value" id="offer_stat"></div>
                                        </div>    
									 <div class="mgv-field" id="ofl-upload" style="display: none;">
											<div class="name">Offer Letter File</div>
											<div class="value uploaded-file" id="offerletter_proof" data-type="file"></div>
											<div class="file-approve-buttons hide">
												<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="offer-letter-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
												<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="offer-letter-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
											</div>	 
									 </div>
										<div class="mgv-field">
                                                <div class="name">
                                                Tuition Fee Status
                                                </div>
                                                <div class="value" id="tuitionfeecheck"></div>
                                        </div>
										<div class="mgv-field">
                                                <div class="name">
                                                Amount Paid
                                                </div>
                                                <div class="value" id="tution_paid"></div>
                                        </div>
										<div class="mgv-field">
                                                <div class="name">
                                                Balance Amount to be Paid
                                                </div>
                                                <div class="value" id="tution_balance"></div>
                                        </div>
										<div class="mgv-field">
                                                <div class="name">
                                                Mode of Maintenance Fund proof
                                                </div>
                                                <div class="value" id="mode_mainatanence"></div>
                                        </div>
									 <div class="mgv-field">
											<div class="name">Proof of Maintenance Fund (Eg : Bank Statement, StudentLoan Letter etc.)</div>
											<div class="value uploaded-file" id="maintenance_proof" data-type="file"></div>
											<div class="file-approve-buttons hide">
												<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="maintanence-fund-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
												<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="maintanence-fund-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
											</div>  
									 </div>
									 <div class="mgv-field">
											<div class="name">TB Screening Test Report</div>
											<div class="value uploaded-file" id="TB_proof" data-type="file"></div>
											<div class="file-approve-buttons hide">
												<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="tb-report-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
												<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="tb-report-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
											</div>   	
									 </div>	
									<div class="mgv-field">
											<div class="name">
											CAS Status
											</div>
											<div class="value" id="cas_stat"></div>
									</div>
									 <div class="mgv-field" id="cas-rcd" style="display: none;">
											<div class="name">CAS Files</div>
											<div class="value uploaded-file" id="cas_proof" data-type="file"></div>
											<div class="file-approve-buttons hide">
												<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="cas-file-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
												<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="cas-file-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
											</div>    	
									 </div>	
									<div class="mgv-field">
										<div class="name">Visa Details</div>
										<div class="value">
											<span id="visa_aplctn_stat"></span><br>
											<span class="visa-details-wrapper hide">
												<span id="visa_apntmnt_date"></span><br>
												<span id="visa_apntmnt_time"></span><br>
												<span id="visa_apntmnt_place"></span><br>
												<span id="visastatus"></span>
											</span>
										</div>
									</div>	
									<div class="mgv-field visa-details-wrapper hide">
										<div class="name">Visa files</div>
										<div class="value uploaded-file" id="visa_file" data-type="file"></div>
										<div class="file-approve-buttons hide">
											<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="visa-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
											<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="visa-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
										</div>     	
									</div>									
									 <div class="mgv-field">
											<div class="name">Flight Ticket</div>
											<div class="value uploaded-file" id="ticket_proof" data-type="file"></div>
											<div class="file-approve-buttons hide">
												<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="flight-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
												<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="flight-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
											</div>     	
									 </div>
									<div class="mgv-field">
											<div class="name">
											Enrollment Status
											</div>
											<div class="value" id="enrollment_stat"></div>
									</div>
									<div class="mgv-field" id="enrolled" style="display: none;">
											<div class="name">
											Enrollment Number
											</div>
											<div class="value" id="enrollment_num"></div>
									</div>
									<div class="mgv-field">
											<div class="name">
											Placement Through
											</div>
											<div class="value" id="placement_through"></div>
									</div>
									<div class="mgv-field">
											<div class="name">
											Invoice Status
											</div>
											<div class="value" id="invoice_stat"></div>
									</div>	
									 <div class="mgv-field">
											<div class="name">Uploaded Invoice</div>
											<div class="value uploaded-file" id="invoice_proof" data-type="file"></div>
											<div class="file-approve-buttons hide">
												<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="invoice-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
												<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="invoice-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
											</div>    	
									 </div>	
									<div class="mgv-field">
											<div class="name">
											Payment Received
											</div>
											<div class="value" id="payment_stat"></div>
									</div>
									<div class="mgv-field">
											<div class="name">
											Sub Agent
											</div>
											<div class="value" id="sub_agent"></div>
									</div>
									<div class="mgv-field">
											<div class="name">
											Invoice from Sub Agent
											</div>
											<div class="value" id="sub_agent_invoice"></div>
									</div>
									<div class="mgv-field">
											<div class="name">
											SubAgent Payment Status
											</div>
											<div class="value" id="sub_agent_paymnt_stat"></div>
									</div>	
								 <div class="mgv-field">
										<div class="name">Additional Documents</div>
										<div class="value uploaded-file" id="additional_proofs" data-type="file"></div>
										<div class="file-approve-buttons hide">
											<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="additional-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
											<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="additional-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
										</div>   	
								 </div>	
								<div class="mgv-field">
										<div class="name">
										Last Communication with Candidate
										</div>
										<div class="value" id="last_communication"></div>
								</div>
					</div>
            </div>
                        
</div>

                