<div id="tab_4" class="tab-pane" >
                <div class="content-head">
                        <h4>English Language Proficiency</h4>
                </div>
                <div class="pane">

                        <div class="academic">
                                <div class="mgv-field">
                                        <div class="name">
                                        Was English the language of instruction in your previous institution  
                                        </div>
                                        <div class="value nodata" id="eng_lang_check" data-type="radio"></div>
                                </div>
                                        <div class="mgv-field">
                                                <div class="name">Number of years you have studied in English</div>
                                                <div class="value nodata" id="yrs_english"></div>
                                        </div>
                                        <div class="mgv-field">
                                                <div class="name">I hold the following qualifications</div>
                                                <div class="value nodata" id="qualifications"></div>
                                        </div>				
                                        <div class="mgv-field hide" id="other-qualifications-wrapper">
                                                <div class="name">Other qualifications</div>
                                                <div class="value nodata" id="other_qualifications"></div>
                                        </div>
                                <div id="ielts-wrap" class="hide">
                                        <h3 style="margin-top: 30px;margin-bottom: 30px;">Score (if known)</h3>
					<div class="mgv-field">
                                                <div class="name">
                                                        Reading Score
                                                </div>
                                                <div class="value nodata" id="reading_score"></div>
                                        </div>
                                        <div class="mgv-field">
                                                <div class="name">
                                                        Writing Score
                                                </div>
                                                <div class="value nodata" id="writing_score"></div>
                                        </div>
                                        <div class="mgv-field">
                                                <div class="name">
                                                       Speaking Score
                                                </div>
                                                <div class="value nodata" id="speaking_score"></div>
                                        </div>											
                                        <div class="mgv-field">
                                                <div class="name">
                                                        Listening Score
                                                </div>
                                                <div class="value nodata" id="listening_score"></div>
                                        </div>
                                        <div class="mgv-field">
                                                <div class="name">
                                                        Over all Score
                                                </div>
                                                <div class="value nodata" id="over_all_score"></div>
                                        </div>																			
                                        <div class="mgv-field">
                                                <div class="name">
                                                       Date of test
                                                </div>
                                                <div class="value nodata" id="test_date"></div>
                                        </div>
                                </div>
                                <div id="ielts-test-wrap" class="hide">
                                        <div class="mgv-field">
                                                <div class="name">
                                                    If test is yet to be completed, please state date and time to be taken
                                                </div>
                                                <div class="value nodata">
							<span id="test_date_time"></span>&nbsp;&nbsp;
							<span id="test_time"></span>
						</div>
                                        </div>
                                </div>	

                        </div>


                        <div class="academic">
                                <div class="mgv-field">
                                        <div class="name">
                                        PTE completed ?  
                                        </div> 
                                        <div class="value nodata" id="pte" data-type="radio"></div>
                                </div>
                                       
                                <div id="pte-wrapper" class="hide">

                                                <h3 style="margin-top: 30px;margin-bottom: 30px;">Score (if known)</h3>
                                        
                                                            <div class="mgv-field">
                                                <div class="name">
                                                        Reading Score
                                                </div>
                                                <div class="value nodata" id="pte_reading"></div>
                                        </div>
                                        <div class="mgv-field">
                                                <div class="name">
                                                        Writing Score
                                                </div>
                                                <div class="value nodata" id="pte_writing"></div>
                                        </div>
                                        <div class="mgv-field">
                                                <div class="name">
                                                       Speaking Score
                                                </div>
                                                <div class="value nodata" id="pte_speaking"></div>
                                        </div>                                                                                  
                                        <div class="mgv-field">
                                                <div class="name">
                                                        Listening Score
                                                </div>
                                                <div class="value nodata" id="pte_listening"></div>
                                        </div>
                                        <div class="mgv-field">
                                                <div class="name">
                                                        Over all Score
                                                </div>
                                                <div class="value nodata" id="pte_overall_score"></div>
                                        </div>                                                                                                                                                  
                                        <div class="mgv-field">
                                                <div class="name">
                                                       Date of test
                                                </div>
                                                <div class="value nodata" id="pte_date_exam"></div>
                                        </div>

                                </div>   
                                        
                        </div>


                        <div class="academic">
                                <div class="mgv-field">
                                        <div class="name">
                                        Duolingo completed ?  
                                        </div> 
                                        <div class="value nodata" id="duolingo" data-type="radio"></div>
                                </div>
                                       
                                <div id="duolingo-wrapper" class="hide">

                            <h3 style="margin-top: 30px;margin-bottom: 30px;">Score (if known)</h3>
                                        
                                        <div class="mgv-field">
                                                <div class="name">
                                                        Reading Score
                                                </div>
                                                <div class="value nodata" id="duolingo_reading"></div>
                                        </div>
                                        <div class="mgv-field">
                                                <div class="name">
                                                        Writing Score
                                                </div>
                                                <div class="value nodata" id="duolingo_writing"></div>
                                        </div>
                                        <div class="mgv-field">
                                                <div class="name">
                                                       Speaking Score
                                                </div>
                                                <div class="value nodata" id="duolingo_speaking"></div>
                                        </div>                                          
                                        <div class="mgv-field">
                                                <div class="name">
                                                        Listening Score
                                                </div>
                                                <div class="value nodata" id="duolingo_listening"></div>
                                        </div>
                                        <div class="mgv-field">
                                                <div class="name">
                                                        Over all Score
                                                </div>
                                                <div class="value nodata" id="duolingo_overall_score"></div>
                                        </div>                                                                          
                                        <div class="mgv-field">
                                                <div class="name">
                                                       Date of test
                                                </div>
                                                <div class="value nodata" id="duolingo_date_exam"></div>
                                        </div>

                                </div>   
                                        
                        </div>


                        <div class="academic">
                                <div class="mgv-field">
                                        <div class="name">
                                        TOEFL completed ?  
                                        </div> 
                                        <div class="value nodata" id="toefl" data-type="radio"></div>
                                </div>
                                       
                                <div id="toefl-wrapper" class="hide">

                            <h3 style="margin-top: 30px;margin-bottom: 30px;">Score (if known)</h3>
                                        
                                        <div class="mgv-field">
                                                <div class="name">
                                                        Reading Score
                                                </div>
                                                <div class="value nodata" id="toefl_reading"></div>
                                        </div>
                                        <div class="mgv-field">
                                                <div class="name">
                                                        Writing Score
                                                </div>
                                                <div class="value nodata" id="toefl_writing"></div>
                                        </div>
                                        <div class="mgv-field">
                                                <div class="name">
                                                       Speaking Score
                                                </div>
                                                <div class="value nodata" id="toefl_speaking"></div>
                                        </div>                                          
                                        <div class="mgv-field">
                                                <div class="name">
                                                        Listening Score
                                                </div>
                                                <div class="value nodata" id="toefl_listening"></div>
                                        </div>
                                        <div class="mgv-field">
                                                <div class="name">
                                                        Over all Score
                                                </div>
                                                <div class="value nodata" id="toefl_overall_score"></div>
                                        </div>                                                                          
                                        <div class="mgv-field">
                                                <div class="name">
                                                       Date of test
                                                </div>
                                                <div class="value nodata" id="toefl_date_exam"></div>
                                        </div>

                                </div>   
                                        
                        </div>

                        
		</div>
        </div>