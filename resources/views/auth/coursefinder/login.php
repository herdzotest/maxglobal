<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Google fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Outlined" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />

  <!-- Stylesheets -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/vendors.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/main.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/custom.css">

  <title>Log In</title>
  <link rel="icon" type="image/x-icon" href="la-assets/coursefinder/img/coursesCards/icons/zodocs_fav.png">
</head>

<body class="preloader-visible" data-barba="wrapper">

  <!-- preloader start -->
  <div class="preloader js-preloader">
    <div class="preloader__bg"></div>
  </div>
  <!-- preloader end -->

  <!-- barba container start -->
  <div class="barba-container" data-barba="container">


    <main class="main-content  
  bg-beige-1
">

      <header data-anim="fade" data-add-bg="" class="header -base js-header">


        <div class="header__container py-10">
          <div class="row justify-between items-center">

            <div class="col-auto">
              <div class="header-left">

                <div class="header__logo ">
                  <a data-barba href="<?php echo url('/coursefinder'); ?>">
                    <img src="la-assets/coursefinder/img/general/logo.png" alt="logo">
                  </a>
                </div>

              </div>
            </div>


            <div class="col-auto">
              <div class="header-right d-flex items-center">

                <div class="header-menu js-mobile-menu-toggle ">
                  <div class="header-menu__content">
                    <div class="mobile-bg js-mobile-bg"></div>

                    <div class="d-none xl:d-flex items-center px-20 py-20 border-bottom-light">
                      
                      <?php 
                      if(isset(Auth::user()->type)) { ?>
                        <a href="<?php echo url('/logout'); ?>" class="text-dark-1">Log out</a>
                        <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="text-dark-1 ml-30">My Account</a>
                      <?php } else { ?>
                        <a href="<?php echo url('/userlogin'); ?>" class="text-dark-1">Log in</a>
                        <a href="<?php echo url('/signup'); ?>" class="text-dark-1 ml-30">Sign up</a>
                      <?php } ?>

                    </div>

                    <div class="menu js-navList">
                      <ul class="menu__nav text-dark-1 -is-active">
                    
                        <li class="menu-item-has-children">
                          <a data-barba href="#">Home</a>
                          <ul class="subnav">
                            <li class="menu__backButton js-nav-list-back">
                              <a href="#">Home</a>
                            </li>
                            <li><a href="<?php echo url('/coursefinder'); ?>">Home 1</a></li>
                            <li><a href="<?php echo url('/coursefinder2'); ?>">Home 2</a></li>
                            <li><a href="<?php echo url('/coursefinder3'); ?>">Home 3</a></li>
                            <li><a href="<?php echo url('/coursefinder4'); ?>">Home 4</a></li>
                            <li><a href="<?php echo url('/coursefinder5'); ?>">Home 5</a></li>
                            <li><a href="<?php echo url('/coursefinder6'); ?>">Home 6</a></li>
                            <li><a href="<?php echo url('/coursefinder7'); ?>">Home 7</a></li>
                            <li><a href="<?php echo url('/coursefinder8'); ?>">Home 8</a></li>
                            <li><a href="<?php echo url('/coursefinder9'); ?>">Home 9</a></li>
                            <li><a href="<?php echo url('/coursefinder10'); ?>">Home 10</a></li>
                          </ul>
                        </li>

                        <li>
                          <a data-barba href="<?php echo url('/aboutus'); ?>">
                            About Us <i class="text-13 ml-10"></i>
                          </a>
                        </li>

                        <li class="menu-item-has-children">
                          <a data-barba href="#">Courses</a>
                          <ul class="subnav">
                            <li class="menu__backButton js-nav-list-back">
                              <a href="#">Courses</a>
                            </li>
                            <li><a href="<?php echo url('/coursesearch'); ?>">Course 1</a></li>
                            <li><a href="<?php echo url('/coursesearch2'); ?>">Course 2</a></li>
                            <li><a href="<?php echo url('/coursesearch3'); ?>">Course 3</a></li>
                            <li><a href="<?php echo url('/coursesearch4'); ?>">Course 4</a></li>
                          </ul>
                        </li>

                        <li>
                          <a data-barba href="<?php echo url('/blog'); ?>">
                            Blog <i class="text-13 ml-10"></i>
                          </a>
                        </li>

                        <li>
                          <a data-barba href="<?php echo url('/topcourses'); ?>">
                            Top Courses <i class="text-13 ml-10"></i>
                          </a>
                        </li>

                        <li>
                          <a data-barba href="<?php echo url('/contactus'); ?>">
                            Contact Us <i class="text-13 ml-10"></i>
                          </a>
                        </li>
                    
                      </ul>
                    </div>

                    <div class="mobile-footer px-20 py-20 border-top-light js-mobile-footer">
                      
                    </div>
                  </div>

                  <div class="header-menu-close" data-el-toggle=".js-mobile-menu-toggle">
                    <div class="size-40 d-flex items-center justify-center rounded-full bg-white">
                      <!-- <div class="icon-close text-dark-1 text-16"></div> -->
                      <i class="fa fa-close"></i>
                    </div>
                  </div>

                  <div class="header-menu-bg"></div>
                </div>


                <div class="mr-30">

                  <div class="d-none xl:d-block ml-20">
                    <button class="text-dark-1 items-center" data-el-toggle=".js-mobile-menu-toggle">
                      <i class="fa fa-bars"></i>
                    </button>
                  </div>

                </div>

                <div class="header-right__buttons md:d-none">
                  <a href="<?php echo url('/signup'); ?>" class="button -sm -rounded -dark-1 text-white">Sign Up</a>
                </div>
              </div>
            </div>

          </div>
        </div>
      </header>


      <div class="content-wrapper  js-content-wrapper"><!-- <br><br><br><br> -->

        <section class="form-page js-mouse-move-container">
          <div class="form-page__img bg-dark-1">
            <div class="form-page-composition">
              <div class="-bg"><img data-move="30" class="js-mouse-move" src="la-assets/coursefinder/img/login/bg.png" alt="bg"></div>
              <div class="-el-1"><img data-move="20" class="js-mouse-move" src="la-assets/coursefinder/img/home-9/hero/bg.png" alt="image"></div>
              <br><br><br><br><br><br><br><br><br><br><br><div class="-el-2"><img data-move="40" class="js-mouse-move" src="la-assets/coursefinder/img/home-9/hero/1.png" alt="icon"></div>
              <div class="-el-3"><img data-move="40" class="js-mouse-move" src="la-assets/coursefinder/img/home-9/hero/2.png" alt="icon"></div>
              <div class="-el-4"><img data-move="40" class="js-mouse-move" src="la-assets/coursefinder/img/home-9/hero/3.png" alt="icon"></div>
            </div>
          </div>

          <div class="form-page__content lg:py-50">
            <div class="container">
              <div class="row justify-center items-center">
                <div class="col-xl-8 col-lg-8">
                  <div class="px-50 py-50 md:px-25 md:py-25 bg-white shadow-1 rounded-16">
                    <h3 class="text-30 lh-13">Login</h3>
                    <p class="mt-10">Don't have an account yet? <a href="<?php echo url('/signup'); ?>" class="text-purple-1">Sign up for free</a></p><br>

                    <?php if (count($errors) > 0) { ?>
                        <div class="alert alert-danger err_txt"><br>
                            <strong>Whoops!</strong> There were some problems with your input.<br>
                            <ul>
                                <?php foreach ($errors->all() as $error) { ?>
                                    <li><?php echo $error; ?></li>
                                <?php } ?>
                            </ul>
                        </div><br>
                    <?php } 
                    if (session('loginfail')) { ?>
                        <div class="alert alert-danger mt20 err_txt">
                          <?php echo session('loginfail'); ?>
                        </div>
                    <?php } ?>

                    <form class="contact-form respondForm__form row y-gap-20 pt-30" action="<?php echo url('/login'); ?>" method="post">
                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                      <input type="hidden" name="pathvalue" value="userlogincf">
                      <div class="col-12">
                        <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Email</label>
                        <input type="email" name="email" placeholder="Email">
                      </div>
                      <div class="col-12">
                        <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Password</label>
                        <input type="password" name="password" placeholder="Password">
                      </div>
                      <div class="col-12">

                        <input type="hidden" name="recaptcha" id="recaptcha">
                        
                        <button type="submit" id="submit" class="button -md -green-1 text-dark-1 fw-500 w-1/1">
                          Login
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>


      </div>
    </main>
  </div>
  <!-- barba container end -->

  <!-- JavaScript -->
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
  <script src="la-assets/coursefinder/js/vendors.js"></script>
  <script src="la-assets/coursefinder/js/main.js"></script>

  <!-- Recaptcha -->
  <script src="https://www.google.com/recaptcha/api.js?render=6LcIlq8jAAAAAIhuDZPNwT7ghj813obj7S1lMcXP"></script>
  <script>
           grecaptcha.ready(function() { 
               grecaptcha.execute('6LcIlq8jAAAAAIhuDZPNwT7ghj813obj7S1lMcXP', {action: '/login'}).then(function(token) { 
                  if (token) { 
                    document.getElementById('recaptcha').value = token;
                  }
               });
           });
  </script>
  <!-- End Recaptcha -->

</body>

</html>