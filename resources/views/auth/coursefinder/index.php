<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Google fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Outlined" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />

  <!-- Stylesheets -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/vendors.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/main.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/custom.css">

  <title>Course Finder</title>
  <link rel="icon" type="image/x-icon" href="la-assets/coursefinder/img/coursesCards/icons/zodocs_fav.png">

</head>

<body class="preloader-visible" data-barba="wrapper">

  <!-- preloader start -->
  <div class="preloader js-preloader">
    <div class="preloader__bg"></div>
  </div>
  <!-- preloader end -->

  <!-- barba container start -->
  <div class="barba-container" data-barba="container">


    <main class="main-content  ">

      <header data-anim="fade" data-add-bg="bg-dark-1" class="header -type-1 js-header">


        <div class="header__container">
          <div class="row justify-between items-center">

            <div class="col-auto">
              <div class="header-left">

                <div class="header__logo ">
                  <a data-barba href="<?php echo url('/coursefinder'); ?>">
                    <img src="la-assets/coursefinder/img/general/logo.png" alt="logo">
                  </a>
                </div>

              </div>
            </div>


            <div class="header-menu js-mobile-menu-toggle ">
              <div class="header-menu__content">
                <div class="mobile-bg js-mobile-bg"></div>

                <div class="d-none xl:d-flex items-center px-20 py-20 border-bottom-light">
                  
                  <?php 
                  if(isset(Auth::user()->type)) { ?>
                    <a href="<?php echo url('/logout'); ?>" class="text-dark-1">Log out</a>
                    <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="text-dark-1 ml-30">My Account</a>
                  <?php } else { ?>
                    <a href="<?php echo url('/userlogin'); ?>" class="text-dark-1">Log in</a>
                    <a href="<?php echo url('/signup'); ?>" class="text-dark-1 ml-30">Sign up</a>
                  <?php } ?>


                </div>

                <div class="menu js-navList">
                  <ul class="menu__nav text-white -is-active">

                    <li class="menu-item-has-children">
                      <a data-barba href="#">Home</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Home</a>
                        </li>
                        <li><a href="<?php echo url('/coursefinder'); ?>">Home 1</a></li>
                        <li><a href="<?php echo url('/coursefinder2'); ?>">Home 2</a></li>
                        <li><a href="<?php echo url('/coursefinder3'); ?>">Home 3</a></li>
                        <li><a href="<?php echo url('/coursefinder4'); ?>">Home 4</a></li>
                        <li><a href="<?php echo url('/coursefinder5'); ?>">Home 5</a></li>
                        <li><a href="<?php echo url('/coursefinder6'); ?>">Home 6</a></li>
                        <li><a href="<?php echo url('/coursefinder7'); ?>">Home 7</a></li>
                        <li><a href="<?php echo url('/coursefinder8'); ?>">Home 8</a></li>
                        <li><a href="<?php echo url('/coursefinder9'); ?>">Home 9</a></li>
                        <li><a href="<?php echo url('/coursefinder10'); ?>">Home 10</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/aboutus'); ?>">
                        About Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li class="menu-item-has-children">
                      <a data-barba href="#">Courses</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Courses</a>
                        </li>
                        <li><a href="<?php echo url('/coursesearch'); ?>">Course 1</a></li>
                        <li><a href="<?php echo url('/coursesearch2'); ?>">Course 2</a></li>
                        <li><a href="<?php echo url('/coursesearch3'); ?>">Course 3</a></li>
                        <li><a href="<?php echo url('/coursesearch4'); ?>">Course 4</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/blog'); ?>">
                        Blog <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/topcourses'); ?>">
                        Top Courses <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/contactus'); ?>">
                        Contact Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>


                  </ul>
                </div>

                <div class="header-menu-close" data-el-toggle=".js-mobile-menu-toggle">
                  <div class="size-40 d-flex items-center justify-center rounded-full bg-white">
                    <i class="fa fa-close"></i>
                  </div>
                </div>
                <div class="header-menu-bg"></div>

                
              </div>

            </div>


            <div class="col-auto">
              <div class="header-right d-flex items-center">

                <div class="header-right__icons text-white d-flex items-center">

                  <div class="d-none xl:d-block ml-20">
                    <button class="text-white items-center" data-el-toggle=".js-mobile-menu-toggle">
                      <i class="fa fa-bars"></i>
                    </button>
                  </div>

                </div>
                
                <div class="header-right__buttons d-flex items-center ml-30 md:d-none">
                  
                  <?php 
                  if(isset(Auth::user()->type)) { ?>
                    <a href="<?php echo url('/logout'); ?>" class="button -underline text-white">Log out</a>
                    <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="button -sm -white text-dark-1 ml-30">My Account</a>
                  <?php } else { ?>
                    <a href="<?php echo url('/userlogin'); ?>" class="button -underline text-white">Log in</a>
                    <a href="<?php echo url('/signup'); ?>" class="button -sm -white text-dark-1 ml-30">Sign up</a>
                  <?php } ?>

                </div>
              </div>
            </div>

          </div>
        </div>
      </header>


      <div class="content-wrapper  js-content-wrapper">

        <section class="masthead -type-1 js-mouse-move-container">
          <div class="masthead__bg">
            <img src="la-assets/coursefinder/img/home-1/hero/bg.png" alt="image">
          </div>

          <div class="container">
            <div data-anim-wrap class="row y-gap-30 justify-between items-end">
              <div class="col-xl-6 col-lg-6 col-sm-10">
                <div class="masthead__content">
                  <h1 data-anim-child="slide-up" class="masthead__title">
                    Process of Overseas Education Visa CRM 
                    with <span class="text-green-1 underline">Zodocs</span>
                  </h1>
                  <p data-anim-child="slide-up delay-1" class="masthead__text">
                    Zodocs CRM software is highly rated in the <br>
                    study abroad education industry.
                  </p>
                  <div data-anim-child="slide-up delay-2" class="masthead__buttons row x-gap-10 y-gap-10">
                    <div class="col-12 col-sm-auto">
                      <a data-barba href="<?php echo url('/signup'); ?>" class="button -md -purple-1 text-white">Join For Free</a>
                    </div>
                    <div class="col-12 col-sm-auto">
                      <a data-barba href="<?php echo url('/coursesearch'); ?>" class="button -md -outline-green-1 text-green-1">Find Courses</a>
                    </div>
                  </div>
                  <div data-anim-child="slide-up delay-3" class="masthead-info row y-gap-15 sm:d-none">

                    <div class="masthead-info__item d-flex items-center text-white">
                      <div class="masthead-info__icon mr-10">
                        <img src="la-assets/coursefinder/img/masthead/icons/1.svg" alt="icon">
                      </div>
                      <div class="masthead-info__title lh-1">Over 12 million students</div>
                    </div>

                    <div class="masthead-info__item d-flex items-center text-white">
                      <div class="masthead-info__icon mr-10">
                        <img src="la-assets/coursefinder/img/masthead/icons/2.svg" alt="icon">
                      </div>
                      <div class="masthead-info__title lh-1">More than 60,000 courses</div>
                    </div>

                    <div class="masthead-info__item d-flex items-center text-white">
                      <div class="masthead-info__icon mr-10">
                        <img src="la-assets/coursefinder/img/masthead/icons/3.svg" alt="icon">
                      </div>
                      <div class="masthead-info__title lh-1">Empowering Global Students</div>
                    </div>

                  </div>
                </div>
              </div>

              <div data-anim-child="slide-up delay-5" class="col-xl-6 col-lg-6">
                <div class="masthead-image">
                  <div class="masthead-image__el1">
                    <img class="js-mouse-move" data-move="40" src="la-assets/coursefinder/img/masthead/1.png" alt="image">

                    <div data-move="30" class="lg:d-none img-el -w-250 px-20 py-20 d-flex items-center bg-white rounded-8 js-mouse-move">
                      <div class="size-50 d-flex justify-center items-center bg-red-2 rounded-full">
                        <img src="la-assets/coursefinder/img/masthead/1.svg" alt="icon">
                      </div>
                      <div class="ml-20">
                        <div class="text-orange-1 text-16 fw-500 lh-1">3000 +</div>
                        <div class="mt-3">Free Courses</div>
                      </div>
                    </div>
                  </div>

                  <div class="masthead-image__el2">
                    <img class="js-mouse-move" data-move="70" src="la-assets/coursefinder/img/masthead/2.png" alt="image">

                    <div data-move="60" class="lg:d-none img-el -w-260 px-20 py-20 d-flex items-center bg-white rounded-8 js-mouse-move">
                      <img src="la-assets/coursefinder/img/masthead/ver.png" alt="icon">
                      <div class="ml-20">
                        <div class="text-orange-1 text-16 fw-500 lh-1">1000 +</div>
                        <div class="mt-3">Universities</div>
                      </div>
                    </div>
                  </div>

                  <div class="masthead-image__el3">
                    <img class="js-mouse-move" data-move="40" src="la-assets/coursefinder/img/masthead/3.png" alt="image">

                    <div data-move="30" class="shadow-4 img-el -w-260 px-30 py-20 d-flex items-center bg-white rounded-8 js-mouse-move">
                      <div class="img-el__side">
                        <div class="size-50 d-flex justify-center items-center bg-purple-1 rounded-full">
                          <img src="la-assets/coursefinder/img/masthead/2.svg" alt="icon">
                        </div>
                      </div>
                      <div class="">
                        <div class="text-purple-1 text-16 fw-500 lh-1">Congrats!</div>
                        <div class="mt-3">Your Admission Completed</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <svg class="svg-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28" preserveAspectRatio="none" shape-rendering="auto">
            <defs>
              <path id="gentle-wave" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z" />
            </defs>
            <g class="svg-waves__parallax">
              <use xlink:href="#gentle-wave" x="48" y="0" />
              <use xlink:href="#gentle-wave" x="48" y="3" />
              <use xlink:href="#gentle-wave" x="48" y="5" />
              <use xlink:href="#gentle-wave" x="48" y="7" />
            </g>
          </svg>
        </section>

        <section class="layout-pt-md layout-pb-lg">
          <div data-anim-wrap class="container">
            <div class="row justify-center text-center">
              <div class="col-auto">

                <div class="sectionTitle ">

                  <h2 class="sectionTitle__title ">Our Courses</h2>

                  <!-- <p class="sectionTitle__text ">10,000+ unique online course list designs</p> -->

                </div>

              </div>
            </div>

            <div class="tabs -pills pt-50 js-tabs">
              <!--<div class="tabs__controls d-flex justify-center x-gap-10 js-tabs-controls">

                <div>
                  <button class="tabs__button px-15 py-8 rounded-8 js-tabs-button is-active" data-tab-target=".-tab-item-1" type="button">All Categories</button>
                </div>

                <div>
                  <button class="tabs__button px-15 py-8 rounded-8 js-tabs-button " data-tab-target=".-tab-item-2" type="button">Animation</button>
                </div>

                <div>
                  <button class="tabs__button px-15 py-8 rounded-8 js-tabs-button " data-tab-target=".-tab-item-3" type="button">Design</button>
                </div>

                <div>
                  <button class="tabs__button px-15 py-8 rounded-8 js-tabs-button " data-tab-target=".-tab-item-4" type="button">Illustration</button>
                </div>

                <div>
                  <button class="tabs__button px-15 py-8 rounded-8 js-tabs-button " data-tab-target=".-tab-item-5" type="button">Lifestyle</button>
                </div>

                <div>
                  <button class="tabs__button px-15 py-8 rounded-8 js-tabs-button " data-tab-target=".-tab-item-6" type="button">Photo &amp; Film</button>
                </div>

                <div>
                  <button class="tabs__button px-15 py-8 rounded-8 js-tabs-button " data-tab-target=".-tab-item-7" type="button">Business</button>
                </div>

                <div>
                  <button class="tabs__button px-15 py-8 rounded-8 js-tabs-button " data-tab-target=".-tab-item-8" type="button">Writing</button>
                </div>

              </div> -->

              <div class="tabs__content pt-60 js-tabs-content">

                <div class="tabs__pane -tab-item-1 is-active">
                  <div class="row y-gap-30 justify-center">


                    <!--Course list -->
                    <?php 
                      if(count($courses) > 0) { 
                        foreach($courses as $result) {
                    ?>
                    <div class="col-lg-3 col-md-6">
                      <div data-anim-child="slide-up delay-1">

                        <!-- <a href="<?php //echo url('/coursedetails/'.$result->id); ?>" class="coursesCard -type-1 "> -->
                        <a href="<?php echo url('/coursedetails/'.Crypt::encrypt($result->id)); ?>" class="coursesCard -type-1 ">
                          <div class="relative">
                            <div class="coursesCard__image overflow-hidden rounded-8">
                              <?php if($result->university_logo!='') { ?>
                                <img class="w-1/1 course_logo" src="<?php echo $result->university_logo; ?>" alt="image">
                              <?php } else { ?>
                                <img class="w-1/1 course_logo" src="la-assets/coursefinder/img/coursesCards/no-image.png" alt="image">
                              <?php } ?>
                              <div class="coursesCard__image_overlay rounded-8"></div>
                            </div>
                            <div class="d-flex justify-between py-10 px-10 absolute-full-center z-3">

                            </div>
                          </div>

                          <div class="h-100 pt-15">
                            <div class="d-flex items-center">
                              <div class="text-14 lh-1 text-yellow-1 mr-10"><?php echo $result->university_name; ?></div>
                              
                            </div>

                            <div class="text-17 lh-15 fw-500 text-dark-1 mt-10"><?php echo $result->course_name; ?></div>

                            <div class="d-flex x-gap-10 items-center pt-10">

                              <div class="d-flex items-center">
                                <div class="mr-8">
                                  <img src="la-assets/coursefinder/img/coursesCards/icons/location-icon.png" alt="icon">
                                </div>
                                <div class="text-14 lh-1"><?php echo $result->country_name; ?></div>
                              </div>

                              <div class="d-flex items-center">
                                <div class="mr-8">
                                  <img src="la-assets/coursefinder/img/coursesCards/icons/2.svg" alt="icon">
                                </div>
                                <div class="text-14 lh-1"><?php echo $result->intake; ?></div>
                              </div>

                              <div class="d-flex items-center">
                                <div class="mr-8">
                                  <img src="la-assets/coursefinder/img/coursesCards/icons/1.svg" alt="icon">
                                </div>
                                <div class="text-14 lh-1"><?php echo $result->course_type; ?></div>
                              </div>

                            </div>

                            <div class="coursesCard-footer">
                              <div class="coursesCard-footer__author">
                                <div>Annual Tuition Fees</div>
                              </div>

                              <div class="coursesCard-footer__price">
                                <div></div>
                                <div>&#8377; <?php echo $result->tuition_fees; ?></div>
                              </div>
                            </div>
                          </div>
                        </a>

                      </div>
                    </div>
                    <?php }} ?>
                    <!-- End Course list -->

                  </div>
                </div>

              </div>
            </div>
          </div>
        </section>


        <section class="layout-pt-lg layout-pb-lg bg-purple-1">
          <div class="container">
            <div class="row justify-center text-center">
              <div class="col-auto">
                <div class="sectionTitle ">
                  <h2 class="sectionTitle__title text-green-1">What People Say</h2>
                </div>
              </div>
            </div>

            <div data-anim-wrap class="js-section-slider pt-50" data-gap="30" data-pagination data-slider-cols="base-3 xl-3 lg-2 md-2 sm-1">
              <div class="swiper-wrapper">

                <div class="swiper-slide">
                  <div data-anim-child="slide-left delay-1" class="testimonials -type-1">
                    <div class="testimonials__content">
                      <h4 class="testimonials__title">EdScholar</h4>
                      <p class="testimonials__text"> “Zodocs CRM streamlined our efforts with ready-to-use analytics and reports. Their excellent support team easily handled our complex requirements.”</p>

                      <div class="testimonials-footer">
                        <div class="testimonials-footer__image">
                          <img src="la-assets/img/avatar3.png" alt="image">
                        </div>

                        <div class="testimonials-footer__content">
                          <div class="testimonials-footer__title">Shibin</div>
                          <div class="testimonials-footer__text">CEO & Founder, EdScholar</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="swiper-slide">
                  <div data-anim-child="slide-left delay-2" class="testimonials -type-1">
                    <div class="testimonials__content">
                      <h4 class="testimonials__title">Masters Education</h4>
                      <p class="testimonials__text"> “Zodocs CRM makes scheduling follow-ups easy, ensuring no leads are missed, and customer support simplifies switching from Excel to CRM. ”</p>

                      <div class="testimonials-footer">
                        <div class="testimonials-footer__image">
                          <img src="la-assets/img/avatar2.png" alt="image">
                        </div>

                        <div class="testimonials-footer__content">
                          <div class="testimonials-footer__title">Blesson</div>
                          <div class="testimonials-footer__text">CEO & Founder, Masters Education</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="swiper-slide">
                  <div data-anim-child="slide-left delay-3" class="testimonials -type-1">
                    <div class="testimonials__content">
                      <h4 class="testimonials__title">EdScholar</h4>
                      <p class="testimonials__text"> “Zodocs CRM streamlined our efforts with ready-to-use analytics and reports. Their excellent support team easily handled our complex requirements.”</p>

                      <div class="testimonials-footer">
                        <div class="testimonials-footer__image">
                          <img src="la-assets/img/avatar4.png" alt="image">
                        </div>

                        <div class="testimonials-footer__content">
                          <div class="testimonials-footer__title">Shibin</div>
                          <div class="testimonials-footer__text">CEO & Founder, EdScholar</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="swiper-slide">
                  <div data-anim-child="slide-left delay-4" class="testimonials -type-1">
                    <div class="testimonials__content">
                      <h4 class="testimonials__title">Masters Education</h4>
                      <p class="testimonials__text"> “Zodocs CRM makes scheduling follow-ups easy, ensuring no leads are missed, and customer support simplifies switching from Excel to CRM.  ”</p>

                      <div class="testimonials-footer">
                        <div class="testimonials-footer__image">
                          <img src="la-assets/img/avatar2.png" alt="image">
                        </div>

                        <div class="testimonials-footer__content">
                          <div class="testimonials-footer__title">Blesson</div>
                          <div class="testimonials-footer__text">CEO & Founder, Masters Education</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>


              <div class="d-flex x-gap-20 items-center justify-end pt-60 lg:pt-40">
                <div class="col-auto">
                  <button class="button -outline-white text-white size-50 rounded-full d-flex justify-center items-center js-prev">
                    <i class="fa fa-arrow-left text-24"></i>
                  </button>
                </div>
                <div class="col-auto">
                  <button class="button -outline-white text-white size-50 rounded-full d-flex justify-center items-center js-next">
                    <i class="fa fa-arrow-right text-24"></i>
                  </button>
                </div>
              </div>

            </div>

            <div data-anim-wrap class="row y-gap-30 counter__row">

              <div class="col-lg-3 col-sm-6">
                <div data-anim-child="slide-left delay-1" class="counter -type-1">
                  <div class="counter__number">20,000+</div>
                  <div class="counter__title">Students worldwide</div>
                </div>
              </div>

              <div class="col-lg-3 col-sm-6">
                <div data-anim-child="slide-left delay-2" class="counter -type-1">
                  <div class="counter__number">50,000+</div>
                  <div class="counter__title">Total course views</div>
                </div>
              </div>

              <div class="col-lg-3 col-sm-6">
                <div data-anim-child="slide-left delay-3" class="counter -type-1">
                  <div class="counter__number">10,000+</div>
                  <div class="counter__title">Five-star course reviews</div>
                </div>
              </div>

              <div class="col-lg-3 col-sm-6">
                <div data-anim-child="slide-left delay-4" class="counter -type-1">
                  <div class="counter__number">10,000+</div>
                  <div class="counter__title">Students community</div>
                </div>
              </div>

            </div>
          </div>
        </section>

        <section class="layout-pt-lg layout-pb-lg bg-beige-1">
          <div data-anim-wrap class="container">
            <div class="row y-gap-30 justify-between items-center">
              <div class="col-xl-5 col-lg-6 col-md-10 order-2 order-lg-1">
                <div class="about-content">
                  <h2 data-anim-child="slide-up delay-1" class="about-content__title">Powering your study abroad journey with <span>ZODOCS</span></h2>
                  <!-- <p data-anim-child="slide-up delay-2" class="about-content__text">Use the list below to bring attention to your product’s key<br> differentiator.</p> -->
                  <div data-anim-child="slide-up delay-3" class="y-gap-20 pt-30">

                    <div class="d-flex items-center">
                      <div class="about-content-list__icon">
                        <i class="icon" data-feather="check"></i>
                      </div>
                      <div class="about-content-list__title">Student application</div>
                    </div>

                    <div class="d-flex items-center">
                      <div class="about-content-list__icon">
                        <i class="icon" data-feather="check"></i>
                      </div>
                      <div class="about-content-list__title">One click staff allocation</div>
                    </div>

                    <div class="d-flex items-center">
                      <div class="about-content-list__icon">
                        <i class="icon" data-feather="check"></i>
                      </div>
                      <div class="about-content-list__title">Course finder</div>
                    </div>

                    <div class="d-flex items-center">
                      <div class="about-content-list__icon">
                        <i class="icon" data-feather="check"></i>
                      </div>
                      <div class="about-content-list__title">Tons of reports</div>
                    </div>

                  </div>

                  <div data-anim-child="slide-up delay-4" class="d-inline-block mt-30">
                    <a href="<?php echo url('/signup'); ?>" class="button -md -dark-1 text-white">Join Free</a>
                  </div>
                </div>
              </div>

              <div class="col-xl-5 col-lg-6 order-1 order-lg-2">
                <div data-anim-child="slide-up delay-5" class="about-image">
                  <img src="la-assets/coursefinder/img/about/1.png" alt="image">
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="layout-pt-lg layout-pb-lg bg-dark-2">
          <div data-anim-wrap class="container">
            <div class="row justify-center text-center">
              <div data-anim-child="slide-up delay-1" class="col-auto">

                <div class="sectionTitle ">

                  <h2 class="sectionTitle__title text-white">What makes Zodocs the Best CRM</h2>

                  <!-- <p class="sectionTitle__text text-white">Lorem ipsum dolor sit amet, consectetur.</p> -->

                </div>

              </div>
            </div>

            <div class="row y-gap-30 pt-50">

              <div data-anim-child="slide-up delay-2" class="col-lg-4 col-md-6">
                <div class="stepCard -type-1 -stepCard-hover">
                  <div class="stepCard__content">
                    <div class="stepCard__icon">
                      <i class="fa fa-university text-64 text-green-1"></i>
                    </div>
                    <h4 class="stepCard__title">01. Easy data access</h4>
                    <p class="stepCard__text"> Browse our courses, or filter your search by your preferred subject area and level of study</p>
                  </div>
                </div>
              </div>

              <div data-anim-child="slide-up delay-3" class="col-lg-4 col-md-6">
                <div class="stepCard -type-1 -stepCard-hover">
                  <div class="stepCard__content">
                    <div class="stepCard__icon">
                      <i class="fa fa-graduation-cap text-64 text-green-1"></i>
                    </div>
                    <h4 class="stepCard__title">02. Timely follow ups</h4>
                    <p class="stepCard__text"> You can set follow ups to ensure support for all the candidates throughout the process</p>
                  </div>
                </div>
              </div>

              <div data-anim-child="slide-up delay-4" class="col-lg-4 col-md-6">
                <div class="stepCard -type-1 -stepCard-hover">
                  <div class="stepCard__content">
                    <div class="stepCard__icon">
                      <i class="fa fa-tasks text-64 text-green-1"></i>
                    </div>
                    <h4 class="stepCard__title">03. Notifications</h4>
                    <p class="stepCard__text"> Zodocs sends proper status update notifications by email/ sms/ whatsapp to students & staff</p>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </section>

        <footer class="footer -type-1 bg-dark-1 -green-links">
          <div class="container">
            
            <div class="py-30 border-top-light-15">
              <div class="row justify-between items-center y-gap-20">
                <div class="col-auto">
                  <div class="d-flex items-center h-100 text-white">
                    © 2022 ZODOCS. All Right Reserved.
                  </div>
                </div>

                <div class="col-auto">
                  <div class="d-flex x-gap-20 y-gap-20 items-center flex-wrap">
                    <div>Powered By
                      <a href="http://demo.zodocs.com/" class="text-white" target="_blank">
                        <span class="text-15">ZODOCS</span>
                      </a>. Solution By
                      <a href="https://www.labgex.com/" class="text-white" target="_blank">
                        <span class="text-15">LabGex Innovations (P) Ltd</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>


      </div>
    </main>
  </div>
  <!-- barba container end -->

  <!-- JavaScript -->
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
  <script src="la-assets/coursefinder/js/vendors.js"></script>
  <script src="la-assets/coursefinder/js/main.js"></script>

</body>

</html>