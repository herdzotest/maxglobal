Dear {{ $user }},<br><br>

Your account has been successfully activated. <br>

You can now login to Zodocs with your credentials
<a href="{{ url('/userlogin') }}">{{ str_replace("http://", "", url('/userlogin')) }}</a><br><br>

Best Regards,<br>
Zodocs


