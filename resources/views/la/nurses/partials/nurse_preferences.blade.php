<div id="tab_4" class="tab-pane " >
                                
                                <div class="content-head">
                                                <h4>Preferences</h4>
                                                <!-- <div class="id">Nurse ID : <b>#12675</b></div> -->
                                        </div>
                                        {!! Form::open(['action' => 'LA\NursesController@store', 'id' => 'preferences-form']) !!}
               
                        <div class="pane">
                                
                                <div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
														<select name="preferred_sector" id="preferred_sector" class="formSelect">
															<option value="">Select</option>
                                                            <option value="NHS">NHS</option>
                                                            <option value="Nursing home">Nursing home</option>
                                                            <option value="Private hospital">Private hospital</option>
														</select>
														<label for="">Preferred Sector </label>
                                                </div>


                                        </div>
                                        <div class=" col s6 ">
                                        <div class="input-field">
													<input name="preferred_designation"  id="preferred_designation" type="text">												
													<label for="preferred_designation">Preferred Designation</label>
											</div>


                                        </div>
                                </div>

                                <div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
                                                    <input name="preferred_location"  id="preferred_location" type="text">
													<label for="preferred_location">Preferred Location</label>
                                                </div>


                                        </div>
										
										<div class="col s6">
                                            <div class="input-field">
                                                <input name="preferred_salary"  id="preferred_salary" type="text">
												<label for="preferred_salary">Preferred Salary</label>
                                            </div>    
										</div>	
                                </div>
								
								<div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
                                                    <input name="preferred_hours"  id="preferred_hours" type="text">
													<label for="preferred_hours">Preferred Hours</label>
                                                </div>


                                        </div>
										
										<div class="col s6">
                                            <div class="input-field">
                                                <input name="preferred_shifts"  id="preferred_shifts" type="text">
												<label for="preferred_shifts">Preferred Shifts</label>
                                            </div>    
										</div>	
                                </div>							
								
                                
                        </div>
                        <div class="controls">
                                        <a class="mgbtn2">Back</a>
                                        <input id="form_name" type="hidden" class="validate" name="type_of" value="preferences">
                                        <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                        {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
                </div>

                