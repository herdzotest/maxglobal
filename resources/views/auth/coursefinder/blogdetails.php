<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Google fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Outlined" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />

  <!-- Stylesheets -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/vendors.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/main.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/custom.css">

  <title>Blog</title>
  <link rel="icon" type="image/x-icon" href="la-assets/coursefinder/img/coursesCards/icons/zodocs_fav.png">
</head>

<body class="preloader-visible" data-barba="wrapper">

  <!-- preloader start -->
  <div class="preloader js-preloader">
    <div class="preloader__bg"></div>
  </div>
  <!-- preloader end -->

  <!-- barba container start -->
  <div class="barba-container" data-barba="container">


    <main class="main-content  ">

      <header data-anim="fade" data-add-bg="bg-dark-1" class="header -type-1 js-header">


        <div class="header__container">
          <div class="row justify-between items-center">

            <div class="col-auto">
              <div class="header-left">

                <div class="header__logo ">
                  <a data-barba href="<?php echo url('/coursefinder'); ?>">
                    <img src="la-assets/coursefinder/img/general/logo.png" alt="logo">
                  </a>
                </div>

              </div>
            </div>


            <div class="header-menu js-mobile-menu-toggle ">
              <div class="header-menu__content">
                <div class="mobile-bg js-mobile-bg"></div>

                <div class="d-none xl:d-flex items-center px-20 py-20 border-bottom-light">
                  
                  <?php 
                  if(isset(Auth::user()->type)) { ?>
                    <a href="<?php echo url('/logout'); ?>" class="text-dark-1">Log out</a>
                    <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="text-dark-1 ml-30">My Account</a>
                  <?php } else { ?>
                    <a href="<?php echo url('/userlogin'); ?>" class="text-dark-1">Log in</a>
                    <a href="<?php echo url('/signup'); ?>" class="text-dark-1 ml-30">Sign up</a>
                  <?php } ?>

                </div>

                <div class="menu js-navList">
                  <ul class="menu__nav text-white -is-active">

                    <li class="menu-item-has-children">
                      <a data-barba href="#">Home</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Home</a>
                        </li>
                        <li><a href="<?php echo url('/coursefinder'); ?>">Home 1</a></li>
                        <li><a href="<?php echo url('/coursefinder2'); ?>">Home 2</a></li>
                        <li><a href="<?php echo url('/coursefinder3'); ?>">Home 3</a></li>
                        <li><a href="<?php echo url('/coursefinder4'); ?>">Home 4</a></li>
                        <li><a href="<?php echo url('/coursefinder5'); ?>">Home 5</a></li>
                        <li><a href="<?php echo url('/coursefinder6'); ?>">Home 6</a></li>
                        <li><a href="<?php echo url('/coursefinder7'); ?>">Home 7</a></li>
                        <li><a href="<?php echo url('/coursefinder8'); ?>">Home 8</a></li>
                        <li><a href="<?php echo url('/coursefinder9'); ?>">Home 9</a></li>
                        <li><a href="<?php echo url('/coursefinder10'); ?>">Home 10</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/aboutus'); ?>">
                        About Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li class="menu-item-has-children">
                      <a data-barba href="#">Courses</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Courses</a>
                        </li>
                        <li><a href="<?php echo url('/coursesearch'); ?>">Course 1</a></li>
                        <li><a href="<?php echo url('/coursesearch2'); ?>">Course 2</a></li>
                        <li><a href="<?php echo url('/coursesearch3'); ?>">Course 3</a></li>
                        <li><a href="<?php echo url('/coursesearch4'); ?>">Course 4</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/blog'); ?>">
                        Blog <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/topcourses'); ?>">
                        Top Courses <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/contactus'); ?>">
                        Contact Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>


                  </ul>
                </div>

                
              </div>

            </div>


            <div class="col-auto">
              <div class="header-right d-flex items-center">

                <div class="header-right__icons text-white d-flex items-center">

                  <div class="d-none xl:d-block ml-20">
                    <button class="text-white items-center" data-el-toggle=".js-mobile-menu-toggle">
                      <i class="fa fa-bars"></i>
                    </button>
                  </div>

                </div>
                
                <div class="header-right__buttons d-flex items-center ml-30 md:d-none">
                  
                  <?php 
                  if(isset(Auth::user()->type)) { ?>
                    <a href="<?php echo url('/logout'); ?>" class="button -underline text-white">Log out</a>
                    <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="button -sm -white text-dark-1 ml-30">My Account</a>
                  <?php } else { ?>
                    <a href="<?php echo url('/userlogin'); ?>" class="button -underline text-white">Log in</a>
                    <a href="<?php echo url('/signup'); ?>" class="button -sm -white text-dark-1 ml-30">Sign up</a>
                  <?php } ?>

                </div>
              </div>
            </div>

          </div>
        </div>
      </header>


      <div class="content-wrapper  js-content-wrapper">


        <section data-anim="fade" class="breadcrumbs ">
          <div class="container">
            <div class="row">
              <div class="col-auto">
                <div class="breadcrumbs__content">

                  <div class="breadcrumbs__item ">
                    <a href="<?php echo url('/coursefinder'); ?>">Home</a>
                  </div>

                  <div class="breadcrumbs__item ">
                    Blog
                  </div>

                </div>
              </div>
            </div>
          </div>
        </section>


        <section class="page-header -type-1">
          <div class="container">
            <div class="page-header__content">
              <div class="row justify-center text-center">
                <div class="col-auto">
                  <div data-anim="slide-up delay-1">
                    <div class="text-14 text-purple-1 uppercase fw-500 mb-8">EDUCATION</div>


                    <h1 class="page-header__title lh-14">Access to World-Class Education<br></h1>


                    <p class="page-header__text">August 10, 2024</p>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>


        <section class="layout-pt-md">
          <div class="container">
            <div class="ratio ratio-16:9 rounded-8 bg-image js-lazy" data-bg="la-assets/coursefinder/img/blog/b1.jpg"></div>
          </div>
        </section>

        <section class="layout-pt-md layout-pb-lg">
          <div class="container">
            <div class="blogSection">
              <div class="blogCard">
                <div class="row justify-center">
                  <div class="col-xl-8 col-lg-9 col-md-11">
                    <div class="blogCard__content">
                      <h4 class="text-18 fw-500"><br>The Complete Guide to MBA Admission Requirements in the USA</h4>
                      <p class="mt-30">Getting an MBA (Master of Business Administration) in the United States can boost your career. But the process to get in isn’t easy. Many people want these spots, so it’s tough.</p>

                      <ul class="ul-list y-gap-10 mt-30">
                        <li>You need a bachelor’s degree from an accredited school to apply. </li>
                        <li>This degree can come from any field, though business-related degrees might give you a small edge. </li>
                        <li>Some programs like students with backgrounds in math-heavy subjects (like engineering or math) because MBA classes involve a lot of number crunching.</li>
                        <li>Seek professional guidance to simplify the application and visa process.</li>
                      </ul>

                      This guide will walk you through what you need to apply for an MBA in the USA. It’ll help you understand the journey better.
                        <br><br>
                        Living and studying in a new country helps you understand and appreciate different cultures, traditions, and viewpoints. This not only enriches your personal growth but also makes you a highly adaptable and globally aware individual.
                      </p>
                    </div>

                    <div class="row y-gap-30 pt-30">
                      <div class="col-sm-6">
                        <img src="la-assets/coursefinder/img/blog-list/5.png" alt="image" class="w-1/1 initial-img rounded-8">
                      </div>
                      <div class="col-sm-6">
                        <img src="la-assets/coursefinder/img/blog-list/2.png" alt="image" class="w-1/1 initial-img rounded-8">
                      </div>
                    </div>

                    <div class="blogCard__content pt-30">
                      <p>
                        Employers often value candidates with international education due to their global exposure, adaptability, and advanced skills. Many study-abroad destinations also offer post-graduation work permits, allowing you to gain valuable professional experience.
                        <br><br>
                        Moving to a foreign country challenges you to step out of your comfort zone. From managing finances to adapting to a new lifestyle, you’ll develop critical life skills and a sense of independence that will benefit you in every aspect of life.
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              <!-- <div class="row justify-center pt-30">
                <div class="col-xl-8 col-lg-9 col-md-11">
                  <div class="row y-gap-20 justify-between items-center">
                    <div class="col-auto">
                      <div class="d-flex items-center">
                        <div class="lh-1 text-dark-1 fw-500 mr-30">Share</div>

                        <div class="d-flex x-gap-15">
                          <a href="blog-single.html#">Fb</a>
                          <a href="blog-single.html#">Tw</a>
                          <a href="blog-single.html#">In</a>
                          <a href="blog-single.html#">Ln</a>
                        </div>
                      </div>
                    </div>

                    <div class="col-auto">
                      <div class="row x-gap-10 y-gap-10">

                        <div class="col-auto">
                          <a href="blog-single.html#" class="badge -sm -light-3 text-11 text-dark-1">Courses</a>
                        </div>

                        <div class="col-auto">
                          <a href="blog-single.html#" class="badge -sm -light-3 text-11 text-dark-1">Learn</a>
                        </div>

                        <div class="col-auto">
                          <a href="blog-single.html#" class="badge -sm -light-3 text-11 text-dark-1">Online</a>
                        </div>

                        <div class="col-auto">
                          <a href="blog-single.html#" class="badge -sm -light-3 text-11 text-dark-1">LMS</a>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row justify-center pt-30">
                <div class="col-xl-8 col-lg-9 col-md-11">
                  <div class="d-flex border-bottom-light border-top-light py-30">
                    <div class="">
                      <div class="bg-image size-70 rounded-full js-lazy" data-bg="img/blog/blog-single/author.png"></div>
                    </div>

                    <div class="ml-30 md:ml-20">
                      <h4 class="text-17 lh-15 fw-500">Brooklyn Simmons</h4>
                      <div class="mt-5">Medical Assistant</div>
                      <p class="mt-25">Studying abroad is a transformative experience that offers unmatched academic and personal benefits. Whether you’re aiming to enhance your career prospects, gain global exposure, or challenge yourself, overseas education is a step toward a brighter future.</p>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row justify-center">
                <div class="col-xl-8 col-lg-9 col-md-11">
                  <div class="border-bottom-light py-30">
                    <div class="row x-gap-50 justify-between items-center">
                      <div class="col-md-4 col-6">
                        <a href="blog-single.html" class="related-nav__item -prev decoration-none">
                          <div class="related-nav__arrow">
                            <i class="icon size-20 pt-5" data-feather="arrow-left"></i>
                          </div>
                          <div class="related-nav__content">
                            <div class="text-17 text-dark-1 fw-500">Prev</div>
                            <p class="text-dark-1 mt-8">How to Choose the Right University for Your Career</p>
                          </div>
                        </a>
                      </div>

                      <div class="col-auto lg:d-none">
                        <div class="related-nav__icon row">

                          <div class="col-4">
                            <span></span>
                          </div>

                          <div class="col-4">
                            <span></span>
                          </div>

                          <div class="col-4">
                            <span></span>
                          </div>

                          <div class="col-4">
                            <span></span>
                          </div>

                          <div class="col-4">
                            <span></span>
                          </div>

                          <div class="col-4">
                            <span></span>
                          </div>

                          <div class="col-4">
                            <span></span>
                          </div>

                          <div class="col-4">
                            <span></span>
                          </div>

                          <div class="col-4">
                            <span></span>
                          </div>

                        </div>
                      </div>

                      <div class="col-md-4 col-6 d-flex justify-end">
                        <a href="blog-single.html" class="related-nav__item -next text-right decoration-none">
                          <div class="related-nav__content">
                            <div class="text-17 text-dark-1 fw-500">Next</div>
                            <p class="text-dark-1 mt-8">The Ultimate Guide to Student Visas for Study Abroad</p>
                          </div>
                          <div class="related-nav__arrow">
                            <i class="icon size-20 pt-5" data-feather="arrow-right"></i>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row justify-center pt-30">
                <div class="col-xl-8 col-lg-9 col-md-11">
                  <div class="blogPost -comments">
                    <div class="blogPost__content">
                      <h2 class="text-20 fw-500">
                        Reviews
                      </h2>

                      <ul class="comments__list mt-30">
                        <li class="comments__item">
                          <div class="comments__item-inner md:direction-column">
                            <div class="comments__img mr-20">
                              <div class="bg-image rounded-full js-lazy" data-bg="la-assets/coursefinder/img/blog-list/avatar.png"></div>
                            </div>

                            <div class="comments__body md:mt-15">
                              <div class="comments__header">
                                <h4 class="text-17 fw-500 lh-15">
                                  Ali Tufan
                                  <span class="text-13 text-light-1 fw-400">3 Days ago</span>
                                </h4>

                                <div class="stars"></div>
                              </div>

                              <h5 class="text-15 fw-500 mt-15">The best LMS Design</h5>
                              <div class="comments__text mt-10">
                                <p>This course is a very applicable. Professor Ng explains precisely each algorithm and even tries to give an intuition for mathematical and statistic concepts behind each algorithm. Thank you very much.</p>
                              </div>

                              <div class="comments__helpful mt-20">
                                <span class="text-13 text-purple-1">Was this review helpful?</span>
                                <button class="button text-13 -sm -purple-1 text-white">Yes</button>
                                <button class="button text-13 -sm -light-7 text-purple-1">No</button>
                              </div>
                            </div>
                          </div>
                        </li>

                        <li class="comments__item">
                          <div class="comments__item-inner md:direction-column">
                            <div class="comments__img mr-20">
                              <div class="bg-image rounded-full js-lazy" data-bg="la-assets/coursefinder/img/blog-list/avatar.png"></div>
                            </div>

                            <div class="comments__body md:mt-15">
                              <div class="comments__header">
                                <h4 class="text-17 fw-500 lh-15">
                                  Ali Tufan
                                  <span class="text-13 text-light-1 fw-400">3 Days ago</span>
                                </h4>

                                <div class="stars"></div>
                              </div>

                              <h5 class="text-15 fw-500 mt-15">The best LMS Design</h5>
                              <div class="comments__text mt-10">
                                <p>This course is a very applicable. Professor Ng explains precisely each algorithm and even tries to give an intuition for mathematical and statistic concepts behind each algorithm. Thank you very much.</p>
                              </div>

                              <div class="comments__helpful mt-20">
                                <span class="text-13 text-purple-1">Was this review helpful?</span>
                                <button class="button text-13 -sm -purple-1 text-white">Yes</button>
                                <button class="button text-13 -sm -light-7 text-purple-1">No</button>
                              </div>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>

                  <div class="respondForm pt-30">
                    <h3 class="text-20 fw-500">
                      Write a Review
                    </h3>

                    <div class="mt-30">
                      <h4 class="text-16 fw-500">What is it like to Course?</h4>
                      <div class="d-flex x-gap-10 pt-10">
                        <div class="icon-star text-14 text-yellow-1"></div>
                        <div class="icon-star text-14 text-yellow-1"></div>
                        <div class="icon-star text-14 text-yellow-1"></div>
                        <div class="icon-star text-14 text-yellow-1"></div>
                        <div class="icon-star text-14 text-yellow-1"></div>
                      </div>
                    </div>

                    <form class="contact-form respondForm__form row y-gap-30 pt-30" action="blog-single.html#">
                      <div class="col-12">
                        <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Review Title</label>
                        <input type="text" name="title" placeholder="Great Courses">
                      </div>
                      <div class="col-12">
                        <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Review Content</label>
                        <textarea name="comment" placeholder="Message" rows="8"></textarea>
                      </div>
                      <div class="col-12">
                        <button type="submit" name="submit" id="submit" class="button -md -purple-1 text-white">
                          Submit Review
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div> -->
            </div>
          </div>
        </section>

        <section class="layout-pt-lg layout-pb-lg bg-light-4">
          <div data-anim-wrap class="container">
            <div data-anim-child="slide-up delay-1" class="row justify-center text-center">
              <div class="col-auto">

                <div class="sectionTitle ">

                  <h2 class="sectionTitle__title ">Related Posts</h2>

                  <p class="sectionTitle__text ">Enhance Career Opportunities</p>

                </div>

              </div>
            </div>

            <div class="row y-gap-30 pt-60">

              <div class="col-lg-3 col-md-6">
                <div data-anim-child="slide-up delay-2" class="blogCard -type-1">
                  <div class="blogCard__image">
                    <img src="la-assets/coursefinder/img/blog-list/5.png" alt="image">
                  </div>
                  <div class="blogCard__content mt-20">
                    <a href="blog-single.html" class="blogCard__category">EDUCATION</a>
                    <h4 class="blogCard__title text-17 lh-15 mt-5">Eco-Education in Our Lives: We Can Change the Future</h4>
                    <div class="blogCard__date text-14 mt-5">November 20, 2024</div>
                  </div>
                </div>
              </div>

              <div class="col-lg-3 col-md-6">
                <div data-anim-child="slide-up delay-3" class="blogCard -type-1">
                  <div class="blogCard__image">
                    <img src="la-assets/coursefinder/img/blog-list/2.png" alt="image">
                  </div>
                  <div class="blogCard__content mt-20">
                    <a href="blog-single.html" class="blogCard__category">EDUCATION</a>
                    <h4 class="blogCard__title text-17 lh-15 mt-5">Engendering a culture of professional development</h4>
                    <div class="blogCard__date text-14 mt-5">August 16, 2022</div>
                  </div>
                </div>
              </div>

              <div class="col-lg-3 col-md-6">
                <div data-anim-child="slide-up delay-4" class="blogCard -type-1">
                  <div class="blogCard__image">
                    <img src="la-assets/coursefinder/img/blog-list/3.png" alt="image">
                  </div>
                  <div class="blogCard__content mt-20">
                    <a href="blog-single.html" class="blogCard__category">EDUCATION</a>
                    <h4 class="blogCard__title text-17 lh-15 mt-5">Increasing engagement with Instagram</h4>
                    <div class="blogCard__date text-14 mt-5">May 10, 2022</div>
                  </div>
                </div>
              </div>

              <div class="col-lg-3 col-md-6">
                <div data-anim-child="slide-up delay-5" class="blogCard -type-1">
                  <div class="blogCard__image">
                    <img src="la-assets/coursefinder/img/blog-list/4.png" alt="image">
                  </div>
                  <div class="blogCard__content mt-20">
                    <a href="blog-single.html" class="blogCard__category">EDUCATION</a>
                    <h4 class="blogCard__title text-17 lh-15 mt-5">How to design a simple, yet unique and memorable brand identity</h4>
                    <div class="blogCard__date text-14 mt-5">October 14, 2023</div>
                  </div>
                </div>
              </div>


            </div>
          </div>
        </section>

        <footer class="footer -type-1 bg-dark-1 -green-links">
          <div class="container">
            
            <div class="py-30 border-top-light-15">
              <div class="row justify-between items-center y-gap-20">
                <div class="col-auto">
                  <div class="d-flex items-center h-100 text-white">
                    © 2022 ZODOCS. All Right Reserved.
                  </div>
                </div>

                <div class="col-auto">
                  <div class="d-flex x-gap-20 y-gap-20 items-center flex-wrap">
                    <div>Powered By
                      <a href="http://demo.zodocs.com/" class="text-white" target="_blank">
                        <span class="text-15">ZODOCS</span>
                      </a>. Solution By
                      <a href="https://www.labgex.com/" class="text-white" target="_blank">
                        <span class="text-15">LabGex Innovations (P) Ltd</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>


      </div>
    </main>
  </div>
  <!-- barba container end -->

  <!-- JavaScript -->
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
  <script src="la-assets/coursefinder/js/vendors.js"></script>
  <script src="la-assets/coursefinder/js/main.js"></script>

</body>

</html>