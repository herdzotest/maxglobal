<div id="tab_10" class="tab-pane " >
        
<div class="content-head">
                                                <h4>Deployment Details </h4>
                                                <!-- <div class="id">Nurse ID : <b>#12675</b></div> -->
                                </div>
                                {!! Form::open(['action' => 'LA\NursesController@store', 'id' => '']) !!}
               
                                <div class="pane">
                                        <div class="row">
											<div class="col s6">
												<div class="input-field">
														<input name="joining_date" id="joining_date" class="datepicker" type="text">												
														<label for="joining_date">Joining Date </label>
												</div>
											</div>
                                        </div>
										<div class="row">
											<div class="col s12"><h2>Rebate period </h2></div>
										</div>
                                        <div class="row">
												<div class=" col s6 ">
														<div class="input-field">
															<input name="rebate_period_inp" id="rebate_period_inp" type="text" maxlength="100">												
														</div>
												</div>
                                                <div class="col s6">
													<div class="input-field">											
														<select name="rebate_period_select" id="rebate_period_select">
															<option value="3-days">3 days</option>
															<option value="3-months">3 months</option>
														</select>											
													</div>
                                                </div>
                                        </div>	
							
                                </div>									
								
                                <div class="controls">
                                                <a class="mgbtn2">Back</a>
                                                <input id="form_name" type="hidden" class="validate" name="type_of" value="joining">
                                                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                                                {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
                        

</div>

                