<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use Dwij\Laraadmin\Helpers\LAHelper;
use Dwij\Laraadmin\Models\LAConfigs;

use App\User;
use App\Models\Employee;
use App\Role;
use Mail;
use Log;

class SyllabusController extends Controller
{
	public function __construct() {
		
		if(Auth::user()->type != 'SUPER_ADMIN' && Auth::user()->type != 'MANAGER')
			return abort(403);		
	}
	
	/**
	 * Display a listing of the Employees.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$syllabus = DB::table('syllabus')->orderBy('id')->get();
		return View('la.syllabus.index', [
			'syllabus' => $syllabus
		]);
	}

	/**
	 * Store a newly created employee in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(count($request->all()) > 0) {
			
			$postedData = $request->all();
			$syllabus = DB::table('syllabus')->find($postedData['syllabus_id']);

			if($syllabus) {
				DB::table('syllabus')->where('id', $postedData['syllabus_id'])
					->update([
						'name' => $postedData['name'],
						'status' => $postedData['status']
					]);
			} else {			
				DB::table('syllabus')->insert([
					'name' => $postedData['name'],
					'status' => $postedData['status']
				]);
			}
		}
		
		return redirect(config('laraadmin.adminRoute')."/syllabus");
	}

	/**
	 * Remove the specified employee from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if($id != '') {
			$syllabus = DB::table('syllabus')->find($id);
			if($syllabus) {
				DB::table('syllabus')->where('id', '=', $id)->delete();
			}
		}
		
		return redirect(config('laraadmin.adminRoute')."/syllabus");
	}
	
}
