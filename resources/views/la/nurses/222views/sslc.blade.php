<div id="tab_3" class="tab-pane " >
                                
                                <div class="content-head">
                                                <h4>Academics</h4>
                                                <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                                        </div>
                        <div class="pane">
        <div class="academic">
                       <h3>GCSE/10th Board </h3>
                                   
                 <div class="mgv-field">
                        <div class="name">
                        Name of Institution
                        </div>
                        <div class="value" id="board_name"></div>
                 </div>
                 
                 <div class="mgv-field">
                        <div class="name">
                                 Country
                        </div>
                        <div class="value" id="country_sslc"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                                 From
                        </div>
                        <div class="value" id="start_year"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                                 To
                        </div>
                        <div class="value" id="end_year"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                        Mode of study 
                        </div>
                        <div class="value" id="mode_study_school"></div>
                 </div>
                 
                 <div class="mgv-field">
                        <div class="name"> Qualification   
                        </div>
                        <div class="value" id="qualification"></div>
                 </div>                 
                 <div class="mgv-field">
                        <div class="name"> Result/Grade    
                        </div>
                        <div class="value" id="grade"></div>
                 </div>
                                  
                 <div class="mgv-field">
                        <div class="name">  Medium Of Instruction     
                        </div>
                        <div class="value" id="medium_of_instr"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">Marks obtained in english(%)     
                        </div>
                        <div class="value" id="eng_score"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">Documents</div>
                        <div class="value uploaded-file" id="document" data-type="file"></div>
						<div class="file-approve-buttons hide">
							<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="sslc-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
							<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="sslc-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
						</div>   
                 </div>
        </div>
		
        <div class="academic">
                       <h3> A level/12th Board  </h3>
                                   
                 <div class="mgv-field">
                        <div class="name">
                        Name of Institution
                        </div>
                        <div class="value" id="hse_board_name"></div>
                 </div>
                 
                 <div class="mgv-field">
                        <div class="name">
                                 Country
                        </div>
                        <div class="value" id="hse_country"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                                 Start Year
                        </div>
                        <div class="value" id="hse_start_year"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                             Completion Year
                        </div>
                        <div class="value" id="hse_end_year"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                        Mode of study 
                        </div>
                        <div class="value" id="hse_mode_study"></div>
                 </div>
                 
                 <div class="mgv-field">
                        <div class="name"> Qualification   
                        </div>
                        <div class="value" id="hse_qualification"></div>
                 </div>                 
                 <div class="mgv-field">
                        <div class="name"> Result/Grade    
                        </div>
                        <div class="value" id="hse_grade"></div>
                 </div>
                                  
                 <div class="mgv-field">
                        <div class="name">  Medium Of Instruction     
                        </div>
                        <div class="value" id="hse_medium_of_instr"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">Marks obtained in english(%)     
                        </div>
                        <div class="value" id="hse_eng_score"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">Documents</div>
                        <div class="value uploaded-file" id="hse_document" data-type="file"></div>
						<div class="file-approve-buttons hide">
							<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="hse-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
							<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="hse-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
						</div>	  
                 </div>				 
                         
        </div>  
        
        <div class="academic">
                       <h3>  Bachelors/UG   </h3>
                                   
                 <div class="mgv-field">
                        <div class="name">
                        Name of Institution
                        </div>
                        <div class="value" id="UG_board_name"></div>
                 </div>
                 
                 <div class="mgv-field">
                        <div class="name">
                                 Country
                        </div>
                        <div class="value" id="UG_country"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                                 Start Year
                        </div>
                        <div class="value" id="UG_start_year"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                                 Completion Year
                        </div>
                        <div class="value" id="UG_end_year"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                        Mode of study 
                        </div>
                        <div class="value" id="UG_mode_study"></div>
                 </div>
                 
                 <div class="mgv-field">
                        <div class="name"> Qualification   
                        </div>
                        <div class="value" id="UG_qualification"></div>
                 </div>                 
                 <div class="mgv-field">
                        <div class="name"> Result/Grade    
                        </div>
                        <div class="value" id="UG_grade"></div>
                 </div>
                                  
                 <div class="mgv-field">
                        <div class="name">  Medium Of Instruction     
                        </div>
                        <div class="value" id="UG_medium_of_instr"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">Marks obtained in english(%)     
                        </div>
                        <div class="value" id="UG_eng_score"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">Documents</div>
                        <div class="value uploaded-file" id="UG_document" data-type="file"></div>
						<div class="file-approve-buttons hide">
							<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="degree-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
							<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="degree-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
						</div>  
                 </div>                         
        </div>  
        
        <div class="academic">
                       <h3>  Masters/PG  </h3>
                                   
                 <div class="mgv-field">
                        <div class="name">
                        Name of Institution
                        </div>
                        <div class="value" id="PG_board_name"></div>
                 </div>
                 
                 <div class="mgv-field">
                        <div class="name">
                                 Country
                        </div>
                        <div class="value" id="PG_country"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                                 Start Year
                        </div>
                        <div class="value" id="PG_start_year"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                                 Completion Year
                        </div>
                        <div class="value" id="PG_end_year"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                        Mode of study 
                        </div>
                        <div class="value" id="PG_mode_study"></div>
                 </div>
                 
                 <div class="mgv-field">
                        <div class="name"> Qualification   
                        </div>
                        <div class="value" id="PG_qualification"></div>
                 </div>                 
                 <div class="mgv-field">
                        <div class="name"> Result/Grade    
                        </div>
                        <div class="value" id="PG_grade"></div>
                 </div>
                                  
                 <div class="mgv-field">
                        <div class="name">  Medium Of Instruction     
                        </div>
                        <div class="value" id="PG_medium_of_instr"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">Marks obtained in english(%)     
                        </div>
                        <div class="value" id="PG_eng_score"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">Documents</div>
                        <div class="value uploaded-file" id="PG_document" data-type="file"></div>
						<div class="file-approve-buttons hide">
							<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="master-doc" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
							<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="master-doc" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
						</div> 
                 </div>                           
        </div>  
	</div>
</div>
				