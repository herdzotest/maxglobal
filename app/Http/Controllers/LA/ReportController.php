<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;
use Dwij\Laraadmin\Helpers\LAHelper;
use Dwij\Laraadmin\Models\LAConfigs;

use App\Models\Upload;
use App\User;
use App\Models\Employee;
use App\Role;
use Mail;
use Log;
use DateTime;


class ReportController extends Controller
{
	public $view_col = 'name';
	public $listing_cols = ['id', 'name','mobile', 'email', 'city', 'date_birth'];
	
	public function __construct() {

		$path = \Request::path();
		$userTypeAccess = array('SUPER_ADMIN','MANAGER','STUDENT','CONSULTANTS','UNIVERSITY_USER','MAIN_AGENTS','SUBCONTRACTORS');
		if ($path=="registration" || $path=="registration_save" || \Request::is('user/verify/*')) {
			
		} else if(!in_array(Auth::user()->type, $userTypeAccess) || (Auth::user()->type == 'STUDENT' && $path == 'admin/students')) {
			return abort(403);	
		}
	}

	public function index()
	{			
		$consultants = DB::table('employees')->select('employees.name')->addselect('users.id')->addselect('employees.mobile')
						->leftJoin('users', 'users.context_id', '=', 'employees.id')
						->where('employees.nurse-user', 0)
						->where('employees.role', 3)->whereNull('employees.deleted_at')->get();

		$docstaffs = '';
		if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER","CONSULTANTS","MAIN_AGENTS"])) {
			$docstaffs = DB::table('employees')->select('employees.name')->addselect('users.id')->addselect('employees.mobile')
						->leftJoin('users', 'users.context_id', '=', 'employees.id')
						->where('employees.role', 5)->whereNull('employees.deleted_at')->get();
		}

		$universityAgents = DB::table('employees')->select('employees.name')->addselect('users.id')
						->leftJoin('users', 'users.context_id', '=', 'employees.id')
						->where('employees.role', 5)->whereNull('employees.deleted_at')->get();

		$agents = DB::table('users')->select('name')->addselect('id')->where('type', 'MAIN_AGENTS')->get();
		$countries = DB::table('countries')->select('country')->where('status', 1)->get();
		$university = DB::table('universities')->select('university')->where('status', 1)->get();
		$stdstatus = DB::table('studentstatus')->select('student_status')->where('status', 1)->get();
		$stdcounselor = DB::table('employees')->select('employees.name')->addselect('users.id')
						->leftJoin('users', 'users.context_id', '=', 'employees.id')
						->where('employees.role', 3)->whereNull('employees.deleted_at')->get();
		$stddocumentation = DB::table('employees')->select('employees.name')->addselect('users.id')
						->leftJoin('users', 'users.context_id', '=', 'employees.id')
						->where('employees.role', 5)->whereNull('employees.deleted_at')->get();
						
		return View('la.report.index', [
			'consultants' => $consultants,
			'universityAgents' => $universityAgents,
			'agents' => $agents,
			'countries' => $countries,
			'university' => $university,
			'stdstatus' => $stdstatus,
			'stdcounselor' => $stdcounselor,
			'stddocumentation' => $stddocumentation,
			'docstaffs' => $docstaffs
		]);
	}
  
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		if(isset($_GET['from_date'])){ $from_date = $_GET['from_date'];} else {$from_date = ''; }
		if(isset($_GET['to_date'])){ $to_date = $_GET['to_date']; } else {$to_date = '';}
		if(isset($_GET['agent_id'])){ $agent_id = $_GET['agent_id']; } else {$agent_id = ''; } 
		if(isset($_GET['course_country'])){ $course_country = $_GET['course_country']; } else {$course_country = ''; }
		if(isset($_GET['entry_date'])){ $entry_date = $_GET['entry_date']; } else {$entry_date = ''; } 
		if(isset($_GET['course_university_new'])){ $course_university_new = $_GET['course_university_new']; } else {$course_university_new = ''; } 
		$leadFrom = (isset($_GET['leadFrom']) && $_GET['leadFrom'] != '') ? $_GET['leadFrom'] : '';
		if(isset($_GET['std_status'])){ $std_status = $_GET['std_status']; } else {$std_status = ''; }
		if(isset($_GET['std_counselor'])){ $std_counselor = $_GET['std_counselor']; } else {$std_counselor = ''; }
		if(isset($_GET['std_docstaff'])){ $std_docstaff = $_GET['std_docstaff']; } else {$std_docstaff = ''; }
		$conditions = "";
		$currentUserId = Auth::user()->id;

		if(Auth::user()->type == 'CONSULTANTS') {
			$conditions .= " AND (u.assigned_to = $currentUserId) ";
		} else if(Auth::user()->type == 'MAIN_AGENTS') {
			$conditions .= " AND (sl.agent_id = $currentUserId) "; 			
		} else if(Auth::user()->type == 'UNIVERSITY_USER') {
			$conditions .= " AND (u.assigned_to_agents = $currentUserId) ";
		}

		// filter condition of agents, entry-date, course-country, leadFrom
		$filterJoins = "";

		/*if($from_date != '' && $to_date != '') {
			$conditions .= " AND (u.created_at  >= '$from_date 00:00:00') AND (u.created_at  <= '$to_date 00:00:00') ";
		}*/

		if ($from_date != '') { 
			$conditions .= " AND (u.created_at  >= '$from_date 00:00:00') "; 
		}

		if ($to_date != '') { 
			$conditions .= " AND (u.created_at < '$to_date 00:00:00') ";
		}

		if ($agent_id != '') {
			$conditions .= " AND (sl.agent_id = $agent_id) ";
		}
		if ($std_status != '') { 
			$conditions .= " AND (u.user_status = '$std_status') "; 
		}
		if ($std_counselor != '') { 
			$conditions .= " AND (u.assigned_to = $std_counselor) "; 
		}
		if ($std_docstaff != '') { 
			$conditions .= " AND (u.assigned_to_agents = $std_docstaff) "; 
		}
		if($leadFrom != '') {
			$conditions .= " AND (TRIM(sl.lead_from) = '$leadFrom') ";
		}		
		if ($entry_date != '' || $course_country != '' || $course_university_new != '') {
			$filterJoins .= " LEFT JOIN data_courses_applied_new AS cpr ON cpr.user_id = u.id  
							  LEFT JOIN data_courses_applied AS cap ON cap.user_id = u.id ";
			if($course_country != '') { 
				$conditions .=	" AND ( (TRIM(cpr.course_country_new) = '$course_country') OR (cpr.json_data LIKE '%$course_country%') OR (TRIM(cap.course_country) = '$course_country') OR (cap.json_data LIKE '%$course_country%') ) ";
			}
			if($entry_date != '') { 
				$conditions .=	" AND ( (TRIM(cpr.entry_date_new) = '$entry_date') OR (cpr.json_data LIKE '%$entry_date%') OR (TRIM(cap.entry_date) = '$entry_date') OR (cap.json_data LIKE '%$entry_date%') ) ";
			}
			if($course_university_new != '') { 
				$conditions .=	" AND ( (TRIM(cpr.course_university_new) = '$course_university_new') OR (cpr.json_data LIKE '%$course_university_new%')) ";
			}
		}

		$query = "SELECT u.id,u.email,u.created_at,u.assigned_to,u.is_approved,u.assigned_to_agents,u.user_status,
			dp.profile_image,dp.sur_name,dp.first_name,dp.dob,dp.mobile,dp.whatsapp,dp.coountry_code_whatsapp,dp.notify, 
			df.follow_date,df.follow_time,sl.lead_from,sl.agent,sl.lead_type FROM users AS u 
			LEFT JOIN data_personal_information AS dp ON dp.user_id = u.id 
			LEFT JOIN data_student_lead AS sl ON u.id = sl.user_id 
			LEFT JOIN data_follow_up AS df ON u.id = df.user_id
			$filterJoins 
			WHERE u.type = :type AND u.deleted_at IS NULL
			$conditions 
			ORDER BY u.created_at DESC";
		$students = DB::select($query, ['type' => 'STUDENT']);
		
		
		$out =  array();
		$out['data'] = array();
		$out['draw'] = count($students);
		$out['recordsTotal'] = count($students);
		$out['recordsFiltered'] = count($students);
		
		foreach($students as $key => $result) {
			
			$studentData = array();
			$studentData[] = $key+1;
			$avatatImg = ($result->profile_image == '') ? 'https://www.gravatar.com/avatar/ef1c169902a351837b6c7f8816cf9b5c.jpg?s=80&d=mm&r=g' : $result->profile_image;
			$studentData[] = '<a href="'.url(config('laraadmin.adminRoute') . '/students/'.$result->id).'"><img src="'.$avatatImg.'" alt=""/><span>'.$result->first_name.' '.$result->sur_name.'</span></a>';
			$studentData[] = $result->email;
			
			
			
			if($result->assigned_to === null) {
				$assigneeName = $profilePic = ''; $assigneeId = 0;
			} else {
				$assignedTo = $result->assigned_to;
				$assignee = DB::select( DB::raw("SELECT employees.id AS assigneeId,employees.name,employees.profilePic FROM users
								LEFT JOIN employees ON users.context_id=employees.id 
								WHERE users.id=$assignedTo") );

				if($assignee) {
					$assigneeName = $assignee[0]->name;
					$assigneeId = $assignee[0]->assigneeId;
					$profilePic = ($assignee[0]->profilePic == '') ? 'https://www.gravatar.com/avatar/ef1c169902a351837b6c7f8816cf9b5c.jpg?s=80&d=mm&r=g' : $assignee[0]->profilePic;
				}
			}
			if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER"])) {
				if(Auth::user()->type == 'MAIN_AGENTS') {
					$studentData[] = '';
				} else {
					$studentData[] = ($result->assigned_to != null) ? '<a target="_blank" class="mgt-con mx-assignee" href="'.url(config('laraadmin.adminRoute') . '/student/consultants/'.$assigneeId).'"><span>'.$assigneeName.'</span></a> ' : '';				
				}
			}



			// students status
			if($result->user_status == 'APPROVE') {
				$status = '<span  class="mg-status approved">Approved</span>';
			} else if($result->user_status == 'REJECT') {
				$status = '<span mg-status  class="mg-status rejected">Rejected</span>';
			} else {
				$statusStudent = ($result->user_status == '') ? 'Pending' : $result->user_status;
				$status = '<span class="mg-status pending">'.$statusStudent.'</span>';
			}
			$studentData[] = (string)$status;

			
			array_push($out['data'],$studentData);
		}
		
		$out = json_encode($out,true);
		
		return $out;
	}
		
}
