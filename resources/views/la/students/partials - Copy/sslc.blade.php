<div id="tab_3" class="tab-pane " >
                                
                                <div class="content-head">
                                                <h4>School / College & University education (from age 11, starting with the most recent) </h4>
                                                <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                                        </div>
                                        {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'school-add-form']) !!}
               
                        <div class="pane">
                                
								<h4 class="sub_title">GCSE / 10th Board</h4>
                                <div class="row">
									<div class=" col s6 ">
										<div class="input-field">
											<input id="board_name"  name="board_name" type="text" class="validate">
											<label for="board_name">Name of Institution</label>
										</div>
									</div>
									<div class=" col s6 ">
										<div class="input-field">
											<select name="coountry_code" id="coountry_code">
												<option value="India">India</option>
												<option value="UK">UK</option>
												<option value="Australia">Australia</option>
											</select>											
											<label for="country">Country</label>
										</div>
									</div>
                                </div>

                                <div class="row">
                                        <div class="col s6">
                                                <div class="input-field ">
                                                                <input id="start_year" name="start_year" type="text" class="validate datepicker">
                                                                <label for="start_year">Start Year</label>
                                                                <!-- <span class="helper-text" data-error="wrong"
                                                                data-success="right">As per Passport</span> -->
                                                        </div>
                                        </div>
                                        <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="end_year" name="end_year" type="text" class="validate datepicker">
                                                                        <label for="end_year">Completion Year</label>

                                                                </div>
                                        </div>
                                </div>
                                <div class="row">
									<div class=" col s6 ">
										<div class="input-field">
											<select name="mode_study" id="mode_study">
												<option value="Full-time">Full time</option>
												<option value="Part-time">Part time</option>
												<option value="Distance-learning">Distance Learning</option>
											</select>	
											<label for="mode_study">Mode of study</label>
										</div>
									</div>
									<div class=" col s6 ">
										<div class="input-field">
											<input id="qualification"  name="qualification" type="text" class="validate">										
											<label for="qualification">Qualification eg.BA(Hons)</label>
										</div>
									</div>
                                </div>								
                                <div class="row">
                                                <div class="col s4">
                                                                <div class="input-field ">
                                                                                <input id="sslcgrade" name="grade" type="text" class="validate">
                                                                                <label for="sslcgrade">Percentage / Grade Obtained</label>

                                                                        </div>
                                                </div>								
                                                <div class="col s4">
                                                        <div class="input-field ">
                                                                        <input id="language" name="medium_of_instr" type="text" class="validate">
                                                                        <label for="language">Medium of Instruction</label>

                                                                </div>
                                                </div>



                                                <div class="col s4">
                                                                <div class="input-field ">
                                                                                <input id="eng_score" name="eng_score" type="text" class="validate">
                                                                                <label for="eng_score">Marks Obtained in English</label>

                                                                        </div>
                                                </div>


                                </div>
                                <div class="row">
                                        <div class="col s6">
                                                <label>Upload Documents</label>
                                                <div class="file-field input-field">
                                                                <div class="btn">
                                                                        <span>Upload</span>
                                                                        <input type="file" multiple > <i class="fa fa-cloud-upload"></i>
                                                                </div>
                                                                <div class="file-path-wrapper">
                                                                        <input class="file-path validate" name="document" type="text" placeholder = "Upload file" >
                                                                </div>
                                                </div>
												<div class="uploaded-file" id="document" data-type="file"></div>	
                                        </div>
                                </div>

								<h4 class="sub_title">A level / 12th Board</h4>
                                <div class="row">
									<div class=" col s6 ">
										<div class="input-field">
											<input id="hse_board_name"  name="hse_board_name" type="text" class="validate">
											<label for="hse_board_name">Name of Institution</label>
										</div>
									</div>
									<div class=" col s6 ">
										<div class="input-field">
											<select name="hse_country" id="hse_country">
												<option value="India">India</option>
												<option value="UK">UK</option>
												<option value="Australia">Australia</option>
											</select>											
											<label for="hse_country">Country</label>
										</div>
									</div>
                                </div>

                                <div class="row">
                                        <div class="col s6">
                                                <div class="input-field ">
                                                                <input id="hse_start_year" name="hse_start_year" type="text" class="validate datepicker">
                                                                <label for="hse_start_year">Start Year</label>
                                                                <!-- <span class="helper-text" data-error="wrong"
                                                                data-success="right">As per Passport</span> -->
                                                        </div>
                                        </div>
                                        <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="hse_end_year" name="hse_end_year" type="text" class="validate datepicker">
                                                                        <label for="hse_end_year">Completion Year</label>

                                                                </div>
                                        </div>
                                </div>
                                <div class="row">
									<div class=" col s6 ">
										<div class="input-field">
											<select name="hse_mode_study" id="hse_mode_study">
												<option value="Full-time">Full time</option>
												<option value="Part-time">Part time</option>
												<option value="Distance-learning">Distance Learning</option>
											</select>	
											<label for="hse_mode_study">Mode of study</label>
										</div>
									</div>
									<div class=" col s6 ">
										<div class="input-field">
											<input id="hse_qualification"  name="hse_qualification" type="text" class="validate">										
											<label for="hse_qualification">Qualification eg.BA(Hons)</label>
										</div>
									</div>
                                </div>								
                                <div class="row">
                                                <div class="col s4">
                                                                <div class="input-field ">
                                                                                <input id="hse_sslcgrade" name="hse_grade" type="text" class="validate">
                                                                                <label for="sslcgrade">Percentage / Grade Obtained</label>

                                                                        </div>
                                                </div>								
                                                <div class="col s4">
                                                        <div class="input-field ">
                                                                        <input id="hse_language" name="hse_eng_score" type="text" class="validate">
                                                                        <label for="hse_language">Medium of Instruction</label>

                                                                </div>
                                                </div>



                                                <div class="col s4">
                                                                <div class="input-field ">
                                                                                <input id="hse_eng_score" name="hse_eng_score" type="text" class="validate">
                                                                                <label for="hse_eng_score">Marks Obtained in English</label>

                                                                        </div>
                                                </div>


                                </div>
                                <div class="row">
                                        <div class="col s6">
                                                <label>Upload Documents</label>
                                                <div class="file-field input-field">
                                                                <div class="btn">
                                                                        <span>Upload</span>
                                                                        <input type="file" multiple > <i class="fa fa-cloud-upload"></i>
                                                                </div>
                                                                <div class="file-path-wrapper">
                                                                        <input class="file-path validate" name="hse_document" type="text" placeholder = "Upload file" >
                                                                </div>
                                                </div>
												<div class="uploaded-file" id="hse_document" data-type="file"></div>	
                                        </div>
                                </div>								
							
								<h4 class="sub_title">Bachelors / UG</h4>
                                <div class="row">
									<div class=" col s6 ">
										<div class="input-field">
											<input id="UG_board_name"  name="UG_board_name" type="text" class="validate">
											<label for="UG_board_name">Name of Institution</label>
										</div>
									</div>
									<div class=" col s6 ">
										<div class="input-field">
											<select name="UG_country" id="UG_country">
												<option value="India">India</option>
												<option value="UK">UK</option>
												<option value="Australia">Australia</option>
											</select>											
											<label for="UG_country">Country</label>
										</div>
									</div>
                                </div>

                                <div class="row">
                                        <div class="col s6">
                                                <div class="input-field ">
                                                                <input id="UG_start_year" name="UG_start_year" type="text" class="validate datepicker">
                                                                <label for="UG_start_year">Start Year</label>
                                                        </div>
                                        </div>
                                        <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="UG_end_year" name="UG_end_year" type="text" class="validate datepicker">
                                                                        <label for="UG_end_year">Completion Year</label>

                                                                </div>
                                        </div>
                                </div>
                                <div class="row">
									<div class=" col s6 ">
										<div class="input-field">
											<select name="UG_mode_study" id="UG_mode_study">
												<option value="Full-time">Full time</option>
												<option value="Part-time">Part time</option>
												<option value="Distance-learning">Distance Learning</option>
											</select>	
											<label for="UG_mode_study">Mode of study</label>
										</div>
									</div>
									<div class=" col s6 ">
										<div class="input-field">
											<input id="UG_qualification"  name="UG_qualification" type="text" class="validate">										
											<label for="UG_qualification">Qualification eg.BA(Hons)</label>
										</div>
									</div>
                                </div>								
                                <div class="row">
                                                <div class="col s4">
                                                                <div class="input-field ">
                                                                                <input id="UG_sslcgrade" name="UG_grade" type="text" class="validate">
                                                                                <label for="sslcgrade">Percentage / Grade Obtained</label>

                                                                        </div>
                                                </div>								
                                                <div class="col s4">
                                                        <div class="input-field ">
                                                                        <input id="UG_language" name="UG_eng_score" type="text" class="validate">
                                                                        <label for="UG_language">Medium of Instruction</label>

                                                                </div>
                                                </div>



                                                <div class="col s4">
                                                                <div class="input-field ">
                                                                                <input id="UG_eng_score" name="UG_eng_score" type="text" class="validate">
                                                                                <label for="UG_eng_score">Marks Obtained in English</label>

                                                                        </div>
                                                </div>


                                </div>
                                <div class="row">
                                        <div class="col s6">
                                                <label>Upload Documents</label>
                                                <div class="file-field input-field">
                                                                <div class="btn">
                                                                        <span>Upload</span>
                                                                        <input type="file" multiple > <i class="fa fa-cloud-upload"></i>
                                                                </div>
                                                                <div class="file-path-wrapper">
                                                                        <input class="file-path validate" name="UG_document" type="text" placeholder = "Upload file" >
                                                                </div>
                                                </div>
												<div class="uploaded-file" id="UG_document" data-type="file"></div>	
                                        </div>
                                </div>	
											  
								<h4 class="sub_title">Masters / PG</h4>
                                <div class="row">
									<div class=" col s6 ">
										<div class="input-field">
											<input id="PG_board_name"  name="PG_board_name" type="text" class="validate">
											<label for="PG_board_name">Name of Institution</label>
										</div>
									</div>
									<div class=" col s6 ">
										<div class="input-field">
											<select name="PG_country" id="PG_country">
												<option value="India">India</option>
												<option value="UK">UK</option>
												<option value="Australia">Australia</option>
											</select>											
											<label for="PG_country">Country</label>
										</div>
									</div>
                                </div>

                                <div class="row">
                                        <div class="col s6">
                                                <div class="input-field ">
                                                                <input id="PG_start_year" name="PG_start_year" type="text" class="validate datepicker">
                                                                <label for="PG_start_year">Start Year</label>
                                                        </div>
                                        </div>
                                        <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="PG_end_year" name="PG_end_year" type="text" class="validate datepicker">
                                                                        <label for="PG_end_year">Completion Year</label>

                                                                </div>
                                        </div>
                                </div>
                                <div class="row">
									<div class=" col s6 ">
										<div class="input-field">
											<select name="PG_mode_study" id="PG_mode_study">
												<option value="Full-time">Full time</option>
												<option value="Part-time">Part time</option>
												<option value="Distance-learning">Distance Learning</option>
											</select>	
											<label for="PG_mode_study">Mode of study</label>
										</div>
									</div>
									<div class=" col s6 ">
										<div class="input-field">
											<input id="PG_qualification"  name="PG_qualification" type="text" class="validate">										
											<label for="PG_qualification">Qualification eg.BA(Hons)</label>
										</div>
									</div>
                                </div>								
                                <div class="row">
                                                <div class="col s4">
                                                                <div class="input-field ">
                                                                                <input id="PG_sslcgrade" name="PG_grade" type="text" class="validate">
                                                                                <label for="PG_sslcgrade">Percentage / Grade Obtained</label>

                                                                        </div>
                                                </div>								
                                                <div class="col s4">
                                                        <div class="input-field ">
                                                                        <input id="PG_language" name="PG_eng_score" type="text" class="validate">
                                                                        <label for="PG_language">Medium of Instruction</label>

                                                                </div>
                                                </div>



                                                <div class="col s4">
                                                                <div class="input-field ">
                                                                                <input id="PG_eng_score" name="PG_eng_score" type="text" class="validate">
                                                                                <label for="PG_eng_score">Marks Obtained in English</label>

                                                                        </div>
                                                </div>


                                </div>
                                <div class="row">
                                        <div class="col s6">
                                                <label>Upload Documents</label>
                                                <div class="file-field input-field">
                                                                <div class="btn">
                                                                        <span>Upload</span>
                                                                        <input type="file" multiple > <i class="fa fa-cloud-upload"></i>
                                                                </div>
                                                                <div class="file-path-wrapper">
                                                                        <input class="file-path validate" name="PG_document" type="text" placeholder = "Upload file" >
                                                                </div>
                                                </div>
												<div class="uploaded-file" id="PG_document" data-type="file"></div>	
                                        </div>
                                </div>												  


                        </div>
                        <div class="controls">
                                        <a class="mgbtn2">Back</a>
                                        <input id="form_name" type="hidden" class="validate" name="type_of" value="school">
                                        <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                        {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
                </div>

                