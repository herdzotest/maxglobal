<div id="tab_7" class="tab-pane " >
        
<div class="content-head">
                                                <h4>Section 7 </h4>
                                                <div class="id">Nurse ID : <b>#12675</b></div>
                                </div>
                                <div class="pane">
                                        <div class="row">
											<div class="col s6">
												<div class="input-field">											
													<input name="nmc_appl_status" id="nmc_appl_status" type="text">
													<label for="cbt_attempt_date">Visa Category </label>
												</div>
											</div>
                                        </div>
																	
                                        <div class="row">
                                                <div class="col s6">
                                                        <label>Application Form (NHS / Nursing Home) File Upload  </label>
														<div class="file-field input-field"></div>
														<div class="uploaded-file" id="appl_form" data-type="file"></div>
														<div class="file-approve-buttons hide">
															<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="appl_form" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
															<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="appl_form" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
														</div>																
                                                </div>
                                                <div class="col s6">
                                                        <label>References File Upload   </label>
														<div class="file-field input-field"></div>
														<div class="uploaded-file" id="refer_file" data-type="file"></div>
														<div class="file-approve-buttons hide">
															<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="refer_file" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
															<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="refer_file" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
														</div>															
                                                </div>
                                        </div>	
										
                                        <div class="row">
											<div class="col s6">
												<div class="input-field">
													<input name="pers_status" id="pers_status" type="text">
													<label for="pers_status">Personal Statement Status  </label>
												</div>
											</div>
                                        </div>
										
										<div id="personal-stat-wrapper" style="display:none;">
											<div class="row">
												<div class="col s12">
													<div class="input-field">
														<input name="pers_stat_text" id="pers_stat_text" type="text">
														<label for="pers_status">Personal Statement</label>
													</div>
												</div>
											</div>
									
											<div class="row">
												<div class="col s12">
													<label>Personal Statement File Upload   </label>
													<div class="file-field input-field"></div>
													<div class="uploaded-file" id="pers_stat_file" data-type="file"></div>
													<div class="file-approve-buttons hide">
														<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="pers_stat_file" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
														<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="pers_stat_file" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
													</div>														
												</div>
											</div>
										</div>
										
                                        <div class="row">
											<div class="col s6">
												<div class="input-field">
													<input name="pcc_status" id="pcc_status" type="text">
													<label for="pcc_status">Police Clearance Certificate</label>
												</div>
											</div>
                                        </div>
										
                                        <div class="row">										
											<div class="col s6 mx-pcc-recieved"  style="display:none;">
												<div class="input-field">
													<input name="pcc_recieved_date" id="pcc_recieved_date" type="text">
													<label for="pers_status">PCC Received Date </label>
												</div>
											</div>
											<div class="col s6">
												<div class="input-field">
													<input name="tb_test_date" id="tb_test_date" type="text">
													<label for="tb_test_date">Medical or TB Screening Test Date</label>
												</div>
											</div>																				
                                        </div>
								
                                        <div class="row">									
											<div class="col s6">
												<label>Medical or TB Screening Test File Upload</label>
												<div class="file-field input-field"></div>
												<div class="uploaded-file" id="tb_file_upload" data-type="file"></div>
												<div class="file-approve-buttons hide">
													<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="tb_file_upload" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
													<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="tb_file_upload" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
												</div>													
											</div>
											<div class="col s6 mx-pcc-recieved"  style="display:none;">
												<label>PCC / DBS File Upload   </label>
												<div class="file-field input-field"></div>
												<div class="uploaded-file" id="pcc_file_upload" data-type="file"></div>
												<div class="file-approve-buttons hide">
													<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="pcc_file_upload" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
													<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="pcc_file_upload" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
												</div>													
											</div>												
                                        </div>	

                                        <div class="row">
											<div class="col s6">
												<div class="input-field">
													<input name="spons_status" id="spons_status" type="text">
													<label for="spons_status">Certificate of Sponsorship Status </label>
												</div>
											</div>																			
                                        </div>
										<div class="row cos-wrapper" style="display:none;">
											<div class="col s6">
												<label>COS (certificate of sponsorship) File</label>
												<div class="file-field input-field"></div>
												<div class="uploaded-file" id="spons_file_upload" data-type="file"></div>
												<div class="file-approve-buttons hide">
													<a href="#" class="btn btn-danger pull-right mx-approve-doc" data-type="spons_file_upload" data-val="0" data-toggle="modal" data-target="#AddModal">REJECT</a>
													<a href="#" class="btn btn-success pull-right mx-approve-doc" data-type="spons_file_upload" data-val="1" data-toggle="modal" data-target="#AddModal">APPROVE</a>
												</div>													
											</div>	
											<div class="col s6"
												<div class="input-field">
													<input name="spons_date" id="spons_date" type="text">
													<label for="spons_date">COS (certificate of sponsorship) Received Date <label>
												</div>
										</div>												

                                </div>									
                        

</div>

                