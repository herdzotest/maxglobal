<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use DB;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        // check whether the user email is verified / user account is enabled
        $isverified = Auth::user()->verified;
        if($isverified == '1' || Auth::user()->enable_disable == '0'){
            
           $message = ($isverified == '1') ? "Your email is not confirmed yet, please check your mail.." : "Your login is blocked due to one or more reasons, please contact administrator.";
            Auth::logout();

            return redirect('/login')->with('loginfail', $message);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {   
        $std_counselor = ''; $std_agent = ''; $std_unv_user = '';
        $students_count = '0'; $nurses_count = '0'; $students_approved_count = '0'; $students_pending_count = '0'; 
        $agents_active_count = '0'; $agents_inactive_count = '0'; $subcontractors_active_count = '0'; $subcontractors_inactive_count = '0';
        $counselor_active_count = '0'; $counselor_inactive_count = '0';   $employers_active_count = '0'; $employers_inactive_count ='0';
        $documentation_active_count = '0'; $documentation_inactive_count = '0';
        if(in_array(Auth::user()->type, ['SUPER_ADMIN','MANAGER','CONSULTANTS','MAIN_AGENTS','UNIVERSITY_USER','SUBCONTRACTORS','EMPLOYER_ROLE'])) {
            $user = DB::table('employees')->select('employees.name')->addselect('employees.mobile')->addselect('employees.profilePic')->addselect('employees.nurse-user AS nurseuser')
                        ->leftJoin('users', 'users.context_id', '=', 'employees.id')
                        ->where('users.id', Auth::user()->id)->get();
        } else {

            if(Auth::user()->type == 'STUDENT') {

            $user = DB::table('data_personal_information')->select('data_personal_information.first_name')->addselect('data_personal_information.mobile')->addselect('users.assigned_to')->addselect('data_student_lead.agent_id')->addselect('data_follow_up.follow_date')->addselect('data_follow_up.follow_time')->addselect('users.user_status')->addselect('data_courses_applied_new.course_university_new')->addselect('data_courses_applied_new.json_data AS courses_applied_json')->addselect('users.assigned_to_agents')
            ->leftJoin('users', 'users.id', '=', 'data_personal_information.user_id')
            ->leftJoin('data_student_lead', 'data_student_lead.user_id', '=', 'data_personal_information.user_id')
            ->leftJoin('data_follow_up', 'data_follow_up.user_id', '=', 'users.id')
            ->leftJoin('data_courses_applied', 'data_courses_applied.user_id', '=', 'users.id')
            ->leftJoin('data_courses_applied_new', 'data_courses_applied_new.user_id', '=', 'users.id')
            ->where('users.id', Auth::user()->id)->get();
            if($user[0]->assigned_to != '') {
                $std_counselor = DB::table('employees')->select('employees.name')->addselect('employees.mobile')->addselect('employees.profilePic')->addselect('users.email')
                ->leftJoin('users', 'users.context_id', '=', 'employees.id')           
                ->where('users.id', $user[0]->assigned_to)->get(); 
            } else {
                $std_counselor = '';
            }
            if($user[0]->assigned_to_agents != '') {
                $std_unv_user = DB::table('employees')->select('employees.name')->addselect('employees.mobile')->addselect('employees.profilePic')->addselect('users.email')
                ->leftJoin('users', 'users.context_id', '=', 'employees.id')           
                ->where('users.id', $user[0]->assigned_to_agents)->get(); 
            } else {
                $std_unv_user = '';
            }
            if($user[0]->agent_id != '') {
                $std_agent = DB::table('employees')->select('employees.name')->addselect('employees.mobile')->addselect('employees.profilePic')->addselect('users.email')
                ->leftJoin('users', 'users.context_id', '=', 'employees.id')           
                ->where('users.id', $user[0]->agent_id)->get(); 
            } else {
                $std_agent = '';
            }

            }

            else if(Auth::user()->type == 'NURSE') {
                $user = DB::table('nurse_personal_information')->select('nurse_personal_information.first_name')->addselect('nurse_personal_information.mobile')->addselect('users.assigned_to')->addselect('users.user_status')->addselect('users.assigned_to_agents')
                ->leftJoin('users', 'users.id', '=', 'nurse_personal_information.user_id')
                ->where('users.id', Auth::user()->id)->get();
            }

        }

        if(in_array(Auth::user()->type, ['SUPER_ADMIN','MANAGER'])) {
            $students = DB::table('users')->select('users.id')
            ->where('users.type', 'STUDENT')->whereNull('deleted_at')->get(); 
            if($students){ $students_count = count($students);}

            $nurses = DB::table('users')->select('users.id')
            ->where('users.type', 'NURSE')->whereNull('deleted_at')->get(); 
            if($nurses){ $nurses_count = count($nurses); }

            $counselor_active = DB::table('employees')->select('employees.id')
            ->leftJoin('users', 'users.context_id', '=', 'employees.id')
            ->where('employees.role', 3)->where('users.enable_disable', 1)->whereNull('employees.deleted_at')->get(); 
            if($counselor_active){ $counselor_active_count = count($counselor_active); }

            $counselor_inactive = DB::table('employees')->select('employees.id')
            ->leftJoin('users', 'users.context_id', '=', 'employees.id')
            ->where('employees.role', 3)->where('users.enable_disable', 0)->whereNull('employees.deleted_at')->get(); 
            if($counselor_inactive){ $counselor_inactive_count = count($counselor_inactive); }

            $documentation_active = DB::table('employees')->select('employees.id')
            ->leftJoin('users', 'users.context_id', '=', 'employees.id')
            ->where('employees.role', 5)->where('users.enable_disable', 1)->whereNull('employees.deleted_at')->get(); 
            if($documentation_active){ $documentation_active_count = count($documentation_active); }

            $documentation_inactive = DB::table('employees')->select('employees.id')
            ->leftJoin('users', 'users.context_id', '=', 'employees.id')
            ->where('employees.role', 5)->where('users.enable_disable', 0)->whereNull('employees.deleted_at')->get(); 
            if($documentation_inactive){ $documentation_inactive_count = count($documentation_inactive); }

            $agents_active = DB::table('employees')->select('employees.id')
            ->leftJoin('users', 'users.context_id', '=', 'employees.id')
            ->where('employees.role', 4)->where('users.enable_disable', 1)->whereNull('employees.deleted_at')->get(); 
            if($agents_active){ $agents_active_count = count($agents_active); }

            $agents_inactive = DB::table('employees')->select('employees.id')
            ->leftJoin('users', 'users.context_id', '=', 'employees.id')
            ->where('employees.role', 4)->where('users.enable_disable', 0)->whereNull('employees.deleted_at')->get(); 
            if($agents_inactive){ $agents_inactive_count = count($agents_inactive); }

            $subcontractors_active = DB::table('employees')->select('employees.id')
            ->leftJoin('users', 'users.context_id', '=', 'employees.id')
            ->where('employees.role', 6)->where('users.enable_disable', 1)->whereNull('employees.deleted_at')->get(); 
            if($subcontractors_active){ $subcontractors_active_count = count($subcontractors_active); }

            $subcontractors_inactive = DB::table('employees')->select('employees.id')
            ->leftJoin('users', 'users.context_id', '=', 'employees.id')
            ->where('employees.role', 6)->where('users.enable_disable', 0)->whereNull('employees.deleted_at')->get(); 
            if($subcontractors_inactive){ $subcontractors_inactive_count = count($subcontractors_inactive); }

            $employers_active = DB::table('employees')->select('employees.id')
            ->leftJoin('users', 'users.context_id', '=', 'employees.id')
            ->where('employees.role', 7)->where('users.enable_disable', 1)->whereNull('employees.deleted_at')->get(); 
            if($employers_active){ $employers_active_count = count($employers_active); }

            $employers_inactive = DB::table('employees')->select('employees.id')
            ->leftJoin('users', 'users.context_id', '=', 'employees.id')
            ->where('employees.role', 7)->where('users.enable_disable', 0)->whereNull('employees.deleted_at')->get(); 
            if($employers_inactive){ $employers_inactive_count = count($employers_inactive); }

        }

        if(Auth::user()->type == 'CONSULTANTS') {
            $students = DB::table('users')->select('users.id')
                ->where('users.type', 'STUDENT')->where('users.assigned_to',Auth::user()->id)->whereNull('deleted_at')->get();
            if($students){ $students_count = count($students); } 

            $nurses = DB::table('users')->select('users.id')
                ->where('users.type', 'NURSE')->where('users.assigned_to',Auth::user()->id)->whereNull('deleted_at')->get();
            if($nurses){ $nurses_count = count($nurses); } 
        } 
        if(Auth::user()->type == 'UNIVERSITY_USER') {
            $students = DB::table('users')->select('users.id')
                ->where('users.type', 'STUDENT')->where('users.assigned_to_agents',Auth::user()->id)->whereNull('deleted_at')->get();
            if($students){ $students_count = count($students); }
        }
        if(Auth::user()->type == 'MAIN_AGENTS') {
            $students = DB::table('data_student_lead')->select('data_student_lead.id')
            ->leftJoin('users', 'users.id', '=', 'data_student_lead.user_id')
            ->where('data_student_lead.agent_id',Auth::user()->id)->whereNull('users.deleted_at')->get();
            if($students){ $students_count = count($students); }

            $students_approved = DB::table('data_student_lead')->select('data_student_lead.id')
            ->leftJoin('users', 'users.id', '=', 'data_student_lead.user_id')
            ->where('data_student_lead.agent_id',Auth::user()->id)->where('users.user_status','Approve')->get();
            if($students_approved){ $students_approved_count = count($students_approved); }

            $students_pending = DB::table('data_student_lead')->select('data_student_lead.id')
            ->leftJoin('users', 'users.id', '=', 'data_student_lead.user_id')
            ->where('data_student_lead.agent_id',Auth::user()->id)->where('users.user_status','Pending')->get();
            if($students_pending){ $students_pending_count = count($students_pending); }
        }

        return View('la.dashboard', [
            'user' => $user,
            'std_counselor' => $std_counselor,
            'std_unv_user' => $std_unv_user,
            'std_agent' => $std_agent,
            'students' => $students_count,
            'nurses' => $nurses_count,
            'students_approved' => $students_approved_count,
            'students_pending' => $students_pending_count,
            'counselor_active' => $counselor_active_count,
            'counselor_inactive' => $counselor_inactive_count,
            'documentation_active' => $documentation_active_count,
            'documentation_inactive' => $documentation_inactive_count,
            'agent_active' => $agents_active_count,
            'agent_inactive' => $agents_inactive_count,
            'subcontractors_active' => $subcontractors_active_count,
            'subcontractors_inactive' => $subcontractors_inactive_count,
            'employers_active' => $employers_active_count,
            'employers_inactive' => $employers_inactive_count
        ]);

        //return view('la.dashboard');
    }
}