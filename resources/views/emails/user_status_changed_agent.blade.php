Dear {{ $agent }},<br><br>

The application form status of {{ $user }} has been changed to status <b>{{ $status }}.</b><br><br>

You can login to <a href="{{ url('/userlogin') }}">{{ str_replace("http://", "", url('/userlogin')) }}</a>, and check the details.<br><br>

Best Regards,<br>
Zodocs
