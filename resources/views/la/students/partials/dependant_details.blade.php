<div id="tab_7" class="tab-pane" >
        
                   
<div class="content-head">
                                        
                                        <h4>Dependant details</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                        </div>
                        {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'personal-add-form']) !!}

                <div class="pane">
					<div class="row">
						<div class=" col s4 ">
							<div class="input-field">
								<select name="childrens" class="validate formSelect" id="childrens" required>
									<option value="No">No</option>
									<option value="Yes">Yes</option>
								</select>	
								<label for="childrens" style="margin-top: -10px;font-size: 13px;">Do you have children</label>
							</div>
						</div>
					</div>	
					<div id="childrens-wrapper" class="hide">					
						<h4 class="sub_title">Details of dependants</h4>
						<div class="stdhst">
								<div class="index">1</div>
								<div class="col-md-6">
									<div class="mg-input-field ">
										<label for="name">Name (First Name + Family Name)</label>
										<input class="form-control" id="name" name="name" type="text">										
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="mg-input-field ">
										<label for="birthdate">Date of birth</label>
										<input class="form-control datepicker" id="birthdate" name="birthdate" type="text">								
									</div>
								</div>
								<div class="col-md-6">
										<div class="mg-input-field ">
												<label for="from_date">Relationship to you?</label>
												<select name="relation" id="relation" class="formSelect">
													<option value="Son">Son</option>
													<option value="Daughter">Daughter</option>
												</select>												
										</div>
								</div>
								<div class="col-md-6">
										<div class="mg-input-field ">
												<label for="current_live">Does this person currently live with you?</label>
												<select name="current_live" id="current_live" class="formSelect">
													<option value="Yes">Yes</option>
													<option value="No">No</option>
												</select>	
										</div>
								</div>

								<div class="col-md-6">
										<div class="mg-input-field ">
												<label for="travel_to_uk">Is this person travelling with you to the UK?</label>
												<select name="travel_to_uk" id="travel_to_uk" class="formSelect">
													<option value="Yes">Yes</option>
													<option value="No">No</option>
												</select>	
										</div>
								</div>	
								
								<div class="clearfix"></div>
						</div>
						
						<!-- <input type="hidden" value="children" name="formtype" class="formtype"> -->
						<!-- <div class="btn addshbtn"> <span class="glyphicon glyphicon-plus"></span></div> -->
					</div>

					<div class="btn addshbtn"> <span class="glyphicon glyphicon-plus"></span></div>
					
                </div>
				<!-- <input type="hidden" value="1" class="wrapper-count"> -->
				<input type="hidden" value="<?php echo $wrp_count_dependant_det; ?>" class="wrapper-count">
				<input type="hidden" value="children" name="formtype" class="formtype">

                <div class="controls">
                                <a class="mgbtn2 back_button">Back</a>
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="dependant_det">
                                 <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                              
                                {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                        </div>
                        {{ Form::close() }}
</div>


