<div id="tab_5" class="tab-pane " >

        
<div class="content-head">
                                        
                                        <h4>Bachelor Degree Details</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                                </div>
                                {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'school-add-form']) !!}
               
                <div class="pane">
                        
                        <div class="row">
                                <div class=" col s12 ">


                                        <p>
                                                Have you completed your Bachelor's Degree?
                                        </p>
                                        <p>
                                                        <label>
                                                                        <input name="degreedone" type="radio" value="1" checked/>
                                                                        <span>Yes</span>
                                                                      </label>
                                                                      <label>
                                                                                <input name="degreedone" type="radio"  value="0" />
                                                                                <span>No</span>
                                                                              </label>
                                                      </p>
                                                      <br>








                                </div>

                        </div>
                        <div id="degree-details" class="degreedone">
                        <div class="row">
                                        <div class=" col s12 ">
                                                        <div class="input-field">
                                                                        <!-- <i class="material-icons prefix">account_circle</i> -->
                                                                        <input id="bachelor_board_name" name="bachelor_board_name" type="text" class="validate">
                                                                        <label for="bachelor_board_name">University Name</label>
                                                                        <!-- <span class="helper-text" data-error="wrong"
                                                                        data-success="right">As per Passport</span> -->
                                                        </div>
                                        </div>
                        </div>
                        <div class="row">
                                <div class="col s6">
                                        <div class="input-field ">
                                                        <input id="start_year_degree" name="start_year_degree" type="text" class="validate" value="">
                                                        <label for="start_year_degree">Start Year</label>
                                                        <!-- <span class="helper-text" data-error="wrong"
                                                        data-success="right">As per Passport</span> -->
                                                </div>
                                </div>

                                <div class="col s6">
                                                <div class="input-field ">
                                                                <input id="end_year_degree" name="end_year_degree" type="text" class="validate">
                                                                <label for="end_year_degree">Completion Year</label>

                                                        </div>
                                </div>


                        </div>
                        <div class="row">
                                        <div class="col s6">
                                                <div class="input-field ">
                                                                <input id="language_degree" name="language_degree" type="text" class="validate">
                                                                <label for="language_degree">Medium of Instruction</label>

                                                        </div>
                                        </div>

                                        <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="grade_degree" name="grade_degree"  type="text" class="validate">
                                                                        <label for="grade_degree">Percentage / Grade Obtained</label>

                                                                </div>
                                        </div>

                                        <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="eng_score_degree" name="eng_score_degree" type="text" class="validate">
                                                                        <label for="eng_score_degree">Marks Obtained in English</label>

                                                                </div>
                                        </div>


                                </div>
                        <br>
                        <div class="row">
                                <div class="col s6">
                                        <label>Upload Documents</label>
                                        <div class="file-field input-field">
                                                        <div class="btn">
                                                                <span>Upload</span>
                                                                <input type="file" multiple > <i class="fa fa-cloud-upload"></i>
                                                        </div>
                                                        <div class="file-path-wrapper">
                                                                <input class="file-path validate" name="degree_doc" type="text" placeholder = "Upload file" >
                                                        </div>
                                        </div>
										<div class="uploaded-file" id="degree_doc" data-type="file"></div>
                                </div>


                                      </div>


                                </div>


                </div>
                <div class="controls">
                                <a class="mgbtn">Back</a>

                                <input id="form_name" type="hidden" class="validate" name="type_of" value="bachelors">
                                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                                                {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
        
        
</div>