<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Google fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Outlined" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />

  <!-- Stylesheets -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/vendors.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/main.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/custom.css">

  <title>Course Finder</title>
  <link rel="icon" type="image/x-icon" href="la-assets/coursefinder/img/coursesCards/icons/zodocs_fav.png">
</head>

<body class="preloader-visible" data-barba="wrapper">

  <!-- preloader start -->
  <div class="preloader js-preloader">
    <div class="preloader__bg"></div>
  </div>
  <!-- preloader end -->

  <!-- barba container start -->
  <div class="barba-container" data-barba="container">


    <main class="main-content  ">

      <header data-anim="fade" data-add-bg="bg-white" class="header -type-4 js-header">


        <div class="header__container py-5 border-bottom-dark">
          <div class="row justify-between items-center">

            <div class="col-auto">
              <div class="header-left d-flex items-center">

                <div class="header__logo pr-30 xl:pr-20 md:pr-0">
                  <a data-barba href="<?php echo url('/coursefinder'); ?>">
                    <img src="la-assets/coursefinder/img/general/logo.png" alt="logo">
                  </a>
                </div>


                <div class="header-menu js-mobile-menu-toggle ">
                  <div class="header-menu__content">
                    <div class="mobile-bg js-mobile-bg"></div>

                    <div class="d-none xl:d-flex items-center px-20 py-20 border-bottom-light">
                      <?php 
                      if(isset(Auth::user()->type)) { ?>
                        <a href="<?php echo url('/logout'); ?>" class="text-dark-1">Log out</a>
                        <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="text-dark-1 ml-30">My Account</a>
                      <?php } else { ?>
                        <a href="<?php echo url('/userlogin'); ?>" class="text-dark-1">Log in</a>
                        <a href="<?php echo url('/signup'); ?>" class="text-dark-1 ml-30">Sign up</a>
                      <?php } ?>
                    </div>

                    <div class="menu js-navList">
                      <ul class="menu__nav text-dark-1 -is-active">
                        <li class="menu-item-has-children">
                      <a data-barba href="#">Home</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Home</a>
                        </li>
                        <li><a href="<?php echo url('/coursefinder'); ?>">Home 1</a></li>
                        <li><a href="<?php echo url('/coursefinder2'); ?>">Home 2</a></li>
                        <li><a href="<?php echo url('/coursefinder3'); ?>">Home 3</a></li>
                        <li><a href="<?php echo url('/coursefinder4'); ?>">Home 4</a></li>
                        <li><a href="<?php echo url('/coursefinder5'); ?>">Home 5</a></li>
                        <li><a href="<?php echo url('/coursefinder6'); ?>">Home 6</a></li>
                        <li><a href="<?php echo url('/coursefinder7'); ?>">Home 7</a></li>
                        <li><a href="<?php echo url('/coursefinder8'); ?>">Home 8</a></li>
                        <li><a href="<?php echo url('/coursefinder9'); ?>">Home 9</a></li>
                        <li><a href="<?php echo url('/coursefinder10'); ?>">Home 10</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/aboutus'); ?>">
                        About Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li class="menu-item-has-children">
                      <a data-barba href="#">Courses</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Courses</a>
                        </li>
                        <li><a href="<?php echo url('/coursesearch'); ?>">Course 1</a></li>
                        <li><a href="<?php echo url('/coursesearch2'); ?>">Course 2</a></li>
                        <li><a href="<?php echo url('/coursesearch3'); ?>">Course 3</a></li>
                        <li><a href="<?php echo url('/coursesearch4'); ?>">Course 4</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/blog'); ?>">
                        Blog <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/topcourses'); ?>">
                        Top Courses <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/contactus'); ?>">
                        Contact Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>
                      </ul>
                    </div>
                  </div>

                  <div class="header-menu-close" data-el-toggle=".js-mobile-menu-toggle">
                    <div class="size-40 d-flex items-center justify-center rounded-full bg-white">
                      <i class="fa fa-close"></i>
                    </div>
                  </div>

                  <div class="header-menu-bg"></div>
                </div>

              </div>
            </div>


            <div class="col-auto">
              <div class="header-right d-flex items-center">
                <div class="header-right__icons text-white d-flex items-center">
                  <div class="d-none xl:d-block pl-30 sm:pl-15">
                    <button class="text-dark-1 items-center" data-el-toggle=".js-mobile-menu-toggle">
                      <i class="fa fa-bars"></i>
                    </button>
                  </div>

                </div>

                <div class="header-right__buttons d-flex items-center ml-30 xl:ml-20 lg:d-none">
                  <?php 
                  if(isset(Auth::user()->type)) { ?>
                    <a href="<?php echo url('/logout'); ?>" class="button -underline text-dark-1">Log out</a>
                    <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="button h-50 px-40 -purple-1 -rounded text-white ml-30 xl:ml-20">My Account</a>
                  <?php } else { ?>
                    <a href="<?php echo url('/userlogin'); ?>" class="button -underline text-dark-1">Log in</a>
                    <a href="<?php echo url('/signup'); ?>" class="button h-50 px-40 -purple-1 -rounded text-white ml-30 xl:ml-20">Sign up</a>
                  <?php } ?>
                </div>
              </div>
            </div>

          </div>
        </div>
      </header>


      <div class="content-wrapper  js-content-wrapper">

        <section data-anim-wrap class="masthead -type-5">
          <div class="masthead__bg pr-60 md:pr-0">
            <img src="la-assets/coursefinder/img/home-6/hero/bg.png" alt="image">
          </div>

          <div class="container">
            <div class="row y-gap-50 items-center justify-between">
              <div class="col-lg-6">
                <div class="masthead__content">
                  <h1 data-anim-child="slide-up delay-1" class="masthead__title">
                    Process Of Overseas  <br class="md:d-none">
                    Education Visa CRM <br class="md:d-none">
                    With <span class="text-purple-1 underline">Zodocs</span>
                  </h1>

                  <div data-anim-child="slide-up delay-2" class="row items-center x-gap-30 y-gap-20 pt-30">
                    <div class="col-auto">
                      <a href="<?php echo url('/coursesearch'); ?>" class="button -md -dark-1 -rounded text-white">Get Started</a>
                    </div>
                  </div>

                  <div data-anim-child="slide-up delay-3" class="row x-gap-20 y-gap-20 items-center pt-60 lg:pt-30">

                    <div class="col-xl-4">
                      <div class="d-flex items-center text-dark-1">
                        <div class="mr-10">
                          <img src="la-assets/coursefinder/img/home-6/hero/icons/1.svg" alt="icon">
                        </div>
                        <div class="fw-500 lh-1 pt-3">Over 12 million students</div>
                      </div>
                    </div>

                    <div class="col-xl-4">
                      <div class="d-flex items-center text-dark-1">
                        <div class="mr-10">
                          <img src="la-assets/coursefinder/img/home-6/hero/icons/2.svg" alt="icon">
                        </div>
                        <div class="fw-500 lh-1 pt-3">More than 60,000 courses</div>
                      </div>
                    </div>

                    <div class="col-xl-4">
                      <div class="d-flex items-center text-dark-1">
                        <div class="mr-10">
                          <img src="la-assets/coursefinder/img/home-6/hero/icons/3.svg" alt="icon">
                        </div>
                        <div class="fw-500 lh-1 pt-3">Empowering Global Students</div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>

              <div class="col-lg-6">
                <div class="composition -type-1 js-mouse-move-container">
                  <div class="-img-1">
                    <img class="js-mouse-move" data-move="40" src="la-assets/coursefinder/img/home-6/hero/2.png" alt="image">
                  </div>
                  <div class="-img-2">
                    <img class="js-mouse-move" data-move="40" src="la-assets/coursefinder/img/home-6/hero/1.png" alt="image">
                  </div>
                  <div class="-img-3">
                    <img class="js-mouse-move" data-move="40" src="la-assets/coursefinder/img/home-6/hero/3.png" alt="image">
                  </div>

                  <div data-move="30" class="-el-1 md:d-none js-mouse-move">
                    <div class="img-el -w-250 px-20 py-20 d-flex items-center bg-white rounded-8">
                      <div class="size-50 d-flex justify-center items-center bg-red-2 rounded-full">
                        <img src="la-assets/coursefinder/img/masthead/1.svg" alt="icon">
                      </div>
                      <div class="ml-20">
                        <div class="text-orange-1 text-16 fw-500 lh-1">3000 +</div>
                        <div class="mt-3">Free Courses</div>
                      </div>
                    </div>
                  </div>

                  <div data-move="60" class="-el-2 md:d-none js-mouse-move">
                    <div class="img-el -w-260 px-20 py-20 d-flex items-center bg-white rounded-8">
                      <img src="la-assets/coursefinder/img/masthead/ver.png" alt="icon">
                      <div class="ml-20">
                        <div class="text-dark-1 text-16 fw-500 lh-1">1000 +</div>
                        <div class="mt-3">Universities</div>
                      </div>
                    </div>
                  </div>

                  <div data-move="30" class="-el-3 md:d-none js-mouse-move">
                    <div class="img-el px-40 py-20 shadow-4 d-flex items-center bg-white rounded-8">
                      <div class="img-el__side">
                        <div class="size-50 d-flex justify-center items-center bg-purple-1 rounded-full">
                          <img src="la-assets/coursefinder/img/masthead/2.svg" alt="icon">
                        </div>
                      </div>
                      <div class="">
                        <div class="text-purple-1 text-16 fw-500 lh-1">Congrats!</div>
                        <div class="mt-3">Your Admission Completed</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="layout-pt-md layout-pb-md">
          <div data-anim-wrap class="container">
            <div class="row justify-center text-center">
              <div class="col-auto">
                <div class="sectionTitle ">
                  <h2 class="sectionTitle__title ">Why Future Learn?</h2>
                </div>
              </div>
            </div>

            <div class="row y-gap-30 justify-between pt-60 lg:pt-50">

              <div class="col-lg-3 col-md-6">
                <div class="coursesCard -type-3 px-0 text-center">
                  <div class="coursesCard__icon bg-white shadow-2">
                    <i class="fa fa-star text-dark-1"></i>
                  </div>

                  <div class="coursesCard__content mt-30">
                    <h5 class="coursesCard__title text-18 lh-1 fw-500">Learn anything</h5>
                    <p class="coursesCard__text text-14 mt-10">The latest design trends meet hand-crafted templates in Sassio Collection.</p>
                  </div>
                </div>
              </div>

              <div class="col-lg-3 col-md-6">
                <div class="coursesCard -type-3 px-0 text-center">
                  <div class="coursesCard__icon bg-white shadow-2">
                    <i class="fa fa-university text-dark-1"></i>
                  </div>

                  <div class="coursesCard__content mt-30">
                    <h5 class="coursesCard__title text-18 lh-1 fw-500">Learn together</h5>
                    <p class="coursesCard__text text-14 mt-10">The latest design trends meet hand-crafted templates in Sassio Collection.</p>
                  </div>
                </div>
              </div>

              <div class="col-lg-3 col-md-6">
                <div class="coursesCard -type-3 px-0 text-center">
                  <div class="coursesCard__icon bg-white shadow-2">
                    <i class="fa fa-users text-dark-1"></i>
                  </div>

                  <div class="coursesCard__content mt-30">
                    <h5 class="coursesCard__title text-18 lh-1 fw-500">Learn with experts</h5>
                    <p class="coursesCard__text text-14 mt-10">The latest design trends meet hand-crafted templates in Sassio Collection.</p>
                  </div>
                </div>
              </div>

              <div class="col-lg-3 col-md-6">
                <div class="coursesCard -type-3 px-0 text-center">
                  <div class="coursesCard__icon bg-white shadow-2">
                    <i class="fa fa-universal-access text-dark-1"></i>
                  </div>

                  <div class="coursesCard__content mt-30">
                    <h5 class="coursesCard__title text-18 lh-1 fw-500">Life Time Access</h5>
                    <p class="coursesCard__text text-14 mt-10">The latest design trends meet hand-crafted templates in Sassio Collection.</p>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </section>

        <section class="layout-pt-md layout-pb-lg">
          <div data-anim-wrap class="container tabs -pills-2 js-tabs">
            <div class="row y-gap-20 justify-between items-end">
              <div class="col-auto">
                <div class="sectionTitle ">
                  <h2 class="sectionTitle__title ">Explore Featured Courses</h2>
                </div>
              </div>
            </div>

            <div class="tabs__content pt-60 lg:pt-40 js-tabs-content">

              <div class="tabs__pane -tab-item-1 is-active">
                <div class="row y-gap-30 justify-center">
                  <!--Course list -->
                  <?php 
                    if(count($courses) > 0) { 
                      foreach($courses as $result) {
                  ?>
                  <div data-anim-child="slide-up delay-1" class="col-lg-6">
                    <a href="<?php echo url('/coursedetails/'.Crypt::encrypt($result->id)); ?>" class="coursesCard -type-4 d-flex sm:d-block items-center border-light rounded-8 px-10 py-10">
                      <div class="coursesCard__image max-w-250 rounded-8">
                        <?php if($result->university_logo!='') { ?>
                          <img class="course_logo" src="<?php echo $result->university_logo; ?>" alt="image" width="451px">
                        <?php } else { ?>
                          <img class="course_logo" src="la-assets/coursefinder/img/coursesCards/no-image.png" alt="image" width="451px">
                        <?php } ?>
                      </div>

                      <div class="coursesCard__content pl-20 sm:pl-10 pr-10">
                        <div class="coursesCard__stars">
                            <div class="text-14 lh-1 text-yellow-1 mr-10"><?php echo $result->university_name; ?></div>
                        </div>
                        <div class="text-17 lh-13 fw-500 text-dark-1 mt-10"><?php echo $result->course_name; ?></div>

                        <div class="d-flex x-gap-15 items-center py-10">

                          <div class="d-flex items-center">
                            <div class="mr-10">
                              <img src="la-assets/coursefinder/img/coursesCards/icons/location-icon.png" alt="icon">
                            </div>
                            <div class="text-14 lh-1 text-light-1"><?php echo $result->country_name; ?></div>
                          </div>

                          <div class="d-flex items-center">
                            <div class="mr-10">
                              <img src="la-assets/coursefinder/img/coursesCards/icons/2.svg" alt="icon">
                            </div>
                            <div class="text-14 lh-1 text-light-1"><?php echo $result->intake; ?></div>
                          </div>

                          <div class="d-flex items-center">
                            <div class="mr-10">
                              <img src="la-assets/coursefinder/img/coursesCards/icons/1.svg" alt="icon">
                            </div>
                            <div class="text-14 lh-1 text-light-1"><?php echo $result->course_type; ?></div>
                          </div>

                        </div>

                        <div class="d-flex justify-between items-center pt-10 border-top-light">
                          <div class="d-flex items-center">
                            <div class="text-light-1 ml-10">Annual Tuition Fees</div>
                          </div>

                          <div class="d-flex items-center">
                            <div class="fw-500 mr-10 line-through text-light-1"></div>
                            <div class="text-18 fw-500 text-dark-1">&#8377; <?php echo $result->tuition_fees; ?></div>
                          </div>
                        </div>
                      </div>
                    </a>
                  </div>
                  <?php }} ?>
                  <!-- End Course list -->

                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="layout-pt-lg bg-dark-2">
          <div data-anim-wrap class="container">
            <div class="row y-gap-30 items-center">
              <div class="col-lg-6 col-md-10">
                <h2 class="text-30 lh-15 text-white">Testimonials</h2>
                <div class="row x-gap-50 y-gap-30 pt-60 lg:pt-40 pr-40 md:pr-0">

                  <div class="col-sm-6 text-white">
                    <div class="text-45 lh-11 fw-700">9/10</div>
                    <div class="mt-10">9/10 users reported better learning outcomes.</div>
                  </div>

                  <div class="col-sm-6 text-white">
                    <div class="text-45 lh-11 fw-700">85%</div>
                    <div class="mt-10">85% of students see their course through to completion</div>
                  </div>

                </div>
              </div>

              <div class="col-lg-4 offset-lg-1">
                <div class="testimonials-slider-2 js-testimonials-slider-2">
                  <div class="swiper-wrapper">

                    <div class="swiper-slide shadow-2">
                      <div class="testimonials -type-1">
                        <div class="testimonials__content">
                          <h4 class="testimonials__title">Lorem ipsum</h4>
                          <p class="testimonials__text"> “Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.”</p>

                          <div class="testimonials-footer">
                            <div class="testimonials-footer__image">
                              <img src="la-assets/coursefinder/img/testimonials/1.png" alt="image">
                            </div>

                            <div class="testimonials-footer__content">
                              <div class="testimonials-footer__title">Lorem ipsum</div>
                              <div class="testimonials-footer__text">Lorem ipsum, Consetetur sadipscing</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="swiper-slide shadow-2">
                      <div class="testimonials -type-1">
                        <div class="testimonials__content">
                          <h4 class="testimonials__title">Lorem ipsum</h4>
                          <p class="testimonials__text"> “Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.”</p>

                          <div class="testimonials-footer">
                            <div class="testimonials-footer__image">
                              <img src="la-assets/coursefinder/img/testimonials/2.png" alt="image">
                            </div>

                            <div class="testimonials-footer__content">
                              <div class="testimonials-footer__title">Lorem ipsum</div>
                              <div class="testimonials-footer__text">Lorem ipsum, Consetetur sadipscing</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="swiper-slide shadow-2">
                      <div class="testimonials -type-1">
                        <div class="testimonials__content">
                          <h4 class="testimonials__title">Lorem ipsum</h4>
                          <p class="testimonials__text"> “Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.”</p>

                          <div class="testimonials-footer">
                            <div class="testimonials-footer__image">
                              <img src="la-assets/coursefinder/img/testimonials/3.png" alt="image">
                            </div>

                            <div class="testimonials-footer__content">
                              <div class="testimonials-footer__title">Lorem ipsum</div>
                              <div class="testimonials-footer__text">Lorem ipsum, Consetetur sadipscing</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>


                  <div class="d-flex x-gap-15 items-center justify-center pt-30">
                    <div class="col-auto">
                      <button class="d-flex items-center text-24 arrow-left-hover text-white js-prev">
                        <i class="fa fa-arrow-left"></i>
                      </button>
                    </div>
                    <div class="col-auto">
                      <div class="pagination -arrows text-white js-pagination"></div>
                    </div>
                    <div class="col-auto">
                      <button class="d-flex items-center text-24 arrow-right-hover text-white js-next">
                        <i class="fa fa-arrow-right"></i>
                      </button>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </section>

        <div class="svg-shape">
          <svg width="1925" height="261" viewBox="0 0 1925 261" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M1924.67 0L1922.7 7.03707C1911.58 46.7293 1877.25 75.5353 1836.23 79.5878L0 261V0H1924.67Z" fill="#1A064F" />
          </svg>
        </div>

        <section class="layout-pt-lg layout-pb-lg">
          <div class="container">
            <div class="row y-gap-30 justify-between items-center">
              <div class="col-xl-5 col-lg-6 col-md-9 lg:order-2">
                <h3 class="text-45 md:text-30 lh-12"><span class="text-purple-1">Learn</span> new skills when<br class="lg:d-none"> and where you like.</h3>
                <p class="mt-20">Use the list below to bring attention to your product’s key<br class="lg:d-none"> differentiator.</p>
                <div class="d-inline-block mt-30">
                  <a href="<?php echo url('/signup'); ?>" class="button -md -dark-1 text-white">Join Free</a>
                </div>
              </div>

              <div class="col-lg-6 lg:order-1">
                <div class="composition -type-3">
                  <div class="-el-1">
                    <div class="bg-dark-1 py-50 px-30 rounded-8">
                      <div class="y-gap-20 pt-25">

                        <div class="d-flex items-center">
                          <div class="d-flex items-center justify-center size-25 rounded-full bg-purple-1 mr-15">
                            <i class="text-white size-12" data-feather="check"></i>
                          </div>
                          <div class="fw-500 text-white">Hand-picked authors</div>
                        </div>

                        <div class="d-flex items-center">
                          <div class="d-flex items-center justify-center size-25 rounded-full bg-purple-1 mr-15">
                            <i class="text-white size-12" data-feather="check"></i>
                          </div>
                          <div class="fw-500 text-white">Easy to follow curriculum</div>
                        </div>

                        <div class="d-flex items-center">
                          <div class="d-flex items-center justify-center size-25 rounded-full bg-purple-1 mr-15">
                            <i class="text-white size-12" data-feather="check"></i>
                          </div>
                          <div class="fw-500 text-white">Free courses</div>
                        </div>

                        <div class="d-flex items-center">
                          <div class="d-flex items-center justify-center size-25 rounded-full bg-purple-1 mr-15">
                            <i class="text-white size-12" data-feather="check"></i>
                          </div>
                          <div class="fw-500 text-white">Money-back guarantee</div>
                        </div>

                      </div>
                    </div>
                  </div>
                  <div class="-el-2">
                    <img class="w-1/1" src="la-assets/coursefinder/img/home-6/learn/1.png" alt="image">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <footer class="footer -type-5 pt-60">
          <div class="container">
            <div class="py-30 border-top-light">
              <div class="row justify-between items-center y-gap-20">
                <div class="col-auto">
                  <div class="footer-footer__copyright d-flex items-center h-100">
                    © 2022 ZODOCS. All Right Reserved.
                  </div>
                </div>

                <div class="col-auto">
                  <div class="d-flex x-gap-20 y-gap-20 items-center flex-wrap">
                    <div>
                      <div class="d-flex x-gap-15">
                        Powered By<a href="http://demo.zodocs.com/" target="_blank" class="ft_txt1">ZODOCS</a>
                        . Solution By<a href="https://www.labgex.com/" target="_blank" class="ft_txt1">LabGex Innovations (P) Ltd</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>


      </div>
    </main>
  </div>
  <!-- barba container end -->

  <!-- JavaScript -->
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
  <script src="la-assets/coursefinder/js/vendors.js"></script>
  <script src="la-assets/coursefinder/js/main.js"></script>

</body>

</html>