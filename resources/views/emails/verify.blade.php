<!DOCTYPE html>
<html>
  <head>
    <title>Welcome Email</title>
  </head>
  <body>
    <h2>Welcome to the site {{$name}}</h2>
    <br/>
    Your registered email-id is {{$user}} <br/>
    Your password: {{$password}}<br/>
    
    Please click on the below link to verify your email account
    <br/>
    <a href="{{url('user/verify', $token)}}">Verify Email</a><br><br>
    Best Regards,<br>
    Zodocs
  </body>
</html>