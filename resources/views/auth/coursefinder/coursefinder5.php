<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Google fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Outlined" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />

  <!-- Stylesheets -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/vendors.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/main.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/custom.css">

  <title>Course Finder</title>
  <link rel="icon" type="image/x-icon" href="la-assets/coursefinder/img/coursesCards/icons/zodocs_fav.png">
</head>

<body class="preloader-visible" data-barba="wrapper">

  <!-- preloader start -->
  <div class="preloader js-preloader">
    <div class="preloader__bg"></div>
  </div>
  <!-- preloader end -->

  <!-- barba container start -->
  <div class="barba-container" data-barba="container">


    <main class="main-content  ">

      <header data-anim="fade" data-add-bg="" class="header -type-3 js-header">


        <div class="header__container py-10">
          <div class="row justify-between items-center">

            <div class="col-auto">
              <div class="header-left d-flex items-center">

                <div class="header__logo ">
                  <a data-barba href="<?php echo url('/coursefinder'); ?>">
                    <img src="la-assets/coursefinder/img/general/logo1.png" alt="logo">
                  </a>
                </div>
              </div>
            </div>


            <div class="col-auto">
              <div class="header-right d-flex items-center">
                <div class="header-right__icons text-white d-flex items-center">

                  <div class="header-menu js-mobile-menu-toggle ">
                    <div class="header-menu__content">
                      <div class="mobile-bg js-mobile-bg"></div>

                      <div class="d-none xl:d-flex items-center px-20 py-20 border-bottom-light">
                        <?php 
                        if(isset(Auth::user()->type)) { ?>
                          <a href="<?php echo url('/logout'); ?>" class="text-dark-1">Log out</a>
                          <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="text-dark-1 ml-30">My Account</a>
                        <?php } else { ?>
                          <a href="<?php echo url('/userlogin'); ?>" class="text-dark-1">Log in</a>
                          <a href="<?php echo url('/signup'); ?>" class="text-dark-1 ml-30">Sign up</a>
                        <?php } ?>
                      </div>

                      <div class="menu js-navList">
                        <ul class="menu__nav text-dark-1 -is-active">
                          
                          <li class="menu-item-has-children">
                      <a data-barba href="#">Home</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Home</a>
                        </li>
                        <li><a href="<?php echo url('/coursefinder'); ?>">Home 1</a></li>
                        <li><a href="<?php echo url('/coursefinder2'); ?>">Home 2</a></li>
                        <li><a href="<?php echo url('/coursefinder3'); ?>">Home 3</a></li>
                        <li><a href="<?php echo url('/coursefinder4'); ?>">Home 4</a></li>
                        <li><a href="<?php echo url('/coursefinder5'); ?>">Home 5</a></li>
                        <li><a href="<?php echo url('/coursefinder6'); ?>">Home 6</a></li>
                        <li><a href="<?php echo url('/coursefinder7'); ?>">Home 7</a></li>
                        <li><a href="<?php echo url('/coursefinder8'); ?>">Home 8</a></li>
                        <li><a href="<?php echo url('/coursefinder9'); ?>">Home 9</a></li>
                        <li><a href="<?php echo url('/coursefinder10'); ?>">Home 10</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/aboutus'); ?>">
                        About Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li class="menu-item-has-children">
                      <a data-barba href="#">Courses</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Courses</a>
                        </li>
                        <li><a href="<?php echo url('/coursesearch'); ?>">Course 1</a></li>
                        <li><a href="<?php echo url('/coursesearch2'); ?>">Course 2</a></li>
                        <li><a href="<?php echo url('/coursesearch3'); ?>">Course 3</a></li>
                        <li><a href="<?php echo url('/coursesearch4'); ?>">Course 4</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/blog'); ?>">
                        Blog <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/topcourses'); ?>">
                        Top Courses <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/contactus'); ?>">
                        Contact Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                          
                        </ul>
                      </div>

                    </div>

                    <div class="header-menu-close" data-el-toggle=".js-mobile-menu-toggle">
                      <div class="size-40 d-flex items-center justify-center rounded-full bg-white">
                        <i class="fa fa-close"></i>
                      </div>
                    </div>

                    <div class="header-menu-bg"></div>
                  </div>

                  <div class="d-none xl:d-block ml-20">
                    <button class="text-dark-1 items-center" data-el-toggle=".js-mobile-menu-toggle">
                      <i class="fa fa-bars"></i>
                    </button>
                  </div>

                </div>

                <div class="header-right__buttons d-flex items-center ml-30 xl:ml-20 md:d-none">
                  
                  <?php 
                  if(isset(Auth::user()->type)) { ?>
                    <a href="<?php echo url('/logout'); ?>" class="button px-30 h-50 -outline-dark-1 text-dark-1">Log out</a>
                    <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="button px-30 h-50 -dark-1 text-white ml-10">My Account</a>
                  <?php } else { ?>
                    <a href="<?php echo url('/userlogin'); ?>" class="button px-30 h-50 -outline-dark-1 text-dark-1">Log in</a>
                    <a href="<?php echo url('/signup'); ?>" class="button px-30 h-50 -dark-1 text-white ml-10">Sign up</a>
                  <?php } ?>

                </div>
              </div>
            </div>

          </div>
        </div>
      </header>


      <div class="content-wrapper  js-content-wrapper">


        <section data-anim-wrap class="masthead -type-2">
          <div class="masthead__bg">
            <div class="bg-image js-lazy" data-bg="la-assets/coursefinder/img/home-3/masthead/bg.jpg"></div>
          </div>

          <div class="container">
            <div class="row y-gap-50 justify-center items-center">
              <div class="col-xl-6 col-lg-11">
                <div class="masthead__content">
                  <div data-anim-child="slide-up delay-2" class="masthead__subtitle fw-500 text-green-1 text-17 lh-15">
                    Zodocs CRM software is highly rated in the study abroad education industry
                  </div>
                  <h1 data-anim-child="slide-up delay-3" class="masthead__title text-white mt-10">
                    Process of Overseas
                    Education Visa CRM
                    with Zodocs
                  </h1>
                  <div data-anim-child="slide-up delay-4" class="masthead__button mt-20">
                    <a href="<?php echo url('/coursesearch'); ?>" class="button -md -white text-dark-1">Find Courses</a>
                  </div>
                </div>
              </div>

              <div class="col-xl-5 col-lg-11">
                <div data-anim-child="slide-up delay-6" class="masthead-form">
                  <h4 class="masthead-form__title text-center text-20 lh-15 fw-500 mb-30">
                    Create Your <span class="text-purple-1"><a href="<?php echo url('/signup'); ?>">Free Account</a></span>
                  </h4>

                  <?php if (count($errors) > 0) { ?>
                        <div class="alert alert-danger err_txt"><br>
                            <strong>Whoops!</strong> There were some problems with your input.<br>
                            <ul>
                                <?php foreach ($errors->all() as $error) { ?>
                                    <li><?php echo $error; ?></li>
                                <?php } ?>
                            </ul>
                        </div><br>
                  <?php } 
                    if (session('loginfail')) { ?>
                        <div class="alert alert-danger mt20 err_txt">
                          <?php echo session('loginfail'); ?>
                        </div>
                  <?php } ?>

                  <form action="<?php echo url('/login'); ?>" method="post">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" name="pathvalue" value="userlogincf">
                    <div class="masthead-form__group">
                      <label>Email</label>
                      <input type="email" name="email" placeholder="Email">
                    </div>

                    <div class="masthead-form__group">
                      <label>Password</label>
                      <input type="password" name="password" placeholder="Password">
                    </div>

                    <div class="masthead-form__group">
                      <button type="submit" class="button -md -purple-1 text-white">Login</button>
                    </div>

                    <div class="masthead-form__desc">
                      
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="layout-pt-sm layout-pb-sm border-bottom-light">
          <div class="container">
            <div class="row y-gap-30 justify-between">

              <div class="col-xl-3 col-md-6">
                <div class="d-flex items-center">
                  <div class="mr-20">
                    <img src="la-assets/coursefinder/img/home-3/masthead/icons/1.svg" alt="icon">
                  </div>

                  <div>
                    <h4 class="text-20 fw-500">100,000 online courses</h4>
                    <div class="text-dark-1">Explore a variety of fresh topics</div>
                  </div>
                </div>
              </div>

              <div class="col-xl-3 col-md-6">
                <div class="d-flex items-center">
                  <div class="mr-20">
                    <img src="la-assets/coursefinder/img/home-3/masthead/icons/2.svg" alt="icon">
                  </div>

                  <div>
                    <h4 class="text-20 fw-500">Expert agent</h4>
                    <div class="text-dark-1">Find the right agent for you</div>
                  </div>
                </div>
              </div>

              <div class="col-xl-3 col-md-6">
                <div class="d-flex items-center">
                  <div class="mr-20">
                    <img src="la-assets/coursefinder/img/home-3/masthead/icons/3.svg" alt="icon">
                  </div>

                  <div>
                    <h4 class="text-20 fw-500">Lifetime access</h4>
                    <div class="text-dark-1">Learn on your schedule</div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </section>

        <section class="layout-pt-lg layout-pb-lg section-bg">
          <div class="section-bg__item bg-light-6"></div>

          <div data-anim-wrap class="container">
            <div class="row y-gap-15 justify-between items-center">
              <div class="col-lg-6">

                <div class="sectionTitle ">
                  <h2 class="sectionTitle__title ">Top courses</h2>
                </div>
              </div>
            </div>

            <div class="relative">
              <div class="overflow-hidden pt-60 lg:pt-50 js-section-slider" data-gap="30" data-loop data-pagination data-nav-prev="js-courses-prev" data-nav-next="js-courses-next" data-slider-cols="xl-4 lg-3 md-2 sm-2">
                <div class="swiper-wrapper">

                  <!--Course list -->
                  <?php 
                    if(count($courses) > 0) { 
                      foreach($courses as $result) {
                  ?>

                  <div class="swiper-slide">
                    <div data-anim-child="slide-up delay-1">

                      <a href="<?php echo url('/coursedetails/'.Crypt::encrypt($result->id)); ?>" class="coursesCard -type-1 px-10 py-10 border-light bg-white rounded-8">
                        <div class="relative">
                          <div class="coursesCard__image overflow-hidden rounded-8">
                            <?php if($result->university_logo!='') { ?>
                              <img class="w-1/1 course_logo" src="<?php echo $result->university_logo; ?>" alt="image">
                            <?php } else { ?>
                              <img class="w-1/1 course_logo" src="la-assets/coursefinder/img/coursesCards/no-image.png" alt="image">
                            <?php } ?>
                            <div class="coursesCard__image_overlay rounded-8"></div>
                          </div>
                          <div class="d-flex justify-between py-10 px-10 absolute-full-center z-3">

                          </div>
                        </div>

                        <div class="h-100 px-10 pt-10">
                          <div class="d-flex items-center">
                            <div class="text-14 lh-1 text-yellow-1 mr-10"><?php echo $result->university_name; ?></div>
                          </div>

                          <div class="text-17 lh-15 fw-500 text-dark-1 mt-10"><?php echo $result->course_name; ?></div>

                          <div class="d-flex x-gap-10 items-center pt-10">

                            <div class="d-flex items-center">
                                <div class="mr-8">
                                  <img src="la-assets/coursefinder/img/coursesCards/icons/location-icon.png" alt="icon">
                                </div>
                                <div class="text-14 lh-1"><?php echo $result->country_name; ?></div>
                              </div>

                              <div class="d-flex items-center">
                                <div class="mr-8">
                                  <img src="la-assets/coursefinder/img/coursesCards/icons/2.svg" alt="icon">
                                </div>
                                <div class="text-14 lh-1"><?php echo $result->intake; ?></div>
                              </div>

                              <div class="d-flex items-center">
                                <div class="mr-8">
                                  <img src="la-assets/coursefinder/img/coursesCards/icons/1.svg" alt="icon">
                                </div>
                                <div class="text-14 lh-1"><?php echo $result->course_type; ?></div>
                              </div>

                          </div>

                          <div class="coursesCard-footer">
                            <div class="coursesCard-footer__author">
                              <div>Annual Tuition Fees</div>
                            </div>

                            <div class="coursesCard-footer__price">
                              <div></div>
                              <div>&#8377; <?php echo $result->tuition_fees; ?></div>
                            </div>
                          </div>
                        </div>
                      </a>

                    </div>
                  </div>

                  <?php }} ?>
                  <!-- End Course list -->

                </div>
              </div>


              <button class="section-slider-nav -prev -dark-bg-dark-2 -outline-dark-1 -absolute-out size-50 rounded-full xl:d-none js-courses-prev">
                <i class="fa fa-arrow-left text-24"></i>
              </button>

              <button class="section-slider-nav -next -dark-bg-dark-2 -outline-dark-1 -absolute-out size-50 rounded-full xl:d-none js-courses-next">
                <i class="fa fa-arrow-right text-24"></i>
              </button>

            </div>
          </div>
        </section>

        <section class="layout-pt-lg layout-pb-lg">
          <div class="container">
            <div class="row y-gap-20 justify-center text-center">
              <div class="col-auto">

                <div class="sectionTitle ">
                  <h2 class="sectionTitle__title ">How it works?</h2>
                </div>

              </div>
            </div>

            <div class="row y-gap-30 justify-between pt-60 lg:pt-40">

              <div class="col-xl-2 col-lg-3 col-md-6">
                <div class="d-flex flex-column items-center text-center">
                  <div class="relative size-120 d-flex justify-center items-center rounded-full bg-light-4">
                    <img src="la-assets/coursefinder/img/home-3/works/1.svg" alt="image">
                    <div class="side-badge">
                      <div class="size-35 d-flex justify-center items-center rounded-full bg-dark-1 -dark-bg-purple-1">
                        <span class="text-14 fw-500 text-white">01</span>
                      </div>
                    </div>
                  </div>
                  <div class="text-17 fw-500 text-dark-1 mt-30">Browse courses from our expert contributors.</div>
                </div>
              </div>


              <div class="col-auto xl:d-none">
                <div class="pt-30">
                  <img src="la-assets/coursefinder/img/misc/lines/1.svg" alt="icon">
                </div>
              </div>


              <div class="col-xl-2 col-lg-3 col-md-6">
                <div class="d-flex flex-column items-center text-center">
                  <div class="relative size-120 d-flex justify-center items-center rounded-full bg-light-4">
                    <img src="la-assets/coursefinder/img/home-3/works/2.svg" alt="image">
                    <div class="side-badge">
                      <div class="size-35 d-flex justify-center items-center rounded-full bg-dark-1 -dark-bg-purple-1">
                        <span class="text-14 fw-500 text-white">02</span>
                      </div>
                    </div>
                  </div>
                  <div class="text-17 fw-500 text-dark-1 mt-30">Purchase quickly and securely.</div>
                </div>
              </div>


              <div class="col-auto xl:d-none">
                <div class="pt-30">
                  <img src="la-assets/coursefinder/img/misc/lines/2.svg" alt="icon">
                </div>
              </div>


              <div class="col-xl-2 col-lg-3 col-md-6">
                <div class="d-flex flex-column items-center text-center">
                  <div class="relative size-120 d-flex justify-center items-center rounded-full bg-light-4">
                    <img src="la-assets/coursefinder/img/home-3/works/3.svg" alt="image">
                    <div class="side-badge">
                      <div class="size-35 d-flex justify-center items-center rounded-full bg-dark-1 -dark-bg-purple-1">
                        <span class="text-14 fw-500 text-white">03</span>
                      </div>
                    </div>
                  </div>
                  <div class="text-17 fw-500 text-dark-1 mt-30">That’s it! Start learning right away.</div>
                </div>
              </div>


            </div>
          </div>
        </section>

        <div class="line px-50">
          <div class="line__item bg-light-5"></div>
        </div>

        <section class="layout-pt-lg layout-pb-lg section-bg">
          <div class="section-bg__item bg-light-6"></div>

          <div class="container">
            <div class="row y-gap-20 justify-center text-center">
              <div class="col-auto">

                <div class="sectionTitle ">
                  <h2 class="sectionTitle__title ">Testimonials</h2>
                </div>

              </div>
            </div>

            <div class="row justify-center pt-60">
              <div class="col-xl-6 col-lg-8 col-md-10">
                <div class="overflow-hidden js-testimonials-slider">
                  <div class="swiper-wrapper">

                    <div class="swiper-slide h-100">
                      <div data-anim="slide-up" class="testimonials -type-2 text-center">
                        <div class="testimonials__icon">
                          <img src="la-assets/coursefinder/img/misc/quote.svg" alt="quote">
                        </div>
                        <div class="testimonials__text md:text-20 fw-500 text-dark-1">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.</div>
                        <div class="testimonials__author">
                          <h5 class="text-17 lh-15 fw-500">Lorem ipsum</h5>
                          <div class="mt-5">Lorem ipsum, Consetetur sadipscing</div>
                        </div>
                      </div>
                    </div>

                    <div class="swiper-slide h-100">
                      <div data-anim="slide-up" class="testimonials -type-2 text-center">
                        <div class="testimonials__icon">
                          <img src="la-assets/coursefinder/img/misc/quote.svg" alt="quote">
                        </div>
                        <div class="testimonials__text md:text-20 fw-500 text-dark-1">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.</div>
                        <div class="testimonials__author">
                          <h5 class="text-17 lh-15 fw-500">Lorem ipsum</h5>
                          <div class="mt-5">Lorem ipsum, Consetetur sadipscing</div>
                        </div>
                      </div>
                    </div>

                    <div class="swiper-slide h-100">
                      <div data-anim="slide-up" class="testimonials -type-2 text-center">
                        <div class="testimonials__icon">
                          <img src="la-assets/coursefinder/img/misc/quote.svg" alt="quote">
                        </div>
                        <div class="testimonials__text md:text-20 fw-500 text-dark-1">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.</div>
                        <div class="testimonials__author">
                          <h5 class="text-17 lh-15 fw-500">Lorem ipsum</h5>
                          <div class="mt-5">Lorem ipsum, Consetetur sadipscing</div>
                        </div>
                      </div>
                    </div>

                    <div class="swiper-slide h-100">
                      <div data-anim="slide-up" class="testimonials -type-2 text-center">
                        <div class="testimonials__icon">
                          <img src="la-assets/coursefinder/img/misc/quote.svg" alt="quote">
                        </div>
                        <div class="testimonials__text md:text-20 fw-500 text-dark-1">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.</div>
                        <div class="testimonials__author">
                          <h5 class="text-17 lh-15 fw-500">Lorem ipsum</h5>
                          <div class="mt-5">Lorem ipsum, Consetetur sadipscing</div>
                        </div>
                      </div>
                    </div>

                    <div class="swiper-slide h-100">
                      <div data-anim="slide-up" class="testimonials -type-2 text-center">
                        <div class="testimonials__icon">
                          <img src="la-assets/coursefinder/img/misc/quote.svg" alt="quote">
                        </div>
                        <div class="testimonials__text md:text-20 fw-500 text-dark-1">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed di nonumy eirmod tempor invidunt ut labore et dolore magn aliq erat.</div>
                        <div class="testimonials__author">
                          <h5 class="text-17 lh-15 fw-500">Lorem ipsum</h5>
                          <div class="mt-5">Lorem ipsum, Consetetur sadipscing</div>
                        </div>
                      </div>
                    </div>

                  </div>

                  <div class="pt-60 lg:pt-40">
                    <div class="pagination -avatars row x-gap-40 y-gap-20 justify-center js-testimonials-pagination">

                      <div class="col-auto">
                        <div class="pagination__item is-active">
                          <img src="la-assets/coursefinder/img/home-3/testimonials/1.png" alt="image">
                        </div>
                      </div>

                      <div class="col-auto">
                        <div class="pagination__item ">
                          <img src="la-assets/coursefinder/img/home-3/testimonials/2.png" alt="image">
                        </div>
                      </div>

                      <div class="col-auto">
                        <div class="pagination__item ">
                          <img src="la-assets/coursefinder/img/home-3/testimonials/3.png" alt="image">
                        </div>
                      </div>

                      <div class="col-auto">
                        <div class="pagination__item ">
                          <img src="la-assets/coursefinder/img/home-3/testimonials/4.png" alt="image">
                        </div>
                      </div>

                      <div class="col-auto">
                        <div class="pagination__item ">
                          <img src="la-assets/coursefinder/img/home-3/testimonials/5.png" alt="image">
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="layout-pt-lg layout-pb-lg">
          <div data-anim-wrap class="container">
            <div class="row y-gap-15 justify-between items-center">
              <div class="col-lg-6">
                <div class="sectionTitle ">
                  <h2 class="sectionTitle__title ">Recent courses</h2>
                </div>
              </div>
            </div>

            <div class="row y-gap-30 justify-center pt-50">

              <!--Course list -->
              <?php 
                if(count($courses) > 0) { 
                  foreach($courses as $result) {
              ?>
              <div class="col-lg-3 col-md-6">
                <div data-anim-child="slide-up delay-1">

                  <a href="<?php echo url('/coursedetails/'.Crypt::encrypt($result->id)); ?>" class="coursesCard -type-1 border-light rounded-8">
                    <div class="relative">
                      <div class="coursesCard__image overflow-hidden rounded-top-8">
                        <?php if($result->university_logo!='') { ?>
                          <img class="w-1/1 course_logo" src="<?php echo $result->university_logo; ?>" alt="image">
                        <?php } else { ?>
                          <img class="w-1/1 course_logo" src="la-assets/coursefinder/img/coursesCards/no-image.png" alt="image">
                        <?php } ?>
                        <div class="coursesCard__image_overlay rounded-top-8"></div>
                      </div>
                      <div class="d-flex justify-between py-10 px-10 absolute-full-center z-3">

                      </div>
                    </div>

                    <div class="h-100 pt-15 pb-10 px-20">
                      <div class="d-flex items-center">
                        <div class="text-14 lh-1 text-yellow-1 mr-10"><?php echo $result->university_name; ?></div>
                      </div>

                      <div class="text-17 lh-15 fw-500 text-dark-1 mt-10"><?php echo $result->course_name; ?></div>

                      <div class="d-flex x-gap-10 items-center pt-10">

                        <div class="d-flex items-center">
                                <div class="mr-8">
                                  <img src="la-assets/coursefinder/img/coursesCards/icons/location-icon.png" alt="icon">
                                </div>
                                <div class="text-14 lh-1"><?php echo $result->country_name; ?></div>
                              </div>

                              <div class="d-flex items-center">
                                <div class="mr-8">
                                  <img src="la-assets/coursefinder/img/coursesCards/icons/2.svg" alt="icon">
                                </div>
                                <div class="text-14 lh-1"><?php echo $result->intake; ?></div>
                              </div>

                              <div class="d-flex items-center">
                                <div class="mr-8">
                                  <img src="la-assets/coursefinder/img/coursesCards/icons/1.svg" alt="icon">
                                </div>
                                <div class="text-14 lh-1"><?php echo $result->course_type; ?></div>
                              </div>

                      </div>

                      <div class="coursesCard-footer">
                        <div class="coursesCard-footer__author">
                          <div>Annual Tuition Fees</div>
                        </div>

                        <div class="coursesCard-footer__price">
                          <div></div>
                          <div>&#8377; <?php echo $result->tuition_fees; ?></div>
                        </div>
                      </div>
                    </div>
                  </a>

                </div>
              </div>
              <?php }} ?>
              <!-- End Course list -->

            </div>

            <div class="row justify-center pt-60 lg:pt-40">
              <div class="col-auto">

                <a href="<?php echo url('/coursesearch'); ?>" class="button -icon -purple-3 text-purple-1">
                  All Courses
                </a>

              </div>
            </div>
          </div>
        </section>

        <section class="layout-pt-lg layout-pb-lg section-bg">
          <div class="section-bg__item bg-light-6"></div>

          <div class="container">
            <div class="row y-gap-20 justify-center text-center">
              <div class="col-auto">
                <div class="sectionTitle ">
                  <h2 class="sectionTitle__title ">Our Achievements</h2>
                </div>
              </div>
            </div>

            <div class="row pt-60">

              <div class="col-lg-3 col-md-6">
                <div class="infoCard -type-2 text-center py-40 -infoCard-hover">
                  <div class="infoCard__image">
                    <img src="la-assets/coursefinder/img/home-3/achieve/1.svg" alt="image">
                  </div>
                  <h5 class="infoCard__title text-24 lh-1 mt-25">350,000+</h5>
                  <p class="infoCard__text mt-5">Students worldwide</p>
                </div>
              </div>

              <div class="col-lg-3 col-md-6">
                <div class="infoCard -type-2 text-center py-40 -infoCard-hover">
                  <div class="infoCard__image">
                    <img src="la-assets/coursefinder/img/home-3/achieve/2.svg" alt="image">
                  </div>
                  <h5 class="infoCard__title text-24 lh-1 mt-25">496,00+</h5>
                  <p class="infoCard__text mt-5">Total course views</p>
                </div>
              </div>

              <div class="col-lg-3 col-md-6">
                <div class="infoCard -type-2 text-center py-40 -infoCard-hover">
                  <div class="infoCard__image">
                    <img src="la-assets/coursefinder/img/home-3/achieve/3.svg" alt="image">
                  </div>
                  <h5 class="infoCard__title text-24 lh-1 mt-25">19,000+</h5>
                  <p class="infoCard__text mt-5">Five-star course reviews</p>
                </div>
              </div>

              <div class="col-lg-3 col-md-6">
                <div class="infoCard -type-2 text-center py-40 -infoCard-hover">
                  <div class="infoCard__image">
                    <img src="la-assets/coursefinder/img/home-3/achieve/4.svg" alt="image">
                  </div>
                  <h5 class="infoCard__title text-24 lh-1 mt-25">987,000+</h5>
                  <p class="infoCard__text mt-5">Students community</p>
                </div>
              </div>

            </div>
          </div>
        </section>

        <footer class="footer -type-5 pt-60">
          <div class="container">
            
            <div class="py-30 border-top-light">
              <div class="row justify-between items-center y-gap-20">
                <div class="col-auto">
                  <div class="footer-footer__copyright d-flex items-center h-100">
                    © 2022 ZODOCS. All Right Reserved.
                  </div>
                </div>

                <div class="col-auto">
                  <div class="d-flex x-gap-20 y-gap-20 items-center flex-wrap">
                    <div>
                      <div class="d-flex x-gap-15">
                        Powered By<a href="http://demo.zodocs.com/" target="_blank" class="ft_txt1">ZODOCS</a>
                        . Solution By<a href="https://www.labgex.com/" target="_blank" class="ft_txt1">LabGex Innovations (P) Ltd</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>


      </div>
    </main>
  </div>
  <!-- barba container end -->

  <!-- JavaScript -->
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
  <script src="la-assets/coursefinder/js/vendors.js"></script>
  <script src="la-assets/coursefinder/js/main.js"></script>

</body>

</html>