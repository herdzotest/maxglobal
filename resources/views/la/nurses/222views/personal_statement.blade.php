<div id="tab_10" class="tab-pane" >
        
<div class="content-head">
                                        <h4>Personal statement</h4>

                                </div>
	
            <div class="pane">
				<div class="academic">
					<div class="mgv-field">
							<div class="name">
							Personal statement 
							</div>
							<div class="value" id="personal_statement"></div>
					</div>  
					<div class="mgv-field">
							<div class="name">
							Transcripts/Certificates 
							</div>
							<div class="value" data-type="checkbox" id="certificates"></div>
					 </div>                 
					<div class="mgv-field">
							<div class="name">
							English Language qualification
							</div>
							<div class="value"  id="eng_qualification" data-type="checkbox" ></div>
					 </div>  
					<div class="mgv-field">
							<div class="name">
								CV/Resumé
							</div>
							<div class="value" id="resume" data-type="checkbox" ></div>
					 </div>				 
					<div class="mgv-field">
							<div class="name" >
							Passport personal details page
							</div>
							<div class="value" id="passport" data-type="checkbox" ></div>
					 </div>             
					<div class="mgv-field">
							<div class="name">
							Reference(s)
							</div>
							<div class="value" id="reference" data-type="checkbox" ></div>
					 </div> 				 
					<div class="mgv-field">
							<div class="name">
								Statement of purpose
							</div>
							<div class="value" id="statement" data-type="checkbox" ></div>
					 </div>	
					<div class="mgv-field">
							<div class="name">
								Confirmation of immigration history (if applicable): to include copies of any previous Tier 4 (General) visas,and if applying for your visa from within the UK a copyof your current Tier 4 (General) visa
							</div>
							<div class="value" id="imigration" data-type="checkbox" ></div>
					 </div>
					<div class="mgv-field">
							<div class="name">Skype profile screen shot</div>
							<div class="value" id="skype_profile" data-type="checkbox" ></div>
					 </div>	
					<div class="mgv-field">
							<div class="name">TB screening Test Report</div>
							<div class="value" id="tb_screenshot" data-type="checkbox" ></div>
					 </div>
					<div class="mgv-field">
							<div class="name">Maintenance fund proof</div>
							<div class="value" id="fund_proof" data-type="checkbox" ></div>
					 </div>
					<div class="mgv-field">
							<div class="name">Experience certificates</div>
							<div class="value" id="exp_cert" data-type="checkbox" ></div>
					 </div>	
				</div>
            </div>        
</div>

                