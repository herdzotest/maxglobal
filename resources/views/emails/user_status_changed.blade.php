Dear {{ $user }},<br><br>

Your application form status has been changed to <b>{{ $status }}.</b><br><br>

You can login to <a href="{{ url('/userlogin') }}">{{ str_replace("http://", "", url('/userlogin')) }}</a> and check the details.<br><br>

Best Regards,<br>
Zodocs