<?php
//Route::get('/coursefinder', 'Auth\AuthController@coursefinder');
Route::get('/coursefinder', 'Coursefinder\CoursefinderController@coursefinder');
Route::get('/coursefinder2', 'Coursefinder\CoursefinderController@coursefinder2');
Route::get('/coursefinder3', 'Coursefinder\CoursefinderController@coursefinder3');
Route::get('/coursefinder4', 'Coursefinder\CoursefinderController@coursefinder4');
Route::get('/coursefinder5', 'Coursefinder\CoursefinderController@coursefinder5');
Route::get('/coursefinder6', 'Coursefinder\CoursefinderController@coursefinder6');
Route::get('/coursefinder7', 'Coursefinder\CoursefinderController@coursefinder7');
Route::get('/coursefinder8', 'Coursefinder\CoursefinderController@coursefinder8');
Route::get('/coursefinder9', 'Coursefinder\CoursefinderController@coursefinder9');
Route::get('/coursefinder10', 'Coursefinder\CoursefinderController@coursefinder10');
Route::get('/aboutus', 'Coursefinder\CoursefinderController@aboutus');
Route::get('/blog', 'Coursefinder\CoursefinderController@blog');
Route::get('/blogdetails', 'Coursefinder\CoursefinderController@blogdetails');
Route::get('/blogdetails1', 'Coursefinder\CoursefinderController@blogdetails1');
Route::get('/blogdetails2', 'Coursefinder\CoursefinderController@blogdetails2');
Route::get('/blogdetails3', 'Coursefinder\CoursefinderController@blogdetails3');
Route::get('/blogdetails4', 'Coursefinder\CoursefinderController@blogdetails4');
Route::get('/contactus', 'Coursefinder\CoursefinderController@contactus');
Route::get('/topcourses', 'Coursefinder\CoursefinderController@topcourses');
Route::get('/signup', 'Coursefinder\CoursefinderController@signup');
Route::get('/userlogin', 'Coursefinder\CoursefinderController@userlogin');
Route::get('/applynow/{id}', 'Coursefinder\CoursefinderController@applynow');
Route::get('/applynow1/{id}', 'Coursefinder\CoursefinderController@applynow1');
Route::get('/coursesearch', 'Coursefinder\CoursefinderController@coursesearch');
Route::get('/coursesearch1', 'Coursefinder\CoursefinderController@coursesearch1');
Route::get('/coursesearch2', 'Coursefinder\CoursefinderController@coursesearch2');
Route::get('/coursesearch3', 'Coursefinder\CoursefinderController@coursesearch3');
Route::get('/coursesearch4', 'Coursefinder\CoursefinderController@coursesearch4');
Route::get('/coursesearch5', 'Coursefinder\CoursefinderController@coursesearch5');
Route::get('/coursesearch6', 'Coursefinder\CoursefinderController@coursesearch6');
Route::get('/coursedetails/{id}', 'Coursefinder\CoursefinderController@coursedetails');
Route::get('/coursedetails2/{id}', 'Coursefinder\CoursefinderController@coursedetails2');
Route::get('/coursedetails3/{id}', 'Coursefinder\CoursefinderController@coursedetails3');
Route::get('/coursedetails4/{id}', 'Coursefinder\CoursefinderController@coursedetails4');
Route::get('/coursedetails5/{id}', 'Coursefinder\CoursefinderController@coursedetails5');
Route::get('/coursedetails6/{id}', 'Coursefinder\CoursefinderController@coursedetails6');
Route::post('/agents_savee', 'LA\MainAgentsController@agentRegistration');
Route::post('/registration_savee', 'LA\StudentsController@registration_save');
Route::post('/nurse_savee', 'LA\NursesController@nurse_save');
/*Route::post('/saveapplication', 'Auth\AuthController@saveapplication');
Route::post('/saveapplications', 'Auth\AuthController@saveapplications');*/
Route::post('/saveapplication', 'Coursefinder\CoursefinderController@saveapplication');
Route::post('/saveapplications', 'Coursefinder\CoursefinderController@saveapplications');

Route::get('/registration', 'LA\StudentsController@registration');
Route::resource('/registration_save', 'LA\StudentsController@registration_save');
Route::resource('/nurse_save', 'LA\NursesController@nurse_save');		
Route::get('/user/verify/{token}', 'LA\StudentsController@verifyUser');
Route::get('/user/verifycf/{token}', 'LA\StudentsController@verifyUsercf');
/* Agents save */
Route::resource('/agents_save', 'LA\MainAgentsController@agentRegistration');
/* ================== Homepage ================== */
//Route::get('/', 'Auth\AuthController@coursefinder');
Route::get('/', 'Coursefinder\CoursefinderController@coursefinder');
Route::get('/home', 'HomeController@index');
Route::auth();

/* ================== Access Uploaded Files ================== */
Route::get('files/{hash}/{name}', 'LA\UploadsController@get_file');

/*
|--------------------------------------------------------------------------
| Admin Application Routes
|--------------------------------------------------------------------------
*/

$as = "";
if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
	$as = config('laraadmin.adminRoute').'.';
	
	// Routes for Laravel 5.3
	Route::get('/logout', 'Auth\LoginController@logout');
	Route::resource(config('laraadmin.adminRoute') .'/registration', 'LA\StudentsController@registration');	
	
	//Route::resource(config('laraadmin.adminRoute') .'/coursefinder', 'Auth\AuthController@coursefinder');
	//Route::resource(config('laraadmin.adminRoute') .'/coursesearch', 'Auth\AuthController@coursesearch');

}



//echo config('laraadmin.adminRoute');exit;	
Route::group(['as' => $as, 'middleware' => ['auth', 'permission:ADMIN_PANEL']], function () {
	
	/* ================== Dashboard ================== */
	
	Route::get(config('laraadmin.adminRoute'), 'LA\DashboardController@index');
	Route::get(config('laraadmin.adminRoute'). '/dashboard', 'LA\DashboardController@index');
	
	/* ================== Users ================== */
	Route::resource(config('laraadmin.adminRoute') . '/users', 'LA\UsersController');
	Route::get(config('laraadmin.adminRoute') . '/user_dt_ajax', 'LA\UsersController@dtajax');
	
	/* ================== Uploads ================== */
	Route::resource(config('laraadmin.adminRoute') . '/uploads', 'LA\UploadsController');
	Route::post(config('laraadmin.adminRoute') . '/upload_files', 'LA\UploadsController@upload_files');
	Route::get(config('laraadmin.adminRoute') . '/uploaded_files', 'LA\UploadsController@uploaded_files');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_caption', 'LA\UploadsController@update_caption');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_filename', 'LA\UploadsController@update_filename');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_public', 'LA\UploadsController@update_public');
	Route::post(config('laraadmin.adminRoute') . '/uploads_delete_file', 'LA\UploadsController@delete_file');

	/* ================== Media ================== */
	Route::resource(config('laraadmin.adminRoute') . '/uploads_media', 'LA\UploadsMediaController');
	Route::post(config('laraadmin.adminRoute') . '/upload_files_media', 'LA\UploadsMediaController@upload_files_media');
	Route::get(config('laraadmin.adminRoute') . '/media_dt_ajax', 'LA\UploadsMediaController@dtajax');
	
	/* ================== Roles ================== */
	Route::resource(config('laraadmin.adminRoute') . '/roles', 'LA\RolesController');
	Route::get(config('laraadmin.adminRoute') . '/role_dt_ajax', 'LA\RolesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_module_role_permissions/{id}', 'LA\RolesController@save_module_role_permissions');
	
	/* ================== Permissions ================== */
	Route::resource(config('laraadmin.adminRoute') . '/permissions', 'LA\PermissionsController');
	Route::get(config('laraadmin.adminRoute') . '/permission_dt_ajax', 'LA\PermissionsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_permissions/{id}', 'LA\PermissionsController@save_permissions');
	
	
	/* ================== Employees ================== */
	Route::resource(config('laraadmin.adminRoute') . '/employees', 'LA\EmployeesController');
	Route::get(config('laraadmin.adminRoute') . '/employee_dt_ajax', 'LA\EmployeesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/change_password/{id}', 'LA\EmployeesController@change_password');
	Route::resource(config('laraadmin.adminRoute') . '/user_profile/{id}/edit', 'LA\EmployeesController@edit');
	Route::resource(config('laraadmin.adminRoute') . '/user_profile/{id}/view', 'LA\EmployeesController@show');
	Route::post(config('laraadmin.adminRoute') . '/deleteEmployeeMail/{id}/{employee}', 'LA\EmployeesController@deleteMail');
	
	/* ================== Managers ================== */
	Route::resource(config('laraadmin.adminRoute') . '/student/managers', 'LA\ManagersController');
	Route::resource(config('laraadmin.adminRoute') . '/nurse/managers', 'LA\ManagersController');
	Route::get(config('laraadmin.adminRoute') . '/managers_dt_ajax', 'LA\ManagersController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/changeManagerPass/{id}', 'LA\ManagersController@change_password');
	Route::post(config('laraadmin.adminRoute') . '/deleteManagersMail/{id}/{employee}', 'LA\ManagersController@deleteMail');
	
	/* ================== Consultants ================== */
	Route::resource(config('laraadmin.adminRoute') . '/student/consultants', 'LA\ConsultantsController');
	Route::resource(config('laraadmin.adminRoute') . '/nurse/consultants', 'LA\ConsultantsController');
	Route::get(config('laraadmin.adminRoute') . '/consultants_dt_ajax', 'LA\ConsultantsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/changeConsultantPass/{id}', 'LA\ConsultantsController@change_password');
	Route::post(config('laraadmin.adminRoute') . '/setTime/{id}', 'LA\ConsultantsController@set_time');
	Route::post(config('laraadmin.adminRoute') . '/deleteConsultantMail/{id}/{employee}', 'LA\ConsultantsController@deleteMail');
	Route::post(config('laraadmin.adminRoute') . '/updateConsultantMail/{id}', 'LA\ConsultantsController@updateRead');
	/*Route::get(config('laraadmin.adminRoute') . '/staff/disable/{id}', 'LA\ConsultantsController@disable');
	Route::get(config('laraadmin.adminRoute') . '/staff/enable/{id}', 'LA\ConsultantsController@enable');*/

	Route::get(config('laraadmin.adminRoute') . '/student/staff/disable/{id}', 'LA\ConsultantsController@disable');
	Route::get(config('laraadmin.adminRoute') . '/student/staff/enable/{id}', 'LA\ConsultantsController@enable');
	Route::get(config('laraadmin.adminRoute') . '/nurse/staff/disable/{id}', 'LA\ConsultantsController@disable');
	Route::get(config('laraadmin.adminRoute') . '/nurse/staff/enable/{id}', 'LA\ConsultantsController@enable');
	
	/* ================== Main agents ================== */
	Route::resource(config('laraadmin.adminRoute') . '/student/main_agents', 'LA\MainAgentsController');
	Route::resource(config('laraadmin.adminRoute') . '/nurse/main_agents', 'LA\MainAgentsController');
	Route::get(config('laraadmin.adminRoute') . '/mainAgents_dt_ajax', 'LA\MainAgentsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/changeMainagentPass/{id}', 'LA\MainAgentsController@change_password');
	Route::post(config('laraadmin.adminRoute') . '/deleteMainAgentMail/{id}/{employee}', 'LA\MainAgentsController@deleteMail');

	Route::get(config('laraadmin.adminRoute') . '/student/mainagent/disable/{id}', 'LA\MainAgentsController@disable');
	Route::get(config('laraadmin.adminRoute') . '/student/mainagent/enable/{id}', 'LA\MainAgentsController@enable');
	Route::get(config('laraadmin.adminRoute') . '/nurse/mainagent/disable/{id}', 'LA\MainAgentsController@disable');
	Route::get(config('laraadmin.adminRoute') . '/nurse/mainagent/enable/{id}', 'LA\MainAgentsController@enable');
	
	/* ================== University users ================== */
	Route::resource(config('laraadmin.adminRoute') . '/university_users', 'LA\UniversityUserController');
	Route::get(config('laraadmin.adminRoute') . '/universityUsers_dt_ajax', 'LA\UniversityUserController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/changeUniversityPass/{id}', 'LA\UniversityUserController@change_password');
	Route::post(config('laraadmin.adminRoute') . '/deleteUniversityuserMail/{id}/{employee}', 'LA\UniversityUserController@deleteMail');

	Route::get(config('laraadmin.adminRoute') . '/universityuser/disable/{id}', 'LA\UniversityUserController@disable');
	Route::get(config('laraadmin.adminRoute') . '/universityuser/enable/{id}', 'LA\UniversityUserController@enable');
	
	/* ================== Sub-contractors ================== */
	Route::resource(config('laraadmin.adminRoute') . '/student/sub_contractors', 'LA\SubContractorsController');
	Route::resource(config('laraadmin.adminRoute') . '/nurse/sub_contractors', 'LA\SubContractorsController');
	Route::get(config('laraadmin.adminRoute') . '/subContractors_dt_ajax', 'LA\SubContractorsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/changesubContractorsPass/{id}', 'LA\SubContractorsController@change_password');
	Route::post(config('laraadmin.adminRoute') . '/deleteSubcontractorsMail/{id}/{employee}', 'LA\SubContractorsController@deleteMail');

	Route::get(config('laraadmin.adminRoute') . '/student/subcontractor/disable/{id}', 'LA\SubContractorsController@disable');
	Route::get(config('laraadmin.adminRoute') . '/student/subcontractor/enable/{id}', 'LA\SubContractorsController@enable');
	Route::get(config('laraadmin.adminRoute') . '/nurse/subcontractor/disable/{id}', 'LA\SubContractorsController@disable');
	Route::get(config('laraadmin.adminRoute') . '/nurse/subcontractor/enable/{id}', 'LA\SubContractorsController@enable');
	
	/* ================== Employers ================== */
	Route::resource(config('laraadmin.adminRoute') . '/employer', 'LA\EmployerController');
	Route::get(config('laraadmin.adminRoute') . '/employer_dt_ajax', 'LA\EmployerController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/changeEmployerPass/{id}', 'LA\EmployerController@change_password');
	Route::post(config('laraadmin.adminRoute') . '/deleteEmployerMail/{id}/{employee}', 'LA\EmployerController@deleteMail');

	Route::get(config('laraadmin.adminRoute') . '/employer/disable/{id}', 'LA\EmployerController@disable');
	Route::get(config('laraadmin.adminRoute') . '/employer/enable/{id}', 'LA\EmployerController@enable');
	Route::get(config('laraadmin.adminRoute') . '/updateNotify/{id}', 'LA\EmployeesController@updateRead');
	
	/* ================== Students ================== */
	Route::resource(config('laraadmin.adminRoute') . '/students', 'LA\StudentsController');
	Route::resource(config('laraadmin.adminRoute') . '/students_personal', 'LA\StudentsController@registration');
	Route::get(config('laraadmin.adminRoute') . '/students_dt_ajax', 'LA\StudentsController@dtajax');
	Route::get(config('laraadmin.adminRoute') . '/students_data/{id}/{type}', 'LA\StudentsController@studentData');
	Route::post(config('laraadmin.adminRoute') . '/students_doc_approve', 'LA\StudentsController@approveDocument');
	Route::post(config('laraadmin.adminRoute') . '/updateStudentNotif/{id}', 'LA\StudentsController@updateRead');
	Route::post(config('laraadmin.adminRoute') . '/deleteStudentNotif/{id}/{studentId}', 'LA\StudentsController@deleteMail');
	Route::post(config('laraadmin.adminRoute') . '/students_assign', 'LA\StudentsController@assignConsultant');
	Route::get(config('laraadmin.adminRoute') . '/students/approve', 
			array('uses'=>'StudentsController@update','as' => 'admin.students.approve'));
	Route::post(config('laraadmin.adminRoute') . '/students_unv_assign', 'LA\StudentsController@assignUniversityUser');
	Route::post(config('laraadmin.adminRoute') . '/changeStudentPass/{id}', 'LA\StudentsController@change_password');
	Route::post(config('laraadmin.adminRoute') . '/students_status', 'LA\StudentsController@assignStatus');
	Route::post(config('laraadmin.adminRoute') . '/students_followup', 'LA\StudentsController@assignFollowup');
	
	/* ================== Nurses ================== */
	Route::resource(config('laraadmin.adminRoute') . '/nurses', 'LA\NursesController');
	Route::get(config('laraadmin.adminRoute') . '/nurses_dt_ajax', 'LA\NursesController@dtajax');
	Route::get(config('laraadmin.adminRoute') . '/nurse_data/{id}/{type}', 'LA\NursesController@nurseData');
	Route::post(config('laraadmin.adminRoute') . '/nurses_assign', 'LA\nurses@assignConsultant');

	Route::post(config('laraadmin.adminRoute') . '/nurse_assign', 'LA\NursesController@assignConsultant');

	Route::get(config('laraadmin.adminRoute') . '/nurses/approve', 
			array('uses'=>'NursesController@update','as' => 'admin.nurses.approve'));
	Route::post(config('laraadmin.adminRoute') . '/nurses_unv_assign', 'LA\NursesController@assignUniversityUser');
	Route::post(config('laraadmin.adminRoute') . '/updateNurseNotif/{id}', 'LA\NursesController@updateRead');
	Route::post(config('laraadmin.adminRoute') . '/deleteNurseNotif/{id}/{studentId}', 'LA\NursesController@deleteMail');
	//Route::post(config('laraadmin.adminRoute') . '/nurses_doc_approve', 'LA\NursesController@approveDocument');
	Route::get(config('laraadmin.adminRoute') . '/nurses_doc_approve', 'LA\NursesController@approveDocument');
	Route::post(config('laraadmin.adminRoute') . '/changeNursePass/{id}', 'LA\NursesController@change_password');
	
	/* ================== Time-tracking-settings functionality page ================== */
	Route::resource(config('laraadmin.adminRoute') . '/settings/timetracking', 'LA\TimetrackingController');
	Route::resource(config('laraadmin.adminRoute') . '/settings/timetracking/edit', 'LA\TimetrackingController@show');
	Route::get(config('laraadmin.adminRoute') . '/settings/timetracking/update', 
			array('uses'=>'App\Controllers\LA\TimetrackingController@update','as' => 'timetracking.post.create'));
	
	/* ================== Mail-settings functionality page ================== */
	Route::resource(config('laraadmin.adminRoute') . '/mailsettings', 'LA\MailSettingsController');
	Route::get(config('laraadmin.adminRoute') . '/mailsettings/sendmail', 
			array('uses'=>'App\Controllers\LA\MailSettingsController@update','as' => 'mailsettings.sendmail'));
			
	/* ================== Courses ================== */
	Route::resource(config('laraadmin.adminRoute') . '/courses', 'LA\CousesController');
	Route::post(config('laraadmin.adminRoute') . '/upload_files_course', 'LA\UploadsController@upload_files_course');
	
	/* ================== Campus ================== */
	Route::resource(config('laraadmin.adminRoute') . '/campus', 'LA\CampusController');

	/* ================== Student Status ================== */
	Route::resource(config('laraadmin.adminRoute') . '/studentstatus', 'LA\StudentstatusController');

	/* ================== Stream ================== */
	Route::resource(config('laraadmin.adminRoute') . '/stream', 'LA\StreamController');

	/* ================== Countries ================== */
	Route::resource(config('laraadmin.adminRoute') . '/countries', 'LA\CountriesController');

	/* ================== Universities ================== */
	Route::resource(config('laraadmin.adminRoute') . '/university', 'LA\UniversityController');

	/* ================== Syllabus ================== */
	Route::resource(config('laraadmin.adminRoute') . '/syllabus', 'LA\SyllabusController');
	
	/* ================== Organizations ================== */
	Route::resource(config('laraadmin.adminRoute') . '/organizations', 'LA\OrganizationsController');
	Route::get(config('laraadmin.adminRoute') . '/organization_dt_ajax', 'LA\OrganizationsController@dtajax');
	
	/* ================== Departments ================== */
	Route::resource(config('laraadmin.adminRoute') . '/departments', 'LA\DepartmentsController');
	Route::get(config('laraadmin.adminRoute') . '/department_dt_ajax', 'LA\DepartmentsController@dtajax');

	/* ================== Nursefiles ================== */
	Route::resource(config('laraadmin.adminRoute') . '/nursefiles', 'LA\NursefilesController');
	Route::get(config('laraadmin.adminRoute') . '/nursefiles/sendmail', 
			array('uses'=>'App\Controllers\LA\NursefilesController@update','as' => 'nursefiles.sendmail'));

	/* ================== Report ================== */
	Route::resource(config('laraadmin.adminRoute') . '/report', 'LA\ReportController');
	Route::get(config('laraadmin.adminRoute') . '/report_dt_ajax', 'LA\ReportController@dtajax');
	Route::get(config('laraadmin.adminRoute') . '/searchreport_dt_ajax', 'LA\ReportController@searchdtajax');

	/* ================== Course Finder ================== */
	Route::resource(config('laraadmin.adminRoute') . '/coursefind', 'LA\CoursefindController');
	Route::get(config('laraadmin.adminRoute') . '/coursefind_dt_ajax', 'LA\CoursefindController@dtajax');
	Route::get(config('laraadmin.adminRoute') . '/searchcoursefind_dt_ajax', 'LA\CoursefindController@searchdtajax');
	Route::get(config('laraadmin.adminRoute') . '/coursefind/viewapplication/{id}', 'LA\CoursefindController@show');
	Route::resource(config('laraadmin.adminRoute') . '/coursefind/{id}/edit', 'LA\CoursefindController@edit');

	/* ================== View Course ================== */
	Route::get(config('laraadmin.adminRoute') . '/courses/viewcourse/{id}', 'LA\CousesController@showcourse');

	/* ================== Backups ================== */
	Route::resource(config('laraadmin.adminRoute') . '/backups', 'LA\BackupsController');
	Route::get(config('laraadmin.adminRoute') . '/backup_dt_ajax', 'LA\BackupsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/create_backup_ajax', 'LA\BackupsController@create_backup_ajax');
	Route::get(config('laraadmin.adminRoute') . '/downloadBackup/{id}', 'LA\BackupsController@downloadBackup');
});
