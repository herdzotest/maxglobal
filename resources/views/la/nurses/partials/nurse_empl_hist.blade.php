<div id="tab_3" class="tab-pane" >
                                        <div class="content-head">
                                        
                                                        <h4>Employment History</h4>
                                                        <!-- <div class="id">Nurse ID : <b>#12675</b></div> -->
                                        </div>
                                        {!! Form::open(['action' => 'LA\NursesController@store', 'id' => 'employement-history']) !!}

											<div class="pane">
												<div id="employement-wrapper">
												<div class="stdhst">
														<div class="index">1</div>
														<div class="col-md-6">
															<div class="mg-input-field ">
																<label for="employer_name">Job Title</label>
																<input class="form-control" id="job_title" name="job_title" type="text" required>										
															</div>
														</div>
														<div class="col-md-6">
															<div class="mg-input-field ">
																<label for="work_nature">Organization Name</label>
																<input class="form-control" id="org_name" name="org_name" type="text" required>								
															</div>
														</div>	
														<div class="col-md-6">
																<div class="mg-input-field ">
																	<label for="ward">Ward</label>
																	<select name="ward" id="ward" class="formSelect">
																		<option value="">Select</option>
																		<option value="Ward 1">Ward  1</option>
																		<option value="Ward 2">Ward 2</option>
																	</select>
																</div>
														</div>														
														<div class="col-md-6">
																<div class="mg-input-field ">
																		<label for="employement_start_date">Start Date</label>
																		<input class="form-control datepicker" id="employement_start_date" name="employement_start_date" type="text">
																</div>
														</div>
														<div class="col-md-6">
																<div class="mg-input-field ">
																		<label for="to_date">End Date</label>
																		<input class="form-control datepicker" id="employement_end_date" name="employement_end_date" type="text">
																</div>
														</div>
														<div class="col-md-6">
																<div class="mg-input-field ">
																		<label for="to_date">Remarks</label>
																		<input class="form-control" id="remarks" name="remarks" type="text">
																</div>
														</div>
														<div class="col-md-6">
																<div class="mg-input-field ">
																	<label for="working_status">Still working</label>
																	<select name="working_status" id="working_status" class="formSelect">
																		<option value="">Select</option>
																		<option value="Yes">Yes</option>
																		<option value="No">No</option>
																	</select>																	
																</div>
														</div>														
														<div class="clearfix"></div>
												</div>
												</div>
												<input type="hidden" value="employement" name="formtype" class="formtype">
												<div class="btn addshbtn"> <span class="glyphicon glyphicon-plus"></span></div>
												<input type="hidden" value="1" class="wrapper-count">	
											
												<div class="row">
													<div class="input-field col s6">
														<input id="pers_statement" name="pers_statement" type="text" class="">
														<label for="pers_statement">Personal Statement</label>
													</div>
												</div>												

												<div class="row">
														<div class="col s6">
																<label>CV File Upload</label>
														<div class="file-field input-field">
																<div class="btn7">
																								<span>Upload</span>
																								<input type="file"><i class="fa fa-cloud-upload"></i> 
																						</div>
																				<div class="file-path-wrapper">
																						<input class="file-path validate" name="cv_file_upload" type="text">

																						<div>
																						</div>
																		
																				</div>
																</div>

																<div class="uploaded-file" id="cv_file_upload" data-type="file"></div>
														</div>
												</div>
												
											</div>
											
								
                        <div class="controls">
                                <a class="mgbtn2">Back</a>
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="employement_history">
                                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
       
                                {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                        </div>
                        {{ Form::close() }}
</div>