<div id="tab_5" class="tab-pane " >

<style>
.displaynone {
	display: none !important;
}
</style>

                                
                                <div class="content-head">
                                                <h4>English Language Evidence</h4>
                                                <!-- <div class="id">Nurse ID : <b>#12675</b></div> -->
                                        </div>
                                        {!! Form::open(['action' => 'LA\NursesController@store', 'id' => 'eng-lang-evidence-form']) !!}
               
                        <div class="pane eng-lang-evidence-wrapper">
                                
                                <div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
														<select name="eng_lang_evidence" id="eng_lang_evidence" class="formSelect">
															<option value="">Select</option>
                                                            <option value="An accepted English language test">An accepted English language test</option>
                                                            <option value="My Practice">My Practice</option>
                                                            <option value="My Training">My Training</option>
                                                            <option value="Not Sure">Not Sure</option>
														</select>
														<label for="eng_lang_evidence" style="margin-top: -20px;">What evidence of your English language do you have to support your application?</label>
                                                </div>


                                        </div>
                                        <div class=" col s6 ">
                                        <div class="input-field">
                                                        <select name="eng_lang_qualif" id="eng_lang_qualif" class="formSelect">
															<option value="">Select</option>
                                                            <option value="IELTS">IELTS</option>
                                                            <option value="OET">OET</option>
                                                            <option value="Min_1year">Minimum 1 year experience in a majority English speaking country</option>
														</select>												
													<label for="preferred_designation">I hold the following English language qualification </label>
											</div>


                                        </div>
                                </div>

                                <div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
                                                <select name="test_sittings" id="test_sittings" class="formSelect">
															<option value="">Select</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
														</select>												
													<label for="preferred_designation">No of test sittings</label>
                                                </div>


                                        </div>
										
										<div class="col s6 ieltsClass hide">
                                            <div class="input-field">
                                                <input name="ielts_passed_date"  id="ielts_passed_date" type="text" class="datepicker">
												<label for="passed_date">IELTS / OET Passed Date</label>
                                            </div>    
										</div>	
                                </div>
								
								<div class="row ieltsClass hide">
                                        <div class=" col s6 ">
                                                <div class="input-field">
                                                    <input name="ielts_number"  id="ielts_number" type="text">
													<label for="ielts_number">IELTS TRF number / OET candidate number</label>
                                                </div>
                                        </div>
										<div class="col s6">
												<label>IELTS TRF / OET Certificate</label>
												<div class="file-field input-field">


																<div class="btn7">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
																<div class="file-path-wrapper">
																		<input class="file-path validate" name="ielts_certificate"  type="text">
																	  

																</div>
												</div>
												<div class="uploaded-file" id="ielts_certificate" data-type="file"></div>											
										</div>	
                                </div>							
								<div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
                                                <input name="validity_date"  id="validity_date" type="text" class="datepicker">
												<label for="validity_date">Validity</label>
                                                </div>


                                        </div>
										
										<div class="col s6">
                                        <label>IELTS / OET TRF File Upload </label>
												<div class="file-field input-field">


																<div class="btn7">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
																<div class="file-path-wrapper">
																		<input class="file-path validate" name="ielts_file_upload"  type="text">
																	  

																</div>
												</div>
											  
												<div class="uploaded-file" id="ielts_file_upload" data-type="file"></div>
										</div>	
                                </div>
								
								<div id="eng-lang-wrapper" class="hide">
									<div class="stdhst">
											<div class="index">1</div>
											<div class="col-md-6">
												<div class="mg-input-field">
													<label for="ielts_country">Country</label>
													<select id="ielts_country" class="searchSelect" name="ielts_country" required>
														<option value="Antigua and Barbuda">Antigua and Barbuda</option>
														<option value="Anguilla">Anguilla</option>
														<option value="Australia">Australia</option>
														<option value="Bahamas">Bahamas</option>
														<option value="Barbados">Barbados</option>
														<option value="Belize">Belize</option>
														<option value="Bermuda">Bermuda</option>
														<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
														<option value="Canada">Canada</option>
														<option value="Cayman Islands">Cayman Islands</option>
														<option value="Dominica">Dominica</option>
														<option value="Falkland Islands">Falkland Islands</option>
														<option value="Gibraltar">Gibraltar</option>
														<option value="Grenada">Grenada</option>
														<option value="Guernsey">Guernsey</option>
														<option value="Guernsey">Guernsey</option>
														<option value="Ireland">Ireland</option>
														<option value="Jamaica">Jamaica</option>
														<option value="Malta">Malta</option>
														<option value="New Zealand">New Zealand</option>
														<option value="Saint Helena">Saint Helena</option>
														<option value="Ascension and Tristan da Cunha">Ascension and Tristan da Cunha</option>
														<option value="St Kitts and Nevis">St Kitts and Nevis</option>
														<option value="St Lucia">St Lucia</option>
														<option value="St Vincent and the Grenadines">St Vincent and the Grenadines</option>
														<option value="Trinidad and Tobago">Trinidad and Tobago</option>
														<option value="United Kingdom">United Kingdom</option>
														<option value="United States">United States</option>
													</select>							
												</div>
											</div>
											<div class="col-md-6">
												<div class="mg-input-field ">
													<label for="work_nature">Work start date</label>
													<input class="form-control datepicker" id="work_start_date" name="work_start_date" type="text">								
												</div>
											</div>													
											<div class="col-md-6">
													<div class="mg-input-field ">
															<label for="employement_start_date">End date</label>
															<input class="form-control datepicker" id="work_end_date" name="work_end_date" type="text">
													</div>
											</div>
											<div class="col-md-6">
													<div class="mg-input-field ">
															<label for="employer_addresss">Employer address</label>
															<input class="form-control" id="employer_addresss" name="employer_addresss" type="text">
													</div>
											</div>
											<div class="col-md-6">
													<div class="mg-input-field ">
															<label for="to_date">Email of employer</label>
															<input class="form-control" id="employer_email" name="employer_email" type="email">
													</div>
											</div>
											<div class="col-md-6">
													<div class="mg-input-field ">
															<label for="to_date">Reference letter</label>
															<input class="form-control" id="reference_letter" name="reference_letter" type="text">
													</div>
											</div>
											<div class="col-md-6">
												<div class="mg-input-field ">
														<label>NCC File Upload</label>
														<div class="file-field input-field">
														<div class="btn7">
																						<span>Upload</span>
																						<input type="file"><i class="fa fa-cloud-upload"></i> 
																				</div>
																		<div class="file-path-wrapper">
																				<input class="file-path validate" name="file_upload_nursing" type="text">

																				<div>
																				</div>
																
																		</div>
														</div>

														<div class="uploaded-file" id="file_upload_nursing" data-type="file"></div>
												</div>
											</div>
																					
											<div class="clearfix"></div>
									</div>
								</div>
								
								<input type="hidden" value="english" name="formtype" class="formtype">
								<div class="btn addshbtn displaynone addbtnenglishevidence"> <span class="glyphicon glyphicon-plus"></span></div>
								<input type="hidden" value="1" class="wrapper-count">									

                        </div>
                        <div class="controls">
                                        <a class="mgbtn2">Back</a>
                                        <input id="form_name" type="hidden" class="validate" name="type_of" value="english_language">
                                        <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                        {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
                </div>

                