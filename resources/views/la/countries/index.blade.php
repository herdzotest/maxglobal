@extends("la.layouts.app")

@section("contentheader_title", "Countries")
@section("contentheader_description", "Country listing")
@section("section", "Countries")
@section("sub_section", "Listing")
@section("htmlheader_title", "Country Listing")

@section("headerElems")
@la_access("Employees", "create")
	<button class="btn btn-success btn-sm pull-right addbtn" data-toggle="modal" id="addCourseModal" data-target="#AddModal"> <i class="icon-plus"></i> Add Country</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-hover mg-table ">
		<thead>
			<tr class="success">
				<th>Country</th>
				<th>Country short name</th>
				<th>Status</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
		<?php 
		if(count($countries) > 0) { 
			foreach($countries as $result) {
		?>
			<tr role="row" class="odd">
				<td><a href="javascript:void(0);"><?php echo $result->country; ?></a></td>
				<td><?php echo $result->short_name; ?></td>
				<td>
					<?php 
					$status = ($result->status == '1') ? "Active" : "InActive";
					echo $status; 
					?>
				</td>
				<td>
					<a href="#" class="btn btn-warning btn-xs mx-course-edit" data-status="<?php echo $result->status; ?>" data-desc="<?php echo $result->short_name; ?>" data-title="<?php echo $result->country; ?>" data-id="<?php echo $result->id; ?>"><i class="fa fa-edit"></i></a>
					<?php
					$output  = Form::open(['route' => [config('laraadmin.adminRoute') . '.countries.destroy', $result->id], 'method' => 'delete', 'style'=>'display:inline','onSubmit'=>"return confirm('Are you sure you wish to delete?');"]);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
					
					echo $output;
					?>
				</td>
			</tr>	
		<?php 
			}
		} else { ?>
			<td>No entry found!!</td>
		<?php 
		}
		?>			
		</tbody>
		</table>
	</div>
</div>

@endsection

@la_access("Employees", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Country</h4>
			</div>
			{!! Form::open(['action' => 'LA\CountriesController@store', 'id' => 'course-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
					<input type="hidden" value="" id="country_id" name="country_id">
					<div class="form-group">
						<label for="name">Country name* :</label>
						<input class="form-control" placeholder="Enter country" data-rule-maxlength="200" required id="country_name" name="country" type="text" value="">
					</div>
					<div class="form-group">
						<label for="name">Country short name :</label>
						<input class="form-control" placeholder="Enter short name" data-rule-maxlength="50" id="country_short_name" name="short_name" type="text" value="">
					</div>					
					<div class="form-group">
						<label for="status">Status :</label>
						<select name="status" id="country_status" class="form-control">
							<option value="1">Active</option>
							<option value="0">InActive</option>
						</select>
					</div>					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success">Submit</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@push('scripts')
<script>
$(function () {
	$("#course-add-form").validate({
	});
	$('body').off('click', '.mx-course-edit');
	$('body').on('click', '.mx-course-edit', function() {
		
		var country_id = $(this).attr('data-id');
		var title = $(this).attr('data-title');
		var desc = $(this).attr('data-desc');
		var status = $(this).attr('data-status');
		$("#country_id").val(country_id);
		$("#country_name").val(title);
		$("#country_short_name").val(desc);
		$("#country_status").val(status);
		$('#AddModal').modal('show');
	});
	$("#AddModal").on('hide.bs.modal', function(){
		$('#AddModal input,#AddModal textarea').val('');
		$('#AddModal select').val(1);
	});
});
</script>
@endpush
