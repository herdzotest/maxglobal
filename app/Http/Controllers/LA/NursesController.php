<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;
use Dwij\Laraadmin\Helpers\LAHelper;
use Dwij\Laraadmin\Models\LAConfigs;

use App\Models\Upload;
use App\User;
use App\Models\Employee;
use App\Role;
use Mail;
use Log;
use DateTime;

class NursesController extends Controller
{
	public $show_action = true;
	
	public function __construct() {
		
		$path = \Request::path();
		
		$userTypeAccess = array('SUPER_ADMIN','MANAGER','NURSE','EMPLOYER_ROLE','SUBCONTRACTORS');
		if ($path=="nurse_save" || $path=="nurse_savee") {
			
		} else if(!in_array(Auth::user()->type, $userTypeAccess)) {
			return abort(403);
		}	
	}

	public function index()
	{			
		$consultants = DB::table('employees')->select('employees.name')->addselect('users.id')
						->leftJoin('users', 'users.context_id', '=', 'employees.id')
						->where('employees.nurse-user', 1)
						->where('employees.role', 3)->whereNull('employees.deleted_at')->get();
						
		$employers = DB::table('employees')->select('employees.name')->addselect('users.id')
						->leftJoin('users', 'users.context_id', '=', 'employees.id')
						->where('employees.role', 7)->whereNull('employees.deleted_at')->get();

		$countries = DB::table('countries')->select('country')->where('status', 1)->get();

		return View('la.nurses.index', [
			'show_actions' => $this->show_action,
			'consultants' => $consultants,
			'universityAgents' => $employers,
			'countries' => $countries
		]);
	}
	
	public function nurse_save(Request $request)
	{

		//Recaptcha
    	$url = 'https://www.google.com/recaptcha/api/siteverify';
		$remoteip = $_SERVER['REMOTE_ADDR'];
		$data = [
		        'secret' => config('services.recaptcha.secret'),
		        'response' => $request->get('recaptcha'),
		        'remoteip' => $remoteip
		      ];
		$options = [
		        'http' => [
		          'header' => "Content-type: application/x-www-form-urlencoded\r\n",
		          'method' => 'POST',
		          'content' => http_build_query($data)
		        ]
		    ];
		$context = stream_context_create($options);
		        $result = file_get_contents($url, false, $context);
		        $resultJson = json_decode($result); 
		if ($resultJson->success != true) { 
		        return back()->withErrors(['captcha' => 'ReCaptcha Error']);
		        }
		if ($resultJson->score >= 0.3) { 
    	//End Recaptcha
			
		//echo '<pre>'; print_r($request->all()); exit;
		
		$path = \Request::path();

		if(count($request->all()) > 0) {
		
			$userId = 0;
			$studentsId=array();
		   $rndtime = 	time();
		   $rules = Module::validateRules("Employees", $request);
		  $validator = Validator::make($request->all(), $rules);
		  $this->emailId = $request->all()['email'];
		
		  $validator->after(function($validator) {
			  if ($this->newEmailValidation() > 0) {
				  $validator->errors()->add('email', 'Email already exists');
			  }
		  });
		  
		  if ($validator->fails()) {
			  return redirect()->back()->withErrors($validator)->withInput();
		  }
		  try {
			  $data = User::create([
				  'name' => $request->all()['first_name'],'context_id' => $rndtime,'email' => $request->all()['email'],
				  'password' => bcrypt( $request->all()['password']),'type' => 'NURSE','created_at' => date("Y-m-d h:i:sa"),
				  'updated_at' => date("Y-m-d h:i:sa"),'verified' => 1,
			  ]);

			  /*$data = User::create([
				  'name' => $request->all()['first_name'],'context_id' => $rndtime,'email' => $request->all()['email'],
				  'password' => bcrypt( $request->all()['password']),'type' => 'NURSE','created_at' => date("Y-m-d h:i:sa"),
				  'updated_at' => date("Y-m-d h:i:sa"),'verified' => 0,
			  ]);*/ // For demo change

			  //try {
				  $studentsId = DB::table('users')
				  ->select('users.id')
				  ->where('users.email', $request->all()['email'])->get();
				  // insert student role
				  $this->insertNurseRole($studentsId[0]->id);

				  //Log
				  DB::table('data_log')->insert(['user_id' => $studentsId[0]->id,'name' => $request->all()['first_name'],'email' => $request->all()['email'],'created_at' => date("Y-m-d h:i:sa") ]);
				  // End Log
			}
			
			//catch exception
			catch(Exception $e) {
			  $validator->errors()->add('user', 'User not  exists');

			  //Log
			  DB::table('data_log')->insert(['name' => $request->all()['first_name'],'email' => $request->all()['email'],'created_at' => date("Y-m-d h:i:sa") ]);
			  // End Log

			}
			$token=sha1(time());
			DB::table("verify_users")->insert([
				'user_id' => $studentsId[0]->id,
				'token' => $token
			  ]);
			  if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
				//	Send mail to User his new Password
			  if($path=="nurse_savee"){
			  	// Mail::send('emails.verify_cf', ['user' =>$request->all()['email'], 'token' =>$token,'name'=>$request->all()['first_name'], 'password' => $request->all()['password']], function ($m) use ($request) {
				// $m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
				// $m->to($request->all()['email'],  $request->all()['first_name'])->subject('Verify Email');
			    // });
			  } else {
			  	// Mail::send('emails.verify', ['user' =>$request->all()['email'], 'token' =>$token,'name'=>$request->all()['first_name'], 'password' => $request->all()['password']], function ($m) use ($request) {
				// $m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
				// $m->to($request->all()['email'],  $request->all()['first_name'])->subject('Verify Email');
			    // });
			  }
			  

			  $request1['stud_name'] = $request->all()['first_name'];
				$request1['stud_email'] = $request->all()['email'];
				$manager_email = DB::table('users')->select('email')->addselect('name')->where('type', 'MANAGER')->get();
				foreach($manager_email as $key => $result) {
					$request1['admin_name'] = $result->name;
					$request1['admin_email'] = $result->email;
					if($request1['admin_email'] != '') {
						//	Send mail to manager
						// Mail::send('emails.addnurse_admin', ['admin' => $request1['admin_name'],'user' =>$request1['stud_name'],'useremail' =>$request1['stud_email']], function ($m) use ($request1) {
						// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
						// 	$m->to($request1['admin_email'])->subject('Scholab - Nurse Registration');
						// }); 
					}
				}

			  
		  } else {
			  Log::info("User created username: ".$request->all()['email']);
		  }

		  // insert student data
		  $insertData = request()->except(['_token','type_of','email','student_id','password','user_type','std_agent_id','recaptcha']);
		  // format birth date
		  if($insertData['dob'] != '') {
			$bday = new DateTime($insertData['dob']); // Your date of birth
			$today = new Datetime(date('Y-m-d'));
			$diff = $today->diff($bday);
            $insertData['age'] = $diff->y;
			$insertData['dob'] = date("M d, Y", strtotime($insertData['dob']));
		  }

		  DB::table("nurse_".$request->all()['type_of'])->insert([
				array_merge($insertData, [ 'user_id' => $studentsId[0]->id ])
			]);

		  /*DB::table("nurse_".$request->all()['type_of'])->insert([
			array_merge(request()->except(['_token','type_of','email','student_id','password','user_type']), [ 'user_id' => $studentsId[0]->id ])

			  ]);
			return View('la.students.registration', [
				'show_actions' => $this->show_action
			]);*/
			//\Session::flash('status', 'Registration Successful, Please check your inbox for further instructions');
		    //return redirect('/registration');
		    if($path=="nurse_savee"){
		    	\Session::flash('status', 'Registration Successful, Please check your inbox for further instructions');
		  		return redirect('/signup');
		    } else {
		    	\Session::flash('status', 'Registration Successful, Please check your inbox for further instructions');
		    	return redirect('/registration');
		    }
		} else {
			return redirect('/login');
		}

		//Recaptcha
        } else {  
	        return back()->withErrors(['captcha' => 'ReCaptcha Error']);
		}
		//End Recaptcha
	}
	
	/**
	 * Show the form for editing the STUDENTS.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$subAgents = DB::table('employees')->select('employees.name')->where('employees.nurse-user', 1)->where('employees.role', 6)->whereNull('employees.deleted_at')->get();
		$employers = DB::select(DB::raw("SELECT name, (CASE WHEN (role = 7) THEN 'Emp' ELSE 'Agents' END) AS userType FROM employees WHERE (role=7 OR role=4) AND deleted_at IS NULL ORDER BY role"));

		$countries = DB::table('countries')->select('country')->where('status', 1)->get();

		$living = DB::table('nurse_personal_information')->select('json_data')->where('user_id', $id)->get();
		if($living && $living[0]->json_data != ''){ $wrp_count_living = count(json_decode($living[0]->json_data,true)) + 1; } else { $wrp_count_living = '1'; }

		//echo '<pre>'; print_r($employers); exit;
		
		return view('la.nurses.edit', [
			'studentId' => $id,
			'subAgents' => $subAgents,
			'employers' => $employers,
			'countries' => $countries,
			'wrp_count_living' => $wrp_count_living
		]);
	}
	
	public function store(Request $request)
	{
		//echo '<pre>'; print_r($request->all()); 
		
		$is_update=false; $recurring_flag = 0;
  		if(Module::hasAccess("Employees", "create")) {
			$this->userId = 0;
			$studentsId=array();
			if($request->all()['user_id']){
					$studentsId[] = (object) array('id' => $request->all()['user_id']);
				//	$studentsId[0]->id=$request->all()['student_id'];
				}
			if ($this->isUserExist("nurse_".$request->all()['type_of'],$request->all()['user_id']) > 0) {
				$is_update=true;
			}

			switch($request->all()['type_of']){
				case "personal_information":
					// insert on user table
					$rndtime = 	time();
					$rules = Module::validateRules("Employees", $request);
					$validator = Validator::make($request->all(), $rules);
					
					if(!$is_update){
						$this->emailId = $request->all()['email'];
						$validator->after(function($validator) {
						if ($this->emailValidation() > 0) {
							$validator->errors()->add('email', 'Email already exists');
						}
					});
					if ($validator->fails()) {
						\Session::put('posted_values', $request->all());
						return redirect()->back()->withErrors($validator)->withInput();
					}
					$data = User::create([
						'name' => $request->all()['first_name'],'context_id' => $rndtime,'email' => $request->all()['email'],
						'password' => bcrypt($rndtime),'type' => 'NURSE','created_at' => date("Y-m-d h:i:sa"),
						'updated_at' => date("Y-m-d h:i:sa")
					]);
					
					try {
						$studentsId = DB::table('users')
						->select('users.id')
						->where('users.email', $request->all()['email'])->get();
						
						// insert nurse role
						$this->insertNurseRole($studentsId[0]->id);				
					  }
					  //catch exception
					  catch(Exception $e) {
						$validator->errors()->add('user', 'User not  exists');
					  }
						if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
							//	Send mail to User his new Password
							// Mail::send('emails.send_login_cred', ['user' =>$request->all()['email'], 'token' => $request->all()['_token'],'password' => $rndtime], function ($m) use ($request) {
							// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
							// 	$m->to($request->all()['email'],  $request->all()['first_name'])->subject('LaraAdmin - Login Credentials Reset');
							// });
						} else {
							Log::info("User created username: ".$request->all()['email']);
						} 
					} else {
						DB::table('users')->where('id', $studentsId[0]->id)->update(['name' => $request->all()['first_name']]);	
					}
					$postData = $request->all();
					unset($postData['_token']); unset($postData['type_of']); unset($postData['formtype']);
					unset($postData['student_id']); 
					$recurring_flag = 1;
					if( isset($request->all()['otheraddress']) && count($request->all()['otheraddress']) > 0 ) {
						$json_datas = json_encode($request->all()['otheraddress'],true);
						unset($postData['otheraddress']);
						$postData['json_data'] = $json_datas;
					}	
					break;
				case "employement_history":
				case "english_language":
				case "dependant_det":
				case "visa_imigration":
				
					//echo '<pre>'; print_r($request->all());
				
					$formtype = $request->all()['formtype'];
					$postData = $request->all();
					unset($postData['_token']); unset($postData['type_of']); unset($postData['formtype']);
					unset($postData['student_id']); 
					$recurring_flag = 1;
					if( isset($request->all()[$formtype]) && count($request->all()[$formtype]) > 0 ) {
						$json_datas = json_encode($request->all()[$formtype],true);
						unset($postData[$formtype]);
						$postData['json_data'] = $json_datas;
					}
				break;
				default:
					break;
			}
			
			//echo '<pre>'; print_r($postData); exit;
			
			if ($is_update) {
				if($recurring_flag) {
					DB::table("nurse_".$request->all()['type_of'])
					->where('user_id', $request->all()['user_id'])
					->update($postData);	
				} else {
					DB::table("nurse_".$request->all()['type_of'])
					->where('user_id', $request->all()['user_id'])
					->update(request()->except(['_token','type_of','email','student_id']));					
				}
			} else if($studentsId[0]->id){
				if($recurring_flag) {
					DB::table("nurse_".$request->all()['type_of'])->insert([
						array_merge($postData, [ 'user_id' => $studentsId[0]->id ])
					]);					
				} else {
					DB::table("nurse_".$request->all()['type_of'])->insert([
						array_merge(request()->except(['_token','type_of','email','student_id']), [ 'user_id' => $studentsId[0]->id ])
					]);
				}
			} 			
			
			return redirect(config('laraadmin.adminRoute').'/nurses/'.$studentsId[0]->id."/edit");
		} else {
			return redirect(config('laraadmin.adminRoute')."/nurses/0/edit");
		}
	}
	
	public function insertNurseRole($userId)
	{
		DB::table('role_user')->insert([
			'role_id' => config('constants.NURSE_ID'),'user_id' => $userId
		]);		
	}
	  
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		if(Auth::user()->type == 'CONSULTANTS') {
			$students = DB::table('users')
						->select('users.email')->addselect('nurse_personal_information.first_name')->addselect('nurse_personal_information.dob')
						->addselect('nurse_personal_information.sur_name')->addselect('users.id')->addselect('users.assigned_to')->addselect('nurse_personal_information.profile_image')
						->addselect('users.is_approved')->addselect('users.assigned_to_agents')
						->leftJoin('nurse_personal_information', 'users.id', '=', 'nurse_personal_information.user_id')
						->where('users.assigned_to', Auth::user()->id)
						->where('users.type', 'NURSE')->whereNull('users.deleted_at')->orderBy('users.created_at', 'desc')->get();			
		} else if(Auth::user()->type == 'EMPLOYER_ROLE') {
			$students = DB::table('users')
						->select('users.email')->addselect('nurse_personal_information.first_name')->addselect('nurse_personal_information.dob')
						->addselect('nurse_personal_information.sur_name')->addselect('users.id')->addselect('users.assigned_to')
						->addselect('users.is_approved')->addselect('users.assigned_to_agents')->addselect('nurse_personal_information.profile_image')
						->leftJoin('nurse_personal_information', 'users.id', '=', 'nurse_personal_information.user_id')
						->where('users.assigned_to_agents', Auth::user()->id)
						->where('users.type', 'NURSE')->whereNull('users.deleted_at')->orderBy('users.created_at', 'desc')->get();				
		} else {
			$students = DB::table('users')
						->select('users.email')->addselect('nurse_personal_information.first_name')->addselect('nurse_personal_information.dob')
						->addselect('nurse_personal_information.sur_name')->addselect('users.id')->addselect('users.assigned_to')
						->addselect('users.is_approved')->addselect('users.assigned_to_agents')->addselect('nurse_personal_information.profile_image')
						->leftJoin('nurse_personal_information', 'users.id', '=', 'nurse_personal_information.user_id')
						->where('users.type', 'NURSE')->whereNull('users.deleted_at')->orderBy('users.created_at', 'desc')->get();
		}
		
		$out =  array();
		$out['data'] = array();
		$out['draw'] = count($students);
		$out['recordsTotal'] = count($students);
		$out['recordsFiltered'] = count($students);
		
		foreach($students as $key => $result) {
			
			$studentData = array();
			$studentData[] = $key+1;
			$avatatImg = ($result->profile_image == '') ? 'https://www.gravatar.com/avatar/ef1c169902a351837b6c7f8816cf9b5c.jpg?s=80&d=mm&r=g' : $result->profile_image;
			$studentData[] = '<a href="'.url(config('laraadmin.adminRoute') . '/nurses/'.$result->id).'"><img src="'.$avatatImg.'" alt=""/><span>'.$result->first_name.' '.$result->sur_name.'</span></a>';
			$studentData[] = $result->email;

			// students status
			if($result->is_approved == '1') {
				$status = '<span  class="mg-status approved">Approved</span>';
			} else if($result->is_approved == '0') {
				$status = '<span mg-status  class="mg-status rejected">Rejected</span>';
			} else {
				$status = '<span class="mg-status pending">Pending</span>';
			}
			$studentData[] = (string)$status;
			
			if($result->assigned_to === null) {
				$assigneeName = '';	$profilePic = '';
			} else {
				$assignedTo = $result->assigned_to;
				$assignee = DB::select( DB::raw("SELECT employees.id AS assigneeId,employees.name,employees.profilePic FROM users
								LEFT JOIN employees ON users.context_id=employees.id 
								WHERE users.id=$assignedTo") );
				$assigneeName = $assignee[0]->name;
				$assigneeId = $assignee[0]->assigneeId;
				$profilePic = ($assignee[0]->profilePic == '') ? 'https://www.gravatar.com/avatar/ef1c169902a351837b6c7f8816cf9b5c.jpg?s=80&d=mm&r=g' : $assignee[0]->profilePic;
			}
			
			//$studentData[] = ($result->assigned_to != null) ? '<a target="_blank" href="'.url(config('laraadmin.adminRoute') . '/consultants/'.$result->assigned_to).'"><img src="'.$profilePic.'" alt="User Image"/><span>'.$assigneeName.'</span></a> (Consultant)' : '';
			$studentData[] = ($result->assigned_to != null) ? '<a target="_blank" href="'.url(config('laraadmin.adminRoute') . '/nurse/consultants/'.$assigneeId).'"><span>'.$assigneeName.'</span></a>' : '';
			
			if($this->show_action) {
				$output = '';
				$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/nurses/'.$result->id.'/edit').'" class="btn btn-warning btn-xs editbtn" ><i class="fa fa-pencil"></i></a>';
				$output .= '<a target="_blank" href="'.url(config('laraadmin.adminRoute') . '/mailsettings?email='.$result->id.'_'.$result->email).'" data-email="'.$result->email.'" class="btn btn-warning btn-xs mailbtn" ><i class="fa fa-envelope"></i></a>';
				
				// students assigning
				if($result->is_approved == null) {
					$output .= '<a title="Assign consultant" data-nurse="'.$result->first_name.'" data-email="'.$result->email.'" data-user="'.$result->id.'" data-toggle="modal" data-target="#AddModal" href="#" class="btn btn-warning btn-xs stud-assign addcbtn" style=""><i class="fa fa-user-plus"></i></a>';	
				} else if($result->is_approved == "1" && $result->assigned_to_agents == null) {
					$output .= '<a title="Assign to Employers" data-email="'.$result->email.'" data-user="'.$result->id.'" data-toggle="modal" data-target="#universityUserModal" href="#" class="btn btn-warning btn-xs stud-assign-university" ><i class="fa fa-user-plus"></i></a>';	
				}
				
				if($result->is_approved === null) {
					// student approve form
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.nurses.approve'], 'method' => 'PUT', 'style'=>'display:inline','onSubmit'=>"return confirm('This action will approve the nurse?');"]);
					$output .= '<input type="hidden" value="'.$result->email.'" name="user_email">';
					$output .= '<input type="hidden" value="'.$result->id.'" name="user_id">';
					$output .= '<input type="hidden" value="1" name="user_status">';
					$output .= ' <button title="Approve nurse" class="btn btn-warning btn-xs apprbtn" type="submit"><i class="fa fa-check"></i></button>';
					$output .= Form::close();
					// student reject form
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.nurses.approve'], 'method' => 'PUT', 'style'=>'display:inline','onSubmit'=>"return confirm('This action will reject the nurse?');"]);
					$output .= '<input type="hidden" value="'.$result->email.'" name="user_email">';
					$output .= '<input type="hidden" value="'.$result->id.'" name="user_id">';
					$output .= '<input type="hidden" value="0" name="user_status">';
					$output .= ' <button title="Reject nurse" class="btn btn-danger btn-xs rejcbtn" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				
				$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.nurses.destroy', $result->id], 'method' => 'delete', 'style'=>'display:inline','onSubmit'=>"return confirm('Are you sure you wish to delete?');"]);
				$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-trash"></i></button>';
				$output .= Form::close();
				// nurse uploaded files
				$output .= '<a target="_blank" href="'.url(config('laraadmin.adminRoute') . '/nursefiles?id='.$result->id).'" class="btn btn-warning btn-xs mailbtn" title="Nurse uploaded files"><i class="fa fa-files-o"></i></a>';

				$studentData[] = (string)$output;
			}

			array_push($out['data'],$studentData);
		}
		$out = json_encode($out,true);
		
		return $out;
	}
	
	public function update(Request $request)
	{
		if(count($request->all()) > 0) {
			$postData = $request->all();
			$email = $postData['user_email'];
			DB::table('users')->where('id', $postData['user_id'])->update([ 'is_approved' => $postData['user_status'] ]);
		} 
		
		return redirect(config('laraadmin.adminRoute') . '/nurses');
	}
	
	/**
	 * Remove from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if($id != '') {
			$users = DB::table('users')->find($id);
			if($users) {
				DB::table('users')->where('id', $id)->update(['deleted_at' => date("Y-m-d H:i:s")]);
			}
		}
		
		return redirect()->route(config('laraadmin.adminRoute') . '.nurses.index');
	}
	
	public function show($id) 
	{
		$students = DB::table('users')
					->select('users.email')->addselect('nurse_personal_information.first_name')->addselect('nurse_personal_information.dob')
					->addselect('nurse_personal_information.sur_name')->addselect('users.id')->addselect('nurse_personal_information.middle_name')
					->addselect('users.is_approved')
					->leftJoin('nurse_personal_information', 'users.id', '=', 'nurse_personal_information.user_id')
					->whereNull('users.deleted_at')
					->where('users.id', $id)->get();		
					
		$mailDetails = DB::table('mail_settings')
					->select('mail_settings.id')->addselect('mail_settings.mail_content')->addselect('mail_settings.subject')
					->addselect('us.name')->addselect('us.type')->addselect('mail_settings.send_date')
					->leftJoin('users', 'users.id', '=', 'mail_settings.to_user')
					->leftJoin('users AS us', 'us.id', '=', 'mail_settings.from_user')
					->where('mail_settings.is_deleted', 0)
					->where('users.context_id', $id)->orderBy('mail_settings.send_date', 'desc')->get();
					
		$notifications = DB::table('notifications')->select('id')->addselect('notif_dat')->addselect('user_id')
				->addselect('doc_type')->addselect('comments')->addselect('status')->addselect('approval_text')
				->where('is_deleted', 0)->where('user_id', $id)->orderBy('notif_dat', 'desc')->get();
				
		return view('la.nurses.show', [
			'no_header' => true,
			'no_padding' => "no-padding",
			'students' => $students[0],
			'mailDetails' => $mailDetails, 'notifications' => $notifications
		]);
	}
	
	public function nurseData($id,$type)
	{
		switch($type){
			case "tab_1":
				$table = 'nurse_personal_information';
				break;
			case "tab_2":
				$table = 'nurse_qualifications';
				break;
			case "tab_3":
				$table = 'nurse_employement_history';
				break;
			case "tab_4":
				$table = 'nurse_preferences';
				break;
			case "tab_5":
				$table = 'nurse_english_language';
				break;
			case "tab_6":
				$table = 'nurse_employer_details';
				break;
			case "tab_7":
				$table = 'nurse_nmc';
				break;
			case "tab_8":
				$table = 'nurse_dependant_det';
				break;
			case "tab_9":
				$table = 'nurse_visa_imigration';
				break;
			case "tab_10":
				$table = 'nurse_joining';
				break;
			case "tab_11":
				$table = 'nurse_feedback';
				break;
			case "tab_12":
				$table = 'nurse_payments';
				break;
			default:
				break;
		}
		$where = $table.'.user_id';
		$data = DB::table($table)->where($where, $id)->get();
		$details = (count($data) > 0) ? $data[0] : $data;

		return json_encode($details,true);
	}
	public function emailValidation()
	{
		$email = DB::table('users')
				->whereNotIn('id', [$this->userId])
				->where('email', $this->emailId)
				->get();
				
		return count($email);
	}
	public function newEmailValidation()
	{
		$email = DB::table('users')
		->where('email', $this->emailId)
		->get();
		
		return count($email);
	}
	
	//public function approveDocument(Request $request)
	public function approveDocument()
	{  
		// New Code
		if(isset($_GET['id'])){ $id = $_GET['id']; } else {$id = ''; }
		if(isset($_GET['type'])){ $type = $_GET['type']; } else {$type = ''; }
		if(isset($_GET['statusvalue'])){ $status = $_GET['statusvalue']; } else {$status = ''; }
		if(isset($_GET['comment'])){ $comments = $_GET['comment']; } else {$comments = ''; }
		// End New code
		//if(count($request->all()) > 0) {
		if($id != '' && $type != '' && $status != '') {  // New code
			/*$id = $request->all()['id'];
			$type = $request->all()['type'];
			$status = $request->all()['statusvalue'];
			$comments = $request->all()['comment'];*/
			
			switch($type){
				case "passport_upload":
					$table = 'nurse_personal_information';
					break;
				case "certificate_upload":
				case "evidence_upload":
					$table = 'nurse_qualifications';
					break;
				case "cv_file_upload":
					$table = 'nurse_employement_history';
					break;
				case "ielts_certificate":
			    case "ielts_file_upload":
			    case "file_upload_nursing":
					$table = 'nurse_english_language';
					break;
				case "addl_docs":
					$table = 'nurse_payments';
					break;
				case "scope_file_upload":
				case "cbt_upload":
				case "nmc_file_upload":
			    case "birth_cert_upload":
					$table = 'nurse_nmc';
					break;
				case "spouse_passport":
					$table = 'nurse_dependant_det';
					break;
				case "image_front":
				case "image_back":
				case "qualification-file":
				case "ielts_file":
					$table = 'nurse_passport_details';
					break;
				case "cv_upload_file":
					$table = 'nurse_employement_info';   
					break;
				/*case "visa_file":
				case "visa_appr_file":
				case "flight_ticket":
					$table = 'nurse_visa';   
					break;*/
				case "appl_form":
				case "refer_file":
				case "pers_stat_file":
				case "tb_file_upload":
				case "pcc_file_upload":
				case "spons_file_upload":
				case "visa_file":
				case "visa_appr_file":
				case "flight_ticket":
					//$table = 'nurse_pcc_nhs'; 
					$table = 'nurse_visa_imigration';  
					break;
				case "nmc_file":
					$table = 'nurse_nmc';
					break;	
				case "offer_letter_file":  
					//$table = 'nurse_agent_info';
					$table = 'nurse_employer_details';
					break;
				case "cbt_score_upload":
					$table = 'nurse_cbt';
					break;				
				default:
					break;
			}
			$column = $type."_approval";
			$approvalText = $this->docapprovalText($type,$status);

			DB::table($table)->where('user_id', $id)->update([$column => $status]);
			DB::table('notifications')->insert(['user_id' => $id,'user_type' => 'NURSE','doc_type' => $type,
				'comments' => $comments,'status' => $status,'approval_text' => $approvalText]);
			
			return "SUCCESS"; 
		} else {
			return "FAILED"; 
		}
	}
	
	public function docapprovalText($type,$status)
	{
		$statusText = ($status == '1') ? 'APPROVED' : 'REJECTED';
		// documents types array
		$docarray = array('passport_upload' => 'Passport front and back image','image_front' => 'Passport front image','qualification-file' => 'Qualification file', 'certificate_upload' => 'Qualification file', 'cv_file_upload' => 'CV file', 'ielts_certificate' => 'IELTS Certificate', 'ielts_file_upload' => 'IELTS file', 'file_upload_nursing' => 'NCC file', 'ielts_file' => 'IELTS file',
				'cv_upload_file' => 'CV file', 'visa_file' => 'Visa doc',
				'visa_appr_file' => 'Visa approval doc', 'flight_ticket' => 'Flight ticket',
				'appl_form' => 'Application form', 'refer_file' => 'Reference doc',
				'pers_stat_file' => 'personal status doc', 'tb_file_upload' => 'TB doc',
				'pcc_file_upload' => 'PCC certificate', 'spons_file_upload' => 'Sponsor doc',
				'nmc_file' => 'NMC doc', 'offer_letter_file' => 'Offer letter','cbt_score_upload' => 'CBT score doc',
				'evidence_upload' => 'Evidence upload doc','oet_certificate' => 'OET certificate doc',
				'registration_cert' => 'Nurse council Registration doc','birth_certificate' => "Birth certificate",'scope_file_upload' => "Scope File",
				'addl_docs' => "Additional documents", 'cbt_upload' => "CBT Score File", 'nmc_file_upload' => "NMC File", 'birth_cert_upload' => "Birth Certificate",'spouse_passport' => 'Spouse passport front and back image',
				'addl_doc' => 'Additional documents' 
			);
		$returnText = "Your ".$docarray[$type]." has been ".$statusText;
		
		return $returnText;
	}
	
	public function updateRead($id)
	{
		DB::table('notifications')->where('id', $id)->update(['is_read' => 1]);
	}
	
	/**
	 * delete notifications
	 *
	 * @param  int  $id
	 * @param  int  studentId
	*/
	public function deleteMail($id,$studentId)
	{
		DB::table('notifications')->where('id', $id)->update(['is_deleted' => 1]);
		\Session::flash('success_notif_delete', 'Notifications is deleted successfully');
		
		return redirect(config('laraadmin.adminRoute') . '/nurses/'.$studentId.'#tab-notifications');
	}
	
	public function isUserExist($table,$user_id)
	{
		$user = DB::table($table)
				->where('user_id', [$user_id])
				->get();
				
		return count($user);
	}
	
	public function assignConsultant(Request $request)
	{
		if(count($request->all()) > 0) { 
			$postData = $request->all(); //print_r($postData); exit;
			DB::table('users')->where('id', $postData['assign_user_id'])->update([ 'assigned_to' => $postData['assigned_to'] ]);
			\Session::flash('status_success', 'Nurse has been assigned to a consultant');

			$consultant_email = DB::table('users')->select('email')->addselect('name')->where('id', $postData['assigned_to'])->get();
			$request['staff_email'] = $consultant_email[0]->email; 
			$staffName = $consultant_email[0]->name;

			$staffs = DB::table('users')->select('context_id')->where('id', $postData['assigned_to'])->get();
			if($staffs && $staffs[0]->context_id != '') {
				$staffId = $staffs[0]->context_id;
			} else {
				$staffId = '';
			}

			// send mail
			if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
				//	Send mail of assignees to nurse
				// Mail::send('emails.assigne_nurse', ['user' => $request->all()['nurse_name'],'staff' => $request->all()['staff_name']], function ($m) use ($request) {
				// 		$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
				// 		$m->to($request->all()['assign_email'])->subject('Zodocs - Nurse Assigned');
				// });

				//	Send mail to assignees
				// Mail::send('emails.assign_staff_nurse', ['user' => $request->all()['nurse_name'],'staff' => $request->all()['staff_name']], function ($m) use ($request) {
				// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
				// 	$m->to($request['staff_email'])->subject('Zodocs - Nurse Assigned');
				// });

				$manager_email = DB::table('users')->select('email')->addselect('name')->where('type', 'MANAGER')->get();
				foreach($manager_email as $key => $result) {
					$request['admin_name'] = $result->name;
					$request['admin_email'] = $result->email;
					if($request['admin_email'] != '') {
					//	Send mail to manager
					// Mail::send('emails.assign_admin_nurse', ['admin' => $request['admin_name'],'user' => $request->all()['nurse_name'],'staff' => $request->all()['staff_name']], function ($m) use ($request) {
					// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
					// 	$m->to($request['admin_email'])->subject('Zodocs - Nurse Assigned');
					// });
					}
				}

			}	

			// Send notification when assigning staff to nurse
			$this->sendAssignNotificationNurse($postData['assign_user_id'],$request->all()['nurse_name'],Auth::user()->id,Auth::user()->type,Auth::user()->name,$staffId,$staffName);
			// End send notification when assigning staff to nurse

		} 
		
		return redirect(config('laraadmin.adminRoute') . '/nurses');
	}

	public function sendAssignNotificationNurse($stdId,$stdName,$fromId,$userType,$userName,$staffId,$staffName) 
	{	
		// Admin assign nurse to staff
			// Send notification to staff
			if($staffId != '') {
				$content = 'Nurse - ' .$stdName.' has been assigned to you by '.$userName. '(Admin)';
				DB::table('notifications')->insert([
							'user_id' => $staffId,
							'comments' => $content,
							'from_user_id' => $fromId,'notif_dat' => date("Y-m-d H:i:s")
						]);
			}
			
			// Send notification to nurse
			if($stdId != '') {
				$content = 'You are assigned to a staff - ' .$staffName.' by '.$userName. '(Admin)';
				DB::table('notifications')->insert([
							'user_id' => $stdId,
							'comments' => $content,
							'from_user_id' => $fromId,'notif_dat' => date("Y-m-d H:i:s")
						]);
			}
			// Send notification to managers
			$managers = DB::table('users')->select('context_id')->addselect('name')->where('type', 'MANAGER')->get();
			$content = 'Nurse - ' .$stdName.' has been assigned to staff - '.$staffName.' by '.$userName. '(Admin)';
			foreach($managers as $key => $result) {
				$admin_name = $result->name;
				$admin_id = $result->context_id;
				if($admin_id != '') {
					//	Send notification to manager
					DB::table('notifications')->insert([
						'user_id' => $admin_id,
						'comments' => $content,
						'from_user_id' => $fromId,'notif_dat' => date("Y-m-d H:i:s")
					]);			
				}
			}
	}

	// assign to employer
	public function assignUniversityUser(Request $request)
	{
		if(count($request->all()) > 0) {
			$postData = $request->all();
			DB::table('users')->where('id', $postData['assign_user_id'])->update([ 'assigned_to_agents' => $postData['assigned_to'] ]);
		} 
		
		return redirect(config('laraadmin.adminRoute') . '/nurses');
	}	
	
	/**
     * Change Password
     *
     * @return
     */
	public function change_password($id, Request $request) {
		
		$validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
			'password_confirmation' => 'required|min:6|same:password'
        ]);
		
		if ($validator->fails()) {
			return \Redirect::to(config('laraadmin.adminRoute') . '/nurses/'.$id.'#tab-account-settings')->withErrors($validator);
		}
		$user = User::where("id", $id)->first();
		$user->password = bcrypt($request->password);
		$user->save();
		
		\Session::flash('success_message', 'Password is successfully changed');
		
		// Send mail to User his new Password
		if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
			// Send mail to User his new Password
			// Mail::send('emails.send_login_cred_change', ['user' => $user, 'password' => $request->password], function ($m) use ($user) {
			// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
			// 	$m->to($user->email, $user->name)->subject(LAConfigs::getByKey('sitename').' - Login Credentials changed');
			// });
		} else {
			Log::info("User change_password: username: ".$user->email." Password: ".$request->password);
		}
		
		return redirect(config('laraadmin.adminRoute') . '/nurses/'.$id.'#tab-account-settings');
	}
}
