@extends("la.layouts.app")

@section("contentheader_title", "Uploads")
@section("contentheader_description", "Uploaded images & files")
@section("section", "Uploads")
@section("sub_section", "Listing")
@section("htmlheader_title", "Uploaded images & files")

@section("headerElems")
@la_access("Uploads", "create")
    @if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER"]))
	<button id="AddNewUploads" class="btn btn-success btn-sm pull-right">Add New</button>
    @endif
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="file_error_text"></div>
<div class="image-upload-wrap">
<form action="{{ url(config('laraadmin.adminRoute') . '/upload_files_media') }}" id="fm_dropzone_main" enctype="multipart/form-data" method="POST">
    {{ csrf_field() }}
    <a id="closeDZ1"><i class="fa fa-times"></i></a>
    <div class="dz-message drag-text"><i class="fa fa-cloud-upload"></i><br>Drop files here to upload</div>
</form>
</div>
<br>

<!-- Media Files -->
<div class="box box-success">
<div class="box-body">
<table id="media-lists" class="table table-hover mg-table mgt3 display nowrap">
		<thead>
			<tr class="success">
				<th>No:</th>
                <th>Image</th>
				<th>Name</th>
                @if($show_actions)
					<th>Actions</th>
				@endif
                </tr>
		</thead>
		<tbody></tbody>
</table>
</div>
</div>
<!-- End Media Files -->
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>

<script src="{{ asset('la-assets/plugins/lightbox2/dist/js/lightbox-plus-jquery.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('la-assets/plugins/lightbox2/dist/css/lightbox.min.css') }}">
@endpush

@push('scripts')

<!-- <script src="{{ asset('la-assets/plugins/lightbox2/dist/js/lightbox-plus-jquery.min.js') }}"></script> -->
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script> 



<script>
var bsurl = $('body').attr("bsurl");
var fm_dropzone_main = null;
var cntFiles = null;
$(function () {   $(".image-upload-wrap").hide();

	@la_access("Uploads", "create")
	fm_dropzone_main = new Dropzone("#fm_dropzone_main", {
        maxFilesize: 2,
        //acceptedFiles: "image/*,application/pdf",

        accept: function(file, done) {
            console.log(file);
            if (file.type != "image/jpeg" && file.type != "image/jpg" && file.type != "image/png" && file.type != "image/gif" && file.type != "image/bmp" && file.type != "application/pdf") {
                $(".file_error_text").html("<p class='file_err'>Error! Files of this type are not accepted</p>");
                this.removeFile(file);
            }
            else {
                done();
            }
        },

        init: function() {
            this.on("complete", function(file) {
                this.removeFile(file);
                $(".file_error_text").html("");
            });
            this.on("success", function(file) {
                $('#media-lists').DataTable().ajax.reload();
                $(".file_error_text").html("");
            });
        }
    });
    $("#fm_dropzone_main").slideUp();
    $("#AddNewUploads").on("click", function() { $(".image-upload-wrap").show();
        //$("#image-upload-wrap").removeAttr("style");
        $("#image-upload-wrap").attr("style", "display:show");
        $("#fm_dropzone_main").slideDown();
    });
    $("#closeDZ1").on("click", function() {  $(".image-upload-wrap").hide();
        $("#fm_dropzone_main").slideUp();
    });
	@endla_access

    // Media Files
    $("#media-lists").DataTable({
		processing: true,
        serverSide: false,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/media_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
    // End Media Files 
});

function formatFile(upload) {
    var image = '';
    if($.inArray(upload.extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
        image = '<img src="'+bsurl+'/files/'+upload.hash+'/'+upload.name+'?s=130">';
    } else {
        switch (upload.extension) {
            case "pdf":
                image = '<i class="fa fa-file-pdf-o"></i>';
                break;
            default:
                image = '<i class="fa fa-file-text-o"></i>';
                break;
        }
    }
    return '<li><a class="fm_file_sel" data-toggle="tooltip" data-placement="top" title="'+upload.name+'" upload=\''+JSON.stringify(upload)+'\'>'+image+'</a></li>';
}
</script>
@endpush
