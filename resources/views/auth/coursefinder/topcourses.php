<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Google fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Outlined" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />

  <!-- Stylesheets -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/vendors.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/main.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/custom.css">

  <title>Top Courses</title>
  <link rel="icon" type="image/x-icon" href="la-assets/coursefinder/img/coursesCards/icons/zodocs_fav.png">
</head>

<body class="preloader-visible" data-barba="wrapper">

  <!-- preloader start -->
  <div class="preloader js-preloader">
    <div class="preloader__bg"></div>
  </div>
  <!-- preloader end -->

  <!-- barba container start -->
  <div class="barba-container" data-barba="container">


    <main class="main-content  ">

      <header data-anim="fade" data-add-bg="bg-dark-1" class="header -type-1 js-header">


        <div class="header__container">
          <div class="row justify-between items-center">

            <div class="col-auto">
              <div class="header-left">

                <div class="header__logo ">
                  <a data-barba href="<?php echo url('/coursefinder'); ?>">
                    <img src="la-assets/coursefinder/img/general/logo.png" alt="logo">
                  </a>
                </div>

              </div>
            </div>


            <div class="header-menu js-mobile-menu-toggle ">
              <div class="header-menu__content">
                <div class="mobile-bg js-mobile-bg"></div>

                <div class="d-none xl:d-flex items-center px-20 py-20 border-bottom-light">
                  
                  <?php 
                  if(isset(Auth::user()->type)) { ?>
                    <a href="<?php echo url('/logout'); ?>" class="text-dark-1">Log out</a>
                    <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="text-dark-1 ml-30">My Account</a>
                  <?php } else { ?>
                    <a href="<?php echo url('/userlogin'); ?>" class="text-dark-1">Log in</a>
                    <a href="<?php echo url('/signup'); ?>" class="text-dark-1 ml-30">Sign up</a>
                  <?php } ?>

                </div>

                <div class="menu js-navList">
                  <ul class="menu__nav text-white -is-active">

                    <li class="menu-item-has-children">
                      <a data-barba href="#">Home</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Home</a>
                        </li>
                        <li><a href="<?php echo url('/coursefinder'); ?>">Home 1</a></li>
                        <li><a href="<?php echo url('/coursefinder2'); ?>">Home 2</a></li>
                        <li><a href="<?php echo url('/coursefinder3'); ?>">Home 3</a></li>
                        <li><a href="<?php echo url('/coursefinder4'); ?>">Home 4</a></li>
                        <li><a href="<?php echo url('/coursefinder5'); ?>">Home 5</a></li>
                        <li><a href="<?php echo url('/coursefinder6'); ?>">Home 6</a></li>
                        <li><a href="<?php echo url('/coursefinder7'); ?>">Home 7</a></li>
                        <li><a href="<?php echo url('/coursefinder8'); ?>">Home 8</a></li>
                        <li><a href="<?php echo url('/coursefinder9'); ?>">Home 9</a></li>
                        <li><a href="<?php echo url('/coursefinder10'); ?>">Home 10</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/aboutus'); ?>">
                        About Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li class="menu-item-has-children">
                      <a data-barba href="#">Courses</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Courses</a>
                        </li>
                        <li><a href="<?php echo url('/coursesearch'); ?>">Course 1</a></li>
                        <li><a href="<?php echo url('/coursesearch2'); ?>">Course 2</a></li>
                        <li><a href="<?php echo url('/coursesearch3'); ?>">Course 3</a></li>
                        <li><a href="<?php echo url('/coursesearch4'); ?>">Course 4</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/blog'); ?>">
                        Blog <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/topcourses'); ?>">
                        Top Courses <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/contactus'); ?>">
                        Contact Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>


                  </ul>
                </div>

                
              </div>

            </div>


            <div class="col-auto">
              <div class="header-right d-flex items-center">

                <div class="header-right__icons text-white d-flex items-center">

                  <div class="d-none xl:d-block ml-20">
                    <button class="text-white items-center" data-el-toggle=".js-mobile-menu-toggle">
                      <i class="fa fa-bars"></i>
                    </button>
                  </div>

                </div>
                
                <div class="header-right__buttons d-flex items-center ml-30 md:d-none">
                  
                  <?php 
                  if(isset(Auth::user()->type)) { ?>
                    <a href="<?php echo url('/logout'); ?>" class="button -underline text-white">Log out</a>
                    <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="button -sm -white text-dark-1 ml-30">My Account</a>
                  <?php } else { ?>
                    <a href="<?php echo url('/userlogin'); ?>" class="button -underline text-white">Log in</a>
                    <a href="<?php echo url('/signup'); ?>" class="button -sm -white text-dark-1 ml-30">Sign up</a>
                  <?php } ?>
                  
                </div>
              </div>
            </div>

          </div>
        </div>
      </header>


      <div class="content-wrapper  js-content-wrapper">


        <section data-anim="fade" class="breadcrumbs ">
          <div class="container">
            <div class="row">
              <div class="col-auto">
                <div class="breadcrumbs__content">

                  <div class="breadcrumbs__item ">
                    <a href="<?php echo url('/coursefinder'); ?>">Home</a>
                  </div>

                  <div class="breadcrumbs__item ">
                    Top Courses
                  </div>

                </div>
              </div>
            </div>
          </div>
        </section>


        <section class="page-header -type-1">
          <div class="container">
            <div class="page-header__content">
              <div class="row justify-center text-center">
                <div class="col-auto">
                  <div data-anim="slide-up delay-1">

                    <h1 class="page-header__title">Top Courses</h1>

                  </div>

                  <div data-anim="slide-up delay-2">

                    <p class="page-header__text">We’re on a mission to deliver engaging, curated courses at a reasonable price.</p>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <!-- Top Courses -->
        <section class="layout-pt-md layout-pb-lg">
          <div data-anim-wrap class="container">
            <div class="tabs -pills js-tabs">
              <div class="tabs__content pt-40 js-tabs-content">

                <div class="tabs__pane -tab-item-1 is-active">
                  <div class="row y-gap-30 pt-30">

                    <?php 
                      if(count($courses) > 0) {
                        foreach($courses as $result) {
                    ?>
                    <div class="col-lg-4 col-md-6">
                      <div class="eventCard -type-1">
                        <div class="eventCard__img">
                          <!-- <img src="la-assets/coursefinder/img/courses-list/1.png" alt="image"> -->
                          <?php if($result->university_logo!='') { ?>
                            <img src="<?php echo $result->university_logo; ?>" alt="image" style="width:410px; height: 287px;">
                          <?php } else { ?>
                            <img src="la-assets/coursefinder/img/coursesCards/no-image.png" alt="image" style="width:410px; height: 287px;">
                          <?php } ?>
                        </div>

                        <div class="eventCard__bg bg-white">
                          <div class="eventCard__content y-gap-10">
                            <div class="eventCard__inner">
                              <h4 class="eventCard__title text-17 fw-500">
                                <?php echo $result->course_name; ?>
                              </h4>
                              <div class="d-flex x-gap-15 pt-10">
                                <div class="d-flex items-center">
                                  <img src="la-assets/coursefinder/img/coursesCards/icons/2.svg" alt="icon">
                                  <div class="text-14">&nbsp;<?php echo $result->intake; ?></div>
                                </div>
                                <div class="d-flex items-center">
                                  <img src="la-assets/coursefinder/img/coursesCards/icons/location-icon.png" alt="icon">
                                  <div class="text-14">&nbsp;<?php echo $result->country_name; ?></div>
                                </div>
                              </div>
                            </div>

                            <div class="eventCard__button">
                              <!-- <a href="#" class="button -sm -rounded -outline-purple-1 text-purple-1 px-25">Apply Now</a> -->
                              <a href="<?php echo url('/applynow').'/'.Crypt::encrypt($result->id); ?>" class="button -sm -rounded -outline-purple-1 text-purple-1 px-25">Apply Now</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php }} else { echo "No top courses to display"; } ?>

                    </div>

                </div>


              </div>
            </div>
          </div>
        </section>
        <!-- End Top Courses -->

        <footer class="footer -type-1 bg-dark-1 -green-links">
          <div class="container">
            
            <div class="py-30 border-top-light-15">
              <div class="row justify-between items-center y-gap-20">
                <div class="col-auto">
                  <div class="d-flex items-center h-100 text-white">
                    © 2022 ZODOCS. All Right Reserved.
                  </div>
                </div>

                <div class="col-auto">
                  <div class="d-flex x-gap-20 y-gap-20 items-center flex-wrap">
                    <div>Powered By
                      <a href="http://demo.zodocs.com/" class="text-white" target="_blank">
                        <span class="text-15">ZODOCS</span>
                      </a>. Solution By
                      <a href="https://www.labgex.com/" class="text-white" target="_blank">
                        <span class="text-15">LabGex Innovations (P) Ltd</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>

      </div>
    </main>
  </div>
  <!-- barba container end -->

  <!-- JavaScript -->
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
  <script src="la-assets/coursefinder/js/vendors.js"></script>
  <script src="la-assets/coursefinder/js/main.js"></script>

</body>

</html>