<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use Dwij\Laraadmin\Helpers\LAHelper;
use Dwij\Laraadmin\Models\LAConfigs;

use App\User;
use Mail;

class NursefilesController extends Controller
{
	public function __construct() {
		
		if(Auth::user()->type != 'SUPER_ADMIN' && Auth::user()->type != 'SUBCONTRACTORS')
			return abort(403);
	}
	
	/**
	 * Display a listing of the Employees.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		if(!isset($_GET['id']) || str_replace(' ','',$_GET['id']) == '' || str_replace(' ','',$_GET['id']) == 0) {
			return redirect(config('laraadmin.adminRoute') . '/nurses');
		} else {
			$nurse_id = str_replace(' ','',$_GET['id']);
			$results1 = DB::select(DB::raw("SELECT np.skype_picture,np.passport_upload,us.id,us.name FROM nurse_personal_information np 
								INNER JOIN users us ON us.id=np.user_id AND us.deleted_at IS NULL WHERE us.id=$nurse_id"));
			$results2 = DB::select(DB::raw("SELECT nq.certificate_upload,nq.evidence_upload,us.id,us.name FROM nurse_qualifications nq 
								INNER JOIN users us ON us.id=nq.user_id AND us.deleted_at IS NULL WHERE us.id=$nurse_id"));
			$results3 = DB::select(DB::raw("SELECT nq.cv_file_upload,us.id,us.name FROM nurse_employement_history nq 
								INNER JOIN users us ON us.id=nq.user_id AND us.deleted_at IS NULL WHERE us.id=$nurse_id"));
			$results4 = DB::select(DB::raw("SELECT nq.ielts_file_upload,us.id,us.name FROM nurse_english_language nq 
								INNER JOIN users us ON us.id=nq.user_id AND us.deleted_at IS NULL WHERE us.id=$nurse_id"));
			$results5 = DB::select(DB::raw("SELECT nq.offer_letter_file,us.id,us.name FROM nurse_employer_details nq 
								INNER JOIN users us ON us.id=nq.user_id AND us.deleted_at IS NULL WHERE us.id=$nurse_id"));
			$results6 = DB::select(DB::raw("SELECT nq.scope_file_upload,nq.cbt_upload,nq.nmc_file_upload,nq.birth_cert_upload,us.id,us.name FROM nurse_nmc nq 
								INNER JOIN users us ON us.id=nq.user_id AND us.deleted_at IS NULL WHERE us.id=$nurse_id"));
			$results7 = DB::select(DB::raw("SELECT nq.spouse_passport,us.id,us.name FROM nurse_dependant_det nq 
								INNER JOIN users us ON us.id=nq.user_id AND us.deleted_at IS NULL WHERE us.id=$nurse_id"));
			$results8 = DB::select(DB::raw("SELECT nq.tb_file_upload,nq.spons_file_upload,us.id,us.name FROM nurse_visa_imigration nq 
								INNER JOIN users us ON us.id=nq.user_id AND us.deleted_at IS NULL WHERE us.id=$nurse_id"));
			$results9 = DB::select(DB::raw("SELECT nq.addl_docs,us.id,us.name FROM nurse_payments nq 
								INNER JOIN users us ON us.id=nq.user_id AND us.deleted_at IS NULL WHERE us.id=$nurse_id"));
			
			return View('la.nursefiles.index', [
				'personal_information' => $results1,
				'qualifications' => $results2,
				'employement_history' => $results3,
				'english_language' => $results4,
				'employer_details' => $results5,
				'nurse_nmc' => $results6,
				'dependant_det' => $results7,
				'visa_imigration' => $results8,
				'nurse_payments' => $results9,
				'nurseId' => $_GET['id']
			]);
		}
	}
	
	public function update(Request $request)
	{
		if(count($request->all()) > 0) {
			
			$data = json_decode($request->all()['jsonData'],true);
			if(count($data) > 0) {
				
				$nurse_id = $request->all()['user_id'];
				$sendMail = $request->all()['send_mail'];
				$contentMsg = $request->all()['content'];
				$content = '<a href="'.$data[0]['link'].'" target="_blank">'.$data[0]['name'].'</a> uploaded files<br><br>';
				$content .= $contentMsg.'<br><br>';
				foreach($data as $values) {
					$content .= $values['type'].' = <a target="_blank" href="'.$values['file'].'">'.$values['file'].'</a><br>';
				}
				//send mail
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers .= 'From: Maxglobal' . "\r\n";
				$to = $sendMail;
				$subject = 'Maxglobal nurse files';
				$message = $content;
				$sendMail = mail($to,$subject,$message,$headers);	
				
				if($sendMail) {
					\Session::flash('success_message', 'Mail successfully sent');
				} else {
					\Session::flash('success_message', 'Mail sending failed');
				}
			}
		}
		
		return redirect(config('laraadmin.adminRoute') . '/nursefiles?id='.$nurse_id);
	}

}
