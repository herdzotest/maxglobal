<div id="tab_4" class="tab-pane" >
                                        <div class="content-head">
                                        
                                                        <h4>English Language Proficiency</h4>
                                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                                                </div>
                                <div class="pane">

                                <div class="academic">
                                <div class="mgv-field">
                                        <div class="name">
                                        Was English the language of instruction in your previous institution  
                                        </div>
                                        <div class="value" id="eng_lang_check" data-type="radio"></div>
                                </div>
                                        <div class="mgv-field">
                                                <div class="name">
                                                Number of years you have studied in English                                                 </div>
                                                <div class="value" id="yrs_english"></div>
                                        </div>
                                        <div class="mgv-field">
                                                <div class="name">
                                                I hold the following qualifications                                                 </div>
                                                <div class="value" id="qualifications"></div>
                                        </div>
										
                                        <div class="mgv-field hide" id="other-qualifications-wrapper">
                                                <div class="name">
                                                        Other qualifications
                                                </div>
                                                <div class="value" id="other_qualifications"></div>
                                        </div>
										
										<h3 style="margin-top: 30px;margin-bottom: 30px;">Score (if known)</h3>
                                        
										<div class="mgv-field">
                                                <div class="name">
                                                        Reading Score
                                                </div>
                                                <div class="value" id="reading_score"></div>
                                        </div>
                                        <div class="mgv-field">
                                                <div class="name">
                                                        Writing Score
                                                </div>
                                                <div class="value" id="writing_score"></div>
                                        </div>
                                        <div class="mgv-field">
                                                <div class="name">
                                                       Speaking Score
                                                </div>
                                                <div class="value" id="speaking_score"></div>
                                        </div>											
                                        <div class="mgv-field">
                                                <div class="name">
                                                        Listening Score
                                                </div>
                                                <div class="value" id="listening_score"></div>
                                        </div>
                                        <div class="mgv-field">
                                                <div class="name">
                                                        Over all Score
                                                </div>
                                                <div class="value" id="over_all_score"></div>
                                        </div>																			
                                        <div class="mgv-field">
                                                <div class="name">
                                                       Date of test
                                                </div>
                                                <div class="value" id="test_date"></div>
                                        </div>
                                        <div class="mgv-field">
                                                <div class="name">
                                                    If test is yet to be completed, please state date and time to be taken
                                                </div>
                                                <div class="value">
													<span id="test_date_time"></span>&nbsp;&nbsp;
													<span id="test_time"></span>
												</div>
                                        </div>											
                                </div>
								</div>
                        </div>