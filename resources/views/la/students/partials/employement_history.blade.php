<div id="tab_5" class="tab-pane" >
        
                   
<div class="content-head">
                                        
                                        <h4>Employment History</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                        </div>
                        {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'personal-add-form']) !!}

                <div class="pane">
					<div id="employement-wrapper">
					<div class="stdhst">
							<div class="index">1</div>
							<div class="col-md-6">
								<div class="mg-input-field ">
									<label for="employer_name">Name of employer/organisation</label>
									<input class="form-control" id="employer_name" name="employer_name" type="text">										
								</div>
							</div>

							<div class="col-md-6">
								<div class="mg-input-field ">
									<label for="job_position">Job Position</label>
									<input class="form-control" id="job_position" name="job_position" type="text">
								</div>
							</div> 
							
							<!-- <div class="col-md-4">
								<div class="mg-input-field ">
									<label for="work_nature">Nature of work</label>
									<input class="form-control" id="work_nature" name="work_nature" type="text">								
								</div>
							</div> -->
							<!-- <div class="col-md-4">
									<div class="mg-input-field ">
											<label for="from_date">From</label>
											<input class="form-control datepicker" id="from_date" name="from_date" type="text">
									</div>
							</div>
							<div class="col-md-4">
									<div class="mg-input-field ">
											<label for="to_date">To</label>
											<input class="form-control datepicker" id="to_date" name="to_date" type="text">
									</div>
							</div> -->

							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="from_date">From</label>
											<?php 
											$curr_yr = date('Y'); 
											$curr_mon = date('m');
    										$months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
											?>
											<select class="searchSelect" name="from_date" id="from_date">
												<option value="">Select</option>
												<?php
													for($i=1990; $i<=$curr_yr;$i++) {
														foreach ($months as $key => $value) { if($i==$curr_yr && $key>$curr_mon) { } else { ?>
															<option value="<?php echo $value.' '.$i; ?>"> <?php echo $value.' '.$i; ?></option>
													<?php	} }
													}
												?>
											</select>
									</div>
							</div>

							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="to_date">To</label>
											<select class="searchSelect" name="to_date" id="to_date">
												<option value="">Select</option>
												<?php
													for($i=1990; $i<=$curr_yr;$i++) {
														foreach ($months as $key => $value) { if($i==$curr_yr && $key>$curr_mon) { } else { ?>
															<option value="<?php echo $value.' '.$i; ?>"> <?php echo $value.' '.$i; ?></option>
													<?php	} }
													}
												?>
											</select>
									</div>
							</div>

							<div class="col-md-6">
								<div class="mg-input-field ">
									<label for="experience">Year of Experience</label>
									<input class="form-control" id="experience" name="experience" type="number" step=".01" min=1>								
								</div>
							</div>

							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="work_mode">Mode of work</label>
											<select class="formSelect" id="work_mode" name="work_mode">
												  <option value="">Select</option>
												  <option value="Full-time"> Full time </option>
												  <option value="Part-time"> Part-time </option>											
											</select>
									</div>
							</div>	
							
							<div class="col-md-6">
											<label>Experience letter</label>
											<div class="file-field input-field">
															<div class="btn">
																	<span>Upload</span>
																	<input type="file" multiple > <i class="fa fa-cloud-upload"></i>
															</div>
															<div class="file-path-wrapper">
																	<input class="file-path validate" name="experience_letter" type="text" placeholder = "Upload file" >
															</div>
											</div>											
											<div class="uploaded-file" id="experience_letter" data-type="file"></div>
							</div>	
							
							
							<div class="clearfix"></div>
					</div>
					</div>
					<input type="hidden" value="employement" name="formtype" class="formtype">
					<div class="btn addshbtn"> <span class="glyphicon glyphicon-plus"></span></div>						
                </div>
				<!-- <input type="hidden" value="1" class="wrapper-count"> -->
				<input type="hidden" value="<?php echo $wrp_count_emp_history; ?>" class="wrapper-count">				
                <div class="controls">
                                <a class="mgbtn2 back_button">Back</a>
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="employement_history">
                                 <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                              
                                {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                        </div>
                        {{ Form::close() }}
</div>


