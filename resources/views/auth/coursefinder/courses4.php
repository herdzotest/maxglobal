<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Google fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Outlined" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />

  <!-- Search Select -->
  <link rel="stylesheet" href="la-assets/coursefinder/searchselect/library/bootstrap-5/bootstrap.min.css">
  <script src="la-assets/coursefinder/searchselect/library/bootstrap-5/bootstrap.bundle.min.js"></script>
  <script src="la-assets/coursefinder/searchselect/library/dselect.js"></script>
  <!-- End Search Select-->

  <!-- Stylesheets -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/vendors.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/main.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/custom.css">

  <title>Courses</title>
  <link rel="icon" type="image/x-icon" href="la-assets/coursefinder/img/coursesCards/icons/zodocs_fav.png">
</head>

<body class="preloader-visible" data-barba="wrapper">

  <!-- preloader start -->
  <div class="preloader js-preloader">
    <div class="preloader__bg"></div>
  </div>
  <!-- preloader end -->

  <!-- barba container start -->
  <div class="barba-container" data-barba="container">


    <main class="main-content  ">

      <header data-anim="fade" data-add-bg="bg-dark-1" class="header -type-1 js-header">


        <div class="header__container">
          <div class="row justify-between items-center">

            <div class="col-auto">
              <div class="header-left">

                <div class="header__logo ">
                  <a data-barba href="<?php echo url('/coursefinder'); ?>">
                    <img src="la-assets/coursefinder/img/general/logo.png" alt="logo">
                  </a>
                </div>
              </div>
            </div>


            <div class="header-menu js-mobile-menu-toggle ">
              <div class="header-menu__content">
                <div class="mobile-bg js-mobile-bg"></div>

                <div class="d-none xl:d-flex items-center px-20 py-20 border-bottom-light">
                  
                  <?php 
                  if(isset(Auth::user()->type)) { ?>
                    <a href="<?php echo url('/logout'); ?>" class="text-dark-1">Log out</a>
                    <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="text-dark-1 ml-30">My Account</a>
                  <?php } else { ?>
                    <a href="<?php echo url('/userlogin'); ?>" class="text-dark-1">Log in</a>
                    <a href="<?php echo url('/signup'); ?>" class="text-dark-1 ml-30">Sign up</a>
                  <?php } ?>

                </div>

                <div class="menu js-navList">
                  <ul class="menu__nav text-white -is-active">
                    <li class="menu-item-has-children">
                      <a data-barba href="#">Home</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Home</a>
                        </li>
                        <li><a href="<?php echo url('/coursefinder'); ?>">Home 1</a></li>
                        <li><a href="<?php echo url('/coursefinder2'); ?>">Home 2</a></li>
                        <li><a href="<?php echo url('/coursefinder3'); ?>">Home 3</a></li>
                        <li><a href="<?php echo url('/coursefinder4'); ?>">Home 4</a></li>
                        <li><a href="<?php echo url('/coursefinder5'); ?>">Home 5</a></li>
                        <li><a href="<?php echo url('/coursefinder6'); ?>">Home 6</a></li>
                        <li><a href="<?php echo url('/coursefinder7'); ?>">Home 7</a></li>
                        <li><a href="<?php echo url('/coursefinder8'); ?>">Home 8</a></li>
                        <li><a href="<?php echo url('/coursefinder9'); ?>">Home 9</a></li>
                        <li><a href="<?php echo url('/coursefinder10'); ?>">Home 10</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/aboutus'); ?>">
                        About Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li class="menu-item-has-children">
                      <a data-barba href="#">Courses</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Courses</a>
                        </li>
                        <li><a href="<?php echo url('/coursesearch'); ?>">Course 1</a></li>
                        <li><a href="<?php echo url('/coursesearch2'); ?>">Course 2</a></li>
                        <li><a href="<?php echo url('/coursesearch3'); ?>">Course 3</a></li>
                        <li><a href="<?php echo url('/coursesearch4'); ?>">Course 4</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/blog'); ?>">
                        Blog <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/topcourses'); ?>">
                        Top Courses <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/contactus'); ?>">
                        Contact Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>
                    
                  </ul>
                </div>
              </div>

              <div class="header-menu-close" data-el-toggle=".js-mobile-menu-toggle">
                <div class="size-40 d-flex items-center justify-center rounded-full bg-white">
                  <i class="fa fa-close"></i>
                </div>
              </div>

              <div class="header-menu-bg"></div>
            </div>


            <div class="col-auto">
              <div class="header-right d-flex items-center">
                <div class="header-right__icons text-white d-flex items-center">
                  <div class="d-none xl:d-block ml-20">
                    <button class="text-white items-center" data-el-toggle=".js-mobile-menu-toggle">
                      <i class="fa fa-bars"></i>
                    </button>
                  </div>
                </div>
                <div class="header-right__buttons d-flex items-center ml-30 md:d-none">
                  
                  <?php 
                  if(isset(Auth::user()->type)) { ?>
                    <a href="<?php echo url('/logout'); ?>" class="button -underline text-white">Log out</a>
                    <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="button -sm -white text-dark-1 ml-30">My Account</a>
                  <?php } else { ?>
                    <a href="<?php echo url('/userlogin'); ?>" class="button -underline text-white">Log in</a>
                    <a href="<?php echo url('/signup'); ?>" class="button -sm -white text-dark-1 ml-30">Sign up</a>
                  <?php } ?>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <div class="content-wrapper  js-content-wrapper">
        <section data-anim="fade" class="breadcrumbs ">
          <div class="container">
            <div class="row">
              <div class="col-auto">
                <div class="breadcrumbs__content">

                  <div class="breadcrumbs__item ">
                    <a href="<?php echo url('/coursefinder'); ?>">Home</a>
                  </div>

                  <div class="breadcrumbs__item ">
                    All courses
                  </div>

                </div>
              </div>
            </div>
          </div>
        </section>


        <section class="page-header -type-1">
          <div class="container">
            <div class="page-header__content">
              <div class="row">
                <div class="col-auto">
                  <div data-anim="slide-up delay-1">
                    <h1 class="page-header__title page_title">Our Courses</h1>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>


        <section class="layout-pt-md1 layout-pb-lg">
          <div data-anim-wrap class="container">
            

            <!-- <div data-anim="slide-up delay-1" class="accordion js-accordion">
              <div class="accordion__item">
                <div class="row y-gap-20 items-center justify-between pb-30">
                

                  <div class="col-auto">
                    <div class="row x-gap-20 y-gap-20">
                      <div class="col-auto">
                        <div class="accordion__button w-unset">
                          <button class="button h-50 px-30 -light-7 text-purple-1">
                            <i class="icon-filter mr-10"></i>
                            Filter
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="accordion__content">
                  <div class="sidebar -courses px-30 py-30 rounded-8 bg-light-3 mb-50" style="height:500px;">
                    <div class="row x-gap-60 y-gap-40">

                      <div class="col-xl-3 col-lg-4 col-sm-6">
                        <div class="sidebar__item">
                          <h5 class="sidebar__title">Country</h5>
                          <div class="sidebar-checkbox">
                            <div class="">
                               <select name="course_country" class="form-select" id="course_country">
                                  <?php
                                          if($course_country != '') {
                                            echo '<option value="'.$course_country.'">'.$course_country.'</option>';
                                          } ?>
                                          <option value="">Select Country</option>
                                          <?php 
                                            foreach($countries as $countriesData) {
                                              echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
                                            }
                                    ?>  
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-xl-3 col-lg-4 col-sm-6">
                        <div class="sidebar__item">
                          <h5 class="sidebar__title">University</h5>
                          <div class="sidebar-checkbox">

                            <div class="">
                               <select name="course_university" class="form-select" id="course_university">
                                    <?php
                                      if($course_university != '') {
                                        echo '<option value="'.$course_university.'">'.$course_university.'</option>';
                                      } ?>
                                        <option value="">Select University</option>
                                      <?php 
                                        foreach($university as $universityData) {
                                          echo '<option value="'.$universityData->university.'">'.$universityData->university.'</option>';
                                      }
                                   ?>
                                </select>
                            </div>
                          </div>
                        </div>
                      </div>


                      <div class="col-xl-3 col-lg-4 col-sm-6">
                        <div class="sidebar__item">
                          <h5 class="sidebar__title">Price (in Rs)</h5>
                          <div class="sidebar-checkbox">

                            <div class="">
                               <select name="course_price" class="form-select" id="course_price">
                                    <?php
                                    if($course_price != '') {
                                      echo '<option value="'.$course_price.'">'.$course_price.'</option>';
                                    } ?>
                                    <option value="">Select Price</option>
                                    <option value="10,000 - 30,000">10,000 - 30,000</option>
                                    <option value="31,000 - 60,000">31,000 - 60,000</option>
                                    <option value="61,000 - 90,000">61,000 - 90,000</option>
                                    <option value="91,000 and Above">91,000 and Above</option>
                                </select>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-xl-3 col-lg-4 col-sm-6">
                        <div class="sidebar__item">
                          <h5 class="sidebar__title">Course</h5>
                          <div class="sidebar-checkbox">
                            <div class="">
                               <select name="course_name" class="form-select" id="course_name">
                                    <?php
                                        if($course_name != '') {
                                          echo '<option value="'.$course_name.'">'.$course_name.'</option>';
                                        } ?>
                                          <option value="">Select Course</option>
                                        <?php 
                                          foreach($coursess as $coursessData) {
                                            echo '<option value="'.$coursessData->course_name.'">'.$coursessData->course_name.'</option>';
                                          }
                                    ?> 
                                </select>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-xl-3 col-lg-4 col-sm-6">
                        <div class="sidebar__item">
                          <h5 class="sidebar__title">Intake</h5>
                          <div class="sidebar-checkbox">
                            <div class="">
                               <select name="course_intake" class="form-select" id="course_intake">
                                    <?php
                                            $curr_yr = date('Y');
                                            $curr_yr2 = date('Y', strtotime('-2 year'));
                                            $nxt_yr = date('Y', strtotime('+1 year'));
                                            $nxt_yr2 = date('Y', strtotime('+2 year'));
                                            $curr_mon = date('m');
                                              $months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
                      
                                            if($course_intake != '') {
                                              echo '<option value="'.$course_intake.'">'.$course_intake.'</option>';
                                            } ?>
                                            <option value="">Select Intake</option>
                                            <?php 
                                                for($i=$curr_yr; $i<=$nxt_yr2;$i++) {
                                                  foreach ($months as $key => $value) { ?>
                                                    <option value="<?php echo $value.' '.$i; ?>"> <?php echo $value.' '.$i; ?></option>
                                                  <?php }
                                                }
                                  ?>
                                </select>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-xl-2 col-lg-4 col-sm-6">
                        <div class="sidebar__item">
                          <h5 class="sidebar__title"></h5>
                          <div class="sidebar-checkbox">
                            <div class="">
                               <div class="form-group">
                                  <button class="button h-50 px-30 -light-7 text-purple-1 searchbtn">Search</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-xl-2 col-lg-4 col-sm-6">
                        <div class="sidebar__item">
                          <h5 class="sidebar__title"></h5>
                          <div class="sidebar-checkbox">
                            <div class="">
                               <div class="form-group">
                                  <button class="button h-50 px-30 -light-7 text-purple-1 clearbtn">Clear</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div> -->



            <!-- Bootstrap search select -->
            <div class="row">
              <div class="col-md-4">
                    <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Country</label>
                    <select name="course_country" class="form-select" id="course_country">
                        <?php
                                if($course_country != '') {
                                  echo '<option value="'.$course_country.'">'.$course_country.'</option>';
                                } ?>
                                <option value="">Select Country</option>
                                <?php 
                                  foreach($countries as $countriesData) {
                                    echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
                                  }
                          ?>  
                    </select>
              </div>

              <div class="col-md-4">
                    <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">University</label>
                    <select name="course_university" class="form-select" id="course_university">
                        <?php
                          if($course_university != '') {
                            echo '<option value="'.$course_university.'">'.$course_university.'</option>';
                          } ?>
                            <option value="">Select University</option>
                          <?php 
                            foreach($university as $universityData) {
                              echo '<option value="'.$universityData->university.'">'.$universityData->university.'</option>';
                          }
                       ?>
                    </select>
              </div>

              <div class="col-md-4">
                    <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Price (in Rs)</label>
                    <select name="course_price" class="form-select" id="course_price">
                        <?php
                        if($course_price != '') {
                          echo '<option value="'.$course_price.'">'.$course_price.'</option>';
                        } ?>
                        <option value="">Select Price</option>
                        <option value="10,000 - 30,000">10,000 - 30,000</option>
                        <option value="31,000 - 60,000">31,000 - 60,000</option>
                        <option value="61,000 - 90,000">61,000 - 90,000</option>
                        <option value="91,000 and Above">91,000 and Above</option>
                    </select>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-4">
                  <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Course</label>
                    <select name="course_name" class="form-select" id="course_name">
                        <?php
                            if($course_name != '') {
                              echo '<option value="'.$course_name.'">'.$course_name.'</option>';
                            } ?>
                              <option value="">Select Course</option>
                            <?php 
                              foreach($coursess as $coursessData) {
                                echo '<option value="'.$coursessData->course_name.'">'.$coursessData->course_name.'</option>';
                              }
                        ?> 
                    </select>
                </div>
                <div class="col-md-4">
                  <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Intake</label>
                    
                    <select name="course_intake" class="form-select" id="course_intake">
                        <?php
                                $curr_yr = date('Y');
                                $curr_yr2 = date('Y', strtotime('-2 year'));
                                $nxt_yr = date('Y', strtotime('+1 year'));
                                $nxt_yr2 = date('Y', strtotime('+2 year'));
                                $curr_mon = date('m');
                                  $months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
          
                                if($course_intake != '') {
                                  echo '<option value="'.$course_intake.'">'.$course_intake.'</option>';
                                } ?>
                                <option value="">Select Intake</option>
                                <?php 
                                    for($i=$curr_yr; $i<=$nxt_yr2;$i++) {
                                      foreach ($months as $key => $value) { ?>
                                        <option value="<?php echo $value.' '.$i; ?>"> <?php echo $value.' '.$i; ?></option>
                                      <?php }
                                    }
                      ?>
                    </select>
                </div>
                <div class="col-md-4"><div class="row">
                  <div class="col-md-4"><br>
                      <div class="form-group">
                          <button class="button h-50 px-30 -light-7 text-purple-1 searchbtn">Search</button>
                      </div>
                  </div>

                  <div class="col-md-4"><br>
                      <div class="form-group">
                          <button class="button h-50 px-30 -light-7 text-purple-1 clearbtn">Clear</button>
                      </div>
                  </div></div>
                </div>

            </div><br><br>
            <!-- End Bootstrap search select -->

            <div class="row y-gap-30">

              <!--Course list -->
              <?php 
                if(count($courses) > 0) {
                  foreach($courses as $result) {
              ?>
              <div data-anim-child="slide-up delay-1" class="col-lg-4 col-md-6">

                <!-- <a href="<?php //echo url('/coursedetails4/'.$result->id); ?>" class="coursesCard -type-1 rounded-8 bg-white shadow-3"> -->
                <a href="<?php echo url('/coursedetails4/'.Crypt::encrypt($result->id)); ?>" class="coursesCard -type-1 rounded-8 bg-white shadow-3">
                  <div class="relative">
                    <div class="coursesCard__image overflow-hidden rounded-top-8">
                      <?php if($result->university_logo!='') { ?>
                        <img class="w-1/1 course_logo1" src="<?php echo $result->university_logo; ?>" alt="image">
                      <?php } else { ?>
                        <img class="w-1/1 course_logo1" src="la-assets/coursefinder/img/coursesCards/no-image.png" alt="image">
                      <?php } ?>
                      <div class="coursesCard__image_overlay rounded-top-8"></div>
                    </div>
                    <div class="d-flex justify-between py-10 px-10 absolute-full-center z-3">

                    </div>
                  </div>

                  <div class="h-100 pt-20 pb-15 px-30">
                    <div class="d-flex items-center">
                      <div class="text-14 lh-1 text-yellow-1 mr-10"><?php echo $result->university_name; ?></div>
                    </div>

                    <div class="text-17 lh-15 fw-500 text-dark-1 mt-10"><?php echo $result->course_name; ?></div>

                    <div class="d-flex x-gap-10 items-center pt-10">

                      <div class="d-flex items-center">
                        <div class="mr-8">
                          <img src="la-assets/coursefinder/img/coursesCards/icons/location-icon.png" alt="icon">
                        </div>
                        <div class="text-14 lh-1"><?php echo $result->country_name; ?></div>
                      </div>

                      <div class="d-flex items-center">
                        <div class="mr-8">
                          <img src="la-assets/coursefinder/img/coursesCards/icons/2.svg" alt="icon">
                        </div>
                        <div class="text-14 lh-1"><?php echo $result->intake; ?></div>
                      </div>

                      <div class="d-flex items-center">
                        <div class="mr-8">
                          <img src="la-assets/coursefinder/img/coursesCards/icons/1.svg" alt="icon">
                        </div>
                        <div class="text-14 lh-1"><?php echo $result->course_type; ?></div>
                      </div>

                    </div>

                    <div class="coursesCard-footer">
                      <div class="coursesCard-footer__author">
                        <div>Annual Tuition Fees</div>
                      </div>

                      <div class="coursesCard-footer__price">
                        <div></div>
                        <div>&#8377; <?php echo $result->tuition_fees; ?></div>
                      </div>
                    </div>
                  </div>
                </a>

              </div>
              <?php }} else { echo "No courses to display"; } ?>
              <!-- End Course list -->
            </div>
          </div>
        </section>

        <footer class="footer -type-1 bg-dark-1 -green-links">
          <div class="container">
            
            <div class="py-30 border-top-light-15">
              <div class="row justify-between items-center y-gap-20">
                <div class="col-auto">
                  <div class="d-flex items-center h-100 text-white">
                    © 2022 ZODOCS. All Right Reserved.
                  </div>
                </div>

                <div class="col-auto">
                  <div class="d-flex x-gap-20 y-gap-20 items-center flex-wrap">
                    <div>Powered By
                      <a href="http://demo.zodocs.com/" class="text-white" target="_blank">
                        <span class="text-15 ft_txt">ZODOCS</span>
                      </a>. Solution By
                      <a href="https://www.labgex.com/" class="text-white" target="_blank">
                        <span class="text-15 ft_txt">LabGex Innovations (P) Ltd</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>


      </div>
    </main>
  </div>
  <!-- barba container end -->

  <!-- JavaScript -->
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
  <script src="la-assets/coursefinder/js/vendors.js"></script>
  <script src="la-assets/coursefinder/js/main.js"></script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

  <script>
    $(".searchbtn").click(function(){
      var course_country = $("#course_country").val(); 
      var course_university = $("#course_university").val();
      var course_intake = $("#course_intake").val();
      var course_name = $("#course_name").val();
      var course_price = $("#course_price").val();
      window.location="<?php echo URL::to('coursesearch4'); ?>?course_country="+course_country+"&course_university="+course_university+"&course_intake="+course_intake+"&course_name="+course_name+"&course_price="+course_price;
    });
    $(".clearbtn").click(function(){
      window.location="<?php echo URL::to('coursesearch4'); ?>";
    });

    var course_country = document.querySelector('#course_country');
    dselect(course_country, {
        search: true
    });

    var course_university = document.querySelector('#course_university');
    dselect(course_university, {
        search: true
    });

    var course_price = document.querySelector('#course_price');
    dselect(course_price, {
        search: true
    });

    var course_name = document.querySelector('#course_name');
    dselect(course_name, {
        search: true
    });

    var course_intake = document.querySelector('#course_intake');
    dselect(course_intake, {
        search: true
    });

  </script>

</body>

</html>