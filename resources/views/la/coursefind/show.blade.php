@extends('la.layouts.app')

@section('htmlheader_title')
	Course View
@endsection


@section('main-content')
<div id="page-content" class="profile2">
	<div class="bg-primary clearfix">
		<div class="col-md-7">
			<div class="row">
				<div class="col-md-2">
					<?php if($applyDetails[0]->university_logo != '') { ?>
						<img class="profile-image" src="<?php echo $applyDetails[0]->university_logo; ?>" alt="">
					<?php } else {  ?>
						<!-- <img class="profile-image" src="{{ asset('la-assets/img/course_icon.png') }}" alt=""> -->
						<img class="profile-image" src="{{ asset('la-assets/coursefinder/img/coursesCards/no-image.png') }}" alt="">
					<?php } ?>
				</div>
				<div class="col-md-10 prname">
					<h4 class="name">{{ $applyDetails[0]->course_name }}</h4>
					<div class="name">{{ $applyDetails[0]->course_type }}</div>
				</div>
			</div>
		</div>
		
		
		<div class="col-md-4"></div>
		
		<div class="col-md-1 actions">
				{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.coursefind.destroy', $applyDetails[0]->apply_id], 'method' => 'delete', 'style'=>'display:inline','onSubmit'=>"return confirm('Are you sure you wish to delete?');"]) }}
					<button class="btn btn-default btn-delete btn-xs" type="submit"><i class="fa fa-times"></i></button>
				{{ Form::close() }}
		</div>

		<div class="row">
			<div class="col-md-12 prdts-prof vhbottom"><br>
				<div class="dats1"><i class="fa fa-university mr5"></i> {{ $applyDetails[0]->university_name }} <span>|</span><i class="fa fa-map-marker mr5"></i> {{ $applyDetails[0]->country_name }} </div>
			</div>
		</div>
		
	</div>

	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		<li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> Personal Info</a></li>
		<li class=""><a role="tab" data-toggle="tab" href="#tab-account-settings" data-target="#tab-account-settings"><i class="fa fa-bars"></i> Intake</a></li>
		<li class=""><a role="tab" data-toggle="tab" href="#tab-account-settime" data-target="#tab-account-settime"><i class="fa fa-bars"></i>  University</a></li>
		
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>Personal Info</h4>
					</div><br>
					<div class="form-group"><label for="name" class="col-md-2">Name :</label><div class="col-md-10 fvalue">{{ $applyDetails[0]->name }}</div></div>
					<div class="form-group"><label for="name" class="col-md-2">Mobile :</label><div class="col-md-10 fvalue">{{ $applyDetails[0]->mobile }}</div></div>
					<div class="form-group"><label for="name" class="col-md-2">Email :</label><div class="col-md-10 fvalue">{{ $applyDetails[0]->email }}</div></div>
					<div class="form-group"><label for="name" class="col-md-2">Country :</label><div class="col-md-10 fvalue">{{ $applyDetails[0]->country_name }}</div></div>
					<div class="form-group"><label for="name" class="col-md-2">State :</label><div class="col-md-10 fvalue">{{ $applyDetails[0]->state }}</div></div>
					<div class="form-group"><label for="name" class="col-md-2">Course :</label><div class="col-md-10 fvalue">{{ $applyDetails[0]->course_name }}</div></div>
					<div class="form-group"><label for="name" class="col-md-2">University :</label><div class="col-md-10 fvalue">{{ $applyDetails[0]->university_name }}</div></div>
					<?php
					$dob = date("d M Y", strtotime($applyDetails[0]->dob));
					?>
					<div class="form-group"><label for="name" class="col-md-2">Date of Birth :</label><div class="col-md-10 fvalue">{{ $dob }}</div></div>
				</div>
			</div>
		</div>
		
		<div role="tabpanel" class="tab-pane fade" id="tab-account-settings">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>Intake</h4>
					</div><br>
				<div class="form-group"><label for="name" class="col-md-3">Gender :</label><div class="col-md-9 fvalue">{{ $applyDetails[0]->gender }}</div></div>
				<div class="form-group"><label for="name" class="col-md-3">Intake Year :</label><div class="col-md-9 fvalue">{{ $applyDetails[0]->intake_year }}</div></div>
				<div class="form-group"><label for="name" class="col-md-3">Intake Month :</label><div class="col-md-9 fvalue">{{ $applyDetails[0]->intake_month }}</div></div>
				<div class="form-group"><label for="name" class="col-md-3">Do you have a leap year? :</label><div class="col-md-9 fvalue">{{ $applyDetails[0]->leap_year }}</div></div>
				<div class="form-group"><label for="name" class="col-md-3">Marital Status :</label><div class="col-md-9 fvalue">{{ $applyDetails[0]->marital_status }}</div></div>
				<div class="form-group"><label for="name" class="col-md-3">Was your visa previously rejected? :</label><div class="col-md-9 fvalue">{{ $applyDetails[0]->visa_reject }}</div></div>
				</div>
			</div>
		</div>

		<div role="tabpanel" class="tab-pane fade" id="tab-account-settime">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>University</h4>
					</div><br>
				<div class="form-group"><label for="name" class="col-md-5">What is your highest education level? :</label><div class="col-md-7 fvalue">{{ $applyDetails[0]->education_level }}</div></div>
				<div class="form-group"><label for="name" class="col-md-5">Which english language test have you taken OR are planning to take? :</label><div class="col-md-7 fvalue">{{ $applyDetails[0]->language_test }}</div></div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>
	<meta name="csrf-token" content="{{ csrf_token() }}" />
</div>
@endsection

@push('scripts')
<script>

</script>
@endpush

