<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Google fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Outlined" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />

  <!-- Stylesheets -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="../la-assets/coursefinder/css/vendors.css">
  <link rel="stylesheet" href="../la-assets/coursefinder/css/main.css">
  <link rel="stylesheet" href="../la-assets/coursefinder/css/custom.css">

  <title>Apply Now</title>
</head>

<body class="preloader-visible" data-barba="wrapper">

  <!-- preloader start -->
  <div class="preloader js-preloader">
    <div class="preloader__bg"></div>
  </div>
  <!-- preloader end -->

  <!-- barba container start -->
  <div class="barba-container" data-barba="container">


    <main class="main-content  
  bg-beige-1
">

      <header data-anim="fade" data-add-bg="" class="header -base js-header">


        <div class="header__container py-10">
          <div class="row justify-between items-center">

            <div class="col-auto">
              <div class="header-left">

                <div class="header__logo ">
                  <a data-barba href="<?php echo url('/coursefinder'); ?>">
                    <img src="../la-assets/coursefinder/img/general/logo.png" alt="logo">
                  </a>
                </div>

              </div>
            </div>


            <div class="col-auto">
              <div class="header-right d-flex items-center">

                <div class="header-menu js-mobile-menu-toggle ">
                  <div class="header-menu__content">
                    <div class="mobile-bg js-mobile-bg"></div>

                    <div class="d-none xl:d-flex items-center px-20 py-20 border-bottom-light">
                      
                      <?php 
                      if(isset(Auth::user()->type)) { ?>
                        <a href="<?php echo url('/logout'); ?>" class="text-dark-1">Log out</a>
                        <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="text-dark-1 ml-30">My Account</a>
                      <?php } else { ?>
                        <a href="<?php echo url('/userlogin'); ?>" class="text-dark-1">Log in</a>
                        <a href="<?php echo url('/signup'); ?>" class="text-dark-1 ml-30">Sign up</a>
                      <?php } ?>
                      
                    </div>

                    <div class="menu js-navList">
                      <ul class="menu__nav text-dark-1 -is-active">

                    <li class="menu-item-has-children">
                      <a data-barba href="#">Home</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Home</a>
                        </li>
                        <li><a href="<?php echo url('/coursefinder'); ?>">Home 1</a></li>
                        <li><a href="<?php echo url('/coursefinder2'); ?>">Home 2</a></li>
                        <li><a href="<?php echo url('/coursefinder3'); ?>">Home 3</a></li>
                        <li><a href="<?php echo url('/coursefinder4'); ?>">Home 4</a></li>
                        <li><a href="<?php echo url('/coursefinder5'); ?>">Home 5</a></li>
                        <li><a href="<?php echo url('/coursefinder6'); ?>">Home 6</a></li>
                        <li><a href="<?php echo url('/coursefinder7'); ?>">Home 7</a></li>
                        <li><a href="<?php echo url('/coursefinder8'); ?>">Home 8</a></li>
                        <li><a href="<?php echo url('/coursefinder9'); ?>">Home 9</a></li>
                        <li><a href="<?php echo url('/coursefinder10'); ?>">Home 10</a></li>
                      </ul>
                    </li>
                        <li>
                          <a data-barba href="<?php echo url('/aboutus'); ?>">
                            About Us <i class="text-13 ml-10"></i>
                          </a>
                        </li>

                        <li class="menu-item-has-children">
                          <a data-barba href="#">Courses</a>
                          <ul class="subnav">
                            <li class="menu__backButton js-nav-list-back">
                              <a href="#">Courses</a>
                            </li>
                            <li><a href="<?php echo url('/coursesearch'); ?>">Course 1</a></li>
                            <li><a href="<?php echo url('/coursesearch2'); ?>">Course 2</a></li>
                            <li><a href="<?php echo url('/coursesearch3'); ?>">Course 3</a></li>
                            <li><a href="<?php echo url('/coursesearch4'); ?>">Course 4</a></li>
                          </ul>
                        </li>
                        
                        <li>
                          <a data-barba href="<?php echo url('/blog'); ?>">
                            Blog <i class="text-13 ml-10"></i>
                          </a>
                        </li>

                        <li>
                          <a data-barba href="<?php echo url('/topcourses'); ?>">
                            Top Courses <i class="text-13 ml-10"></i>
                          </a>
                        </li>

                        <li>
                          <a data-barba href="<?php echo url('/contactus'); ?>">
                            Contact Us <i class="text-13 ml-10"></i>
                          </a>
                        </li>


                      </ul>
                    </div>

                    <div class="mobile-footer px-20 py-20 border-top-light js-mobile-footer">
                    
                    </div>
                  </div>

                  <div class="header-menu-close" data-el-toggle=".js-mobile-menu-toggle">
                    <div class="size-40 d-flex items-center justify-center rounded-full bg-white">
                      <!-- <div class="icon-close text-dark-1 text-16"></div> -->
                      <i class="fa fa-close"></i>
                    </div>
                  </div>

                  <div class="header-menu-bg"></div>
                </div>


                <div class="mr-30">

                  <div class="d-none xl:d-block ml-20">
                    <button class="text-dark-1 items-center" data-el-toggle=".js-mobile-menu-toggle">
                      <i class="fa fa-bars"></i>
                    </button>
                  </div>

                </div>

                <div class="header-right__buttons md:d-none">
                  <a href="<?php echo url('/signup'); ?>" class="button -sm -rounded -dark-1 text-white">Sign Up</a>
                </div>
              </div>
            </div>

          </div>
        </div>
      </header>


      <div class="content-wrapper  js-content-wrapper">

        <section class="form-page">
          <div class="form-page__img bg-dark-1">
            <div class="form-page-composition">
              <div class="-bg"><img data-move="30" class="js-mouse-move" src="../la-assets/coursefinder/img/login/bg.png" alt="bg"></div>
              <div class="-el-1"><img data-move="20" class="js-mouse-move" src="../la-assets/coursefinder/img/home-9/hero/bg.png" alt="image"></div>
              <br><br><br><br><br><br><br><br><br><br><br><div class="-el-2"><img data-move="40" class="js-mouse-move" src="../la-assets/coursefinder/img/home-9/hero/1.png" alt="icon"></div>
              <div class="-el-3"><img data-move="40" class="js-mouse-move" src="../la-assets/coursefinder/img/home-9/hero/2.png" alt="icon"></div>
              <div class="-el-4"><img data-move="40" class="js-mouse-move" src="../la-assets/coursefinder/img/home-9/hero/3.png" alt="icon"></div>
            </div>
          </div>

          <div class="form-page__content lg:py-50">
            <div class="container"><br><br><br><br>
              <div class="row justify-center items-center">
                <div class="col-xl-12 col-lg-12">
                  <div class="px-50 py-50 md:px-25 md:py-25 bg-white shadow-1 rounded-16">
                    <h3 class="text-30 lh-13">Apply Now</h3>

                    <?php if (count($errors) > 0) { ?>
                        <div class="alert alert-danger err_txt"><br>
                            <strong>Whoops!</strong> There were some problems with your input.<br>
                            <ul>
                                <?php foreach ($errors->all() as $error) { ?>
                                    <li><?php echo $error; ?></li>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php } 
                    if (session('success_message')) { ?>
                      <br><div class="alert alert-success success_txt"><?php echo session('success_message'); ?></div>
                    <?php } ?>
                    
                    <form class="contact-form respondForm__form row y-gap-20 pt-30" action="<?php echo url('/saveapplications'); ?>" method="post">
                      <?php echo csrf_field(); ?>
                      <div class="col-lg-12">
                        <?php echo $courseDetails[0]->university_name.' - '.$courseDetails[0]->course_name; ?>
                      </div>
                      <div class="col-lg-6">
                          <input type="hidden" id="university_id" name="university_id" value="<?php echo $courseDetails[0]->university; ?>">
                      </div>
                      <div class="col-lg-6">
                          <input type="hidden" id="course_id" name="course_id" value="<?php echo $courseDetails[0]->id; ?>">
                      </div>



                      <!-- Accordions -->
                      <div class="accordion -block js-accordion">

                  <div class="accordion__item bg-light-4">
                    <div class="accordion__button">
                      <div class="accordion__icon">
                        <div class="icon" data-feather="plus"></div>
                        <div class="icon" data-feather="minus"></div>
                      </div>
                      <span class="text-17 fw-500 text-dark-1">Personal Info</span>
                    </div>

                    <div class="accordion__content">
                      <div class="accordion__content__inner">
                        <p></p>

                        <div class="row">
                          <div class="col-lg-6">
                            <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Name *</label>
                            <input type="text" id="person_name" name="person_name" placeholder="Name" required>
                          </div>
                          <div class="col-lg-6">
                            <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Email *</label>
                              <input type="email" id="email" name="email" placeholder="Email" required>
                          </div>
                        </div><br>
                        <div class="row">
                          <div class="col-lg-6">
                            <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Contact Number *</label>
                              <input type="text" id="mobile" name="mobile" placeholder="Contact Number" required>
                          </div>
                          <div class="col-lg-6">
                            <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Country *</label>
                            <select name="country" id="country" required>
                              <option value="">Select</option>
                              <?php 
                                foreach($countries as $countriesData) {
                                  echo '<option value="'.$countriesData->id.'">'.$countriesData->country.'</option>';
                                }
                              ?>
                            </select>
                          </div>
                        </div><br>
                        <div class="row">
                          <div class="col-lg-6">
                            <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">State</label>
                              <input type="text" id="state" name="state" placeholder="State">
                          </div>
                          <div class="col-lg-6">
                            <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Date of Birth</label>
                            <input type="text" id="dob" name="dob" placeholder="Date of Birth" class="form-control datepicker1">
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>

                  <div class="accordion__item bg-light-4">
                    <div class="accordion__button">
                      <div class="accordion__icon">
                        <div class="icon" data-feather="plus"></div>
                        <div class="icon" data-feather="minus"></div>
                      </div>
                      <span class="text-17 fw-500 text-dark-1">Intake</span>
                    </div>

                    <div class="accordion__content">
                      <div class="accordion__content__inner">
                        <p></p>

                        <div class="row">
                          <div class="col-lg-6">
                            <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Gender</label>
                            <select name="gender" id="gender">
                              <option value="">Select</option>
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                            </select>
                          </div>
                          <div class="col-lg-6">
                            <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Intake Year</label>
                              <select name="intake_year" id="intake_year">
                                <option value="">Select</option>
                                <option value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
                                <option value="<?php echo date('Y', strtotime('+1 year')); ?>"><?php echo date('Y', strtotime('+1 year')); ?></option>
                                <option value="<?php echo date('Y', strtotime('+2 year')); ?>"><?php echo date('Y', strtotime('+2 year')); ?></option>
                              </select>
                          </div>
                        </div><br>
                        <div class="row">
                          <div class="col-lg-6">
                            <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Intake Month</label>
                              <select name="intake_month" id="intake_month">
                                <option value="">Select</option>
                                <option value="Jan">Jan</option>
                                <option value="Feb">Feb</option>
                                <option value="Mar">Mar</option>
                                <option value="Apr">Apr</option>
                                <option value="May">May</option>
                                <option value="Jun">Jun</option>
                                <option value="Jul">Jul</option>
                                <option value="Aug">Aug</option>
                                <option value="Sep">Sep</option>
                                <option value="Oct">Oct</option>
                                <option value="Nov">Nov</option>
                                <option value="Dec">Dec</option>
                              </select>
                          </div>
                          <div class="col-lg-6">
                            <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Do you have a leap year?</label>
                              <select name="leap_year" id="leap_year">
                                <option value="">Select</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                              </select>
                          </div>
                        </div><br>
                        <div class="row">
                          <div class="col-lg-6">
                            <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Marital Status</label>
                              <select name="marital_status" id="marital_status">
                                <option value="">Select</option>
                                <option value="Unmarried">Unmarried</option>
                                <option value="Married">Married</option>
                              </select>
                          </div>
                          <div class="col-lg-6">
                            <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Was your visa previously rejected?</label>
                            <select name="visa_reject" id="visa_reject">
                              <option value="">Select</option>
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                            </select>
                          </div>
                      </div>

                      </div>
                    </div>
                  </div>

                  <div class="accordion__item bg-light-4">
                    <div class="accordion__button">
                      <div class="accordion__icon">
                        <div class="icon" data-feather="plus"></div>
                        <div class="icon" data-feather="minus"></div>
                      </div>
                      <span class="text-17 fw-500 text-dark-1">University</span>
                    </div>

                    <div class="accordion__content">
                      <div class="accordion__content__inner">
                        <p></p>

                        <div class="col-lg-12">
                          <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">What is your highest education level?</label>
                            <select name="education_level" id="education_level">
                              <option value="">Select</option>
                              <option value="Plus Two">Plus Two</option>
                              <option value="Undergraduate Degree">Undergraduate Degree</option>
                              <option value="Postgraduate Degree">Postgraduate Degree</option>
                              <option value="Undergraduate Diploma">Undergraduate Diploma</option>
                              <option value="Postgraduate Diploma">Postgraduate Diploma</option>
                            </select>
                        </div><br>
                        <div class="col-lg-12">
                          <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Which english language test have you taken OR are planning to take?</label>
                            <select name="language_test" id="language_test">
                              <option value="">Select</option>
                              <option value="IELTS">IELTS</option>
                              <option value="TOEFL">TOEFL</option>
                              <option value="PTE">PTE</option>
                              <option value="Not Planning To Take Any">Not Planning To Take Any</option>
                            </select>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
                <!-- End Accordions -->
 
                      <div class="col-12">
                        <button type="submit" id="submit" class="button -md -green-1 text-dark-1 fw-500 w-1/1">
                          Apply Now
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </main>
  </div>
  <!-- barba container end -->

  <!-- JavaScript -->
  <link rel="stylesheet" href="../la-assets/plugins/datepicker/datepicker3.css"/> <!-- Date Picker -->
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="../la-assets/plugins/datepicker/bootstrap-datepicker.js"></script>


  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
  <script src="../la-assets/coursefinder/js/vendors.js"></script>
  <script src="../la-assets/coursefinder/js/main.js"></script>


  <script>
  $(function () { 
    $('.datepicker1').datepicker({
      autoclose: true,
      endDate: new Date(),
      format: 'yyyy-mm-dd'
    });
  });
  </script>


</body>

</html>