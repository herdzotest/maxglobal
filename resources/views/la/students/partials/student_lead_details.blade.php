<div id="tab_12" class="tab-pane stud_lead" >
        
                   
<div class="content-head">
                                        
                                        <h4>Student Lead Details</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                        </div>
                        {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'personal-add-form']) !!}
                <div class="pane" >
                        <div class="row">
                                <div class=" col s6 ipfield">
                                        <div class="input-field">
                                                <input name="lead_type"  id="lead_type" type="text">
                                                <label for="lead_type">Lead Type</label>
                                        </div>
                                </div>
                                <div class=" col s6 ipfield">
					<div class="input-field">
					<select class="searchSelect" id="lead_from" name="lead_from">
                                                <option value="">Select</option>
                                                <option value="Agents"> Agents </option>
                                                <option value="Facebook"> Facebook </option>
                                                <option value="Instagram"> Instagram </option>
                                                <option value="Google"> Google </option>
                                                <option value="Email"> Email </option>
                                                <option value="Help Desk"> Help Desk </option>
                                                <option value="Reference"> Reference </option>
                                                <option value="Walk In"> Walk In </option>
                                                <option value="Personal"> Personal </option>
	                                </select>
					<label for="lead_from" style="margin-top: -40px;font-size: 13px;">Lead Generated From</label>
					</div>
				</div>
                        </div>
                        <div id="lead_agents" class="hide">
                        <div class="row">
                                <div class=" col s6 ipfield">
                                        <div class="input-field">
                                                <input name="agent_id" id="agent_id" type="hidden">
                                                <select class="searchSelect" id="agent" name="agent">
							<option class="searchSelect" value="">Select</option>
							<?php
								foreach($agents as $agentData) {
									echo '<option data-id="'.$agentData->id.'" value="'.$agentData->name.'">'.$agentData->name.'</option>';
								}
							?>											
						</select>
                                                <label for="agent" style="margin-top: -40px;font-size: 13px;">Agents</label>
                                        </div>
                                </div>
                        </div>
                        </div>
                </div>
                <div class="controls">
                                <a class="mgbtn2 back_button">Back</a>
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="student_lead">
                                 <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                              
                                {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                        </div>
                        {{ Form::close() }}
</div>
