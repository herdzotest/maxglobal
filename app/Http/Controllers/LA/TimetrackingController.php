<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;

class TimetrackingController extends Controller
{
	public function __construct() {
		
		if(Auth::user()->type != 'SUPER_ADMIN' && Auth::user()->type != 'MANAGER')
			return abort(403);		
	}
	
	/**
	 * Display a listing of time track settings.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$timeSettings = DB::table('time_tracking_settings')->first();

		return View('la.timetracking.index', [ ])->with('timeSettings', $timeSettings);
	}
	
	/**
	 * Show the form for editing the time track settings.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show()
	{
		$timeSettings = DB::table('time_tracking_settings')->first();
		
		return View('la.timetracking.edit', [ ])->with('timeSettings', $timeSettings);
	}
	
	/**
	 * Store a newly created setting in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request)
	{
		if(count($request->all()) > 0) {
			$postedData = $request->all();
			$timeTrack = DB::table('time_tracking_settings')->find($postedData['id']);
			
			if($timeTrack) {
				DB::table('time_tracking_settings')->where('id', $postedData['id'])
					->update(['status' => $postedData['status'],'from_time' => $postedData['input_starttime'],
					'to_time' => $postedData['input_endtime']]);
			} else {
				DB::table('time_tracking_settings')->insert([
					'status' => $postedData['status'],
					'from_time' => $postedData['input_starttime'],
					'to_time' => $postedData['input_endtime']
				]);
			}
		}
		
		return redirect()->route(config('laraadmin.adminRoute') . '.settings.timetracking.index');
	}
	
}
