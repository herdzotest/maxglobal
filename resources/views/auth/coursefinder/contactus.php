<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Google fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Outlined" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />

  <!-- Stylesheets -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/vendors.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/main.css">
  <link rel="stylesheet" href="la-assets/coursefinder/css/custom.css">

  <title>Contact Us</title>
  <link rel="icon" type="image/x-icon" href="la-assets/coursefinder/img/coursesCards/icons/zodocs_fav.png">
</head>

<body class="preloader-visible" data-barba="wrapper">

  <!-- preloader start -->
  <div class="preloader js-preloader">
    <div class="preloader__bg"></div>
  </div>
  <!-- preloader end -->

  <!-- barba container start -->
  <div class="barba-container" data-barba="container">


    <main class="main-content  ">

      <header data-anim="fade" data-add-bg="bg-dark-1" class="header -type-1 js-header">


        <div class="header__container">
          <div class="row justify-between items-center">

            <div class="col-auto">
              <div class="header-left">

                <div class="header__logo ">
                  <a data-barba href="<?php echo url('/coursefinder'); ?>">
                    <img src="la-assets/coursefinder/img/general/logo.png" alt="logo">
                  </a>
                </div>

              </div>
            </div>


            <div class="header-menu js-mobile-menu-toggle ">
              <div class="header-menu__content">
                <div class="mobile-bg js-mobile-bg"></div>

                <div class="d-none xl:d-flex items-center px-20 py-20 border-bottom-light">
                  
                  <?php 
                  if(isset(Auth::user()->type)) { ?>
                    <a href="<?php echo url('/logout'); ?>" class="text-dark-1">Log out</a>
                    <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="text-dark-1 ml-30">My Account</a>
                  <?php } else { ?>
                    <a href="<?php echo url('/userlogin'); ?>" class="text-dark-1">Log in</a>
                    <a href="<?php echo url('/signup'); ?>" class="text-dark-1 ml-30">Sign up</a>
                  <?php } ?>

                </div>

                <div class="menu js-navList">
                  <ul class="menu__nav text-white -is-active">

                    <li class="menu-item-has-children">
                      <a data-barba href="#">Home</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Home</a>
                        </li>
                        <li><a href="<?php echo url('/coursefinder'); ?>">Home 1</a></li>
                        <li><a href="<?php echo url('/coursefinder2'); ?>">Home 2</a></li>
                        <li><a href="<?php echo url('/coursefinder3'); ?>">Home 3</a></li>
                        <li><a href="<?php echo url('/coursefinder4'); ?>">Home 4</a></li>
                        <li><a href="<?php echo url('/coursefinder5'); ?>">Home 5</a></li>
                        <li><a href="<?php echo url('/coursefinder6'); ?>">Home 6</a></li>
                        <li><a href="<?php echo url('/coursefinder7'); ?>">Home 7</a></li>
                        <li><a href="<?php echo url('/coursefinder8'); ?>">Home 8</a></li>
                        <li><a href="<?php echo url('/coursefinder9'); ?>">Home 9</a></li>
                        <li><a href="<?php echo url('/coursefinder10'); ?>">Home 10</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/aboutus'); ?>">
                        About Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li class="menu-item-has-children">
                      <a data-barba href="#">Courses</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Courses</a>
                        </li>
                        <li><a href="<?php echo url('/coursesearch'); ?>">Course 1</a></li>
                        <li><a href="<?php echo url('/coursesearch2'); ?>">Course 2</a></li>
                        <li><a href="<?php echo url('/coursesearch3'); ?>">Course 3</a></li>
                        <li><a href="<?php echo url('/coursesearch4'); ?>">Course 4</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/blog'); ?>">
                        Blog <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/topcourses'); ?>">
                        Top Courses <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/contactus'); ?>">
                        Contact Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>


                  </ul>
                </div>

                
              </div>

            </div>


            <div class="col-auto">
              <div class="header-right d-flex items-center">

                <div class="header-right__icons text-white d-flex items-center">

                  <div class="d-none xl:d-block ml-20">
                    <button class="text-white items-center" data-el-toggle=".js-mobile-menu-toggle">
                      <i class="fa fa-bars"></i>
                    </button>
                  </div>

                </div>
                
                <div class="header-right__buttons d-flex items-center ml-30 md:d-none">
                  
                  <?php 
                  if(isset(Auth::user()->type)) { ?>
                    <a href="<?php echo url('/logout'); ?>" class="button -underline text-white">Log out</a>
                    <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="button -sm -white text-dark-1 ml-30">My Account</a>
                  <?php } else { ?>
                    <a href="<?php echo url('/userlogin'); ?>" class="button -underline text-white">Log in</a>
                    <a href="<?php echo url('/signup'); ?>" class="button -sm -white text-dark-1 ml-30">Sign up</a>
                  <?php } ?>
                  
                </div>
              </div>
            </div>

          </div>
        </div>
      </header>


      <div class="content-wrapper  js-content-wrapper">

        <section class="">
          <!-- <div id="map" class="map"></div> -->
          <div id="map1" class="map"><iframe src="https://www.google.com/maps/embed/v1/place?q=G3B,+Thejaswini+Building,+Technopark+Campus,+Thiruvananthapuram&key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8" width="1650" height="500" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe></div>
        </section>

        <section class="layout-pt-md layout-pb-lg">
          <div data-anim-wrap class="container"><br><br>
            <div class="row y-gap-50 justify-between">
              <div class="col-lg-4">
                <h3 class="text-24 fw-500">Keep In Touch With Us.</h3>
                <!-- <p class="mt-25">Neque convallis a cras semper auctor. Libero id faucibus nisl tincidunt egetnvallis.</p> -->

                <div class="y-gap-30 pt-60 lg:pt-40">

                  <div class="d-flex items-center">
                    <div class="d-flex justify-center items-center size-60 rounded-full bg-light-7">
                      <img src="la-assets/coursefinder/img/contact-1/1.svg" alt="icon">
                    </div>
                    <div class="ml-20">G3B, Thejaswini Building,<br>
                    Technopark Campus,<br>
                    Thiruvananthapuram - 695581</div>
                  </div>

                  <div class="d-flex items-center">
                    <div class="d-flex justify-center items-center size-60 rounded-full bg-light-7">
                      <img src="la-assets/coursefinder/img/contact-1/2.svg" alt="icon">
                    </div>
                    <div class="ml-20">+91 9745 746 250</div>
                  </div>

                  <div class="d-flex items-center">
                    <div class="d-flex justify-center items-center size-60 rounded-full bg-light-7">
                      <img src="la-assets/coursefinder/img/contact-1/3.svg" alt="icon">
                    </div>
                    <div class="ml-20">inbox@labgex.com</div>
                  </div>

                </div>
              </div>

              <div class="col-lg-7">
                <h3 class="text-24 fw-500">Send a Message</h3>
                <!-- <p class="mt-25">Neque convallis a cras semper auctor. Libero id faucibus nisl<br> tincidunt egetnvallis.</p> -->

                <form class="contact-form row y-gap-30 pt-60 lg:pt-40" action="#">
                  <div class="col-md-6">
                    <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Name</label>
                    <input type="text" name="title" placeholder="Name...">
                  </div>
                  <div class="col-md-6">
                    <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Email Address</label>
                    <input type="text" name="title" placeholder="Email...">
                  </div>
                  <div class="col-12">
                    <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Message...</label>
                    <textarea name="comment" placeholder="Message" rows="8"></textarea>
                  </div>
                  <div class="col-12">
                    <button type="submit" name="submit" id="submit" class="button -md -purple-1 text-white">
                      Send Message
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>

        <section class="layout-pt-lg layout-pb-lg bg-light-4">
          <div class="container">
            <div class="row justify-center text-center">
              <div class="col-xl-8 col-lg-9 col-md-11">

                <div class="sectionTitle ">

                  <h2 class="sectionTitle__title ">Frequently Asked Questions</h2>

                  <p class="sectionTitle__text ">Find everything you need to start your overseas education journey with confidence!</p>

                </div>


                <div class="accordion -block text-left pt-60 lg:pt-40 js-accordion">

                  <div class="accordion__item">
                    <div class="accordion__button">
                      <div class="accordion__icon">
                        <div class="icon" data-feather="plus"></div>
                        <div class="icon" data-feather="minus"></div>
                      </div>
                      <span class="text-17 fw-500 text-dark-1">Why choose Zodocs?</span>
                    </div>

                    <div class="accordion__content">
                      <div class="accordion__content__inner">
                        <p>We connect ambitious students with top universities and institutions worldwide, offering a seamless platform for exploring courses, submitting applications, and receiving expert guidance.</p>
                      </div>
                    </div>
                  </div>

                  <div class="accordion__item">
                    <div class="accordion__button">
                      <div class="accordion__icon">
                        <div class="icon" data-feather="plus"></div>
                        <div class="icon" data-feather="minus"></div>
                      </div>
                      <span class="text-17 fw-500 text-dark-1">Do you feature only specific countries or universities on Zodocs?</span>
                    </div>

                    <div class="accordion__content">
                      <div class="accordion__content__inner">
                        <p>Zodocs offers a wide selection of courses and universities from popular study destinations such as the USA, UK, Canada, Australia, and more.</p>
                      </div>
                    </div>
                  </div>

                  <div class="accordion__item">
                    <div class="accordion__button">
                      <div class="accordion__icon">
                        <div class="icon" data-feather="plus"></div>
                        <div class="icon" data-feather="minus"></div>
                      </div>
                      <span class="text-17 fw-500 text-dark-1">Can I explore courses before creating an account?</span>
                    </div>

                    <div class="accordion__content">
                      <div class="accordion__content__inner">
                        <p>Yes, you can browse courses and universities and submit the application without an account. However, creating an account allows you to save your preferences and begin the application process.</p>
                      </div>
                    </div>
                  </div>

                  <div class="accordion__item">
                    <div class="accordion__button">
                      <div class="accordion__icon">
                        <div class="icon" data-feather="plus"></div>
                        <div class="icon" data-feather="minus"></div>
                      </div>
                      <span class="text-17 fw-500 text-dark-1">How do I apply to universities through Zodocs?</span>
                    </div>

                    <div class="accordion__content">
                      <div class="accordion__content__inner">
                        <p>Browse available courses, select your preferred program and submit your application. Log in to your account and upload the required documents with necessary data and track its status directly through the portal.</p>
                      </div>
                    </div>
                  </div>

                  <div class="accordion__item">
                    <div class="accordion__button">
                      <div class="accordion__icon">
                        <div class="icon" data-feather="plus"></div>
                        <div class="icon" data-feather="minus"></div>
                      </div>
                      <span class="text-17 fw-500 text-dark-1">What documents are required by Zodocs for applying to a foreign university?</span>
                    </div>

                    <div class="accordion__content">
                      <div class="accordion__content__inner">
                        <p>Typical documents include academic transcripts, a statement of purpose (SOP), letters of recommendation, a valid passport, and proof of English proficiency (IELTS/TOEFL).</p>
                      </div>
                    </div>
                  </div>

                  <div class="accordion__item">
                    <div class="accordion__button">
                      <div class="accordion__icon">
                        <div class="icon" data-feather="plus"></div>
                        <div class="icon" data-feather="minus"></div>
                      </div>
                      <span class="text-17 fw-500 text-dark-1">How do I upload my documents through Zodocs?</span>
                    </div>

                    <div class="accordion__content">
                      <div class="accordion__content__inner">
                        <p>Log in to your account on the portal, navigate to your profile section and upload the required documents mentioned under 'Academic Details', 'Employment History', 'Further Details' and 'Immigration History'. If you face any issues, contact the support team for assistance.</p>
                      </div>
                    </div>
                  </div>

                  <div class="accordion__item">
                    <div class="accordion__button">
                      <div class="accordion__icon">
                        <div class="icon" data-feather="plus"></div>
                        <div class="icon" data-feather="minus"></div>
                      </div>
                      <span class="text-17 fw-500 text-dark-1">What is a Statement of Purpose (SOP), and how do I write one?</span>
                    </div>

                    <div class="accordion__content">
                      <div class="accordion__content__inner">
                        <p>An SOP is a personal essay explaining your motivation for studying the course and your career aspirations. It should be clear, concise, and personalized for each university.</p>
                      </div>
                    </div>
                  </div>

                  <div class="accordion__item">
                    <div class="accordion__button">
                      <div class="accordion__icon">
                        <div class="icon" data-feather="plus"></div>
                        <div class="icon" data-feather="minus"></div>
                      </div>
                      <span class="text-17 fw-500 text-dark-1">Can I track my application status on Zodocs?</span>
                    </div>

                    <div class="accordion__content">
                      <div class="accordion__content__inner">
                        <p>Yes, our portal allows you to monitor the progress of your applications and receive updates in real-time.</p>
                      </div>
                    </div>
                  </div>

                  <div class="accordion__item">
                    <div class="accordion__button">
                      <div class="accordion__icon">
                        <div class="icon" data-feather="plus"></div>
                        <div class="icon" data-feather="minus"></div>
                      </div>
                      <span class="text-17 fw-500 text-dark-1">How can I find out my next follow-up date?</span>
                    </div>

                    <div class="accordion__content">
                      <div class="accordion__content__inner">
                        <p>Once you log in to your student portal, your next follow-up date will be displayed on your profile under the "Follow Up" section. You will also receive email or SMS notifications reminding you of the date.</p>
                      </div>
                    </div>
                  </div>

                  <div class="accordion__item">
                    <div class="accordion__button">
                      <div class="accordion__icon">
                        <div class="icon" data-feather="plus"></div>
                        <div class="icon" data-feather="minus"></div>
                      </div>
                      <span class="text-17 fw-500 text-dark-1">Is there any assistance available for follow-up sessions?</span>
                    </div>

                    <div class="accordion__content">
                      <div class="accordion__content__inner">
                        <p>Absolutely! Our counselors will guide you during your follow-up session to clarify doubts, track application status, and address any concerns you might have.</p>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </section>

        <footer class="footer -type-1 bg-dark-1 -green-links">
          <div class="container">
            
            <div class="py-30 border-top-light-15">
              <div class="row justify-between items-center y-gap-20">
                <div class="col-auto">
                  <div class="d-flex items-center h-100 text-white">
                    © 2022 ZODOCS. All Right Reserved.
                  </div>
                </div>

                <div class="col-auto">
                  <div class="d-flex x-gap-20 y-gap-20 items-center flex-wrap">
                    <div>Powered By
                      <a href="http://demo.zodocs.com/" class="text-white" target="_blank">
                        <span class="text-15">ZODOCS</span>
                      </a>. Solution By
                      <a href="https://www.labgex.com/" class="text-white" target="_blank">
                        <span class="text-15">LabGex Innovations (P) Ltd</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>


      </div>
    </main>
  </div>
  <!-- barba container end -->

  <!-- JavaScript -->
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
  <script src="la-assets/coursefinder/js/vendors.js"></script>
  <script src="la-assets/coursefinder/js/main.js"></script>

</body>

</html>