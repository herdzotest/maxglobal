<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
	<a href="{{ url(config('laraadmin.adminRoute')) }}" class="logo">

		 <div class="sb-logo">
			<img src="{{asset('la-assets/img/Zodocs_logo.png')}}" alt="Zodocs">
		 </div>
	</a>
        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::fallback(asset('la-assets/img/user2-160x160.jpg'))->get(Auth::user()->email) }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
        @if(LAConfigs::getByKey('sidebar_search'))
		<!--
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
	                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form> -->
        @endif
        <!-- /.search form -->

		<?php 
        $action = app('request')->route()->getAction();
        $controller = class_basename($action['controller']);
		$controllerName = explode("@",$controller);
		$controllerName = str_replace('Controller','',$controllerName[0]);
		$currentUrl = $_SERVER['REQUEST_URI'];
		$type = (strpos($currentUrl,'student') !== false) ? 'student' : 'nurse';
		?>
		
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MODULES</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="<?php echo ($controllerName == 'Dashboard') ? 'active_menu' : ''; ?>"><a href="{{ url(config('laraadmin.adminRoute')) }}"><i class='icon-speedometer'></i> <span>Dashboard</span></a></li>
            <?php
            $menuItems = Dwij\Laraadmin\Models\Menu::where("parent", 0)->orderBy('hierarchy', 'asc')->get();
            ?>
            @foreach ($menuItems as $menu)
                @if($menu->type == "module")
                    <?php
                    $temp_module_obj = Module::get($menu->name);
                    ?>
                    @la_access($temp_module_obj->id)
						@if(isset($module->id) && $module->name == $menu->name)
                        	<?php echo LAHelper::print_menu($menu ,true); ?>
						@else
							<?php echo LAHelper::print_menu($menu); ?>
						@endif
                    @endla_access
                @else
                    <?php echo LAHelper::print_menu($menu); ?>
                @endif
            @endforeach
            <!-- LAMenus -->
			
			@if(!in_array(Auth::user()->type,["NURSE","EMPLOYER_ROLE","STUDENT"])) 
				
				<?php
				$activeMenu = '';
				if($controllerName == 'Students' || (in_array($controllerName,['Managers','Consultants','MainAgents','SubContractors','UniversityUser']) && $type == 'student') ) { 
					$activeMenu = 'active_menu';

					
				}

					$context_id = Auth::user()->context_id;
					$type_of_user = DB::table('employees')->select('type_user')->addselect('dept')->where('id', $context_id)->get();
					if($type_of_user[0]->type_user == '1' || in_array(Auth::user()->type,["SUPER_ADMIN","MANAGER"])){
						$typ = 1;
					} else {
						$typ = 0;
					}

				?>
				<?php if($typ == 1){ ?>

				<li class="treeview <?php echo $activeMenu; ?>">
					<a href="#"> <i class="icon-people"></i> <span>Students</span><i class="fa fa-angle-left pull-right"></i></a>
					<ul class="treeview-menu <?php echo ($activeMenu != '') ? 'menu-open' : '' ?>" style="<?php echo ($activeMenu != '') ? 'display:block;' : '' ?>">
						<li class="<?php echo ($controllerName == 'Students') ? 'active_sub_menu' : ''; ?>"><a href="{{ route('admin.students.index') }}"><span>Student Lists</span></a></li>
						@if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER","CONSULTANTS"]))
							
							@role("SUPER_ADMIN")
								<li class="<?php echo ($controllerName == 'Managers' && $type == 'student') ? 'active_sub_menu' : ''; ?>"><a href="{{ url(config('laraadmin.adminRoute') . '/student/managers') }}"> <span>Managers</span></a></li>
							@endrole
							@if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER"]))
								<li class="<?php echo ($controllerName == 'Consultants' && $type == 'student') ? 'active_sub_menu' : ''; ?>"><a href="{{ url(config('laraadmin.adminRoute') . '/student/consultants') }}"> <span>Staff</span></a></li>
							@endif
							<li class="<?php echo ($controllerName == 'MainAgents' && $type == 'student') ? 'active_sub_menu' : ''; ?>"><a href="{{ url(config('laraadmin.adminRoute') . '/student/main_agents') }}"> <span>Main Agents</span></a></li>
							<li class=""><a href="{{ url(config('laraadmin.adminRoute') . '/student/sub_contractors') }}"> <span>Sub-contractors</span></a></li>
							<li class=""><a href="{{ route('admin.university_users.index') }}"> <span>University users</span></a></li>
						@endif
					</ul>				
				</li>


			<?php } ?>

			


			@endif     
			
			@if(!in_array(Auth::user()->type,["STUDENT","UNIVERSITY_USER","NURSE"]))
				
				<?php
				$activeMenu = '';
				if($controllerName == 'Nurses' || (in_array($controllerName,['Managers','Consultants','MainAgents','SubContractors','Employer']) && $type == 'nurse') ) {
					$activeMenu = 'active_menu';
				}

				    $context_id = Auth::user()->context_id;
					$type_of_user = DB::table('employees')->select('type_user')->addselect('dept')->where('id', $context_id)->get();
					if($type_of_user[0]->type_user == '0' || in_array(Auth::user()->type,["SUPER_ADMIN","MANAGER"]) ){
						$typ = 0;
					} else {
						$typ = 1;
					}

				?>	

				<?php if($typ == 0){ ?>		
				<li class="treeview <?php echo $activeMenu; ?>">
					<a href="#"><i class="icon-people"></i> <span>Nurses</span> <i class="fa fa-angle-left pull-right"></i></a>
					<ul class="treeview-menu <?php echo ($activeMenu != '') ? 'menu-open' : '' ?>" style="<?php echo ($activeMenu != '') ? 'display:block;' : '' ?>">
						<li class="<?php echo ($controllerName == 'Nurses') ? 'active_sub_menu' : ''; ?>"><a href="{{ route('admin.nurses.index') }}"> <span>Nurses Lists</span></a></li>
						@if(Auth::user()->type == "SUPER_ADMIN" || Auth::user()->type == "MANAGER")
							
							@role("SUPER_ADMIN")
								<li class="<?php echo ($controllerName == 'Managers' && $type == 'student') ? 'active_sub_menu' : ''; ?>"><a href="{{ url(config('laraadmin.adminRoute') . '/nurse/managers') }}"> <span>Managers</span></a></li>
							@endrole
							<li class="<?php echo ($controllerName == 'Consultants' && $type == 'nurse') ? 'active_sub_menu' : ''; ?>"><a href="{{ url(config('laraadmin.adminRoute') . '/nurse/consultants') }}"> <span>Consultants</span></a></li>
							<li class="<?php echo ($controllerName == 'MainAgents' && $type == 'nurse') ? 'active_sub_menu' : ''; ?>"><a href="{{ url(config('laraadmin.adminRoute') . '/nurse/main_agents') }}"> <span>Main Agents</span></a></li>
							<li class="<?php echo ($controllerName == 'SubContractors' && $type == 'nurse') ? 'active_sub_menu' : ''; ?>"><a href="{{ url(config('laraadmin.adminRoute') . '/nurse/sub_contractors') }}"> <span>Sub-contractors</span></a></li>
							<li class="<?php echo ($controllerName == 'Employer' && $type == 'nurse') ? 'active_sub_menu' : ''; ?>"><a href="{{ route('admin.employer.index') }}"> <span>Employers</span></a></li>
						@endif
					</ul>					
				</li>

				<?php } ?>

			@endif

			@if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER"]))
				<li class="<?php echo ($controllerName == 'Coursefind') ? 'active_menu' : ''; ?>"><a href="{{ url(config('laraadmin.adminRoute') . '/coursefind') }}"> <i class="fa fa-bars"></i><span>Course Finder</span></a></li>
			@endif

			@if(Auth::user()->type == "CONSULTANTS" || Auth::user()->type == "MANAGER" || Auth::user()->type == "SUPER_ADMIN" || Auth::user()->type == "MAIN_AGENTS" || Auth::user()->type == "UNIVERSITY_USER" || Auth::user()->type == "SUBCONTRACTORS" || Auth::user()->type == "EMPLOYER_ROLE")
			<?php $id=Auth::user()->context_id; ?>
				<li class="<?php echo ($controllerName == 'EmployeesController') ? 'active_menu' : ''; ?>"><a href="<?php echo url(config("laraadmin.adminRoute") . '/user_profile/'.$id.'/view'); ?>"><i class="icon-user"></i> <span>View Profile</span> </a></li>	
			@endif

			@if(Auth::user()->type == "STUDENT")
			<?php $id=Auth::user()->id; ?>
				<li class="<?php echo ($controllerName == 'StudentsController') ? 'active_menu' : ''; ?>"><a href="<?php echo url(config("laraadmin.adminRoute") . '/students/'.$id); ?>"><i class="icon-user"></i> <span>View Profile</span> </a></li>
			@endif

			@if(Auth::user()->type == "NURSE")
			<?php $id=Auth::user()->id; ?>
				<li class="<?php echo ($controllerName == 'NursesController') ? 'active_menu' : ''; ?>"><a href="<?php echo url(config("laraadmin.adminRoute") . '/nurses/'.$id); ?>"><i class="icon-user"></i> <span>View Profile</span> </a></li>
			@endif
            
			@if(Auth::user()->type == "SUPER_ADMIN" || Auth::user()->type == "MANAGER")
				
				<?php
				$activeMenu = '';
				if(in_array($controllerName,['Timetracking','Couses','Campus','Countries','University','Syllabus','Stream','Studentstatus'])) {
					$activeMenu = 'active_menu';
				}
				?>					
			
				<li class="treeview <?php echo $activeMenu; ?>">
					<a href="javascript:void(0);">
						<i class="fa fa-gears"></i> 
						<span>Settings</span> <i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu <?php echo ($activeMenu != '') ? 'menu-open' : '' ?>" style="<?php echo ($activeMenu != '') ? 'display:block;' : '' ?>">
						<!-- <li class="<?php //echo ($controllerName == 'Timetracking') ? 'active_sub_menu' : ''; ?>"><a href="{{ route('admin.settings.timetracking.index') }}"><i class="icon-clock"></i> <span>Time tracking</span></a></li> -->
						<li class="<?php echo ($controllerName == 'Couses') ? 'active_sub_menu' : ''; ?>"><a href="{{ route('admin.courses.index') }}"><i class="icon-settings"></i> <span>Manage Courses</span></a></li>
						<li class="<?php echo ($controllerName == 'Countries') ? 'active_sub_menu' : ''; ?>"><a href="{{ route('admin.countries.index') }}"><i class="icon-settings"></i> <span>Manage Countries</span></a></li>
						<li class="<?php echo ($controllerName == 'University') ? 'active_sub_menu' : ''; ?>"><a href="{{ route('admin.university.index') }}"><i class="icon-settings"></i> <span>Manage Universities</span></a></li>
						<li class="<?php echo ($controllerName == 'Syllabus') ? 'active_sub_menu' : ''; ?>"><a href="{{ route('admin.syllabus.index') }}"><i class="icon-settings"></i> <span>Manage Syllabus</span></a></li>
						<li class="<?php echo ($controllerName == 'Campus') ? 'active_sub_menu' : ''; ?>"><a href="{{ route('admin.campus.index') }}"><i class="icon-settings"></i> <span>Manage Campus</span></a></li>
						<li class="<?php echo ($controllerName == 'Stream') ? 'active_sub_menu' : ''; ?>"><a href="{{ route('admin.stream.index') }}"><i class="icon-settings"></i> <span>Manage Stream</span></a></li>
						<li class="<?php echo ($controllerName == 'Studentstatus') ? 'active_sub_menu' : ''; ?>"><a href="{{ route('admin.studentstatus.index') }}"><i class="icon-settings"></i> <span>Application Status</span></a></li>
					</ul>
				</li>
			@endif

			@if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER"]))
				<li class="<?php echo ($controllerName == 'Report') ? 'active_menu' : ''; ?>"><a href="{{ url(config('laraadmin.adminRoute') . '/report') }}"> <i class="fa fa-bars"></i><span>Report</span></a></li>
			@endif

			@if(Auth::user()->type == 'SUPER_ADMIN' || Auth::user()->type == 'MANAGER')
				<li class="<?php echo ($controllerName == 'MailSettings') ? 'active_menu' : ''; ?>"><a href="{{ route('admin.mailsettings.index') }}"><i class="icon-envelope"></i> <span>Mail settings</span> </a></li>	
				<!-- <li class="<?php //echo ($controllerName == 'Uploads') ? 'active_menu' : ''; ?>"><a href="{{ url(config('laraadmin.adminRoute') . '/uploads') }}"><i class="fa fa-files-o"></i> <span>Uploaded files</span> </a></li> -->
			@endif

			@if(Auth::user()->type == 'SUPER_ADMIN' || Auth::user()->type == 'MANAGER' || Auth::user()->type == 'MAIN_AGENTS')	
				<li class="<?php echo ($controllerName == 'UploadsMedia') ? 'active_menu' : ''; ?>"><a href="{{ url(config('laraadmin.adminRoute') . '/uploads_media') }}"><i class="fa fa-files-o"></i> <span>Media files</span> </a></li>
			@endif 

		</ul><!-- /.sidebar-menu -->
    </section>
	<!-- /.sidebar -->
	<a href="#" class="sidebar-toggle b-l" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
</aside>
