Dear {{ $user->name }},<br><br>

Your login credentials are changed:<br><br>

Username: {{ $user->email }}<br>
password: {{ $password }}<br><br>

You can login on <a href="{{ url('/userlogin') }}">{{ str_replace("http://", "", url('/userlogin')) }}</a>.<br><br>

Best Regards,<br>
Zodocs
