<div id="tab_3" class="tab-pane " >
                                
                                <div class="content-head">
                                                <h4>School / College & University education <small>(from age 11, starting with the most recent)</small> </h4>
                                                <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                                        </div>
                                        {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'school-add-form']) !!}
               
                        <div class="pane">
                            <div class="mgv-wrap">
								<h4 class="sub_title">GCSE / 10th Board</h4>
                                <div class="row">
									<div class=" col s6 ">
										<div class="input-field">
											<input id="institute_name"  name="institute_name" type="text" class="validate">
											<label for="institute_name">Name of Institution</label>
										</div>
									</div>
									<div class="col s6">
										<div class="input-field">
											<select name="country_sslc" id="country_sslc" class="searchSelect">
												<option value="">Select</option>
												<?php
												foreach($countries as $countriesData) {
													echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
												}
												?>
											</select>			
											<label for="country" style="margin-top: -40px;font-size: 13px;">Country</label>
										</div>
									</div>
                                </div>

                                <div class="row">
                                	<div class="col s6">
                                            <div class="input-field ">
                                                <?php
						$current_year = date('Y');
						$earliest_year = 1990; ?>
						<select name="start_year" id="start_year" class="searchSelect">
                                                <option value="">Select</option>
						<?php
						foreach(range(date('Y'), $earliest_year) as $x) {
						echo '<option value="'.$x.'"'.($x === $current_year ? ' selected="selected"' : '').'>'.$x.'</option>';
						}
						?>
						</select>
						<label for="start_year" style="margin-top: -40px;font-size: 13px;">Start Year</label>
					    </div>
                                        </div>

                                        <div class="col s6">
                                            <div class="input-field ">
                                                <?php
						$current_year = date('Y');
						$earliest_year = 1990; ?>
						<select name="end_year" id="end_year" class="searchSelect">
                                                <option value="">Select</option>
						<?php
						foreach(range(date('Y'), $earliest_year) as $x) {
						echo '<option value="'.$x.'"'.($x === $current_year ? ' selected="selected"' : '').'>'.$x.'</option>';
						}
						?>
						</select>
						<label for="end_year" style="margin-top: -40px;font-size: 13px;">Completion Year</label>
					    </div>
                                        </div>
                                </div>
                                <div class="row">
                                        <div class="col s6">
                                        	<label for="board_name" style="margin-top: -40px;font-size: 13px;">Syllabus</label>
                                                <select name="board_name" id="board_name" class="searchSelect">
                                                    <option value="">Select</option>
							<?php
							foreach($syllabus as $syllabusData) {
								echo '<option value="'.$syllabusData->name.'">'.$syllabusData->name.'</option>';
							}
							?>
						</select>
                                        </div>
                                        <div class="col s6">
                                                <div class="input-field ">
                                                        <input id="grade" name="grade" type="text" class="validate">
                                                        <label for="grade">Percentage / Grade Obtained</label>

                                                </div>
                                        </div>	
                                </div>
                                <div class="row">
									<div class=" col s6 ">
										<div class="input-field">
											<select name="mode_study_school" id="mode_study_school" class="formSelect">
												<option value="">Select</option>
												<option value="Full-time">Full time</option>
												<option value="Part-time">Part time</option>
												<option value="Distance-learning">Distance Learning</option>
											</select>	
											<label for="mode_study_school">Mode of study</label>
										</div>
									</div>
									<div class=" col s6 ">
										<div class="input-field">
											<input id="qualification"  name="qualification" type="text" class="validate">										
											<label for="qualification">Qualification eg.BA(Hons)</label>
										</div>
									</div>
                                </div>								
                                <div class="row">							
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="medium_of_instr" name="medium_of_instr" type="text" class="validate">
                                                                        <label for="language">Medium of Instruction</label>

                                                                </div>
                                                </div>
                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="eng_score" name="eng_score" type="text" class="validate">
                                                                                <label for="eng_score">Marks Obtained in English</label>

                                                                        </div>
                                                </div>
				</div>			
                                <div class="row">
                                        <div class="col s6">
                                                <label>Upload Documents</label>
                                                <div class="file-field input-field">
                                                                <div class="btn">
                                                                        <span>Upload</span>
                                                                        <input type="file" multiple > <i class="fa fa-cloud-upload"></i>
                                                                </div>
                                                                <div class="file-path-wrapper">
                                                                        <input class="file-path validate" name="document" type="text" placeholder = "Upload file" >
                                                                </div>
                                                </div>
												<div class="uploaded-file" id="document" data-type="file"></div>	
                                        </div>
								</div>
                        </div>
						<div class="mgv-wrap">

					
								<h4 class="sub_title">A level / 12th Board</h4>
                                <div class="row">
									<div class=" col s6 ">
										<div class="input-field">
											<input id="hse_institute_name"  name="hse_institute_name" type="text" class="validate">
											<label for="hse_institute_name">Name of Institution</label>
										</div>
									</div>
									<div class=" col s6 ">
										<div class="input-field">
											<label for="hse_country" style="margin-top: -40px;font-size: 13px;">Country</label>
											<select name="hse_country" id="hse_country" class="searchSelect">
												<option value="">Select</option>
												<?php
												foreach($countries as $countriesData) {
													echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
												}
												?>		
											</select>
										</div>
									</div>
                                </div>

                                <div class="row">
                                	<div class="col s6">
                                            <div class="input-field ">
                                                <?php
						$current_year = date('Y');
						$earliest_year = 1990; ?>
						<select name="hse_start_year" id="hse_start_year" class="searchSelect">
                                                <option value="">Select</option>
						<?php
						foreach(range(date('Y'), $earliest_year) as $x) {
						echo '<option value="'.$x.'"'.($x === $current_year ? ' selected="selected"' : '').'>'.$x.'</option>';
						}
						?>
						</select>
						<label for="hse_start_year" style="margin-top: -40px;font-size: 13px;">Start Year</label>
					    </div>
                                        </div>

                                        <div class="col s6">
                                            <div class="input-field ">
                                                <?php
						$current_year = date('Y');
						$earliest_year = 1990; ?>
						<select name="hse_end_year" id="hse_end_year" class="searchSelect">
                                                <option value="">Select</option>
						<?php
						foreach(range(date('Y'), $earliest_year) as $x) {
						echo '<option value="'.$x.'"'.($x === $current_year ? ' selected="selected"' : '').'>'.$x.'</option>';
						}
						?>
						</select>
						<label for="hse_end_year" style="margin-top: -40px;font-size: 13px;">Completion Year</label>
					    </div>
                                        </div>
                                </div>
                                <div class="row">
					<div class=" col s6 ">
						<div class="input-field">
							<select name="stream" id="stream" class="searchSelect">
                                                    		<option value="">Select</option>
								<?php
								foreach($stream as $streamData) {
								echo '<option value="'.$streamData->stream_name.'">'.$streamData->stream_name.'</option>';
								}
								?>
							</select>
							<label for="stream" style="margin-top: -40px;font-size: 13px;">Stream</label>
						</div>
					</div>
					<div class=" col s6 ">
						<div class="input-field">
							<select name="hse_board_name" id="hse_board_name" class="searchSelect">
								<option value="">Select</option>
								<?php
								foreach($syllabus as $syllabusData) {
								echo '<option value="'.$syllabusData->name.'">'.$syllabusData->name.'</option>';
								}
								?>
							</select>											
							<label for="hse_board_name" style="margin-top: -40px;font-size: 13px;">Syllabus</label>
						</div>
					</div>
                                </div>
                                <div class="row">
					<div class=" col s6 ">
						<div class="input-field">
							<select name="hse_mode_study" id="hse_mode_study" class="formSelect">
								<option value="">Select</option>
								<option value="Full-time">Full time</option>
								<option value="Part-time">Part time</option>
								<option value="Distance-learning">Distance Learning</option>
							</select>	
							<label for="hse_mode_study">Mode of study</label>
						</div>
					</div>
									<div class=" col s6 ">
										<div class="input-field">
											<input id="hse_qualification"  name="hse_qualification" type="text" class="validate">										
											<label for="hse_qualification">Qualification eg.BA(Hons)</label>
										</div>
									</div>
                                </div>								
                                <div class="row">
                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="hse_grade" name="hse_grade" type="text" class="validate">
                                                                                <label for="sslcgrade">Percentage / Grade Obtained</label>

                                                                        </div>
                                                </div>								
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="hse_medium_of_instr" name="hse_medium_of_instr" type="text" class="validate">
                                                                        <label for="hse_language">Medium of Instruction</label>

                                                                </div>
                                                </div>
								</div>
	
								<div class="row">
                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="hse_eng_score" name="hse_eng_score" type="text" class="validate">
                                                                                <label for="hse_eng_score">Marks Obtained in English</label>

                                                                        </div>
                                                </div>


                                </div>
                                <div class="row">
                                        <div class="col s6">
                                                <label>Upload Documents</label>
                                                <div class="file-field input-field">
                                                                <div class="btn">
                                                                        <span>Upload</span>
                                                                        <input type="file" multiple > <i class="fa fa-cloud-upload"></i>
                                                                </div>
                                                                <div class="file-path-wrapper">
                                                                        <input class="file-path validate" name="hse_document" type="text" placeholder = "Upload file" >
                                                                </div>
                                                </div>
												<div class="uploaded-file" id="hse_document" data-type="file"></div>	
                                        </div>
                                </div>								
			</div>





							  
							
			<!-- </div> -->



			<!-- New Code -->
			<div id="school-wrapper">
						
				<div class="mgv-wrap">
				<h4 class="sub_title">Additional Qualification</h4>
                                <div class="row">
					<div class=" col s6 ">
						<div class="input-field">
							<select name="UG_board_name" id="UG_board_name" class="searchSelect">
                                                		<option value="">Select</option>
								<?php
								foreach($courses as $courseData) {
								echo '<option value="'.$courseData->course_name.'">'.$courseData->course_name.'</option>';
								}
								?>
							</select>
							<label for="UG_board_name" style="margin-top: -40px;font-size: 13px;">Course Name</label>
						</div>
					</div>
					<div class=" col s6 ">
						<div class="input-field">
							<select name="ug_university" id="ug_university" class="searchSelect">
								<option value="">Select</option>
   								<?php
								foreach($university as $universityData) {
								echo '<option value="'.$universityData->university.'">'.$universityData->university.'</option>';
								}
								?>
							</select>				
							<label for="ug_university" style="margin-top: -40px;font-size: 13px;">University Name</label>
						</div>
					</div>
                                </div>
                                <div class="row">
                                    <div class="col s6">
                                        <div class="input-field ">
					    <input id="UG_grade" name="UG_grade" type="number" class="validate" step=".01" min=1>
                                            <label for="sslcgrade">University Percentage(%)</label>
                                        </div>
                                        </div>
                                        <div class="col s6">
                                            	<div class="input-field ">
                                                <select name="UG_end_year" id="UG_end_year" class="searchSelect">
                                                	<option value="">Select</option>
								<?php
								foreach(range(date('Y'), $earliest_year) as $x) {
								echo '<option value="'.$x.'"'.($x === $current_year ? ' selected="selected"' : '').'>'.$x.'</option>';
								}
								?>
						</select>
						<label for="UG_end_year" style="margin-top: -40px;font-size: 13px;">Pass Out Year</label>
                                            	</div>
                                        </div>
                                </div>
                                <div class="row">
					<div class=" col s6 ">
						<div class="input-field">
							<input id="UG_backlogs" name="UG_backlogs" type="number" class="validate">
							<label for="UG_backlogs">Backlogs</label>
						</div>
					</div>
					<div class=" col s6 ">
						<div class="input-field">
							<select name="UG_mode_study" id="UG_mode_study" class="formSelect">
                                                		<option value="">Select</option>
								<option value="Full-time">Full time</option>
								<option value="Distance">Distance</option>
							</select>	
							<label for="UG_mode_study">Mode of study</label>
						</div>
					</div>
                                </div>  
                                <div class="row">
                                        <div class="col s6">
                                                <label>Upload Documents</label>
                                                <div class="file-field input-field">
                                                       <div class="btn">
                                                                <span>Upload</span>
                                                                <input type="file" multiple > <i class="fa fa-cloud-upload"></i>
                                                        </div>
                                                        <div class="file-path-wrapper">
                                                                <input class="file-path validate" name="UG_document" type="text" placeholder = "Upload file" >
                                                        </div>
                                                </div>
						<div class="uploaded-file" id="UG_document" data-type="file"></div>	
                                        </div>
                                </div>  	
			</div>
			<div class="clearfix"></div>	
			</div>
			<div class="btn addshbtn"> <span class="glyphicon glyphicon-plus"></span></div>
			</div>					
			<input type="hidden" value="<?php echo $wrp_count_academic; ?>" class="wrapper-count">
			<input type="hidden" value="school" name="formtype" class="formtype">
			<!-- End New Code -->

                        <div class="controls">
                                        <a class="mgbtn2 back_button">Back</a>
                                        <input id="form_name" type="hidden" class="validate" name="type_of" value="school">
                                        <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                        {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
                </div>

                