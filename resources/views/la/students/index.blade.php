@extends("la.layouts.app")

@section("contentheader_title", "Students")
@section("contentheader_description", "Students listing")
@section("section", "Students")
@section("sub_section", "Listing")
@section("htmlheader_title", "Students Listing")

@section("headerElems")
@la_access("Employees", "create")
	<a href="<?php echo url(config("laraadmin.adminRoute") . '/students/0/edit'); ?>" class="btn btn-success btn-sm pull-right addbtn" > <i class="icon-user-follow"></i> Add Student</a>
@endla_access
@endsection

@section("main-content")



@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('status_success'))
	<div class="alert alert-success">{{ session('status_success') }}</div>
@endif





<!--  Agent filter 11111 -->   
@if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER","CONSULTANTS","MAIN_AGENTS"]))
<div class="reportfilter">
	<div class="row">
		<div class="col-md-9">
		<div class="clearfix">
	@if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER","CONSULTANTS"]))
	<div class="col-md-4">	
		<div class="form-group">
			<!--<select name="agent_id" id="agent_id" class="form-control"> -->
			<select name="agent_id" id="agent_id" class="searchSelect">
				<option value="">Select Agent</option>
				<?php
					foreach($agents as $agentsData) {
						echo '<option id="sale_agnt" value="'.$agentsData->id.'">'.$agentsData->name.'</option>';
					}
				?>
			</select>
		</div>
	</div>
	@else
		<input type="hidden" name="agent_id" id="agent_id">
	@endif
	<div class="col-md-4">	
		<div class="form-group">
			<select name="course_country" id="course_country" class="searchSelect">
				<option value="">Select Country</option>
				<?php
					foreach($countries as $countriesData) {
						echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
					}
				?>
			</select>
		</div>
	</div>

	<?php 
		$curr_yr = date('Y');
		$curr_yr2 = date('Y', strtotime('-2 year'));
		$nxt_yr = date('Y', strtotime('+1 year'));
		$nxt_yr2 = date('Y', strtotime('+2 year'));
		$curr_mon = date('m');
    	$months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
	?>
	<div class="col-md-4">	
		<div class="form-group">
			<select class="searchSelect" name="entry_date" id="entry_date">
				<option value="">Select Intake</option>
				<?php
					for($i=$curr_yr2; $i<=$nxt_yr2;$i++) {
						foreach ($months as $key => $value) { ?>
							<option value="<?php echo $value.' '.$i; ?>"> <?php echo $value.' '.$i; ?></option>
						<?php	}
					}
				?>
			</select>
		</div>
	</div>

	@if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER","CONSULTANTS"]))
	<div class="col-md-4">	
		<div class="form-group">
			<select class="searchSelect" name="course_university_new" id="course_university_new">
				<option value="">Select University</option>
				<?php
				foreach($university as $universityData) {
					echo '<option value="'.$universityData->university.'">'.$universityData->university.'</option>';
				}
				?>
			</select>
		</div>
	</div>
	@endif

	@if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER","CONSULTANTS"]))
	<div class="col-md-4">	
		<div class="form-group">
			<select class="searchSelect" name="lead_from" id="lead_from"> 
				<option value="">Select Lead Type</option>
                <option value="Agents"> Agents </option>
                <option value="Facebook"> Facebook </option>
                <option value="Instagram"> Instagram </option>
              	<option value="Google"> Google </option>
                <option value="Email"> Email </option>
                <option value="Help Desk"> Help Desk </option>
                <option value="Reference"> Reference </option>
                <option value="Walk In"> Walk In </option>
                <option value="Personal"> Personal </option>
			</select>
		</div>
	</div>
	@endif

	<div class="col-md-4">	
		<div class="form-group">
			<select class="searchSelect" name="std_status" id="std_status">
				<option value="">Select Status</option>
				<?php
				foreach($stdstatus as $stdstatusData) {
					echo '<option value="'.$stdstatusData->student_status.'">'.$stdstatusData->student_status.'</option>';
				}
				?>
			</select>
		</div>
	</div>

	@if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER"]))
	<div class="col-md-4">	
		<div class="form-group">
			<select name="std_counselor" id="std_counselor" class="searchSelect">
				<option value="">Select Staff</option>
				<?php
					foreach($stdcounselor as $stdcounselorData) {
						echo '<option id="stdcouslr" value="'.$stdcounselorData->id.'">'.$stdcounselorData->name.'</option>';
					}
				?>
			</select>
		</div>
	</div>
	@else
		<input type="hidden" name="std_counselor" id="std_counselor">
	@endif

	@if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER","CONSULTANTS"]))
	<div class="col-md-4">	
		<div class="form-group">
			<select name="std_docstaff" id="std_docstaff" class="searchSelect">
				<option value="">Select University User</option>
				<?php
					foreach($stddocumentation as $stddocumentationData) {
						echo '<option id="stddocs" value="'.$stddocumentationData->id.'">'.$stddocumentationData->name.'</option>';
					}
				?>
			</select>
		</div>
	</div>
	@else
		<input type="hidden" name="std_docstaff" id="std_docstaff">
	@endif

</div>
		</div>
	<div class="col-md-3">
		<div class="col-md-8 pl0"><div class="form-group">
			<button class="btn btn-success btn-sm pull-right searchbtn"><span class="fa fa-search"></span> Search</button>
		</div>
    	</div>
    	<div class="col-md-4 pl0"><div class="form-group">
			<button class="btn btn-success btn-sm pull-right clearbtn"><span class="fa fa-times"></span> Clear</button>
		</div>
		</div>
	</div>
	</div>
</div>
<br>
@endif
<!--  Agent filter ends -->





<!-- <div class="box box-success">
	<div class="box-body"> -->
		<table id="mx-sudents-lists" class="table table-hover mg-table mgt2 display nowrap">
			<thead>
			<tr class="success">
				<th>No:</th>
				<th>Name</th>
				<th>Email</th>
				<th>Date</th>
				<th>Lead Source</th>
				<!--<th>Date-of-birth</th>-->
				<th>Status</th>
				@if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER"]))
					<th>Assignee</th>
				@endif
				@if($show_actions)
					<th>Actions</th>
				@endif
			</tr>
			</thead>
			<tbody></tbody>
		</table>
	<!-- </div>
</div> -->

<!-- common modals -->
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Assign a consultant</h4>
			</div>
			<form action="{{ url(config('laraadmin.adminRoute') . '/students_assign') }}" id="assign-stud-form" method="post">
			{{ csrf_field() }}
			<div class="modal-body">
				<div class="box-body">					
					<?php
					if(count($consultants) > 0) {
					?>
						<input type="hidden" value="" name="assign_user_id" id="assign_user_id">
						<input type="hidden" value="" name="assign_email" id="assign_email">

						
						<input type="hidden" value="" name="stud_name" id="stud_name">
						<input type="hidden" value="" name="staff_name" id="staff_name">
						<input type="hidden" value="" name="staff_phone" id="staff_phone">
						<input type="hidden" value="" name="stud_notify" id="stud_notify">

						<div class="form-group">
							<label for="assigned_to">Select consultant * :</label>
							<select class="form-control stud-assign-select" name="assigned_to" required>
								<option value="">Select consultant</option>
								<?php
								foreach($consultants as $cons) {
								?>
									<option value="<?php echo $cons->id; ?>" data-name="<?php echo $cons->name; ?>" data-phone="<?php echo $cons->mobile; ?>"> <?php echo $cons->name; ?></option>
								<?php
								}
								?>
							</select>
						</div>
					<?php
					} else {
						echo '<h3>No consultants to assign</h3>';
					}
					?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			</form>
		</div>
	</div>
</div>

<!--  Change student status -->
<div class="modal fade" id="AddModal-studentStatus" role="dialog" aria-labelledby="myModalLabel-studentStatus">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel-studentStatus">Assign Student Application Status</h4>
			</div>
			<form action="{{ url(config('laraadmin.adminRoute') . '/students_status') }}" id="assign-stud-status" method="post">
			{{ csrf_field() }}
			<div class="modal-body">
				<div class="box-body">					
					<input type="hidden" value="" name="student_user_id" id="student_user_id">
					<input type="hidden" value="" name="student_user_email" id="student_user_email">
					<input type="hidden" value="" name="student_user_name" id="student_user_name">
					<input type="hidden" value="" name="student_user_notify" id="student_user_notify">
					<?php $studentstatus = DB::table('studentstatus')->select('student_status')->where('status', 1)->get(); ?>
					<div class="form-group">
						<label for="assigned_to">Select status * :</label>
						<select class="form-control" name="user_status_assigned" required>
							<!-- <option value="">Select status</option>
							<option value="APPROVE">Approve</option>
							<option value="REJECT">Reject</option>
							<option value="Processing">Processing</option>
							<option value="Passport processing">Passport processing</option>
							<option value="IELTS processing">IELTS processing</option> -->
							<option value="">Select status</option>
							<?php
								foreach($studentstatus as $studentstatusData) {
									echo '<option value="'.$studentstatusData->student_status.'">'.$studentstatusData->student_status.'</option>';
								}
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			</form>
		</div>
	</div>
</div>



<!--  Change next follow up -->
<div class="modal fade" id="AddModal-studentFollowup" role="dialog" aria-labelledby="myModalLabel-studentFollowup">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel-studentFollowup">Assign Student Next Follow Up</h4>
			</div>
			<form action="{{ url(config('laraadmin.adminRoute') . '/students_followup') }}" id="assign-stud-followup" method="post">
			{{ csrf_field() }}
			<div class="modal-body">
				<div class="box-body">					
					<input type="hidden" value="" name="st_user_id" id="st_user_id">
					<input type="hidden" value="" name="st_user_email" id="st_user_email">
					<input type="hidden" value="" name="st_user_name" id="st_user_name">
					<input type="hidden" value="" name="st_user_notify" id="st_user_notify">
					<?php //$studentFollowDate = DB::table('data_follow_up')->select('follow_date')->where('user_id', 188)->get(); ?>
					<div class="form-group">
						<label for="assigned_to">Date for next follow up * :</label> 
						<!--<input id="follow_date" name="follow_date" type="date" class="datepicker" required> -->
						<input id="follow_date" name="follow_date" type="date" class="" required>
					</div>
					<div class="form-group">
						<label for="assigned_to">Time for next follow up * :</label> 
						<!-- <input id="follow_time" name="follow_time" type="text" class="timepicker" required> -->
						<input id="follow_time" name="follow_time" type="time" class="" required>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			</form>
		</div>
	</div>
</div>



<div class="modal fade" id="universityUserModal" role="dialog" aria-labelledby="universityModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="universityModalLabel">Assign a University user</h4>
			</div>
			<form action="{{ url(config('laraadmin.adminRoute') . '/students_unv_assign') }}" id="assign-univerUser-form" method="post">
			{{ csrf_field() }}
			<div class="modal-body">
				<div class="box-body">					
					<?php
					if(count($universityAgents) > 0) {
					?>
						<input type="hidden" value="" name="assign_user_id" id="assign_unv_user_id">
						<input type="hidden" value="" name="assign_email" id="assign_unv_email">


						<input type="hidden" value="" name="stud_namee" id="stud_namee">
						<input type="hidden" value="" name="staff_namee" id="staff_namee">
						<input type="hidden" value="" name="staff_phonee" id="staff_phonee">
						<input type="hidden" value="" name="stud_notifyy" id="stud_notifyy">

						<div class="form-group">
							<label for="assigned_to">Select University user * :</label>
							<select class="form-control stud-assign-unv-select" name="assigned_to" required>
								<option value="">Select</option>
								<?php
								foreach($universityAgents as $unv) {
								?>
									<option value="<?php echo $unv->id; ?>"> <?php echo $unv->name; ?></option>
								<?php
								}
								?>
							</select>
						</div>
					<?php
					} else {
						echo '<h3>No University user to assign</h3>';
					}
					?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			</form>
		</div>
	</div>
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>


<!-- Search -->
<script src="{{ asset('la-assets/stdentsform/js/main.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<!-- Search -->


<script>
var bsurl = $('body').attr("bsurl");
var fm_dropzone_main = null;
var cntFiles = null;
$(function () {
	$("#mx-sudents-lists").DataTable({
		processing: true,
        serverSide: false,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/students_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		columnDefs: [ { orderable: false, targets: [-1] }],
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	
	$("#assign-stud-form").validate();
	$("#assign-univerUser-form").validate();

	$('body').off('click', '.stud-assign');
	$('body').on('click', '.stud-assign', function() {
		$("#assign_user_id").val($(this).attr("data-user"));
		$("#assign_email").val($(this).attr("data-email"));

		$("#stud_name").val($(this).attr("data-student"));
		$("#stud_notify").val($(this).attr("data-notify"));

	});
	$('body').off('click', '.stud-assign-university');
	$('body').on('click', '.stud-assign-university', function() {
		$("#assign_unv_user_id").val($(this).attr("data-user"));
		$("#assign_unv_email").val($(this).attr("data-email"));

		$("#stud_namee").val($(this).attr("data-student"));
		$("#stud_notifyy").val($(this).attr("data-notify"));
	});
	// change status of student
	/*$('#mx-sudents-lists_wrapper').on('click', '.apprbtn', function() {
		
		$("#student_user_id").val($(this).attr('data-id'));
		$("#student_user_email").val($(this).attr('data-email'));
		$("#student_user_name").val($(this).attr('data-name'));
		$("#AddModal-studentStatus").modal();
	});*/

	$('body').off('click', '.apprbtn');
	$('body').on('click', '.apprbtn', function() {
		$("#myModalLabel-studentStatus").text('Change status');
		$("#student_user_id").val($(this).attr('data-id'));
		$("#student_user_email").val($(this).attr('data-email'));
		$("#student_user_name").val($(this).attr('data-name'));
		$("#student_user_notify").val($(this).attr('data-notify'));
	});




	$('body').off('click', '.followupbtn');
	$('body').on('click', '.followupbtn', function() {
		$("#myModalLabel-studentFollowup").text('Change next follow up');
		$("#st_user_id").val($(this).attr('data-id'));
		$("#st_user_email").val($(this).attr('data-email'));
		$("#st_user_name").val($(this).attr('data-name'));
		$("#st_user_notify").val($(this).attr('data-notify'));
	});
	// end change next follow up of student

	$('.timepicker').datetimepicker({
        format: 'hh:mm A',
    });
    $('#input_date').datetimepicker({
		format: 'DD/MM/YYYY'
	});
	follow_date.min = new Date().toISOString().split("T")[0]; 


	$('body').off('change', '.stud-assign-select');
	$('body').on('change', '.stud-assign-select', function() {
		
		$("#staff_name").val($(this).find('option:selected').attr('data-name'));
		$("#staff_phone").val($(this).find('option:selected').attr('data-phone'));
	});


	$('body').off('change', '.stud-assign-unv-select');
	$('body').on('change', '.stud-assign-unv-select', function() {
		
		$("#staff_namee").val($(this).find('option:selected').attr('data-name'));
		$("#staff_phonee").val($(this).find('option:selected').attr('data-phone'));
	});




	/* Search by Agent, Country, Intake */
	$('body').on('click', '.searchbtn', function() { 
		$("#mx-sudents-lists").dataTable().fnDestroy();  
		var agent_id = document.getElementById("agent_id").value;
		var course_country = document.getElementById("course_country").value;
		var entry_date = document.getElementById("entry_date").value;
		var course_university_new = document.getElementById("course_university_new").value;
		var leadFrom = $.trim($("#lead_from").val());
		var std_status = document.getElementById("std_status").value;
		var std_counselor = document.getElementById("std_counselor").value;
		var std_docstaff = document.getElementById("std_docstaff").value;
		var dt = "#mx-sudents-lists";
		$(dt).DataTable({
		processing: true,
        serverSide: false,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "ajax": {
        'type': 'GET',
        'url': "{{ url(config('laraadmin.adminRoute') . '/students_dt_ajax') }}",
        'data': {
			   "agent_id"  : agent_id,
			   "course_country"	: course_country,
			   "entry_date"	: entry_date,
			   "course_university_new" : course_university_new,
			   "leadFrom": leadFrom,
			   "std_status": std_status,
			   "std_counselor": std_counselor,
			   "std_docstaff": std_docstaff
			}
        },
        "aaSorting": [],
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		columnDefs: [ { orderable: false, targets: [-1] }],
		columnDefs: [ { orderable: false, targets: [-1] }]
		});
	});

	$('body').on('click', '.clearbtn', function() {
      window.location="<?php echo URL::to('admin/students'); ?>";
    });

	$('body').on('click', '.clearbtn1', function() {
		$("#agent_id").val('');
		$("#course_country").val('');
		$("#entry_date").val('');
		$("#course_university_new").val('');
		$("#std_status").val('');
		$("#std_counselor").val('');
		$("#std_docstaff").val('');
		
		$("#mx-sudents-lists").dataTable().fnDestroy();  
		var dt = "#mx-sudents-lists";
		$(dt).DataTable({
		processing: true,
        serverSide: false,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "ajax": {
        'type': 'GET',
        'url': "{{ url(config('laraadmin.adminRoute') . '/students_dt_ajax') }}",
        },
        "aaSorting": [],
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		columnDefs: [ { orderable: false, targets: [-1] }],
		columnDefs: [ { orderable: false, targets: [-1] }]
		});
	});
	/* End Search by Agent */



});
function loadUploadedFiles() {
    // load folder files
    $.ajax({
        dataType: 'json',
        url: "{{ url(config('laraadmin.adminRoute') . '/uploaded_files') }}",
        success: function ( json ) {
            console.log("loadUploadedFiles",json);
            cntFiles = json.uploads;
            $("ul.files_container").empty();
            if(cntFiles.length) {
                for (var index = 0; index < cntFiles.length; index++) {
                    var element = cntFiles[index];
                    var li = formatFile(element);
                    $("ul.files_container").append(li);
                }
            } else {
                $("ul.files_container").html("<div class='text-center text-danger' style='margin-top:40px;'>No Files</div>");
            }
        }
    });
}
function formatFile(upload) {
    var image = '';
    if($.inArray(upload.extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
        image = '<img src="'+bsurl+'/files/'+upload.hash+'/'+upload.name+'?s=130">';
    } else {
        switch (upload.extension) {
            case "pdf":
                image = '<i class="fa fa-file-pdf-o"></i>';
                break;
            default:
                image = '<i class="fa fa-file-text-o"></i>';
                break;
        }
    }
    return '<li><a class="fm_file_sel" data-toggle="tooltip" data-placement="top" title="'+upload.name+'" upload=\''+JSON.stringify(upload)+'\'>'+image+'</a></li>';
}
</script>
@endpush