@extends("la.layouts.app")

@section("contentheader_title", "Courses")
@section("contentheader_description", "Courses listing")
@section("section", "Courses")
@section("sub_section", "Listing")
@section("htmlheader_title", "Courses Listing")

@section("headerElems")
@la_access("Employees", "create")
	<button class="btn btn-success btn-sm pull-right addbtn" data-toggle="modal" id="addCourseModal" data-target="#AddModal"> <i class="icon-plus"></i> Add Courses</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-hover mg-table ">
		<thead>
			<tr class="success">
				<th>Course</th>
				<th>University</th>
				<th>Country</th>
				<th>Status</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
		<?php 
		if(count($courses) > 0) { 
			foreach($courses as $result) {
		?>
			<tr role="row" class="odd">
				<td><a href="<?php echo url(config('laraadmin.adminRoute') . '/courses/viewcourse/'.$result->id) ?>"><?php echo $result->course_name; ?></a></td>
				<td>
					
					<?php if($result->university_logo != '') { ?>
						<a href="<?php echo $result->university_logo; ?>" data-lightbox="example-set"><img src="<?php echo $result->university_logo; ?>" alt="" class="apprbtn" data-url="'.$university_logo.'" width="150px" height="150px" style="padding-bottom:10px; padding-top:10px;" /></a>
					<?php } else {  ?>
						<a href="{{ asset('la-assets/coursefinder/img/coursesCards/no-image.png') }}" data-lightbox="example-set"><img src="{{ asset('la-assets/coursefinder/img/coursesCards/no-image.png') }}" alt="" class="apprbtn" data-url="'.$university_logo.'" width="150px" height="150px" style="padding-bottom:10px; padding-top:10px;" /></a>
					<?php } ?>
					<?php echo $result->university_name; ?>
				</td>
				<td><?php echo $result->country_name; ?></td>
				<td>
					<?php 
					$status = ($result->status == '1') ? "Active" : "Inactive";
					echo $status; 
					?>
				</td>
				<td>
					<a href="#" class="btn btn-warning btn-xs mx-course-logo" data-id="<?php echo $result->id; ?>" data-toggle="tooltip" title="Upload Logo"><i class="fa fa-files-o"></i></a>
					<a href="<?php echo url(config('laraadmin.adminRoute') . '/courses/viewcourse/'.$result->id) ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></a>
					<a href="#" class="btn btn-warning btn-xs mx-course-edit" data-status="<?php echo $result->status; ?>" data-desc="<?php echo $result->course_description; ?>" data-title="<?php echo $result->course_name; ?>" data-id="<?php echo $result->id; ?>" data-university="<?php echo $result->university; ?>"  data-country="<?php echo $result->country; ?>"  data-intake="<?php echo $result->intake; ?>"  data-duration="<?php echo $result->duration; ?>"  data-course_type="<?php echo $result->course_type; ?>"  data-tuition_fees="<?php echo $result->tuition_fees; ?>"  data-test_requirements="<?php echo $result->test_requirements; ?>"  data-entry_requirements="<?php echo $result->entry_requirements; ?>" data-application_fee="<?php echo $result->application_fee; ?>" data-application_deadline="<?php echo $result->application_deadline; ?>" data-remarks="<?php echo $result->remarks; ?>" data-course_description="<?php echo $result->course_description; ?>" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
					<?php
					$output  = Form::open(['route' => [config('laraadmin.adminRoute') . '.courses.destroy', $result->id], 'method' => 'delete', 'style'=>'display:inline','onSubmit'=>"return confirm('Are you sure you wish to delete?');"]);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
					
					echo $output;
					?>
				</td>
			</tr>	
		<?php 
			}
		} else { ?>
			<td>No entry found!!</td>
		<?php 
		}
		?>			
		</tbody>
		</table>
	</div>
</div>

@endsection

@la_access("Employees", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Course Details</h4>
			</div>
			{!! Form::open(['action' => 'LA\CousesController@store', 'id' => 'course-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
					<input type="hidden" value="" id="course_id" name="course_id">
					<div class="form-group">
						<label for="name">Course Name* :</label>
						<input class="form-control" required id="course_name" name="course_name" type="text" value="">
					</div>

					<div class="form-group">
						<label for="name">Course Description :</label>
						<textarea class="form-control" id="course_description" name="course_description" rows="5" value=""></textarea>
					</div>

					<div class="form-group">
						<label for="name">University* :</label>
						<select class="form-control" name="university" id="university" required>
							<option value="">Select</option>
							<?php
							foreach($university as $universityData) {
								echo '<option value="'.$universityData->id.'">'.$universityData->university.'</option>';
							}
							?>
						</select>
					</div>

					<div class="form-group">
						<label for="name">Country* :</label>
						<select name="country" id="country" class="form-control" required>
							<option value="">Select</option>
							<?php
								foreach($countries as $countriesData) {
									echo '<option value="'.$countriesData->id.'">'.$countriesData->country.'</option>';
								}
							?>
						</select>
					</div>

					<?php 
						$curr_yr = date('Y');
						$curr_yr2 = date('Y', strtotime('-2 year'));
						$nxt_yr = date('Y', strtotime('+1 year'));
						$nxt_yr2 = date('Y', strtotime('+2 year'));
						$curr_mon = date('m');
				    	$months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
					?>
		
					<div class="form-group">
						<label for="name">Intake* :</label>
						<select class="form-control" name="intake" id="intake" required>
							<option value="">Select</option>
							<?php
								for($i=$curr_yr; $i<=$nxt_yr2;$i++) {
									foreach ($months as $key => $value) { ?>
										<option value="<?php echo $value.' '.$i; ?>"> <?php echo $value.' '.$i; ?></option>
									<?php	}
								}
							?>
						</select>
					</div>

					<div class="form-group">
						<label for="name">Duration* :</label>
						<input class="form-control" id="duration" name="duration" type="number" value="" min="0.3" style="appearance: textfield;" required>
					</div>

					<div class="form-group">
						<label for="status">Course Type* :</label>
						<select name="course_type" id="course_type" class="form-control" required>
							<option value="UG">UG</option>
							<option value="PG">PG</option>
						</select>
					</div>

					<div class="form-group">
						<label for="name">Yearly Tuition Fees (in Rupees)* :</label>
						<input class="form-control" id="tuition_fees" name="tuition_fees" type="number" value="" min="1" style="appearance: textfield;" required>
					</div>

					<div class="form-group">
						<label for="name">Test Requirements :</label>
						<textarea class="form-control" id="test_requirements" name="test_requirements" rows="5" value=""></textarea>
					</div>

					<div class="form-group">
						<label for="name">Entry Requirements :</label>
						<textarea class="form-control" id="entry_requirements" name="entry_requirements" rows="5" value=""></textarea>
					</div>

					<div class="form-group">
						<label for="name">Application Fee (in Rupees)* :</label>
						<input class="form-control" id="application_fee" name="application_fee" type="number" value="" min="1" style="appearance: textfield;" required>
					</div>

					<div class="form-group">
						<label for="date_birth">Application Deadline* :</label>
							<input class="form-control" id="application_deadline" name="application_deadline" type="date" value="<?= date('Y-m-d'); ?>" required>
					</div>

					<div class="form-group">
						<label for="name">Remarks :</label>
						<textarea class="form-control" data-rule-maxlength="500" id="remarks" name="remarks" rows="5" value=""></textarea>
					</div>

					<div class="form-group">
						<label for="status">Status :</label>
						<select name="status" id="course_status" class="form-control">
							<option value="1">Active</option>
							<option value="0">Inactive</option>
						</select>
					</div>					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success">Submit</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

@endla_access





<div class="modal fade" id="AddModalImg" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">     
        <div class="modal-content">
            <div class="modal-header hd-text">
                Upload Image  <br><div class="file_error_text_img"></div>
            </div>
            <div class="modal-body">       <div class="image-upload-wrap">
                                    
          		<form action="{{ url(config('laraadmin.adminRoute') . '/upload_files_course') }}" id="fm_dropzone_main_img" enctype="multipart/form-data" method="POST">
                    {{ csrf_field() }}
                    <a id="closeDZ1Img"><i class="fa fa-times"></i></a>
                    <input type="hidden" value="" id="coursee_id" name="coursee_id">
                    <div class="dz-message drag-text"><i class="fa fa-cloud-upload"></i><br><h3>Drop files here to upload</h3></div>
                </form>
            </div>            </div>
        </div>
    </div>               
</div>




@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>


<script src="{{ asset('la-assets/plugins/lightbox2/dist/js/lightbox-plus-jquery.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('la-assets/plugins/lightbox2/dist/css/lightbox.min.css') }}">

@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<style> .uploaded-file { width: 150px; height: 150px;}</style>
<script>
var fm_dropzone_main_img = null;
$(function () {		 

	$(".mg-table").DataTable({
		processing: true
	});
	$("#course-add-form").validate({
	});
	$('body').off('click', '.mx-course-edit');
	$('body').on('click', '.mx-course-edit', function() {
		
		var course_id = $(this).attr('data-id');
		var title = $(this).attr('data-title');
		var desc = $(this).attr('data-desc');
		var status = $(this).attr('data-status');
		$("#course_id").val(course_id);
		$("#course_name").val(title);
		$("#course_desc").val(desc);
		$("#course_status").val(status);
		$("#course_description").val($(this).attr('data-course_description'));
		$("#university").val($(this).attr('data-university'));
		$("#country").val($(this).attr('data-country'));
		$("#intake").val($(this).attr('data-intake'));
		$("#duration").val($(this).attr('data-duration'));
		$("#course_type").val($(this).attr('data-course_type'));
		$("#tuition_fees").val($(this).attr('data-tuition_fees'));
		$("#test_requirements").val($(this).attr('data-test_requirements'));
		$("#entry_requirements").val($(this).attr('data-entry_requirements'));
		$("#application_fee").val($(this).attr('data-application_fee'));
		$("#application_deadline").val($(this).attr('data-application_deadline'));
		$("#remarks").val($(this).attr('data-remarks'));

		$('#AddModal').modal('show');
	});
	$("#AddModal").on('hide.bs.modal', function(){
		$('#AddModal input,#AddModal textarea').val('');
		$('#AddModal select').val(1);
	});



	$('body').off('click', '.mx-course-logo');
	$('body').on('click', '.mx-course-logo', function() {
		$('#AddModalImg').modal('show');
		var coursee_id = $(this).attr('data-id');
		$("#coursee_id").val(coursee_id); 
	});

	// Image Upload
    fm_dropzone_main_img = new Dropzone("#fm_dropzone_main_img", {
        maxFilesize: 2,
        
        accept: function(file, done) {
            console.log(file);
            if (file.type != "image/jpeg" && file.type != "image/jpg" && file.type != "image/png" && file.type != "image/gif" && file.type != "image/bmp") {
                $(".file_error_text_img").html("<p class='file_err'>Error! Files of this type are not accepted</p>");
                this.removeFile(file);
            }
            else {
                done();
            }
        },

        init: function() {
                this.on('addedfile', function(file) {
                if (this.files.length > 1) {
                    alert("Only 1 file allowed")
                this.removeFile(this.files[0]);
                }
                });

            this.on("complete", function(file) {     $('#AddModalImg').modal('close');
                this.removeFile(file);  $(".file_error_text_img").html("");
            });
            this.on("success", function(file) {
                $('#AddModalImg').modal('hide');   
                $(".image-upload-wrap").hide();
                window.location="{{URL::to('admin/courses')}}";
            });
        }
    }); 
// End Image Upload

});
</script>
@endpush
