@extends('la.layouts.auth')

@section('htmlheader_title')
    Password reset
@endsection

@section('content')

<?php
// get token of reset password
$url  = $_SERVER["REQUEST_URI"];
$path = explode("/", $url); 
$token = end($path);
?>

    <body class="login-page mg-login">
    <div class="login-banner">
        <span>
            <h3>
                STUDY. <br>
                WORK. <br>
                LIVE.
            </h3>
        </span>
    </div>
    <div class="login-box">
        <div class="login-logo">
        <a href="{{ url('/home') }}"><img src="{{asset('la-assets/img/Zodocs_logo.png')}}" alt=""></a>
        </div><!-- /.login-logo -->

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="login-box-body">
            <p class="login-box-msg">Reset Password</p>
            <form action="{{ url('/password/reset') }}" method="post">
				{{ csrf_field() }}
				<input type="hidden" name="token" value="<?php echo $token; ?>"/>
                <div class="form-group has-feedback">
                    <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}"/>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Password" name="password"/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Password" name="password_confirmation"/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <div class="row">
                    <div class="col-xs-8">
                        <button type="submit" class="btn btn-primary btn-block btn-flat loginbtn">RESET PASSWORD</button>
                    </div><!-- /.col -->
					<div class="col-md-12 rstlink">
						Already Registered ? <a class="" href="{{ url('/login') }}">Login Now</a>
					</div>
                </div>
            </form>

            <!--<a href="{{ url('/register') }}" class="text-center">Register a new membership</a>-->

        </div><!-- /.login-box-body -->

    </div><!-- /.login-box -->

    @include('la.layouts.partials.scripts_auth')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
    </body>

@endsection