<div id="tab_7" class="tab-pane " >
        
<div class="content-head">
                                                <h4>Section 7 </h4>
                                                <!-- <div class="id">Nurse ID : <b>#12675</b></div> -->
                                </div>
                                {!! Form::open(['action' => 'LA\NursesController@store', 'id' => '']) !!}
               
                                <div class="pane">
                                        <div class="row">
											<div class="col s6">
												<div class="input-field">											
													<select name="nmc_appl_status" id="nmc_appl_status">
														<option value="Not-Applicable">Not Applicable</option>
														<option value="SPONSORSHIP">SPONSORSHIP</option>
														<option value="PERMANENT-RESIDENCE">PERMANENT RESIDENCE</option>
														<option value="CITIZENSHIP">CITIZENSHIP</option>
														<option value="STUDENT-VISA">STUDENT VISA</option>
														<option value="WAITING-FOR-RCOS">WAITING FOR RCOS</option>
														<option value="OVERSEAS-PRE-REGISTRATION">OVERSEAS-PRE REGISTRATION</option>
													</select>											
													<label for="cbt_attempt_date">Visa Category </label>
												</div>
											</div>
                                        </div>
																	
                                        <div class="row">
                                                <div class="col s6">
                                                        <label>Application Form (NHS / Nursing Home) File Upload  </label>
                                                        <div class="file-field input-field">
																<div class="file-field input-field">
																				<div class="btn">
																						<span>Upload</span>
																						<input type="file" name="appl_form" multiple> <i class="fa fa-cloud-upload"></i> 
																				</div>
																		<div class="file-path-wrapper">
																				<input class="file-path validate">

																				<div>
																				</div>
																
																		</div>
																</div>

																<div class="uploaded-file" id="appl_form" data-type="file"></div>																
                                                        </div>
                                                </div>
                                                <div class="col s6">
                                                        <label>References File Upload   </label>
                                                        <div class="file-field input-field">
																<div class="file-field input-field">
																				<div class="btn">
																						<span>Upload</span>
																						<input type="file" name="refer_file" multiple> <i class="fa fa-cloud-upload"></i> 
																				</div>
																		<div class="file-path-wrapper">
																				<input class="file-path validate">

																				<div>
																				</div>
																
																		</div>
																</div>

																<div class="uploaded-file" id="refer_file" data-type="file"></div>																
                                                        </div>
                                                </div>
                                        </div>	
										
                                        <div class="row">
											<div class="col s6">
												<div class="input-field">
													<select name="pers_status" id="pers_status">
														<option value="Not-Applicable">Not Applicable</option>
														<option value="Applicable">Applicable</option>
													</select>												
													<label for="pers_status">Personal Statement Status  </label>
												</div>
											</div>
                                        </div>
										
										<div id="personal-stat-wrapper" style="display:none;">
											<div class="row">
												<div class="col s12">
													<div class="input-field">
														<input name="pers_stat_text" id="pers_stat_text" type="text">
														<label for="pers_status">Personal Statement</label>
													</div>
												</div>
											</div>
									
											<div class="row">
												<div class="col s12">
													<label>Personal Statement File Upload   </label>
													<div class="file-field input-field">
															<div class="file-field input-field">
																			<div class="btn">
																					<span>Upload</span>
																					<input type="file" name="pers_stat_file"> <i class="fa fa-cloud-upload"></i> 
																			</div>
																	<div class="file-path-wrapper">
																			<input class="file-path validate">

																			<div>
																			</div>
															
																	</div>
															</div>

															<div class="uploaded-file" id="pers_stat_file" data-type="file"></div>																
													</div>
												</div>
											</div>
										</div>
										
                                        <div class="row">
											<div class="col s6">
												<div class="input-field">
													<select name="pcc_status" id="pcc_status">
														<option value="Not-Applicable">Not Applicable</option>
														<option value="Received">Received</option>
														<option value="Not-Received">Not Received</option>
													</select>												
													<label for="pcc_status">Police Clearance Certificate</label>
												</div>
											</div>
                                        </div>
										
                                        <div class="row">										
											<div class="col s6 mx-pcc-recieved"  style="display:none;">
												<div class="input-field">
													<input name="pcc_recieved_date" id="pcc_recieved_date" class="datepicker" type="text">
													<label for="pers_status">PCC Received Date </label>
												</div>
											</div>
											<div class="col s6">
												<div class="input-field">
													<input name="tb_test_date" id="tb_test_date" class="datepicker" type="text">
													<label for="tb_test_date">Medical or TB Screening Test Date</label>
												</div>
											</div>																				
                                        </div>
								
                                        <div class="row">									
											<div class="col s6">
												<label>Medical or TB Screening Test File Upload</label>
												<div class="file-field input-field">
														<div class="file-field input-field">
																		<div class="btn">
																				<span>Upload</span>
																				<input type="file" name="tb_file_upload"> <i class="fa fa-cloud-upload"></i> 
																		</div>
																<div class="file-path-wrapper">
																		<input class="file-path validate">

																		<div>
																		</div>
														
																</div>
														</div>

														<div class="uploaded-file" id="tb_file_upload" data-type="file"></div>																
												</div>
											</div>
											<div class="col s6 mx-pcc-recieved"  style="display:none;">
												<label>PCC / DBS File Upload   </label>
												<div class="file-field input-field">
														<div class="file-field input-field">
																		<div class="btn">
																				<span>Upload</span>
																				<input type="file" name="pcc_file_upload"> <i class="fa fa-cloud-upload"></i> 
																		</div>
																<div class="file-path-wrapper">
																		<input class="file-path validate">

																		<div>
																		</div>
														
																</div>
														</div>

														<div class="uploaded-file" id="pcc_file_upload" data-type="file"></div>																
												</div>
											</div>												
                                        </div>	

                                        <div class="row">
											<div class="col s6">
												<div class="input-field">
													<select name="spons_status" id="spons_status">
														<option value="Not-Applicable">Not Applicable</option>
														<option value="Received">Received</option>
														<option value="Not-Received">Not Received</option>
													</select>												
													<label for="spons_status">Certificate of Sponsorship Status </label>
												</div>
											</div>																			
                                        </div>
										<div class="row cos-wrapper" style="display:none;">
											<div class="col s6">
												<label>COS (certificate of sponsorship) File</label>
												<div class="file-field input-field">
														<div class="file-field input-field">
																		<div class="btn">
																				<span>Upload</span>
																				<input type="file" name="spons_file_upload" multiple> <i class="fa fa-cloud-upload"></i> 
																		</div>
																<div class="file-path-wrapper">
																		<input class="file-path validate">

																		<div>
																		</div>
														
																</div>
														</div>

														<div class="uploaded-file" id="spons_file_upload" data-type="file"></div>																
												</div>
											</div>	
											<div class="col s6">
												<div class="input-field">
													<input name="spons_date" id="spons_date" class="datepicker" type="text">
													<label for="spons_date">COS (certificate of sponsorship) Received Date <label>
												</div>
											</div>												
                                        </div>	
                                </div>									
								
                                <div class="controls">
                                                <a class="mgbtn2">Back</a>
                                                <input id="form_name" type="hidden" class="validate" name="type_of" value="pcc_nhs">
                                                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                                                {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
                        

</div>

                