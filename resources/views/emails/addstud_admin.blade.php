Dear {{ $admin }},<br><br>

Student: {{ $user }} with email id {{ $useremail }} registered on {{ url('/') }}.<br><br>

You can login on <a href="{{ url('/userlogin') }}">{{ str_replace("http://", "", url('/userlogin')) }}</a> for further details.<br><br>

Best Regards,<br>
Zodocs