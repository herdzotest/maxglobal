<?php
/*$notifications = DB::table('notifications')->addselect('approval_text')
		->where('is_deleted', 0)->where('user_id', Auth::user()->id)->where('user_type', 'STUDENT')
		->where('is_read', 0)->orderBy('notif_dat', 'desc')->get();*/

$notifications = DB::table('notifications')->select('notifications.id')->addselect('notifications.comments')
		->where('is_deleted', 0)->where('user_id', Auth::user()->context_id)
		->where('is_read', 0)->orderBy('notif_dat', 'desc')->get();
$notificationCount = count($notifications); echo $notificationCount;

if(in_array(Auth::user()->type,['SUPER_ADMIN','MANAGER','CONSULTANTS','MAIN_AGENTS','SUBCONTRACTORS','UNIVERSITY_USER','EMPLOYER_ROLE'])) {
	$employee = DB::table('employees')->addselect('profilePic')
		->where('id', Auth::user()->context_id)->get(); 
	$avatatImg = ($employee[0]->profilePic == '') ? 'https://www.gravatar.com/avatar/ef1c169902a351837b6c7f8816cf9b5c.jpg?s=80&d=mm&r=g' : $employee[0]->profilePic;
} else {
	$avatatImg = 'https://www.gravatar.com/avatar/ef1c169902a351837b6c7f8816cf9b5c.jpg?s=80&d=mm&r=g';
}

?>			
	
		<!-- Navbar Right Menu -->
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav notinav">

				<!-- Messages: style can be found in dropdown.less-->
				@if(LAConfigs::getByKey('show_messages'))
				<!-- <li class="dropdown messages-menu">
					
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-speech "></i>
						<span class="label label-success">24</span>
					</a>
					<ul class="dropdown-menu">
						<li class="header">You have 4 messages</li>
						<li>
							
							<ul class="menu">
								<li>
									<a href="#">
										<div class="pull-left">
											
											<img src="@if(isset(Auth::user()->email)) {{ Gravatar::fallback(asset('la-assets/img/user2-160x160.jpg'))->get(Auth::user()->email) }} @else asset('/img/user2-160x160.jpg' @endif" class="img-circle" alt="User Image"/>
										</div>
										
										<h4>
											Support Team
											<small><i class="fa fa-clock-o"></i> 5 mins</small>
										</h4>
										
										<p>Why not buy a new awesome theme?</p>
									</a>
								</li>
							</ul>
						</li>
						<li class="footer"><a href="#">See All Messages</a></li>
					</ul>
				</li> --><!-- /.messages-menu -->
				@endif

				@if(LAConfigs::getByKey('show_notifications'))
				<!-- Notifications Menu -->
				<li class="dropdown notifications-menu notifications-wrapper">
					<!-- Menu toggle button -->
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<i class="icon-bell "></i>

						<span class="label label-warning"><?php echo $notificationCount; ?></span>

						<?php
						if($notificationCount > 0) {
						?>
							@push('scripts')
							<script>
							$(function () {
								$('.notif_count').each(function () {
									$(this).prop('Counter',0).animate({
										Counter: $(this).text()
									}, {
										duration: 2000,
										easing: 'swing',
										step: function (now) {
											$(this).text(Math.ceil(now));
										}
									});
								});
								function blink_text() {
									$('.notif_count').fadeOut(500);
									$('.notif_count').fadeIn(500);
								}
								setInterval(blink_text, 2000);	
							});
							</script>
							@endpush
							<input type="hidden" id="notifCountVal" value="<?php echo $notificationCount; ?>">
							<span class="label notif_count" style="height: 20px !important;
								width: 20px !important;
								line-height: 15px !important;
								font-size: 12px !important;
								top: 12px !important;
								right: 5px !important;background-color: #ef0010!important;"><?php echo $notificationCount; ?></span>
						<?php
						}
						?>



					</a>
					<ul class="dropdown-menu">
						<li class="header">You have <?php echo $notificationCount; ?> notifications</li>
						<li>
							<!-- Inner Menu: contains the notifications -->
							<ul class="menu">
								<?php
								foreach($notifications as $notif) {
								?>
									<li><!-- start notification -->
										<!-- <a href="#">
											<i class="fa fa-users text-aqua"></i> 
											<?php //echo $notif->approval_text; ?>
										</a> -->
										<a href="{{ url(config('laraadmin.adminRoute') . '/user_profile/'.Auth::user()->context_id.'/view#tab-timeline') }}">
											<i class="fa fa-users text-aqua"></i> 
											<?php echo $notif->comments; ?>
										</a>
									</li><!-- end notification -->
								<?php
								}
								?>
							</ul>
						</li>
						<?php
						if($notificationCount > 0) { 
						?>
							<!-- <li class="footer"><a href="{{ url(config('laraadmin.adminRoute') . '/students/'.Auth::user()->id.'#tab-notifications') }}">View all</a></li> -->
							<li class="footer"><a href="{{ url(config('laraadmin.adminRoute') . '/user_profile/'.Auth::user()->context_id.'/view#tab-timeline') }}">View all</a></li>
						<?php
						}
						?>
					</ul>
				</li>
				@endif

				@if(LAConfigs::getByKey('show_tasks'))
				<!-- <li class="dropdown tasks-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-flag-o"></i>
						<span class="label label-danger">9</span>
					</a>
					<ul class="dropdown-menu">
						<li class="header">You have 9 tasks</li>
						<li>
							<ul class="menu">
								<li>
									<a href="#">
										<h3>
											Design some buttons
											<small class="pull-right">20%</small>
										</h3>
										<div class="progress xs">
											<div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
												<span class="sr-only">20% Complete</span>
											</div>
										</div>
									</a>
								</li>
							</ul>
						</li>
						<li class="footer">
							<a href="#">View all tasks</a>
						</li>
					</ul>
				</li> -->
				@endif
				@if (Auth::guest())
					<li><a href="{{ url('/login') }}">Login</a></li>
					<li><a href="{{ url('/register') }}">Register</a></li>
				@else
					<!-- User Account Menu -->
					<li class="dropdown user user-menu">
						<!-- Menu Toggle Button -->
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<!-- The user image in the navbar-->
							<!--<img src="{{ Gravatar::fallback(asset('la-assets/img/user2-160x160.jpg'))->get(Auth::user()->email) }}" class="user-image" alt="User Image"/> -->
							<img src="{{ $avatatImg }}" class="user-image" alt="User Image"/>
							<!-- hidden-xs hides the username on small devices so only the image appears. -->
							<!-- <span class="hidden-xs"></span> -->
						</a>
						<ul class="dropdown-menu userdd">
							<!-- The user image in the menu -->
							<li class="user-header">
								<!-- <img src="{{ Gravatar::fallback(asset('la-assets/img/user2-160x160.jpg'))->get(Auth::user()->email) }}" class="img-circle" alt="User Image" /> -->
								<p>
								<?php
									if(strlen(Auth::user()->name) < 7) {
										$usr_name = Auth::user()->name;
									} else {
										$usr_name = substr(Auth::user()->name,0,7).'...';
									}
								?>
								<!-- Welcome	{{ Auth::user()->name }} -->
								Welcome	{{ $usr_name }}
									<?php
									$datec = Auth::user()['created_at'];
									?>
							
								</p>
							</li>
							<!-- Menu Body -->
							@role("SUPER_ADMIN")
							
							<li class="user-body">
								<!--
								<div class="col-xs-6 text-center mb10">
									<a href="{{ url(config('laraadmin.adminRoute') . '/lacodeeditor') }}"><i class="fa fa-code"></i> <span>Editor</span></a>
								</div>
								
								
								<div class="col-xs-6 text-center mb10">
									<a href="{{ url(config('laraadmin.adminRoute') . '/modules') }}"><i class="fa fa-cubes"></i> <span>Modules</span></a>
								</div>
								<div class="col-xs-6 text-center mb10">
									<a href="{{ url(config('laraadmin.adminRoute') . '/la_menus') }}"><i class="fa fa-bars"></i> <span>Menus</span></a>
								</div>
								
								<div class="col-xs-6 text-center mb10">
									<a href="{{ url(config('laraadmin.adminRoute') . '/la_configs') }}"><i class="fa fa-cogs"></i> <span>Configure</span></a>
								</div>
								
								
								<div class="col-xs-6 text-center">
									<a href="{{ url(config('laraadmin.adminRoute') . '/backups') }}"><i class="fa fa-hdd-o"></i> <span>Backups</span></a>
								</div>
								-->
							</li>
							
							@endrole
							<!-- Menu Footer-->
							<li class="user-footer">
								
								<?php
								$urlType = '';
								if(Auth::user()->type == "STUDENT") {
									$urlType = 'students';
								} else if(Auth::user()->type == "NURSE") {
									$urlType = 'nurses';
								}
								?>
								
								<div class="pull-left">
									@if(in_array(Auth::user()->type,["STUDENT","NURSE"]))
									<a href="{{ url(config('laraadmin.adminRoute') . '/'.$urlType.'/') .'/'. Auth::user()->id }}" class="btn btn-default btn-flat">Profile</a>
									@else
									<a href="{{ url(config('laraadmin.adminRoute') . '/user_profile/') .'/'. Auth::user()->context_id.'/view' }}" class="btn btn-default btn-flat">Profile</a>
									@endif
								</div>
							
								<div class="pull-right">
									<a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Sign out</a>
								</div>
							</li>
						</ul>
					</li>
				@endif
				@if(LAConfigs::getByKey('show_rightsidebar'))
				<!-- Control Sidebar Toggle Button -->
				<!-- <li>
					<a href="#" data-toggle="control-sidebar"><i class="fa fa-comments-o"></i> <span class="label label-warning">10</span></a>
					
				</li> -->
				@endif
			</ul>
		</div>