
<style>
#std_form .searchSelectLabel {
	margin-top: -40px;
}
#std_form .select2-container {
	margin-top: 17px;
}
</style>

<div id="std_form" class="tab-pane active" >
        
                   
<div class="content-head">
                                        
                                        <h4>Student Details</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                        </div>
                        {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'personal-add-form']) !!}
                <div class="pane agent_student_form">
						<input type="hidden" id="mx-simple-stud-form">
						<div class="row">
                            <div class="input-field col s6">
								<input name="first_name" required id="first_name" type="text" class="validate">
                                <label for="first_name">Name*</label>
                                <span class="helper-text" data-error="wrong" data-success="right">As per Passport</span>
                            </div>
                            <?php
							$current_year = date('Y');
							$earliest_year = 1990; 
							if($studentId == 0 || $studentId == '') {
							?>
							<div class="col s6 ipfield">
								<div class="input-field">
									<!-- <i class="material-icons prefix">mail</i> -->
									<?php
									if($studentId == 0 || $studentId == '') {
									?>
										<input name="email" id="email" required type="email" class="validate">
									<?php } else { ?>
										<input type="email" value="" readonly>
									<?php } ?>
									<label for="email">Email*</label>
								</div>
							</div>
							<?php } ?>
                        </div>

						<div class="row">
							<div class="col s2">
								<div class="input-field">
									<select name="coountry_code" id="coountry_code" required class="formSelect">
										<option value="91" selected>+91</option>
										<option value="966">+966</option>
										<option value="971">+971</option>
										<option value="968">+968</option>
									</select>
									<label for="coountry_code">Country code* </label>
								</div>
							</div>
							<div class="col s4">
								<div class="input-field">
									<input id="mobile" min="10" name="mobile" type="number" required class="validate">
									<label for="contact_no">Contact Number* </label>
								</div>
							</div>
							<div class="col s2">
								<div class="input-field">
									<select name="coountry_code_whatsapp" id="coountry_code_whatsapp" required class="formSelect">
										<option value="91" selected>+91</option>
										<option value="966">+966</option>
										<option value="971">+971</option>
										<option value="968">+968</option>
									</select>
									<label for="coountry_code_whatsapp">Country code* </label>
								</div>
							</div>
							<div class="col s4">
								<div class="input-field">
									<input id="whatsapp" min="10" name="whatsapp" type="number" required class="validate">
									<label for="whatsapp">Whatsapp* </label>
								</div>
							</div>
						</div>

                        <div class="row">
							<div class="input-field col s6">
                                <input id="dob" name="dob" type="text" class="dateofbirth">
                                <label for="dob">Date of Birth</label>
                                <span class="helper-text" data-error="wrong" data-success="right">As per Passport</span>
                            </div>
                        </div>

                        <div class="row">
                        		<div class="input-field col s6">
								<select name="end_year" id="end_year" class="searchSelect">
									<option value="">Select</option>
									<?php
										foreach(range(date('Y'), $earliest_year) as $x) {
											echo '<option value="'.$x.'"'.($x === $current_year ? ' selected="selected"' : '').'>'.$x.'</option>';
										}
									?>
								</select>
								<label for="end_year" class="searchSelectLabel">10th passout year</label>
                            	</div>
                            	<div class="col s6">
									<div class="input-field">
                                        <input id="grade" name="grade" type="number" class="validate" step=".01" min=1>
                                        <label for="grade">10th Percentage(%)</label>
                                    </div>
								</div>
                        </div>

                        <div class="row">
                        	<div class="input-field col s6">
								<select name="hse_end_year" id="hse_end_year" class="searchSelect">
									<option value="">Select</option>
									<?php
										foreach(range(date('Y'), $earliest_year) as $x) {
											echo '<option value="'.$x.'"'.($x === $current_year ? ' selected="selected"' : '').'>'.$x.'</option>';
										}
									?>
								</select>
								<label for="hse_end_year" class="searchSelectLabel">12th passout year</label>
                            </div>
                            <div class="col s6">
								<div class="input-field">
                                    <input id="hse_grade" name="hse_grade" type="number" class="validate" step=".01" min=1 >
                                    <label for="sslcgrade">12th Percentage(%)</label>
                                </div>
							</div>
                        </div>

                        <div class="row">
                        	<div class="input-field col s6">
								<select name="UG_end_year" id="UG_end_year" class="searchSelect" style="top:14px;">
									<option value="">Select</option>
									<?php
										foreach(range(date('Y'), $earliest_year) as $x) {
											echo '<option value="'.$x.'"'.($x === $current_year ? ' selected="selected"' : '').'>'.$x.'</option>';
										}
									?>
								</select>
								<label for="UG_end_year" class="searchSelectLabel">UG passout year</label>
							</div>
                            <div class="col s6">
                                <div class="input-field ">
									<input id="UG_grade" name="UG_grade" type="number" class="validate" step=".01" min=1 >
                                    <label for="sslcgrade">UG Percentage(%)</label>
                                </div>
                            </div>
                        </div>

						<div class="row">
							<div class="input-field col s6">
									<select name="ug_university" id="ug_university" class="searchSelect" style="top:14px;">
									<option value="">Select</option>
									<?php
										foreach($university as $universityData) {
													echo '<option value="'.$universityData->university.'">'.$universityData->university.'</option>';
										}
									?>
									</select>
									<label for="ug_university" class="searchSelectLabel">UG University</label>
							</div>
                        </div>
                        <div class="row">
                        	<div class="input-field col s6">
								<select name="course_title" id="course_title" class="formSelect">
										<option value="">Select</option>
										<?php
										foreach($courses as $courseData) {
													echo '<option value="'.$courseData->course_name.'">'.$courseData->course_name.'</option>';
										}
										?>										
								</select>
								<label for="gender">Preferred Course</label>
							</div>
                        	<div class="input-field col s6">
								<select name="course_country" id="course_country" class="formSelect" >
										<option value="">Select</option>
										<?php
										foreach($countries as $countriesData) {
											echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
										}
										?>										
								</select>
								<label for="gender">Preferred Country</label>
                            </div>
                        </div>
						<input id="notify" type="hidden" name="notify" value="0">
                </div>
                <div class="controls">
                                <!--<a href="{{ route('admin.students.index') }}" class="mgbtn2">Back</a>-->
                                <!-- <a class="waves-effect waves-light btn" type="submit"><i class="material-icons left">save</i>SAVE</a> -->
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="personal_information">
                                 <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                              
                                {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                        </div>
                        {{ Form::close() }}
</div>
