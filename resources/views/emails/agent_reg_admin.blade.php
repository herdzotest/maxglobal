Dear {{ $admin }},<br><br>

There is an agent registration in Zodocs.<br>
Agent Name : {{ $agent }} <br>
Email : {{ $agentemail }} <br><br>

You can login to <a href="{{ url('/userlogin') }}">{{ str_replace("http://", "", url('/userlogin')) }}</a>. Please check the details and enable the account.<br><br>

Best Regards,<br>
Zodocs
