@extends('la.layouts.app')

@section('htmlheader_title')
	Nurse View
@endsection


@section('main-content')
<div id="page-content" class="profile2">

	<div class="bg-primary clearfix vhead">
		<div class="vhead-wrap">
			<div class="namewrap">
					<div class="vhimg" style=""> </div>
					<!-- <div class="profile-icon text-primary"><i class="fa fa-group"></i></div> -->
					<h4 class="name">
						<?php echo $students->first_name; ?> <?php  echo $students->sur_name; ?>
						<?php if($students->is_approved == '1') { ?>
							<a href="#" title="Approved"><span class="apprbadge"></span></a>
						<?php } ?>
					</h4>
					<!--
					<div class="row stats">
						<div class="col-md-4"><i class="fa fa-facebook"></i> 234</div>
						<div class="col-md-4"><i class="fa fa-twitter"></i> 12</div>
						<div class="col-md-4"><i class="fa fa-instagram"></i> 89</div>
					</div>
					-->
			</div>
			<div class="vhactions">
				@la_access("Employees", "edit")
					<a href="{{ url(config('laraadmin.adminRoute') . '/nurses/'.$students->id.'/edit') }}" class="btn mgbtn3"><i class="fa fa-pencil"></i></a><br>
				@endla_access
			</div>
		</div>
		
		<div class="vhbottom">
		<i class="fa fa-envelope-o"></i> <?php echo  $students->email; ?>
		<span>| </span> 
		<i class="fa fa-calendar-o"></i> <?php echo  $students->dob; ?>
		</div>		
		
	</div>

	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		<li class=""><a href="{{ url(config('laraadmin.adminRoute') . '/nurses/') }}" data-toggle="tooltip" data-placement="right" title="Back to view"><i class="fa fa-chevron-left"></i></a></li>
		<li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> General Info</a></li>
		<li class=""><a role="tab" data-toggle="tab" href="#tab-timeline" data-target="#tab-timeline"><i class="fa fa-clock-o"></i> Timeline</a></li>
		@if(Entrust::hasRole("SUPER_ADMIN") || Entrust::hasRole("MANAGER") || Entrust::hasRole("NURSE"))
			<li class=""><a role="tab" data-toggle="tab" href="#tab-account-settings" data-target="#tab-account-settings"><i class="fa fa-key"></i> Account settings</a></li>
		@endif
		<li class=""><a role="tab" data-toggle="tab" href="#tab-notifications" data-target="#tab-notifications"><i class="fa fa-clock-o"></i> Notifications</a></li>
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading" style="border-bottom:none;">
						<h4></h4>
					</div>
					<div class="panel-body">
						<!--
						<div class="form-group"><label for="name" class="col-md-2">Name :</label><div class="col-md-10 fvalue"><?php echo $students->first_name; ?> <?php echo $students->middle_name; ?> <?php  echo $students->sur_name; ?></div></div>
						<div class="form-group"><label for="email" class="col-md-2">Email :</label><div class="col-md-10 fvalue"><a href="mailto:manager@gmail.com"><?php echo $students->email; ?></a></div></div>						
						<div class="form-group"><label for="email" class="col-md-2">Email :</label><div class="col-md-10 fvalue"><a href="mailto:manager@gmail.com"><?php echo $students->email; ?></a></div></div>	
						-->
						
						<main class="clearfix">
											<div class="form_sidebar ">
												<ul >
														<li class="active">
																<a  class="active" href="#tab_1" data-toggle="tab" aria-expanded="true">
																		<span> 1 </span>
																		<div>Personal Information</div>
																</a>
														</li>
														<li>
																<a  href="#tab_2" data-toggle="tab" aria-expanded="false">
																		<span> 2 </span>
																		<div> QUALIFICATIONS</div>
																</a>
														</li>
														<li>
																<a  href="#tab_3" data-toggle="tab" aria-expanded="false">
																		<span> 3 </span>
																		<div>Employement history</div>
																</a>
														</li>
														<li>
																<a  href="#tab_4" data-toggle="tab" aria-expanded="false">
																		<span> 4 </span>
																		<div>Preferences</div>
																</a>
														</li>
														<li>
																<a  href="#tab_5" data-toggle="tab" aria-expanded="false">
																		<span> 5 </span>
																		<div>English language</div>
																</a>
														</li>
														<li>
																<a  href="#tab_6" data-toggle="tab" aria-expanded="false">
																		<span> 6 </span>
																		<div>Employer details</div>
																</a>
														</li>
														<li>
																<a  href="#tab_7" data-toggle="tab" aria-expanded="false">
																		<span>7</span>
																		<div>NMC Registration</div>
																</a>
														</li>
														<li>
																<a  href="#tab_8" data-toggle="tab" aria-expanded="false">
																		<span>8</span>
																		<div>Dependant details</div>
																</a>
														</li>
														<li>
																<a  href="#tab_9" data-toggle="tab" aria-expanded="false">
																		<span>9</span>
																		<div>Visa & Immigration</div>
																</a>
														</li>
														<li>
																<a  href="#tab_10" data-toggle="tab" aria-expanded="false">
																		<span>10</span>
																		<div>Deployement details</div>
																</a>
														</li>
														<li>
																<a  href="#tab_11" data-toggle="tab" aria-expanded="false">
																		<span>11</span>
																		<div>Feedback</div>
																</a>
														</li>
														<li>
																<a  href="#tab_12" data-toggle="tab" aria-expanded="false">
																		<span>12</span>
																		<div>Post Deployement</div>
																</a>
														</li>														
												</ul>
											</div>
											<br>
											
											<div class="contents tab-content" id="mx_students_view">
											@include('la.nurses.views.personal')
											@include('la.nurses.views.qualification')
											@include('la.nurses.views.empl_hist')
											@include('la.nurses.views.preferences')
											@include('la.nurses.views.english_lang')
											@include('la.nurses.views.employer_deat')
											@include('la.nurses.views.nurse_nmc')
											@include('la.nurses.views.dependant')
											@include('la.nurses.views.nurse_visa')
											@include('la.nurses.views.nurse_joining')
											@include('la.nurses.views.nurse_feedback')
											@include('la.nurses.views.nurse_payments')
											</div>
									</main>						
						
						
					</div>
				</div>
			</div>
		</div>
		
		<!-- time-line -->
		<div role="tabpanel" class="tab-pane fade in p20 bg-white" id="tab-timeline">
			@if(Session::has('success_mail_delete'))
				<p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success_mail_delete') }}</p>
			@endif
			<?php
			$mailDetailswithIds = json_encode(array(),true);
			if(count($mailDetails) == 0) {
			?>
				<div class="text-center p30"><i class="fa fa-list-alt" style="font-size: 100px;"></i> <br> No mails to show</div>
			<?php
			} else {
				$mailDetailsIds = array_column($mailDetails,'id');
				$mailDetailswithIds = json_encode(array_combine($mailDetailsIds,$mailDetails),true);
			?>
				<input type="hidden" value="<?php echo $mailDetailswithIds; ?>">
				<ul class="timeline timeline-inverse" id="mail-lists">
					<?php 
					$currentDate = '';
					foreach($mailDetails as $result) {	
						$dates = explode(' ',$result->send_date);
						$old_timestamp = strtotime($dates[1]);
						$new_time = date('h:i a', $old_timestamp);
						
						if($dates[0] != $currentDate) {
							$currentDate = $dates[0];
							$old_date_timestamp = strtotime($dates[0]);
							$new_date = date('d M Y', $old_date_timestamp);
						?>
							<li class="time-label">
								<span class="bg-red"><?php echo $new_date; ?></span>
							</li>
						<?php
						}
						?>
						<li>
							<i class="fa fa-envelope bg-blue"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> <?php echo $new_time; ?></span>
								<h3 class="timeline-header header"><a href="javascript:void(0);"><?php echo $result->name; ?></a> has sended you an email</h3>
								<h3 class="timeline-header subject">Subject: &nbsp;<a href="javascript:void(0);"><?php echo $result->subject; ?></a></h3>
								<div class="timeline-body">
									<?php
									$mailContent = strip_tags($result->mail_content);
									if(strlen($mailContent) > 150)
										$mailContent = substring(0,150,$mailContent).'...';
									
									echo $mailContent;
									?>
								</div>
								<div class="timeline-footer">
									<a href="javascript:void(0);" data-url="{{ url(config('laraadmin.adminRoute') . '/updateConsultantMail/'.$result->id) }}" data-id="<?php echo $result->id; ?>" data-time="<?php echo $new_time; ?>" data-user="<?php echo $result->name; ?>" data-date="<?php echo $new_date; ?>" data-subject="<?php echo $result->subject; ?>" class="btn btn-primary btn-xs mail-read-more">Read more</a>
									<a href="javascript:void(0);" data-id="<?php echo $result->id; ?>" class="btn btn-danger btn-xs mail-delete">Delete</a>
									<form action="{{ url(config('laraadmin.adminRoute') . '/deleteConsultantMail/'.$result->id.'/'.$employee->id) }}" id="delete-mail-form-<?php echo $result->id; ?>" method="post">
										{{ csrf_field() }}
									</form>
								</div>
							</div>
						</li>
					<?php 
					}
					?>
					<li><i class="fa fa-clock-o bg-gray"></i></li>
				</ul>
				<ul class="timeline timeline-inverse" id="mail-details">
					<li class="time-label">
						<span class="bg-red"><?php echo $new_date; ?></span>
						<a href="javascript:void(0);" id="mail-back" class="btn btn-primary" style="float: right;margin-right: 15px;">Back</a>
					</li>
					<li>
						<i class="fa fa-envelope bg-blue"></i>
						<div class="timeline-item">
							<span class="time"><i class="fa fa-clock-o"></i><span></span></span>
							<h3 class="timeline-header header"><a href="javascript:void(0);"></a> has sended you an email</h3>
							<h3 class="timeline-header subject">Subject: &nbsp;<a href="javascript:void(0);"></a></h3>
							<div class="timeline-body"></div>
							<div class="timeline-footer">							
							</div>
						</div>
					</li>
					<li><i class="fa fa-clock-o bg-gray"></i></li>
				</ul>
			<?php
			}
			?>
		</div>
		
		<!-- notifications -->
		<div role="tabpanel" class="tab-pane fade in p20 bg-white" id="tab-notifications">
			@if(Session::has('success_notif_delete'))
				<p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success_notif_delete') }}</p>
			@endif
			<?php
			$notifDetailswithIds = json_encode(array(),true);
			if(count($notifications) == 0) {
			?>
				<div class="text-center p30"><i class="fa fa-list-alt" style="font-size: 100px;"></i> <br> No notifications to show</div>
			<?php
			} else {
				
				// documents types array
				$docarray = array('image_front' => 'Passport front image', 'image_back' => 'Passport back image',
					'qualification-file' => 'Qualification file', 'ielts_file' => 'IELTS file',
					'cv_upload_file' => 'CV file', 'visa_file' => 'Visa doc',
					'visa_appr_file' => 'Visa approval doc', 'flight_ticket' => 'Flight ticket',
					'appl_form' => 'Application form', 'refer_file' => 'Reference doc',
					'pers_stat_file' => 'personal status doc', 'tb_file_upload' => 'TB doc',
					'pcc_file_upload' => 'PCC image', 'spons_file_upload' => 'Sponsor doc',
					'nmc_file' => 'NMC doc', 'offer_letter_file' => 'Offerr letter','cbt_score_upload' => 'CBT score doc',
				);

				$notifDetailsIds = array_column($notifications,'id');
				$notifDetailswithIds = json_encode(array_combine($notifDetailsIds,$notifications),true);
			?>
				<input type="hidden" value="<?php echo $notifDetailswithIds; ?>">
				<ul class="timeline timeline-inverse" id="notif-lists">
					<?php 
					$currentDate = '';
					foreach($notifications as $result) {	
						$dates = explode(' ',$result->notif_dat);
						$old_timestamp = strtotime($dates[1]);
						$new_time = date('h:i a', $old_timestamp);
						
						if($dates[0] != $currentDate) {
							$currentDate = $dates[0];
							$old_date_timestamp = strtotime($dates[0]);
							$new_date = date('d M Y', $old_date_timestamp);
						?>
							<li class="time-label">
								<span class="bg-red"><?php echo $new_date; ?></span>
							</li>
						<?php
						}
						?>
						<li>
							<i class="fa fa-bell-o bg-blue"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> <?php echo $new_time; ?></span>
								<h3 class="timeline-header header"><a href="javascript:void(0);"><?php echo $result->approval_text; ?></a></h3>
								<div class="timeline-body">
									<?php
									$comments = strip_tags($result->comments);
									if(strlen($comments) > 150)
										$comments = substring(0,150,$comments).'...';
									
									echo $comments;
									?>
								</div>
								<div class="timeline-footer">
									<a href="javascript:void(0);" data-url="{{ url(config('laraadmin.adminRoute') . '/updateNurseNotif/'.$result->id) }}" data-id="<?php echo $result->id; ?>" data-time="<?php echo $new_time; ?>" data-filetype="<?php echo $result->approval_text; ?>" 
										 data-date="<?php echo $new_date; ?>" class="btn btn-primary btn-xs notif-read-more">Read more</a>
									<a href="javascript:void(0);" data-id="<?php echo $result->id; ?>" class="btn btn-danger btn-xs notif-delete">Delete</a>
									<form action="{{ url(config('laraadmin.adminRoute') . '/deleteNurseNotif/'.$result->id.'/'.$result->user_id) }}" id="delete-notif-form-<?php echo $result->id; ?>" method="post">
										{{ csrf_field() }}
									</form>
								</div>
							</div>
						</li>
					<?php 
					}
					?>
					<li><i class="fa fa-clock-o bg-gray"></i></li>
				</ul>
				<ul class="timeline timeline-inverse" id="notif-details">
					<li class="time-label">
						<span class="bg-red"><?php echo $new_date; ?></span>
						<a href="javascript:void(0);" id="notif-back" class="btn btn-primary" style="float: right;margin-right: 15px;">Back</a>
					</li>
					<li>
						<i class="fa fa-envelope bg-blue"></i>
						<div class="timeline-item">
							<span class="time"><i class="fa fa-clock-o"></i><span></span></span>
							<h3 class="timeline-header header"><a href="javascript:void(0);"></a> </h3>
							<div class="timeline-body"></div>
							<div class="timeline-footer"></div>
						</div>
					</li>
					<li><i class="fa fa-clock-o bg-gray"></i></li>
				</ul>
			<?php
			}
			?>
		</div>
		
		@if(Entrust::hasRole("SUPER_ADMIN") || Entrust::hasRole("MANAGER")  || Entrust::hasRole("NURSE"))
			<div role="tabpanel" class="tab-pane fade" id="tab-account-settings">
				<div class="tab-content">
					<form action="{{ url(config('laraadmin.adminRoute') . '/changeNursePass/'.$students->id) }}" id="password-reset-form" class="general-form dashed-row white" method="post" accept-charset="utf-8">
						{{ csrf_field() }}
						<div class="panel">
							<div class="panel-default panel-heading">
								<h4>Account settings</h4>
							</div>
							<div class="panel-body">
								@if (count($errors) > 0)
									<div class="alert alert-danger">
										<ul>
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
								@if(Session::has('success_message'))
									<p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success_message') }}</p>
								@endif
								<div class="form-group">
									<label for="password" class=" col-md-2">Password</label>
									<div class=" col-md-10">
										<input type="password" name="password" value="" id="password" class="form-control" placeholder="Password" autocomplete="off" required="required" data-rule-minlength="6" data-msg-minlength="Please enter at least 6 characters.">
									</div>
								</div>
								<div class="form-group">
									<label for="password_confirmation" class=" col-md-2">Retype password</label>
									<div class=" col-md-10">
										<input type="password" name="password_confirmation" value="" id="password_confirmation" class="form-control" placeholder="Retype password" autocomplete="off" required="required" data-rule-equalto="#password" data-msg-equalto="Please enter the same value again.">
									</div>
								</div>
							</div>
							<div class="panel-footer">
								<button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> Change Password</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		@endif
	</div>
</div>

<!-- common modal -->
<!-- <div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel"> -->
<div class="modal fade" id="AddModal1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Submit comment</h4>
			</div>
			<div class="modal-body">
				<div class="box-body">
					<input type="hidden" id="doc-type" value="">
					<input type="hidden" id="doc-value" value="">
					<div class="form-group">
						<label for="address">Comments :</label>
						<textarea class="form-control" placeholder="Enter comments" data-rule-maxlength="300" cols="30" rows="3" name="comments" id="mx-comments"></textarea>
					</div>					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="mx-comment-close" data-dismiss="modal">Close</button>
				<input class="btn btn-success" type="submit" id="mx-comment" value="Submit">
			</div>
		</div>
	</div>
</div>

<meta name="csrf-token" content="{{ csrf_token() }}" />
<input type="hidden" value="<?php echo $students->id; ?>" id="student_id">

@endsection

@push('styles')
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Varela+Round&display=swap" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/stdentsform/css/normalize.css') }}"/>
<link rel="stylesheet" href="{{ asset('la-assets/stdentsform/css/materialize.css') }}">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/stdentsform/css/main.css') }}"/>
<style> .uploaded-file { width: 150px; height: 150px;}</style>
@endpush

@push('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script>
$(function () {
	
	var studentId = $("#student_id").val();
	$(".uploaded-file").addClass("hide");
	getStdentData(studentId,'tab_1');
	$("#mx_students_view input,#mx_students_view textarea").prop("readonly", true);

	$('body').off('click', '.form_sidebar li a');
	$('body').on('click', '.form_sidebar li a', function() {
		
		type = $(this).attr('href');
		getStdentData(studentId,type);
	});
	
	$('body').off('click', '.mx-approve-doc');
	$('body').on('click', '.mx-approve-doc', function() {		
		$("#doc-type").val($(this).attr('data-type'));
		$("#doc-value").val($(this).attr('data-val'));
	});
	$('body').off('click', '#mx-comment');
	$('body').on('click', '#mx-comment', function() {	
		
		var comments = $("#mx-comments").val();
		type = $("#doc-type").val();
		statusvalue = $("#doc-value").val();
		var url = '{{ url(config('laraadmin.adminRoute')) }}/nurses_doc_approve';
		
		$.ajax({
		   headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: url,
			//type: "POST",
			type: "GET",
			data: {'id':studentId,'comment': comments,'type':type,'statusvalue':statusvalue},
			success: function(response){
				if(response == 'SUCCESS') {
					if(statusvalue == '1') {
						$("[data-type=" + type + "]").parent().html("<span class='approve_badge btn btn-success pull-right'>APPROVED</span>");
					} else {
						$("[data-type=" + type + "]").parent().html("<span class='rejected_badge btn btn-success pull-right'>REJECTED</span>");
					}
				}
				$("#mx-comment-close").trigger('click');				
			}
		});			
	});
	
	// mail details....
	$("#mail-details").hide();
	$('body').off('click', '.mail-read-more');
	$('body').on('click', '.mail-read-more', function() {
		$("#mail-details").show(); $("#mail-lists").hide();
		var mailDate = $(this).attr('data-date');
		var mailId = $(this).attr('data-id');
		var mailContent = <?php echo $mailDetailswithIds; ?>;
		mailContent = mailContent[mailId]['mail_content'];
		var mailSubject = $(this).attr('data-subject');
		var mailFrom = $(this).attr('data-user');
		var mailTime = $(this).attr('data-time');
		var url = $(this).attr('data-url');
		
		$("#mail-details .time-label bg-red").text(mailDate);
		$("#mail-details .timeline-body").html(mailContent);
		$("#mail-details .subject a").text(mailSubject);
		$("#mail-details .header a").text(mailFrom);
		$("#mail-details .time span").text(mailTime);
		
		// update the read count of mail
		$.ajax({
		   headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: url,
			type: "POST",
			data: {},
			success: function(data){
			}
		});
	});
	$('body').off('click', '#mail-back');
	$('body').on('click', '#mail-back', function() {
		$("#mail-details").hide(); $("#mail-lists").show();
	});
	$('body').off('click', '.mail-delete');
	$('body').on('click', '.mail-delete', function() {
		var mailId = $(this).attr('data-id');
		if(confirm('are you sure?')) {
			$('#delete-mail-form-'+mailId).submit();
		} else { return false; }
	});
	
	// notification details....
	$("#notif-details").hide();
	$('body').off('click', '.notif-read-more');
	$('body').on('click', '.notif-read-more', function() {
		$("#notif-details").show(); $("#notif-lists").hide();
		var notifDate = $(this).attr('data-date');
		var notifId = $(this).attr('data-id');
		var notifContent = <?php echo $notifDetailswithIds; ?>; 
		notifContent = notifContent[notifId]['comments'];
		var notiffiletype = $(this).attr('data-filetype');
		var notifTime = $(this).attr('data-time');
		var url = $(this).attr('data-url');
		
		$("#notif-details .time-label bg-red").text(notifDate);
		$("#notif-details .timeline-body").html(notifContent);
		$("#notif-details .header a").text(notiffiletype);
		$("#notif-details .time span").text(notifTime);
		
		// update the read count of mail
		$.ajax({
		   headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: url,
			type: "POST",
			data: {},
			success: function(data){
			}
		});
	});
	$('body').off('click', '#notif-back');
	$('body').on('click', '#notif-back', function() {
		$("#notif-details").hide(); $("#notif-lists").show();
	});
	$('body').off('click', '.notif-delete');
	$('body').on('click', '.notif-delete', function() {
		var notifId = $(this).attr('data-id');
		if(confirm('are you sure?')) {
			$('#delete-notif-form-'+notifId).submit();
		} else { return false; }
	});
});

function getStdentData(studentId,type) {
	type = type.replace('#','');
	var url = '{{ url(config('laraadmin.adminRoute')) }}/nurse_data/' + studentId + '/' + type;

	$.ajax({
		url: url,
		data: {},
		success: function(response){
			var details = JSON.parse(response);
			
			$.each(details, function(key, value) {
				element = $("#"+key);   elementName = $("input[name='" + key + "']"); 


				
				if(element.length > 0) {
					if(element.attr('data-type') == 'file') {
						if(value != null && value != '') {
							
							element.removeClass("hide"); // remove hide class of frame to show image 
							if(details[key + "_approved"] != null) {
								var studStatus = (details[key + "_approved"] == '1') ? 'APPROVED' : 'REJECTED';
								element.next().html("<span class='approve_badge btn btn-success pull-right'>" + studStatus + "</span>");
							} 
							var userType = "<?php echo Auth::user()->type; ?>";
							if(userType != 'STUDENT' && userType != 'NURSE') {
								element.parent().next().removeClass('hide'); // show approve reject button
							}
							element.next().removeClass('hide');
							$(".appendedImages").remove();
							var images = value.split(",");
							
							element.css("background-image", "url(" + images[0] + ")");
							element.wrap('<a target="_blank" href="' + images[0].split("?")[0] + '"></a>');
							
							if(images.length > 1) {
								for(i=1;i<=(images.length)-1;i++) {
									element.parent().parent().append('<a target="_blank" class="appendedImages" href="' + images[i].split("?")[0] + '"><div style="background-image: url(' + images[i] + ')" class="uploaded-file" data-type="file"></div></a>');
								}
							}
						}
					} else {
						if(element.attr('data-type') == 'radio') {
							if(value == '0') {
								element.text('Yes');
							} else if(value == '2') {
								element.text('Partly');
							} else {
								element.text('No');
							}
						} else {
							value = (value == '' || value == 'null') ? '' : value;
							element.text(value);
						}
					}

					// Dependent details new
					switch(key) {
						case 'relatiion_status':
							if(value == 'Married' || value == 'Civil-partnership') {
								$('#spouse-details-wrapper').removeClass('hide');
							}
							break;
						case 'current_live_status':
							if(value == 'No') { 
								$('#spouse-adrress-details-wrapper').removeClass('hide');
							}
							break;
						case 'childrens':
							if(value == 'YES') { 
								//$('#child-wrap').removeClass('hide');
								$('#childrens-wrapper').removeClass('hide');
							}
							break;
					}
					// End Dependent details new



				} else if(key == 'json_data' && value != null) {

					var jsonData = JSON.parse(value);
					$(".common-wrapper").remove();
					
					$.each(jsonData, function(key, value) {
						switch(type) {
							case 'tab_3':
								/*$("#employement-wrapper").append('<div class="academic common-wrapper"> <div class="mgv-field"> <div class="name"> Job title </div> <div class="value">' + value.job_title + '</div> </div> <div class="mgv-field"> <div class="name"> Organization name </div> <div class="value">' + value.org_name + '</div> </div> <div class="mgv-field"> <div class="name"> Ward </div> <div class="value">' + value.ward + '</div> </div> <div class="mgv-field"> <div class="name"> Working status </div> <div class="value">' + value.working_status + '</div> </div> <div class="mgv-field"> <div class="name"> Start Date </div> <div class="value">' + value.employement_start_date + '</div> </div> <div class="mgv-field"> <div class="name"> End Date </div> <div class="value">' + value.employement_end_date + '</div> </div> <div class="mgv-field"> <div class="name"> Remarks </div> <div class="value">' + value.remarks + '</div> </div> <div class="mgv-field"> <div class="name"> Personal statement </div> <div class="value">' + value.pers_statement + '</div> </div> </div> ');
								break;*/
								$("#employement-wrapper").append(' <tr class="common-wrapper"> <td>' + value.job_title + '</td> <td>' + value.org_name + '</td> <td>' + value.ward + '</td> <td>' + value.working_status + '</td> <td>' + value.employement_start_date + '</td> <td>' + value.employement_end_date + '</td> <td>' + value.remarks + '</td> <td>' + value.pers_statement + '</td> </tr>');
								break;
							case 'tab_5':
								$("#englishLang-wrapper").append('<div class="academic common-wrapper"> <div class="mgv-field"> <div class="name"> Country </div> <div class="value">' + value.ielts_country + '</div> </div> <div class="mgv-field"> <div class="name"> Work start date </div> <div class="value">' + value.work_start_date + '</div> </div> <div class="mgv-field"> <div class="name"> Work End date </div> <div class="value">' + value.work_end_date + '</div> </div> <div class="mgv-field"> <div class="name"> Employer address </div> <div class="value">' + value.employer_addresss + '</div> </div> <div class="mgv-field"> <div class="name"> Email ID of employer </div> <div class="value">' + value.employer_email + '</div> </div> <div class="mgv-field"> <div class="name"> Nursing council registration certificate </div> <div class="value"> <div class="uploaded-file" id="file_upload_nursing'+key+'" data-type="file"></div> </div> </div> <div class="mgv-field"> <div class="name"> Reference letter </div> <div class="value">' + value.reference_letter + '</div> </div> </div>');
								
								//valueExperience = value.experience_letter;
								valueExperience = value.file_upload_nursing;
								if(valueExperience != 'null' && valueExperience.replace(' ','') != '') {
									//element = $('#experienceLetter-' + key);
									//element = $('#file_upload_nursing-' + key);
									element = $('#file_upload_nursing'+key);
									displayUploadedFiles(element,key,valueExperience);
								} else {
									element.text('No data available').removeClass('hide').removeClass('uploaded-file');
								}
								
								break;
							case 'tab_8':
								$("#childrens-wrapper").append(' <div class="academic common-wrapper"> <div class="mgv-field"> <div class="name"> Name (First Name+Family Name) </div> <div class="value" id="name">' + value.name + '</div> </div> <div class="mgv-field"> <div class="name"> Date of birth </div> <div class="value">' + value.birthdate + '</div> </div> <div class="mgv-field"> <div class="name"> Relationship to you? </div> <div class="value">' + value.relation + '</div> </div> <div class="mgv-field"> <div class="name"> Does this person currently live with you? </div> <div class="value">' + value.current_live + '</div> </div> <div class="mgv-field"> <div class="name"> Is this person travelling with you to the UK? </div> <div class="value">' + value.travel_to_uk + '</div> </div>');
								break;
							case 'tab_9':
								$("#imigration-wrapper").append(' <div class="academic common-wrapper"> <div class="mgv-field"> <div class="name"> Country </div> <div class="value">' + value.other_country_name + '</div> </div> <div class="mgv-field"> <div class="name"> Reason for your visit </div> <div class="value">' + value.reason + '</div> </div> <div class="mgv-field"> <div class="name"> Date you entered </div> <div class="value">' + value.visite_date + '</div> </div> <div class="mgv-field"> <div class="name"> Date you left </div> <div class="value">' + value.leave_date + '</div> </div> </div> ');
								break;									
							default:
								break;
						}
					});
				} 
			});
		}
	});			
}





//display the uploaded files
	function displayUploadedFiles(element,key,value) {
		element.addClass('uploaded-file');
		element.removeClass("hide"); // remove hide class of frame to show image 
		var images = value.split(",");
		var extension = images[0].split("?")[0].substr( (images[0].split("?")[0].lastIndexOf('.') +1) );
		if($.inArray(extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
			element.css("background-image", "url(" + images[0] + ")");
			element.wrap('<a target="_blank" href="' + images[0].split("?")[0] + '"></a>');								
		} else {
			switch (extension) {
				case "pdf":
					element.html('<i class="fa fa-file-pdf-o"></i>');
					break;
				default:
					element.html('<i class="fa fa-file-text-o"></i>');
					break;
			}
			element.wrap('<a target="_blank" href="' + images[0] + '"></a>');
		}							
		if(images.length > 1) {
			for(i=1;i<=(images.length)-1;i++) {
				
				var extension = images[i].split("?")[0].substr( (images[i].split("?")[0].lastIndexOf('.') +1) );
				if($.inArray(extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
					element.parent().parent().append('<a target="_blank" class="appendedImages" href="' + images[i].split("?")[0] + '"><div style="background-image: url(' + images[i] + ')" class="uploaded-file" data-type="file"></div></a>');
				} else {
					switch (extension) {
						case "pdf":
							var appendedHtml = '<i class="fa fa-file-pdf-o"></i>';
							break;
						default:
							var appendedHtml = '<i class="fa fa-file-text-o"></i>';
							break;
					}
					element.parent().parent().append('<a target="_blank" class="appendedImages" href="' + images[i] + '"><div class="uploaded-file" data-type="file">' + appendedHtml +'</div></a>');
				}									
			}								
		}		
	}




</script>
<script src="{{ asset('la-assets/stdentsform/js/main_view.js') }}"></script>
@endpush

