@extends("la.layouts.app")

@section("contentheader_title", "Nurses")
@section("contentheader_description", "Nurses listing")
@section("section", "Nurses")
@section("sub_section", "Listing")
@section("htmlheader_title", "Nurses Listing")

@section("headerElems")
@la_access("Employees", "create")
	<a href="<?php echo url(config("laraadmin.adminRoute") . '/nurses/0/edit'); ?>" class="btn btn-success btn-sm pull-right addbtn" > ✛ Add Nurses</a>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('status_success'))
	<div class="alert alert-success">{{ session('status_success') }}</div>
@endif

<!-- <div class="box box-success">
	<div class="box-body"> -->
		<table id="mx-sudents-lists" class="table mg-table mgt2 ">
			<thead>
			<tr class="success">
				<th>No:</th>
				<th>Name</th>
				<th>Email</th>
				<!--<th>Date-of-birth</th>-->
				<th>Status</th>
				<th>Assignee</th>
				@if($show_actions)
					<th>Actions</th>
				@endif
			</tr>
			</thead>
			<tbody></tbody>
		</table>
	<!-- </div>
</div> -->

<!-- common modals -->
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Assign a consultant</h4>
			</div>
			<!-- <form action="{{ url(config('laraadmin.adminRoute') . '/students_assign') }}" id="assign-stud-form" method="post"> -->
			<form action="{{ url(config('laraadmin.adminRoute') . '/nurse_assign') }}" id="assign-stud-form" method="post">
			{{ csrf_field() }}
			<div class="modal-body">
				<div class="box-body">					
					<?php
					if(count($consultants) > 0) {
					?>
						<input type="hidden" value="" name="assign_user_id" id="assign_user_id">
						<input type="hidden" value="" name="assign_email" id="assign_email">

						<input type="hidden" value="" name="nurse_name" id="nurse_name">
						<input type="hidden" value="" name="staff_name" id="staff_name">
						<div class="form-group">
							<label for="assigned_to">Select consultant * :</label>
							<select class="form-control nurse-assign-select" name="assigned_to" required>
								<option value="">Select consultant</option>
								<?php
								foreach($consultants as $cons) {
								?>
									<option value="<?php echo $cons->id; ?>" data-name="<?php echo $cons->name; ?>"> <?php echo $cons->name; ?></option>
								<?php
								}
								?>
							</select>
						</div>
					<?php
					} else {
						echo '<h3>No consultants to assign</h3>';
					}
					?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="universityUserModal" role="dialog" aria-labelledby="universityModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="universityModalLabel">Assign a Employer</h4>
			</div>
			<form action="{{ url(config('laraadmin.adminRoute') . '/nurses_unv_assign') }}" id="assign-univerUser-form" method="post">
			{{ csrf_field() }}
			<div class="modal-body">
				<div class="box-body">					
					<?php
					if(count($universityAgents) > 0) {
					?>
						<input type="hidden" value="" name="assign_user_id" id="assign_unv_user_id">
						<input type="hidden" value="" name="assign_email" id="assign_unv_email">
						<div class="form-group">
							<label for="assigned_to">Select Employer * :</label>
							<select class="form-control" name="assigned_to" required>
								<option value="">Select</option>
								<?php
								foreach($universityAgents as $unv) {
								?>
									<option value="<?php echo $unv->id; ?>"> <?php echo $unv->name; ?></option>
								<?php
								}
								?>
							</select>
						</div>
					<?php
					} else {
						echo '<h3>No Employers to assign</h3>';
					}
					?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			</form>
		</div>
	</div>
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {
	$("#mx-sudents-lists").DataTable({
		processing: true,
        serverSide: false,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/nurses_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		columnDefs: [ { orderable: false, targets: [-1] }],
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	
	$("#assign-stud-form").validate();
	$("#assign-univerUser-form").validate();

	$('body').off('click', '.stud-assign');
	$('body').on('click', '.stud-assign', function() {
		$("#assign_user_id").val($(this).attr("data-user"));
		$("#assign_email").val($(this).attr("data-email"));

		$("#nurse_name").val($(this).attr("data-nurse"));

	});
	$('body').off('click', '.stud-assign-university');
	$('body').on('click', '.stud-assign-university', function() {
		$("#assign_unv_user_id").val($(this).attr("data-user"));
		$("#assign_unv_email").val($(this).attr("data-email"));
	});

	$('body').off('change', '.nurse-assign-select');
	$('body').on('change', '.nurse-assign-select', function() {
		$("#staff_name").val($(this).find('option:selected').attr('data-name'));
	});

});
</script>
@endpush
