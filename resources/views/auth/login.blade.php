@extends('la.layouts.auth')

@section('htmlheader_title')
    Log in
@endsection

@section('content')
<body class="hold-transition login-page mg-login ">
    <div class="login-banner">
        <span>
            <h3>
                STUDY. <br>
                WORK. <br>
                LIVE.
            </h3>
        </span>
    </div>
    <div class="login-box">
        <img class="prop"  src="{{ asset('la-assets/img/prop1.png') }}" alt="">
        <div class="login-wrap">
            
        <div class="login-logo">
            <a href="{{ url('/home') }}"><img src="{{asset('la-assets/img/Zodocs_logo.png')}}" alt=""></a>
        </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (session('status'))
  <div class="alert alert-success">
    {{ session('status') }}
  </div>
@endif
@if (session('warning'))
  <div class="alert alert-warning">
    {{ session('warning') }}
  </div>
@endif

@if (session('loginfail'))
  <div class="alert alert-danger mt20">
    {{ session('loginfail') }}
  </div>
@endif

    <div class="login-box-body">
    <p class="login-box-msg">Welcome back to Zodocs</p>
    <p class="loginp2">Sign In to Explore</p>

    <form action="{{ url('/login') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="pathvalue" value="sitelogin">
        <div class="form-group has-feedback">
            <label for="email">Email</label>
            <input type="email" class="form-control" placeholder="" name="email"/>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
        <label for="password">Password</label>
            <input type="password" class="form-control" placeholder="" name="password" id="pwd"/>
            <!-- <span class="glyphicon glyphicon-lock form-control-feedback"></span> -->
            <span toggle="#pwd" class="fa fa-fw fa-eye field-icon toggle-password"></span>
        </div>
        <div class="row">
        <div class="col-xs-6 pr5">
            <button type="submit" class="btn btn-primary btn-block btn-flat loginbtn">Sign In</button>

            </div>
            <div class="col-xs-6 pl5">
            <a  href="{{ url('/registration') }}" class="btn btn-primary btn-block btn-flat regbtn " target="_blank">Register</a>

            </div>
            <!-- <div class="col-xs-6">
                <div class="checkbox icheck">
                    <label >
                        <input type="checkbox" name="remember"> Remember Me
                    </label>
                </div>
            </div> -->
            <div class="col-xs-12 rstlink"  >
          Forgot Password ? <a class="" href="{{ url('/password/reset') }}">Reset Password</a>

            </div>


        </div>
    </form>

    @include('auth.partials.social_login')

	
    <!--<a href="{{ url('/register') }}" class="text-center">Register a new membership</a>-->

</div><!-- /.login-box-body -->
</div>

</div><!-- /.login-box -->

    @include('la.layouts.partials.scripts_auth')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });


            $(".toggle-password").click(function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
                    var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                    input.attr("type", "text");
            } else {
                    input.attr("type", "password");
            }
            });


        });
    </script>
</body>

@endsection
