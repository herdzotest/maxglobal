@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/settings/timetracking') }}">Time tracking settings </a> :
@endsection
@section("contentheader_description")
@section("section", "Time tracking settings")
@section("section_url", url(config('laraadmin.adminRoute') . '/settings/timetracking'))

@section("htmlheader_title", "Time tracking settings")

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{{ Form::open(array('url' => route('timetracking.post.create'), 'id' => 'timetrack-edit-form')) }}
				
					<input name="_method" type="hidden" value="PUT">
					<input name="id" type="hidden" value="<?php echo (count($timeSettings) > 0) ? $timeSettings->id : '0' ?>">
					
					<div class="form-group">
						<label for="name">Status* :</label>
						<select class="form-control" name="status">
							<option value="ANYTIME" <?php echo (count($timeSettings) > 0 && $timeSettings->status == 'ANYTIME') ? 'selected' : '' ?> >All Days</option>
							<!--<option value="EVERYDAY" <?php //echo (count($timeSettings) > 0 && $timeSettings->status == 'EVERYDAY') ? 'selected' : '' ?> >EVERYDAY (Sunday - Saturday)</option>-->
							<option value="OFFICE-TIME" <?php echo (count($timeSettings) > 0 && $timeSettings->status == 'OFFICE-TIME') ? 'selected' : '' ?> >Week Days (Monday - Friday)</option>
						</select>
					</div>
					<div class="form-group">
						<label for="name">From-time* :</label>
						<input placeholder="Enter from-time" value="<?php echo (count($timeSettings) > 0) ? $timeSettings->from_time : ''; ?>" type="text" name="input_starttime" id="input_starttime" class="form-control timepicker" required>
					</div>
					<div class="form-group">
						<label for="name">To-time* :</label>
						<input placeholder="Enter end-time" value="<?php echo (count($timeSettings) > 0) ? $timeSettings->to_time : '' ?>" type="text" name="input_endtime" id="input_endtime" class="form-control timepicker" required>
					</div>
					<div class="form-group">
						<input class="btn btn-success mgbtn" type="submit" value="Update">
						<button class="btn btn-default pull-right">
							<a class="mgbtn2" href="{{ url(config('laraadmin.adminRoute') . '/settings/timetracking') }}">Cancel</a>
						</button>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
    $('.timepicker').datetimepicker({
        format: 'hh:mm A',
    }); 
	$("#timetrack-edit-form").validate({
		
	});
});
</script>
@endpush
