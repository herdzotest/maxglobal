<div id="tab_6" class="tab-pane " >
        
<div class="content-head">
                                                <h4>Master Degree Details</h4>
                                                <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                                </div>
                                {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'masters-add-form']) !!}
               
                                <div class="pane">

                                        <div class="row">
                                                <div class=" col s12 ">


                                                        <p>
                                                                Have you completed your Master's Degree?
                                                        </p>
                                                        <p>
                                                                        <label>
                                                                                        <input name="pgdone" type="radio" value="1" />
                                                                                        <span>Yes</span>
                                                                                      </label>
                                                                                      <label>
                                                                                                <input name="pgdone" type="radio"  value="0" />
                                                                                                <span>No</span>
                                                                                              </label>
                                                                      </p>
                                                                      <br>








                                                </div>

                                        </div>
                                        <div id="pg-details" style="display: none">
                                        <div class="row">
                                                        <div class=" col s12 ">
                                                                        <div class="input-field">
                                                                                        <!-- <i class="material-icons prefix">account_circle</i> -->
                                                                                        <input id="univ_name" name="univ_name" type="text" class="validate">
                                                                                        <label for="univ_name">University Name</label>
                                                                                        <!-- <span class="helper-text" data-error="wrong"
                                                                                        data-success="right">As per Passport</span> -->
                                                                        </div>
                                                        </div>
                                        </div>
                                        <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="pg-start-year" name="pg-start-year" type="text" class="validate">
                                                                        <label for="pg-start-year">Start Year</label>
                                                                        <!-- <span class="helper-text" data-error="wrong"
                                                                        data-success="right">As per Passport</span> -->
                                                                </div>
                                                </div>

                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="pg-comp-year" name="pg-comp-year" type="text" class="validate">
                                                                                <label for="pg-comp-year">Completion Year</label>

                                                                        </div>
                                                </div>


                                        </div>
                                        <div class="row">
                                                        <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="pgmedium" name="pgmedium" type="text" class="validate">
                                                                                <label for="pgmedium">Medium of Instruction</label>

                                                                        </div>
                                                        </div>
        
                                                        <div class="col s6">
                                                                        <div class="input-field ">
                                                                                        <input id="pg-grade" name="pg-grade" type="text" class="validate">
                                                                                        <label for="pg-grade">Percentage / Grade Obtained</label>
        
                                                                                </div>
                                                        </div>

                                                        <div class="col s6">
                                                                        <div class="input-field ">
                                                                                        <input id="pg-englishmark" name="pg-englishmark" type="text" class="validate">
                                                                                        <label for="pg-englishmark">Marks Obtained in English</label>
        
                                                                                </div>
                                                        </div>
        
        
                                                </div>
                                        <br>
                                        <div class="row">
                                                <div class="col s6">
                                                        <label>Upload Master Degree Documents</label>
                                                        <div class="file-field input-field">
                                                                        <div class="btn">
                                                                                <span>Upload</span>
                                                                                <input type="file" multiple > <i class="fa fa-cloud-upload"></i>
                                                                        </div>
                                                                        <div class="file-path-wrapper">
                                                                                <input class="file-path validate" name="master_doc" type="text" placeholder = "Upload file" >
                                                                        </div>
                                                        </div>
														<div class="uploaded-file" id="master_doc" data-type="file"></div>
                                                </div>


                                                      </div>


                                                </div>


                                </div>
                                <div class="pane2">
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
                                                                        <select name="country_pref">
                                                                                <option value="" disabled selected>select Preferred Country </option>
                                                                                <option value="1">UK  </option>
                                                                                <option value="2">USA </option>
                                                                                <option value="3">CANADA</option>
                                                                                <option value="4">AUSTRALIA</option>
                                                                                <option value="5">NEWZEALAND</option>


                                                                        </select>
                                                                        <label for="">Country Preferred</label>
                                                                </div>
        
        
                                                        </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
                                                                        <select name="course_pref">
                                                                                <option value="" disabled selected>select Preferred Course </option>
                                                                                <option value="1">Course 1 </option>
                                                                                <option value="2">Course 2 </option>
                                                                                <option value="3">Course 3</option>
                                                                                <option value="4">Course 4</option>
                                                                                <option value="5">Other</option>
                                                                        </select>
                                                                        <label for="">Course Preferred</label>
                                                                </div>
                                                        <div id="course-intsd" style="display:none">
                                                                <div class="input-field ">
                                                                        <input id="" type="text" class="validate">
                                                                        <label for="">Specify Your Preferred Course</label>

                                                                </div>
                                                        </div>
        
                                                        </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
                                                                        <select name="university_pref">
                                                                                <option value="" disabled selected>select Preferred University </option>
                                                                                <option value="1">University 1 </option>
                                                                                <option value="2">University 2 </option>
                                                                                <option value="3">University 3</option>
                                                                                <option value="4">University 4</option>
                                                                                <option value="5">Other</option>
                                                                        </select>
                                                                        <label for="">University Preferred</label>
                                                                </div>
                                                        <div id="uni-intsd" style="display:none">
                                                                <div class="input-field ">
                                                                        <input id="" type="text" class="validate">
                                                                        <label for="">Specify Your Preferred University</label>

                                                                </div>
                                                        </div>
        
                                                        </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
                                                                        <input id="" type="text" class="datepicker">
                                                                        <label for="">Course Preferred Intake</label>
                                                                </div>
                                                        </div>  
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                <label for="">Letter of Recommendation File Upload</label>
                                                                <div class="file-field input-field">
                                                                        <div class="btn">
                                                                                <span>Upload</span>
                                                                                <input type="file" multiple > <i class="fa fa-cloud-upload"></i>
                                                                        </div>
                                                                        <div class="file-path-wrapper">
                                                                                <input class="file-path validate"  name="letter_url" type="text" placeholder = "Upload file" >
                                                                        </div>
                                                                </div>
																<div class="uploaded-file" id="letter_url" data-type="file"></div>
                                                        </div>  
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                        <label for="">Statement of Purpose File Upload</label>

                                                                <div class="file-field input-field">
                                                                        <div class="btn">
                                                                                <span>Upload</span>
                                                                                <input type="file" multiple > <i class="fa fa-cloud-upload"></i>
                                                                        </div>
                                                                        <div class="file-path-wrapper">
                                                                                <input class="file-path validate" name="statement_url" type="text" placeholder = "Upload file" >
                                                                        </div>
                                                                </div>
																<div class="uploaded-file" id="statement_url" data-type="file"></div>
                                                        </div>  
                                                </div>


                                </div>
                                <div class="controls">
                                                <a class="mgbtn2">Back</a>
                                                <input id="form_name" type="hidden" class="validate" name="type_of" value="masters">
                                                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                                                {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
                        

</div>

                