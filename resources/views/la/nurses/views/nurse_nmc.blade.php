<div id="tab_7" class="tab-pane " >
        
<div class="content-head">
                                                <h4>NMC Details</h4>
                                                <!-- <div class="id">Nurse ID : <b>#12675</b></div> -->
                                </div>
                                <div class="pane">
									<div class="academic">
										 <div class="mgv-field">
												<div class="name">
													NMC Registration Status 
												</div>
												<div class="value" id="nmc_appl_status"></div>
										 </div>
										 <div class="mgv-field">
												<div class="name">
													Supporting declaration about your health from your medical practitioner/occupation health
												</div>
												<div class="value">
													<span id="declaration_name"></span><br/>
													<span id="declaration_org"></span><br/>
													<span id="declaration_addr"></span><br/>
													<span id="declaration_contact"></span><br/>
													<span id="declaration_email"></span>
												</div>
										 </div>
										 <div class="mgv-field">
												<div class="name">
												Police clearance certificates /DBS certificate(Scope of practice)
												</div>
												<div class="value">
													<span id="scope_country"></span><br/>
													<span id="scope_council"></span><br/>
													<div class="uploaded-file" id="scope_file_upload" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
													<div class="file-approve-buttons">
														<!-- <a href="#" class="btn btn-danger mx-approve-doc" data-type="pcc_file_upload" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
														<a href="#" class="btn btn-success mx-approve-doc" data-type="pcc_file_upload" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a> -->
														<a href="#" class="btn btn-danger mx-approve-doc" data-type="scope_file_upload" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
														<a href="#" class="btn btn-success mx-approve-doc" data-type="scope_file_upload" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
													</div>  	
												</div>
										 </div>	
										 
										 <div class="mgv-field">
												<div class="name">
													Have You Received Letter of invitation?
												</div>
												<div class="value" id="letter_invitation">
												</div>
										 </div>
										 <div class="mgv-field">
												<div class="name">
													CBT Attempt Date
												</div>
												<div class="value" id="cbt_attempt_date">
												</div>
										 </div>
										 <div class="mgv-field">
												<div class="name">
													CBT Attempt Status
												</div>
												<div class="value" id="cbt_attempt_status">
												</div>
										 </div>
										 <div class="mgv-field">
												<div class="name">
													CBT Passed Date
												</div>
												<div class="value" id="cbt_passed_date">
												</div>
										 </div>
										 <div class="mgv-field">
												<div class="name">
													CBT Number of Attempts
												</div>
												<div class="value" id="cbt_attempts">
												</div>
										 </div>
										 <div class="mgv-field">
												<div class="name">
													CBT Score
												</div>
											<div class="value">
												<div class="uploaded-file" id="cbt_upload" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
												<div class="file-approve-buttons">
													<!-- <a href="#" class="btn btn-danger mx-approve-doc" data-type="cbt_score_upload" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
													<a href="#" class="btn btn-success mx-approve-doc" data-type="cbt_score_upload" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a> -->
													<a href="#" class="btn btn-danger mx-approve-doc" data-type="cbt_upload" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
													<a href="#" class="btn btn-success mx-approve-doc" data-type="cbt_upload" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
												</div> 
											</div>
										 </div>
										 <div class="mgv-field">
												<div class="name">
													CBT validity
												</div>
												<div class="value" id="cbt_validity">
												</div>
										 </div>
										 <div class="mgv-field">
												<div class="name">
													NMC Full Application Submission Date
												</div>
												<div class="value" id="nmc_appl_date">
												</div>
										 </div>
										 <div class="mgv-field">
												<div class="name">
													NMC status screenshot/decision letter
												</div>
											<div class="value">
												<div class="uploaded-file" id="nmc_file_upload" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
												<div class="file-approve-buttons">
													<!-- <a href="#" class="btn btn-danger mx-approve-doc" data-type="nmc_file" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
													<a href="#" class="btn btn-success mx-approve-doc" data-type="nmc_file" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a> -->
													<a href="#" class="btn btn-danger mx-approve-doc" data-type="nmc_file_upload" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
													<a href="#" class="btn btn-success mx-approve-doc" data-type="nmc_file_upload" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
												</div>
											</div>
										 </div>
										 <div class="mgv-field">
												<div class="name">
													Birth Certificate
												</div>
											<div class="value">
												<div class="uploaded-file" id="birth_cert_upload" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
												<div class="file-approve-buttons">
													<!-- <a href="#" class="btn btn-danger mx-approve-doc" data-type="birth_certificate" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
													<a href="#" class="btn btn-success mx-approve-doc" data-type="birth_certificate" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a> -->
													<a href="#" class="btn btn-danger mx-approve-doc" data-type="birth_cert_upload" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
													<a href="#" class="btn btn-success mx-approve-doc" data-type="birth_cert_upload" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
												</div>
											</div>
										 </div>										 
									</div> 
                                </div>
</div>

                