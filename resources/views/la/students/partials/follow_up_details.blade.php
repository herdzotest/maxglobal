<div id="tab_13" class="tab-pane" >                 
	<div class="content-head">
                                        
                                        <h4>Add Follow Up</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                        </div>  
                        {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'personal-add-form']) !!}
                        <!-- {!! Form::open() !!} -->
                <div class="pane">
		        <div id="follow_up-wrapper">
				<div class="stdhst">
					<div class="index">1</div>
						<div class="col-md-6">
							<div class="mg-input-field ">
								<label for="follow_date">Date for next follow up</label>
                                                                <input id="follow_date" name="follow_date" type="text" class="futuredatepicker">
							</div>
						</div> 
						<div class="col-md-6">
							<div class="mg-input-field ">
								<label for="test_time">Time for next follow up</label>
                                                                <input id="follow_time" name="follow_time" type="text" class="timepicker">							
							</div>
						</div>
						<div class="col-md-12">
							<div class="mg-input-field ">
								<label for="prefix">Representative Note</label>
							        <textarea id="representative_note" name="representative_note" class="materialize-textarea" maxlength="500"></textarea>	
							</div>
						</div>
						<div class="clearfix"></div>
				</div>
			</div>
			<input type="hidden" value="follow_up" name="formtype" class="formtype">
			<div class="btn addshbtn"> <span class="glyphicon glyphicon-plus"></span></div>						
                </div>
                <input type="hidden" value="<?php echo $wrp_count_follow_up;  ?>" class="wrapper-count">						
                <div class="controls">
                        <a class="mgbtn2 back_button">Back</a>
                        <input id="form_name" type="hidden" class="validate" name="type_of" value="follow_up">
                        <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                        {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                </div>
                {{ Form::close() }}    
</div> 