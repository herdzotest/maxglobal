<div id="tab_4" class="tab-pane " >
                                
                                <div class="content-head">
                                                <h4>Section 4</h4>
                                                <!-- <div class="id">Nurse ID : <b>#12675</b></div> -->
                                        </div>
                                        {!! Form::open(['action' => 'LA\NursesController@store', 'id' => 'agentselect-add-form']) !!}
               
                        <div class="pane">
                                
                                <div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
														<select name="main_agent" id="main_agent">
															<option value="">Select</option>
														</select>
														<label for="">Employer/ Main Agent </label>
                                                </div>


                                        </div>
                                        <div class=" col s6 ">
                                                <div class="input-field">
														<select name="work_site" id="work_site">
															<option value="">Select</option>
															<option value="care_home">Care Home</option>
															<option value="hospitals">Hospitals</option>
														</select>
														<label for="">Work Site </label>
                                                </div>


                                        </div>
                                </div>

                                <div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
														<select name="mode_interview" id="mode_interview">
															<option value="NOT">Not Applicable</option>
															<option value="skype">Skype</option>
															<option value="face-face">Face-to-face</option>
														</select>
														<label for="mode_interview">Mode of Interview  </label>
                                                </div>


                                        </div>
										
										<div class="col s6">
												<select name="interview_status" id="interview_status">
													<option value="Not"> Not Applicable</option>
													<option value="pass">Passed</option>
													<option value="failed">Failed</option>
												</select>
												<label for="">Interview status  </label>
										</div>	
                                </div>
								
								<div id="offer-letter-wrapper" style="display:none;">
									<div class="row">
										<div class="col s6">
											<div class="input-field">
													<input name="offer_letter_date"  required id="offer_letter_date" type="text">												
													<label for="offer_letter_date">Offer Letter Received Date  </label>
											</div>
										</div>
										<div class=" col s6 ">
												<div class="input-field">
														<input name="offer_letter_signed" class="datepicker" required id="offer_letter_signed" type="text">												
														<label for="offer_letter_signed">Offer Letter Signed Date   </label>
												</div>
										</div>
									</div>
									
								 <div class="row">
											<div class="col s12">
													<label>Offer Letter File Upload   </label>
													<div class="file-field input-field">
																			<div class="btn">
																					<span>Upload</span>
																					<input type="file" name="offer_letter_file"> <i class="fa fa-cloud-upload"></i> 
																			</div>
																	<div class="file-path-wrapper">
																			<input class="file-path validate">

																			<div>
																			</div>
															
																	</div>
													</div>

													<div class="uploaded-file" id="offer_letter_file" data-type="file"></div>
											</div>											
									</div>
								</div>								
								
                                <div class="row">
                                                <div class="col s6">
													<div class="input-field">
															<input name="interview_schedule_time"  required id="interview_schedule_time" type="text">												
															<label for="interview_schedule_time">Interview Schedule Time  </label>
													</div>
                                                </div>
												<div class=" col s6 ">
														<div class="input-field">
																<input name="interview_schedule_date" class="datepicker" required id="interview_schedule_date" type="text">												
																<label for="interview_schedule_date">Interview Schedule Date  </label>
														</div>
												</div>
                                        </div>

                        </div>
                        <div class="controls">
                                        <a class="mgbtn2">Back</a>
                                        <input id="form_name" type="hidden" class="validate" name="type_of" value="agent_info">
                                        <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                        {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
                </div>

                