@extends("la.layouts.app")

@section("contentheader_title", "Compose mail")
@section("section", "Compose mail")
@section("htmlheader_title", "Compose mail")

@section("main-content")

<div class="box box-success">
	<div class="box-header"></div>
	<div class="box-body">
		@if(Session::has('success_mail_message'))
			<p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success_mail_message') }}</p>
		@endif
		<div class="row">
			<div class="col-md-12 ">
				{{ Form::open(array('url' => route('mailsettings.sendmail'),'id' => 'compose-mail')) }}
					
					<input name="_method" type="hidden" value="PUT">
					<div class="form-group">
						<label for="reciepents">Select receipients* :</label>
						<?php $emailSelected = (isset($_GET['email'])) ? $_GET['email'] : '' ;?>
						<select id="reciepents" class="form-control" data-placeholder="Email to:" multiple rel="select2" name="reciepents[]" required>
							<?php foreach($users as $result) { 
								$optionValues = $result->user_id.'_'.$result->email;
							?>
								<option <?php echo ($emailSelected == $optionValues) ? 'selected' : ''; ?> value="<?php echo $optionValues; ?>"><?php echo $result->user_name; ?></option>
							<?php } ?>
						</select>
						<input type="hidden" id="receipients-flag" value="1">
					</div>
					<div class="form-group">
						<label for="subject">Subject :</label>
						<input type="text" class="form-control" name="subject" placeholder="Subject">
					</div>
					<div class="form-group">
						<label for="mail-content">Mail-content* :</label>
						<textarea name="mail_content" class="form-control" id="mail-content" placeholder="Message" required></textarea>
					</div>
					<div class="form-group">
						<button type="submit" class="pull-right btn mgbtn" id="sendEmail">Send <i class="fa fa-arrow-circle-right"></i></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script src="{{ asset('la-assets/plugins/ckeditor/ckeditor.js') }}"></script>
<script>
$(function () {
	CKEDITOR.replace('mail-content');
	CKEDITOR.config.removePlugins = 'image,about';
	
	$("#compose-mail").validate();

	$("#reciepents").change(function(e) {
		if ($("#reciepents option:selected").length > 5) {
			
			var value = $(this).val()[1];
			$('#reciepents').find('option[value="'+value+'"]').removeAttr("selected");
			if($("#receipients-flag").val() == '1') {
				$("#receipients-flag").val(0);
				$("#reciepents").select2();
			}
		}
	});
});
</script>
@endpush
