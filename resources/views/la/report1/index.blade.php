@extends("la.layouts.app")

@section("contentheader_title", "Report")
@section("contentheader_description", "Report listing")
@section("section", "Report")
@section("sub_section", "Listing")
@section("htmlheader_title", "Report Listing")

@section("headerElems")
@la_access("Employees", "create")
	<!-- <button class="btn btn-success btn-sm pull-right addbtn dataExport"> Export to Excel</button> -->
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<style type="text/css">
#mx-sudents-lists > tfoot > tr > th {
   border-top: 3px solid #ccc3c3;
}
#example2 > tfoot > tr > th {
   border-top: 3px solid #ccc3c3;
}
#example3 > tfoot > tr > th {
   border-top: 3px solid #ccc3c3;
}
</style>
<div class="reportfilter">
	<div class="row">
		<div class="col-md-9">
		<div class="clearfix">
	<div class="col-md-4">
		<!-- <div class="input-group date">
			<input class="form-control" placeholder="From Date" id="from_date" name="from_date" type="text" value="" required="true"  ><span class="input-group-addon" style="background-color: #d2d6de;"><span class="fa fa-calendar"></span></span>
		</div> -->
		<label for="start_date">Start Date</label>
		<input class="form-control dateclass placeholderclass" placeholder="Select Date" id="from_date" name="from_date" type="date" value="" required="true"   max="<?= date('Y-m-d'); ?>" onClick="$(this).removeClass('placeholderclass')">
	</div>
	<div class="col-md-4">
		<!-- <div class="input-group date">
			<input class="form-control" placeholder="To Date" id="to_date" name="to_date" type="text" value="" required="true"  ><span class="input-group-addon" style="background-color: #d2d6de;"><span class="fa fa-calendar"></span></span>
		</div> -->
		<label for="end_date">End Date</label>
		<input class="form-control dateclass placeholderclass" placeholder="Select Date" id="to_date" name="to_date" type="date" value="" required="true"   max="<?= date('Y-m-d'); ?>"  onClick="$(this).removeClass('placeholderclass')">
	</div>
	<div class="col-md-4">
		<label for="agent">Agent</label>
		<div class="form-group">
			<select name="agent_id" id="agent_id" class="form-control"   onchange="getSales(this);">
				<option value="">Select Agent</option>
				<?php
					foreach($agents as $agentsData) {
						echo '<option id="sale_agnt" value="'.$agentsData->id.'">'.$agentsData->name.'</option>';
					}
				?>
			</select>
		</div>
	</div>
	<!-- <div class="col-md-4">
		<label for="agent_sales">Agent Sales</label>
		<div class="form-group">
			<select name="sales" id="sales" class="form-control"  >
				<option value="">Select Sale</option>
			</select>
		</div>
	</div>
	<div class="col-md-4">
		<label for="type">Type</label>
		<div class="form-group">
			<select name="rtype" id="rtype" class="form-control"   onchange="showStatus(this);">
				<option value="Sales">Sales</option>
				<option value="Payment">Payment</option>
				<option value="Expense">Expense</option>
			</select>
		</div>
	</div>
	<div class="col-md-4" id="pay_status">
		<label for="payment_status">Payment Status</label>
		<div class="form-group">
			<select name="rstatus" id="rstatus" class="form-control"  >
				<option value="">Select Status</option>
				<option value="Paid">Paid</option>
				<option value="Unpaid">Unpaid</option>
				<option value="Partly Paid">Partly Paid</option>
				<option value="Over Paid">Over Paid</option>
			</select>
		</div>
	</div> -->


</div>
		</div>

		<div class="col-md-3">
		

		<div class="col-md-8 pl0"><label for="search" style="color: rgba(0, 0, 0, 0);">Search</label><div class="form-group">
		<button class="btn btn-success btn-sm pull-right searchbtn"><span class="fa fa-search"></span> Search</button>
	</div>
    </div>

    <div class="col-md-4 pl0"><label for="clear" style="color: rgba(0, 0, 0, 0);">Clear</label><div class="form-group">
		<button class="btn btn-success btn-sm pull-right clearbtn"><span class="fa fa-times"></span> Clear</button>
	</div>
	</div>

		</div>
	</div>

</div>
<br>

<!-- <div class="box box-success">  
	<div class="box-body"> -->
		<table id="mx-sudents-lists" class="table table-hover mg-table display mgt2">
		<thead>
			<tr class="success">
				<th>Name</th>
				<th>Email</th>
				@if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER"]))
					<th>Assignee</th>
				@endif
				
				<th>Status</th>
				
			</tr>
		</thead>
		<tbody></tbody>
		</table>

	<!-- </div>
</div> -->

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>


<!-- Search -->
<script src="{{ asset('la-assets/stdentsform/js/main.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<!-- Search -->





<!-- Export to excel -->
<script src="{{ asset('la-assets/js/tableexports/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('la-assets/js/tableexports/jszip.min.js') }}"></script>
<script src="{{ asset('la-assets/js/tableexports/pdfmake.min.js') }}"></script>
<script src="{{ asset('la-assets/js/tableexports/vfs_fonts.js') }}"></script>
<script src="{{ asset('la-assets/js/tableexports/buttons.html5.min.js') }}"></script>
<!-- End Export to excel -->

<script>
$(function () {

	var logo ="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAABh7SURBVHgB7d0JfBRF1gDwV3MkkwAGgoAghyAssgjIoaKsIoqLoKuwH4rrscK3gogg97EcooAshxxCwqWAIIKIIq6iAnLfd0hCLkJOQu47mbO7a6sSoj1hQhLI0VPz/r9fa/J6gGRm3ryq193VAAghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEkGchgDSFUuoTmQ3trpnllnlWaJhjV/zNMvWxKeBlMOhsjUwkpW19feTD/hBCCMkHVK0wQTQgIot2PnHdPvBsqqNvUiFtA4TKmTbwT7GSumlWCvkSLX6lVJtOB/BQQ13i31rod7zZzmdj23okFFCVwwSpJaxSGH+Jl9/8Ico2IdkstwZC7DIFJbEQ/GMKKBRIoEqImxOkdLx3E0PI1M6mDwbca9gFqMpggtQwlhj6n2Mcw78Nt0/PtNPm7BUgCoA9Jh+8ovMo2Nl7/laJUF68Y31dyryuvqMHtTB8B+iOYYLUoKs5tMeys+bNCXlKh6IAe/bZUAqCMgEKZahUIpQXf66p4eTGx+u+1tSHxAG6bZggNWRHhGPq1lDbxywP9PwNzAtFeA5LmjxWVe4gEW4V9/MisK5n3TeHtDBsAXRbMEGqGRtSeS8+bd10LFEeUhRgz7jM0uNMKkC6lX2vgztOhPLisx/0WfJRJ9MkQJWGCVKNWHKYZh+y/RSULj1T8kyz+QacTqOQboHiZ78GEoRvo/9k2hTQzWcooErRAaoWfDK+8Ljlq6A0lhw3KOx9W1Q5zFDjAq5Y3xp/ybIaUKVgglSTDZccC08kKX9Xx4LSAdJqITlKLIu0jAy8ah0LqMIwQarB2ST67A8RjonqWCI75n2tAGrduCDz8uAc2h1QhWCCVDE2tLpr1TnLZnXMylpXlzNBExxsEvTG2bzv2M/pBahcmCBVLPCcbXGmmd6jjoVksCOBMmhGcK7cal6kdSagcmGCVKErmbTnb1flEepYKptzJGtgaFXafyKss+IttA2gW8IEqSJsyEICzlo/552qEvxrXj20yCxTmBJkXgXoljBBqsjROHlIbJbSUR2LZ0fJzQ7QrO3Xbf3OZNLHAZUJE6QK8KPlmy85FqljfDIclQWaNzE0LwBQmTBBqsC2YGl6WqHSQh2LYF0rm4Ym5mU5miF3/TrR9joglzBB7lCWmbbcGW6foo4VsGFVXB64jSkhljWsCvoCugkmyB1afdq+grVwTepYeCYfdoHbSLAodaeFWuYDugkmyB24mEyfOp0kvaSOZVi02dYtz5Ir1rExVtoekBNMkDvwVZB1sfp7XjRC08Et8cveZwQVLALkBBPkNh2Ikf4ZlaH0UMdicgDybOC2tl93vHgC275OMEFuA5vQNthwzr5cHeMdq0iNnG91u/iBzZHn87bzU/UBFcEEuQ2rz9g+zrPRBuoYTw5JAbcXnC83/yzOMRxQEUyQSorJop33RMrvqmP59uKj5qL4IMwcwKpIXUCYIJW15aJ9QekObmiae7V1y5NiVfQfhFlnAMIEqYygJPrXc9ek/upYUj6/hFa8S/sXRVmnRebRB8DDYYJUEBtyGFadtq5Xx/icIzRdzHUvbGzG/m5QwVbwcJggFbQ7UhmRkk+bq2OxOQRsEgjrYKTUdd9V+jx4MEyQCmDVo972INscdYwnRrQbnK17u4gVQB8HMGWfdRW/1gU8FCZIBXx22jE31woN1bEwNrRyCNDWLYv3ZQdIRi+4mKq0XHtBGQUeChOkHNfzaYdfIiWnpXKy2adrokBt3dK8EiSQpT/WdJh50BaQS6k/eCBMkHIEHLGtlVWVgndzg1PFHXHorBT0MQo4fP5IkAwLhSm7HSvAA2GC3MLxWPpSaJryhDqWmAuQawVhmYJsYPf1uSm+Lsjxekg67QEeBhOkDPwGN19esC1Vx3hbNzJD3OphZEMrWqAH2dt40z5eOcfvta0FD4MJUoZvg6XJ13MVp2VxrmQCWARt6xIbBe9wB9jql31h4W9xcrddkXQQeBBMEBcKC2mzHZccTgurWRwAV7PErR6mkOKulWK49Ym8E/dbPvOkVRkxQVwIOOVYZnWA00A8nLV1FYHOt1LjQyt9qgz2eqZyH3s1R2m47JzkMQtgY4KUEpJKHz8eK7+ijvG27jVB27rETIuOeTjqmMqtHiXmHrMv8pS2LyZIKVvO2JeUjoUK3Nb1DmMTc3ag3F63/OpRIpu1gmftdywED4AJonIkhg4OT1V6qmPxOewNYQEhGZIU0KcoLDl8gOoq9yGw6oL97fPp4t9GARPkBjbx9Pn8hDVQHXPIAGFpYlYPwg7+eYWw6qHXseGVN1QWb3mP/NnyjejnaWGC3LD1nDQ1xwKN1bEodszD4QarI1YaazZ4B8lAWMu6MkOr0s6mKG2+DJf/HwSGCcIUUNpkV4g0WR0z2wFis0FIhngFdJlK0aRc8rmzju3MI9alfG1iEBQmCLP2gGOBTQKnI2SX08Rs6xIba+tGFZfFO6keJRLy4K4Fp6UpICiPT5DQJPqXw1flt9Sx1AKA5HwQklcwG1qxg56ylwFkbwNUhXmnbHMScuj9ICCPThA+wQw8at8AqvvFF930JgWEpL9GQZdeXBYddX2gqhSyhHv3gHUDCMijE+S3cPnNpFylnToWl81veiNeY4ZXDWNk8Xn7ssnI5h9V+9LvjpGePJpInwLBeGyC8LbuV+cc/1HHeMcqStBFGIyhChB78dd2n+qZU084WriOPa9Cvac8NkG+OCPNzjJDM3UsnE3MtXQ32qpSNLRKLR5ayT7GomMf1eFsMm23UbC2r0cmSFoubffjJWmCOlZgLz5qLpqirlXkjXYcYcd1fKq3Izv9qHUlv1c8CMIjE2TlYccadiTY6aqgsFQi1OqIJQzh7Je6cQ2LxOYelFTvEDLFTE3vH7Z9AoLwuAQ5GUv7X0qSn1bHMgrZCytgW1fPunH6tD+qB0+QmhB4yTE8NJt2AQF4VILwZf23nLI7XUbL27rByQJ2raysekT8URL5IgzVXT1K8Od00mGrEPdg96gE2RWkjEnMVpzWm43OIFDgxje9KYvhMvvPjXu080m5q+vMq9Ov8dLje1i1BjfnMQnCqof/9vO2D9UxvjrilQwQju46AZ3qPDLJVDtXyL5/tHAzX9MY3JjHJMi6o9Iss534qWMRrK0rC7Y6Iu9aGaL/+J6fkCgba+eGUZE5yt0rQ9378lyPSBDW1m3762VptDqWz8boCTnizT0M4eT3oRUnedfu+gpzTlkXsiriB27KIxJkzRH7MlYpnEp9aLJOuLaujjUbiGpBbV49qvqUksrKsFL91OOOD8FNCZ8gJ2Poi+fi6QvqWBKrHOmFIJSi1divOFfEmmrrlmdZsHWcu7Z9hU4QVtpNaw/b1qhjRTe9SRFvaKWPYi+l6jQZ2WgAqtPGy8tXwX/ngHkzuCGhE+Sbc8qErEJoqo7xtq5oN73RpfChlSrp2fGOqrrWo6ocT5E7f31Veh3cjLAJkk9po53nHf9Wx6x8dUTR1tZlXStdtPPLWFQ9iPZ+z+knrYF8zWNwI8ImyBf7HP+xOMDpVsZhKeK1dfVX9U5Dq6LqUUtt3fLE5Cl+S0KkqeBGhEyQiCTa67dIxem062wzwDXB2rokXQekVEXk1QM0WD1KfHTGOjfZQu8DNyFcgvALdlYetK9nLVynd8nlZMF+VRs7Wh7tPM+gGq4eJfIdFN47Yl4HbkK4BDkQIQ9JzKLt1bGUPIAsMwhFF63//TT2EoqXe5zVsTNWevZ4Gn0S3IBQCcLXZ9p6XFqgjhW1da8LNrRKY0OrbOffiS8dqujd5+WcfKIwANyAUAny1UlpenoBtFTHrqQSsRZhkFlLN+7mYZTWh1alnUiVO22Jlt4CjRMmQbIttPX35xWnDonFLl5blySwRHCUrh46Nv9wv5dy6hnLGlb1fUHDhEmQFXvsAQ4ZnC64Dk8Ra3VEks+GVik3V4qK3tdDa5IKFdPcS/ZZoGFCJMj5WPrc+TgYoI5lm4lYbV2W6OSqsfhumuqwXqfJg4IVteiSbVo6pU1Bo9w+QfhltJuOOpY4xwCCkwQ7Yp7COlSWm38nWSPnW92uAtb2HXPIsho0yu0T5JdLyrtxGfTP6lgsm3fkCnTTm/pGYtMl3dzCLToZ0Y2rR4mvYxwvHUihz4AGuXWCsOrRYMsJaa46xldHjBTslmkrnjRNfLmbfk/puDu1dcvz/on89Xw0ABrj1s/wxiPS9AIr1FfHolLFuulN72b6C/9s5xW44FX9SPUJulo5lb2qhObQVmujHCNBY9z2WU7Noff/dFFxut656KY3ArV1+W0DV/T2Hc6/bt2AxE0ZYFhesk8RLEG42Rcty1gVqQca4rbP8vpD8ieS7Lw64uUksdq673Tw+rxLfXKh5PsPB+pm3duAWESrHiVSLdQ446LlI9AQt3ymz12lz5+KlgeqY6m5BJLzxKkeLeroUlf19p6kjhFCCla84fWOO7d1y7Mk1D7+Sj7tCBrhdgnCL7hZ85vDqS1YdNMbwdq6a3ubRrKEyC0dH9SNfNmnPTkHgrIpAMOPF24HjXC7BPkpSBmRlk9bqGNxmaRo/iGKfi0MBwe0Nuwqa//8wYbJILBDqVLHX5Okl0AD3CpB+ATumxOO2eoY71hdEWgRBiN7RT59wmfcrR7zWGty6PWHdbtBYJMvWFZpYVVGt0qQNfukeblm0kgdC78u1iIMk7ubljzgT4LLe9zy1/XD/aruNoOaE5ItNwu4Io2DWuY2CRKZTB/Zc0lxWh2RHy2PzxSnejxQXxf38aPGCp2816gOSZ4/0PhvENjMi+bFmZQ2h1rkNgmyeo/jczYZd/p5w6+LtTri+r513mIT8wqfJPNeH7Kgc3OSAILKtVMYfcy8BmqRWyTI4TBpSEwq7aSOpRcQSMsDYbzcTv9Tr2bkiKt9vyQ4+pX15+b8zTgFBLYt3v78uQz6KNQSzScIm6h5fXlEWayOFbV1E8UZWtUxEljytOuJ+eHr9Mnnf7TsPp9Je7raP/Ahsv3pDrqzILBJQQW1VkU0nyDbjivT0nKd27rRqQQKrCCMuT29p7Q0kauu9k04bF6hSEQ/7oB1cVl/fu0bhqFudsVtpbC270M7EqRXoRZoOkEKC2mz709JThNRm4O3dcU51eLBhrqECd2NLt/8my/LI88nK134wnBHE6W/fB8tDXT1uHYNSdi/epFtILDJF80BfFEOqGGafqdtOCx/yFq4JnUsIlknzOqI/Gz1Lwb4vuJqXwGlTSYesqwC/rve2N7fb1vH3iR1XT2ezUUm3GUCYcUVyg0XhdtrfL6l2QSJS6Nd919S3lbH8i0EEgRq6w7rZPy8x93ktKt9o3+2r00vAMJXMSnZEnNoo7F7bYtcPb5xXZIy+wXDNBDY/HDLnHxKG0MN0myCBP4irWUfmr9nA+/mXkoU517m/iYiLX3G2+VxjJNJtO+mYMdLRWvultoCL0rvXkqn3Vz9uYl99Au7NCMxIKhcfnnuGUuNrqelyQT5NYgOi0yiPdSxRFY5sgrEqR5Le5tG3kWIy1uIjt9rXUV51VBu3mSJwIQ91jKv4V73hnEoCOyLONvLx9LoE1BDNJcg/HyrLYccC1nt+D0b+OqI/KCgKB5uqo8c2smw3tW+bWxifipBaeeqepRs+68qj/wcTV1O2B9tSY7+vTM5AAIbczF/I1+DGWqA5t51mw8r0/LM4HS+VTTrWtkcIAR+KcfKPj5vu9qXR+nd43fbV6vnHWVto/5rXVvWhH3R/xnHitz2vZgr378x3jEKaoCmEiTTTJv/eFp2uoy26KY3Ai3C8HZn4+ae95JjrvZN/F5anZJHQd25KmuLy6KNJ+2R5rn6e9r6k9BxTxkCQWCzQs3zamJVRk0lyMY9yjy75HwZbViSOG3dhj7EseSvXhNc7Qu+TruvPycNvtXQqvS24pg09moubefq71v0on5Kk3riXnmYZFH8PoqwVvuqjJpJkKBY2udImDwYij8ji2QVAlwTqK0b8Kz3UDYxz3S1b+Ju+wqFv/EVUuHNwarr+F32T139fYQQ87JBXv8CgS2MsE6LttK2UI00kSCsVJLVu+XPWC+X3/Wen0RSyL7ODrsmzsT8mVb6Y//oYNjqat93IdKQfZHK45WpHiXbf0OU/nuv0L+6+ntf60429GqtCwVBWWQKo87mV+sZBJp4B7JPO/pEJ/0XXkYoAH7Ig9L8lFziLUpbl0/MF/YxTXK1j58+MeF7KbAiE/OytjE77NvK6urMf944FgRl0hN42N9wCqqRZj6i3+hN5q0ZY+zUt4t+g0yJJTRBp+ll8Svjve6GNT3ucX3EfNIP0uL4TGhYkYl5WVtkKvX/9LDiMhF6tyUHBnch+0Ag/GNzaCvjrrjn6rf5uKPvGKhGmvyIDkui3UaspruPRdB7gNDin/KmrVRcd+P//Ji7rgKPr5E4hZZ+JDP+Pd9WrEoW3vR7ptAHO823h1RFE6KBD9CYRV7+DQjJKb2PdwdbzbUlFtjpzc/XLX72ij9WFb/tx1bs5+jV2BC5skfdYd3uIiehBmhykP/ne8mFY/N0Tb+dpHutTRNw68ui1gzwHeYqObiJ30mBMj++I9/5ll0AZMJmx1pX/05DX3Jtbn/jdHBjrevq8rc9XmfY8afrPVBTycFpfpDP18Ga/4Mye+63dIaVL87gRhXk5Y6GH3YM8nZ5xPvHEDrobwGOnVDFDk8y9uvdjux1ta/rElv0xWTlfneqIHw94pmdTItndvSexT5obFDDNJ8gJeKyaevJm2jgjtNKf3dIED8TgfDRPvc18yHxpX8XnvRtpkvJsem0IVSxjs0g4fIcr1au9p2Np716BlqPyfyETzdIkEEtjfs/6ek76n4TiYJa4jZ91PsakNgd43QDjszU9+3eGuJAwwzsWd3xss/zrpKDW/CzMiM2hSVHFQytSm+XE6HlqoP0PVf/7sOtyPGlL3hNBY3r5q9P3N+vTr/vn6rTtzaTg3ObClLa1lN02KSvpFXXc8CktQqyYoBp7Ps99Ctc/dwFBbRJs4lSSl41XjLcsC7QjABDA1dLl3Ijdtq3rTstv6q1CtLUl8DiR3xHvNHG8BlohNseiXutJ9kYtsLQbExf3ecGDf0Wk3p6rSgrObjRW2lgHp+yy9W3ZeYCGbdFWVjWz7B2kPGfL3YwHAWNMLDkGNXBa3PkkHp3ayk5OLetIGqXk2jXyd/In/4czK8TqJ0KYtBTWPocqxyPlJ0cB8No36fnyftq4pov/qFxeq7+se6ticsDaXy1mFE/ODatLqoktVdBBrbSH1r8WJ3R7eqRy6BBQiRIiUNR9Nl3N0lfhqfQJjWZIH4+BL591fTcs63JnrJ+Nn6ku/04OSUqxflU/ur0UCuSGLRI3/JWjwk4KU8dt9u2QOJZW4MJ0r6BLnvNE6bX+jQz/goaJs7JTsxTfyL7wuYZWga8Zhzf1I/UyIq9Qx7U/xg21tTyVsnBLf0JZkQlkUZ3ckpJZbegGGixbi+95W3NRj+mX3h6tM9jD7fQX4Ea0MSX0OW9fGdEvFK3mdaTgxOqgqixT+w6U3cqS5fvl0fY+di8iivIA411qQEveg/t25qU+yLnUurf6k0lM8cMNa7xXSClbtTVYxP2ctsCa07LE6bvty/JstIqryD8Aq6xnbzWL+7lPYH9LG5z8FfYBCmRnE3vW/ibMm3DcemdPH6Y6Q4TpPu9JHZKb8MHrzyo38Ze6ArdLnR4oLLjs710MNSSSQN1qz4ZRt6ryGPZB4vfnEPSv1eecUzNsNA7ThBfI4FhHby+nNbVa24LP1IjVaoqCZ8gJfjVZ9svwuDdwfI/Dl5RnruWSyv0gpuM7Ah0c11Yvz/pdg/qYvi6S6M/7hlYESfCaO+/TKOHlFq86MvAPr3PLyc9u7R2fcKkK/zeHL9EQ/+d4fah++Pl/rG51KeiCeLP5mRPtzQceKGN7ttB7Y3b/QjJAjflMQlSWnohbXo2Hh6NyVTaJ+fT5jlmpbFd1nn7+Sg5fr76jPv8aUSnpvrgLvfARVYpbvuK+M3soF1UInQp+V4PigxEx08brFD1IcT1bUnZnJ/c2E8oKf4aile/1xFF0ctEZ+T3wi1+jM7YpTUcGdyL3HYLNa2A3nMuBXqGZUld47Jp21xJ8acK+/fYv+BjIJbmd9GEB/wN57s3059vW48Iew0KQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghsfwPe439Lbn1i98AAAAASUVORK5CYII=";
        
    var today = new Date();
	var dd = String(today.getDate()).padStart(2, '0');
	var mm = String(today.getMonth() + 1).padStart(2, '0'); 
	var yyyy = today.getFullYear();
	today = mm + '/' + dd + '/' + yyyy;


	
	var table = $("#mx-sudents-lists").DataTable({
		processing: true,
        serverSide: false,
        bScrollCollapse: true,
        sScrollX: "100%",
        ajax: "{{ url(config('laraadmin.adminRoute') . '/report_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		"aaSorting": [],
		columnDefs: [ { orderable: false, targets: [-1] }],
		columnDefs: [ { orderable: false, targets: [-1] }],

		// Column Sum
        "footerCallback": function(row, data, start, end, display) {
                    var api = this.api();
                    api.columns('.sum', { page: 'current' }).every(function () {
                        var sum = api
                            .cells( null, this.index(), { page: 'all'} )
                            .render('display')
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0;
                                return x + y;
                            }, 0);
                        sum = sum.toFixed(2);
                        $(this.footer()).html(sum);
                    });
        },
		// End Column Sum

		dom: 'Bfrtip',
        buttons: [
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { 
            	extend: 'pdfHtml5', 
            	footer: true,
            	title: 'Sales Report',        	
	customize: function (doc) {
        doc.pageMargins = [20,20,20,20];
        doc.defaultStyle.fontSize = 7;
        doc.styles.tableHeader.fontSize = 7;
        doc.styles.title.fontSize = 9;
        doc.content[0].text = doc.content[0].text.trim();

        // Create a footer
        doc['footer']=(function(page, pages) {
            return {
                columns: [
                    'Acts. Designed by Herdzo Innovations',
                    {
                        alignment: 'right',
                        text: ['page ', { text: page.toString() },  ' of ', { text: pages.toString() }]
                    }
                ],
                margin: [10, 0]
            }
        });

        doc['header']=(function(page, pages) {
            return {
                columns: [
                	{
                        image: logo,
                        width: 20,
                        height: 20
                    },
                    {
                        alignment: 'right',
                        text: ['Created on: ', { text: today }]
                    }
                ],
                margin: [10, 10]
            }
        });
        
        var objLayout = {};
        /*objLayout['hLineWidth'] = function(i) { return .5; };
        objLayout['vLineWidth'] = function(i) { return .5; };
        objLayout['hLineColor'] = function(i) { return '#aaa'; };
        objLayout['vLineColor'] = function(i) { return '#aaa'; };
        objLayout['paddingLeft'] = function(i) { return 4; };
        objLayout['paddingRight'] = function(i) { return 4; }; */
        doc.content[1].layout = objLayout;
    }



        	}
        ]

	});




/*var myDataTable = $("#mx-sudents-lists").DataTable();
myDataTable.column([6]).visible(false);*/
	

	$('[name="from_date"]').change(function () { 
		document.getElementById("to_date").setAttribute("min", $(this).val());
    });

    $('[name="to_date"]').change(function () { 
		document.getElementById("from_date").setAttribute("max", $(this).val());
    });

    $('body').on('click', '.clearbtn', function() {
    	$("#from_date").removeAttr("max");
    	$("#to_date").removeAttr("min");
		$("#from_date").val('');
		$("#to_date").val('');
		$("#agent_id").val('');
		/*$("#sales").val('');
		$("#rtype").val('Sales');
		$("#rstatus").val('');
		$("#pay_status").show();*/
	});

	$('body').on('click', '.searchbtn', function() {
		$("#mx-sudents-lists").dataTable().fnDestroy();  $("#example2").dataTable().fnDestroy();  $("#example3").dataTable().fnDestroy();
		var from_date = document.getElementById("from_date").value; 
		var to_date = document.getElementById("to_date").value;
		var agent_id = document.getElementById("agent_id").value;
		/*var sale_id = document.getElementById("sales").value;
		var rtype = document.getElementById("rtype").value;
		var rstatus = document.getElementById("rstatus").value;*/
		
		/*if(rtype=="Sales"){
			var dt = "#mx-sudents-lists";
			$("#example2").hide(); $("#example3").hide();
			$("#mx-sudents-lists").show();
		} else if(rtype=="Payment"){ 
			var dt = "#example2";
			$("#mx-sudents-lists").hide(); $("#example3").hide();
			$("#example2").show();
		} else {
			var dt = "#example3";
			$("#mx-sudents-lists").hide(); $("#example2").hide();
			$("#example3").show();
		}*/

		//$("#mx-sudents-lists").DataTable({
		$(dt).DataTable({
		processing: true,
        serverSide: false,
        bScrollCollapse: true,
        sScrollX: "100%",
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "ajax": {
        'type': 'GET',
        'url': "{{ url(config('laraadmin.adminRoute') . '/report_dt_ajax') }}",
        'data': {
           "from_date": from_date,
           "to_date": to_date,
           "agent_id"  : agent_id
           /*"sale_id"	: sale_id,
           "rtype"	: rtype,
           "rstatus"	: rstatus*/
        }
        },
        "aaSorting": [],
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		columnDefs: [ { orderable: false, targets: [-1] }],
		columnDefs: [ { orderable: false, targets: [-1] }],

		// Column Sum   
        /*"footerCallback": function(row, data, start, end, display) {
                    var api = this.api();
                    api.columns('.sum1', { page: 'current' }).every(function () {
                        var sum = api
                            .cells( null, this.index(), { page: 'all'} )
                            .render('display')
                            .reduce(function (a, b) {
                                var x = parseFloat(a) || 0;
                                var y = parseFloat(b) || 0;
                                return x + y;
                            }, 0);
                        sum = sum.toFixed(2);
                        console.log(this.index() +' '+ sum); 
                        $(this.footer()).html(sum);
                    });
        },*/
		// End Column Sum

		dom: 'Bfrtip',
        /*buttons: [
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]*/
        buttons: [
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            //{ extend: 'pdfHtml5', footer: true }

            { 
            	extend: 'pdfHtml5', 
            	footer: true,
            	title: 'Payment Report',        	
	customize: function (doc) {
        doc.pageMargins = [20,20,20,20];
        doc.defaultStyle.fontSize = 7;
        doc.styles.tableHeader.fontSize = 7;
        doc.styles.title.fontSize = 9;
        doc.content[0].text = doc.content[0].text.trim();

        // Create a footer
        doc['footer']=(function(page, pages) {
            return {
                columns: [
                    'Acts. Designed by Herdzo Innovations',
                    {
                        alignment: 'right',
                        text: ['page ', { text: page.toString() },  ' of ', { text: pages.toString() }]
                    }
                ],
                margin: [10, 0]
            }
        });

        doc['header']=(function(page, pages) {
            return {
                columns: [
                	{
                        image: logo,
                        width: 20,
                        height: 20
                    },
                    {
                        alignment: 'right',
                        text: ['Created on: ', { text: today }]
                    }
                ],
                margin: [10, 10]
            }
        });
        
        var objLayout = {};
        /*objLayout['hLineWidth'] = function(i) { return .5; };
        objLayout['vLineWidth'] = function(i) { return .5; };
        objLayout['hLineColor'] = function(i) { return '#aaa'; };
        objLayout['vLineColor'] = function(i) { return '#aaa'; };
        objLayout['paddingLeft'] = function(i) { return 4; };
        objLayout['paddingRight'] = function(i) { return 4; }; */
        doc.content[1].layout = objLayout;
    }



        	}

        ] 
	});
	});

	// trigger date-picker
	$('body').on( "click", "#from_date,#to_date", function() {
  		$(this).next().trigger('click');
	});
});

function getSales(obj)
{
    $.ajax({
    type: "GET",
    url: "{{ url(config('laraadmin.adminRoute') . '/get_sales') }}",
    data:'agent_id='+obj.value,
    success: function(data){
        $("#sales").html(data);
    }
    });
}

function showStatus(obj)
{
	var rtype = document.getElementById("rtype").value;
	if(rtype == 'Payment' || rtype == 'Expense') {
		$("#rstatus").val('');
		$("#pay_status").hide();
	} else {
		$("#pay_status").show();
	}
}

</script>
@endpush
