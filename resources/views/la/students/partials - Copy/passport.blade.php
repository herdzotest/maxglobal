<div id="tab_2" class="tab-pane" >
                                        <div class="content-head">
                                        
                                                        <h4>Passport Details</h4>
                                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                                        </div>
                                        {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'passport-add-form']) !!}
                                <div class="pane">
                               
                                        <div class="row">
                                                <div class=" col s7 ipfield">
                                                        <div class="input-field">
                                                                        <!-- <i class="material-icons prefix">account_circle</i> -->
                                                                        <input id="passport_no" name="passport_no" type="text" class="validate">
                                                                        <label for="passport_no" >Passport Number</label>
                                                                        <span class="helper-text" data-error="wrong"
                                                                        data-success="right">As per Passport</span>
                                                        </div>


                                                </div>

                                        </div>

                                        <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                        <input id="issued_date" name="issued_date" type="text" class="datepicker" required>
                                                                        <label for="issued_date">Passport Issue Date</label>
                                                                        <span class="helper-text" data-error="wrong"
                                                                        data-success="right">As per Passport</span>
                                                                </div>
                                                </div>

                                                <div class="col s6">
                                                                <div class="input-field ">
                                                                                <input id="expiry_date" name="expiry_date" type="text" class="datepicker" required>
                                                                                <label for="expiry_date">Passport Expiry Date</label>
                                                                                <span class="helper-text" data-error="wrong"
                                                                                data-success="right">As per Passport</span>
                                                                        </div>
                                                </div>


                                        </div>
                                        <br>
                                        <div class="row">
                                                <div class="col s6">
                                                        <label>Upload Passport Front Page</label>
                                                        <div class="file-field input-field">
                                                                                <div class="btn">
                                                                                        <span>Upload</span>
                                                                                        <input type="file"><i class="fa fa-cloud-upload"></i> 
                                                                                </div>
                                                                        <div class="file-path-wrapper">
                                                                                <input class="file-path validate" name="image_front" type="text">

                                                                                <div>
                                                                                </div>
                                                                
                                                                        </div>
                                                        </div>

														<div class="uploaded-file" id="image_front" data-type="file"></div>
                                                </div>
                                                <div class="col s6">
                                                                <label>Upload Passport Back Page</label>
                                                                <div class="file-field input-field">


                                                                                <div class="btn">
                                                                                        <span>Upload</span>
                                                                                        <input type="file"><i class="fa fa-cloud-upload"></i> 
                                                                                </div>
                                                                                <div class="file-path-wrapper">
                                                                                        <input class="file-path validate" name="image_back"  type="text">
                                                                                      

                                                                                </div>
                                                                </div>
                                                              
																<div class="uploaded-file" id="image_back" data-type="file"></div>
                                                        </div>
                                        </div>




                                </div>
                                <div class="pane2">
                                    
                                                <div class="row">
                                                        <div class=" col s6 ">
                                                                        <div class="input-field">
                                                                                        <input id="skype_id" name="skype_id" type="text" class="validate">
                                                                                        <label for="">Skype ID</label>
                                                                        </div>
                                                                </div>
                                                                
                                                                <div class="col s12">
                                                                                <label>Skype ID Screenshot</label>
                                                                                <div class="file-field input-field">
                                                                                                <div class="btn">
                                                                                                        <span>Upload</span>
                                                                                                        <input type="file"><i class="fa fa-cloud-upload"></i> 
                                                                                                </div>
                                                                                                <div class="file-path-wrapper">
                                                                                                        <input class="file-path validate" name="skpe_screenshot" type="text">
                
                                                                                                </div>
                                                                                </div>
																				<div class="uploaded-file" id="skpe_screenshot" data-type="file"></div>
                                                                </div>
                                                </div>
                                                <div class="row">
                                                                <div class=" col s6 ">
                                                                        <div class="input-field">
                                                                                <input id="contact_no" name="contact_no" type="text" class="validate">
                                                                                <label for="">Contact Number </label>
                                                                        </div>
                                                                </div>
                                                                <div class=" col s6 ">
                                                                        <div class="input-field">
                                                                                <input id="mobile" name="mobile" type="text" class="validate">
                                                                                <label for="">Mobile Number</label>
                                                                        </div>
                                                                        </div>
                                                                <div class=" col s6 ">
                                                                        <div class="input-field">
                                                                                <input id="whatsapp" name="whatsapp" type="text" class="validate">
                                                                                <label for="">Whatsapp Number</label>
                                                                        </div>
                                                                </div>
                                                </div>
                                                <div class="row">
                                                                <div class=" col s12 ">
                                                                        <div class="input-field">
                                                                                <input id="prev_study_sector" name="prev_study_sector" type="text" class="validate">
                                                                                <label for="">Previous Study Sector   </label>
                                                                                <span class="helper-text">(Ex : BA Economics, MA, +2 etc.)</span>
                                                                        </div>
                                                                </div>
                                       
                                                </div>
                                                <div class="row">
                                                               

                                                                <div class="col s12">
                                                                                <label>CV upload</label>
                                                                                <div class="file-field input-field">
                                                                                                <div class="btn">
                                                                                                        <span>Upload</span>
                                                                                                        <input type="file"><i class="fa fa-cloud-upload"></i> 
                                                                                                </div>
                                                                                                <div class="file-path-wrapper">
                                                                                                        <input class="file-path validate" name="cv_upload" type="text">
                
                                                                                                </div>
                                                                                </div>
																				<div class="uploaded-file" id="cv_upload" data-type="file"></div>
                                                                </div>
                                       
                                                </div>
                                                <div class="row">
                                                                <div class=" col s12 ">
                                                                        <div class="input-field">
                                                                                <input id="next_kin" name="next_kin" type="text" class="validate">
                                                                                <label for="">Next of Kin   </label>
                                                                                <span class="helper-text">(Name and contact no.of family or friend to be contacted in emergency)</span>
                                                                        </div>
                                                                </div>
                                       
                                                </div>

        
        
        
        
                                        </div>
                                        <div class="controls">
                                <a class="mgbtn2">Back</a>
                                <!-- <a class="waves-effect waves-light btn" type="submit"><i class="material-icons left">save</i>SAVE</a> -->
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="passport_details">
                                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
       
                                {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                        </div>
                        {{ Form::close() }}
</div>