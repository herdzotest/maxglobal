@extends('la.layouts.app')

@section('htmlheader_title') Dashboard @endsection
@section('contentheader_title') Dashboard @endsection
@section('contentheader_description') Organisation Overview @endsection

@section('main-content')
<!-- Main content -->
        <section class="content">
        <div class="row">
        <div class="col-md-6">
           <div class="mg-card namecard">
            @if(Auth::user()->type == "STUDENT" || Auth::user()->type == "NURSE")
            <?php
              $avatatImg = 'https://www.gravatar.com/avatar/ef1c169902a351837b6c7f8816cf9b5c.jpg?s=80&d=mm&r=g';
            ?>
            @else
            <?php
              $avatatImg = ($user[0]->profilePic == '') ? 'https://www.gravatar.com/avatar/ef1c169902a351837b6c7f8816cf9b5c.jpg?s=80&d=mm&r=g' : $user[0]->profilePic;
            ?>
            @endif
            <img src="<?php echo $avatatImg; ?>" alt="">
            <div class="info">
              <h3><?php echo Auth::user()->name; ?></h3>
              @if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER"]))
              <p>ADMIN</p>
              @elseif(Auth::user()->type == "MAIN_AGENTS")
              <p>AGENT</p>
              @elseif(Auth::user()->type == "CONSULTANTS")
              <p>STAFF</p>
              @elseif(Auth::user()->type == "UNIVERSITY_USER")
              <p>UNIVERSITY USER</p>
              @elseif(Auth::user()->type == "SUBCONTRACTORS")
              <p>SUB-CONTRACTOR</p>
              @elseif(Auth::user()->type == "STUDENT")
              <p>STUDENT</p>
              @elseif(Auth::user()->type == "EMPLOYER_ROLE")
              <p>EMPLOYER</p>
              @else
              <p>NURSE</p>
              @endif

                <div><i class="fa fa-envelope"></i> <?php echo Auth::user()->email; ?></div>
                <div><i class="fa fa-phone"></i> <?php echo $user[0]->mobile; ?></div>

                @if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER","CONSULTANTS","MAIN_AGENTS","UNIVERSITY_USER","SUBCONTRACTORS"]))
                  <?php $id=Auth::user()->context_id; ?>
                  <div><a href="<?php echo url(config("laraadmin.adminRoute") . '/user_profile/'.$id.'/view'); ?>">View More</a></div>
                @endif

                @if(Auth::user()->type == "STUDENT")
                  <div>Status: <?php echo $user[0]->user_status; ?></div>
                  <?php $id=Auth::user()->id; ?>
                  <div><a href="<?php echo url(config("laraadmin.adminRoute") . '/students/'.$id); ?>">View More</a></div>
                @endif

          </div>
            </div>
        </div>



        @if((Auth::user()->type == "STUDENT"))
        <div class="col-md-6"><div class="mg-card">
          <div class="title">
              Staff
          </div>
          <div class="info">
            <?php if($std_counselor && $std_counselor[0] != '') { ?>
            <h4><?php echo $std_counselor[0]->name; ?></h4>
            <div><i class="fa fa-envelope"></i> <?php echo $std_counselor[0]->email; ?></div>
            <div><i class="fa fa-phone"></i> <?php echo $std_counselor[0]->mobile; ?></div>
            <?php } ?>
          </div>
        </div></div>

        <div class="col-md-6"><div class="mg-card">
          <div class="title">
              University User
          </div>
          <div class="info">
            <?php if($std_unv_user && $std_unv_user[0] != '') { ?>
            <h4><?php echo $std_unv_user[0]->name; ?></h4>
            <div><i class="fa fa-envelope"></i> <?php echo $std_unv_user[0]->email; ?></div>
            <div><i class="fa fa-phone"></i> <?php echo $std_unv_user[0]->mobile; ?></div>
            <?php } ?>
          </div>
        </div></div>

        <div class="col-md-6"><div class="mg-card">
          <div class="title">
              Agent
          </div>
          <div class="info">
            <?php if($std_agent && $std_agent[0] != '') { ?>
            <h4><?php echo $std_agent[0]->name; ?></h4>
            <div><i class="fa fa-envelope"></i> <?php echo $std_agent[0]->email; ?></div>
            <div><i class="fa fa-phone"></i> <?php echo $std_agent[0]->mobile; ?></div>
            <?php } ?>
          </div>
        </div></div>

        <div class="col-md-6"><div class="mg-card">
          <div class="title">
              Next Follow Date
          </div>
          <div class="info">
            <?php if($user[0]->follow_date != '' && $user[0]->follow_time != '') { ?>
            <div><i class="fa fa-clock-o"></i> <?php echo $user[0]->follow_date.' at '.$user[0]->follow_time; ?></div>
            <?php } ?>
          </div>
        </div></div>
        @endif

        @if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER","UNIVERSITY_USER","MAIN_AGENTS"]))
        <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Students Assigned
            </div>
            <div class="number">
            <?php if($students) { ?>
              <?php echo $students; ?>
            <?php } else { echo 0; } ?>
            </div>
          </a>
        </div>
        @endif


        @if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER"]))
        <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Nurses Assigned
            </div>
            <div class="number">
            <?php if($nurses) { ?>
              <?php echo $nurses; ?>
            <?php } else { echo 0; } ?>
            </div>
          </a>
        </div>
        @endif

        <?php if((Auth::user()->type == "CONSULTANTS") && ($user[0]->nurseuser == 1)){ ?>
        <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Nurses Assigned
            </div>
            <div class="number">
            <?php if($nurses) { ?>
              <?php echo $nurses; ?>
            <?php } else { echo 0; } ?>
            </div>
          </a>
       </div>
<?php } else if((Auth::user()->type == "CONSULTANTS") && ($user[0]->nurseuser == 0)){ ?>
  <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Students Assigned
            </div>
            <div class="number">
            <?php if($students) { ?>
              <?php echo $students; ?>
            <?php } else { echo 0; } ?>
            </div>
          </a>
       </div>
<?php } ?>



        @if(Auth::user()->type == "MAIN_AGENTS")
        <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Students Pending
            </div>
            <div class="number">
            <?php if($students_pending) { ?>
              <?php echo $students_pending; ?>
            <?php } else { echo 0; } ?>
            </div>
          </a>
        </div>
        <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Students Approved
            </div>
            <div class="number">
            <?php if($students_approved) { ?>
              <?php echo $students_approved; ?>
            <?php } else { echo 0; } ?>
            </div>
          </a>
        </div>
        @endif

        @if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER"]))
        <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Active Staffs
            </div>
            <div class="number">
            <?php if($counselor_active) { ?>
              <?php echo $counselor_active; ?>
            <?php } else { echo 0; } ?>
            </div>
          </a>
        </div>
        <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Inactive Staffs
            </div>
            <div class="number">
            <?php if($counselor_inactive) { ?>
              <?php echo $counselor_inactive; ?>
            <?php } else { echo 0; } ?>
            </div>
          </a>
        </div>
        <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Active University Agents
            </div>
            <div class="number">
            <?php if($documentation_active) { ?>
              <?php echo $documentation_active; ?>
            <?php } else { echo 0; } ?>
            </div>
          </a>
        </div>
        <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Inactive University Agents
            </div>
            <div class="number">
            <?php if($documentation_inactive) { ?>
              <?php echo $documentation_inactive; ?>
            <?php } else { echo 0; } ?>
            </div>
          </a>
        </div>
        <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Active Agents
            </div>
            <div class="number">
            <?php if($agent_active) { ?>
              <?php echo $agent_active; ?>
            <?php } else { echo 0; } ?>
            </div>
          </a>
        </div>
        <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Inactive Agents
            </div>
            <div class="number">
            <?php if($agent_inactive) { ?>
              <?php echo $agent_inactive; ?>
            <?php } else { echo 0; } ?>
            </div>
          </a>
        </div>
        <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Active sub-contractors
            </div>
            <div class="number">
            <?php if($subcontractors_active) { ?>
              <?php echo $subcontractors_active; ?>
            <?php } else { echo 0; } ?>
            </div>
          </a>
        </div>
         <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Inactive sub-contractors
            </div>
            <div class="number">
            <?php if($subcontractors_inactive) { ?>
              <?php echo $subcontractors_inactive; ?>
            <?php } else { echo 0; } ?>
            </div>
          </a>
        </div>

        <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Active Employers
            </div>
            <div class="number">
            <?php if($employers_active) { ?>
              <?php echo $employers_active; ?>
            <?php } else { echo 0; } ?>
            </div>
          </a>
        </div>
         
         <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Inactive Employers
            </div>
            <div class="number">
            <?php if($employers_inactive) { ?>
              <?php echo $employers_inactive; ?>
            <?php } else { echo 0; } ?>
            </div>
          </a>
        </div>
        @endif


        <!-- <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Students Pending Approval
            </div>
            <div class="number">
              9
            </div>
          </a>
          </div>
          <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Students Pending Approval
            </div>
            <div class="number">
             11
            </div>
          </a>
          </div> -->
          <!-- <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Students Pending Approval
            </div>
            <div class="number">
             8
            </div>
          </a>
          </div>
          <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Students Pending Approval
            </div>
            <div class="number">
              48
            </div>
          </a>
          </div> -->
          <!-- <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Students Pending Approval
            </div>
            <div class="number">
              37
            </div>
          </a>
          </div>
          <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Students Pending Approval
            </div>
            <div class="number">
              21
            </div>
          </a>
          </div> -->
		
        </div>
        </section>

@endsection

@push('styles')
<!-- Morris chart -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/morris/morris.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- <script src="{{ asset('la-assets/plugins/morris/morris.min.js') }}"></script> -->
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<!-- <script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script> -->
@endpush

@push('scripts')
<script>
(function($) {
	$('body').pgNotification({
		style: 'circle',
		title: 'Zodocs',
		message: "Welcome to Zodocs...",
		position: "top-right",
		timeout: 0,
		type: "success",
		thumbnail: '<img width="40" height="40" style="display: inline-block;" src="{{ Gravatar::fallback(asset('la-assets/img/user2-160x160.jpg'))->get(Auth::user()->email, 'default') }}" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" alt="">'
	}).show();
})(window.jQuery);
</script>
@endpush