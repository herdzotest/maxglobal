<div id="tab_2" class="tab-pane" >
        
                   
<div class="content-head">
                                        
                                        <h4>Courses Preferred</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                        </div>
                        {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'personal-add-form']) !!}
                <div class="pane dropdown_wrapper">
					<div id="course-wrapper">
					<div class="stdhst">
							<div class="index">1</div>
							<div class="col-md-6">
								<div class="mg-input-field ">
									<label for="course_country">Preferred Country</label>
									<select class="searchSelect" name="course_country" id="course_country">
										<option value="">Select</option>
										<?php
										foreach($countries as $countriesData) {
											echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
										}
										?>										
									</select>												
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="mg-input-field ">
									<label for="level_study">Preferred Level</label>
									<select class="searchSelect" name="level_study" id="level_study">
									  <option value="">Select</option>
									  <option value="Diploma"> Diploma </option>
									  <option value="Bachelors"> Bachelors </option>
									  <option value="Masters"> Masters </option>
									  <option value="Doctoral"> Doctoral </option>
									  <!-- <option value="Entry-level"> Entry-level </option>
									  <option value="Level-1"> Level 1 </option>
									  <option value="Level-2"> Level 2 </option>
									  <option value="Level-3"> Level 3 </option>
									  <option value="Level-4"> Level 4 </option>
									  <option value="Level-5"> Level 5 </option>
									  <option value="Level-6/Bachelors/UG"> Level 6/Bachelors/UG </option>
									  <option value="Level-7/Masters Degree/PG"> Level 7/Masters Degree/PG </option>
									  <option value="Level-8/Doctorate"> Level 8/Doctorate </option> -->
									</select>								
								</div>
							</div>
							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="course_university">Preferred University</label>
											<select class="searchSelect" name="course_university" id="course_university">
												<option value="">Select</option>
												<?php
												foreach($university as $universityData) {
													echo '<option value="'.$universityData->university.'">'.$universityData->university.'</option>';
												}
												?>													
											</select>												
									</div>

							</div>
							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="course_title">Preferred Course</label>
											<select class="searchSelect" id="course_title" name="course_title">
												<option value="">Select</option>
												<?php
												foreach($courses as $courseData) {
													echo '<option value="'.$courseData->course_name.'">'.$courseData->course_name.'</option>';
												}
												?>											
											</select>
									</div>
							</div>
							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="mode_study">Mode of study</label>
											<select class="searchSelect" id="mode_study" name="mode_study">
												  <option value="">Select</option>
												  <option value="Full time"> Full time </option>
												  <option value="Part time"> Part time </option>
												  <option value="Distance Learning"> Distance Learning </option>											
											</select>
									</div>
							</div>
							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="campus_stage">Preferred Campus</label>
											<select class="searchSelect" id="campus_stage" name="campus_stage">
												<option value="">Select</option>
												<?php
												foreach($campus as $campusData) {
													echo '<option value="'.$campusData->campus_name.'">'.$campusData->campus_name.'</option>';
												}
												?>											
											</select>
									</div>
							</div>
							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="stage_name">Preferred Stage</label>
											<select class="searchSelect" id="stage_name" name="stage_name">
												  <option value="">Select</option>
												  <option value="Year-1">Year 1</option>
												  <option value="Year-2">Year 2</option>
												  <option value="Year-3">Year 3</option>											
											</select>									
									</div>
							</div>						

							<div class="col-md-6">
									<div class="mg-input-field ">
											<label for="entry_date">Preferred Intake</label>
											<!-- <input class="form-control datepicker" id="entry_date" name="entry_date" type="text"> -->
											<?php 
											$curr_yr = date('Y');
											$curr_yr2 = date('Y', strtotime('-2 year')); 
											$nxt_yr = date('Y', strtotime('+1 year'));
											$nxt_yr2 = date('Y', strtotime('+2 year'));
											$curr_mon = date('m');
    										$months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
											?>
											<select class="searchSelect" name="entry_date" id="entry_date">
												<option value="">Select</option>
												<?php
													for($i=$curr_yr2; $i<=$nxt_yr2;$i++) {
														foreach ($months as $key => $value) { /*if($i==$curr_yr && $key<$curr_mon) { } else { */ ?>
															<option value="<?php echo $value.' '.$i; ?>"> <?php echo $value.' '.$i; ?></option>
													<?php	//} 
														}
													}
												?>
											</select>
									</div>
							</div>								
							<div class="clearfix"></div>
					</div>
					</div>
					
					<div class="btn addshbtn"> <span class="glyphicon glyphicon-plus"></span></div>						
                </div>
				<!-- <input type="hidden" value="1" class="wrapper-count"> -->
				<input type="hidden" value="<?php echo $wrp_count_coursepreferred; ?>" class="wrapper-count">
				<input type="hidden" value="courses" name="formtype" class="formtype">
                <div class="controls">
                                <a class="mgbtn2 back_button">Back</a>
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="courses_applied">
                                 <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                              
                                {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                        </div>
                        {{ Form::close() }}
</div>


