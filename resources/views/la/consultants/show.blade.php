@extends('la.layouts.app')

@section('htmlheader_title')
	Staff View
@endsection


@section('main-content')
<div id="page-content" class="profile2">
	<div class="bg-primary clearfix">
		<div class="col-md-7">
			<div class="row">
				<div class="col-md-2">
					<!-- <img class="profile-image" src="{{ asset('la-assets/img/1.jpg') }}" alt=""> -->

				<?php
				$avatatImg = ($employee->profilePic == '') ? 'https://www.gravatar.com/avatar/ef1c169902a351837b6c7f8816cf9b5c.jpg?s=80&d=mm&r=g' : $employee->profilePic;
				?>
				<img class="profile-image" src="<?php echo $avatatImg; ?>" alt="{{ $employee->$view_col }}">

					<!-- <div class="profile-icon text-primary"><i class="fa {{ $module->fa_icon }}"></i></div> -->
				</div>
				<div class="col-md-10 prname">
					<h4 class="name">{{ $employee->$view_col }}</h4>
					<div class="label2">Staff</div>
					<!--
					<div class="row stats">
						<div class="col-md-4"><i class="fa fa-facebook"></i> 234</div>
						<div class="col-md-4"><i class="fa fa-twitter"></i> 12</div>
						<div class="col-md-4"><i class="fa fa-instagram"></i> 89</div>
					</div>
					-->
					<!-- <p class="desc">{{ substr($employee->about, 0, 33) }}@if(strlen($employee->about) > 33)...@endif</p> -->
				</div>
			</div>
		</div>
		<div class="col-md-2 prdts">
			<div class="dats1"><!--<div class="label2"></div>--></div>
			<div class="dats1 prof-ic"><i class="fa fa-envelope-o"></i> {{ $employee->email }}</div>
			<div class="dats1 prof-ic"><i class="fa fa-map-marker"></i> {{ $employee->city }}</div>
		</div>
		
		<div class="col-md-2"></div>
		
		<div class="col-md-1 actions">
			@la_access("Employees", "edit")
				<a href="{{ url(config('laraadmin.adminRoute') . '/'.$type.'/consultants/'.$employee->id.'/edit') }}" class="btn btn-xs btn-edit btn-default"><i class="fa fa-pencil"></i></a><br>
			@endla_access
			
			@la_access("Employees", "delete")
				{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.'.$type.'.consultants.destroy', $employee->id], 'method' => 'delete', 'style'=>'display:inline','onSubmit'=>"return confirm('Are you sure you wish to delete?');"]) }}
					<button class="btn btn-default btn-delete btn-xs" type="submit"><i class="fa fa-times"></i></button>
				{{ Form::close() }}
			@endla_access
		</div>

		<div class="row">
			<div class="col-md-12 prdts-prof vhbottom">
				<div class="dats1"><i class="fa fa-envelope-o mr5"></i> {{ $employee->email }} <span>|</span><i class="fa fa-map-marker mr5"></i> {{ $employee->city }} </div>
			</div>
		</div>
		
	</div>

	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		<li class=""><a href="{{ url(config('laraadmin.adminRoute') . '/'.$type.'/consultants/'.$employee->id) }}" data-toggle="tooltip" data-placement="right" title="Back to view"><i class="fa fa-chevron-left"></i></a></li>
		<li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> General Info</a></li>
		<!-- <li class=""><a role="tab" data-toggle="tab" href="#tab-timeline" data-target="#tab-timeline"><i class="fa fa-clock-o"></i> Timeline</a></li> -->
		@if(Entrust::hasRole("SUPER_ADMIN") || Entrust::hasRole("MANAGER"))
			<li class=""><a role="tab" data-toggle="tab" href="#tab-account-settings" data-target="#tab-account-settings"><i class="fa fa-key"></i> Account settings</a></li>
			<!-- <li class=""><a role="tab" data-toggle="tab" href="#tab-account-settime" data-target="#tab-account-settime"><i class="fa fa-hourglass-2"></i> Set time</a></li> -->
		@endif
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>General Info</h4>
					</div>
					<div class="panel-body">
						@la_display($module, 'name')
						@la_display($module, 'mobile')
						@la_display($module, 'mobile2')
						@la_display($module, 'email')
						@la_display($module, 'city')
						@la_display($module, 'address')
						@la_display($module, 'about')
						@la_display($module, 'date_birth')
					</div>
				</div>
			</div>
		</div>
		
		<div role="tabpanel" class="tab-pane fade in p20 bg-white" id="tab-timeline">
			@if(Session::has('success_mail_delete'))
				<p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success_mail_delete') }}</p>
			@endif
			<?php
			$mailDetailswithIds = json_encode(array(),true);
			if(count($mailDetails) == 0) {
			?>
				<div class="text-center p30"><i class="fa fa-list-alt" style="font-size: 100px;"></i> <br> No mails to show</div>
			<?php
			} else {
				$mailDetailsIds = array_column($mailDetails,'id');
				$mailDetailswithIds = json_encode(array_combine($mailDetailsIds,$mailDetails),true);
			?>
				<input type="hidden" value="<?php echo $mailDetailswithIds; ?>">
				<ul class="timeline timeline-inverse" id="mail-lists">
					<?php 
					$currentDate = '';
					foreach($mailDetails as $result) {	
						$dates = explode(' ',$result->send_date);
						$old_timestamp = strtotime($dates[1]);
						$new_time = date('h:i a', $old_timestamp);
						
						if($dates[0] != $currentDate) {
							$currentDate = $dates[0];
							$old_date_timestamp = strtotime($dates[0]);
							$new_date = date('d M Y', $old_date_timestamp);
						?>
							<li class="time-label">
								<span class="bg-red"><?php echo $new_date; ?></span>
							</li>
						<?php
						}
						?>
						<li>
							<i class="fa fa-envelope bg-blue"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> <?php echo $new_time; ?></span>
								<h3 class="timeline-header header"><a href="javascript:void(0);"><?php echo $result->name; ?></a> has sended you an email</h3>
								<h3 class="timeline-header subject">Subject: &nbsp;<a href="javascript:void(0);"><?php echo $result->subject; ?></a></h3>
								<div class="timeline-body">
									<?php
									$mailContent = strip_tags($result->mail_content);
									if(strlen($mailContent) > 150)
										$mailContent = substring(0,150,$mailContent).'...';
									
									echo $mailContent;
									?>
								</div>
								<div class="timeline-footer">
									<a href="javascript:void(0);" data-url="{{ url(config('laraadmin.adminRoute') . '/updateConsultantMail/'.$result->id) }}" data-id="<?php echo $result->id; ?>" data-time="<?php echo $new_time; ?>" data-user="<?php echo $result->name; ?>" data-date="<?php echo $new_date; ?>" data-subject="<?php echo $result->subject; ?>" class="btn btn-primary btn-xs mail-read-more">Read more</a>
									<a href="javascript:void(0);" data-id="<?php echo $result->id; ?>" class="btn btn-danger btn-xs mail-delete">Delete</a>
									<form action="{{ url(config('laraadmin.adminRoute') . '/deleteConsultantMail/'.$result->id.'/'.$employee->id) }}" id="delete-mail-form-<?php echo $result->id; ?>" method="post">
										{{ csrf_field() }}
									</form>
								</div>
							</div>
						</li>
					<?php 
					}
					?>
					<li><i class="fa fa-clock-o bg-gray"></i></li>
				</ul>
				<ul class="timeline timeline-inverse" id="mail-details">
					<li class="time-label">
						<span class="bg-red"><?php echo $new_date; ?></span>
						<a href="javascript:void(0);" id="mail-back" class="btn btn-primary" style="float: right;margin-right: 15px;">Back</a>
					</li>
					<li>
						<i class="fa fa-envelope bg-blue"></i>
						<div class="timeline-item">
							<span class="time"><i class="fa fa-clock-o"></i><span></span></span>
							<h3 class="timeline-header header"><a href="javascript:void(0);"></a> has sended you an email</h3>
							<h3 class="timeline-header subject">Subject: &nbsp;<a href="javascript:void(0);"></a></h3>
							<div class="timeline-body"></div>
							<div class="timeline-footer">							
							</div>
						</div>
					</li>
					<li><i class="fa fa-clock-o bg-gray"></i></li>
				</ul>
			<?php
			}
			?>
		</div>
		
		@if(Entrust::hasRole("SUPER_ADMIN") || Entrust::hasRole("MANAGER"))
			<div role="tabpanel" class="tab-pane fade" id="tab-account-settings">
				<div class="tab-content">
					<form action="{{ url(config('laraadmin.adminRoute') . '/changeConsultantPass/'.$employee->id) }}" id="password-reset-form" class="general-form dashed-row white" method="post" accept-charset="utf-8">
						{{ csrf_field() }}
						<div class="panel">
							<div class="panel-default panel-heading">
								<h4>Account settings</h4>
							</div>
							<div class="panel-body">
								@if (count($errors) > 0)
									<div class="alert alert-danger">
										<ul>
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
								@if(Session::has('success_message'))
									<p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success_message') }}</p>
								@endif
								<div class="form-group">
									<label for="password" class=" col-md-2">Password</label>
									<div class=" col-md-10">
										<input type="password" name="password" value="" id="password" class="form-control" placeholder="Password" autocomplete="off" required="required" data-rule-minlength="6" data-msg-minlength="Please enter at least 6 characters.">
									</div>
								</div>
								<div class="form-group">
									<label for="password_confirmation" class=" col-md-2">Retype password</label>
									<div class=" col-md-10">
										<input type="password" name="password_confirmation" value="" id="password_confirmation" class="form-control" placeholder="Retype password" autocomplete="off" required="required" data-rule-equalto="#password" data-msg-equalto="Please enter the same value again.">
									</div>
								</div>
							</div>
							<div class="panel-footer">
								<button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> Change Password</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- set time for track time -->
			<div role="tabpanel" class="tab-pane fade" id="tab-account-settime">
				<div class="tab-content">
					<form action="{{ url(config('laraadmin.adminRoute') . '/setTime/'.$employee->id) }}" id="time-set-form" class="general-form dashed-row white" method="post" accept-charset="utf-8">
						{{ csrf_field() }}
						<div class="panel">
							<div class="panel-default panel-heading">
								<h4>Set time tab</h4>
							</div>
							<div class="panel-body">
								@if (count($errors) > 0)
									<div class="alert alert-danger">
										<ul>
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
								@if(Session::has('success_message'))
									<p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success_message') }}</p>
								@endif
								
								<input name="id" type="hidden" value="<?php echo (count($timeTrack) > 0) ? $timeTrack[0]->id : '0' ?>">
								<div class="form-group">
									<label for="from-time" class="col-md-2">Date</label>
									<div class="col-md-4">
										<input placeholder="Select date" value="<?php echo (count($timeTrack) > 0) ? $timeTrack[0]->track_date : ''; ?>" type="text" name="input_date" id="input_date" class="form-control" autocomplete="off" required>
									</div>
								</div>
								<div class="form-group">
									<label for="from-time" class="col-md-2">From-time</label>
									<div class="col-md-4">
										<input placeholder="Enter from-time" value="<?php echo (count($timeTrack) > 0) ? $timeTrack[0]->from_time : ''; ?>" type="text" name="input_starttime" id="input_starttime" class="form-control timepicker" autocomplete="off" required>
									</div>
								</div>
								<div class="form-group">
									<label for="password_confirmation" class=" col-md-2">To-time</label>
									<div class="col-md-4">
										<input placeholder="Enter end-time" value="<?php echo (count($timeTrack) > 0) ? $timeTrack[0]->to_time : ''; ?>" type="text" name="input_endtime" id="input_endtime" class="form-control timepicker" autocomplete="off" required>
									</div>
								</div>
								<div class="form-group">
									<label for="password_confirmation" class=" col-md-2">Reason</label>
									<div class="col-md-4">
										<textarea placeholder="Enter the reason for extra-time" class="form-control" name="reasons" autocomplete="off"><?php echo (count($timeTrack) > 0) ? $timeTrack[0]->reason : ''; ?></textarea>
									</div>
								</div>
							</div>
							<div class="panel-footer">
								<button type="submit" class="btn btn-primary"><span class="fa fa-check-circle"></span> Set Time</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		@endif

	</div>
	</div>
	</div>
	<meta name="csrf-token" content="{{ csrf_token() }}" />
</div>
@endsection

@push('scripts')
<script>
$(function () {
	$("#mail-details").hide();
	$('body').off('click', '.mail-read-more');
	$('body').on('click', '.mail-read-more', function() {
		$("#mail-details").show(); $("#mail-lists").hide();
		var mailDate = $(this).attr('data-date');
		var mailId = $(this).attr('data-id');
		var mailContent = <?php echo $mailDetailswithIds; ?>;
		mailContent = mailContent[mailId]['mail_content'];
		var mailSubject = $(this).attr('data-subject');
		var mailFrom = $(this).attr('data-user');
		var mailTime = $(this).attr('data-time');
		var url = $(this).attr('data-url');
		
		$("#mail-details .time-label bg-red").text(mailDate);
		$("#mail-details .timeline-body").html(mailContent);
		$("#mail-details .subject a").text(mailSubject);
		$("#mail-details .header a").text(mailFrom);
		$("#mail-details .time span").text(mailTime);
		
		// update the read count of mail
		$.ajax({
		   headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: url,
			type: "POST",
			data: {},
			success: function(data){
			}
		});
	});
	$('body').off('click', '#mail-back');
	$('body').on('click', '#mail-back', function() {
		$("#mail-details").hide(); $("#mail-lists").show();
	});
	$('body').off('click', '.mail-delete');
	$('body').on('click', '.mail-delete', function() {
		var mailId = $(this).attr('data-id');
		if(confirm('are you sure?')) {
			$('#delete-mail-form-'+mailId).submit();
		} else { return false; }
	});

    $('.timepicker').datetimepicker({
        format: 'hh:mm A',
    });
    $('#input_date').datetimepicker({
		format: 'DD/MM/YYYY'
    });
});
</script>
@endpush

