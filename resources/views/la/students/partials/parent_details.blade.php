<div id="tab_8" class="tab-pane" >
        
                   
<div class="content-head">
                                        
                                        <h4>Parent/Guardian's Details</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                        </div>
                        {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'personal-add-form']) !!}
                <div class="pane" >
                        <div class="row">
								<p>Mother's name</p>
                                <div class=" col s3 ipfield">
                                        <div class="input-field">
						<select name="mother_prefix" id="mother_prefix" class="formSelect">
							<option value="Mr">Mr.</option>
							<option value="Mrs">Mrs.</option>
						</select>
						<label for="mother_prefix">Prefix</label>
                                        </div>
                                </div>
                                <div class=" col s3 ipfield">
                                        <div class="input-field">
                                                <input name="mother_firstname" required id="mother_firstname" type="text" class="validate">
                                                <label for="mother_firstname">First Name</label>
                                        </div>


                                </div>
                                <div class=" col s3 ipfield">
                                        <div class="input-field">
                                                <input id="mother_middlename" name="mother_middlename" type="text" class="validate" required>
                                                <label for="mother_middlename">Middle Name</label>
                                        </div>


                                </div>
                                <div class=" col s3 ipfield">
                                        <div class="input-field">
                                                <input id="mother_lastname" name="mother_lastname" type="text" class="validate">
                                                <label for="mother_lastname">Last Name</label>
                                        </div>
                                 </div>

                        </div>
                        <div class="row">
                                <div class=" col s6 ipfield">
                                        <div class="input-field">
                                                <input name="mother_birth" required id="mother_birth" type="text" class="validate datepicker">
                                                <label for="mother_birth">Date of birth</label>
                                        </div>
                                </div>
                                <div class=" col s6 ipfield">
					<div class="input-field">
					<select id="mother_nation" name="mother_nation" class="searchSelect">
                                            <option value="">Select</option>
                                            <option value="Afghan"> Afghan </option>
                                            <option value="Albanian"> Albanian </option>
                                            <option value="Algerian"> Algerian </option>
                                            <option value="American"> American </option>
                                            <option value="Andorran"> Andorran </option>
                                            <option value="Angolan"> Angolan </option>
                                            <option value="Anguillan"> Anguillan </option>
                                            <option value="Citizen of Antigua and Barbuda"> Citizen of Antigua and Barbuda </option>
                                            <option value="Argentine"> Argentine </option>
                                            <option value="Armenian"> Armenian </option>
                                            <option value="Australian"> Australian </option>
                                            <option value="Austrian"> Austrian </option>
                                            <option value="Azerbaijani"> Azerbaijani </option>
                                            <option value="Bahamian"> Bahamian </option>
                                            <option value="Bahraini"> Bahraini </option>
                                            <option value="Bangladeshi"> Bangladeshi </option>
                                            <option value="Barbadian"> Barbadian </option>
                                            <option value="Belarusian"> Belarusian </option>
                                            <option value="Belgian"> Belgian </option>
                                            <option value="Belizean"> Belizean </option>
                                            <option value="Beninese"> Beninese </option>
                                            <option value="Bermudian"> Bermudian </option>
                                            <option value="Bhutanese"> Bhutanese </option>
                                            <option value="Bolivian"> Bolivian </option>
                                            <option value="Citizen of Bosnia and Herzegovina"> Citizen of Bosnia and Herzegovina </option>
                                            <option value="Botswanan"> Botswanan </option>
                                            <option value="Brazilian"> Brazilian </option>
                                            <option value="British"> British </option>
                                            <option value="British-Virgin-Islander"> British Virgin Islander </option>
                                            <option value="Bruneian"> Bruneian </option>
                                            <option value="Bulgarian"> Bulgarian </option>
                                            <option value="Burkinan"> Burkinan </option>
                                            <option value="Burmese"> Burmese </option>
                                            <option value="Burundian"> Burundian </option>
                                            <option value="Cambodian"> Cambodian </option>
                                            <option value="Cameroonian"> Cameroonian </option>
                                            <option value="Canadian Cape Verdean"> Canadian Cape Verdean </option>
                                            <option value="Cayman Islander"> Cayman Islander </option>
                                            <option value="Central African"> Central African </option>
                                            <option value="Chadian"> Chadian </option>
                                            <option value="Chilean"> Chilean </option>
                                            <option value="Chinese"> Chinese </option>
                                            <option value="Colombian"> Colombian </option>
                                            <option value="Comoran Congolese (Congo)"> Comoran Congolese (Congo) </option>
                                            <option value="Congolese (DRC)"> Congolese (DRC) </option>
                                            <option value="Cook Islander"> Cook Islander </option>
                                            <option value="Costa Rican"> Costa Rican </option>
                                            <option value="Croatian"> Croatian </option>
                                            <option value="Cuban"> Cuban </option>
                                            <option value="Cymraes"> Cymraes </option>
                                            <option value="Cymro"> Cymro </option>
                                            <option value="Cypriot"> Cypriot </option>
                                            <option value="Czech"> Czech </option>
                                            <option value="Danish"> Danish </option>
                                            <option value="Djiboutian"> Djiboutian </option>
                                            <option value="Dominican"> Dominican </option>
                                            <option value="Citizen of the Dominican Republic"> Citizen of the Dominican Republic </option>
                                            <option value="Dutch"> Dutch </option>
                                            <option value="East Timorese"> East Timorese </option>
                                            <option value="Ecuadorean"> Ecuadorean </option>
                                            <option value="Egyptian"> Egyptian </option>
                                            <option value="Emirati"> Emirati </option>
                                            <option value="English"> English </option>
                                            <option value="Equatorial Guinean"> Equatorial Guinean </option>
                                            <option value="Eritrean"> Eritrean </option>
                                            <option value="Estonian"> Estonian </option>
                                            <option value="Ethiopian"> Ethiopian </option>
                                            <option value="Faroese"> Faroese </option>
                                            <option value="Fijian"> Fijian </option>
                                            <option value="Filipino"> Filipino </option>
                                            <option value="Finnish"> Finnish </option>
                                            <option value="French"> French </option>
                                            <option value="Gabonese"> Gabonese </option>
                                            <option value="Gambian"> Gambian </option>
                                            <option value="Georgian"> Georgian </option>
                                            <option value="German"> German </option>
                                            <option value="Ghanaian"> Ghanaian </option>
                                            <option value="Gibraltarian"> Gibraltarian </option>
                                            <option value="Greek"> Greek </option>
                                            <option value="Greenlandic"> Greenlandic </option>
                                            <option value="Grenadian"> Grenadian </option>
                                            <option value="Guamanian"> Guamanian </option>
                                            <option value="Guatemalan"> Guatemalan </option>
                                            <option value="Citizen of Guinea-Bissau"> Citizen of Guinea-Bissau </option>
                                            <option value="Guinean Guyanese"> Guinean Guyanese </option>
                                            <option value="Haitian"> Haitian </option>
                                            <option value="Honduran"> Honduran </option>
                                            <option value="Hong Konger"> Hong Konger </option>
                                            <option value="Hungarian"> Hungarian </option>
                                            <option value="Icelandic"> Icelandic </option>
                                            <option value="Indian"> Indian </option>
                                            <option value="Indonesian"> Indonesian </option>
                                            <option value="Iranian"> Iranian </option>
                                            <option value="Iraqi"> Iraqi </option>
                                            <option value="Irish"> Irish </option>
                                            <option value="Israeli"> Israeli </option>
                                            <option value="Italian"> Italian </option>
                                            <option value="Ivorian"> Ivorian </option>
                                            <option value="Jamaican"> Jamaican </option>
                                            <option value="Japanese"> Japanese </option>
                                            <option value="Jordanian"> Jordanian </option>
                                            <option value="Kazakh"> Kazakh </option>
                                            <option value="Kenyan"> Kenyan </option>
                                            <option value="Kittitian"> Kittitian </option>
                                            <option value="Citizen of Kiribati"> Citizen of Kiribati </option>
                                            <option value="Kosovan"> Kosovan </option>
                                            <option value="Kuwaiti"> Kuwaiti </option>
                                            <option value="Kyrgyz"> Kyrgyz </option>
                                            <option value="Lao"> Lao </option>
                                            <option value="Latvian"> Latvian </option>
                                            <option value="Lebanese"> Lebanese </option>
                                            <option value="Liberian"> Liberian </option>
                                            <option value="Libyan"> Libyan </option>
                                            <option value="Liechtenstein citizen"> Liechtenstein citizen </option>
                                            <option value="Lithuanian"> Lithuanian </option>
                                            <option value="Luxembourger"> Luxembourger </option>
                                            <option value="Macanese"> Macanese </option>
                                            <option value="Macedonian"> Macedonian </option>
                                            <option value="Malagasy"> Malagasy </option>
                                            <option value="Malawian"> Malawian </option>
                                            <option value="Malaysian"> Malaysian </option>
                                            <option value="Maldivian"> Maldivian </option>
                                            <option value="Malian"> Malian </option>
                                            <option value="Maltese"> Maltese </option>
                                            <option value="Marshallese"> Marshallese </option>
                                            <option value="Martiniquais"> Martiniquais </option>
                                            <option value="Mauritanian"> Mauritanian </option>
                                            <option value="Mauritian"> Mauritian </option>
                                            <option value="Mexican"> Mexican </option>
                                            <option value="Micronesian"> Micronesian </option>
                                            <option value="Moldovan"> Moldovan </option>
                                            <option value="Monegasque"> Monegasque </option>
                                            <option value="Mongolian"> Mongolian </option>
                                            <option value="Montenegrin"> Montenegrin </option>
                                            <option value="Montserratian"> Montserratian </option>
                                            <option value="Moroccan"> Moroccan </option>
                                            <option value="Mosotho"> Mosotho </option>
                                            <option value="Mozambican"> Mozambican </option>
                                            <option value="Namibian"> Namibian </option>
                                            <option value="Nauruan"> Nauruan </option>
                                            <option value="Nepalese"> Nepalese </option>
                                            <option value="New Zealander"> New Zealander </option>
                                            <option value="Nicaraguan"> Nicaraguan </option>
                                            <option value="Nigerian"> Nigerian </option>
                                            <option value="Nigerien"> Nigerien </option>
                                            <option value="Niuean"> Niuean </option>
                                            <option value="North Korean"> North Korean </option>
                                            <option value="Northern Irish"> Northern Irish </option>
                                            <option value="Norwegian"> Norwegian </option>
                                            <option value="Omani"> Omani </option>
                                            <option value="Pakistani"> Pakistani </option>
                                            <option value="Palauan"> Palauan </option>
                                            <option value="Palestinian"> Palestinian </option>
                                            <option value="Panamanian"> Panamanian </option>
                                            <option value="Papua New Guinean"> Papua New Guinean </option>
                                            <option value="Paraguayan"> Paraguayan </option>
                                            <option value="Peruvian"> Peruvian </option>
                                            <option value="Pitcairn Islander"> Pitcairn Islander </option>
                                            <option value="Polish"> Polish </option>
                                            <option value="Portuguese"> Portuguese </option>
                                            <option value="Prydeinig Puerto Rican"> Prydeinig Puerto Rican </option>
                                            <option value="Qatari"> Qatari </option>
                                            <option value="Romanian"> Romanian </option>
                                            <option value="Russian"> Russian </option>
                                            <option value="Rwandan"> Rwandan </option>
                                            <option value="Salvadorean"> Salvadorean </option>
                                            <option value="Sammarinese"> Sammarinese </option>
                                            <option value="Samoan"> Samoan </option>
                                            <option value="Sao Tomean"> Sao Tomean </option>
                                            <option value="Saudi Arabian"> Saudi Arabian </option>
                                            <option value="Scottish"> Scottish </option>
                                            <option value="Senegalese"> Senegalese </option>
                                            <option value="Serbian"> Serbian </option>
                                            <option value="Citizen of Seychelles"> Citizen of Seychelles </option>
                                            <option value="Sierra Leonean"> Sierra Leonean </option>
                                            <option value="Singaporean"> Singaporean </option>
                                            <option value="Slovak"> Slovak </option>
                                            <option value="Slovenian Solomon Islander"> Slovenian Solomon Islander </option>
                                            <option value="Somali"> Somali </option>
                                            <option value="South African"> South African </option>
                                            <option value="South Korean"> South Korean </option>
                                            <option value="South Sudanese"> South Sudanese </option>
                                            <option value="Spanish"> Spanish </option>
                                            <option value="Sri Lankan"> Sri Lankan </option>
                                            <option value="St Helenian"> St Helenian </option>
                                            <option value="St Lucian"> St Lucian </option>
                                            <option value="Stateless"> Stateless </option>
                                            <option value="Sudanese"> Sudanese </option>
                                            <option value="Surinamese"> Surinamese </option>
                                            <option value="Swazi"> Swazi </option>
                                            <option value="Swedish"> Swedish </option>
                                            <option value="Swiss"> Swiss </option>
                                            <option value="Syrian"> Syrian </option>
                                            <option value="Taiwanese"> Taiwanese </option>
                                            <option value="Tajik"> Tajik </option>
                                            <option value="Tanzanian"> Tanzanian </option>
                                            <option value="Thai"> Thai </option>
                                            <option value="Togolese"> Togolese </option>
                                            <option value="Tongan"> Tongan </option>
                                            <option value="Trinidadian"> Trinidadian </option>
                                            <option value="Tristanian"> Tristanian </option>
                                            <option value="Tunisian"> Tunisian </option>
                                            <option value="Turkish"> Turkish </option>
                                            <option value="Turkmen"> Turkmen </option>
                                            <option value="Turks and Caicos Islander"> Turks and Caicos Islander </option>
                                            <option value="Tuvaluan"> Tuvaluan </option>
                                            <option value="Ugandan"> Ugandan </option>
                                            <option value="Ukrainian"> Ukrainian </option>
                                            <option value="Uruguayan"> Uruguayan </option>
                                            <option value="Uzbek"> Uzbek </option>
                                            <option value="Vatican citizen Citizen of Vanuatu"> Vatican citizen Citizen of Vanuatu </option>
                                            <option value="Venezuelan"> Venezuelan </option>
                                            <option value="Vietnamese"> Vietnamese </option>
                                            <option value="Vincentian"> Vincentian </option>
                                            <option value="Wallisian"> Wallisian </option>
                                            <option value="Welsh"> Welsh </option>
                                            <option value="Yemeni"> Yemeni </option>
                                            <option value="Zambian"> Zambian </option>
                                            <option value="Zimbabwean"> Zimbabwean </option>
					</select>
					<label for="mother_nation" style="margin-top: -40px;font-size: 13px;">Nationality</label>
				</div>
			</div>
                        </div>
                        <div class="row">
				<p>Father's name</p>
                                <div class=" col s3 ipfield">
                                        <div class="input-field">
						<select name="father_prefix" id="father_prefix" class="formSelect">
							<option value="Mr">Mr.</option>
							<option value="Mrs">Mrs.</option>
						</select>
						<label for="father_prefix">Prefix</label>
                                        </div>
                                </div>
                                <div class=" col s3 ipfield">
                                        <div class="input-field">
                                                <input name="father_firstname" required id="father_firstname" type="text" class="validate">
                                                <label for="father_firstname">First Name</label>
                                        </div>
                                </div>
                                <div class=" col s3 ipfield">
                                        <div class="input-field">
                                                <input id="father_middlename" name="father_middlename" type="text" class="validate" required>
                                                <label for="father_middlename">Middle Name</label>
                                        </div>
                                </div>
                                <div class=" col s3 ipfield">
                                        <div class="input-field">
                                                <input id="father_lastname" name="father_lastname" type="text" class="validate">
                                                <label for="father_lastname">Last Name</label>
                                        </div>
                                </div>

                        </div>
                        <div class="row">
                                <div class=" col s6 ipfield">
                                        <div class="input-field">
                                                <input name="father_birth" required id="father_birth" type="text" class="validate datepicker">
                                                <label for="father_birth">Date of birth</label>
                                        </div>
                                </div>
                        <div class=" col s6 ipfield">
	<div class="input-field">
	    <select id="father_nation" name="father_nation" class="searchSelect">
            <option value="">Select</option>
            <option value="Afghan"> Afghan </option>
            <option value="Albanian"> Albanian </option>
            <option value="Algerian"> Algerian </option>
            <option value="American"> American </option>
            <option value="Andorran"> Andorran </option>
            <option value="Angolan"> Angolan </option>
            <option value="Anguillan"> Anguillan </option>
            <option value="Citizen of Antigua and Barbuda"> Citizen of Antigua and Barbuda </option>
            <option value="Argentine"> Argentine </option>
            <option value="Armenian"> Armenian </option>
            <option value="Australian"> Australian </option>
            <option value="Austrian"> Austrian </option>
            <option value="Azerbaijani"> Azerbaijani </option>
            <option value="Bahamian"> Bahamian </option>
            <option value="Bahraini"> Bahraini </option>
            <option value="Bangladeshi"> Bangladeshi </option>
            <option value="Barbadian"> Barbadian </option>
            <option value="Belarusian"> Belarusian </option>
            <option value="Belgian"> Belgian </option>
            <option value="Belizean"> Belizean </option>
            <option value="Beninese"> Beninese </option>
            <option value="Bermudian"> Bermudian </option>
            <option value="Bhutanese"> Bhutanese </option>
            <option value="Bolivian"> Bolivian </option>
            <option value="Citizen of Bosnia and Herzegovina"> Citizen of Bosnia and Herzegovina </option>
            <option value="Botswanan"> Botswanan </option>
            <option value="Brazilian"> Brazilian </option>
            <option value="British"> British </option>
            <option value="British-Virgin-Islander"> British Virgin Islander </option>
            <option value="Bruneian"> Bruneian </option>
            <option value="Bulgarian"> Bulgarian </option>
            <option value="Burkinan"> Burkinan </option>
            <option value="Burmese"> Burmese </option>
            <option value="Burundian"> Burundian </option>
            <option value="Cambodian"> Cambodian </option>
            <option value="Cameroonian"> Cameroonian </option>
            <option value="Canadian Cape Verdean"> Canadian Cape Verdean </option>
            <option value="Cayman Islander"> Cayman Islander </option>
            <option value="Central African"> Central African </option>
            <option value="Chadian"> Chadian </option>
            <option value="Chilean"> Chilean </option>
            <option value="Chinese"> Chinese </option>
            <option value="Colombian"> Colombian </option>
            <option value="Comoran Congolese (Congo)"> Comoran Congolese (Congo) </option>
            <option value="Congolese (DRC)"> Congolese (DRC) </option>
            <option value="Cook Islander"> Cook Islander </option>
            <option value="Costa Rican"> Costa Rican </option>
            <option value="Croatian"> Croatian </option>
            <option value="Cuban"> Cuban </option>
            <option value="Cymraes"> Cymraes </option>
            <option value="Cymro"> Cymro </option>
            <option value="Cypriot"> Cypriot </option>
            <option value="Czech"> Czech </option>
            <option value="Danish"> Danish </option>
            <option value="Djiboutian"> Djiboutian </option>
            <option value="Dominican"> Dominican </option>
            <option value="Citizen of the Dominican Republic"> Citizen of the Dominican Republic </option>
            <option value="Dutch"> Dutch </option>
            <option value="East Timorese"> East Timorese </option>
            <option value="Ecuadorean"> Ecuadorean </option>
            <option value="Egyptian"> Egyptian </option>
            <option value="Emirati"> Emirati </option>
            <option value="English"> English </option>
            <option value="Equatorial Guinean"> Equatorial Guinean </option>
            <option value="Eritrean"> Eritrean </option>
            <option value="Estonian"> Estonian </option>
            <option value="Ethiopian"> Ethiopian </option>
            <option value="Faroese"> Faroese </option>
            <option value="Fijian"> Fijian </option>
            <option value="Filipino"> Filipino </option>
            <option value="Finnish"> Finnish </option>
            <option value="French"> French </option>
            <option value="Gabonese"> Gabonese </option>
            <option value="Gambian"> Gambian </option>
            <option value="Georgian"> Georgian </option>
            <option value="German"> German </option>
            <option value="Ghanaian"> Ghanaian </option>
            <option value="Gibraltarian"> Gibraltarian </option>
            <option value="Greek"> Greek </option>
            <option value="Greenlandic"> Greenlandic </option>
            <option value="Grenadian"> Grenadian </option>
            <option value="Guamanian"> Guamanian </option>
            <option value="Guatemalan"> Guatemalan </option>
            <option value="Citizen of Guinea-Bissau"> Citizen of Guinea-Bissau </option>
            <option value="Guinean Guyanese"> Guinean Guyanese </option>
            <option value="Haitian"> Haitian </option>
            <option value="Honduran"> Honduran </option>
            <option value="Hong Konger"> Hong Konger </option>
            <option value="Hungarian"> Hungarian </option>
            <option value="Icelandic"> Icelandic </option>
            <option value="Indian"> Indian </option>
            <option value="Indonesian"> Indonesian </option>
            <option value="Iranian"> Iranian </option>
            <option value="Iraqi"> Iraqi </option>
            <option value="Irish"> Irish </option>
            <option value="Israeli"> Israeli </option>
            <option value="Italian"> Italian </option>
            <option value="Ivorian"> Ivorian </option>
            <option value="Jamaican"> Jamaican </option>
            <option value="Japanese"> Japanese </option>
            <option value="Jordanian"> Jordanian </option>
            <option value="Kazakh"> Kazakh </option>
            <option value="Kenyan"> Kenyan </option>
            <option value="Kittitian"> Kittitian </option>
            <option value="Citizen of Kiribati"> Citizen of Kiribati </option>
            <option value="Kosovan"> Kosovan </option>
            <option value="Kuwaiti"> Kuwaiti </option>
            <option value="Kyrgyz"> Kyrgyz </option>
            <option value="Lao"> Lao </option>
            <option value="Latvian"> Latvian </option>
            <option value="Lebanese"> Lebanese </option>
            <option value="Liberian"> Liberian </option>
            <option value="Libyan"> Libyan </option>
            <option value="Liechtenstein citizen"> Liechtenstein citizen </option>
            <option value="Lithuanian"> Lithuanian </option>
            <option value="Luxembourger"> Luxembourger </option>
            <option value="Macanese"> Macanese </option>
            <option value="Macedonian"> Macedonian </option>
            <option value="Malagasy"> Malagasy </option>
            <option value="Malawian"> Malawian </option>
            <option value="Malaysian"> Malaysian </option>
            <option value="Maldivian"> Maldivian </option>
            <option value="Malian"> Malian </option>
            <option value="Maltese"> Maltese </option>
            <option value="Marshallese"> Marshallese </option>
            <option value="Martiniquais"> Martiniquais </option>
            <option value="Mauritanian"> Mauritanian </option>
            <option value="Mauritian"> Mauritian </option>
            <option value="Mexican"> Mexican </option>
            <option value="Micronesian"> Micronesian </option>
            <option value="Moldovan"> Moldovan </option>
            <option value="Monegasque"> Monegasque </option>
            <option value="Mongolian"> Mongolian </option>
            <option value="Montenegrin"> Montenegrin </option>
            <option value="Montserratian"> Montserratian </option>
            <option value="Moroccan"> Moroccan </option>
            <option value="Mosotho"> Mosotho </option>
            <option value="Mozambican"> Mozambican </option>
            <option value="Namibian"> Namibian </option>
            <option value="Nauruan"> Nauruan </option>
            <option value="Nepalese"> Nepalese </option>
            <option value="New Zealander"> New Zealander </option>
            <option value="Nicaraguan"> Nicaraguan </option>
            <option value="Nigerian"> Nigerian </option>
            <option value="Nigerien"> Nigerien </option>
            <option value="Niuean"> Niuean </option>
            <option value="North Korean"> North Korean </option>
            <option value="Northern Irish"> Northern Irish </option>
            <option value="Norwegian"> Norwegian </option>
            <option value="Omani"> Omani </option>
            <option value="Pakistani"> Pakistani </option>
            <option value="Palauan"> Palauan </option>
            <option value="Palestinian"> Palestinian </option>
            <option value="Panamanian"> Panamanian </option>
            <option value="Papua New Guinean"> Papua New Guinean </option>
            <option value="Paraguayan"> Paraguayan </option>
            <option value="Peruvian"> Peruvian </option>
            <option value="Pitcairn Islander"> Pitcairn Islander </option>
            <option value="Polish"> Polish </option>
            <option value="Portuguese"> Portuguese </option>
            <option value="Prydeinig Puerto Rican"> Prydeinig Puerto Rican </option>
            <option value="Qatari"> Qatari </option>
            <option value="Romanian"> Romanian </option>
            <option value="Russian"> Russian </option>
            <option value="Rwandan"> Rwandan </option>
            <option value="Salvadorean"> Salvadorean </option>
            <option value="Sammarinese"> Sammarinese </option>
            <option value="Samoan"> Samoan </option>
            <option value="Sao Tomean"> Sao Tomean </option>
            <option value="Saudi Arabian"> Saudi Arabian </option>
            <option value="Scottish"> Scottish </option>
            <option value="Senegalese"> Senegalese </option>
            <option value="Serbian"> Serbian </option>
            <option value="Citizen of Seychelles"> Citizen of Seychelles </option>
            <option value="Sierra Leonean"> Sierra Leonean </option>
            <option value="Singaporean"> Singaporean </option>
            <option value="Slovak"> Slovak </option>
            <option value="Slovenian Solomon Islander"> Slovenian Solomon Islander </option>
            <option value="Somali"> Somali </option>
            <option value="South African"> South African </option>
            <option value="South Korean"> South Korean </option>
            <option value="South Sudanese"> South Sudanese </option>
            <option value="Spanish"> Spanish </option>
            <option value="Sri Lankan"> Sri Lankan </option>
            <option value="St Helenian"> St Helenian </option>
            <option value="St Lucian"> St Lucian </option>
            <option value="Stateless"> Stateless </option>
            <option value="Sudanese"> Sudanese </option>
            <option value="Surinamese"> Surinamese </option>
            <option value="Swazi"> Swazi </option>
            <option value="Swedish"> Swedish </option>
            <option value="Swiss"> Swiss </option>
            <option value="Syrian"> Syrian </option>
            <option value="Taiwanese"> Taiwanese </option>
            <option value="Tajik"> Tajik </option>
            <option value="Tanzanian"> Tanzanian </option>
            <option value="Thai"> Thai </option>
            <option value="Togolese"> Togolese </option>
            <option value="Tongan"> Tongan </option>
            <option value="Trinidadian"> Trinidadian </option>
            <option value="Tristanian"> Tristanian </option>
            <option value="Tunisian"> Tunisian </option>
            <option value="Turkish"> Turkish </option>
            <option value="Turkmen"> Turkmen </option>
            <option value="Turks and Caicos Islander"> Turks and Caicos Islander </option>
            <option value="Tuvaluan"> Tuvaluan </option>
            <option value="Ugandan"> Ugandan </option>
            <option value="Ukrainian"> Ukrainian </option>
            <option value="Uruguayan"> Uruguayan </option>
            <option value="Uzbek"> Uzbek </option>
            <option value="Vatican citizen Citizen of Vanuatu"> Vatican citizen Citizen of Vanuatu </option>
            <option value="Venezuelan"> Venezuelan </option>
            <option value="Vietnamese"> Vietnamese </option>
            <option value="Vincentian"> Vincentian </option>
            <option value="Wallisian"> Wallisian </option>
            <option value="Welsh"> Welsh </option>
            <option value="Yemeni"> Yemeni </option>
            <option value="Zambian"> Zambian </option>
            <option value="Zimbabwean"> Zimbabwean </option>											
	</select>
	<label for="father_nation" style="margin-top: -40px;font-size: 13px;">Nationality</label>
	</div>
</div>
</div>
</div>
                        <div class="controls">
                                <a class="   mgbtn2 back_button">Back</a>
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="parent_det">
                                 <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                              
                                {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                        </div>
                        {{ Form::close() }}
</div>