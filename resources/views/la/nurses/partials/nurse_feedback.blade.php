<div id="tab_11" class="tab-pane " >
                                
                                <div class="content-head">
                                                <h4>Feedback</h4>
                                                <!-- <div class="id">Nurse ID : <b>#12675</b></div> -->
                                        </div>
                                        {!! Form::open(['action' => 'LA\NursesController@store', 'id' => 'agentselect-add-form']) !!}
               
                        <div class="pane">
							<h4>Tell us how it went</h4>
							<div class="row">
								<div class="col s12">
									<div class="input-field">
										<textarea id="feedback" name="feedback" class="materialize-textarea"></textarea>
										<label for="passed_date">Feedback</label>
									</div>    
								</div>	
							</div>
                        </div>
                        <div class="controls">
                                        <a class="mgbtn2">Back</a>
                                        <input id="form_name" type="hidden" class="validate" name="type_of" value="feedback">
                                        <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                        {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
                </div>

                