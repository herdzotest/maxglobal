<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use Dwij\Laraadmin\Helpers\LAHelper;
use Dwij\Laraadmin\Models\LAConfigs;

use App\User;
use App\Models\Employee;
use App\Role;
use Mail;
use Log;

class CountriesController extends Controller
{
	public function __construct() {
		
		if(Auth::user()->type != 'SUPER_ADMIN' && Auth::user()->type != 'MANAGER')
			return abort(403);		
	}
	
	/**
	 * Display a listing of the Employees.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$countries = DB::table('countries')->orderBy('id')->get();
		return View('la.countries.index', [
			'countries' => $countries
		]);
	}
	
	public function show()
	{
		/*
		$courses = DB::table('manage_courses')->orderBy('id')->get();
		return View('la.courses.index', [
			'courses' => $courses
		]);
		*/
	}

	/**
	 * Store a newly created employee in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(count($request->all()) > 0) {
			
			$postedData = $request->all();
			$countries = DB::table('countries')->find($postedData['country_id']);

			if($countries) {
				DB::table('countries')->where('id', $postedData['country_id'])
					->update([
						'country' => $postedData['country'],
						'short_name' => $postedData['short_name'],
						'status' => $postedData['status']
					]);
			} else {			
				DB::table('countries')->insert([
					'country' => $postedData['country'],
					'short_name' => $postedData['short_name'],
					'status' => $postedData['status']
				]);
			}
		}
		
		return redirect(config('laraadmin.adminRoute')."/countries");
	}

	/**
	 * Remove the specified employee from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if($id != '') {
			$countries = DB::table('countries')->find($id);
			if($countries) {
				DB::table('countries')->where('id', '=', $id)->delete();
			}
		}
		
		return redirect(config('laraadmin.adminRoute')."/countries");
	}
	
}
