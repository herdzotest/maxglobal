<div id="tab_1" class="tab-pane active" >
        
                   
<div class="content-head">
                                        
                                        <h4>Personal Information</h4>
                                        <!-- <div class="id" id="nurse_id">Nurse ID : <b>#12675</b></div>-->
                        </div>
		<div class="pane" >
			<div class="academic">
                 <div class="mgv-field">
                        <div class="name">
                                 Title 
                        </div>
                        <div class="value" id="title"></div>
                 </div>

                 <div class="mgv-field">
                        <div class="name">
                                 First Name (as per passport)
                        </div>
                        <div class="value" id="first_name">
                        </div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                                 Middle Name (as per passport)   
                        </div>
                        <div class="value" id="middle_name">
                        </div>
                 </div>
                 <div class="mgv-field">
                        <div class="name" >
                                 Sur Name (as per passport)   
                        </div>
                        <div class="value" id="sur_name">
                        </div>
                 </div>
                 <div class="mgv-field">
                        <div class="name" >
                                Nationality   
                        </div>
                        <div class="value" id="nationality">
                        </div>
                 </div>	
                 <div class="mgv-field">
                        <div class="name">
                                 Gender 
                        </div>
                        <div class="value" id="gender">
                        </div>
                 </div>				 
                 <div class="mgv-field">
                        <div class="name">
                                 Date of Birth 
                        </div>
                        <div class="value" id="dob">
                        </div>
                 </div>
                 <div class="mgv-field">
					<div class="name">Profile Image </div>
                                   <div class="value">
                                   <div class="uploaded-file" id="profile_image" data-type="file"></div>
                                   </div>
					
                 </div>
				 
                 <div class="mgv-field">
					<div class="name">Home Address (as per passport)</div>
					<div class="value">
						<span id="home_address"></span><br>
						<span id="address_2"></span><br>
						<span id="city"></span><br>
						<span id="state"></span><br>
						<span id="zip_code"></span>
						<span id="country"></span>
					</div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                            Is this your correspondence address? 
                        </div>
                        <div class="value" id="correspondence_address_flag">
                        </div>
                 </div>
                 <div class="mgv-field">
					<div class="name">Home Address (as per passport)</div>
					<div class="value">
						<span id="correspondence_addr_1"></span><br>
						<span id="correspondence_addr_2"></span><br>
						<span id="correspondence_city"></span><br>
						<span id="correspondence_state"></span><br>
						<span id="correspondence_zip_code"></span>
						<span id="correspondence_country"></span>
					</div>
                 </div>	
                 <div class="mgv-field">
                        <div class="name">
                            Have you lived at any other address in past 2 years? 
                        </div>
                        <div class="value" id="lived_othe_addr">
                        </div>
                 </div>	
                 <div class="mgv-field">
					<div class="name">Home Address (as per passport)</div>
					<div class="value">
						<span id="other_address_1"></span><br>
						<span id="other_address_2"></span><br>
						<span id="other_city"></span><br>
						<span id="other_state"></span><br>
						<span id="other_zip_code"></span>
						<span id="other_country"></span>
					</div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                            When did you start living at this address? 
                        </div>
                        <div class="value" id="start_living_addr">
                        </div>
                 </div>	
                 <div class="mgv-field">
                        <div class="name">
                            When did you stop living at this address? 
                        </div>
                        <div class="value" id="stop_living_addr">
                        </div>
                 </div>					 
                 <div class="mgv-field">
                        <div class="name">
                                Mobile Number 
                        </div>
                        <div class="value" id="mobile">
                        </div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                            WhatsApp Number 
                        </div>
                        <div class="value" id="whatsapp">
                        </div>
                 </div>	
                 <div class="mgv-field">
                        <div class="name">
                            IMO Number 
                        </div>
                        <div class="value" id="imo_no">
                        </div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                            Botim Number 
                        </div>
                        <div class="value" id="btm_number">
                        </div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                            Skype ID 
                        </div>
                        <div class="value" id="skype_id">
                        </div>
                 </div>	
                 <div class="mgv-field">
					   <div class="name">Skype ID Profile Screenshots </div>
					   <div class="value">
							<div class=" uploaded-file" id="skype_picture" data-type="file"></div>	
					   </div>				
                 </div>
                 <div class="mgv-field">
					<div class="name">Passport details</div>
					<div class="value">
						<span id="passport_no"></span><br>
						<span id="date_issue"></span><br>
						<span id="date_expiry"></span>
					</div>
                 </div>				 
                 <div class="mgv-field">
                                   <div class="name">Passport File (Passport front page & back page) </div>
                                   <div class="value">

					<div class="uploaded-file" id="passport_upload" data-type="file"></div>
					<div class="uploaded-file" id="passport_back" data-type="file"></div>
					<div class="file-approve-buttons ">
						<!-- <a href="#" class="btn btn-danger mx-approve-doc" data-type="image_front" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
						<a href="#" class="btn btn-success mx-approve-doc" data-type="image_front" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a> -->
                        <a href="#" class="btn btn-danger mx-approve-doc" data-type="passport_upload" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
                        <a href="#" class="btn btn-success mx-approve-doc" data-type="passport_upload" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
                                   </div>  	
                                   </div>				
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                            Do you have any disability or other condition which might necessitate special arrangement or facilities? 
                        </div>
                        <div class="value">
							<span id="any_disability"></span><br>
							<span id="disability_comment"></span>
						</div>
                 </div>	
            </div>
		</div>
</div>