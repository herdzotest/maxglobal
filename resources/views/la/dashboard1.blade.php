@extends('la.layouts.app')

@section('htmlheader_title') Dashboard @endsection
@section('contentheader_title') Dashboard @endsection
@section('contentheader_description') Organisation Overview @endsection

@section('main-content')
<!-- Main content -->
        <section class="content">
        <div class="row">
        <div class="col-md-6">
           <div class="mg-card namecard">
            <img src="{{asset('/la-assets/img/1.jpg')}}" alt="">
            <div class="info">
              <h3>John Doe</h3>
              <p>Admin</p>

                <div><i class="fa fa-envelope"></i> johndoe@gmail.com</div>
                <div><i class="fa fa-phone"></i> +91985553225</div>

          </div>
            </div>
        </div>
        <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Students Pending Approval
            </div>
            <div class="number">
              9
            </div>
          </a>
          </div>
          <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Students Pending Approval
            </div>
            <div class="number">
             11
            </div>
          </a>
          </div>
          <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Students Pending Approval
            </div>
            <div class="number">
             8
            </div>
          </a>
          </div>
          <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Students Pending Approval
            </div>
            <div class="number">
              48
            </div>
          </a>
          </div>
          <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Students Pending Approval
            </div>
            <div class="number">
              37
            </div>
          </a>
          </div>
          <div class="col-md-3">
          <a href="#" class="mg-card">
            <div class="title">
              Students Pending Approval
            </div>
            <div class="number">
              21
            </div>
          </a>
          </div>
		
        </div>
        </section>

@endsection

@push('styles')
<!-- Morris chart -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/morris/morris.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ asset('la-assets/plugins/morris/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush

@push('scripts')
<script>
(function($) {
	$('body').pgNotification({
		style: 'circle',
		title: 'Schol Lab',
		message: "Welcome to Schol Lab...",
		position: "top-right",
		timeout: 0,
		type: "success",
		thumbnail: '<img width="40" height="40" style="display: inline-block;" src="{{ Gravatar::fallback(asset('la-assets/img/user2-160x160.jpg'))->get(Auth::user()->email, 'default') }}" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" alt="">'
	}).show();
})(window.jQuery);
</script>
@endpush