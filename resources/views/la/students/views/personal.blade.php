<div id="tab_1" class="tab-pane active" >
        
                   
<div class="content-head">
                                        
                                        <h4>Personal Information</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                        </div>
		<div class="pane" >
			<div class="academic">
                 <div class="mgv-field">
                        <div class="name">
                                 Prefix 
                        </div>
                        <div class="value" id="prefix"></div>
                 </div>

                 <div class="mgv-field">
                        <div class="name">
                                 First Name  
                        </div>
                        <div class="value" id="first_name">
                        </div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                                 Middle Name   
                        </div>
                        <div class="value" id="middle_name">
                        </div>
                 </div>
                 <div class="mgv-field">
                        <div class="name" >
                                 Sur Name   
                        </div>
                        <div class="value" id="sur_name">
                        </div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                                 Date of Birth 
                        </div>
                        <div class="value" id="dob">
                        </div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                                 Gender 
                        </div>
                        <div class="value" id="gender">
                        </div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                                 Age 
                        </div>
                        <div class="value" id="age">
                        </div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                                 Contact Number 
                        </div>
                        <div class="value">
							<span id="coountry_code"></span> <span> - </span> <span id="mobile"></span>
                        </div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                                 Whatsapp Number 
                        </div>
                        <div class="value">
                     <span id="coountry_code_whatsapp"></span> <span> - </span> <span id="whatsapp"></span>
                        </div>
                 </div>
                 <div class="mgv-field">
					<div class="name">Profile Image </div>
                                   <div class="value">
                                   <div class=" uploaded-file" id="profile_image" data-type="file"></div>
                                   </div>
					
                 </div>
                 <div class="mgv-field">
                                   <div class="name">Passport Front Page </div>
                                   <div class="value">

					<div class=" uploaded-file" id="image_front" data-type="file"></div>
					<div class="file-approve-buttons ">
						<a href="#" class="btn btn-danger  mgb-rej mx-approve-doc" data-type="passport-front" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
						<a href="#" class="btn btn-success  mgb-app mx-approve-doc" data-type="passport-front" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
                                   </div>  	
                                   </div>				
                 </div>
                 <div class="mgv-field">
                                   <div class="name">Passport Back Page </div>
                                   <div class="value">
                                   <div class=" uploaded-file" id="image_back" data-type="file"></div>
                                   
					<div class="file-approve-buttons ">
						<a href="#" class="btn btn-danger mgb-rej mx-approve-doc" data-type="passport-back" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
						<a href="#" class="btn btn-success mgb-app mx-approve-doc" data-type="passport-back" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
                                   </div>  
                                   </div>					
                 </div>
                 <div class="mgv-field">
					<div class="name">Current Address</div>
					<div class="value">
						<span id="address"></span><br>
						<span id="address_2"></span><br>
						<span id="city"></span><br>
						<span id="state"></span><br>
						<span id="zip_code"></span>
					</div>
                 </div>
				 
                <div class="mgv-field">
					<div class="name">Country</div>
					<div class="value" id="country"></div>
                </div>
                <div class="mgv-field">
					<div class="name">How long have you lived at this address?</div>
					<div class="value">
						<span id="lived_yrs"></span> Years <span id="lived_mnths"></span> Months
					</div>
                </div>
            </div>
		</div>
</div>