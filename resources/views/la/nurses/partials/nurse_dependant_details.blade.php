<div id="tab_8" class="tab-pane " >
                                
                                <div class="content-head">
                                                <h4>Dependant Details</h4>
                                                <!-- <div class="id">Nurse ID : <b>#12675</b></div> -->
                                        </div>
                                        {!! Form::open(['action' => 'LA\NursesController@store', 'id' => '']) !!}
               
                        <div class="pane">
                                
                                <div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
														<select name="relatiion_status" id="relatiion_status" class="formSelect">
															<option value="">Select</option>
                                                            <option value="Single">Single</option>
                                                            <option value="Married">Married</option>
															<option value="Civil-partnership">Civil partnership</option>
														</select>
														<label for="relatiion_status">Relationship Status</label>
                                                </div>


                                        </div>
                                </div>
                                
							<div id="married-details" class="hide">
                                <div class="row">
										<h4> Spouse Name </h4>
                                        <div class="input-field  col s3">
											<select name="spouse_prefix" id="spouse_prefix">
												<option value="Mr"> Mr. </option>
												<option value="Mrs."> Mrs. </option>
												<option value="Ms."> Ms. </option>
												<option value="Miss."> Miss. </option>
												<option value="Dr."> Dr. </option>
												<option value="Professor."> Professor. </option>
												<option value="Sister."> Sister. </option>
												<option value="Father."> Father. </option>
												<option value="Major."> Major. </option>
												<option value="Reverend."> Reverend. </option>
												<option value="Pastor."> Pastor. </option>
												<option value="Captain."> Captain. </option>
												<option value="Rabbi."> Rabbi. </option>
												<option value="Mx."> Mx. </option>
											</select>
											<label for="prefix">Prefix</label>
                                        </div>
                                        <div class="input-field col s3">
                                                        <input name="spouse_firstname" id="spouse_firstname" type="text">
                                                        <label for="spouse_firstname">First Name</label>
                                           
                                        </div>


                                                <div class="input-field col s3">
                                                                <input id="spouse_middlename" name="spouse_middlename" type="text" class="validate">
                                                                <label for="spouse_middlename">Middle Name</label>
                                                                
                                                </div>


                                                        <div class="input-field col s3 ">
                                                                        <input id="spouse_familyname" name="spouse_familyname" type="text" class="validate">
                                                                        <label for="spouse_familyname">Family Name</label>
                                                                        
                                                        </div>



                        </div>
                                <div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
                                                    <input name="spouse_birth_date"  id="spouse_birth_date" type="text" class="datepicker">
													<label for="spouse_birth_date">Date of Birth of Spouse</label>
                                                </div>


                                        </div>
										
										<div class="col s6">
                                        <div class="input-field">
														<select name="current_live_status" id="current_live_status">
                                                            <option value="Yes">Yes</option>
                                                            <option value="No">No</option>
														</select>
														<label for="current_live_status">Do they currently live with you ? </label>
                                                </div>
   
										</div>	
                                </div>
								
                                <div class="row hide dependent-addr-wrapper">
									<h4> If no, provide their currrent address </h4>
									<div class="input-field col s6">
									   <input type="text" id="spouse_current_addr" name="spouse_current_addr">
									   <label for="spouse_current_addr">Street Address</label>
									</div>
									<div class="input-field col s6">
									   <input type="text" id="spouse_current_addr_line" name="spouse_current_addr_line">
									   <label for="spouse_current_addr_line">Street Address Line2</label>
									</div>						
								</div>
					
					    <div class="row hide dependent-addr-wrapper">
							<div class="input-field col s6">
							   <input type="text" id="spouse_current_city" name="spouse_current_city">
							   <label for="spouse_current_city">City</label>
							</div>
							<div class="input-field col s6">
							   <input type="text" id="spouse_current_state" name="spouse_current_state" >
							   <label for="spouse_current_state">State / Province</label>
							</div>						
						</div>
						
					    <div class="row hide dependent-addr-wrapper">
							<div class="input-field col s6">
							   <input type="text" id="spouse_current_zipcode" name="spouse_current_zipcode" >
							   <label for="spouse_current_zipcode">Postal / Zip code</label>
							</div>
							<div class="input-field col s6">
								<select class="searchSelect" name="spouse_current_country" id="spouse_current_country">
								  <option value=""> Please Select </option>
									<?php
									foreach($countries as $countriesData) {
									echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
									}
									?>
								</select>							   
							   <label for="spouse_current_country" style="margin-top: -40px;font-size: 13px;">Country</label>
							</div>						
						</div>
								<div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
                                                    <input name="spouse_passport_no"  id="spouse_passport_no" type="text">
													<label for="spouse_passport_no">Passport Number of Spouse</label>
                                                </div>


                                        </div>
                                </div>
								<div class="row">
										<div class="col s6">
                                        <label>Passport of Spouse (front page)</label>
										<div class="file-field input-field">
																<div class="btn9">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
														<div class="file-path-wrapper">
																<input class="file-path validate" name="spouse_passport" type="text">

																<div>
																</div>
												
														</div>
										</div>

										<div class="uploaded-file" id="spouse_passport" data-type="file"></div>

										</div>
										
										<div class="col s6">
                                        <label>Passport of Spouse (back page)</label>
										<div class="file-field input-field">
																<div class="btn9">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
														<div class="file-path-wrapper">
																<input class="file-path validate" name="spouse_passport_back" type="text">

																<div>
																</div>
												
														</div>
										</div>

										<div class="uploaded-file" id="spouse_passport_back" data-type="file"></div>

										</div>										
                                </div>								
								<div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
														<select class="formSelect" name="spouse_travel_uk" id="spouse_travel_uk">
															<option value="">Select</option>
                                                            <option value="YES">YES</option>
                                                            <option value="NO">NO</option>
														</select>
														<label for="spouse_travel_uk">Will they be travelling with you to UK ? </label>
                                                </div>


                                        </div>
                                        <div class=" col s6 ">
                                        <div class="input-field">
														<select class="formSelect" name="childrens" id="childrens">
															<option value="">Select</option>
                                                            <option value="YES">YES</option>
                                                            <option value="NO">NO</option>
														</select>
														<label for="childrens">Do you have children ? </label>
                                                </div>

                                        </div>
                                </div>
								
								<!-- hide -->
								<div id="childrens-wrapper" class="childrens-wrapper hide">					
									<h4 class="sub_title">Details of dependants</h4>
									<div class="stdhst">
											<div class="index">1</div>
											<div class="col-md-6">
												<div class="mg-input-field ">
													<label for="name">Name (First Name + Family Name)</label>
													<input class="form-control" id="name" name="name" type="text">										
												</div>
											</div>
											
											<div class="col-md-6">
												<div class="mg-input-field ">
													<label for="birthdate">Date of birth</label>
													<input class="form-control datepicker" id="birthdate" name="birthdate" type="text">								
												</div>
											</div>
											<div class="col-md-6">
													<div class="mg-input-field ">
															<label for="from_date">Relationship to you?</label>
															<select class="formSelect" name="relation" id="relation">
																<option value="Son">Son</option>
																<option value="Daughter">Daughter</option>
															</select>												
													</div>
											</div>
											<div class="col-md-6">
													<div class="mg-input-field ">
															<label for="current_live">Does this person currently live with you?</label>
															<select class="formSelect" name="current_live" id="current_live">
																<option value="Yes">Yes</option>
																<option value="No">No</option>
															</select>	
													</div>
											</div>

											<div class="col-md-6">
													<div class="mg-input-field ">
															<label for="travel_to_uk">Is this person travelling with you to the UK?</label>
															<select class="formSelect" name="travel_to_uk" id="travel_to_uk">
																<option value="Yes">Yes</option>
																<option value="No">No</option>
															</select>	
													</div>
											</div>	
											
											<div class="clearfix"></div>
									</div>
								</div>	
								<div class="row">
									<input type="hidden" value="children" name="formtype" class="formtype">
									<div class="btn addshbtn childrens-wrapper-btn displaynone"> <span class="glyphicon glyphicon-plus"></span></div>
									<input type="hidden" value="1" class="wrapper-count">
								</div>
							</div>
							
                                <div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
                                                    <input name="mother_firstname"  id="mother_firstname" type="text">
													<label for="mother_firstname">Mother's Full Name</label>
                                                </div>


                                        </div>
										
										<div class="col s6">
                                            <div class="input-field">
                                                <input name="mother_birth"  id="mother_birth" type="text" class="datepicker">
												<label for="mother_birth">Mother's DOB</label>
                                            </div>    
										</div>	
                                </div>
                                <div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
                                                    <input name="father_firstname"  id="father_firstname" type="text">
													<label for="father_firstname">Father's Full Name</label>
                                                </div>


                                        </div>
										
										<div class="col s6">
                                            <div class="input-field">
                                                <input name="father_birth"  id="father_birth" type="text" class="datepicker">
												<label for="father_birth">Father's DOB</label>
                                            </div>    
										</div>	
                                </div>

                                
                        </div>
                        <div class="controls">
                                        <a class="mgbtn2">Back</a>
                                        <input id="form_name" type="hidden" class="validate" name="type_of" value="dependant_det">
                                        <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                        {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
                </div>

                