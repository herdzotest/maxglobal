<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;
use Dwij\Laraadmin\Helpers\LAHelper;
use Dwij\Laraadmin\Models\LAConfigs;


use App\User;
use App\Models\Employee;
use App\Role;
use Mail;
use Log;

class EmployeesController extends Controller
{
	public $show_action = true;
	public $view_col = 'name';
	public $listing_cols = ['id', 'name','mobile', 'email', 'city', 'date_birth'];
	
	public function __construct() {

		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Employees', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Employees', $this->listing_cols);
		}
	}

	/**
	 * Display the specified employee.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Auth::user()->context_id != $id)
			return abort(403);

		if(Module::hasAccess("Employees", "view")) {
			
			$employee = Employee::find($id);

			if(isset($employee->id)) {
				$module = Module::get('Employees');
				$module->row = $employee;
				$mailDetails = DB::table('mail_settings')
							->select('mail_settings.id')->addselect('mail_settings.mail_content')->addselect('mail_settings.subject')
							->addselect('us.name')->addselect('us.type')->addselect('mail_settings.send_date')
							->leftJoin('users', 'users.id', '=', 'mail_settings.to_user')
							->leftJoin('users AS us', 'us.id', '=', 'mail_settings.from_user')
							->where('mail_settings.is_deleted', 0)
							->where('users.context_id', $id)->orderBy('mail_settings.send_date', 'desc')->get();

				$notifData = DB::table('notifications')
							->select('notifications.id')->addselect('notifications.comments')->addselect('notifications.notif_dat')->addselect('users.name') 
							->leftJoin('users', 'users.id', '=', 'notifications.from_user_id')
							->where('notifications.is_deleted', 0)
							->whereNotNull('notifications.notif_dat')
							->where('notifications.user_id', $id)->orderBy('notifications.notif_dat', 'desc')->get();
						
				return view('la.employees.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding",
					'mailDetails' => $mailDetails,
					'notifDetails' => $notifData
				])->with('employee', $employee);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("employee"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	public function deleteMail($id,$employee)
	{
		DB::table('mail_settings')->where('id', $id)->update(['is_deleted' => 1]);
		\Session::flash('success_mail_delete', 'Mail is deleted successfully');
		
		return redirect(config('laraadmin.adminRoute') . '/consultants/'.$employee.'#tab-timeline');
	}

	/**
	 * Show the form for editing the specified employee.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Auth::user()->context_id != $id)
			return abort(403);
		
		if(Module::hasAccess("Employees", "edit")) {			
			$employee = Employee::find($id);
			if(isset($employee->id)) {	
				$module = Module::get('Employees');
				unset($module->fields['designation']); unset($module->fields['dept']); unset($module->fields['gender']);
				$module->row = $employee;
				
				return view('la.employees.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('employee', $employee);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("employee"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified employee in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Auth::user()->context_id != $id)
			return abort(403);
		
		if(Module::hasAccess("Employees", "edit")) {
			
			$rules = Module::validateRules("Employees", $request, true);			
			$validator = Validator::make($request->all(), $rules);
			
			$this->emailId = $request->all()['email']; $this->userId = $id;
			$validator->after(function($validator) {
				if ($this->emailValidation() > 0) {
					$validator->errors()->add('email', 'Email already exists');
				}
			});
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Employees", $request, $id);
			DB::table('employees')->where('id',$insert_id)->update(array('profilePic' => $request->all()['profilePic'] ));
			DB::table('users')->where('context_id', $insert_id)->update(['email' => $request->all()['email'], 'name' => $request->all()['name'] ]);
			return redirect(config('laraadmin.adminRoute') . '/user_profile/'.$id.'/view');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * email validation 
	 */
	private function emailValidation()
	{
		$email = DB::table('employees')
				->whereNotIn('id', [$this->userId])
				->where('email', $this->emailId)
				->get();
				
		return count($email);
	}
	
	/**
     * Change Employee Password
     *
     * @return
     */
	public function change_password($id, Request $request) {

		if(Auth::user()->context_id != $id)
			return abort(403);
		
		$validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
			'password_confirmation' => 'required|min:6|same:password'
        ]);
		
		if ($validator->fails()) {
			return \Redirect::to(config('laraadmin.adminRoute') . '/employees/'.$id)->withErrors($validator);
		}
		
		$employee = Employee::find($id);
		$user = User::where("context_id", $employee->id)->first();
		$user->password = bcrypt($request->password);
		$user->save();
	
		\Session::flash('success_message', 'Password is successfully changed');
		
		// Send mail to User his new Password
		if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
			// Send mail to User his new Password
			// Mail::send('emails.send_login_cred_change', ['user' => $user, 'password' => $request->password], function ($m) use ($user) {
			// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
			// 	$m->to($user->email, $user->name)->subject('Zodocs - Login Credentials changed');
			// });
		} else {
			Log::info("User change_password: username: ".$user->email." Password: ".$request->password);
		}
		
		return redirect(config('laraadmin.adminRoute') . '/user_profile/'.$id.'/view#tab-account-settings');
	}

	public function updateRead($id) {	
		DB::table('notifications')->where('user_id', $id)->update(['is_read' => 1]);
	}

}
