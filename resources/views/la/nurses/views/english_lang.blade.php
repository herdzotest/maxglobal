<div id="tab_5" class="tab-pane " >

        
<div class="content-head">
                                        
                                        <h4> ENGLISH LANGUAGE EVIDENCE  </h4>

                                        <!--  <div class="id">Sutdent ID : <b>#12675</b></div>-->
                                </div>
                <div class="pane">
                        
            <div class="academic">      
                 <div class="mgv-field">
                        <div class="name">
                        What evidence of your English language do you have to support your application?
                        </div>
                        <div class="value" id="eng_lang_evidence"></div>
                 </div>
                 
                 <div class="mgv-field">
                        <div class="name">
                        I hold the following English language qualification	
                        </div>
                        <div class="value" id="eng_lang_qualif"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                            Number of test sittings
                        </div>
                        <div class="value" id="test_sittings"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                           IELTS / OET Passed Date
                        </div>
                        <div class="value" id="ielts_passed_date"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                        IELTS TRF number / OET candidate number
                        </div>
                        <div class="value" id="ielts_number"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                        IELTS TRF / OET certificate
                        </div>
					    <div class="value">
							<div class="uploaded-file" id="ielts_certificate" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
							<div class="file-approve-buttons">
								<!-- <a href="#" class="btn btn-danger mx-approve-doc" data-type="oet_certificate" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
								<a href="#" class="btn btn-success mx-approve-doc" data-type="oet_certificate" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a> -->
								<a href="#" class="btn btn-danger mx-approve-doc" data-type="ielts_certificate" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
								<a href="#" class="btn btn-success mx-approve-doc" data-type="ielts_certificate" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
							</div>  	
					    </div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                        VALIDITY
                        </div>
                        <div class="value" id="validity_date"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                        IELTS TRF / OET certificate
                        </div>
					    <div class="value">
							<div class="uploaded-file" id="ielts_file_upload" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
							<div class="file-approve-buttons">
								<!-- <a href="#" class="btn btn-danger mx-approve-doc" data-type="oet_certificate" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
								<a href="#" class="btn btn-success mx-approve-doc" data-type="oet_certificate" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a> -->
								<a href="#" class="btn btn-danger mx-approve-doc" data-type="ielts_file_upload" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
								<a href="#" class="btn btn-success mx-approve-doc" data-type="ielts_file_upload" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
							</div>  	
					    </div>
                 </div>					       
			</div>  
			
			<div id="englishLang-wrapper">
				<div class="academic">      
					 <div class="mgv-field">
							<div class="name">
							Country
							</div>
							<div class="value" id="ielts_country"></div>
					 </div>
					 
					 <div class="mgv-field">
							<div class="name">
							Work start date	
							</div>
							<div class="value" id="work_start_date"></div>
					 </div>
					 <div class="mgv-field">
							<div class="name">
								Work End date 
							</div>
							<div class="value" id="work_end_date"></div>
					 </div>
					 <div class="mgv-field">
							<div class="name">
								Employer address
							</div>
							<div class="value" id="employer_addresss"></div>
					 </div>
					 <div class="mgv-field">
							<div class="name">
							Email ID of employer
							</div>
							<div class="value" id="employer_email"></div>
					 </div>
					 <div class="mgv-field">
							<div class="name">
							Nursing council registration certificate
							</div>
							<div class="value">
								<div class="uploaded-file" id="file_upload_nursing" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
								<div class="file-approve-buttons">
									<!-- <a href="#" class="btn btn-danger mx-approve-doc" data-type="registration_cert" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
									<a href="#" class="btn btn-success mx-approve-doc" data-type="registration_cert" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a> -->
									<a href="#" class="btn btn-danger mx-approve-doc" data-type="file_upload_nursing" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
									<a href="#" class="btn btn-success mx-approve-doc" data-type="file_upload_nursing" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
								</div>  	
							</div>
					 </div>
					 <div class="mgv-field">
							<div class="name">
							Reference letter
							</div>
							<div class="value" id="reference_letter"></div>
					 </div>					       
				</div>  			
			</div>			
    </div>  
</div>