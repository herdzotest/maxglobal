<div id="tab_6" class="tab-pane" >
		<div class="content-head">
		
						<h4>Employer details</h4>
					   <!-- <div class="id">Nurse ID : <b>#12675</b></div>-->
		</div>
	<div class="pane">
		<div class="academic">
			 <div class="mgv-field">
					<div class="name">
						Employer/ Main Agent 
					</div>
					<div class="value" id="main_agent"></div>
			 </div>
			 <div class="mgv-field">
					<div class="name">
						Work Site (Care Home/Hospitals)
					</div>
					<div class="value" id="work_site">
					</div>
			 </div>
			 <div class="mgv-field">
					<div class="name">
						Mode of Interview
					</div>
					<div class="value" id="mode_interview">
					</div>
			 </div>
			 <div class="mgv-field">
					<div class="name">
						Interview Schedule Date
					</div>
					<div class="value" id="interview_schedule_date">
					</div>
			 </div>
			 <div class="mgv-field">
					<div class="name">
						Interview Schedule Time
					</div>
					<div class="value" id="interview_schedule_time">
					</div>
			 </div>
			 <div class="mgv-field">
					<div class="name">
						Interview Status
					</div>
					<div class="value" id="interview_status">
					</div>
			 </div>
			 <div class="mgv-field">
					<div class="name">
						Offer Letter Status
					</div>
					<div class="value" id="offer_letter_status">
					</div>
			 </div>
			 <div class="mgv-field">
					<div class="name">
						Offer Letter Received Date
					</div>
					<div class="value" id="offer_letter_date">
					</div>
			 </div>
			 <div class="mgv-field">
					<div class="name">
						Offer Letter Signed Date
					</div>
					<div class="value" id="offer_letter_signed">
					</div>
			 </div>
			 <div class="mgv-field">
                        <div class="name"> Offer Letter File </div>
					    <div class="value">
							<span id="offer_letter_file_name"></span><br>
							<div class="uploaded-file" id="offer_letter_file" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
							<div class="file-approve-buttons">
                                        <a href="#" class="btn btn-danger mx-approve-doc" data-type="offer_letter_file" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
                                        <a href="#" class="btn btn-success mx-approve-doc" data-type="offer_letter_file" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
							</div>  	
					    </div>
                 </div> 
			 <!-- <div class="mgv-field">
					<div class="name">
						Preferred Designation
					</div>
					<div class="value" id="preferred_designation">
					</div>
			 </div>
			 <div class="mgv-field">
					<div class="name">
						Preferred location
					</div>
					<div class="value" id="preferred_location">
					</div>
			 </div>
			 <div class="mgv-field">
					<div class="name">
						Preferred salary
					</div>
					<div class="value" id="preferred_salary">
					</div>
			 </div>
			 <div class="mgv-field">
					<div class="name">
						Preferred hours
					</div>
					<div class="value" id="preferred_hours">
					</div>
			 </div>
			 <div class="mgv-field">
					<div class="name">
						Preferred shifts
					</div>
					<div class="value" id="preferred_shifts">
					</div>
			 </div>	 -->			 
		</div>                               									

	</div>
</div>