<div id="tab_1" class="tab-pane active" >
        
                   
<div class="content-head">
                                        
                                        <h4>Personal Details</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                        </div>
                        {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'personal-add-form']) !!}
                <div class="pane" >
                        <div class="row">
                                <div class=" col s3 ipfield">
                                        <div class="input-field">
											<select name="prefix" id="prefix">
												<option value="Mr">Mr.</option>
												<option value="Mrs">Mrs.</option>
												<option value="Mrs">Miss.</option>
											</select>
											<label for="prefix">Prefix</label>
                                        </div>
                                </div>
                                <div class=" col s3 ipfield">
                                        <div class="input-field">
                                                        <!-- <i class="material-icons prefix">account_circle</i> -->
                                                        <input name="first_name" required id="first_name" type="text">
                                                        <label for="first_name">First Name</label>
                                           
                                                        <span class="helper-text" data-error="wrong"
                                                        data-success="right">As per Passport</span>
                                        </div>


                                </div>
                                <div class=" col s3 ipfield">
                                                <div class="input-field">
                                                                <input id="middle_name" name="middle_name" type="text" class="validate">
                                                                <label for="middle_name">Middle Name</label>
                                                                <span class="helper-text" data-error="wrong"
                                                                data-success="right">As per Passport</span>
                                                </div>


                                        </div>
                                        <div class=" col s3 ipfield">
                                                        <div class="input-field">
                                                                        <input id="sur_name" name="sur_name" type="text" class="validate">
                                                                        <label for="sur_name">Sur Name</label>
                                                                        <span class="helper-text" data-error="wrong"
                                                                        data-success="right">As per Passport</span>
                                                        </div>


                                                </div>

                        </div>
                        <div class="row">
                                <div class="input-field col s6">
                                                <!-- <i class="material-icons prefix">calendar_today</i> -->

                                        <input id="dob" required  name="dob" type="text" class="datepicker">
                                        <label for="dob">Date of Birth</label>
                                        <span class="helper-text" data-error="wrong"
                                        data-success="right">As per Passport</span>
                                </div>
                                <div class="input-field col s6">
									<select name="gender" id="gender">
										<option value="male">Male</option>
										<option value="female">Female</option>
										<option value="not_specified">Not specified / Indeterminate</option>
										<option value="other">Other</option>
									</select>
									<label for="gender">Prefix</label>
                                </div>
                        </div>
				
						<div class="row">
							<div class="col s6 ipfield">
								<div class="input-field">
									<!-- <i class="material-icons prefix">mail</i> -->
									<?php
									if($studentId == 0 || $studentId == '') {
									?>
										<input name="email" id="email" required type="text">
									<?php } else { ?>
										<input type="text" value="" readonly>
									<?php } ?>
									<label for="email">Email</label>
								</div>
							</div>
							<div class="col s2">
								<div class="input-field">
									<select name="coountry_code" id="coountry_code">
										<option value="91">91</option>
										<option value="91">91</option>
									</select>
									<label for="coountry_code">Country code </label>
								</div>
							</div>
							<div class="col s4">
								<div class="input-field">
									<input id="contact_no" name="contact_no" type="text" class="validate">
									<label for="contact_no">Contact Number </label>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col s6">
								<label>Upload Profile image</label>
								<div class="file-field input-field">
									<div class="btn">
											<span>Upload</span>
                                                                                        <input type="file"><i class="fa fa-cloud-upload"></i> 
                                                                                        
									</div>
									<div class="file-path-wrapper">
										<input class="file-path validate" name="profile_image" type="text">
											<div></div>
									</div>
								</div>
								<div class="uploaded-file" id="profile_image" data-type="file"></div>
							</div>
							<div class="col s6">
								<div class="input-field">
									<input id="passport_no" name="passport_no" type="text" class="validate">
									<label for="passport_no" >Passport Number</label>
									<span class="helper-text" data-error="wrong" data-success="right">As per Passport</span>
								</div>
							</div>
						</div>
						
						<div class="row">
								<div class="col s6">
										<label>Upload Passport Front Page</label>
										<div class="file-field input-field">
																<div class="btn">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
														<div class="file-path-wrapper">
																<input class="file-path validate" name="image_front" type="text">

																<div>
																</div>
												
														</div>
										</div>

										<div class="uploaded-file" id="image_front" data-type="file"></div>
								</div>
								<div class="col s6">
												<label>Upload Passport Back Page</label>
												<div class="file-field input-field">


																<div class="btn">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
																<div class="file-path-wrapper">
																		<input class="file-path validate" name="image_back"  type="text">
																	  

																</div>
												</div>
											  
												<div class="uploaded-file" id="image_back" data-type="file"></div>
										</div>
						</div>	
						
						<h4 class="sub_title">Current Address</h4>
						
					    <div class="row">
							<div class="input-field col s6">
								<!-- <i class="material-icons prefix">home</i> -->
							   <input type="text" id="address" name="address" required>
							   <label for="address">Street Address</label>
							</div>
							<div class="input-field col s6">
								<!-- <i class="material-icons prefix">home</i> -->
							   <input type="text" id="address_2" name="address_2" required>
							   <label for="address_2">Street Address Line2</label>
							</div>						
						</div>
					
					    <div class="row">
							<div class="input-field col s6">
							   <input type="text" id="city" name="city" required>
							   <label for="city">City</label>
							</div>
							<div class="input-field col s6">
							   <input type="text" id="state" name="state" required>
							   <label for="state">State / Province</label>
							</div>						
						</div>
						
					    <div class="row">
							<div class="input-field col s6">
							   <input type="text" id="zip_code" name="zip_code" required>
							   <label for="zip_code">Postal / Zip code</label>
							</div>
							<div class="input-field col s6">
								<select name="country" id="country" required>
									<option value="India">India</option>
									<option value="UK">UK</option>
									<option value="Australia">Australia</option>
								</select>							   
							   <label for="country">Country</label>
							</div>						
						</div>
						
						<h4 class="sub_title">How long have you lived at this address?</h4>
						
					    <div class="row">
							<div class="input-field col s6">
								<select name="lived_yrs" id="lived_yrs">
									<?php
									for($i=0;$i<=10;$i++) {
									?>
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php
									}
									?>
								</select>							   
							   <label for="lived_yrs">Years</label>
							</div>
							<div class="input-field col s6">
								<select name="lived_mnths" id="lived_mnths">
									<?php
									for($i=0;$i<=10;$i++) {
									?>
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php
									}
									?>
								</select>							   
							   <label for="lived_mnths">Months</label>
							</div>						
						</div>
                </div>
                <div class="controls">
                                <a class="   mgbtn2">Back</a>
                                <!-- <a class="waves-effect waves-light btn" type="submit"><i class="material-icons left">save</i>SAVE</a> -->
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="personal_information">
                                 <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                              
                                {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                        </div>
                        {{ Form::close() }}
</div>