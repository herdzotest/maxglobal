
$(function () {
	$("#mx_upload_image").on("click", function(e) {
		$('#AddModalUpload').modal();
	});
	$("#closeDZ1").on("click", function(e) {
		$('#AddModalUpload').modal("hide");
	});
	
	fm_dropzone_main = new Dropzone("#fm_dropzone_main", {
        maxFilesize: 1,
        acceptedFiles: "image/*",
        init: function() {
			this.on('addedfile', function(file) {
				if (this.files.length > 1) {
					alert("Only 1 file allowed")
					this.removeFile(this.files[0]);
				}
			});
            this.on("complete", function(file) {
                this.removeFile(file);
            });
            this.on("success", function(file) {
				
				var APP_URL = $("#mx_upload_image").attr('data-url');
				var response = JSON.parse(file.xhr.response);
				var upload = response.upload;
				var imagePath = APP_URL + '/files/' + upload.hash + '/'+ upload.name;
				$("#view_upload_image img").attr('src',imagePath+'?s=130');
				$("#profile-pic").val(imagePath+'?s=130');
				$("#view_upload_image a").attr('href',imagePath);
				$("#view_upload_image").removeClass('hide');
				$('#AddModalUpload').modal("hide");
            });
        }
    });
	$("#profile-remove-img").on("click", function(e) {
		$("#profile-pic").val('');
	});
});
