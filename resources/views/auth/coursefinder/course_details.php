<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Google fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css2?family=Material+Icons+Outlined" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />

  <!-- Stylesheets -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="../la-assets/coursefinder/css/vendors.css">
  <link rel="stylesheet" href="../la-assets/coursefinder/css/main.css">
  <link rel="stylesheet" href="../la-assets/coursefinder/css/custom.css">

  <title>Course Details</title>
  <link rel="icon" type="image/x-icon" href="../la-assets/coursefinder/img/coursesCards/icons/zodocs_fav.png">
</head>

<body class="preloader-visible" data-barba="wrapper">

  <!-- preloader start -->
  <div class="preloader js-preloader">
    <div class="preloader__bg"></div>
  </div>
  <!-- preloader end -->

  <!-- barba container start -->
  <div class="barba-container" data-barba="container">
    <main class="main-content  ">
      <header data-anim="fade" data-add-bg="bg-dark-1" class="header -type-1 js-header">
        <div class="header__container">
          <div class="row justify-between items-center">
            <div class="col-auto">
              <div class="header-left">
                <div class="header__logo ">
                  <a data-barba href="<?php echo url('/coursefinder'); ?>">
                    <img src="../la-assets/coursefinder/img/general/logo.png" alt="logo">
                  </a>
                </div>
              </div>
            </div>


            <div class="header-menu js-mobile-menu-toggle ">
              <div class="header-menu__content">
                <div class="mobile-bg js-mobile-bg"></div>

                <div class="d-none xl:d-flex items-center px-20 py-20 border-bottom-light">
                  
                  <?php 
                  if(isset(Auth::user()->type)) { ?>
                    <a href="<?php echo url('/logout'); ?>" class="text-dark-1">Log out</a>
                    <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="text-dark-1 ml-30">My Account</a>
                  <?php } else { ?>
                    <a href="<?php echo url('/userlogin'); ?>" class="text-dark-1">Log in</a>
                    <a href="<?php echo url('/signup'); ?>" class="text-dark-1 ml-30">Sign up</a>
                  <?php } ?>

                </div>

                <div class="menu js-navList">
                  <ul class="menu__nav text-white -is-active">
                    <li class="menu-item-has-children">
                      <a data-barba href="#">Home</a>
                      <ul class="subnav">
                        <li class="menu__backButton js-nav-list-back">
                          <a href="#">Home</a>
                        </li>
                        <li><a href="<?php echo url('/coursefinder'); ?>">Home 1</a></li>
                        <li><a href="<?php echo url('/coursefinder2'); ?>">Home 2</a></li>
                        <li><a href="<?php echo url('/coursefinder3'); ?>">Home 3</a></li>
                        <li><a href="<?php echo url('/coursefinder4'); ?>">Home 4</a></li>
                        <li><a href="<?php echo url('/coursefinder5'); ?>">Home 5</a></li>
                        <li><a href="<?php echo url('/coursefinder6'); ?>">Home 6</a></li>
                        <li><a href="<?php echo url('/coursefinder7'); ?>">Home 7</a></li>
                        <li><a href="<?php echo url('/coursefinder8'); ?>">Home 8</a></li>
                        <li><a href="<?php echo url('/coursefinder9'); ?>">Home 9</a></li>
                        <li><a href="<?php echo url('/coursefinder10'); ?>">Home 10</a></li>
                      </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/aboutus'); ?>">
                        About Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li class="menu-item-has-children">
                          <a data-barba href="#">Courses</a>
                          <ul class="subnav">
                            <li class="menu__backButton js-nav-list-back">
                              <a href="#">Courses</a>
                            </li>
                            <li><a href="<?php echo url('/coursesearch'); ?>">Course 1</a></li>
                            <li><a href="<?php echo url('/coursesearch2'); ?>">Course 2</a></li>
                            <li><a href="<?php echo url('/coursesearch3'); ?>">Course 3</a></li>
                            <li><a href="<?php echo url('/coursesearch4'); ?>">Course 4</a></li>
                          </ul>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/blog'); ?>">
                        Blog <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/topcourses'); ?>">
                        Top Courses <i class="text-13 ml-10"></i>
                      </a>
                    </li>

                    <li>
                      <a data-barba href="<?php echo url('/contactus'); ?>">
                        Contact Us <i class="text-13 ml-10"></i>
                      </a>
                    </li>
                    
                  </ul>
                </div>
              </div>

              <div class="header-menu-close" data-el-toggle=".js-mobile-menu-toggle">
                <div class="size-40 d-flex items-center justify-center rounded-full bg-white">
                  <i class="fa fa-close"></i>
                </div>
              </div>

              <div class="header-menu-bg"></div>
            </div>

            <div class="col-auto">
              <div class="header-right d-flex items-center">
                <div class="header-right__icons text-white d-flex items-center">

                  <div class="d-none xl:d-block ml-20">
                    <button class="text-white items-center" data-el-toggle=".js-mobile-menu-toggle">
                      <i class="fa fa-bars"></i>
                    </button>
                  </div>
                </div>

                <div class="header-right__buttons d-flex items-center ml-30 md:d-none">
                  
                  <?php 
                  if(isset(Auth::user()->type)) { ?>
                    <a href="<?php echo url('/logout'); ?>" class="button -underline text-white">Log out</a>
                    <a href="<?php echo config('laraadmin.adminRoute').'/dashboard'; ?>" class="button -sm -white text-dark-1 ml-30">My Account</a>
                  <?php } else { ?>
                    <a href="<?php echo url('/userlogin'); ?>" class="button -underline text-white">Log in</a>
                    <a href="<?php echo url('/signup'); ?>" class="button -sm -white text-dark-1 ml-30">Sign up</a>
                  <?php } ?>

                </div>
              </div>
            </div>

          </div>
        </div>
      </header>


      <div class="content-wrapper  js-content-wrapper">


        <section data-anim="fade" class="breadcrumbs ">
          <div class="container">
            <div class="row">
              <div class="col-auto">
                <div class="breadcrumbs__content">

                  <div class="breadcrumbs__item ">
                    <a href="<?php echo url('/coursefinder'); ?>">Home</a>
                  </div>

                  <div class="breadcrumbs__item ">
                    <a href="<?php echo url('/coursesearch'); ?>">All courses</a>
                  </div>

                  <div class="breadcrumbs__item ">
                    <?php echo $courses[0]->course_name?>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </section>


        <section class="page-header -type-5 bg-white">
          <div class="page-header__bg">
            <div class="bg-image js-lazy" data-bg="../la-assets/coursefinder/img/event-single/bg.png"></div>
          </div>

          <div class="container">
            <div class="page-header__content pt-90 pb-">
              <div class="row y-gap-30 relative">
                <div class="col-xl-7 col-lg-8">
                  <div class="d-flex x-gap-15 y-gap-10 pb-20">
                    <div>
                      <div class="badge px-15 py-8 text-11 bg-orange-1 text-white fw-400"><?php echo $courses[0]->course_name?></div>
                    </div>
                    <div>
                      <div class="badge px-15 py-8 text-11 bg-purple-1 text-white fw-400"><?php echo $courses[0]->country?></div>
                    </div>
                  </div>

                  <div data-anim="slide-up delay-1">
                    <h1 class="text-30 lh-14 pr-60 lg:pr-0"><?php echo $courses[0]->university.' - '.$courses[0]->course_name?></h1>
                  </div>
                  <?php if($courses[0]->course_description != '') { ?>
                    <p class="col-xl-9 mt-20"><?php echo substr($courses[0]->course_description,0,200).'....'?></p>
                  <?php } ?>

                  <div class="d-flex x-gap-30 y-gap-10 items-center flex-wrap pt-20">
                    
                    <div class="d-flex items-center text-light-1"><i class="fa fa-users"></i>
                      <div class="text-14 ml-8">853 enrolled on this course</div>
                    </div>
                  </div>

                  <div class="d-flex items-center pt-20">
                    
                  </div>
                </div>

                <div class="courses-single-info">
                  <div class="bg-white shadow-2 rounded-8 border-light py-10 px-10">
                    <div class="relative">
                      <?php if($courses[0]->university_logo!='') { ?>
                        <img class="w-1/1 course_logo" src="<?php echo $courses[0]->university_logo; ?>" alt="image">
                      <?php } else { ?>
                        <img class="w-1/1 rounded-8 course_logo" src="../la-assets/coursefinder/img/coursesCards/no-image.png" alt="image">
                      <?php } ?>
                      
                    </div>

                    <div class="courses-single-info__content scroll-bar-1 pt-30 pb-20 px-20">
                      <div class="d-flex justify-between items-center mb-30">
                        <div class="text-14 lh-1">Annual Fees</div><div class="text-24 lh-1 text-dark-1 fw-500">&#8377; <?php echo $courses[0]->tuition_fees?></div>
                        <div class="lh-1 line-through"></div>
                      </div>

                      <!-- <button class="button -md -purple-1 text-white w-1/1"><a href="<?php echo url('/applynow').'/'.$courses[0]->id; ?>">Apply Now</a></button> -->
                      <a href="<?php echo url('/applynow').'/'.Crypt::encrypt($courses[0]->id); ?>"><button class="button -md -purple-1 text-white w-1/1">Apply Now</button></a>

                      <a href="<?php echo url('/signup'); ?>"><button class="button -md -outline-dark-1 text-dark-1 w-1/1 mt-10">Register</button></a>

                      <div class="text-14 lh-1 text-center mt-30"></div>

                      <div class="mt-25">

                        <div class="d-flex justify-between py-8 ">
                          <div class="d-flex items-center text-dark-1">
                            <i class="fa fa-file"></i>
                            <div class="ml-10">Course Type</div>
                          </div>
                          <div><?php echo $courses[0]->course_type; ?></div>
                        </div>

                        <div class="d-flex justify-between py-8 border-top-light">
                          <div class="d-flex items-center text-dark-1">
                            <i class="fa fa-clock"></i>
                            <div class="ml-10">Duration</div>
                          </div>
                          <div><?php echo $courses[0]->duration; ?></div>
                        </div>

                        <div class="d-flex justify-between py-8 border-top-light">
                          <div class="d-flex items-center text-dark-1">
                            <i class="fa fa-calendar"></i>
                            <div class="ml-10">Intake</div>
                          </div>
                          <div><?php echo $courses[0]->intake; ?></div>
                        </div>

                        <div class="d-flex justify-between py-8 border-top-light">
                          <div class="d-flex items-center text-dark-1">
                            <i class="fa fa-inr"></i>
                            <div class="ml-10">Application Fees</div>
                          </div>
                          <div>&#8377; <?php echo $courses[0]->application_fee; ?></div>
                        </div>

                        <div class="d-flex justify-between py-8 border-top-light">
                          <div class="d-flex items-center text-dark-1">
                            <i class="fa fa-calendar"></i>
                            <div class="ml-10">Application Deadline</div>
                          </div>
                          <div><?php echo date("d M Y", strtotime($courses[0]->application_deadline)); ?></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>


        <section class="layout-pt-md layout-pb-md">
          <div class="container">
            <div class="row">
              <div class="col-lg-8">
                <div class="tabs -active-purple-2 js-tabs">
                  <div class="tabs__controls d-flex js-tabs-controls">
                    <button class="tabs__button js-tabs-button is-active" data-tab-target=".-tab-item-1" type="button">
                      Overview
                    </button>
                    <?php if($courses[0]->test_requirements != '' || $courses[0]->entry_requirements != '' || $courses[0]->remarks != '') { ?>
                    <button class="tabs__button js-tabs-button ml-30" data-tab-target=".-tab-item-2" type="button">
                      Course Content
                    </button>
                    <?php } ?>
                    <!-- <button class="tabs__button js-tabs-button ml-30" data-tab-target=".-tab-item-3" type="button">
                      Instructors
                    </button>
                    <button class="tabs__button js-tabs-button ml-30" data-tab-target=".-tab-item-4" type="button">
                      Reviews
                    </button> -->
                  </div>

                  <div class="tabs__content pt-60 lg:pt-40 js-tabs-content">
                    <div class="tabs__pane -tab-item-1 is-active">
                      <?php if($courses[0]->course_description != '') { ?>
                      <h4 class="text-18 fw-500 course_desc">Description</h4>
                      <div class="show-more mt-30 js-show-more">
                        <div class="show-more__content course_desc1">
                          <p class="">
                            <?php echo $courses[0]->course_description; ?>
                          </p>
                        </div>
                        <!-- <button class="show-more__button text-purple-1 fw-500 underline mt-30 course_desc">Show more</button> -->
                        <button class="show-more__button text-purple-1 fw-500 underline course_desc">Show more</button>
                      </div>
                      <?php } ?>

                      <div class="mt-60">
                        <h4 class="text-20 mb-30 course_desc">Course Details</h4>
                        <div class="row x-gap-100 justfiy-between">
                          <div class="col-md-12">
                            <div class="y-gap-20">

                              <div class="d-flex items-center">
                                <div class="d-flex justify-center items-center border-light rounded-full size-20 mr-10 highlight">
                                  <i class="size-12" data-feather="check"></i>
                                </div>
                                <p>Course : <?php echo $courses[0]->course_name; ?></p>
                              </div>

                              <div class="d-flex items-center">
                                <div class="d-flex justify-center items-center border-light rounded-full size-20 mr-10 highlight">
                                  <i class="size-12" data-feather="check"></i>
                                </div>
                                <p>University : <?php echo $courses[0]->university; ?></p>
                              </div>

                              <div class="d-flex items-center">
                                <div class="d-flex justify-center items-center border-light rounded-full size-20 mr-10 highlight">
                                  <i class="size-12" data-feather="check"></i>
                                </div>
                                <p>Country : <?php echo $courses[0]->country; ?></p>
                              </div>

                              <div class="d-flex items-center">
                                <div class="d-flex justify-center items-center border-light rounded-full size-20 mr-10 highlight">
                                  <i class="size-12" data-feather="check"></i>
                                </div>
                                <p>Course Type : <?php echo $courses[0]->course_type; ?></p>
                              </div>

                              <div class="d-flex items-center">
                                <div class="d-flex justify-center items-center border-light rounded-full size-20 mr-10 highlight">
                                  <i class="size-12" data-feather="check"></i>
                                </div>
                                <p>Duration : <?php echo $courses[0]->duration; ?></p>
                              </div>

                              <div class="d-flex items-center">
                                <div class="d-flex justify-center items-center border-light rounded-full size-20 mr-10 highlight">
                                  <i class="size-12" data-feather="check"></i>
                                </div>
                                <p>Intake : <?php echo $courses[0]->intake; ?></p>
                              </div>

                              <div class="d-flex items-center">
                                <div class="d-flex justify-center items-center border-light rounded-full size-20 mr-10 highlight">
                                  <i class="size-12" data-feather="check"></i>
                                </div>
                                <p>Annual Tuition Fees : &#8377; <?php echo $courses[0]->tuition_fees; ?></p>
                              </div>

                              <div class="d-flex items-center">
                                <div class="d-flex justify-center items-center border-light rounded-full size-20 mr-10 highlight">
                                  <i class="size-12" data-feather="check"></i>
                                </div>
                                <p>Application Fees : &#8377; <?php echo $courses[0]->application_fee; ?></p>
                              </div>

                              <div class="d-flex items-center">
                                <div class="d-flex justify-center items-center border-light rounded-full size-20 mr-10 highlight">
                                  <i class="size-12" data-feather="check"></i>
                                </div>
                                <p>Application Deadline : <?php echo date("d M Y", strtotime($courses[0]->application_deadline)); ?></p>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    
                    <div class="tabs__pane -tab-item-2">
                      <h2 class="text-20 fw-500 course_desc">Course Content</h2>
                      <div class="mt-10">
                        <div class="accordion -block-2 text-left js-accordion">

                          <?php if($courses[0]->test_requirements != '') { ?>
                          <div class="accordion__item">
                            <div class="accordion__button py-20 px-30 bg-light-4">
                              <div class="d-flex items-center">
                                <div class="accordion__icon">
                                  <div class="icon" data-feather="chevron-down"></div>
                                  <div class="icon" data-feather="chevron-up"></div>
                                </div>
                                <span class="text-17 fw-500 text-dark-1">Test Requirements</span>
                              </div>
                            </div>

                            <div class="accordion__content">
                              <div class="accordion__content__inner px-30 py-30">
                                <div class="y-gap-20">

                                  <div class="d-flex justify-between">
                                    <div class="d-flex items-center">
                                      <div class="d-flex justify-center items-center size-30 rounded-full bg-purple-3 mr-10">
                                        <!-- <div class="icon-play text-9"></div> --><i class="fa fa-arrow-right"></i>
                                      </div>
                                      <div><?php echo $courses[0]->test_requirements; ?></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <?php } ?>

                          <?php if($courses[0]->entry_requirements != '') { ?>
                          <div class="accordion__item">
                            <div class="accordion__button py-20 px-30 bg-light-4">
                              <div class="d-flex items-center">
                                <div class="accordion__icon">
                                  <div class="icon" data-feather="chevron-down"></div>
                                  <div class="icon" data-feather="chevron-up"></div>
                                </div>
                                <span class="text-17 fw-500 text-dark-1">Entry Requirements</span>
                              </div>
                            </div>

                            <div class="accordion__content">
                              <div class="accordion__content__inner px-30 py-30">
                                <div class="y-gap-20">

                                  <div class="d-flex justify-between">
                                    <div class="d-flex items-center">
                                      <div class="d-flex justify-center items-center size-30 rounded-full bg-purple-3 mr-10">
                                        <!-- <div class="icon-play text-9"></div> --><i class="fa fa-arrow-right"></i>
                                      </div>
                                      <div><?php echo $courses[0]->entry_requirements; ?></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <?php } ?>

                          <?php if($courses[0]->remarks != '') { ?>
                          <div class="accordion__item">
                            <div class="accordion__button py-20 px-30 bg-light-4">
                              <div class="d-flex items-center">
                                <div class="accordion__icon">
                                  <div class="icon" data-feather="chevron-down"></div>
                                  <div class="icon" data-feather="chevron-up"></div>
                                </div>
                                <span class="text-17 fw-500 text-dark-1">Remarks</span>
                              </div>
                            </div>

                            <div class="accordion__content">
                              <div class="accordion__content__inner px-30 py-30">
                                <div class="y-gap-20">

                                  <div class="d-flex justify-between">
                                    <div class="d-flex items-center">
                                      <div class="d-flex justify-center items-center size-30 rounded-full bg-purple-3 mr-10">
                                        <!-- <div class="icon-play text-9"></div> --><i class="fa fa-arrow-right"></i>
                                      </div>
                                      <div><?php echo $courses[0]->remarks; ?></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                    

                    <div class="tabs__pane -tab-item-3">
                      <h2 class="text-20 fw-500">Instructor</h2>

                      <div class="mt-30">
                        <div class="d-flex x-gap-20 y-gap-20 items-center flex-wrap">
                          <div class="size-120">
                            <img class="object-cover" src="../la-assets/coursefinder/img/misc/verified/1.png" alt="image">
                          </div>

                          <div class="">
                            <h5 class="text-17 lh-14 fw-500">Floyd Miles</h5>
                            <p class="mt-5">President of Sales</p>

                            <div class="d-flex x-gap-20 y-gap-10 flex-wrap items-center pt-10">
                              <div class="d-flex items-center">
                                <div class="d-flex items-center mr-8">
                                  <div class="icon-star text-11 text-yellow-1"></div>
                                  <div class="text-14 lh-12 text-yellow-1 ml-5">4.5</div>
                                </div>
                                <div class="text-13 lh-1">Instructor Rating</div>
                              </div>

                              <div class="d-flex items-center text-light-1">
                                <div class="icon-comment text-13 mr-8"></div>
                                <div class="text-13 lh-1">23,987 Reviews</div>
                              </div>

                              <div class="d-flex items-center text-light-1">
                                <div class="icon-person-3 text-13 mr-8"></div>
                                <div class="text-13 lh-1">692 Students</div>
                              </div>

                              <div class="d-flex items-center text-light-1">
                                <div class="icon-wall-clock text-13 mr-8"></div>
                                <div class="text-13 lh-1">15 Course</div>
                              </div>

                            </div>
                          </div>
                        </div>

                        <div class="mt-30">
                          <p>
                            Back in 2010, I started brainspin with a desire to design compelling and engaging apps. For over 7 years, I have designed many high profile web and iPhone applications. The applications range from 3D medical aided web applications to project management applications for niche industries.
                            <br><br>
                            I am also the founder of a large local design organization, Salt Lake Designers, where I and other local influencers help cultivate the talents of up and coming UX designers through workshops and panel discussions.
                          </p>
                        </div>
                      </div>
                    </div>

                    <div class="tabs__pane -tab-item-4">
                      <div class="blogPost -comments">
                        <div class="blogPost__content">
                          <h2 class="text-20 fw-500">Student feedback</h2>
                          <div class="row x-gap-10 y-gap-10 pt-30">
                            <div class="col-md-4">
                              <div class="d-flex items-center justify-center flex-column py-50 text-center bg-light-6 rounded-8">
                                <div class="text-60 lh-11 text-dark-1 fw-500">4.8</div>
                                <div class="d-flex x-gap-5 mt-10">
                                  <div class="icon-star text-11 text-yellow-1"></div>
                                  <div class="icon-star text-11 text-yellow-1"></div>
                                  <div class="icon-star text-11 text-yellow-1"></div>
                                  <div class="icon-star text-11 text-yellow-1"></div>
                                  <div class="icon-star text-11 text-yellow-1"></div>
                                </div>
                                <div class="mt-10">Course Rating</div>
                              </div>
                            </div>

                            <div class="col-md-8">
                              <div class="py-20 px-30 bg-light-6 rounded-8">
                                <div class="row y-gap-15">

                                  <div class="col-12">
                                    <div class="d-flex items-center">
                                      <div class="progress-bar w-1/1 mr-15">
                                        <div class="progress-bar__bg bg-light-12"></div>
                                        <div class="progress-bar__bar bg-purple-1 w-1/1"></div>
                                      </div>
                                      <div class="d-flex x-gap-5 pr-15">
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                      </div>
                                      <div class="text-dark-1">70%</div>
                                    </div>
                                  </div>

                                  <div class="col-12">
                                    <div class="d-flex items-center">
                                      <div class="progress-bar w-1/1 mr-15">
                                        <div class="progress-bar__bg bg-light-12"></div>
                                        <div class="progress-bar__bar bg-purple-1 w-1/2"></div>
                                      </div>
                                      <div class="d-flex x-gap-5 pr-15">
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                      </div>
                                      <div class="text-dark-1">15%</div>
                                    </div>
                                  </div>

                                  <div class="col-12">
                                    <div class="d-flex items-center">
                                      <div class="progress-bar w-1/1 mr-15">
                                        <div class="progress-bar__bg bg-light-12"></div>
                                        <div class="progress-bar__bar bg-purple-1 w-1/3"></div>
                                      </div>
                                      <div class="d-flex x-gap-5 pr-15">
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                      </div>
                                      <div class="text-dark-1">20%</div>
                                    </div>
                                  </div>

                                  <div class="col-12">
                                    <div class="d-flex items-center">
                                      <div class="progress-bar w-1/1 mr-15">
                                        <div class="progress-bar__bg bg-light-12"></div>
                                        <div class="progress-bar__bar bg-purple-1 w-1/5"></div>
                                      </div>
                                      <div class="d-flex x-gap-5 pr-15">
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                      </div>
                                      <div class="text-dark-1">3%</div>
                                    </div>
                                  </div>

                                  <div class="col-12">
                                    <div class="d-flex items-center">
                                      <div class="progress-bar w-1/1 mr-15">
                                        <div class="progress-bar__bg bg-light-12"></div>
                                        <div class="progress-bar__bar bg-purple-1 w-1/7"></div>
                                      </div>
                                      <div class="d-flex x-gap-5 pr-15">
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                        <div class="icon-star text-11 text-yellow-1"></div>
                                      </div>
                                      <div class="text-dark-1">2%</div>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                          </div>

                          <h2 class="text-20 fw-500 mt-60 lg:mt-40">Reviews</h2>
                          <ul class="comments__list mt-30">
                            <li class="comments__item">
                              <div class="comments__item-inner md:direction-column">
                                <div class="comments__img mr-20">
                                  <div class="bg-image rounded-full js-lazy" data-bg="../la-assets/coursefinder/img/avatars/1.png"></div>
                                </div>

                                <div class="comments__body md:mt-15">
                                  <div class="comments__header">
                                    <h4 class="text-17 fw-500 lh-15">
                                      Ali Tufan
                                      <span class="text-13 text-light-1 fw-400">3 Days ago</span>
                                    </h4>

                                    <div class="stars"></div>
                                  </div>

                                  <h5 class="text-15 fw-500 mt-15">The best LMS Design</h5>
                                  <div class="comments__text mt-10">
                                    <p>This course is a very applicable. Professor Ng explains precisely each algorithm and even tries to give an intuition for mathematical and statistic concepts behind each algorithm. Thank you very much.</p>
                                  </div>

                                  <div class="comments__helpful mt-20">
                                    <span class="text-13 text-purple-1">Was this review helpful?</span>
                                    <button class="button text-13 -sm -purple-1 text-white">Yes</button>
                                    <button class="button text-13 -sm -light-7 text-purple-1">No</button>
                                  </div>
                                </div>
                              </div>
                            </li>

                            <li class="comments__item">
                              <div class="comments__item-inner md:direction-column">
                                <div class="comments__img mr-20">
                                  <div class="bg-image rounded-full js-lazy" data-bg="../la-assets/coursefinder/img/avatars/1.png"></div>
                                </div>

                                <div class="comments__body md:mt-15">
                                  <div class="comments__header">
                                    <h4 class="text-17 fw-500 lh-15">
                                      Ali Tufan
                                      <span class="text-13 text-light-1 fw-400">3 Days ago</span>
                                    </h4>

                                    <div class="stars"></div>
                                  </div>

                                  <h5 class="text-15 fw-500 mt-15">The best LMS Design</h5>
                                  <div class="comments__text mt-10">
                                    <p>This course is a very applicable. Professor Ng explains precisely each algorithm and even tries to give an intuition for mathematical and statistic concepts behind each algorithm. Thank you very much.</p>
                                  </div>

                                  <div class="comments__helpful mt-20">
                                    <span class="text-13 text-purple-1">Was this review helpful?</span>
                                    <button class="button text-13 -sm -purple-1 text-white">Yes</button>
                                    <button class="button text-13 -sm -light-7 text-purple-1">No</button>
                                  </div>
                                </div>
                              </div>
                            </li>

                            <li class="comments__item">
                              <div class="d-flex justify-center">
                                <button class="text-purple-1 lh-12 underline fw-500">View All Reviews</button>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>

                      <div class="respondForm pt-60">
                        <h3 class="text-20 fw-500">
                          Write a Review
                        </h3>

                        <div class="mt-30">
                          <h4 class="text-16 fw-500">What is it like to Course?</h4>
                          <div class="d-flex x-gap-10 pt-10">
                            <div class="icon-star text-14 text-yellow-1"></div>
                            <div class="icon-star text-14 text-yellow-1"></div>
                            <div class="icon-star text-14 text-yellow-1"></div>
                            <div class="icon-star text-14 text-yellow-1"></div>
                            <div class="icon-star text-14 text-yellow-1"></div>
                          </div>
                        </div>

                        <form class="contact-form respondForm__form row y-gap-30 pt-30" action="courses-single-4.html#">
                          <div class="col-12">
                            <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Review Title</label>
                            <input type="text" name="title" placeholder="Great Courses">
                          </div>
                          <div class="col-12">
                            <label class="text-16 lh-1 fw-500 text-dark-1 mb-10">Review Content</label>
                            <textarea name="comment" placeholder="Message" rows="8"></textarea>
                          </div>
                          <div class="col-12">
                            <button type="submit" name="submit" id="submit" class="button -md -purple-1 text-white">
                              Submit Review
                            </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <?php if(count($unvcourses) > 0) { ?>
        <section class="layout-pt-md layout-pb-lg">
          <div data-anim-wrap class="container">
            <div class="row">
              <div class="col-auto">

                <div class="sectionTitle ">

                  <h2 class="sectionTitle__title course_desc">Related Courses</h2>

                  <p class="sectionTitle__text "></p>

                </div>

              </div>
            </div>

            <div class="relative pt-60 lg:pt-50 course_desc">
              <div class="overflow-hidden js-section-slider" data-gap="30" data-loop data-pagination data-nav-prev="js-courses-prev" data-nav-next="js-courses-next" data-slider-cols="xl-4 lg-3 md-2">
                <div class="swiper-wrapper">
                  <?php
                  $i=1; 
                  foreach($unvcourses as $res) { ?>
                  <div data-anim-child="slide-up delay-<?php echo $i; ?>" class="swiper-slide">

                    <!-- <a href="<?php //echo url('/coursedetails/'.$res->id); ?>" class="coursesCard -type-1 "> -->
                    <a href="<?php echo url('/coursedetails/'.Crypt::encrypt($res->id)); ?>" class="coursesCard -type-1 ">
                      <div class="relative">
                        <div class="coursesCard__image overflow-hidden rounded-8">
                          <?php if($res->university_logo!='') { ?>
                            <img class="w-1/1 course_logo" src="<?php echo $res->university_logo; ?>" alt="image">
                          <?php } else { ?>
                            <img class="w-1/1 course_logo" src="../la-assets/coursefinder/img/coursesCards/no-image.png" alt="image">
                          <?php } ?>
                          <div class="coursesCard__image_overlay rounded-8"></div>
                        </div>
                        <div class="d-flex justify-between py-10 px-10 absolute-full-center z-3">

                        </div>
                      </div>

                      <div class="h-100 pt-15">
                        <div class="d-flex items-center">
                          <div class="text-14 lh-1 text-yellow-1 mr-10"><?php echo $res->university; ?></div>
                        </div>

                        <div class="text-17 lh-15 fw-500 text-dark-1 mt-10"><?php echo $res->course_name; ?></div>

                        <div class="d-flex x-gap-10 items-center pt-10">

                          <div class="d-flex items-center">
                            <div class="mr-8">
                              <img src="../la-assets/coursefinder/img/coursesCards/icons/location-icon.png" alt="icon">
                            </div>
                            <div class="text-14 lh-1"><?php echo $res->country; ?></div>
                          </div>

                          <div class="d-flex items-center">
                            <div class="mr-8">
                              <img src="../la-assets/coursefinder/img/coursesCards/icons/2.svg" alt="icon">
                            </div>
                            <div class="text-14 lh-1"><?php echo $res->intake; ?></div>
                          </div>

                          <div class="d-flex items-center">
                            <div class="mr-8">
                              <img src="../la-assets/coursefinder/img/coursesCards/icons/1.svg" alt="icon">
                            </div>
                            <div class="text-14 lh-1"><?php echo $res->course_type; ?></div>
                          </div>

                        </div>

                        <div class="coursesCard-footer">
                          <div class="coursesCard-footer__author">
                            <div>Annual Tuition Fees</div>
                          </div>

                          <div class="coursesCard-footer__price">
                            <div></div>
                            <div>&#8377; <?php echo $res->tuition_fees; ?></div>
                          </div>
                        </div>
                      </div>
                    </a>

                  </div>
                  <?php $i++; } ?>

                </div>
              </div>


              <button class="section-slider-nav -prev -dark-bg-dark-2 -white -absolute size-70 rounded-full shadow-5 js-courses-prev">
                <i class="fa fa-angle-left"></i>
              </button>

              <button class="section-slider-nav -next -dark-bg-dark-2 -white -absolute size-70 rounded-full shadow-5 js-courses-next">
                <i class="fa fa-angle-right"></i>
              </button>

            </div>
          </div>
        </section>
        <?php } ?>

        <footer class="footer -type-1 bg-dark-1 -green-links">
          <div class="container">
            
            <div class="py-30 border-top-light-15">
              <div class="row justify-between items-center y-gap-20">
                <div class="col-auto">
                  <div class="d-flex items-center h-100 text-white">
                    © 2022 ZODOCS. All Right Reserved.
                  </div>
                </div>

                <div class="col-auto">
                  <div class="d-flex x-gap-20 y-gap-20 items-center flex-wrap">
                    <div>Powered By
                      <a href="http://demo.zodocs.com/" class="text-white" target="_blank">
                        <span class="text-15">ZODOCS</span>
                      </a>. Solution By
                      <a href="https://www.labgex.com/" class="text-white" target="_blank">
                        <span class="text-15">LabGex Innovations (P) Ltd</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>


      </div>
    </main>
  </div>
  <!-- barba container end -->

  <!-- JavaScript -->
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
  <script src="../la-assets/coursefinder/js/vendors.js"></script>
  <script src="../la-assets/coursefinder/js/main.js"></script>

</body>

</html>