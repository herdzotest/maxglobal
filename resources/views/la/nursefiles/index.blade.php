@extends("la.layouts.app")

@section("contentheader_title", "Nurse files")
@section("htmlheader_title", "Nurse files")


<style>
#mx-nurse-files img { height:46px; }
#mx-nurse-files input[type="checkbox"] {
	margin-right: 12px;
    vertical-align: middle;
}
#mx-nurse-files .fa-file-pdf-o, #mx-nurse-files .fa-file-text-o {
    font-size: 38px;
    text-align: center;
    padding-top: 13px;
}
</style>

@section("headerElems")
@la_access("Employees", "create")
	<button class="btn btn-success btn-sm pull-right addbtn" data-toggle="modal" data-target="#AddModal"><i class="icon-user-follow"></i> Send Mail</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(Session::has('success_message'))
	<p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('success_message') }}</p>
@endif

<div class="box box-success">
	<div class="box-body">
		<table id="mx-nurse-files" class="table table-hover mg-table mgt2">
		<thead>
			<tr class="success">
				<th></th>
				<th>Nurse</th>
				<th>type</th>
				<th>File</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach($personal_information as $files) {
				if($files->skype_picture != '') {
				
					$fullImageSkype = explode("?",$files->skype_picture);
					$skype_ext = pathinfo($fullImageSkype[0], PATHINFO_EXTENSION);

					if( !in_array($skype_ext, ["jpg", "jpeg", "png", "gif", "bmp"]) ) {
						switch ($skype_ext) {
							case "pdf":
								$skypeElement = '<i class="fa fa-file-pdf-o"></i>';
								break;
							default:
								$skypeElement = '<i class="fa fa-file-text-o"></i>';
								break;
						}
					} else {
						$skypeElement = '<img src="'.$files->skype_picture.'" alt=""></a>';
					}		
					echo '<tr><td></td>';
					echo '<td><input type="checkbox" data-type="Skype Profile Screenshot" data-name="'.$files->name.'" data-file="'.$fullImageSkype[0].'" data-link="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'"><a href="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'" target="_blank">'.$files->name.'</a></td>';
					echo '<td>Skype Profile Screenshot</td>';
					echo '<td><a href="'.$fullImageSkype[0].'" target="_blank">'.$skypeElement.'</a></td>';
					echo '</tr>';
				}
				if($files->passport_upload != '') {
					
					$fullImagePassport = explode("?",$files->passport_upload);
					$skype_ext = pathinfo($fullImagePassport[0], PATHINFO_EXTENSION);
					
					if( !in_array($skype_ext, ["jpg", "jpeg", "png", "gif", "bmp"]) ) {
						switch ($skype_ext) {
							case "pdf":
								$skypeElement = '<i class="fa fa-file-pdf-o"></i>';
								break;
							default:
								$skypeElement = '<i class="fa fa-file-text-o"></i>';
								break;
						}
					} else {
						$skypeElement = '<img src="'.$files->passport_upload.'" alt=""></a>';
					}					
					echo '<tr><td></td>';
					echo '<td><input type="checkbox" data-type="Passport Page" data-name="'.$files->name.'" data-file="'.$fullImagePassport[0].'" data-link="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'"><a href="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'" target="_blank">'.$files->name.'</a></td>';
					echo '<td>Passport Page</td>';
					echo '<td><a href="'.$fullImagePassport[0].'" target="_blank">'.$skypeElement.'</a></td>';
					echo '</tr>';
				}
			}
			foreach($qualifications as $files) {
				if($files->certificate_upload != '') {
					
					$certificate = explode("?",$files->certificate_upload);
					$certificate_ext = pathinfo($certificate[0], PATHINFO_EXTENSION);

					if( !in_array($certificate_ext, ["jpg", "jpeg", "png", "gif", "bmp"]) ) {
						switch ($certificate_ext) {
							case "pdf":
								$certificateElement = '<i class="fa fa-file-pdf-o"></i>';
								break;
							default:
								$certificateElement = '<i class="fa fa-file-text-o"></i>';
								break;
						}
					} else {
						$certificateElement = '<img src="'.$files->certificate_upload.'" alt=""></a>';
					}

					echo '<tr><td></td>';
					echo '<td><input type="checkbox" data-type="Qualification certificate" data-name="'.$files->name.'" data-file="'.$certificate[0].'" data-link="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'"><a href="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'" target="_blank">'.$files->name.'</a></td>';
					echo '<td>Qualification certificate</td>';
					echo '<td><a href="'.$certificate[0].'" target="_blank">'.$certificateElement.'</a></td>';
					echo '</tr>';
				}
				if($files->evidence_upload != '') {
					
					$certificate = explode("?",$files->evidence_upload);
					$certificate_ext = pathinfo($certificate[0], PATHINFO_EXTENSION);
					
					if( !in_array($certificate_ext, ["jpg", "jpeg", "png", "gif", "bmp"]) ) {
						switch ($certificate_ext) {
							case "pdf":
								$certificateElement = '<i class="fa fa-file-pdf-o"></i>';
								break;
							default:
								$certificateElement = '<i class="fa fa-file-text-o"></i>';
								break;
						}
					} else {
						$certificateElement = '<img src="'.$files->evidence_upload.'" alt=""></a>';
					}
					echo '<tr><td></td>';
					echo '<td><input type="checkbox" data-type="Evidence upload" data-name="'.$files->name.'" data-file="'.$certificate[0].'" data-link="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'"><a href="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'" target="_blank">'.$files->name.'</a></td>';
					echo '<td>Evidence upload</td>';
					echo '<td><a href="'.$certificate[0].'" target="_blank">'.$certificateElement.'</a></td>';
					echo '</tr>';
				}
			}
			foreach($employement_history as $files) {
				if($files->cv_file_upload != '') {
					
					$certificate = explode("?",$files->cv_file_upload);
					$certificate_ext = pathinfo($certificate[0], PATHINFO_EXTENSION);

					if( !in_array($certificate_ext, ["jpg", "jpeg", "png", "gif", "bmp"]) ) {
						switch ($certificate_ext) {
							case "pdf":
								$certificateElement = '<i class="fa fa-file-pdf-o"></i>';
								break;
							default:
								$certificateElement = '<i class="fa fa-file-text-o"></i>';
								break;
						}
					} else {
						$certificateElement = '<img src="'.$files->cv_file_upload.'" alt=""></a>';
					}

					echo '<tr><td></td>';
					echo '<td><input type="checkbox" data-type="CV file"  data-name="'.$files->name.'" data-file="'.$certificate[0].'" data-link="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'"><a href="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'" target="_blank">'.$files->name.'</a></td>';
					echo '<td>CV file</td>';
					echo '<td><a href="'.$certificate[0].'" target="_blank">'.$certificateElement.'</a></td>';
					echo '</tr>';
				}
			}
			foreach($english_language as $files) {
				if($files->ielts_file_upload != '') {
					
					$certificate = explode("?",$files->ielts_file_upload);
					$certificate_ext = pathinfo($certificate[0], PATHINFO_EXTENSION);

					if( !in_array($certificate_ext, ["jpg", "jpeg", "png", "gif", "bmp"]) ) {
						switch ($certificate_ext) {
							case "pdf":
								$certificateElement = '<i class="fa fa-file-pdf-o"></i>';
								break;
							default:
								$certificateElement = '<i class="fa fa-file-text-o"></i>';
								break;
						}
					} else {
						$certificateElement = '<img src="'.$files->ielts_file_upload.'" alt=""></a>';
					}

					echo '<tr><td></td>';
					echo '<td><input type="checkbox" data-type="IELTS File" data-name="'.$files->name.'" data-file="'.$certificate[0].'" data-link="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'"><a href="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'" target="_blank">'.$files->name.'</a></td>';
					echo '<td>IELTS File</td>';
					echo '<td><a href="'.$certificate[0].'" target="_blank">'.$certificateElement.'</a></td>';
					echo '</tr>';
				}
			}
			foreach($employer_details as $files) {
				if($files->offer_letter_file != '') {
					
					$certificate = explode("?",$files->offer_letter_file);
					$certificate_ext = pathinfo($certificate[0], PATHINFO_EXTENSION);

					if( !in_array($certificate_ext, ["jpg", "jpeg", "png", "gif", "bmp"]) ) {
						switch ($certificate_ext) {
							case "pdf":
								$certificateElement = '<i class="fa fa-file-pdf-o"></i>';
								break;
							default:
								$certificateElement = '<i class="fa fa-file-text-o"></i>';
								break;
						}
					} else {
						$certificateElement = '<img src="'.$files->offer_letter_file.'" alt=""></a>';
					}

					echo '<tr><td></td>';
					echo '<td><input type="checkbox" data-type="Offer letter File" data-name="'.$files->name.'" data-file="'.$certificate[0].'" data-link="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'"><a href="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'" target="_blank">'.$files->name.'</a></td>';
					echo '<td>Offer letter File</td>';
					echo '<td><a href="'.$certificate[0].'" target="_blank">'.$certificateElement.'</a></td>';
					echo '</tr>';
				}
			}
			foreach($nurse_nmc as $files) {
				if($files->scope_file_upload != '') {
					
					$certificate = explode("?",$files->scope_file_upload);
					$certificate_ext = pathinfo($certificate[0], PATHINFO_EXTENSION);

					if( !in_array($certificate_ext, ["jpg", "jpeg", "png", "gif", "bmp"]) ) {
						switch ($certificate_ext) {
							case "pdf":
								$certificateElement = '<i class="fa fa-file-pdf-o"></i>';
								break;
							default:
								$certificateElement = '<i class="fa fa-file-text-o"></i>';
								break;
						}
					} else {
						$certificateElement = '<img src="'.$files->scope_file_upload.'" alt=""></a>';
					}

					echo '<tr><td></td>';
					echo '<td><input type="checkbox" data-type="Pcc Scope File" data-name="'.$files->name.'" data-file="'.$certificate[0].'" data-link="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'"><a href="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'" target="_blank">'.$files->name.'</a></td>';
					echo '<td>Pcc Scope File</td>';
					echo '<td><a href="'.$certificate[0].'" target="_blank">'.$certificateElement.'</a></td>';
					echo '</tr>';
				}
				if($files->cbt_upload != '') {
					
					$certificate = explode("?",$files->cbt_upload);
					$certificate_ext = pathinfo($certificate[0], PATHINFO_EXTENSION);

					if( !in_array($certificate_ext, ["jpg", "jpeg", "png", "gif", "bmp"]) ) {
						switch ($certificate_ext) {
							case "pdf":
								$certificateElement = '<i class="fa fa-file-pdf-o"></i>';
								break;
							default:
								$certificateElement = '<i class="fa fa-file-text-o"></i>';
								break;
						}
					} else {
						$certificateElement = '<img src="'.$files->cbt_upload.'" alt=""></a>';
					}

					echo '<tr><td></td>';
					echo '<td><input type="checkbox" data-type="CBT File" data-name="'.$files->name.'" data-file="'.$certificate[0].'" data-link="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'"><a href="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'" target="_blank">'.$files->name.'</a></td>';
					echo '<td>CBT File</td>';
					echo '<td><a href="'.$certificate[0].'" target="_blank">'.$certificateElement.'</a></td>';
					echo '</tr>';
				}
				if($files->nmc_file_upload != '') {
					
					$certificate = explode("?",$files->nmc_file_upload);
					$certificate_ext = pathinfo($certificate[0], PATHINFO_EXTENSION);

					if( !in_array($certificate_ext, ["jpg", "jpeg", "png", "gif", "bmp"]) ) {
						switch ($certificate_ext) {
							case "pdf":
								$certificateElement = '<i class="fa fa-file-pdf-o"></i>';
								break;
							default:
								$certificateElement = '<i class="fa fa-file-text-o"></i>';
								break;
						}
					} else {
						$certificateElement = '<img src="'.$files->nmc_file_upload.'" alt=""></a>';
					}

					echo '<tr><td></td>';
					echo '<td><input type="checkbox" data-type="NMC File" data-name="'.$files->name.'" data-file="'.$certificate[0].'" data-link="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'"><a href="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'" target="_blank">'.$files->name.'</a></td>';
					echo '<td>NMC File</td>';
					echo '<td><a href="'.$certificate[0].'" target="_blank">'.$certificateElement.'</a></td>';
					echo '</tr>';
				}
				if($files->birth_cert_upload != '') {
					
					$certificate = explode("?",$files->birth_cert_upload);
					$certificate_ext = pathinfo($certificate[0], PATHINFO_EXTENSION);

					if( !in_array($certificate_ext, ["jpg", "jpeg", "png", "gif", "bmp"]) ) {
						switch ($certificate_ext) {
							case "pdf":
								$certificateElement = '<i class="fa fa-file-pdf-o"></i>';
								break;
							default:
								$certificateElement = '<i class="fa fa-file-text-o"></i>';
								break;
						}
					} else {
						$certificateElement = '<img src="'.$files->birth_cert_upload.'" alt=""></a>';
					}

					echo '<tr><td></td>';
					echo '<td><input type="checkbox" data-type="Birth certificate File" data-name="'.$files->name.'" data-file="'.$certificate[0].'" data-link="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'"><a href="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'" target="_blank">'.$files->name.'</a></td>';
					echo '<td>Birth certificate File</td>';
					echo '<td><a href="'.$certificate[0].'" target="_blank">'.$certificateElement.'</a></td>';
					echo '</tr>';
				}					
			}
			foreach($dependant_det as $files) {
				if($files->spouse_passport != '') {
					
					$certificate = explode("?",$files->spouse_passport);
					$certificate_ext = pathinfo($certificate[0], PATHINFO_EXTENSION);

					if( !in_array($certificate_ext, ["jpg", "jpeg", "png", "gif", "bmp"]) ) {
						switch ($certificate_ext) {
							case "pdf":
								$certificateElement = '<i class="fa fa-file-pdf-o"></i>';
								break;
							default:
								$certificateElement = '<i class="fa fa-file-text-o"></i>';
								break;
						}
					} else {
						$certificateElement = '<img src="'.$files->spouse_passport.'" alt=""></a>';
					}

					echo '<tr><td></td>';
					echo '<td><input type="checkbox" data-type="Spouse passport" data-name="'.$files->name.'" data-file="'.$certificate[0].'" data-link="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'"><a href="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'" target="_blank">'.$files->name.'</a></td>';
					echo '<td>Spouse passport</td>';
					echo '<td><a href="'.$certificate[0].'" target="_blank">'.$certificateElement.'</a></td>';
					echo '</tr>';
				}
			}
			foreach($visa_imigration as $files) {
				if($files->tb_file_upload != '') {
					
					$certificate = explode("?",$files->tb_file_upload);
					$certificate_ext = pathinfo($certificate[0], PATHINFO_EXTENSION);

					if( !in_array($certificate_ext, ["jpg", "jpeg", "png", "gif", "bmp"]) ) {
						switch ($certificate_ext) {
							case "pdf":
								$certificateElement = '<i class="fa fa-file-pdf-o"></i>';
								break;
							default:
								$certificateElement = '<i class="fa fa-file-text-o"></i>';
								break;
						}
					} else {
						$certificateElement = '<img src="'.$files->tb_file_upload.'" alt=""></a>';
					}

					echo '<tr><td></td>';
					echo '<td><input type="checkbox" data-type="TB file" data-name="'.$files->name.'" data-file="'.$certificate[0].'" data-link="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'"><a href="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'" target="_blank">'.$files->name.'</a></td>';
					echo '<td>TB file</td>';
					echo '<td><a href="'.$certificate[0].'" target="_blank">'.$certificateElement.'</a></td>';
					echo '</tr>';
				}
				if($files->spons_file_upload != '') {
					
					$certificate = explode("?",$files->spons_file_upload);
					$certificate_ext = pathinfo($certificate[0], PATHINFO_EXTENSION);

					if( !in_array($certificate_ext, ["jpg", "jpeg", "png", "gif", "bmp"]) ) {
						switch ($certificate_ext) {
							case "pdf":
								$certificateElement = '<i class="fa fa-file-pdf-o"></i>';
								break;
							default:
								$certificateElement = '<i class="fa fa-file-text-o"></i>';
								break;
						}
					} else {
						$certificateElement = '<img src="'.$files->spons_file_upload.'" alt=""></a>';
					}

					echo '<tr><td></td>';
					echo '<td><input type="checkbox" data-type="Sponsor file upload" data-name="'.$files->name.'" data-file="'.$certificate[0].'" data-link="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'"><a href="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'" target="_blank">'.$files->name.'</a></td>';
					echo '<td>Sponsor file upload</td>';
					echo '<td><a href="'.$certificate[0].'" target="_blank">'.$certificateElement.'</a></td>';
					echo '</tr>';
				}
			}
			foreach($nurse_payments as $files) {
				if($files->addl_docs != '') {
					
					$certificate = explode("?",$files->addl_docs);
					$certificate_ext = pathinfo($certificate[0], PATHINFO_EXTENSION);

					if( !in_array($certificate_ext, ["jpg", "jpeg", "png", "gif", "bmp"]) ) {
						switch ($certificate_ext) {
							case "pdf":
								$certificateElement = '<i class="fa fa-file-pdf-o"></i>';
								break;
							default:
								$certificateElement = '<i class="fa fa-file-text-o"></i>';
								break;
						}
					} else {
						$certificateElement = '<img src="'.$files->addl_docs.'" alt=""></a>';
					}

					echo '<tr><td></td>';
					echo '<td><input type="checkbox" data-type="TB file" data-name="'.$files->name.'" data-file="'.$certificate[0].'" data-link="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'"><a href="'.url(config('laraadmin.adminRoute') . '/nurses/'.$files->id).'" target="_blank">'.$files->name.'</a></td>';
					echo '<td>TB file</td>';
					echo '<td><a href="'.$certificate[0].'" target="_blank">'.$certificateElement.'</a></td>';
					echo '</tr>';
				}
			}				
			?>
		</tbody>
		</table>
	</div>
</div>

@la_access("Employees", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog mgmodal-dialog" role="document">
		<div class="modal-content mgmodal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Send files</h4>
			</div>
			{{ Form::open(array('url' => route('nursefiles.sendmail'),'id' => 'send-mail')) }}
			<div class="modal-body">
				<input name="_method" type="hidden" value="PUT">
				<div class="box-body">
					<div class="form-group">
						<label for="">Email * :</label>
						<input type="email" required class="form-control" placeholder="Enter email" value="" name="send_mail" class="">
					</div>
					<div class="form-group">
						<label for="">Enter content :</label>
						<textarea class="form-control" placeholder="Enter content" data-rule-maxlength="200" cols="30" rows="3" name="content"></textarea>
					</div>
					<input type="hidden" value="" name="jsonData" id="jsonData">
					<input type="hidden" value="<?php echo $nurseId; ?>" name="user_id" id="user_id">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default mgbtn2" data-dismiss="modal">Close</button>
				<button type="submit" class="btn mgbtn" id="sendEmail">Send </button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@include('la.common.imageUpload')
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>

@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/js/common.js') }}"></script>
<script>
$(function () {
	$("#mx-nurse-files").DataTable({
		processing: true,
        serverSide: false,
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		//columnDefs: [ { orderable: false, targets: [-1] }],
	});
	$("#send-mail").validate({	
	});
	$('body').on('click', '.addbtn', function(event) {

		var obj = $(".mg-table input[type='checkbox']:checked");
		var data = [];
		obj.each(function(key) {
			data[key] = {'type': $(this).attr('data-type'),'name': $(this).attr('data-name'),'file': $(this).attr('data-file'),'link': $(this).attr('data-link')};
		});
		$("#jsonData").val(JSON.stringify(data));
	});
});
</script>
@endpush
