@extends("la.layouts.app")

@section("contentheader_title", "Nurses section")
@section("htmlheader_title", "Nurses data")
@section("headerElems")
@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<style>
/* #mx_students_view .fa-file-pdf-o, #mx_students_view .fa-file-text-o {
    width: 89px;
    font-size: 71px;
    text-align: center;
    padding-top: 13px;
} */
/* New */
#mx_students_view .modal-dialog {
    width: 600px !important; 
    margin: 60px auto !important;
}
#mx_students_view .file-field input[type=file] {
    width:20%
}
#mx_students_view #AddModal .modal-content {
    Top: 0px
}
/* End New */
</style>

<div class="box box-success studentbox">
	<!--<div class="box-header"></div>-->
	<div class="box-body">

	<input type="hidden" value="<?php echo $studentId; ?>" id="student_id">
	<?php
	// for personal information form
	$postedValueflag = $postedValues = '';
	if (Session::has('posted_values') && $studentId == 0) {
		$postedValueflag = 1;
		$postedValues = json_encode(Session::get('posted_values')); 
		Session::forget('posted_values');
	}
	?>
	<input type="hidden" value="<?php echo $postedValueflag; ?>" id="posted_values_flag">
	<input type="hidden" value='<?php echo $postedValues; ?>' id="posted_values">
    <main class="clearfix">
                        <div class="form_sidebar ">

                                <ul >
                                        <li class="active">
                                                <a  class="active" href="#tab_1" data-toggle="tab" aria-expanded="true">
                                                        <span> 1 </span>
                                                        <div>Personal Information</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_2" data-toggle="tab" aria-expanded="false">
                                                        <span> 2 </span>
                                                        <div> Qualifications</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_3" data-toggle="tab" aria-expanded="false">
                                                        <span> 3 </span>
                                                        <div>Employment History</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_4" data-toggle="tab" aria-expanded="false">
                                                        <span> 4 </span>
                                                        <div>Preferences</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_5" data-toggle="tab" aria-expanded="false">
                                                        <span> 5 </span>
                                                        <div>English Language Evidence</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_6" data-toggle="tab" aria-expanded="false">
                                                        <span> 6 </span>
                                                        <div>Employer Details</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_7" data-toggle="tab" aria-expanded="false">
                                                        <span>7</span>
                                                        <div>NMC Registration</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_8" data-toggle="tab" aria-expanded="false">
                                                        <span>8</span>
                                                        <div>Dependant Details</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_9" data-toggle="tab" aria-expanded="false">
                                                        <span>9</span>
                                                        <div>Visa & Immigration</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_10" data-toggle="tab" aria-expanded="false">
                                                        <span>10</span>
                                                        <div>Deployment Details</div>
                                                </a>
                                        </li>
                                        <li>
                                                <a  href="#tab_11" data-toggle="tab" aria-expanded="false">
                                                        <span>11</span>
                                                        <div>Feedback</div>
                                                </a>
                                        </li>	
                                        <li>
                                                <a  href="#tab_12" data-toggle="tab" aria-expanded="false">
                                                        <span>12</span>
                                                        <div>Post Deployment Section</div>
                                                </a>
                                        </li>												
                                </ul>
                        </div>
                        <br>
                        
                        <div class="contents tab-content" id="mx_students_view">
                        @include('la.nurses.partials.personal')
						@include('la.nurses.partials.nurse_quqlification')
                        @include('la.nurses.partials.nurse_empl_hist')
                        @include('la.nurses.partials.nurse_preferences')
                        @include('la.nurses.partials.nurse_eng_lang_evidences')
						@include('la.nurses.partials.nurse_employer_details')
						@include('la.nurses.partials.nurse_nmc')
						@include('la.nurses.partials.nurse_dependant_details')
                        @include('la.nurses.partials.nurse_visa')
                        @include('la.nurses.partials.nurse_joining')
                        @include('la.nurses.partials.nurse_feedback')
						@include('la.nurses.partials.nurse_payments')
                        </div>
                </main>
	</div>
</div>

@la_access("Employees", "create")
<!-- <div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
	Upload Image
			</div>
			<div class="modal-body">
			
                <form action="{{ url(config('laraadmin.adminRoute') . '/upload_files') }}" id="fm_dropzone_main" enctype="multipart/form-data" method="POST">
    {{ csrf_field() }}
    <a id="closeDZ1"><i class="fa fa-times"></i></a>
    <div class="dz-message"><i class="fa fa-cloud-upload"></i><br>Drop files here to upload</div>
</form>
				</div>
		
		</div>
	</div>
</div> -->

<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">     
            <div class="modal-content">
                        <div class="modal-header hd-text">
                            Upload Image <br><div class="file_error_text"></div>
                        </div>
                <div class="modal-body">  <div class="image-upload-wrap">
                                    
        <form action="{{ url(config('laraadmin.adminRoute') . '/upload_files') }}" id="fm_dropzone_main" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            <a id="closeDZ1"><i class="fa fa-times"></i></a>
            <div class="dz-message drag-text"><i class="fa fa-cloud-upload"></i><br><h3>Drop files here to upload</h3></div>
        </form>
                </div>   </div>
            </div>
        </div>               
</div>


<div class="modal fade" id="AddModalImg" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">     
                                <div class="modal-content">
                                    <div class="modal-header hd-text">
                            Upload Image  <br><div class="file_error_text_img"></div>
                                    </div>

                                    <div class="modal-body">       <div class="image-upload-wrap">
                                    
                                        <form action="{{ url(config('laraadmin.adminRoute') . '/upload_files') }}" id="fm_dropzone_main_img" enctype="multipart/form-data" method="POST">
                            {{ csrf_field() }}
                            <a id="closeDZ1Img"><i class="fa fa-times"></i></a>
                            <div class="dz-message drag-text"><i class="fa fa-cloud-upload"></i><br><h3>Drop files here to upload</h3></div>
                        </form>
                                        </div>            </div>
                                </div>
                            </div>               
                        </div>


@endla_access

@endsection

@push('styles')

<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Varela+Round&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/stdentsform/css/normalize.css') }}"/>
<link rel="stylesheet" href="{{ asset('la-assets/stdentsform/css/materialize.css') }}">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/stdentsform/css/main.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/stdentsform/js/nurse_main.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<style> .uploaded-file { width: 150px; height: 150px;}</style>

<script>
var bsurl = $('body').attr("bsurl");
var fm_dropzone_main = null;
var fm_dropzone_main_img = null;
var cntFiles = null;
var count=null;
var elem;
$(function () {
	$('#AddModal').modal({
		dismissible: true
	});

    $('#AddModalImg').modal({
        dismissible: true
    });

	@la_access("Uploads", "create")
	fm_dropzone_main = new Dropzone("#fm_dropzone_main", {
        maxFilesize: 2,
        //acceptedFiles: "image/*",

        accept: function(file, done) {
            console.log(file);
            if (file.type != "application/pdf") {
                $(".file_error_text").html("<p class='file_err'>Error! Files of this type are not accepted</p>");
                this.removeFile(file);
            }
            else {
                done();
            }
        },

        init: function() {
                this.on('addedfile', function(file) {
                if (this.files.length > 1) {
                    alert("Only 1 file allowed")
                this.removeFile(this.files[0]);
                }
                });

            this.on("complete", function(file) {
                this.removeFile(file);  $(".file_error_text").html("");
            });
            this.on("success", function(file) {
                $('#AddModal').modal('open');
                loadUploadedFiles();    $(".file_error_text").html("");
            });
        }
    });


// Hide pictures
    $("#nurse-profile-remove-img").on("click", function(e) { 
        $("#nurse-pro-img").val('');
        $("#profile_image").addClass('hide');
        $("#nurse-profile-remove-img").hide();
    });
// End Hide pictures


	
    //$(".btn input").on("click", function(e) {
    //$("#mx_students_view").on("click", ".btn1 input", function(e) {
    //$("#mx_students_view").on("click", ".btn input", function(e) {
    $("#mx_students_view").on("click", ".btn7 input", function(e) {
        e.preventDefault();
        elem=$(this).closest('.file-field').find(".file-path-wrapper input");
        $('#AddModal').modal('open');
         //$("#fm_dropzone_main").slideDown();
    });
    $("#closeDZ1").on("click", function() {
        $('#AddModal').modal('close');
    });



    // Image Upload
    fm_dropzone_main_img = new Dropzone("#fm_dropzone_main_img", {
        maxFilesize: 2,
        //acceptedFiles: "image/*,application/pdf",

        accept: function(file, done) {
            console.log(file);
            if (file.type != "image/jpeg" && file.type != "image/jpg" && file.type != "image/png" && file.type != "image/gif" && file.type != "image/bmp") {
                $(".file_error_text_img").html("<p class='file_err'>Error! Files of this type are not accepted</p>");
                this.removeFile(file);
            }
            else {
                done();
            }
        },

        init: function() {
                this.on('addedfile', function(file) {
                if (this.files.length > 1) {
                    alert("Only 1 file allowed")
                this.removeFile(this.files[0]);
                }
                });

            this.on("complete", function(file) {    $('#AddModalImg').modal('close');
                this.removeFile(file);  $(".file_error_text_img").html("");
            });
            this.on("success", function(file) {
                $('#AddModalImg').modal('open');    
                loadUploadedFiles();    $(".file_error_text_img").html("");
            });
        }
    });

    $("#mx_students_view").on("click", ".btn9 input", function(e) {
        e.preventDefault();
        elem=$(this).closest('.file-field').find(".file-path-wrapper input");
        $('#AddModalImg').modal('open');
    });
    $("#closeDZ1Img").on("click", function() {
        $('#AddModalImg').modal('close');
    });
// End Image Upload




	@endla_access
	
    $("body").on("click", "ul.files_container .fm_file_sel", function() {
        var upload = $(this).attr("upload");
        upload = JSON.parse(upload);
        $("#EditFileModal .modal-title").html("File: "+upload.name);
        $(".file-info-form input[name=file_id]").val(upload.id);
        $(".file-info-form input[name=filename]").val(upload.name);
        $(".file-info-form input[name=url]").val(bsurl+'/files/'+upload.hash+'/'+upload.name);
        $(".file-info-form input[name=caption]").val(upload.caption);
        $("#EditFileModal #downFileBtn").attr("href", bsurl+'/files/'+upload.hash+'/'+upload.name+"?download");
        

        @if(!config('laraadmin.uploads.private_uploads'))
        if(upload.public == "1") {
            $(".file-info-form input[name=public]").attr("checked", !0);
            $(".file-info-form input[name=public]").next().removeClass("On").addClass("Off");
        } else {
            $(".file-info-form input[name=public]").attr("checked", !1);
            $(".file-info-form input[name=public]").next().removeClass("Off").addClass("On");
        }
        @endif

        $("#EditFileModal .fileObject").empty();
        if($.inArray(upload.extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
            $("#EditFileModal .fileObject").append('<img src="'+bsurl+'/files/'+upload.hash+'/'+upload.name+'">');
            $("#EditFileModal .fileObject").css("padding", "15px 0px");
        } else {
            switch (upload.extension) {
                case "pdf":
                    // TODO: Object PDF
                    $("#EditFileModal .fileObject").append('<object width="100%" height="325" data="'+bsurl+'/files/'+upload.hash+'/'+upload.name+'"></object>');
                    $("#EditFileModal .fileObject").css("padding", "0px");
                    break;
                default:
                    $("#EditFileModal .fileObject").append('<i class="fa fa-file-text-o"></i>');
                    $("#EditFileModal .fileObject").css("padding", "15px 0px");
                    break;
            }
        }
        $("#EditFileModal").modal('show');
    });
    @if(!config('laraadmin.uploads.private_uploads') && Module::hasFieldAccess("Uploads", "public", "write"))
    $('#EditFileModal .Switch.Ajax').click(function() {
        $.ajax({
            url: "{{ url(config('laraadmin.adminRoute') . '/uploads_update_public') }}",
            method: 'POST',
            data: $("form.file-info-form").serialize(),
            success: function( data ) {
               loadUploadedFiles();
            }
        });
        
    });
    @endif

    loadUploadedFiles();
	
	/* get student data on edit section */
	var studentId = $("#student_id").val();
	$(".uploaded-file").addClass("hide");
	
	$('body').off('click', '.form_sidebar li a');
	$('body').on('click', '.form_sidebar li a', function() {
		
		type = $(this).attr('href');
		getStdentData(studentId,type);
	});	
	if(studentId != '0' && ((localStorage.getItem('activeTab') == null) || (localStorage.getItem('activeTab') == 'tab_1')) ) { 
		getStdentData(studentId,'tab_1');
		$("#mx_students_view input,#mx_students_view textarea").prop("readonly", false);
	} else {
		var activeTab = localStorage.getItem('activeTab');
		$(".studentbox").find('.form_sidebar li a[href="#'+activeTab+'"]').trigger("click");
	}
	
	// prevent navigation on student create
	$('body').on('click', '.form_sidebar li', function(event) {
		if(studentId == '0') {
			event.stopPropagation();
		} else {
			setTimeout(function(){ 
				var obj = $(".form_sidebar li.active a");
				var activeTab = obj.attr('href').replace("#",'');
				localStorage.setItem("activeTab",activeTab);				
			}, 800);
		}
	});
	// add more tab functionality
	$('body').on('click', '.addshbtn', function(event) {
		
		var wrapperCount = $("#mx_students_view .tab-pane.active .wrapper-count").val();
		var formType = $("#mx_students_view .tab-pane.active .formtype").val();			
		var indexVal = parseInt(wrapperCount)-1;
		switch(formType) {
		  case 'employement':
			$("#employement-wrapper").append('<div class="stdhst wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button> <div class="col-md-6"> <div class="mg-input-field "> <label for="employer_name">Job Title</label> <input class="form-control" id="" name="employement[' + indexVal + '][job_title]" type="text" required> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="work_nature">Organization Name</label> <input class="form-control" id="" name="employement[' + indexVal + '][org_name]" type="text" required> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="work_nature">Ward</label> <select class="formSelect" id="" name="employement[' + indexVal + '][ward]"> <option value="">Select</option> <option value="Ward 1">Ward 1</option><option value="Ward2">Ward2</option> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="employement_start_date">Start Date</label> <input class="form-control datepicker" id="" name="employement[' + indexVal + '][employement_start_date]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="to_date">End Date</label> <input class="form-control datepicker" id="" name="employement[' + indexVal + '][employement_end_date]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="to_date">Remarks</label> <input class="form-control" id="" name="employement[' + indexVal + '][remarks]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="working_status">Still working</label> <select class="formSelect" name="employement[' + indexVal + '][working_status]" id=""> <option value="">Select</option> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div>   </div> <div class="clearfix"></div> </div>');
			break;
		  case 'english':
			$("#eng-lang-wrapper").append('<div class="stdhst wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button> <div class="col-md-6"> <div class="mg-input-field "> <label for="ielts_country">Country</label> <select class="searchSelect" id="" name="english[' + indexVal + '][ielts_country]" required> <option value="Antigua and Barbuda">Antigua and Barbuda</option> <option value="Anguilla">Anguilla</option> <option value="Australia">Australia</option> <option value="Bahamas">Bahamas</option> <option value="Barbados">Barbados</option> <option value="Belize">Belize</option> <option value="Bermuda">Bermuda</option> <option value="British Indian Ocean Territory">British Indian Ocean Territory</option> <option value="Canada">Canada</option> <option value="Cayman Islands">Cayman Islands</option> <option value="Dominica">Dominica</option> <option value="Falkland Islands">Falkland Islands</option> <option value="Gibraltar">Gibraltar</option> <option value="Grenada">Grenada</option> <option value="Guernsey">Guernsey</option> <option value="Guernsey">Guernsey</option> <option value="Ireland">Ireland</option> <option value="Jamaica">Jamaica</option> <option value="Malta">Malta</option> <option value="New Zealand">New Zealand</option> <option value="Saint Helena">Saint Helena</option> <option value="Ascension and Tristan da Cunha">Ascension and Tristan da Cunha</option> <option value="St Kitts and Nevis">St Kitts and Nevis</option> <option value="St Lucia">St Lucia</option> <option value="St Vincent and the Grenadines">St Vincent and the Grenadines</option> <option value="Trinidad and Tobago">Trinidad and Tobago</option> <option value="United Kingdom">United Kingdom</option> <option value="United States">United States</option> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="work_nature">Work start date</label> <input class="form-control datepicker" id="" name="english[' + indexVal + '][work_start_date]" type="text" required> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="employement_start_date">End date</label> <input class="form-control datepicker" id="" name="english[' + indexVal + '][work_end_date]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="employer_addresss">Employer address</label> <input class="form-control" id="" name="english[' + indexVal + '][employer_addresss]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="to_date">Email of employer</label> <input class="form-control" id="" name="english[' + indexVal + '][employer_email]" type="text"> </div> </div>        <div class="col-md-6"> <div class="mg-input-field "> <label for="to_date">Reference letter</label> <input class="form-control" id="" name="english[' + indexVal + '][reference_letter]" type="text"> </div> </div>      <div class="col-md-6"> <div class="mg-input-field "> <label>NCC File Upload</label> <div class="file-field input-field"> <div class="btn7"> <span>Upload</span> <input type="file"><i class="fa fa-cloud-upload"></i> </div> <div class="file-path-wrapper"> <input class="file-path validate" name="english[' + indexVal + '][file_upload_nursing]" type="text"> <div> </div> </div> </div> <div class="uploaded-file" id="english[' + indexVal + '][file_upload_nursing]" data-type="file"></div> </div> </div>  <div class="clearfix"></div> </div>');
			break;
		  case 'children':
			$("#childrens-wrapper").append('<div class="stdhst wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button>   <div class="col-md-6"> <div class="mg-input-field "> <label for="name">Name (First Name + Family Name)</label> <input class="form-control" id="" name="children[' + indexVal + '][name]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="birthdate">Date of birth</label> <input class="form-control datepicker" id="" name="children[' + indexVal + '][birthdate]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="from_date">Relationship to you?</label> <select class="formSelect" name="children[' + indexVal + '][relation]" id=""> <option value="Son">Son</option> <option value="Daughter">Daughter</option> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="current_live">Does this person currently live with you?</label> <select name="children[' + indexVal + '][current_live]" id="" class="formSelect"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div><div class="col-md-6"> <div class="mg-input-field "> <label for="travel_to_uk">Is this person travelling with you to the UK?</label> <select class="formSelect" name="children[' + indexVal + '][travel_to_uk]" id=""> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="clearfix"></div> </div>');
			break;
		  case 'imigration':
			$("#imigration-wrapper").append('<div class="stdhst wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button>  <div class="col-md-6"> <div class="mg-input-field "> <label for="name">Country</label> <select class="searchSelect" name="imigration[' + indexVal + '][pc_country]" id="" required> <option value=""> Please Select </option>    <?php foreach($countries as $countriesData) { echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>'; } ?>  </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="visit_reason">Reason of visit</label> <select class="formSelect" id="" name="imigration[' + indexVal + '][visit_reason]"> <option value="">Select</option> <option value="Work"> Work </option> <option value="Tourism"> Tourism </option> <option value="Others"> Others </option> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="pc_entry_date">Date you entered</label> <input class="form-control datepicker" id="" name="imigration[' + indexVal + '][pc_entry_date]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="pc_exit_date">Date you left</label> <input class="form-control datepicker" id="" name="imigration[' + indexVal + '][pc_exit_date]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="current_live">Can you provide criminal record certificate for this country?</label> <select class="formSelect" name="imigration[' + indexVal + '][crim_record_cert]" id=""> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="col-md-6"> <!--<div class="mg-input-field "> --><label>Police Clearances Certificate(PCC)</label> <div class="file-field input-field"> <div class="btn1"> <span>Upload</span> <input type="file" multiple><i class="fa fa-cloud-upload"></i> </div> <div class="file-path-wrapper"> <input class="file-path validate" name="imigration[' + indexVal + '][pcc_clearance_cert]" type="text" placeholder = "Upload file"> <!--<div> </div> --> </div> </div> <div class="uploaded-file hide"  id="imigration[' + indexVal + '][pcc_clearance_cert]" data-type="file"></div> </div> <!--</div> -->  <div class="clearfix"></div>  </div>');
			break;			
		  default:
			break;
		}
		/*$('select').formSelect();
		$('.datepicker').datepicker({selectMonths: true,selectYears: 30});*/
        $('.formSelect').formSelect();
        $('.searchSelect').select2();
        $('.datepicker').datepicker({selectMonths: true,selectYears: 30});
		$("#mx_students_view .tab-pane.active .wrapper-count").val(parseInt(wrapperCount)+1);
	});	
	// remove tab functionality
	$('body').on('click', '.xclose', function(event) {
		$(this).parent().remove();
		var wrapperCount = $("#mx_students_view .tab-pane.active .wrapper-count").val();
		$("#mx_students_view .tab-pane.active .wrapper-count").val(parseInt(wrapperCount)-1);
	});	
	// back button functionality
	$('#mx_students_view').on('click', '.back_button', function() {
		
		var id = $("#mx_students_view .tab-pane.active").attr('id');
		var backtab = '#tab_' + (parseInt(id.replace('tab_','')) - 1);
		$("a[href$=" + backtab + "]").trigger('click');
	});	
	// session values set functionality on personal information
	var jsonDtaSessionFlag = $("#posted_values_flag").val();
	if(studentId == "0" && jsonDtaSessionFlag == "1") {
		var jsonDtaSession = JSON.parse($("#posted_values").val());
		$.each($("#personal-add-form input,#personal-add-form select"), function(key, value) {
			id = $(this).attr("id"); 
			$("#"+id).val(jsonDtaSession[id]);
			$('#personal-add-form select').formSelect();
		});
		if(jsonDtaSession['lived_othe_addr'] == "1") {
			$('#row_other_1').show(); $('#row_other_2').show(); $('#row_other_3').show(); $('#row_other_4').show();
		} else {
			$('#row_other_1').hide(); $('#row_other_2').hide(); $('#row_other_3').hide(); $('#row_other_4').hide();
		}
		if (jsonDtaSession['correspondence_address_flag'] == "NO") {
			$('#row_corres_1').show(); $('#row_corres_2').show(); $('#row_corres_3').show();
		} else {
			$('#row_corres_1').hide(); $('#row_corres_2').hide(); $('#row_corres_3').hide();
		}	
		if (jsonDtaSession['any_disability'] == "YES") {
			$('#row_disability').show();
		} else {
			$('#row_disability').hide();
		}	
		$("input[name='correspondence_address_flag'][value='" + jsonDtaSession['correspondence_address_flag'] + "']").prop('checked', true);
		$("input[name='gender'][value='" + jsonDtaSession['gender'] + "']").prop('checked', true);
		$("input[name='any_disability'][value='" + jsonDtaSession['any_disability'] + "']").prop('checked', true);
	}
});

function getStdentData(studentId,type) {
	type = type.replace('#','');
	var url = '{{ url(config('laraadmin.adminRoute')) }}/nurse_data/' + studentId + '/' + type;
	
	$.ajax({
		url: url,
		data: {},
		success: function(response){
			var details = JSON.parse(response);
			$.each(details, function(key, value) {
				element = $("#"+key);  elementName = $("input[name='" + key + "']");

				if(element.length > 0) {
					if(element.attr('data-type') == 'file') {
						if(value != null && value != '') {
							
							element.removeClass("hide"); // remove hide class of frame to show image 
							$(".appendedImages").remove();
							$("input[name='" + key + "']").val(value);
							var images = value.split(",");
							var extension = images[0].split("?")[0].substr( (images[0].split("?")[0].lastIndexOf('.') +1) );
							
							if($.inArray(extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
								element.css("background-image", "url(" + images[0] + ")");
								element.wrap('<a target="_blank" href="' + images[0].split("?")[0] + '"></a>');
							} else {
								switch (extension) {
									case "pdf":
										element.html('<i class="fa fa-file-pdf-o"></i>');
										break;
									default:
										element.html('<i class="fa fa-file-text-o"></i>');
										break;
								}
								element.wrap('<a target="_blank" href="' + images[0] + '"></a>');
							}
							if(images.length > 1) {
								for(i=1;i<=(images.length)-1;i++) {
									
									var extension = images[i].split("?")[0].substr( (images[i].split("?")[0].lastIndexOf('.') +1) );
									if($.inArray(extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
										element.parent().parent().append('<a target="_blank" class="appendedImages" href="' + images[i].split("?")[0] + '"><div style="background-image: url(' + images[i] + ')" class="uploaded-file" data-type="file"></div></a>');
									} else {
										switch (extension) {
											case "pdf":
												var appendedHtml = '<i class="fa fa-file-pdf-o"></i>';
												break;
											default:
												var appendedHtml = '<i class="fa fa-file-text-o"></i>';
												break;
										}
										element.parent().parent().append('<a target="_blank" class="appendedImages" href="' + images[i] + '"><div class="uploaded-file" data-type="file">' + appendedHtml +'</div></a>');
									}									
								}
							}				
						}
					} else {
						element.val(value);
						element.next().addClass('active');
						if(element.prop('type') == 'select-one') {
							//element.formSelect();

                            if(element.hasClass('searchSelect'))
                                element.select2();
                            else
                                element.formSelect();

						}             
						switch(key) { 
							case 'eng_lang_qualif':
								if (value == "Min_1year") {
								  $('#eng-lang-wrapper').removeClass('hide');
								  $('.addbtnenglishevidence').removeClass("displaynone");
								  $(".ieltsClass").addClass('hide');
								} else {
									if(value == "IELTS" || value == "OET") {
										$(".ieltsClass").removeClass('hide');
									} else {
										$(".ieltsClass").addClass('hide');
									}
									$('#eng-lang-wrapper').addClass('hide');
									$('.addbtnenglishevidence').addClass("displaynone");
								}								
								break;	
							case 'lived_othe_addr':
								if (value == "1") {
								  $('#row_other_1').show(); $('#row_other_2').show(); $('#row_other_3').show(); $('#row_other_4').show();
								} else {
								  $('#row_other_1').hide(); $('#row_other_2').hide(); $('#row_other_3').hide(); $('#row_other_4').hide();
								}
								break;
							case 'visa_applied':
								if (value == "YES") {
									$('.visa_details').removeClass('hide');
								} else {
									$('.visa_details').addClass('hide');
								}									
								break;
							case 'spons_status':
								if (value == "Received") {
								  $('.cos-wrapper').removeClass('hide');
								} else {
								  $('.cos-wrapper').addClass('hide');
								}
								break;
							case 'flight_tkt_status':
								if (value == "Received") {
								  $('.flight-ticket-wrapper').removeClass('hide');
								} else {
								  $('.flight-ticket-wrapper').addClass('hide');
								}
								break;
							case 'letter_invitation':
								if (value == "1") { 
								  $('.invitation-wrapper').removeClass('hide');
                                  
								} else {
								  $('.invitation-wrapper').addClass('hide');
                                  
								}								
								break;
							case 'current_live_status':
								if (value == "No") {
								  $('.dependent-addr-wrapper').removeClass('hide');
								} else {
								  $('.dependent-addr-wrapper').addClass('hide');
								}
								break;
							case 'relatiion_status':
								if (value == "Married" || value == "Civil-partnership") {
								  $('#married-details').removeClass('hide');
								} else {
								  $('#married-details').addClass('hide');
								}
								break;								
							case 'childrens':
								if (value == "YES") {
								  $('.childrens-wrapper').removeClass('hide');
								  $('.childrens-wrapper-btn').removeClass("displaynone");
								} else {
								  $('.childrens-wrapper').addClass('hide');
								  $('.childrens-wrapper-btn').addClass("displaynone");
								}								
								break;
							case 'max_invoice_status':
								if (value == "Submitted") {
								  $('.inv-status-wrapper').show();
								} else {
								  $('.inv-status-wrapper').hide();
								}
								break;	
							case 'employer_pay':
								if (value == "Received") {
								  $('.outs-status-wrapper').show();
								} else {
								  $('.outs-status-wrapper').hide();
								}								
								break;
							case 'sub_agents':
								if (value != "Not-applicable") {
								  $('.sub-agent-wrapper').show();
								} else {
								  $('.sub-agent-wrapper').hide();
								}								
								break;
							case 'sub_pay_status':
								if (value == "PAID") {
								  $('#paid-wrapper').show();
								} else {
								  $('#paid-wrapper').hide();
								}								
								break;
							case 'mode_interview':
								if (value == "Others") {
									$('#mode_interview_wrapper').removeClass('hide');
								} else {
									$('#mode_interview_wrapper').addClass('hide');
								}								
								break;
							case 'interview_status':
								if (value == "Passed") {
									$('#off-letter-det').removeClass('hide');
								} else {
									$('#off-letter-det').addClass('hide');
								}								
								break;								
							default:
								break;							
						}
					}
				} else if (elementName.attr('type') == 'radio') {
					$("input[name='" + key + "'][value='" + value + "']").prop('checked', true);
					switch(key) {
						case 'any_disability':
							if (value == "YES") {
								$('#row_disability').show();
							} else {
								$('#row_disability').hide();
							}							
							break;
						case 'correspondence_address_flag':
							if (value == "NO") {
							  $('#row_corres_1').show(); $('#row_corres_2').show(); $('#row_corres_3').show();
							} else {
							  $('#row_corres_1').hide(); $('#row_corres_2').hide(); $('#row_corres_3').hide();
							}							
							break;	
                        case 'letter_invitation':
                                if (value == "1") { 
                                  $('.invitation-wrapper').removeClass('hide');
                                  
                                } else {
                                  $('.invitation-wrapper').addClass('hide');
                                  
                                }                               
                                break;					
						default:
							break;							
					}					
				}  else if(key == 'json_data' && value != null) {

                    var selectelement = selectelementSearch = datepickerelement = datepickerelement1 = timepickerelement = '';
                    
					var jsonData = JSON.parse(value);
					$(".common-wrapper").remove();
					
                    var wc=0; // Wrapper count

					$.each(jsonData, function(key, value) {
						//wrapperCount = key+1; indexVal = key;

                        // Wrapper count
                        wrapperCount = parseInt(wc)+1;
                        indexVal = parseInt(wc);
                        wc = parseInt(wc)+1;
                        // End wrapper count
						
						switch(type) {
							case 'tab_1':
							
								var counter = wrapperCount;
							
								$("#other-addr-wrapp").append('<div class="stdhst common-wrapper wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div> <div class="row" id=""> <div class="input-field col s6"> <input type="text" id="" name="otheraddress[' + indexVal + '][other_address_1]" > <label for="other_address_1">Street Address</label> </div> <div class="input-field col s6"> <input type="text" id="" name="otheraddress[' + indexVal + '][other_address_2]" > <label for="other_address_2">Street Address Line2</label> </div> </div> <div class="row"> <div class="input-field col s6"> <input type="text" id="" name="otheraddress[' + indexVal + '][other_city]" > <label for="other_city">City</label> </div> <div class="input-field col s6"> <input type="text" id="" name="otheraddress[' + indexVal + '][other_state]" > <label for="other_state">State / Province</label> </div> </div> <div class="row"> <div class="input-field col s6"> <input type="text" id="" name="otheraddress[' + indexVal + '][other_zip_code]" > <label for="other_zip_code">Postal / Zip code</label> </div> <div class="input-field col s6"> <select class="searchSelect" name="otheraddress[' + indexVal + '][other_country]" id="" > <option value=""> Please Select </option> <option value="United States"> United States </option> <option value="Afghanistan"> Afghanistan </option> <option value="Albania"> Albania </option> <option value="Algeria"> Algeria </option> <option value="American Samoa"> American Samoa </option> <option value="Andorra"> Andorra </option> <option value="Angola"> Angola </option> <option value="Anguilla"> Anguilla </option> <option value="Antigua and Barbuda"> Antigua and Barbuda </option> <option value="Argentina"> Argentina </option> <option value="Armenia"> Armenia </option> <option value="Aruba"> Aruba </option> <option value="Australia"> Australia </option> <option value="Austria"> Austria </option> <option value="Azerbaijan"> Azerbaijan </option> <option value="The Bahamas"> The Bahamas </option> <option value="Bahrain"> Bahrain </option> <option value="Bangladesh"> Bangladesh </option> <option value="Barbados"> Barbados </option> <option value="Belarus"> Belarus </option> <option value="Belgium"> Belgium </option> <option value="Belize"> Belize </option> <option value="Benin"> Benin </option> <option value="Bermuda"> Bermuda </option> <option value="Bhutan"> Bhutan </option> <option value="Bolivia"> Bolivia </option> <option value="Bosnia and Herzegovina"> Bosnia and Herzegovina </option> <option value="Botswana"> Botswana </option> <option value="Brazil"> Brazil </option> <option value="Brunei"> Brunei </option> <option value="Bulgaria"> Bulgaria </option> <option value="Burkina Faso"> Burkina Faso </option> <option value="Burundi"> Burundi </option> <option value="Cambodia"> Cambodia </option> <option value="Cameroon"> Cameroon </option> <option value="Canada"> Canada </option> <option value="Cape Verde"> Cape Verde </option> <option value="Cayman Islands"> Cayman Islands </option> <option value="Central African Republic"> Central African Republic </option> <option value="Chad"> Chad </option> <option value="Chile"> Chile </option> <option value="China"> China </option> <option value="Christmas Island"> Christmas Island </option> <option value="Cocos (Keeling) Islands"> Cocos (Keeling) Islands </option> <option value="Colombia"> Colombia </option> <option value="Comoros"> Comoros </option> <option value="Congo"> Congo </option> <option value="Cook Islands"> Cook Islands </option> <option value="Costa Rica"> Costa Rica </option> <option value="Cote dIvoire"> Cote dIvoire </option> <option value="Croatia"> Croatia </option> <option value="Cuba"> Cuba </option> <option value="Curacao"> Curacao </option> <option value="Cyprus"> Cyprus </option> <option value="Czech Republic"> Czech Republic </option> <option value="Democratic Republic of the Congo"> Democratic Republic of the Congo </option> <option value="Denmark"> Denmark </option> <option value="Djibouti"> Djibouti </option> <option value="Dominica"> Dominica </option> <option value="Dominican Republic"> Dominican Republic </option> <option value="Ecuador"> Ecuador </option> <option value="Egypt"> Egypt </option> <option value="El Salvador"> El Salvador </option> <option value="Equatorial Guinea"> Equatorial Guinea </option> <option value="Eritrea"> Eritrea </option> <option value="Estonia"> Estonia </option> <option value="Ethiopia"> Ethiopia </option> <option value="Falkland Islands"> Falkland Islands </option> <option value="Faroe Islands"> Faroe Islands </option> <option value="Fiji"> Fiji </option> <option value="Finland"> Finland </option> <option value="France"> France </option> <option value="French Polynesia"> French Polynesia </option> <option value="Gabon"> Gabon </option> <option value="The Gambia"> The Gambia </option> <option value="Georgia"> Georgia </option> <option value="Germany"> Germany </option> <option value="Ghana"> Ghana </option> <option value="Gibraltar"> Gibraltar </option> <option value="Greece"> Greece </option> <option value="Greenland"> Greenland </option> <option value="Grenada"> Grenada </option> <option value="Guadeloupe"> Guadeloupe </option> <option value="Guam"> Guam </option> <option value="Guatemala"> Guatemala </option> <option value="Guernsey"> Guernsey </option> <option value="Guinea"> Guinea </option> <option value="Guinea-Bissau"> Guinea-Bissau </option> <option value="Guyana"> Guyana </option> <option value="Haiti"> Haiti </option> <option value="Honduras"> Honduras </option> <option value="Hong Kong"> Hong Kong </option> <option value="Hungary"> Hungary </option> <option value="Iceland"> Iceland </option> <option value="India"> India </option> <option value="Indonesia"> Indonesia </option> <option value="Iran"> Iran </option> <option value="Iraq"> Iraq </option> <option value="Ireland"> Ireland </option> <option value="Israel"> Israel </option> <option value="Italy"> Italy </option> <option value="Jamaica"> Jamaica </option> <option value="Japan"> Japan </option> <option value="Jersey"> Jersey </option> <option value="Jordan"> Jordan </option> <option value="Kazakhstan"> Kazakhstan </option> <option value="Kenya"> Kenya </option> <option value="Kiribati"> Kiribati </option> <option value="North Korea"> North Korea </option> <option value="South Korea"> South Korea </option> <option value="Kosovo"> Kosovo </option> <option value="Kuwait"> Kuwait </option> <option value="Kyrgyzstan"> Kyrgyzstan </option> <option value="Laos"> Laos </option> <option value="Latvia"> Latvia </option> <option value="Lebanon"> Lebanon </option> <option value="Lesotho"> Lesotho </option> <option value="Liberia"> Liberia </option> <option value="Libya"> Libya </option> <option value="Liechtenstein"> Liechtenstein </option> <option value="Lithuania"> Lithuania </option> <option value="Luxembourg"> Luxembourg </option> <option value="Macau"> Macau </option> <option value="Macedonia"> Macedonia </option> <option value="Madagascar"> Madagascar </option> <option value="Malawi"> Malawi </option> <option value="Malaysia"> Malaysia </option> <option value="Maldives"> Maldives </option> <option value="Mali"> Mali </option> <option value="Malta"> Malta </option> <option value="Marshall Islands"> Marshall Islands </option> <option value="Martinique"> Martinique </option> <option value="Mauritania"> Mauritania </option> <option value="Mauritius"> Mauritius </option> <option value="Mayotte"> Mayotte </option> <option value="Mexico"> Mexico </option> <option value="Micronesia"> Micronesia </option> <option value="Moldova"> Moldova </option> <option value="Monaco"> Monaco </option> <option value="Mongolia"> Mongolia </option> <option value="Montenegro"> Montenegro </option> <option value="Montserrat"> Montserrat </option> <option value="Morocco"> Morocco </option> <option value="Mozambique"> Mozambique </option> <option value="Myanmar"> Myanmar </option> <option value="Nagorno-Karabakh"> Nagorno-Karabakh </option> <option value="Namibia"> Namibia </option> <option value="Nauru"> Nauru </option> <option value="Nepal"> Nepal </option> <option value="Netherlands"> Netherlands </option> <option value="Netherlands Antilles"> Netherlands Antilles </option> <option value="New Caledonia"> New Caledonia </option> <option value="New Zealand"> New Zealand </option> <option value="Nicaragua"> Nicaragua </option> <option value="Niger"> Niger </option> <option value="Nigeria"> Nigeria </option> <option value="Niue"> Niue </option> <option value="Norfolk Island"> Norfolk Island </option> <option value="Turkish Republic of Northern Cyprus"> Turkish Republic of Northern Cyprus </option> <option value="Northern Mariana"> Northern Mariana </option> <option value="Norway"> Norway </option> <option value="Oman"> Oman </option> <option value="Pakistan"> Pakistan </option> <option value="Palau"> Palau </option> <option value="Palestine"> Palestine </option> <option value="Panama"> Panama </option> <option value="Papua New Guinea"> Papua New Guinea </option> <option value="Paraguay"> Paraguay </option> <option value="Peru"> Peru </option> <option value="Philippines"> Philippines </option> <option value="Pitcairn Islands"> Pitcairn Islands </option> <option value="Poland"> Poland </option> <option value="Portugal"> Portugal </option> <option value="Puerto Rico"> Puerto Rico </option> <option value="Qatar"> Qatar </option> <option value="Republic of the Congo"> Republic of the Congo </option> <option value="Romania"> Romania </option> <option value="Russia"> Russia </option> <option value="Rwanda"> Rwanda </option> <option value="Saint Barthelemy"> Saint Barthelemy </option> <option value="Saint Helena"> Saint Helena </option> <option value="Saint Kitts and Nevis"> Saint Kitts and Nevis </option> <option value="Saint Lucia"> Saint Lucia </option> <option value="Saint Martin"> Saint Martin </option> <option value="Saint Pierre and Miquelon"> Saint Pierre and Miquelon </option> <option value="Saint Vincent and the Grenadines"> Saint Vincent and the Grenadines </option> <option value="Samoa"> Samoa </option> <option value="San Marino"> San Marino </option> <option value="Sao Tome and Principe"> Sao Tome and Principe </option> <option value="Saudi Arabia"> Saudi Arabia </option> <option value="Senegal"> Senegal </option> <option value="Serbia"> Serbia </option> <option value="Seychelles"> Seychelles </option> <option value="Sierra Leone"> Sierra Leone </option> <option value="Singapore"> Singapore </option> <option value="Slovakia"> Slovakia </option> <option value="Slovenia"> Slovenia </option> <option value="Solomon Islands"> Solomon Islands </option> <option value="Somalia"> Somalia </option> <option value="Somaliland"> Somaliland </option> <option value="South Africa"> South Africa </option> <option value="South Ossetia"> South Ossetia </option> <option value="South Sudan"> South Sudan </option> <option value="Spain"> Spain </option> <option value="Sri Lanka"> Sri Lanka </option> <option value="Sudan"> Sudan </option> <option value="Suriname"> Suriname </option> <option value="Svalbard"> Svalbard </option> <option value="eSwatini"> eSwatini </option> <option value="Sweden"> Sweden </option> <option value="Switzerland"> Switzerland </option> <option value="Syria"> Syria </option> <option value="Taiwan"> Taiwan </option> <option value="Tajikistan"> Tajikistan </option> <option value="Tanzania"> Tanzania </option> <option value="Thailand"> Thailand </option> <option value="Timor-Leste"> Timor-Leste </option> <option value="Togo"> Togo </option> <option value="Tokelau"> Tokelau </option> <option value="Tonga"> Tonga </option> <option value="Transnistria Pridnestrovie"> Transnistria Pridnestrovie </option> <option value="Trinidad and Tobago"> Trinidad and Tobago </option> <option value="Tristan da Cunha"> Tristan da Cunha </option> <option value="Tunisia"> Tunisia </option> <option value="Turkey"> Turkey </option> <option value="Turkmenistan"> Turkmenistan </option> <option value="Turks and Caicos Islands"> Turks and Caicos Islands </option> <option value="Tuvalu"> Tuvalu </option> <option value="Uganda"> Uganda </option> <option value="Ukraine"> Ukraine </option> <option value="United Arab Emirates"> United Arab Emirates </option> <option value="United Kingdom"> United Kingdom </option> <option value="Uruguay"> Uruguay </option> <option value="Uzbekistan"> Uzbekistan </option> <option value="Vanuatu"> Vanuatu </option> <option value="Vatican City"> Vatican City </option> <option value="Venezuela"> Venezuela </option> <option value="Vietnam"> Vietnam </option> <option value="British Virgin Islands"> British Virgin Islands </option> <option value="Isle of Man"> Isle of Man </option> <option value="US Virgin Islands"> US Virgin Islands </option> <option value="Wallis and Futuna"> Wallis and Futuna </option> <option value="Western Sahara"> Western Sahara </option> <option value="Yemen"> Yemen </option> <option value="Zambia"> Zambia </option> <option value="Zimbabwe"> Zimbabwe </option> <option value="other"> Other </option> </select> <label for="other_country">Country</label> </div> </div> <div class="row" id=""> <div class=" col s8 ipfield"> <div class="input-field"> <input name="otheraddress[' + indexVal + '][start_living_addr]" id="" data-count="0" type="text" data-id="' + counter + '" class="start_living_addr' + counter + ' datepicker living_addr"> <label for="start_living_addr">When did you start living at this address ? </label> </div> </div> <div class=" col s8 ipfield"> <div class="input-field"> <input name="otheraddress[' + indexVal + '][stop_living_addr]" id="" data-count="0" data-id="' + counter + '" type="text" class="stop_living_addr' + counter + ' datepicker living_addr" > <label for="stop_living_addr">When did you stop living at this address ? </label> </div> </div> </div>  <div class="clearfix"></div> </div>');
								
								$( "input[name*='otheraddress[" + key + "][other_address_1]']").val(value.other_address_1).next().addClass('active');
								$( "input[name*='otheraddress[" + key + "][other_address_2]']").val(value.other_address_2).next().addClass('active');
								$( "input[name*='otheraddress[" + key + "][other_city]']").val(value.other_city).next().addClass('active');
								$( "input[name*='otheraddress[" + key + "][other_state]']").val(value.other_state).next().addClass('active');
								$( "input[name*='otheraddress[" + key + "][other_zip_code]']").val(value.other_zip_code).next().addClass('active');
								$( "select[name*='otheraddress[" + key + "][other_country]']").val(value.other_country).next().addClass('active');
								$( "input[name*='otheraddress[" + key + "][start_living_addr]']").val(value.start_living_addr).next().addClass('active');
								$( "input[name*='otheraddress[" + key + "][stop_living_addr]']").val(value.stop_living_addr).next().addClass('active');
								
								/*selectelement = $("#other-addr-wrapp select");
								datepickerelement = $("#other-addr-wrapp .datepicker");*/

                                selectelement = $("#other-addr-wrapp .formSelect");
                                selectelementSearch = $("#other-addr-wrapp .searchSelect");
                                datepickerelement = $("#other-addr-wrapp .datepicker");

								break;							
							case 'tab_3':
								$("#employement-wrapper").append('<div class="stdhst common-wrapper wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button> <div class="col-md-6"> <div class="mg-input-field "> <label for="employer_name">Job Title</label> <input class="form-control" id="" name="employement[' + indexVal + '][job_title]" type="text" required> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="work_nature">Organization Name</label> <input class="form-control" id="" name="employement[' + indexVal + '][org_name]" type="text" required> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="work_nature">Ward</label> <select name="employement[' + indexVal + '][ward]" id="" class="formSelect"> <option value="">Select</option> <option value="Ward 1">Ward 1</option> <option value="Ward 2">Ward 2</option> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="employement_start_date">Start Date</label> <input class="form-control datepicker" id="" name="employement[' + indexVal + '][employement_start_date]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="to_date">End Date</label> <input class="form-control datepicker" id="" name="employement[' + indexVal + '][employement_end_date]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="to_date">Remarks</label> <input class="form-control" id="" name="employement[' + indexVal + '][remarks]" type="text"> </div> </div>  <div class="col-md-6"> <div class="mg-input-field "> <label for="working_status">Still working</label> <select name="employement[' + indexVal + '][working_status]" id="" class="formSelect"> <option value="">Select</option> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div>  <div class="clearfix"></div> </div>');

								//$( "input[name*='employement[" + key + "][job_title]']").val(value.job_title);
                                $( "input[name*='employement[" + indexVal + "][job_title]']").val(value.job_title);
								$( "input[name*='employement[" + indexVal + "][org_name]']").val(value.org_name);
								$( "select[name*='employement[" + indexVal + "][ward]']").val(value.ward);
								$( "input[name*='employement[" + indexVal + "][employement_start_date]']").val(value.employement_start_date);
								$( "input[name*='employement[" + indexVal + "][employement_end_date]']").val(value.employement_end_date);
								$( "input[name*='employement[" + indexVal + "][remarks]']").val(value.remarks);
								$( "select[name*='employement[" + indexVal + "][working_status]']").val(value.working_status);
								
								/*selectelement = $("#employement-wrapper select");
								datepickerelement = $("#employement-wrapper .datepicker");*/

                                selectelement = $("#employement-wrapper .formSelect");
                                selectelementSearch = $("#employement-wrapper .searchSelect");
                                datepickerelement = $("#employement-wrapper .datepicker");

								break;
							case 'tab_5':
								$("#eng-lang-wrapper").append('<div class="stdhst common-wrapper wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button> <div class="col-md-6"> <div class="mg-input-field "> <label for="ielts_country">Country</label> <select class="searchSelect" id="" name="english[' + indexVal + '][ielts_country]" required> <option value="Antigua and Barbuda">Antigua and Barbuda</option> <option value="Anguilla">Anguilla</option> <option value="Australia">Australia</option> <option value="Bahamas">Bahamas</option> <option value="Barbados">Barbados</option> <option value="Belize">Belize</option> <option value="Bermuda">Bermuda</option> <option value="British Indian Ocean Territory">British Indian Ocean Territory</option> <option value="Canada">Canada</option> <option value="Cayman Islands">Cayman Islands</option> <option value="Dominica">Dominica</option> <option value="Falkland Islands">Falkland Islands</option> <option value="Gibraltar">Gibraltar</option> <option value="Grenada">Grenada</option> <option value="Guernsey">Guernsey</option> <option value="Guernsey">Guernsey</option> <option value="Ireland">Ireland</option> <option value="Jamaica">Jamaica</option> <option value="Malta">Malta</option> <option value="New Zealand">New Zealand</option> <option value="Saint Helena">Saint Helena</option> <option value="Ascension and Tristan da Cunha">Ascension and Tristan da Cunha</option> <option value="St Kitts and Nevis">St Kitts and Nevis</option> <option value="St Lucia">St Lucia</option> <option value="St Vincent and the Grenadines">St Vincent and the Grenadines</option> <option value="Trinidad and Tobago">Trinidad and Tobago</option> <option value="United Kingdom">United Kingdom</option> <option value="United States">United States</option> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="work_nature">Work start date</label> <input class="form-control datepicker" id="" name="english[' + indexVal + '][work_start_date]" type="text" required> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="employement_start_date">End date</label> <input class="form-control datepicker" id="" name="english[' + indexVal + '][work_end_date]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="employer_addresss">Employer address</label> <input class="form-control" id="" name="english[' + indexVal + '][employer_addresss]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="to_date">Email of employer</label> <input class="form-control" id="" name="english[' + indexVal + '][employer_email]" type="email"> </div> </div>    <div class="col-md-6"> <div class="mg-input-field "> <label for="to_date">Reference letter</label> <input class="form-control" id="" name="english[' + indexVal + '][reference_letter]" type="text"> </div> </div>    <div class="col-md-6"> <div class="mg-input-field "> <label>NCC File Upload</label> <div class="file-field input-field"> <div class="btn7"> <span>Upload</span> <input type="file"><i class="fa fa-cloud-upload"></i> </div> <div class="file-path-wrapper"> <input class="file-path validate" name="english[' + indexVal + '][file_upload_nursing]" type="text"> <div> </div> </div> </div> <div class="uploaded-file" id="english[' + indexVal + '][file_upload_nursing]" data-type="file"></div> </div> </div>  <div class="clearfix"></div> </div>');
			
								//$( "select[name*='english[" + key + "][ielts_country]']").val(value.ielts_country);
                                $( "select[name*='english[" + indexVal + "][ielts_country]']").val(value.ielts_country);
								$( "input[name*='english[" + indexVal + "][work_start_date]']").val(value.work_start_date);
								$( "input[name*='english[" + indexVal + "][work_end_date]']").val(value.work_end_date);
								$( "input[name*='english[" + indexVal + "][employer_addresss]']").val(value.employer_addresss);
                                $( "input[name*='english[" + indexVal + "][employer_email]']").val(value.employer_email);
								$( "input[name*='english[" + indexVal + "][reference_letter]']").val(value.reference_letter);

								/*selectelement = $("#eng-lang-wrapper select");
								datepickerelement = $("#eng-lang-wrapper .datepicker");*/

                                selectelement = $("#eng-lang-wrapper .formSelect");
                                selectelementSearch = $("#eng-lang-wrapper .searchSelect");
                                datepickerelement = $("#eng-lang-wrapper .datepicker");
								
								valueExperience = value.file_upload_nursing;
								if(file_upload_nursing != null && file_upload_nursing != '') { // display the uploaded files
									element = $('#english-' + indexVal + '-file_upload_nursing');
									key = 'employement[' + indexVal + '][file_upload_nursing]';
									//displayUploadedFiles(element,key,valueExperience);
								}						
								break;	
							case 'tab_8':
								$("#childrens-wrapper").append('<div class="stdhst common-wrapper wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button>   <div class="col-md-6"> <div class="mg-input-field "> <label for="name">Name (First Name + Family Name)</label> <input class="form-control" id="" name="children[' + indexVal + '][name]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="birthdate">Date of birth</label> <input class="form-control datepicker" id="" name="children[' + indexVal + '][birthdate]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="from_date">Relationship to you?</label> <select class="formSelect" name="children[' + indexVal + '][relation]" id=""> <option value="Son">Son</option> <option value="Daughter">Daughter</option> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="current_live">Does this person currently live with you?</label> <select class="formSelect" name="children[' + indexVal + '][current_live]" id=""> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div><div class="col-md-6"> <div class="mg-input-field "> <label for="travel_to_uk">Is this person travelling with you to the UK?</label> <select name="children[' + indexVal + '][travel_to_uk]" id="" class="formSelect"> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="clearfix"></div> </div>');
								
								//$( "input[name*='children[" + key + "][name]']").val(value.name);
                                $( "input[name*='children[" + indexVal + "][name]']").val(value.name);
								$( "input[name*='children[" + indexVal + "][birthdate]']").val(value.birthdate);
								$( "select[name*='children[" + indexVal + "][relation]']").val(value.relation);
								$( "select[name*='children[" + indexVal + "][current_live]']").val(value.current_live);
								$( "select[name*='children[" + indexVal + "][travel_to_uk]']").val(value.travel_to_uk);
								
								/*selectelement = $("#childrens-wrapper select");
								datepickerelement = $("#childrens-wrapper .datepicker");*/

                                selectelement = $("#childrens-wrapper .formSelect");
                                selectelementSearch = $("#childrens-wrapper .searchSelect");
                                datepickerelement = $("#childrens-wrapper .datepicker");

								break;	
							case 'tab_9':
								$("#imigration-wrapper").append('<div class="stdhst common-wrapper wrapper-' + wrapperCount + '"><div class="index">' + (parseInt(wrapperCount)+1) + '</div><button class="xclose"><span class="glyphicon glyphicon-trash"></span></button>  <div class="col-md-6"> <div class="mg-input-field "> <label for="name">Country</label> <select class="searchSelect" name="imigration[' + indexVal + '][pc_country]" id="" required> <option value=""> Please Select </option>     <?php foreach($countries as $countriesData) { echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>'; } ?>  </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="visit_reason">Reason of visit</label> <select class="formSelect" id="" name="imigration[' + indexVal + '][visit_reason]"> <option value="">Select</option> <option value="Work"> Work </option> <option value="Tourism"> Tourism </option> <option value="Others"> Others </option> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="pc_entry_date">Date you entered</label> <input class="form-control datepicker" id="" name="imigration[' + indexVal + '][pc_entry_date]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="pc_exit_date">Date you left</label> <input class="form-control datepicker" id="" name="imigration[' + indexVal + '][pc_exit_date]" type="text"> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label for="current_live">Can you provide criminal record certificate for this country?</label> <select class="formSelect" name="imigration[' + indexVal + '][crim_record_cert]" id=""> <option value="Yes">Yes</option> <option value="No">No</option> </select> </div> </div> <div class="col-md-6"> <div class="mg-input-field "> <label>Police Clearance Certificate(PCC)</label> <div class="file-field input-field"> <div class="btn1"> <span>Upload</span> <input type="file"><i class="fa fa-cloud-upload"></i> </div> <div class="file-path-wrapper"> <input class="file-path validate" name="imigration[' + indexVal + '][pcc_clearance_cert]" type="text"> <div> </div> </div> </div> <div class="uploaded-file" id="imigration[' + indexVal + '][pcc_clearance_cert]" data-type="file"></div> </div> </div>  <div class="clearfix"></div>  </div>');
			
								//$( "select[name*='imigration[" + key + "][pc_country]']").val(value.pc_country);
                                $( "select[name*='imigration[" + indexVal + "][pc_country]']").val(value.pc_country);
								$( "select[name*='imigration[" + indexVal + "][visit_reason]']").val(value.visit_reason);
								$( "input[name*='imigration[" + indexVal + "][pc_entry_date]']").val(value.pc_entry_date);
								$( "input[name*='imigration[" + indexVal + "][pc_exit_date]']").val(value.pc_exit_date);
								$( "select[name*='imigration[" + indexVal + "][crim_record_cert]']").val(value.crim_record_cert);

								/*selectelement = $("#imigration-wrapper select");
								datepickerelement = $("#imigration-wrapper .datepicker");*/

                                selectelement = $("#imigration-wrapper .formSelect");
                                selectelementSearch = $("#imigration-wrapper .searchSelect");
                                datepickerelement = $("#imigration-wrapper .datepicker");
								
								valueClearance = value.pcc_clearance_cert;
								if(valueClearance != null && valueClearance != '') { // display the uploaded files
									element = $('#imigration-' + indexVal + '-pcc_clearance_cert');
									key = 'imigration[' + indexVal + '][pcc_clearance_cert]';
									//displayUploadedFiles(element,key,valueExperience);
								}						
								break;								
							default:
								break;
						}
					});
					/*selectelement.formSelect();
					datepickerelement.datepicker({yearRange: [1960,2022],changeMonth: true,changeYear: true});*/

                    if(selectelement != '')
                        selectelement.formSelect();
                    if(selectelementSearch != '')
                        selectelementSearch.select2();
                    if(datepickerelement != '')
                        datepickerelement.datepicker({yearRange: [1960,2022],changeMonth: true,changeYear: true});
                    if(datepickerelement1 != '')
                        datepickerelement1.datepicker({yearRange: [2021,2025],minDate: new Date(),selectMonths: true});
                    if(timepickerelement != '')
                        timepickerelement.timepicker();

					$("#mx_students_view .tab-pane.active .wrapper-count").val(parseInt(wrapperCount)+1);
				} 
			});
		}
	});			
}//display the uploaded files
function displayUploadedFiles(element,key,value) {
    
    element.removeClass("hide"); // remove hide class of frame to show image 
    //$(".appendedImages").remove();
    $("input[name='" + key + "']").val(value);
    var images = value.split(",");
    var extension = images[0].split("?")[0].substr( (images[0].split("?")[0].lastIndexOf('.') +1) );
    if($.inArray(extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
        element.css("background-image", "url(" + images[0] + ")");
        element.wrap('<a target="_blank" href="' + images[0].split("?")[0] + '"></a>');
    } else {
        switch (extension) {
            case "pdf":
                element.html('<i class="fa fa-file-pdf-o"></i>');
                break;
            default:
                element.html('<i class="fa fa-file-text-o"></i>');
                break;
        }
        element.wrap('<a target="_blank" href="' + images[0] + '"></a>');
    }
    if(images.length > 1) {
        for(i=1;i<=(images.length)-1;i++) {
            
            var extension = images[i].split("?")[0].substr( (images[i].split("?")[0].lastIndexOf('.') +1) );
            if($.inArray(extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
                element.parent().parent().append('<a target="_blank" class="appendedImages" href="' + images[i].split("?")[0] + '"><div style="background-image: url(' + images[i] + ')" class="uploaded-file" data-type="file"></div></a>');
            } else {
                switch (extension) {
                    case "pdf":
                        var appendedHtml = '<i class="fa fa-file-pdf-o"></i>';
                        break;
                    default:
                        var appendedHtml = '<i class="fa fa-file-text-o"></i>';
                        break;
                }
                element.parent().parent().append('<a target="_blank" class="appendedImages" href="' + images[i] + '"><div class="uploaded-file" data-type="file">' + appendedHtml +'</div></a>');
            }                                   
        }
    }   
}






//display the uploaded files
function displayUploadedFiles(element,key,value) {
    
    element.removeClass("hide"); // remove hide class of frame to show image 
    //$(".appendedImages").remove();
    $("input[name='" + key + "']").val(value);
    var images = value.split(",");
    var extension = images[0].split("?")[0].substr( (images[0].split("?")[0].lastIndexOf('.') +1) );
    if($.inArray(extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
        element.css("background-image", "url(" + images[0] + ")");
        element.wrap('<a target="_blank" href="' + images[0].split("?")[0] + '"></a>');
    } else {
        switch (extension) {
            case "pdf":
                element.html('<i class="fa fa-file-pdf-o"></i>');
                break;
            default:
                element.html('<i class="fa fa-file-text-o"></i>');
                break;
        }
        element.wrap('<a target="_blank" href="' + images[0] + '"></a>');
    }
    if(images.length > 1) {
        for(i=1;i<=(images.length)-1;i++) {
            
            var extension = images[i].split("?")[0].substr( (images[i].split("?")[0].lastIndexOf('.') +1) );
            if($.inArray(extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
                element.parent().parent().append('<a target="_blank" class="appendedImages" href="' + images[i].split("?")[0] + '"><div style="background-image: url(' + images[i] + ')" class="uploaded-file" data-type="file"></div></a>');
            } else {
                switch (extension) {
                    case "pdf":
                        var appendedHtml = '<i class="fa fa-file-pdf-o"></i>';
                        break;
                    default:
                        var appendedHtml = '<i class="fa fa-file-text-o"></i>';
                        break;
                }
                element.parent().parent().append('<a target="_blank" class="appendedImages" href="' + images[i] + '"><div class="uploaded-file" data-type="file">' + appendedHtml +'</div></a>');
            }                                   
        }
    }   
}



function loadUploadedFiles() {
    // load folder files
    $.ajax({
        dataType: 'json',
        url: "{{ url(config('laraadmin.adminRoute') . '/uploaded_files') }}",
        success: function ( json ) {
            cntFiles = json.uploads;
            $("ul.files_container").empty();
            if(cntFiles.length) {
                //for (var index = 0; index < cntFiles.length; index++) {
                    var element = cntFiles[(cntFiles.length)-1];
                    var li = formatFile(element);
					
					if($.inArray(element.extension, ["jpg", "jpeg", "png", "gif", "bmp"]) == -1) {
						switch (element.extension) {
							case "pdf":
								image = '<i class="fa fa-file-pdf-o"></i>';
								break;
							default:
								image = '<i class="fa fa-file-text-o"></i>';
								break;
						}						
					} else{
						image = '<img src='+li+'>';
					}
					if($(elem).parent().prev().find('input').attr("multiple") == undefined) {
						$(elem).closest('div').find('img').remove();
					}
                    $(elem).closest('div').append(image);
					var elementNameObj = $('input[name="'+$(elem).attr('name')+'"]');
					currentValue = elementNameObj.val();
					li = (currentValue != '') ? currentValue + ',' + li : li;
					
					//console.log()
					
                    elementNameObj.val(li);
                //}
            } else {
                $("ul.files_container").html("<div class='text-center text-danger' style='margin-top:40px;'>No Files</div>");
            }
			$('#AddModal').modal('close');
        }
    });
}
function formatFile(upload) {
    var image = '';
    var url='';
    if($.inArray(upload.extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
		url=bsurl+'/files/'+upload.hash+'/'+upload.name+'?s=130';
        image = '<img src='+url+'>';
    } else {
		url=bsurl+'/files/'+upload.hash+'/'+upload.name;
		/*
        switch (upload.extension) {
            case "pdf":
                image = '<i class="fa fa-file-pdf-o"></i>';
                break;
            default:
                image = '<i class="fa fa-file-text-o"></i>';
                break;
        }
		*/
    }
	
    return url;
}

//$('select').formSelect();

$('[name="correspondence_address_flag"]').change(function () {
    if ($(this).val() === "NO") {
      $('#row_corres_1').show();
      $('#row_corres_2').show();
      $('#row_corres_3').show();
	} else {
      $('#row_corres_1').hide();
      $('#row_corres_2').hide();
      $('#row_corres_3').hide();
	}
});

$('[name="lived_othe_addr"]').change(function () {
    if ($(this).val() === "1") {
      $('#row_other_1').show();
      $('#row_other_2').show();
      $('#row_other_3').show();
      $('#row_other_4').show();
	} else {
      $('#row_other_1').hide();
      $('#row_other_2').hide();
      $('#row_other_3').hide();
      $('#row_other_4').hide();
	}
});
</script>
@endpush