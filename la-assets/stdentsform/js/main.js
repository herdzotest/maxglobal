
// .material-tooltip

// https://materializecss.com/pickers.html

$(document).ready(function(){
    //$('select').formSelect();

    $('.formSelect').formSelect();
    $('.searchSelect').select2();

    $('.datepicker').datepicker({
		yearRange: [1960,2022],
		changeMonth: true,
		changeYear: true,
    });

    $('.dateofbirth').datepicker({
        yearRange: [1960,2021],
        maxDate: new Date(),
        selectMonths: true
    });

    $('.testdatepicker').datepicker({
        yearRange: [1980,2021],
        maxDate: new Date(),
        selectMonths: true
    });

    $('.futuredatepicker').datepicker({
        yearRange: [2021,2025],
        minDate: new Date(),
        selectMonths: true
    });

	$('.timepicker').timepicker();

    $('.entrydate').datepicker( {
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            format: "mm yyyy"   
    });

});

$('[name="visa_aplctn_stat"]').change(function () {
	if ($(this).val() == "SUBMITTED") {
	  $('#visa-application-details').removeClass('hide');
	} else {
	  $('#visa-application-details').addClass('hide');
	}
});  
  
  $('[name="uniselect"]').change(function () {
    if ($(this).val() === "5") {
      $('#uni-intsd').show();
  } else {
      $('#uni-intsd').hide();
  }
  });
  
  $('[name="courseselect"]').change(function () {
    if ($(this).val() === "5") {
      $('#course-intsd').show();
  } else {
      $('#course-intsd').hide();
  }
  });

  $('[name="invoice"]').change(function () {
    if ($(this).val() === "1") {
      $('#invoiced').show();
  } else {
      $('#invoiced').hide();
  }
  });

  $('[name="enrollment_stat"]').change(function () {
    if ($(this).val() == "ENROLLED") {
      $('#enrolled').show();
  } else {
      $('#enrolled').hide();
  }
  });

  $('[name="visastatus"]').change(function () {
    if ($(this).val() === "1") {
      $('#visa-rcd').show();
  } else {
      $('#visa-rcd').hide();
  }
  });

  $('[name="tuitionfeecheck"]').change(function () {
  if ($(this).val() === "1") {
    $('#tf-paid').show();
} else {
    $('#tf-paid').hide();
}
});

$('[name="cas_stat"]').change(function () {
    if ($(this).val() == "RECEIVED") {
      $('#cas-rcd').show();
  } else {
      $('#cas-rcd').hide();
  }
  });

$('[name="offer_stat"]').change(function () {

    if ($(this).val() === "RECEIVED") {
  
      $('#ofl-upload').show();
  } else {
      $('#ofl-upload').hide();
  }
  });


  $('[name="degreedone"]').change(function () {
    if ($('[name="degreedone"]:checked').val() === "1") {
        $('#degree-details').show();
    } else {
        $('#degree-details').hide();
    }
});

  $('[name="ukdeg"]').change(function () {
    if ($('[name="ukdeg"]:checked').val() === "1") {
        $('#ukdegdone').show();
    } else {
        $('#ukdegdone').hide();
    }
});

$('[name="exp"]').change(function () {
    if ($('[name="exp"]:checked').val() === "1") {
        $('#havexp').show();
    } else {
        $('#havexp').hide();
    }
});

$('[name="studyhistory"]').change(function () {
    if ($('[name="studyhistory"]:checked').val() === "1") {
        $('#havsh').show();
    } else {
        $('#havsh').hide();
    }
});

$('[name="is_certified"]').change(function () {
    if ($('[name="is_certified"]:checked').val() === "1") {
        $('#ielts-details').show();
    } else {
        $('#ielts-details').hide();
    }
});

// English Lang Check
$('[name="eng_lang_check"]').change(function () {
    if ($('[name="eng_lang_check"]:checked').val() == "1") {
        $('#ielts-wrapper').removeClass('hide');
        $('#ielts-test-wrapper').addClass('hide');
    } else {
        $('#ielts-wrapper').addClass('hide');
        $('#ielts-test-wrapper').removeClass('hide');
    }
});
// End English Lang Check

$('[name="studiedabroad"]').change(function () {
    if ($('[name="studiedabroad"]:checked').val() === "1") {
        $('#studiedabroad').show();
    } else {
        $('#studiedabroad').hide();
    }
});

$('[name="visarefusal"]').change(function () {
    if ($('[name="visarefusal"]:checked').val() === "1") {
        $('#visarefusal').show();
    } else {
        $('#visarefusal').hide();
    }
});

$('[name="toeic"]').change(function () {
    if ($('[name="toeic"]:checked').val() === "1") {
        $('#toeic').show();
    } else {
        $('#toeic').hide();
    }
});

$('[name="qualifications"]').change(function () {
    if ($(this).val() == "OTHERS") {
        $('#other-qualifications-wrapper').removeClass('hide');
    } else {
        $('#other-qualifications-wrapper').addClass('hide');
    }
});
$('[name="lived_other_address"]').change(function () {
    if ($(this).val() == "0") {
        $('#lived_other_address_wrapper').removeClass('hide');
    } else {
        $('#lived_other_address_wrapper').addClass('hide');
    }
});
$('[name="relatiion_status"]').change(function () {
    if ($(this).val() == "Married" || $(this).val() == "Civil-partnership") {
        $('#spouse-details-wrapper').removeClass('hide');
    } else {
        $('#spouse-details-wrapper').addClass('hide');
    }
});
$('[name="current_live_status"]').change(function () {
    if ($(this).val() == "No") {
        $('#spouse-adrress-details-wrapper').removeClass('hide');
    } else {
        $('#spouse-adrress-details-wrapper').addClass('hide');
    }
});
$('[name="childrens"]').change(function () {
    if ($(this).val() == "Yes") {
        $('#childrens-wrapper').removeClass('hide');
    } else {
        $('#childrens-wrapper').addClass('hide');
    }
});
$('[name="other_countries"]').change(function () {
    if ($(this).val() == "0") {
        $('#other-countrier-details-wrapper').removeClass('hide');
    } else {
        $('#other-countrier-details-wrapper').addClass('hide');
    }
});
$('[name="studied_uk"]').change(function () {
    if ($(this).val() == "0") {
        $('#prev-studied-country-wrapper').removeClass('hide');
    } else {
        $('#prev-studied-country-wrapper').addClass('hide');
    }
});




$('[name="lead_from"]').change(function () {
    if ($(this).val() == "Agents") {
        $('#lead_agents').removeClass('hide');
    } else {
        $('#lead_agents').addClass('hide');
    }
});
$('[name="pte"]').change(function () {
    if ($(this).val() == "1") {
        $('#pte-wrapper').removeClass('hide');
    } else {
        $('#pte-wrapper').addClass('hide');
    }
});
$('[name="duolingo"]').change(function () {
    if ($(this).val() == "1") {
        $('#duolingo-wrapper').removeClass('hide');
    } else {
        $('#duolingo-wrapper').addClass('hide');
    }
});
$('[name="toefl"]').change(function () {
    if ($(this).val() == "1") {
        $('#TOEFL-wrapper').removeClass('hide');
    } else {
        $('#TOEFL-wrapper').addClass('hide');
    }
});
$('[name="agent"]').change(function () {
    $("#agent_id").val($(this).find(':selected').attr('data-id'));
});

$('[name="dob"]').change(function () {
    var dob = new Date($(this).val());
    var birth_year = dob.getFullYear(); 
    var birth_month = dob.getMonth()+1; 
    var birth_day = dob.getDate(); 
    var today_date = new Date();
    var today_year = today_date.getFullYear();
    var today_month = today_date.getMonth()+1; 
    var today_day = today_date.getDate();
    var age = today_year - birth_year;
    if ( today_month < (birth_month - 1))
    {
        age--;
    }
    if (((birth_month - 1) == today_month) && (today_day < birth_day))
    {
        age--;
    }
    $('.stdage').addClass('active');
    $('[name="age"]').val(age);
});
