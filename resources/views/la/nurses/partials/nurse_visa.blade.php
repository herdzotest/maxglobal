<div id="tab_9" class="tab-pane " >
        
<div class="content-head">
                                                <h4>Visa & Immigration </h4>
                                                <!-- <div class="id">Nurse ID : <b>#12675</b></div> -->
                                </div>
                                {!! Form::open(['action' => 'LA\NursesController@store', 'id' => '']) !!}
               
                                <div class="pane">
								
                                        
								<div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
														<select name="in_uk" id="in_uk" class="formSelect">
															<option value="">Select</option>
                                                            <option value="YES">YES</option>
                                                            <option value="NO">NO</option>
														</select>
														<label for="">Have you been to the UK in the past 10 years ?</label>
                                                </div>

                                        </div>
                                        <div class=" col s6 ">
                                        <div class="input-field">
														<select name="issued_uk_visa" id="issued_uk_visa" class="formSelect">
															<option value="">Select</option>
                                                            <option value="YES">YES</option>
                                                            <option value="NO">NO</option>
														</select>
														<label for="">Have you been issued with a UK visa in the past 10 years ?</label>
											</div>


                                        </div>
                                </div>
								<div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
														<select name="other_countries" id="other_countries" class="formSelect">
															<option value="">Select</option>
                                                            <option value="YES">YES</option>
                                                            <option value="NO">NO</option>
														</select>
														<label for="">Have you been to the any other countries in the past 10 years ?</label>
                                                </div>

                                        </div>
                                </div>
								
								<div id="imigration-wrapper">					
									<h4>Provide a police clearance from each overseas country you have lived in for 12 months or more (in total, not necessarily consecutive) in the last ten years</h4>
									<div class="clearfix"></div>
									<div class="stdhst">
											<div class="index">1</div>
											<div class="col-md-6">
												<div class="mg-input-field ">
													<label for="name">Country</label>
													<select name="pc_country" id="pc_country" class="searchSelect">
													<option value=""> Please Select </option>
													<?php
													foreach($countries as $countriesData) {
													echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
													}
													?>	
													</select>
												</div>
											</div>
											
											<div class="col-md-6">
												<div class="mg-input-field ">
													<label for="visit_reason">Reason of visit</label>
													<select id="visit_reason" name="visit_reason" class="formSelect">
													  <option value="">Select</option>
													  <option value="Work"> Work </option>
													  <option value="Tourism"> Tourism </option>
													  <option value="Others"> Others </option>
													</select>		
												</div>
											</div>
											<div class="col-md-6">
													<div class="mg-input-field ">
															<label for="pc_entry_date">Date you entered</label>
															<input class="form-control datepicker" id="pc_entry_date" name="pc_entry_date" type="text">
													</div>
											</div>
											<div class="col-md-6">
													<div class="mg-input-field ">
															<label for="pc_exit_date">Date you left</label>
															<input class="form-control datepicker" id="pc_exit_date" name="pc_exit_date" type="text">
													</div>
											</div>
											
											<div class="col-md-6">
													<div class="mg-input-field ">
															<label for="current_live">Can you provide criminal record certificate for this country?</label>
															<select name="crim_record_cert" id="crim_record_cert">
																<option value="Yes">Yes</option>
																<option value="No">No</option>
															</select>	
													</div>
											</div>
									<div class="col-md-6">
										<div class="mg-input-field ">
											<label>Police Clearance Certificate(PCC)</label>
											<div class="file-field input-field">
												<div class="btn7">
												<span>Upload</span>
												<input type="file"><i class="fa fa-cloud-upload"></i> 
												</div>
											<div class="file-path-wrapper">
												<input class="file-path validate" name="pcc_clearance_cert" type="text">
											<div>
											</div>					
											</div>
											</div>
										<div class="uploaded-file" id="pcc_clearance_cert" data-type="file"></div>
										</div>
									</div>						
									<div class="clearfix"></div>
									</div>
								</div>	
								<input type="hidden" value="imigration" name="formtype" class="formtype">
								<div class="btn addshbtn"> <span class="glyphicon glyphicon-plus"></span></div>
								<input type="hidden" value="1" class="wrapper-count">	
                                									
								<div class="clearfix"></div>
								
								<div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
														<select name="visa_category" id="visa_category" class="formSelect">
															<option value="">Select</option>
                                                            <option value="SPONSORSHIP-Tier">SPONSORSHIP[Tier 2]</option>
                                                            <option value="PERMANENT RESIDENCE">PERMANENT RESIDENCE</option>
															<option value="CITIZENSHIP">CITIZENSHIP</option>
                                                            <option value="STUDENT VISA">STUDENT VISA</option>
															<option value="WAITING FOR RCOS">WAITING FOR RCOS</option>
															<option value="OVERSEAS-PRE REGISTRATION">OVERSEAS-PRE REGISTRATION</option>
															<option value="Not Applicable">Not Applicable</option>
														</select>
														<label for="">Visa Category</label>
                                                </div>

                                        </div>
                                </div>


								<div class="row">
								<div class="col s12">
                                        <label>Application Form (NHS / Nursing Home) </label>
										<div class="file-field input-field">
																<div class="btn7">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
														<div class="file-path-wrapper">
																<input class="file-path validate" name="appl_form" type="text">

																<div>
																</div>
												
														</div>
										</div>

										<div class="uploaded-file" id="appl_form" data-type="file"></div>

										</div>	
                                </div>				

								<div class="row">
								<div class="col s12">
                                        <label>References File Upload </label>
										<div class="file-field input-field">
																<div class="btn7">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
														<div class="file-path-wrapper">
																<input class="file-path validate" name="refer_file" type="text">

																<div>
																</div>
												
														</div>
										</div>

										<div class="uploaded-file" id="refer_file" data-type="file"></div>

										</div>	
                                </div>			

								<div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
														<select name="pers_status" id="pers_status" class="formSelect">
															<option value="">Select</option>
															<option value="Applicable">Applicable</option>
															<option value="Not-Applicable">Not Applicable</option>
														</select>
														<label for="">Personal Statement Status</label>
                                                </div>

                                        </div>
                                </div>

							<div id="personal-stat-wrapper" style="display:none;">
								<div class="row">
									<div class=" col s12 ">
												<div class="input-field">

												<textarea name="pers_stat_text" class="materialize-textarea" id="pers_stat_text"></textarea>
												<label for="pers_stat_text">Personal Statement</label>

												</div>
									</div>
								</div>
								<div class="row">
									<div class="col s12">
                                        <label>Personal Statement</label>
										<div class="file-field input-field">
																<div class="btn7">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
														<div class="file-path-wrapper">
																<input class="file-path validate" name="pers_stat_file" type="text">

																<div>
																</div>
												
														</div>
										</div>

										<div class="uploaded-file" id="pers_stat_file" data-type="file"></div>

										</div>	
                                </div>	
							
							</div>

								<div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
														<select name="pcc_status" id="pcc_status" class="formSelect">
															<option value="">Select</option>
															<option value="Received">Received</option>
															<option value="Not Received">Not Received</option>
															<option value="Not Applicable">Not Applicable</option>
														</select>
														<label for="">PCC Status </label>
                                                </div>

                                        </div>
                                        <div class=" col s6 ">
                                        <div class="input-field">

											<div class="input-field">
                                                <input name="pcc_recieved_date"  id="pcc_recieved_date" type="text" class="datepicker">
												<label for="pcc_recieved_date">PCC Received Date</label>
                                            </div> 

										</div>
                                        </div>
                                </div>

								<div class="row">
								<div class="col s6">
                                        <label>PCC / DBS </label>
										<div class="file-field input-field">
																<div class="btn7">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
														<div class="file-path-wrapper">
																<input class="file-path validate" name="pcc_file_upload" type="text">

																<div>
																</div>
												
														</div>
										</div>

										<div class="uploaded-file" id="pcc_file_upload" data-type="file"></div>

										</div>	
										<div class="col s6">

											<div class="input-field">
                                                <input name="tb_test_date"  id="tb_test_date" type="text" class="datepicker">
												<label for="tb_test_date">Medical/TB Screening Test Date</label>
                                            </div> 

										</div>
                                </div>

								<div class="row">
                                        <div class=" col s6 ">
										<label>Medical/TB Screening Test upload</label>
										<div class="file-field input-field">
																<div class="btn7">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
														<div class="file-path-wrapper">
																<input class="file-path validate" name="tb_file_upload" type="text">

																<div>
																</div>
												
														</div>
										</div>

										<div class="uploaded-file" id="tb_file_upload" data-type="file"></div>

                                        </div>
                                        <div class=" col s6 ">
                                        <div class="input-field">

														<select name="spons_status" id="spons_status" class="formSelect">
															<option value="Received">Received</option>
															<option value="Not-Received">Not Received</option>
															<option value="Not-Applicable">Not Applicable</option>
														</select>
														<label for="">Certificate of Sponsorship Status</label>

										</div>
                                        </div>
                                </div>


								<div class="row cos-wrapper hide">
                                        <div class=" col s6 ">
										<div class="input-field">
												<input name="spons_date"  id="spons_date" type="text" class="datepicker">
												<label for="spons_date">COS Received Date</label>
                                        </div>
										</div>
                                        <div class=" col s6 ">


										<label>COS File Upload</label>
										<div class="file-field input-field">
																<div class="btn7">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
														<div class="file-path-wrapper">
																<input class="file-path validate" name="spons_file_upload" type="text">

																<div>
																</div>
												
														</div>
										</div>

										<div class="uploaded-file" id="spons_file_upload" data-type="file"></div>

										
                                       
                                        </div>
                                </div>

								<div class="row">
                                        <div class=" col s6 ">
                                                <div class="input-field">
							<select name="visa_applied" id="visa_applied" class="formSelect">
							<option value="">Select</option>
                                                            <option value="YES">YES</option>
                                                            <option value="NO">NO</option>
							</select>
							<label for="">Have You Applied For Visa  ?</label>
                                                </div>

                                        </div>
                                        <div class=" col s6">
                                        <div class="input-field">
														<select name="visa_status" id="visa_status">
															<option value="">Select</option>
                                                            <option value="Submitted Application Online">Submitted Application Online</option>
                                                            <option value="Documents Submitted To Home Office"> Documents Submitted To Home Office</option>
														</select>
														<label for="">Visa Status?</label>
                                                </div>
                                        </div>
                                </div>
								<div class="row visa_details hide">
                                        <div class=" col s6 ">
										<div class="input-field">
												<input name="visa_appt_date"  id="visa_appt_date" type="text" class="datepicker">
												<label for="visa_appt_date">Visa Appointment Date</label>
                                        </div>
										</div>
                                        <div class=" col s6 ">
										<div class="input-field">
												<input name="visa_reciev_date"  id="visa_reciev_date" type="text" class="datepicker">
												<label for="visa_reciev_date">Visa Received Date </label>
                                        </div>

                                       
                                        </div>
                                </div>
								<div class="row visa_details hide">
                                        <div class=" col s6 ">
										<div class="input-field">
												<input name="visa_exp_date"  id="visa_exp_date" type="text" class="datepicker">
												<label for="visa_exp_date">Visa Expiry Date</label>
                                        </div>
										</div>
                                        <div class=" col s6 ">


										<label>Visa Copy File Upload</label>
										<div class="file-field input-field">
																<div class="btn7">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
														<div class="file-path-wrapper">
																<input class="file-path validate" name="visa_file" type="text">

																<div>
																</div>
												
														</div>
										</div>

										<div class="uploaded-file" id="visa_file" data-type="file"></div>

										
                                       
                                        </div>
                                </div>
								<div class="row visa_details hide">
                                        <div class=" col s6 ">


										<label>Visa Approval Letter File Upload</label>
										<div class="file-field input-field">
																<div class="btn7">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
														<div class="file-path-wrapper">
																<input class="file-path validate" name="visa_appr_file" type="text">

																<div>
																</div>
												
														</div>
										</div>

										<div class="uploaded-file" id="visa_appr_file" data-type="file"></div>

										
                                       
                                        </div>
                                </div>
								<div class="row">
                                        <div class=" col s6 ">
										<div class="input-field">
										<select name="flight_tkt_status" id="flight_tkt_status">
															<option value="">Select</option>
                                                            <option value="Received">Received</option>
                                                            <option value="Not Received">Not Received</option>
															<option value="Not Applicable">Not Applicable</option>
														</select>
														<label for="">Flight Ticket Status?</label>
                                        </div>
										</div>								
                                        <div class=" col s6 flight-ticket-wrapper hide">
										
										<label>Flight Ticket File Upload</label>
										<div class="file-field input-field">
																<div class="btn7">
																		<span>Upload</span>
																		<input type="file"><i class="fa fa-cloud-upload"></i> 
																</div>
														<div class="file-path-wrapper">
																<input class="file-path validate" name="flight_ticket" type="text">

																<div>
																</div>
												
														</div>
										</div>

										<div class="uploaded-file" id="flight_ticket" data-type="file"></div>

										</div>
                                </div>


								</div>
                                <div class="controls">
                                                <a class="mgbtn2">Back</a>
                                                <input id="form_name" type="hidden" class="validate" name="type_of" value="visa_imigration">
                                                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                                                {!! Form::submit( 'SAVE', ['class'=>'waves-effect waves-light btn mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
                        

</div>

                