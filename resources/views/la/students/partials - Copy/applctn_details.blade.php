<div id="tab_9" class="tab-pane " >
        
<div class="content-head">
                                        <h4>Immigration History</h4>
                                        <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->

                                </div>
                                {!! Form::open(['action' => 'LA\StudentsController@store', 'id' => 'application-add-form']) !!}
									
                                <div class="pane">
								
										<div class="row">
											<div class="col s6">
													<p>Have you been to the UK in the past 10 years?</p>
													<p>
															<label>
																	<input name="in_uk" type="radio" value="0" checked/>
																	<span>Yes</span>
															</label>
															<label>
																	<input name="in_uk" type="radio"
																			value="1" />
																	<span>No</span>
															</label>
													</p>
											</div>	
										</div>

										<div class="row">
											<div class="col s12">
													<p>Have you previously studied in any country on either a Tier 4 (General) visa or a Pre-Tier 4 student visa?</p>
													<p>
															<label>
																	<input name="studied_uk" type="radio" value="0" checked/>
																	<span>Yes</span>
															</label>
															<label>
																	<input name="studied_uk" type="radio"
																			value="1" />
																	<span>No</span>
															</label>
													</p>
											</div>	
										</div>										
										<div class="row">
											<div class="col s12">
													<p>Have you been issued with a UK visa in the past 10 years?</p>
													<p>
															<label>
																	<input name="uk_visa_issued" type="radio" value="0" checked/>
																	<span>Yes</span>
															</label>
															<label>
																	<input name="uk_visa_issued" type="radio"
																			value="1" />
																	<span>No</span>
															</label>
													</p>
											</div>	
										</div>
										<div class="row">
											<div class="col s12">
													<p>Have you been to any other countries in the past 10 years?</p>
													<p>
															<label>
																	<input name="other_countries" type="radio" value="0" checked/>
																	<span>Yes</span>
															</label>
															<label>
																	<input name="other_countries" type="radio"
																			value="1" />
																	<span>No</span>
															</label>
													</p>
											</div>	
										</div>										
										
                                        <div class="row">
                                                <div class=" col s6 ">
                                                        <div class="input-field">
                                                                <select name="course">
                                                                        <option value="" disabled>Choose your Course
                                                                        </option>
                                                                        <option value="1">Course A</option>
                                                                        <option value="2" selected>Course B</option>
                                                                        <option value="3">Course C</option>
                                                                </select>
                                                                <label for="">Selected Course</label>
                                                        </div>
                                                </div>
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                <select name="University" id="University">
                                                                        <option value="" disabled>Select</option>
                                                                        <option value="University A">University A</option>
                                                                        <option value="University B">University B</option>
                                                                </select>
                                                                <label for="course">Selected University</label>
                                                        </div>
                                                </div>									
                                        </div>
                                        <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                <input id="tution_fee" name="tution_fee" type="text" class="validate">
                                                                <label for="tution_fee">Tuition Fee</label>
                                                        </div>
                                                </div>
                                               <div class="col s6">
                                                        <div class="input-field ">
                                                                <select name="scholarship_stat">
                                                                        <option value="" disabled>your Status</option>
                                                                        <option value="1" selected>OFFERED </option>
                                                                        <option value="2">NOT RECEIVED</option>
                                                                        <option value="3">RECEIVED</option>
                                                                        <option value="4">NOT APPLICABLE</option>

                                                                </select>
                                                                <label for="">Scholarship Status</label>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                <select name="uni_aplctn_stat">
                                                                        <option value="" disabled>your Status</option>
                                                                        <option value="1" selected>SUBMITED </option>
                                                                        <option value="2">REFUSED</option>
                                                                        <option value="3">ACCEPTED</option>
                                                                        <option value="4">NOT APPLICABLE</option>

                                                                </select>
                                                                <label for="">University Application Status</label>

                                                        </div>
												</div>
												<div class="col s6">
                                                        <div class="input-field ">
                                                                <select name="offer_stat">
                                                                        <option value="" disabled selected>your offer letter status</option>
                                                                        <option value="1">RECEIVED </option>
                                                                        <option value="2">WAITING</option>
                                                                        <option value="3">NOT APPLICABLE</option>
                                                                </select>
                                                                <label for="">Offer Letter Status</label>
                                                        </div>														
                                                </div>
                                        </div>
                                        <div class="row" id="ofl-upload" style="display: none;">
                                                <div class="col s6">
													<label>Upload Offer Letter File</label>
													<div class="file-field input-field">
															<div class="btn">
																	<span>Upload</span>
																	<input type="file" multiple>
															</div>
															<div class="file-path-wrapper">
																	<input class="file-path validate"
																			type="text" name="offerletter_proof" 
																			placeholder="Upload file">
															</div>
													</div>
													<div class="uploaded-file" id="offerletter_proof" data-type="file"></div>
                                                </div>
                                        </div>

                                        <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                <select name="tuitionfeecheck" id="tuitionfeecheck">
                                                                        <option value="" disabled selected>your payment status</option>
                                                                        <option value="1">PAID </option>
                                                                        <option value="2">NOT PAID</option>
                                                                </select>
                                                                <label for="tuitionfeecheck">Tuition Fee Status</label>
                                                        </div>
                                                </div>
												<div class="col s6">
														<div class="input-field">
															<input id="" name="tution_paid" type="text" class="validate">
															<label for="">Amount Paid</label>
														</div>
												</div>
                                        </div>
										
										<div class="row">
												<div class="col s6">
														<div class="input-field ">
																<input id="tution_balance" name="tution_balance" type="text" class="validate">
																<label for="">Balance Amount to be Paid</label>
														</div>
												</div>
												<div class="col s6">
														<div class="input-field ">
                                                                <select name="maintenance_proof" id="maintenance_proof">
                                                                        <option value="" disabled>Select</option>
                                                                        <option value="Bank-Statement">Bank Statement</option>
                                                                        <option value="Student-Loan-Letter">Student Loan Letter </option>
                                                                </select>
																<label for="">Mode of Maintenance Fund proof</label>
														</div>
												</div>											
										</div>
												
                                        <div class="row">
                                                <div class="col s6">
                                                        <label>Proof of Maintenance Fund (Eg : Bank Statement, Student
                                                                Loan Letter etc.)</label>
                                                        <div class="file-field input-field">
                                                                <div class="btn">
                                                                        <span>Upload</span>
                                                                        <input type="file" multiple>
                                                                </div>
                                                                <div class="file-path-wrapper">
                                                                        <input class="file-path validate" name="maintenance_proof" type="text"
                                                                                placeholder="Upload file">
                                                                </div>
                                                        </div>
														<div class="uploaded-file" id="maintenance_proof" data-type="file"></div>
                                                </div>
                                                <div class="col s6">
                                                        <label> TB Screening Test Report File Upload</label>
                                                        <div class="file-field input-field">
                                                                <div class="btn">
                                                                        <span>Upload</span>
                                                                        <input type="file" multiple>
                                                                </div>
                                                                <div class="file-path-wrapper">
                                                                        <input class="file-path validate" name="TB_proof" type="text"
                                                                                placeholder="Upload file">
                                                                </div>
                                                        </div>
														<div class="uploaded-file" id="TB_proof" data-type="file"></div>
                                                </div>												
                                        </div>
										
										
                                        <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                <select name="cas_stat">
                                                                        <option value="" disabled selected>Your CAS status</option>
                                                                        <option value="1">RECEIVED </option>
                                                                        <option value="2">PENDING</option>
                                                                        <option value="3">NOT APPLICABLE</option>
                                                                </select>
                                                                <label for="">CAS Status</label>
                                                        </div>
												</div>
												<div class="col s8" id="cas-rcd" style="display: none">
														<label>Upload CAS Files</label>
														<div class="file-field input-field">
																<div class="btn">
																		<span>Upload</span>
																		<input type="file" multiple>
																</div>
																<div class="file-path-wrapper">
																		<input class="file-path validate"
																				type="text" name="cas_proof"
																				placeholder="Upload file">
																</div>
														</div>
														<div class="uploaded-file" id="cas_proof" data-type="file"></div>
												</div>
                                        </div>
                                        <div class="row">
                                                <div class="col s6">
                                                        <div class="input-field ">
                                                                <select name="visa_aplctn_stat">
                                                                        <option value="" disabled selected>your Visa Application status</option>
                                                                        <option value="1">SUBMITTED </option>
                                                                        <option value="2">NOT SUBMITTED</option>
                                                                </select>
                                                                <label for="">Visa Application Status</label>
                                                        </div>
                                                </div>
												<div class="col s6">
                                                        <label>Upload Flight Ticket</label>
                                                        <div class="file-field input-field">
                                                                <div class="btn">
                                                                        <span>Upload</span>
                                                                        <input type="file" multiple>
                                                                </div>
                                                                <div class="file-path-wrapper">
                                                                        <input class="file-path validate" name="ticket_proof" type="text"
                                                                                placeholder="Upload file">
                                                                </div>
                                                        </div>
														<div class="uploaded-file" id="ticket_proof" data-type="file"></div>
												</div>												
                                        </div>
                                        <div class="row">
                                                <div class="col s12">
                                                        <div class="input-field ">
                                                                <select name="enrollment_stat">
                                                                        <option value="" disabled selected>your Enrollment Status</option>
                                                                        <option value="1">ENROLLED </option>
                                                                        <option value="2">NOT ENROLLED</option>

                                                                </select>
                                                                <label for="">Enrollment Status</label>
                                                        </div>
                                                        <div id="enrolled" style="display: none">
                                                                <br>

                                                                <div class="input-field ">
                                                                        <input id="" type="text" name="enrollment_num" class="validate">
                                                                        <label for="">Enrollment Number</label>

                                                                </div>
                                                        </div>

                                                </div>
                                        </div>
                                        <div class="row">
                                                <div class="col s12">
                                                        <div class="input-field ">
                                                                <select name="placement_through" id="placement_through">
                                                                        <option value="" disabled selected>your Placement Agent</option>
                                                                        <option value="1">AGENT 1 </option>
                                                                        <option value="2">AGENT 2</option>
                                                                        <option value="3">AGENT 3</option>
                                                                </select>
                                                                <label for="">Placement Through</label>
                                                        </div>


                                                </div>
                                        </div>
                                        <div class="row">
                                                <div class="col s12">
                                                        <div class="input-field ">
                                                                <select name="invoice_stat" id="invoice_stat">
                                                                        <option value="" disabled selected>your Invoice Status</option>
                                                                        <option value="1">SUBMITTED </option>
                                                                        <option value="2">NOT SUBMITTED</option>



                                                                </select>
                                                                <label for="">Invoice Status</label>
                                                        </div>
                                                        <div id="invoiced" style="display: none">
                                                                <br>

                                                                <label>Upload Invoice</label>
                                                                <div class="file-field input-field">
                                                                        <div class="btn">
                                                                                <span>Upload</span>
                                                                                <input type="file"  multiple>
                                                                        </div>
                                                                        <div class="file-path-wrapper">
                                                                                <input class="file-path validate"
                                                                                        type="text" name="invoice_proof"
                                                                                        placeholder="Upload file">
                                                                        </div>
                                                                </div>
																<div class="uploaded-file" id="invoice_proof" data-type="file"></div>

                                                        </div>

                                                </div>
                                        </div>
                                        <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
                                                                        <select name="payment_stat" id="payment_stat">
                                                                                <option value="" disabled selected>your Payment Status</option>
                                                                                <option value="1">RECEIVED </option>
                                                                                <option value="2">NOT RECEIVED</option>
                                                                        </select>
                                                                        <label for="">Payment Received</label>
                                                                </div>
        
        
                                                        </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
                                                                        <select name="sub_agent" id="sub_agent">
                                                                                <option value="" disabled selected>select Sub Agent</option>
                                                                                <option value="1">SUB AGENT 1 </option>
                                                                                <option value="2">SUB AGENT 2</option>
                                                                                <option value="3">SUB AGENT 3</option>
                                                                                <option value="4">NO SUB AGENT </option>

                                                                        </select>
                                                                        <label for="">Sub Agent</label>
                                                                </div>
        
        
                                                        </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
                                                                        <select name="sub_agent_invoice" id="sub_agent_invoice">
                                                                                <option value="" disabled selected>select Sub Agent Invoice Status</option>
                                                                                <option value="1">RECEIVED  </option>
                                                                                <option value="2">NOT RECEIVED</option>
                                                                                <option value="3">NOT APPLICABLE</option>

                                                                        </select>
                                                                        <label for="">Invoice from Sub Agent</label>
                                                                </div>
        
        
                                                        </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field">
                                                                        <select name="sub_agent_paymnt_stat" id="sub_agent_paymnt_stat">
                                                                                <option value="" disabled selected>select SubAgent Payment Status</option>
                                                                                <option value="1">PAID  </option>
                                                                                <option value="2">NOT PAID</option>
                                                                                <option value="3">NOT APPLICABLE</option>

                                                                        </select>
                                                                        <label for="">SubAgent Payment Status</label>
                                                                </div>
        
        
                                                        </div>
                                                </div>

                                                <div class="row">
                                                        <div class="col s12">
                                                                <label>Additional Documents File Upload</label>
                                                                <div class="file-field input-field">
                                                                        <div class="btn">
                                                                                <span>Upload</span>
                                                                                <input type="file" multiple>
                                                                        </div>
                                                                        <div class="file-path-wrapper">
                                                                                <input class="file-path validate"
                                                                                        type="text" name="additional_proofs"
                                                                                        placeholder="Upload file">
                                                                        </div>
                                                                </div>
																<div class="uploaded-file" id="additional_proofs" data-type="file"></div>
        
                                                        </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col s12">
                                                                <div class="input-field ">
                                                                                <textarea id="last_communication" name="last_communication" class="materialize-textarea"></textarea>
                                                                                <label>Last Communication with Candidate</label>

                                                                </div>
        
                                                        </div>
                                                </div>

                                </div>
                                <div class="controls">
                                        <a class="mgbtn2">Back</a>
                                                        <input id="form_name" type="hidden" class="validate" name="type_of" value="immigration_history">
                                                        <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
                                                      {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                                              </div>
                                              {{ Form::close() }}
  
                        
</div>

                