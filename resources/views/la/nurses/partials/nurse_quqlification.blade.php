<div id="tab_2" class="tab-pane" >
                                        <div class="content-head">
                                        
                                                        <h4>Qualification details</h4>
                                                        <!-- <div class="id">Nurse ID : <b>#12675</b></div> -->
                                        </div>
                                        {!! Form::open(['action' => 'LA\NursesController@store', 'id' => 'nurse-qualification']) !!}
                                <div class="pane">
									<div class="mgv-wrap">
									<h4 class="sub_title">Enter the details of your qualifications below and upload a clear scan of the supporting documents</h4>
									<div class="row">
										<div class="col s6 ipfield">
											<div class="input-field">
												<input id="qualification" name="qualification" type="text" class="validate">
												<label for="qualification">Qualification gained</label>
											</div>
										</div>
										<div class="col s6 ipfield">
											<div class="input-field">
												<input id="date_awarded" name="date_awarded" type="text" class="validate datepicker">
												<label for="date_awarded" >Date awarded</label>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col s6 ipfield">
											<div class="input-field">
												<input id="student_ref_no" name="student_ref_no" type="text" class="validate">
												<label for="student_ref_no">Students reference number</label>
											</div>
										</div>
										<div class="col s6 ipfield">
											<div class="input-field">
												<input id="institution" name="institution" type="text" class="validate">
												<label for="date_awarded" >Education institute name</label>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col s8">
										<p>Did you use this qualification to register in the country/state you trained in?</p>
										<p>
												<label>
													<!-- <input name="qualify_register" type="radio" value="YES" checked> -->
													<input name="qualify_register" type="radio" value="1" checked>
													<span>YES</span>
												</label>
												<label>
													<!-- <input name="qualify_register" type="radio" value="NO"> -->
													<input name="qualify_register" type="radio" value="0">
													<span>NO</span>
												</label>
										  </p>
										</div>
									</div>
									<div class="row">
										<div class="col s6">
											<label for="">Upload qualification certificate</label>
											<div class="file-field input-field">
													<div class="btn7">
															<span>Upload</span>
															<input type="file" id=""> <i class="fa fa-cloud-upload"></i> 
													</div>
													<div class="file-path-wrapper">
														<input class="file-path validate" name="certificate_upload" type="text" placeholder="Upload file" value="">
														<div></div>
													</div>	
											</div>
											<div class="uploaded-file" id="certificate_upload" data-type="file"></div>
										</div>
										<div class="col s6 ipfield">
											<div class="input-field">
												<input id="certificate_upload_name" name="certificate_upload_name" type="text">
												<label for="date_awarded" >Certificate name</label>
											</div>
										</div>										
									</div>										
									
									<div class="row">
										<div class="col s8">
											<div class="input-field">
												<input id="confirm_name" name="confirm_name" type="text" class="">
												<label>Please confirm the name your qualification was awarded in</label>
											</div>
										</div>
									</div>	
									</div>
									<div class="mgv-wrap">
										<h4 class="sub_title">You have told us your name is different on your evidence</h4>
										<div class="row">
											<div class="col s6">
												<div class="input-field">
													<input id="different_first_name" name="different_first_name" type="text" class="validate">
													<label for="different_first_name">First name</label>
												</div>
											</div>
											<div class="col s6">
												<div class="input-field">
													<input id="different_last_name" name="different_last_name" type="text" class="validate">
													<label for="different_last_name" >Last name</label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col s6">
												<div class="input-field">
													<input id="reasons" name="reasons" type="text" class="">
													<label for="different_first_name">Reasons name are different</label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col s6">
												<label for="">Upload evidence</label>
												<div class="file-field input-field">
														<div class="btn7">
																<span>Upload</span>
																<input type="file" id=""> <i class="fa fa-cloud-upload"></i> 
														</div>
														<div class="file-path-wrapper">
															<input class="file-path" name="evidence_upload" type="text" placeholder = "Upload file" >
															<div></div>
														</div>	
												</div>
												<div class="uploaded-file" id="evidence_upload" data-type="file"></div>
											</div>
										</div>											
									</div>
                                </div>
								
                        <div class="controls">
                                <a class="mgbtn2">Back</a>
                                <input id="form_name" type="hidden" class="validate" name="type_of" value="qualifications">
                                <input type="hidden" value="<?php echo $studentId; ?>" name="user_id" id="student_id">
       
                                {!! Form::submit( 'SAVE', ['class'=>'btn btn-success mgbtn']) !!}
                        </div>
                        {{ Form::close() }}
</div>