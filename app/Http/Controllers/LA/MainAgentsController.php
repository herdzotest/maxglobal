<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use Dwij\Laraadmin\Helpers\LAHelper;
use Dwij\Laraadmin\Models\LAConfigs;

use App\User;
use App\Models\Employee;
use App\Role;
use Mail;
use Log;

class MainAgentsController extends Controller
{
	public $show_action = true;
	public $view_col = 'name';
	public $listing_cols = ['id', 'name','mobile','email','city', 'date_birth'];
	//public $listing_cols = ['id', 'name','mobile','email'];
	
	public function __construct() {
		
		$path = \Request::path();
		if ($path=="agents_save" || $path=="agents_savee") {
			
		} else if(Auth::user()->type != 'SUPER_ADMIN' && Auth::user()->type != 'MANAGER' && Auth::user()->type != 'CONSULTANTS') {
			return abort(403);
		}
		
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Employees', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Employees', $this->listing_cols);
		}
		$this->type = $this->returnType();
	}
	
	/**
	 * Display a listing of the Employees.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Employees');
		unset($module->fields['designation']); unset($module->fields['dept']); unset($module->fields['gender']);

		$staffs = DB::table('users')->select('users.name')->addselect('users.id')
		->leftJoin('employees', 'employees.id', '=', 'users.context_id')
		->where('users.type', 'CONSULTANTS')->whereNull('employees.deleted_at')->where('employees.role',3)->get();

		if(Module::hasAccess($module->id)) {
			return View('la.mainAgents.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module,'type' => $this->type,
				'staffs' => $staffs
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new employee.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created employee in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{	
		$path = \Request::path();
		
		if(Module::hasAccess("Employees", "create") || $path == "agents_save") {
		
			$rules = Module::validateRules("Employees", $request);
			$validator = Validator::make($request->all(), $rules);
			$this->emailId = $request->all()['email']; $this->userId = 0;
			$validator->after(function($validator) {
				if ($this->emailValidation() > 0) {
					$validator->errors()->add('email', 'Email already exists');
				}
			});
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Employees", $request);
			// update role id for user role-id = 4 (Main-agent)
			$refer_code = substr(md5(microtime()), 0, 7);

			$nurse_user = ($this->type == 'nurse')? 1 : 0;
			$type_user = ($this->type == 'student')? 1 : 0;

			DB::table('employees')->where('id',$insert_id)->update(array('role'=>config('constants.MAINAGENT_ID'),'profilePic' => $request->all()['profilePic'],'refer_code' => $refer_code,'type_user' => $type_user,'nurse-user' => $nurse_user ));
			// insert on user table
			$rndtime = 	time();

			try {
				$data = User::create([
					'name' => $request->all()['name'],'context_id' => $insert_id,'email' => $request->all()['email'],
					'password' => bcrypt($rndtime),'type' => 'MAIN_AGENTS','created_at' => date("Y-m-d h:i:sa"),
					'updated_at' => date("Y-m-d h:i:sa")
				]);
				// insert in role table
				DB::table('role_user')->insert([
					'role_id' => config('constants.MAINAGENT_ID'),'user_id' => $data->id, 'employee_id' => $insert_id
				]);
				//Log
				DB::table('data_log')->insert(['user_id' => $data->id,'name' => $request->all()['name'],'email' => $request->all()['email'],'created_by' => Auth::user()->id,'created_at' => date("Y-m-d h:i:sa") ]);
				// End Log
			}
			//catch exception
			catch(Exception $e) {
				$validator->errors()->add('user', 'User not  exists');

				// Log
					DB::table('data_log')->insert(['name' => $request->all()['name'],'email' => $request->all()['email'],'created_by' => Auth::user()->id,'created_at' => date("Y-m-d h:i:sa") ]);
				// End Log
			} 

			if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
				// Send mail to User his new Password
				// Mail::send('emails.send_login_cred', ['user' =>$request->all()['email'], 'token' => $request->all()['_token'],'password' => $rndtime], function ($m) use ($request) {
				// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
				// 	$m->to($request->all()['email'],  $request->all()['name'])->subject('Zodocs  - Login Credentials');
				// });
			} else {
				Log::info("User created username: ".$request->all()['email']);
			}
			return redirect()->route(config('laraadmin.adminRoute') . '.'.$this->type.'.main_agents.index');			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	public function agentRegistration(Request $request)
	{

		//Recaptcha
    	$url = 'https://www.google.com/recaptcha/api/siteverify';
		$remoteip = $_SERVER['REMOTE_ADDR'];
		$data = [
		        'secret' => config('services.recaptcha.secret'),
		        'response' => $request->get('recaptcha'),
		        'remoteip' => $remoteip
		      ];
		$options = [
		        'http' => [
		          'header' => "Content-type: application/x-www-form-urlencoded\r\n",
		          'method' => 'POST',
		          'content' => http_build_query($data)
		        ]
		    ];
		$context = stream_context_create($options);
		        $result = file_get_contents($url, false, $context);
		        $resultJson = json_decode($result); 
		if ($resultJson->success != true) { 
		        return back()->withErrors(['captcha' => 'ReCaptcha Error']);
		        }
		if ($resultJson->score >= 0.3) {
    	//End Recaptcha

		$path = \Request::path();
		if(\Request::path() == "agents_save" || \Request::path() == "agents_savee") {
		
			$rules = Module::validateRules("Employees", $request);
			$validator = Validator::make($request->all(), $rules);
			$this->emailId = $request->all()['email']; $this->userId = 0;
			$validator->after(function($validator) {
				if ($this->emailValidation() > 0) {
					$validator->errors()->add('email', 'Email already exists');
				}
			});
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			$refer_code = substr(md5(microtime()), 0, 7);
			//$insert_id = Module::insert("Employees", $request);
			// update role id for user role-id = 4 (Main-agent)
			//DB::table('employees')->where('id',$insert_id)->update(array('role'=>config('constants.MAINAGENT_ID')));
			$insert_id = DB::table('employees')->insertGetId(['name' => $request->all()['first_name'],'mobile' => $request->all()['mobile'],'mobile2' => $request->all()['whatsapp'],
				'email' => $request->all()['email'],'city' => $request->all()['city'],'date_birth' => $request->all()['dob'],'role'=>config('constants.MAINAGENT_ID'),'refer_code' => $refer_code ]);
			// insert on user table
			$rndtime = 	time();

			try 
			{

				$data = User::create([
					'name' => $request->all()['first_name'],'context_id' => $insert_id,'email' => $request->all()['email'],
					'password' => bcrypt( $request->all()['password']),'type' => 'MAIN_AGENTS','created_at' => date("Y-m-d h:i:sa"),
					'updated_at' => date("Y-m-d h:i:sa")
				]);
				DB::table('users')->where('context_id', $insert_id)->update(['enable_disable' => 0 ]);
				//DB::table('users')->where('context_id', $insert_id)->update(['enable_disable' => 1 ]); // For Demo change
				
				// insert in role table
				$userId = $data->id;
				DB::table('role_user')->insert([
					'role_id' => config('constants.MAINAGENT_ID'),'user_id' => $userId, 'employee_id' => $insert_id
				]);

				// insert data to verify users email
					$token=sha1(time());
					DB::table("verify_users")->insert([
						'user_id' => $userId,
						'token' => $token
					]);

				//Log
				DB::table('data_log')->insert(['user_id' => $userId,'name' => $request->all()['first_name'],'email' => $request->all()['email'],'created_at' => date("Y-m-d h:i:sa") ]);
				// End Log

			}
			//catch exception
			catch(Exception $e) {
				$validator->errors()->add('user', 'User not  exists');

				// Log
					DB::table('data_log')->insert(['name' => $request->all()['first_name'],'email' => $request->all()['email'],'created_at' => date("Y-m-d h:i:sa") ]);
				// End Log
			}

			if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
				/*// Send mail to User his new Password
				Mail::send('emails.send_login_cred', ['user' =>$request->all()['email'], 'token' => $request->all()['_token'],'password' => $rndtime], function ($m) use ($request) {
					$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
					$m->to($request->all()['email'],  $request->all()['first_name'])->subject('Zodocs  - Login Credentials');
				});*/
				//	Send confirmation link to User..
				/*Mail::send('emails.verify', ['user' =>$request->all()['email'], 'token' =>$token,'name'=>$request->all()['first_name'], 'password' => $request->all()['password']], function ($m) use ($request) {
					$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
					$m->to($request->all()['email'],  $request->all()['first_name'])->subject(LAConfigs::getByKey('sitename').' -- Verify Email');
				});*/

				// Send login credentials to agent
				if($path=="agents_savee"){
					// Mail::send('emails.send_login_cred_agent_cf', ['user' =>$request->all()['email'], 'name'=>$request->all()['first_name'], 'token' => $request->all()['_token'],'password' => $request->all()['password']], function ($m) use ($request) {
					// $m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
					// $m->to($request->all()['email'],  $request->all()['first_name'])->subject('Zodocs - Login Credentials');
					// });
				} else {
					// Mail::send('emails.send_login_cred_agent', ['user' =>$request->all()['email'], 'name'=>$request->all()['first_name'], 'token' => $request->all()['_token'],'password' => $request->all()['password']], function ($m) use ($request) {
					// $m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
					// $m->to($request->all()['email'],  $request->all()['first_name'])->subject('Zodocs - Login Credentials');
					// });
				}
				

				// Send notification to admin
				$manager_email = DB::table('users')->select('email')->addselect('name')->where('type', 'MANAGER')->get();
					foreach($manager_email as $key => $result) {
						$request['admin_name'] = $result->name;
						$request['admin_email'] = $result->email;
						if($request['admin_email'] != '') {
						//	Send mail to manager
							// Mail::send('emails.agent_reg_admin', ['admin' =>$request['admin_name'],'agent' => $request->all()['first_name'],'agentemail' => $request->all()['email']], function ($m) use ($request) {
							// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
							// 	$m->to($request['admin_email'])->subject('Zodocs - Agent Registration');
							// });
						}
					}


			} else {
				Log::info("User created username: ".$request->all()['email']);
			}
			//return redirect()->route(config('laraadmin.adminRoute') . '.'.$this->type.'.main_agents.index');

			//\Session::flash('status', 'Registration Successful, Please check your inbox for further instructions');
			//return redirect('/registration');
			if($path=="agents_savee"){
		    	\Session::flash('status', 'Registration Successful, Please check your inbox for further instructions');
		  		return redirect('/signup');
		    } else {
		    	\Session::flash('status', 'Registration Successful, Please check your inbox for further instructions');
		    	return redirect('/registration');
		    }	
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}

		//Recaptcha
        } else {  
	        return back()->withErrors(['captcha' => 'ReCaptcha Error']);
		}
		//End Recaptcha
	}

	/**
	 * Display the specified employee.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Employees", "view")) {
			
			$employee = Employee::find($id);
			if(isset($employee->id)) {
				$module = Module::get('Employees');
				$module->row = $employee;
				$mailDetails = DB::table('mail_settings')
							->select('mail_settings.id')->addselect('mail_settings.mail_content')->addselect('mail_settings.subject')
							->addselect('us.name')->addselect('us.type')->addselect('mail_settings.send_date')
							->leftJoin('users', 'users.id', '=', 'mail_settings.to_user')
							->leftJoin('users AS us', 'us.id', '=', 'mail_settings.from_user')
							->where('mail_settings.is_deleted', 0)
							->where('users.context_id', $id)->orderBy('mail_settings.send_date', 'desc')->get();

				return view('la.mainAgents.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding",
					'mailDetails' => $mailDetails,'type' => $this->type
				])->with('employee', $employee);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("employee"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	public function deleteMail($id,$employee)
	{
		DB::table('mail_settings')->where('id', $id)->update(['is_deleted' => 1]);
		\Session::flash('success_mail_delete', 'Mail is deleted successfully');
		
		return redirect(config('laraadmin.adminRoute') . '/'.$type.'/main_agents/'.$employee.'#tab-timeline');
	}

	/**
	 * Show the form for editing the specified employee.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Employees", "edit")) {			
			$employee = Employee::find($id);
			if(isset($employee->id)) {	
				$module = Module::get('Employees');
				unset($module->fields['designation']); unset($module->fields['dept']); unset($module->fields['gender']);
				$module->row = $employee;
				
				return view('la.mainAgents.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
					'type' => $this->type
				])->with('employee', $employee);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("employee"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified employee in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Employees", "edit")) {
			
			$rules = Module::validateRules("Employees", $request, true);
			$validator = Validator::make($request->all(), $rules);
			$this->emailId = $request->all()['email']; $this->userId = $id;
			$validator->after(function($validator) {
				if ($this->emailValidation() > 0) {
					$validator->errors()->add('email', 'Email already exists');
				}
			});
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Employees", $request, $id);
			DB::table('employees')->where('id',$insert_id)->update(array('profilePic' => $request->all()['profilePic'] ));
			DB::table('users')->where('context_id', $insert_id)->update(['email' => $request->all()['email'], 'name' => $request->all()['name'] ]);
			return redirect(config('laraadmin.adminRoute') . '/'.$this->type.'/main_agents/'.$id);
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * email validation 
	 */
	public function emailValidation()
	{
		$email = DB::table('employees')
				->whereNotIn('id', [$this->userId])
				->where('email', $this->emailId)
				->get();
				
		return count($email);
	}

	/**
	 * Remove the specified employee from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Employees", "delete")) {
			Employee::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.'.$this->type.'.main_agents.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	public function disable($id)
	{	
		DB::table('users')->where('context_id', $id)->update(['enable_disable' => 0]);
		$agentDet = DB::table('users')->where('context_id', $id)->get();
		$agentData = $agentDet[0];
		\Session::flash('success_message', 'Agent is successfully disabled');

		if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "")     {
				//	confirmation mail inactive user
			//   Mail::send('emails.deactive_user', ['user' => $agentData->name], function ($m) use ($agentData)  {
			// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
			// 	$m->to($agentData->email)->subject(LAConfigs::getByKey('sitename').' -- Account Deactivated');
			//   }); 
		}
		
		return redirect()->route(config('laraadmin.adminRoute') . '.'.$this->type.'.main_agents.index');	
	}

	public function enable($id)
	{	
		DB::table('users')->where('context_id', $id)->update(['enable_disable' => 1]);
		$agentDet = DB::table('users')->where('context_id', $id)->get();
		$agentData = $agentDet[0];
		\Session::flash('success_message', 'Agent is successfully enabled');

		if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "")     {
				// confirmation mail active user
			//   Mail::send('emails.active_user', ['user' => $agentData->name], function ($m) use ($agentData)  {
			// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
			// 	$m->to($agentData->email)->subject(LAConfigs::getByKey('sitename').' -- Account Activated');
			//   }); 
		}

		return redirect()->route(config('laraadmin.adminRoute') . '.'.$this->type.'.main_agents.index');	
	}
	
	public function returnType()
	{
		$currentUrl = $_SERVER['REQUEST_URI'];
		$type = (strpos($currentUrl,'student') !== false) ? 'student' : 'nurse';
		
		return $type;
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$type = $_GET['type'];
		$column = ($type == 'nurse') ? 'nurse-user' : 'type_user';


		$employeeCols = preg_filter('/^/', 'employees.', $this->listing_cols);
		$employeeCols[] = 'employees.profilePic';

		$conditions = "";
		if(isset($_GET['staff_id'])){ $staff_id = $_GET['staff_id']; } else {$staff_id = ''; }
		if ($staff_id != '') {
			
		} 



		/*$values = DB::table('employees')->select($this->listing_cols)->addselect('profilePic')
					->whereNull('deleted_at')->where('role',4)->where($column,1);*/
		$values = DB::table('employees')->select($employeeCols)
					->leftJoin('staff_agents', 'employees.id', '=', 'staff_agents.staff_emp_id')
					->whereNull('deleted_at')->where('role',4)->where($column,1);			
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Employees');

		$staffData = DB::table('staff_agents')
					->select('users.context_id AS agent_emp_id')->addselect('staff_agents.staff_user_id')->addselect('employees.name')
					->leftJoin('employees', 'staff_agents.staff_emp_id', '=', 'employees.id')
					->leftJoin('users', 'staff_agents.agent_id', '=', 'users.id')
					->get();		
		$agentEmpIds = array_column($staffData, 'agent_emp_id');
		$staffNames = array_column($staffData, 'name');
		$assignStaff=array_combine($agentEmpIds,$staffNames);
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$avatatImg = ($data->data[$i][6] == '') ? 'https://www.gravatar.com/avatar/ef1c169902a351837b6c7f8816cf9b5c.jpg?s=80&d=mm&r=g' : $data->data[$i][6];
					/*$avatatImg = ($data->data[$i][4] == '') ? 'https://www.gravatar.com/avatar/ef1c169902a351837b6c7f8816cf9b5c.jpg?s=80&d=mm&r=g' : $data->data[$i][4];*/
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/'.$type.'/main_agents/'.$data->data[$i][0]).'"><img src="'.$avatatImg.'" alt=""/><span>'.$data->data[$i][$j].'</span></a>';
				}
			}

			if(isset($assignStaff[$data->data[$i][0]])) {
				$data->data[$i][7] = $assignStaff[$data->data[$i][0]];
			} else {
				$data->data[$i][7] = '';
			}
			
			if($this->show_action) {

				$agentStatus = DB::table('users')
						->select('users.enable_disable')
						->where('users.context_id', $data->data[$i][0])->get();

				$output = '';
				if(Module::hasAccess("Employees", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/'.$type.'/main_agents/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs editbtn" ><i class="fa fa-pencil"></i></a>';
				}
				
				if(Module::hasAccess("Employees", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.'.$type.'.main_agents.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline','onSubmit'=>"return confirm('Are you sure you wish to delete?');"]);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-trash"></i></button>';
					$output .= Form::close();
				}

				if(count($agentStatus) > 0 && $agentStatus[0]->enable_disable == '1') {
					$output .= ' <a href="'.url(config('laraadmin.adminRoute') . '/'.$type.'/mainagent/disable/'.$data->data[$i][0]).'"><button class="btn btn-success btn-xs tgnon" data-toggle="tooltip" title="Disable Agent" type="submit"><i class="fa fa-toggle-on"></i></button>';
				} else {
					$output .= ' <a href="'.url(config('laraadmin.adminRoute') . '/'.$type.'/mainagent/enable/'.$data->data[$i][0]).'"><button class="btn btn-success btn-xs tgnoff" data-toggle="tooltip" title="Enable Agent" type="submit"><i class="fa fa-toggle-off"></i></button>';
				}

				unset($data->data[$i][6]);
				$data->data[$i] = array_values($data->data[$i]);
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
	
	/**
     * Change Employee Password
     *
     * @return
     */
	public function change_password($id, Request $request) {
		
		$validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
			'password_confirmation' => 'required|min:6|same:password'
        ]);
		
		if ($validator->fails()) {
			return \Redirect::to(config('laraadmin.adminRoute') . '/'.$this->type.'/main_agents/'.$id.'#tab-account-settings')->withErrors($validator);
		}
		
		$employee = Employee::find($id);
		$user = User::where("context_id", $employee->id)->first();
		$user->password = bcrypt($request->password);
		$user->save();
		
		\Session::flash('success_message', 'Password is successfully changed');
		
		// Send mail to User his new Password
		if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
			// Send mail to User his new Password
			// Mail::send('emails.send_login_cred_change', ['user' => $user, 'password' => $request->password], function ($m) use ($user) {
			// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
			// 	$m->to($user->email, $user->name)->subject('Zodocs - Login Credentials changed');
			// });
		} else {
			Log::info("User change_password: username: ".$user->email." Password: ".$request->password);
		}
		
		return redirect(config('laraadmin.adminRoute') . '/'.$this->type.'/main_agents/'.$id.'#tab-account-settings');
	}

}
