<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;
use Dwij\Laraadmin\Helpers\LAHelper;
use Dwij\Laraadmin\Models\LAConfigs;

use App\Models\Upload;
use App\User;
use App\Models\Employee;
use App\Role;
use Mail;
use Log;
use DateTime;


class StudentsController extends Controller
{
	public $show_action = true;
	public $view_col = 'name';
	public $listing_cols = ['id', 'name','mobile', 'email', 'city', 'date_birth'];
	
	public function __construct() {

		$path = \Request::path();
		$userTypeAccess = array('SUPER_ADMIN','MANAGER','STUDENT','CONSULTANTS','UNIVERSITY_USER','MAIN_AGENTS','SUBCONTRACTORS');
		if ($path=="registration" || $path=="registration_save" || $path=="registration_savee" || \Request::is('user/verify/*') || \Request::is('user/verifycf/*')) {
			
		} else if(!in_array(Auth::user()->type, $userTypeAccess) || (Auth::user()->type == 'STUDENT' && $path == 'admin/students')) {
			//return abort(403);	
		}
	}

	public function index()
	{			
		$consultants = DB::table('employees')->select('employees.name')->addselect('users.id')->addselect('employees.mobile')
						->leftJoin('users', 'users.context_id', '=', 'employees.id')
						->where('employees.nurse-user', 0)
						->where('employees.role', 3)->whereNull('employees.deleted_at')->get();

		$docstaffs = '';
		if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER","CONSULTANTS","MAIN_AGENTS"])) {
			$docstaffs = DB::table('employees')->select('employees.name')->addselect('users.id')->addselect('employees.mobile')
						->leftJoin('users', 'users.context_id', '=', 'employees.id')
						->where('employees.role', 5)->whereNull('employees.deleted_at')->get();
		}

		$universityAgents = DB::table('employees')->select('employees.name')->addselect('users.id')
						->leftJoin('users', 'users.context_id', '=', 'employees.id')
						->where('employees.role', 5)->whereNull('employees.deleted_at')->get();

		$agents = DB::table('users')->select('name')->addselect('id')->where('type', 'MAIN_AGENTS')->get();
		$countries = DB::table('countries')->select('country')->where('status', 1)->get();
		$university = DB::table('universities')->select('university')->where('status', 1)->get();
		$stdstatus = DB::table('studentstatus')->select('student_status')->where('status', 1)->get();
		$stdcounselor = DB::table('employees')->select('employees.name')->addselect('users.id')
						->leftJoin('users', 'users.context_id', '=', 'employees.id')
						->where('employees.role', 3)->whereNull('employees.deleted_at')->get();
		$stddocumentation = DB::table('employees')->select('employees.name')->addselect('users.id')
						->leftJoin('users', 'users.context_id', '=', 'employees.id')
						->where('employees.role', 5)->whereNull('employees.deleted_at')->get();
						
		return View('la.students.index', [
			'show_actions' => $this->show_action,
			'consultants' => $consultants,
			'universityAgents' => $universityAgents,
			'agents' => $agents,
			'countries' => $countries,
			'university' => $university,
			'stdstatus' => $stdstatus,
			'stdcounselor' => $stdcounselor,
			'stddocumentation' => $stddocumentation,
			'docstaffs' => $docstaffs
		]);
	}

	public function registration()
	{
		return View('la.students.registration', [
			'show_actions' => $this->show_action
		]);
	}

	public function registration_save(Request $request)
	{	

		//Recaptcha
    	$url = 'https://www.google.com/recaptcha/api/siteverify';
		$remoteip = $_SERVER['REMOTE_ADDR'];
		$data = [
		        'secret' => config('services.recaptcha.secret'),
		        'response' => $request->get('recaptcha'),
		        'remoteip' => $remoteip
		      ];
		$options = [
		        'http' => [
		          'header' => "Content-type: application/x-www-form-urlencoded\r\n",
		          'method' => 'POST',
		          'content' => http_build_query($data)
		        ]
		    ];
		$context = stream_context_create($options);
		        $result = file_get_contents($url, false, $context);
		        $resultJson = json_decode($result); 
		if ($resultJson->success != true) { 
		        return back()->withErrors(['captcha' => 'ReCaptcha Error']);
		        }
		if ($resultJson->score >= 0.3) { 
    	//End Recaptcha

		$path = \Request::path();
		if(count($request->all()) > 0) {
		
			$userId = 0;
			$studentsId=array();
		   $rndtime = 	time();
		   $rules = Module::validateRules("Employees", $request);
		  $validator = Validator::make($request->all(), $rules);
		  $this->emailId = $request->all()['email'];
		
		  $validator->after(function($validator) {
			  if ($this->emailValidation() > 0) {
				  $validator->errors()->add('email', 'Email already exists');
			  }
		  });
		  
		  if ($validator->fails()) {
			  return redirect()->back()->withErrors($validator)->withInput();
		  }

		  try {
			  $data = User::create([
				  'name' => $request->all()['first_name'],'context_id' => $rndtime,'email' => $request->all()['email'],
				  'password' => bcrypt( $request->all()['password']),'type' => 'STUDENT','created_at' => date("Y-m-d h:i:sa"),
				  'updated_at' => date("Y-m-d h:i:sa"),'verified' => 1,
			  ]);

			  /*$data = User::create([
				  'name' => $request->all()['first_name'],'context_id' => $rndtime,'email' => $request->all()['email'],
				  'password' => bcrypt( $request->all()['password']),'type' => 'STUDENT','created_at' => date("Y-m-d h:i:sa"),
				  'updated_at' => date("Y-m-d h:i:sa"),'verified' => 0,
			  ]);*/  // For demo change

		  	  //try {
			  $studentsId = DB::table('users')
			  ->select('users.id')
			  ->where('users.email', $request->all()['email'])->get();
			  // insert student role
			  $this->insertStudentRole($studentsId[0]->id);

			  //Log
			  DB::table('data_log')->insert(['user_id' => $studentsId[0]->id,'name' => $request->all()['first_name'],'email' => $request->all()['email'],'created_at' => date("Y-m-d h:i:sa") ]);
			  // End Log

			}
			
			//catch exception
			catch(Exception $e) {
			  $validator->errors()->add('user', 'User not  exists');

			  //Log
			  DB::table('data_log')->insert(['name' => $request->all()['first_name'],'email' => $request->all()['email'],'created_at' => date("Y-m-d h:i:sa") ]);
			  // End Log

			}
			$token=sha1(time());
			DB::table("verify_users")->insert([
				'user_id' => $studentsId[0]->id,
				'token' => $token
			  ]);
			  if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
				//	Send mail to User his new Password
			if($path=="registration_savee"){
			// 	Mail::send('emails.verify_cf', ['user' =>$request->all()['email'], 'token' =>$token,'name'=>$request->all()['first_name'], 'password' => $request->all()['password']], function ($m) use ($request) {
			// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
			// 	$m->to($request->all()['email'],  $request->all()['first_name'])->subject('Verify Email');
			//    });
			} else {
			// 	Mail::send('emails.verify', ['user' =>$request->all()['email'], 'token' =>$token,'name'=>$request->all()['first_name'], 'password' => $request->all()['password']], function ($m) use ($request) {
			// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
			// 	$m->to($request->all()['email'],  $request->all()['first_name'])->subject('Verify Email');
			//    });
			}
			  

			  $request1['stud_name'] = $request->all()['first_name'];
				$request1['stud_email'] = $request->all()['email'];
				$manager_email = DB::table('users')->select('email')->addselect('name')->where('type', 'MANAGER')->get();
				foreach($manager_email as $key => $result) {
					$request1['admin_name'] = $result->name;
					$request1['admin_email'] = $result->email;
					if($request1['admin_email'] != '') {
						//	Send mail to manager
						// Mail::send('emails.addstud_admin', ['admin' => $request1['admin_name'],'user' =>$request1['stud_name'],'useremail' =>$request1['stud_email']], function ($m) use ($request1) {
						// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
						// 	$m->to($request1['admin_email'])->subject('Scholab - Student Registration');
						// });
					}
				}

			  
		  } else {
			  Log::info("User created username: ".$request->all()['email']);
		  }

		  // insert student data
		  $insertData = request()->except(['_token','type_of','email','student_id','password','user_type','std_agent_id','recaptcha']);
		  // format birth date
		  if($insertData['dob'] != '') {
			$bday = new DateTime($insertData['dob']);
			$today = new Datetime(date('Y-m-d'));
			$diff = $today->diff($bday);
            $insertData['age'] = $diff->y;
			$insertData['dob'] = date("M d, Y", strtotime($insertData['dob']));
		  }
		  $insertData['coountry_code'] = 91;
		  DB::table("data_".$request->all()['type_of'])->insert([
				array_merge($insertData, [ 'user_id' => $studentsId[0]->id ])
			]);

		  // Insert Agent Id
			if($request->all()['std_agent_id'] != '') {
				//$std_agent = DB::table('users')->select('name')->addselect('id')->where('context_id', $request->all()['std_agent_id'])->get();
				$std_agent = DB::table('users')
						->select('users.name')->addselect('users.id')
						->leftJoin('employees', 'users.context_id', '=', 'employees.id')
						->where('employees.refer_code', $request->all()['std_agent_id'])->get();
				if($std_agent && $std_agent[0]->name != ''){ 
					$std_agent_name = $std_agent[0]->name;
				} else { $std_agent_name = ''; }
				DB::table('data_student_lead')->insert(['user_id' => $studentsId[0]->id,'lead_type' => 'Agents','lead_from' => 'Agents','agent' => $std_agent_name,'agent_id' => $std_agent[0]->id ]);
			} 
			// End Insert Agent Id

		  /*DB::table("data_".$request->all()['type_of'])->insert([
			array_merge(request()->except(['_token','type_of','email','student_id','password','user_type']), [ 'user_id' => $studentsId[0]->id ])

			  ]);
			return View('la.students.registration', [
				'show_actions' => $this->show_action
			]); */
		  //\Session::flash('status', 'Registration Successful, Please check your inbox for further instructions');
		  if($path=="registration_savee"){
		  	\Session::flash('status', 'Registration Successful, Please check your inbox for further instructions');
		  	return redirect('/signup');
		  } else {
		  	\Session::flash('status', 'Registration Successful, Please check your inbox for further instructions');
		  	return redirect('/registration');
		  }
		  //return redirect('/registration');
		} else {
			return redirect('/login');
		}

		//Recaptcha
        } else {  
	        return back()->withErrors(['captcha' => 'ReCaptcha Error']);
		}
		//End Recaptcha
	}

	public function verifyUser($token)
	{
	  $verifyUser =  DB::table('verify_users')
	  ->where('token', $token)->first();
	  
	  if(isset($verifyUser) ){
		$user = DB::table('users')
		->where('users.id',$verifyUser->user_id)->first();
		
		if($user->verified) {
		  DB::table("users")
		  ->where('id', $verifyUser->user_id)
		 ->update(['verified' => 0]);
		 
		  $status = "Your e-mail is verified. You can now login.";
		} else {
		  $status = "Your e-mail is already verified. You can now login.";
		}
	  } else {
		return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
	  }
	  
	  return redirect('/login')->with('status', $status);
	}

	public function verifyUsercf($token)
	{
	  $verifyUser =  DB::table('verify_users')
	  ->where('token', $token)->first();
	  
	  if(isset($verifyUser) ){
		$user = DB::table('users')
		->where('users.id',$verifyUser->user_id)->first();
		
		if($user->verified) {
		  DB::table("users")
		  ->where('id', $verifyUser->user_id)
		 ->update(['verified' => 0]);
		 
		  $status = "Your e-mail is verified. You can now login.";
		} else {
		  $status = "Your e-mail is already verified. You can now login.";
		}
	  } else {
		return redirect('/userlogin')->with('warning', "Sorry your email cannot be identified.");
	  }
	  
	  return redirect('/userlogin')->with('status', $status);
	}
	
	/**
	 * Show the form for editing the STUDENTS.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if($id > 0) {
			$student_email = DB::table('users')->select('email')->where('id', $id)->get();
			$std_email = $student_email[0]->email;
		} else { $std_email = ''; }
		
		$courses = DB::table('manage_courses')->select('manage_courses.id')->addselect('manage_courses.course_name')
			->where('manage_courses.status', 1)->get();	
		$countries = DB::table('countries')->select('country')->where('status', 1)->get();	
		$university = DB::table('universities')->select('university')->where('status', 1)->get();
		$syllabus = DB::table('syllabus')->select('name')->where('status', 1)->get();
		$agents = DB::table('users')->select('name')->addselect('id')->where('type', 'MAIN_AGENTS')->get();
		$campus = DB::table('campus')->select('campus_name')->where('status', 1)->get();
		$stream = DB::table('stream')->select('stream_name')->where('status', 1)->get();
		$academic = DB::table('data_school')->select('json_data')->where('user_id', $id)->get();

		if($academic && $academic[0]->json_data != ''){ $wrp_count_academic = count(json_decode($academic[0]->json_data,true)) + 1; } else { $wrp_count_academic = '1'; }
		$coursepreferred = DB::table('data_courses_applied')->select('json_data')->where('user_id', $id)->get();
		if($coursepreferred && $coursepreferred[0]->json_data != ''){ $wrp_count_coursepreferred = count(json_decode($coursepreferred[0]->json_data,true)) + 1; } else { $wrp_count_coursepreferred = '1'; }
		$courseapplied = DB::table('data_courses_applied_new')->select('json_data')->where('user_id', $id)->get();
		if($courseapplied && $courseapplied[0]->json_data != ''){ $wrp_count_courseapplied = count(json_decode($courseapplied[0]->json_data,true)) + 1; } else { $wrp_count_courseapplied = '1'; }
		$emp_history = DB::table('data_employement_history')->select('json_data')->where('user_id', $id)->get();
		if($emp_history && $emp_history[0]->json_data != ''){ $wrp_count_emp_history = count(json_decode($emp_history[0]->json_data,true)) + 1; } else { $wrp_count_emp_history = '1'; }
		$follow_up = DB::table('data_follow_up')->select('json_data')->where('user_id', $id)->get();
		if($follow_up && $follow_up[0]->json_data != ''){ $wrp_count_follow_up = count(json_decode($follow_up[0]->json_data,true)) + 1; } else { $wrp_count_follow_up = '1'; }
		$dependant_det = DB::table('data_dependant_det')->select('json_data')->where('user_id', $id)->get();
		if($dependant_det && $dependant_det[0]->json_data != ''){ $wrp_count_dependant_det = count(json_decode($dependant_det[0]->json_data,true)) + 1; } else { $wrp_count_dependant_det = '1'; }
		
		return view('la.students.edit', [
			'studentId' => $id,
			'courses' => $courses,
			'countries' => $countries,
			'university' => $university,
			'syllabus' => $syllabus,
			'agents' => $agents,
			'campus' => $campus,
			'stream' => $stream,
			'std_email' => $std_email,
			'wrp_count_academic' => $wrp_count_academic,
			'wrp_count_coursepreferred' => $wrp_count_coursepreferred,
			'wrp_count_courseapplied' => $wrp_count_courseapplied,
			'wrp_count_emp_history' => $wrp_count_emp_history,
			'wrp_count_follow_up' => $wrp_count_follow_up,
			'wrp_count_dependant_det' => $wrp_count_dependant_det
		]);
	}

	public function store(Request $request)
	{ 
		$is_update=false; $recurring_flag = 0;
  		if(Module::hasAccess("Employees", "create")) {
			$this->userId = 0;
			$studentsId=array();
			if($request->all()['user_id']){
					$studentsId[] = (object) array('id' => $request->all()['user_id']);
				}
			if ($this->isUserExist("data_".$request->all()['type_of'],$request->all()['user_id']) > 0) {
				$is_update=true;
			}
			//switch($request->all()['type_of']){
			$pageType = $request->all()['type_of'];
			switch($pageType){
				case "personal_information":
				// insert on user table
				$rndtime = 	time();
				 $rules = Module::validateRules("Employees", $request);
				$validator = Validator::make($request->all(), $rules);
				
				if(!$is_update){
					$this->emailId = $request->all()['email'];
					$validator->after(function($validator) {
						if ($this->emailValidation() > 0) {
							$validator->errors()->add('email', 'Email already exists');
						}
					});
					if ($validator->fails()) {
						return redirect()->back()->withErrors($validator)->withInput();
					}
					try {
					$data = User::create([
						'name' => $request->all()['first_name'],'context_id' => $rndtime,'email' => $request->all()['email'],
						'password' => bcrypt($rndtime),'type' => 'STUDENT','created_at' => date("Y-m-d h:i:sa"),
						'updated_at' => date("Y-m-d h:i:sa")
					]);
					//try {
						$studentsId = DB::table('users')
						->select('users.id')
						->where('users.email', $request->all()['email'])->get();
						
						/*if( in_array(Auth::user()->type, ["CONSULTANTS","MAIN_AGENTS"]) ) {
							DB::table('users')->where('id', $studentsId[0]->id)->update(['assigned_to' => Auth::user()->id ]);
						}*/

						if(Auth::user()->type == "CONSULTANTS") {
							DB::table('users')->where('id', $studentsId[0]->id)->update(['assigned_to' => Auth::user()->id ]);
						} else if( Auth::user()->type == 'MAIN_AGENTS') {
							DB::table('data_student_lead')->insert(['user_id' => $studentsId[0]->id,'lead_type' => 'Agents','lead_from' => 'Agents','agent' => Auth::user()->name,'agent_id' => Auth::user()->id ]);
						} else if( Auth::user()->type == "UNIVERSITY_USER") {
							DB::table('users')->where('id', $studentsId[0]->id)->update(['assigned_to_agents' => Auth::user()->id ]);
						}

						// insert student role
						$this->insertStudentRole($studentsId[0]->id);

						// Log
						DB::table('data_log')->insert(['user_id' => $studentsId[0]->id,'name' => $request->all()['first_name'],'email' => $request->all()['email'],'created_by' => Auth::user()->id,'created_at' => date("Y-m-d h:i:sa") ]);
						// End Log
						

					} 
				   //catch exception
				   catch(Exception $e) {
					$validator->errors()->add('user', 'User not  exists');

					// Log
						DB::table('data_log')->insert(['name' => $request->all()['first_name'],'email' => $request->all()['email'],'created_by' => Auth::user()->id,'created_at' => date("Y-m-d h:i:sa") ]);
					// End Log

				   } 
					if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
					//	Send mail to User his new Password
						if($request->all()['notify'] == 1) {
						// Mail::send('emails.send_login_cred', ['user' =>$request->all()['email'], 'token' => $request->all()['_token'],'password' => $rndtime], function ($m) use ($request) {
						// //	var_dump($request);
						// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
						// 	$m->to($request->all()['email'],  $request->all()['first_name'])->subject('Zodocs - Login Credentials');
						// });
					 }

						$request1['stud_name'] = $request->all()['first_name'];
						$request1['stud_email'] = $request->all()['email'];
						$manager_email = DB::table('users')->select('email')->addselect('name')->where('type', 'MANAGER')->get();
						foreach($manager_email as $key => $result) {
							$request1['admin_name'] = $result->name;
							$request1['admin_email'] = $result->email;
							if($request1['admin_email'] != '') {
							//	Send mail to manager
								// Mail::send('emails.addstud_admin', ['admin' => $request1['admin_name'],'user' =>$request1['stud_name'],'useremail' =>$request1['stud_email']], function ($m) use ($request1) {
								// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
								// 	$m->to($request1['admin_email'])->subject('Scholab - Student Registration');
								// }); 
							}
						}

					} else {
						Log::info("User created username: ".$request->all()['email']);
					} 

					// Send add student notification
					$this->sendStudentAddNotif($studentsId[0]->id,$request->all()['first_name'],Auth::user()->id,Auth::user()->type,Auth::user()->name);
					// End Send add student notification

				} else {
					DB::table('users')->where('id', $studentsId[0]->id)->update(['name' => $request->all()['first_name']]);		
				}
				break;
				case "passport_details":
					 $image_front="";
					 $image_back="";
					//$file_array=["image_front"=>0,"image_back"=>0];
					//	array_merge(request()->except(['_token','type_of']), ['image_front' => $image_front]);
					// Code to be executed if n=label1
					break;
				//case "school":
					//$document="";
					//array_merge(request()->except(['_token','type_of']), ['document' => $document]);
					// Code to be executed if n=label2
					//break;
				case "higher":
						$document="";
						array_merge(request()->except(['_token','type_of']), ['document' => $document]);
						// Code to be executed if n=label2
						break;
				case "bachelors":
							$document="";
							array_merge(request()->except(['_token','type_of']), ['document' => $document]);
							break;
				case "masters":
					$letter_url="";
					$statement_url="";
					array_merge(request()->except(['_token','type_of']), ['letter_url' => $letter_url,'statement_url' => $statement_url]);
								break;
				case "misc":
										$exprience="";
										$over_document='';
										array_merge(request()->except(['_token','type_of']), ['exprience' => $exprience,'over_document' => $over_document]);
				break;
				case "aplctndetails":
					$offerletter_proof="";
					$maintenance_proof='';
					$TB_proof="";
					$cas_proof='';
					$visa_file="";
					$ticket_proof='';
					$file_array=["offerletter_proof"=>0,"maintenance_proof"=>0,
						"TB_proof"=>0,"cas_proof"=>0,"visa_file"=>0,"ticket_proof"=>0,"invoice_proof"=>0,"additional_proofs"=>1,
					];
					array_merge(request()->except(['_token','type_of']), ['exprience' => $offerletter_proof,'over_document' => $maintenance_proof]);
				break;
				case "courses_applied":
				case "school":
				case "courses_applied_new":
				case "employement_history":
				case "follow_up":
				case "dependant_det":
				case "immigration_history":

					$formtype = $request->all()['formtype'];
					$postData = $request->all();
					unset($postData['_token']); unset($postData['type_of']); unset($postData['formtype']);
					unset($postData['student_id']); 
					$recurring_flag = 1;

					if($pageType == "courses_applied_new" && str_replace(' ','',$postData['status_note']) != '') {
						// Remove empty elements
						$note = array();
						foreach ($postData['status_note'] as $key => $value) {
							if($value != ''){$note[] = $value;}
						} 
						$postData['status_note'] = json_encode($note,true); 
						// End Remove empty elements
					}
					$postData['json_data'] = '';

					if( isset($request->all()[$formtype]) && count($request->all()[$formtype]) > 0 ) {
						$json_datas = json_encode($request->all()[$formtype],true);
						unset($postData[$formtype]);
						$postData['json_data'] = $json_datas;
					}
					unset($postData['formtype_studied']); 
					if(isset($request->all()['imigration_studied']) && count($request->all()['imigration_studied']) > 0 ) {
						$json_datas_studied = json_encode($request->all()['imigration_studied'],true);
						unset($postData['imigration_studied']);
						$postData['json_data_studied'] = $json_datas_studied;				
					}
				break;
				default:
					break;
			}

			if ($is_update) { 

				// Edit email
				if(isset($request->all()['email']) && $request->all()['email'] != '') {
					$std_id = $request->all()['user_id'];
					$user_email = DB::table('users')->where('email', $request->all()['email'])->whereNotIn('id', [$std_id])->get();
					if(count($user_email) > 0) {
						$validator->errors()->add('email', 'Email already exists');
						return redirect()->back()->withErrors($validator)->withInput();
					} else {
						DB::table('users')->where('id', $request->all()['user_id'] )->update(['email' => $request->all()['email'] ]);
					}
				}
				// End edit email

				if($recurring_flag) { 
					DB::table("data_".$request->all()['type_of'])
					->where('user_id', $request->all()['user_id'])
					->update($postData);	
				} else {

					if(Auth::user()->type == "MAIN_AGENTS") {
						$this->updateStudentData($studentsId[0]->id,$request->all());
					} else {
						DB::table("data_".$request->all()['type_of'])
						->where('user_id', $request->all()['user_id'])
						->update(request()->except(['_token','type_of','email','student_id']));
					}				
				} 
				// send notifications to staff,agents,students
				if($pageType == "courses_applied" || $pageType == "courses_applied_new") {
					$this->sendNotifications($studentsId[0]->id);
				}
			} else if($studentsId[0]->id){	
				if($recurring_flag) { 	
					DB::table("data_".$request->all()['type_of'])->insert([
						array_merge($postData, [ 'user_id' => $studentsId[0]->id ])
					]);					
				} else { 
					if(Auth::user()->type == "MAIN_AGENTS") {
						$this->insertStudentData($studentsId[0]->id,$request->all());
					} else {
						DB::table("data_".$request->all()['type_of'])->insert([
							array_merge(request()->except(['_token','type_of','email','student_id']), [ 'user_id' => $studentsId[0]->id ])
						]);
					}
				}
			}
			
			if( $request->all()['user_id']==0){
				\Session::flash('status_success', 'Student Added Successfully');
				}
				else{
					\Session::flash('status_success', 'Updated Successfully');	
				}
			return redirect(config('laraadmin.adminRoute').'/students/'.$studentsId[0]->id."/edit");
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}


	public function sendNotifications($stdId) 
	{
		$studentData = DB::table('users')->select('assigned_to')->addselect('assigned_to_agents')->addselect('name')
					->where('id', $stdId)->get();	
		$stdName = $studentData[0]->name;

		$studentNotify = DB::table('data_personal_information')->select('notify')
					->where('user_id', $stdId)->get();
		$stdNotify = $studentNotify[0]->notify;
		
		$notif_txt = 'There is change in the Courses Applied or Courses Preferred for the student '.$stdName.' that has been done by '.Auth::user()->name.', Please check the details..';
		if($stdNotify == 1) {
		DB::table('notifications')->insert([ //student notification
			'user_id' => $stdId,'comments' => $notif_txt,'notif_dat' => date("Y-m-d H:i:s")
		]);	}	
		
		$std_lead = DB::table('data_student_lead')->select('agent_id')->where('user_id', $stdId)->get();
		$std_agent_id = $stdAgentId = '';
		if(count($std_lead) > 0) {
			$stdAgentId = $std_lead[0]->agent_id;
		}
		if($stdAgentId != '') {
			DB::table('notifications')->insert([ //Agent notification
				'user_id' => $stdAgentId,'comments' => $notif_txt,'notif_dat' => date("Y-m-d H:i:s")
			]);
		}
		$assigned_to = $studentData[0]->assigned_to;
		$assigned_toDoc = $studentData[0]->assigned_to_agents;
		$assignedTo = '';
		if($assigned_to || $assigned_toDoc) {
			$assignedTo = $assigned_to. (($assigned_toDoc == null) ? '' : ','.$assigned_toDoc);
		}
		if($assignedTo != '') {
			/*$assignee = DB::select( DB::raw("SELECT employees.id AS assigneeId,users.staff_designation AS designation FROM users
							LEFT JOIN employees ON users.context_id=employees.id 
							WHERE users.id IN ($assignedTo)") );*/
							$assignee = DB::select( DB::raw("SELECT employees.id AS assigneeId FROM users
							LEFT JOIN employees ON users.context_id=employees.id 
							WHERE users.id IN ($assignedTo)") );
			if(count($assignee) > 0)  {	
	
				foreach($assignee as $result) {
					
					DB::table('notifications')->insert([ //Staff notification
						'user_id' => $result->assigneeId,'comments' => $notif_txt,'notif_dat' => date("Y-m-d H:i:s")
					]);
				}
			}	
		}
		// send notification to admins
		$this->sendNotifyAdmins($notif_txt);		
	}

	public function sendStudentAddNotif($stdId, $stdName, $fromId, $userType, $userName) 
	{	
		$managers = DB::table('users')->select('context_id')->addselect('name')->where('type', 'MANAGER')->get();
		if($userType == "CONSULTANTS") {
			$desig = 'Staff';
			$content = $userName.'('.$desig .') added a new student - '.$stdName.' <a href="'.url(config('laraadmin.adminRoute') . '/students/'.$stdId.'/edit').'">. Click here to edit the student</a>';
			foreach($managers as $key => $result) {
				$admin_name = $result->name;
				$admin_id = $result->context_id;
				if($admin_id != '') {
					//	Send notification to manager
					DB::table('notifications')->insert([
						'user_id' => $admin_id,
						'comments' => $content,
						'from_user_id' => $fromId,'notif_dat' => date("Y-m-d H:i:s")
					]);			
				}
			}

		} else if($userType == "MAIN_AGENTS") {
			$desig = 'Agent';
			$content = $userName.'('.$desig .') added a new student - '.$stdName.' <a href="'.url(config('laraadmin.adminRoute') . '/students/'.$stdId.'/edit').'">. Click here to edit the student</a>';
			foreach($managers as $key => $result) {
				$admin_name = $result->name;
				$admin_id = $result->context_id;
				if($admin_id != '') {
					//	Send notification to manager
					DB::table('notifications')->insert([
						'user_id' => $admin_id,
						'comments' => $content,
						'from_user_id' => $fromId,'notif_dat' => date("Y-m-d H:i:s")
					]);			
				}
			}
			$staffs = DB::table('staff_agents')->select('staff_emp_id')->where('agent_id', $fromId)->get();
			if(isset($staffs) && count($staffs) > 0) {
				foreach($staffs as $key => $result) {
					$staff_id = $result->staff_emp_id;
					if($staff_id != '') {
						//	Send notification to staff
						DB::table('notifications')->insert([
							'user_id' => $staff_id,
							'comments' => $content,
							'from_user_id' => $fromId,'notif_dat' => date("Y-m-d H:i:s")
						]);	
					}
				} 
			}
		}
	}
	
	
	  
	public function insertStudentRole($userId)
	{
		DB::table('role_user')->insert([
			'role_id' => config('constants.STUDENT_ID'),'user_id' => $userId
		]);		
	}

	public function insertStudentData($id,$request_data)
	{ 
		DB::table('data_personal_information')->insert(['user_id' => $id,'first_name' => $request_data['first_name'],'dob' => $request_data['dob'],
				'coountry_code' => $request_data['coountry_code'],'mobile' => $request_data['mobile'],'coountry_code_whatsapp' => $request_data['coountry_code_whatsapp'],'whatsapp' => $request_data['whatsapp'] ]);
		DB::table('data_school')->insert(['user_id' => $id,'end_year' => $request_data['end_year'],'grade' => $request_data['grade'],'hse_end_year' => $request_data['hse_end_year'],'hse_grade' => $request_data['hse_grade'],'UG_end_year' => $request_data['UG_end_year'],'UG_grade' => $request_data['UG_grade'],'ug_university' => $request_data['ug_university'] ]);
		DB::table('data_courses_applied')->insert(['user_id' => $id,'course_country' => $request_data['course_country'],'course_title' => $request_data['course_title'] ]);
	}

	public function updateStudentData($id,$request_data)
	{
		DB::table('data_personal_information')->where('user_id', $id)->update(['first_name' => $request_data['first_name'],'dob' => $request_data['dob'],'coountry_code' => $request_data['coountry_code'],'mobile' => $request_data['mobile'],'coountry_code_whatsapp' => $request_data['coountry_code_whatsapp'],'whatsapp' => $request_data['whatsapp'] ]);

		$check_data = DB::table('data_school')->select('id')->where('user_id', $id)->get();
		if($check_data && $check_data[0]->id != ''){
			DB::table('data_school')->where('user_id', $id)->update(['end_year' => $request_data['end_year'],'grade' => $request_data['grade'],'hse_end_year' => $request_data['hse_end_year'],'hse_grade' => $request_data['hse_grade'],'UG_end_year' => $request_data['UG_end_year'],'UG_grade' => $request_data['UG_grade'],'ug_university' => $request_data['ug_university'] ]);
		} else {
			DB::table('data_school')->insert(['user_id' => $id,'end_year' => $request_data['end_year'],'grade' => $request_data['grade'],'hse_end_year' => $request_data['hse_end_year'],'hse_grade' => $request_data['hse_grade'],'UG_end_year' => $request_data['UG_end_year'],'UG_grade' => $request_data['UG_grade'],'ug_university' => $request_data['ug_university'] ]);
		}

		$check_data_courses = DB::table('data_courses_applied')->select('id')->where('user_id', $id)->get();
		if($check_data_courses && $check_data_courses[0]->id != ''){
			DB::table('data_courses_applied')->where('user_id', $id)->update(['course_country' => $request_data['course_country'],'course_title' => $request_data['course_title']  ]);
		} else {
			DB::table('data_courses_applied')->insert(['user_id' => $id,'course_country' => $request_data['course_country'],'course_title' => $request_data['course_title'] ]);
		}
	}

	  
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{


		// New Code
		if(isset($_GET['agent_id'])){ $agent_id = $_GET['agent_id']; } else {$agent_id = ''; } 
		if(isset($_GET['course_country'])){ $course_country = $_GET['course_country']; } else {$course_country = ''; }
		if(isset($_GET['entry_date'])){ $entry_date = $_GET['entry_date']; } else {$entry_date = ''; } 
		if(isset($_GET['course_university_new'])){ $course_university_new = $_GET['course_university_new']; } else {$course_university_new = ''; } 
		$leadFrom = (isset($_GET['leadFrom']) && $_GET['leadFrom'] != '') ? $_GET['leadFrom'] : '';
		if(isset($_GET['std_status'])){ $std_status = $_GET['std_status']; } else {$std_status = ''; }
		if(isset($_GET['std_counselor'])){ $std_counselor = $_GET['std_counselor']; } else {$std_counselor = ''; }
		if(isset($_GET['std_docstaff'])){ $std_docstaff = $_GET['std_docstaff']; } else {$std_docstaff = ''; }
		$conditions = "";
		$currentUserId = Auth::user()->id;

		if(Auth::user()->type == 'CONSULTANTS') {
			$conditions .= " AND (u.assigned_to = $currentUserId) ";
			// Staff agents 
				/*$staff_agents = DB::table('staff_agents')->select('agent_id')->where('staff_user_id', $currentUserId)->get();
				if(isset($staff_agents) && count($staff_agents) > 0) {
					foreach($staff_agents as $key => $result) {
						$conditions .= " OR (sl.agent_id = $result->agent_id) "; 
					} 
				}*/
			// End Staff agents
		} else if(Auth::user()->type == 'MAIN_AGENTS') {
			$conditions .= " AND (sl.agent_id = $currentUserId) "; 			
		} else if(Auth::user()->type == 'UNIVERSITY_USER') {
			$conditions .= " AND (u.assigned_to_agents = $currentUserId) ";
		}

		// filter condition of agents, entry-date, course-country, leadFrom
		$filterJoins = "";
		if ($agent_id != '') {
			$conditions .= " AND (sl.agent_id = $agent_id) ";
		}
		if ($std_status != '') { 
			$conditions .= " AND (u.user_status = '$std_status') "; 
		}
		if ($std_counselor != '') { 
			$conditions .= " AND (u.assigned_to = $std_counselor) "; 
		}
		if ($std_docstaff != '') { 
			$conditions .= " AND (u.assigned_to_agents = $std_docstaff) "; 
		}
		if($leadFrom != '') {
			$conditions .= " AND (TRIM(sl.lead_from) = '$leadFrom') ";
		}		
		if ($entry_date != '' || $course_country != '' || $course_university_new != '') {
			$filterJoins .= " LEFT JOIN data_courses_applied_new AS cpr ON cpr.user_id = u.id  
							  LEFT JOIN data_courses_applied AS cap ON cap.user_id = u.id ";
			if($course_country != '') { 
				$conditions .=	" AND ( (TRIM(cpr.course_country_new) = '$course_country') OR (cpr.json_data LIKE '%$course_country%') OR (TRIM(cap.course_country) = '$course_country') OR (cap.json_data LIKE '%$course_country%') ) ";
			}
			if($entry_date != '') { 
				$conditions .=	" AND ( (TRIM(cpr.entry_date_new) = '$entry_date') OR (cpr.json_data LIKE '%$entry_date%') OR (TRIM(cap.entry_date) = '$entry_date') OR (cap.json_data LIKE '%$entry_date%') ) ";
			}
			if($course_university_new != '') { 
				$conditions .=	" AND ( (TRIM(cpr.course_university_new) = '$course_university_new') OR (cpr.json_data LIKE '%$course_university_new%')) ";
			}
		}

		$query = "SELECT u.id,u.email,u.created_at,u.assigned_to,u.is_approved,u.assigned_to_agents,u.user_status,
			dp.profile_image,dp.sur_name,dp.first_name,dp.dob,dp.mobile,dp.whatsapp,dp.coountry_code_whatsapp,dp.notify, 
			df.follow_date,df.follow_time,sl.lead_from,sl.agent,sl.lead_type FROM users AS u 
			LEFT JOIN data_personal_information AS dp ON dp.user_id = u.id 
			LEFT JOIN data_student_lead AS sl ON u.id = sl.user_id 
			LEFT JOIN data_follow_up AS df ON u.id = df.user_id
			$filterJoins 
			WHERE u.type = :type AND u.deleted_at IS NULL
			$conditions 
			ORDER BY u.created_at DESC";
		$students = DB::select($query, ['type' => 'STUDENT']);
		


		// End New Code
		/*if(Auth::user()->type == 'CONSULTANTS' || Auth::user()->type == 'MAIN_AGENTS') {
			$students = DB::table('users')
						->select('users.email')->addselect('data_personal_information.first_name')->addselect('data_personal_information.dob')
						->addselect('data_personal_information.sur_name')->addselect('users.id')->addselect('users.assigned_to')->addselect('users.user_status')->addselect('data_personal_information.profile_image')->addselect('data_personal_information.whatsapp')->addselect('data_personal_information.coountry_code_whatsapp')->addselect('data_personal_information.notify')
						->addselect('users.is_approved')->addselect('users.assigned_to_agents')
						->leftJoin('data_personal_information', 'users.id', '=', 'data_personal_information.user_id')
						->where('users.assigned_to', Auth::user()->id)
						->where('users.type', 'STUDENT')->orderBy('users.created_at', 'desc')->get();			
		} else if(Auth::user()->type == 'UNIVERSITY_USER') {
			$students = DB::table('users')
						->select('users.email')->addselect('data_personal_information.first_name')->addselect('data_personal_information.dob')
						->addselect('data_personal_information.sur_name')->addselect('users.id')->addselect('users.assigned_to')->addselect('data_personal_information.profile_image')->addselect('data_personal_information.whatsapp')->addselect('data_personal_information.coountry_code_whatsapp')->addselect('data_personal_information.notify')
						->addselect('users.is_approved')->addselect('users.assigned_to_agents')->addselect('users.user_status')
						->leftJoin('data_personal_information', 'users.id', '=', 'data_personal_information.user_id')
						->where('users.assigned_to_agents', Auth::user()->id)
						->where('users.type', 'STUDENT')->orderBy('users.created_at', 'desc')->get();				
		} else {
			$students = DB::table('users')
						->select('users.email')->addselect('data_personal_information.first_name')->addselect('data_personal_information.dob')
						->addselect('data_personal_information.sur_name')->addselect('users.id')->addselect('users.assigned_to')
						->addselect('users.is_approved')->addselect('users.assigned_to_agents')->addselect('users.user_status')->addselect('data_personal_information.profile_image')->addselect('data_personal_information.whatsapp')->addselect('data_personal_information.coountry_code_whatsapp')->addselect('data_personal_information.notify')
						->leftJoin('data_personal_information', 'users.id', '=', 'data_personal_information.user_id')
						->where('users.type', 'STUDENT')->orderBy('users.created_at', 'desc')->get();
		}*/
		
		$out =  array();
		$out['data'] = array();
		$out['draw'] = count($students);
		$out['recordsTotal'] = count($students);
		$out['recordsFiltered'] = count($students);
		
		foreach($students as $key => $result) {
			
			$studentData = array();
			$studentData[] = $key+1;
			$avatatImg = ($result->profile_image == '') ? 'https://www.gravatar.com/avatar/ef1c169902a351837b6c7f8816cf9b5c.jpg?s=80&d=mm&r=g' : $result->profile_image;
			$studentData[] = '<a href="'.url(config('laraadmin.adminRoute') . '/students/'.$result->id).'"><img src="'.$avatatImg.'" alt=""/><span>'.$result->first_name.' '.$result->sur_name.'</span></a>';
			$studentData[] = $result->email;

			$studentData[] = date('d-M-Y', strtotime($result->created_at));
			if($result->lead_from == 'Agents') { 
				$studentData[] = $result->agent;
			} else { $i=0;
				$studentData[] = $result->lead_from;
			}
			
			// students status
			if($result->user_status == 'APPROVE') {
				$status = '<span  class="mg-status approved">Approved</span>';
			} else if($result->user_status == 'REJECT') {
				$status = '<span mg-status  class="mg-status rejected">Rejected</span>';
			} else {
				$statusStudent = ($result->user_status == '') ? 'Pending' : $result->user_status;
				$status = '<span class="mg-status pending">'.$statusStudent.'</span>';
			}
			$studentData[] = (string)$status;
			
			if($result->assigned_to === null) {
				$assigneeName = $profilePic = ''; $assigneeId = 0;
			} else {
				$assignedTo = $result->assigned_to;
				$assignee = DB::select( DB::raw("SELECT employees.id AS assigneeId,employees.name,employees.profilePic FROM users
								LEFT JOIN employees ON users.context_id=employees.id 
								WHERE users.id=$assignedTo") );

				if($assignee) {
					$assigneeName = $assignee[0]->name;
					$assigneeId = $assignee[0]->assigneeId;
					$profilePic = ($assignee[0]->profilePic == '') ? 'https://www.gravatar.com/avatar/ef1c169902a351837b6c7f8816cf9b5c.jpg?s=80&d=mm&r=g' : $assignee[0]->profilePic;
				}
			}
			if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER"])) {
				if(Auth::user()->type == 'MAIN_AGENTS') {
					$studentData[] = '';
				} else {
					//$studentData[] = ($result->assigned_to != null) ? '<a target="_blank" class="mgt-con mx-assignee" href="'.url(config('laraadmin.adminRoute') . '/student/consultants/'.$assigneeId).'"><img src="'.$profilePic.'" alt="User Image"/><span>'.$assigneeName.'</span></a> ' : '';
					$studentData[] = ($result->assigned_to != null) ? '<a target="_blank" class="mgt-con mx-assignee" href="'.url(config('laraadmin.adminRoute') . '/student/consultants/'.$assigneeId).'"><span>'.$assigneeName.'</span></a> ' : '';				
				}
			}

			if($this->show_action) {
				$output = '';
				$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/students/'.$result->id.'/edit').'" class="btn btn-warning btn-xs editbtn" data-toggle="tooltip" title="Edit" ><i class="fa fa-pencil"></i></a>';
				$output .= '<a target="_blank" href="'.url(config('laraadmin.adminRoute') . '/mailsettings?email='.$result->id.'_'.$result->email).'" data-email="'.$result->email.'" class="btn btn-warning btn-xs mailbtn" data-toggle="tooltip" title="Mail" ><i class="fa fa-envelope"></i></a>';

				if($result->whatsapp != '') {
					$output .= '<a target="_blank" href="https://api.whatsapp.com/send?phone='.$result->coountry_code_whatsapp.$result->whatsapp.'" class="btn btn-warning btn-xs whatsappbtn" data-toggle="tooltip" title="Whatsapp" ><i class="fa fa-whatsapp "></i></a>';
				}
				
				// students assigning
				//if($result->is_approved == null && $result->assigned_to === null) {
					
				if(Auth::user()->type != 'MAIN_AGENTS') {
					
					if(Auth::user()->type == 'SUPER_ADMIN' || Auth::user()->type == 'MANAGER') {
						//if($result->is_approved == null) {
						$output .= '<a title="Assign staff" data-student="'.$result->first_name.'" data-email="'.$result->email.'" data-user="'.$result->id.'" data-notify="'.$result->notify.'" data-toggle="modal" data-target="#AddModal" href="#" class="btn btn-warning btn-xs stud-assign addcbtn" >  <i class="fa fa-user-plus"></i> <span data-toggle="tooltip" title="Assign Staff"></span></a>';	
						//} else if($result->is_approved == "1" && $result->assigned_to_agents == null) {
							//$output .= '<a title="Assign to University users" data-email="'.$result->email.'" data-user="'.$result->id.'" data-toggle="modal" data-target="#universityUserModal"  href="#" class="btn btn-warning btn-xs stud-assign-university addcbtn" >  <i class="fa fa-university "></i> <span data-toggle="tooltip" title="Assign University"></span></a>';	
						//}
					}
					
					if($result->is_approved === null) {
						// student approve form
						//$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.students.approve'], 'method' => 'PUT', 'style'=>'display:inline','onSubmit'=>"return confirm('This action will approve the student?');"]);
						///$output .= '<input type="hidden" value="'.$result->email.'" name="user_email">';
						//$output .= '<input type="hidden" value="'.$result->id.'" name="user_id">';
						//$output .= '<input type="hidden" value="1" name="user_status">';
						
						/*$output .= ' <button title="Change status" data-name="'.$result->first_name.'" data-email="'.$result->email.'" data-id="'.$result->id.'" class="btn btn-warning btn-xs apprbtn" data-toggle="tooltip" title="Change status" type="submit"><i class="fa fa-file"></i></button>';*/

						$output .= '<a title="Change status" data-name="'.$result->first_name.'" data-email="'.$result->email.'" data-id="'.$result->id.'" data-notify="'.$result->notify.'"  data-toggle="modal" data-target="#AddModal-studentStatus" href="#" class="btn btn-warning btn-xs apprbtn" >  <i class="fa fa-file"></i> <span data-toggle="tooltip" title="Change status"></span></a>';

						//$output .= Form::close();
						// student reject form
						/*
						$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.students.approve'], 'method' => 'PUT', 'style'=>'display:inline','onSubmit'=>"return confirm('This action will reject the student?');"]);
						$output .= '<input type="hidden" value="'.$result->email.'" name="user_email">';
						$output .= '<input type="hidden" value="'.$result->id.'" name="user_id">';
						$output .= '<input type="hidden" value="0" name="user_status">';
						$output .= ' <button title="Reject Student" class="btn btn-danger btn-xs rejcbtn" data-toggle="tooltip" title="Reject" type="submit"><i class="fa fa-times"></i></button>';
						$output .= Form::close();
						*/
					}
				}


			if(Auth::user()->type == 'CONSULTANTS') {

					/*$output .= '<a title="Assign staff" data-student="'.$result->first_name.'" data-email="'.$result->email.'" data-user="'.$result->id.'" data-notify="'.$result->notify.'" data-toggle="modal" data-target="#AddModal" href="#" class="btn btn-warning btn-xs stud-assign addcbtn" >  <i class="fa fa-user-plus"></i> <span data-toggle="tooltip" title="Assign Staff"></span></a>';*/	

					$output .= '<a title="Assign University user" data-student="'.$result->first_name.'" data-email="'.$result->email.'" data-user="'.$result->id.'" data-notify="'.$result->notify.'" data-toggle="modal" data-target="#universityUserModal"  href="#" class="btn btn-warning btn-xs stud-assign-university addcbtn" >  <i class="fa fa-university "></i> <span data-toggle="tooltip" title="Assign University User"></span></a>';

					$output .= '<a title="Change next follow up" data-name="'.$result->first_name.'" data-email="'.$result->email.'" data-id="'.$result->id.'" data-notify="'.$result->notify.'"  data-toggle="modal" data-target="#AddModal-studentFollowup" href="#" class="btn btn-warning btn-xs followupbtn" >  <i class="fa fa-calendar"></i> <span data-toggle="tooltip" title="Change next follow up"></span></a>';
			}
				
			if(Auth::user()->type == 'SUPER_ADMIN' || Auth::user()->type == 'MANAGER' || Auth::user()->type == 'MAIN_AGENTS') {
				if($result->user_status != 'Approve') {	
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.students.destroy', $result->id], 'method' => 'delete', 'style'=>'display:inline','onSubmit'=>"return confirm('Are you sure you wish to delete?');"]);
					$output .= ' <button class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete" type="submit"><i class="fa fa-trash"></i></button>';
					$output .= Form::close();
				}
			}


				$studentData[] = (string)$output;
			}

			array_push($out['data'],$studentData);
		}
		
		$out = json_encode($out,true);
		
		return $out;
	}
	
	public function assignStatus(Request $request)
	{
		if(count($request->all()) > 0) {
			$postData = $request->all();
			DB::table('users')->where('id', $postData['student_user_id'])->update([ 'user_status' => $postData['user_status_assigned'] ]);

			
			$studentData = DB::table('users')->select('assigned_to')->addselect('assigned_to_agents')->where('id', $postData['student_user_id'])->get();
			\Session::flash('status_success', 'Application form successfully updated');	

			$std_lead = DB::table('data_student_lead')->select('agent_id')->where('user_id', $postData['student_user_id'])->get();
			$std_agent_id = '';   $agent_context_id = '';
			if(!$std_lead) { $request['agent_email'] = ''; }
			else if($std_lead && $std_lead[0]->agent_id != '') { 
				$std_agent_id = $std_lead[0]->agent_id;
				$mainagent_email = DB::table('users')->select('email')->addselect('name')->addselect('context_id')->where('id', $std_lead[0]->agent_id)->get();
				$request['agent_email'] = $mainagent_email[0]->email;
				$request['agent_name'] = $mainagent_email[0]->name;
				$agent_context_id = $mainagent_email[0]->context_id;
			} else { $request['agent_email'] = ''; $std_agent_id = '';}

			// send notifications to staff,agents,students,admin on status change
			/*$this->statusNotification($postData['student_user_id'],$postData['student_user_name'],$postData['user_status_assigned'],$std_agent_id,Auth::user()->name,$studentData,$postData['student_user_notify']);*/
			$this->statusNotification($postData['student_user_id'],$postData['student_user_name'],$postData['user_status_assigned'],$agent_context_id,Auth::user()->name,$studentData,$postData['student_user_notify']);



			if($postData['student_user_email'] != '') {
				if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
					if($postData['student_user_notify'] == 1) {
					// Mail::send('emails.user_status_changed', ['user' =>$request->all()['student_user_name'], 'status' => $request->all()['user_status_assigned']], function ($m) use ($request) {
					// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
					// 	$m->to($request->all()['student_user_email'])->subject('Zodocs - Application form status');
					// }); 
				}


					$manager_email = DB::table('users')->select('email')->addselect('name')->where('type', 'MANAGER')->get();
					foreach($manager_email as $key => $result) {
						$request['admin_name'] = $result->name;
						$request['admin_email'] = $result->email;
						if($request['admin_email'] != '') {
						//	Send mail to manager
							// Mail::send('emails.user_status_changed_admin', ['admin' =>$request['admin_name'],'user' => $request->all()['student_user_name'],'status' => $request->all()['user_status_assigned']], function ($m) use ($request) {
							// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
							// 	$m->to($request['admin_email'])->subject('Zodocs - Application form status');
							// });
						}
					}

					if($request['agent_email'] != '') {
						// Mail::send('emails.user_status_changed_agent', ['agent' =>$request['agent_name'], 'user' =>$request->all()['student_user_name'], 'status' => $request->all()['user_status_assigned']], function ($m) use ($request) {
						// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
						// 	$m->to($request['agent_email'])->subject('Zodocs - Application form status');
						// });
					}

				} else {
					Log::info("Student status changed: ".$request->all()['student_user_email']);
				}
			}
		} 
		
		return redirect(config('laraadmin.adminRoute') . '/students');
	}

	/* send notifications on user status change */
	public function statusNotification($stdId,$stdName,$stdStatus,$stdAgentId,$userName,$studentData,$studentNotify)
	{ 
		$notif_txt = 'Application form status of '.$stdName.' has been changed to '.$stdStatus.' by '.$userName;
		if($studentNotify == 1) {
			DB::table('notifications')->insert([ //student notification
				'user_id' => $stdId,'comments' => $notif_txt,'notif_dat' => date("Y-m-d H:i:s")
			]);	
		}	
		if($stdAgentId != '') {
			DB::table('notifications')->insert([ //Agent notification
				'user_id' => $stdAgentId,'comments' => $notif_txt,'notif_dat' => date("Y-m-d H:i:s")
			]);
		}
		
		$assigned_to = $studentData[0]->assigned_to;
		$assigned_toDoc = $studentData[0]->assigned_to_agents;
		$assignedStaffs = '';
		if($assigned_to || $assigned_toDoc) {
			$assignedStaffs = $assigned_to. (($assigned_toDoc == null) ? '' : ','.$assigned_toDoc);
		}
		if($assignedStaffs != '') {
			$assignee = DB::select( DB::raw("SELECT employees.id AS assigneeId FROM users
							LEFT JOIN employees ON users.context_id=employees.id 
							WHERE users.id IN ($assignedStaffs)") );
			
			if(count($assignee) > 0)  {	
	
				foreach($assignee as $result) {
					
					DB::table('notifications')->insert([ //Staff notification
						'user_id' => $result->assigneeId,'comments' => $notif_txt,'notif_dat' => date("Y-m-d H:i:s")
					]);
				}
			}	
		}
		// send notification to admins
		$this->sendNotifyAdmins($notif_txt);
	}



	public function assignFollowup(Request $request)
	{
		if(count($request->all()) > 0) {
			$postData = $request->all();
			$follow_date = strtr($postData['follow_date'], '/', '-');
            $next_follow_date = date('M d, Y', strtotime($follow_date));

			$std_agent_id = '';
            $std_lead = DB::table('data_student_lead')->select('agent_id')->where('user_id', $postData['st_user_id'])->get();
            if($std_lead && $std_lead[0]->agent_id != '') {
            	$std_agent_id = $std_lead[0]->agent_id;

            	$mainagent_context_id = DB::table('users')->select('context_id')->where('id', $std_agent_id)->get();
				$std_agent_contextid = $mainagent_context_id[0]->context_id;
            } else {
            	$std_agent_contextid = '';
            }

			//DB::table('data_follow_up')->where('user_id', $postData['st_user_id'])->update([ 'follow_date' => $next_follow_date, 'follow_time' => $postData['follow_time'] ]);

			$std_flp = DB::table('data_follow_up')->select('follow_date')->where('user_id', $postData['st_user_id'])->get();
			if($std_flp && $std_flp[0]->follow_date != '') {
				DB::table('data_follow_up')->where('user_id', $postData['st_user_id'])->update([ 'follow_date' => $next_follow_date, 'follow_time' => $postData['follow_time'] ]);
			} else {
				DB::table('data_follow_up')->insert([ 'user_id' => $postData['st_user_id'], 'follow_date' => $next_follow_date,'follow_time' => $postData['follow_time'] ]);
			}
			
			\Session::flash('status_success', 'Next followup date updated successfully');

			$studentData = DB::table('users')->select('assigned_to')->addselect('assigned_to_agents')->where('id', $postData['st_user_id'])->get();
			
			// send notifications to staff,agents,students,admins
			//$this->followupNotification($postData['st_user_id'],$postData['st_user_name'],$postData['follow_date'],$postData['follow_time'],Auth::user()->name,$std_agent_id,$studentData,$postData['st_user_notify']);
			$this->followupNotification($postData['st_user_id'],$postData['st_user_name'],$postData['follow_date'],$postData['follow_time'],Auth::user()->name,$std_agent_contextid,$studentData,$postData['st_user_notify']);
			
			if($postData['st_user_email'] != '' && env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {

				$postData['followup'] = $postData['follow_date'].' '.$postData['follow_time'];
				//	Send mail of next-follow-up-date
				if($postData['st_user_notify'] == 1) {
				// Mail::send('emails.nextfollowup', ['user' => $request->all()['st_user_name'],'followup' => $postData['followup']], function ($m) use ($postData) {
				// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
				// 	$m->to($postData['st_user_email'])->subject('Scholab - Next Followup date');
				// });	
			}
			} else {
				//Log::info("Student status changed: ".$postData['st_user_email']);
			}
			
		} 
		
		return redirect(config('laraadmin.adminRoute') . '/students');
	}

	/* send  notification on change of follow up date */
	public function followupNotification($stdId,$stdName,$followDate,$followTime,$userName,$stdAgentId,$studentData,$studentNotify)
	{
		$notif_txt = 'Next follow up date of student '.$stdName.' has been set to '.$followDate.' at '.$followTime.' by '.$userName;
		if($studentNotify == 1) {
		DB::table('notifications')->insert([ //student notification
			'user_id' => $stdId,'comments' => $notif_txt,'notif_dat' => date("Y-m-d H:i:s")
		]);	}
		if($stdAgentId != '') {
			DB::table('notifications')->insert([ //Agent notification
				'user_id' => $stdAgentId,'comments' => $notif_txt,'notif_dat' => date("Y-m-d H:i:s")
			]);
		}
		
		$assigned_to = $studentData[0]->assigned_to;
		$assigned_toDoc = $studentData[0]->assigned_to_agents;
		$assignedStaffs = '';
		if($assigned_to || $assigned_toDoc) {
			$assignedStaffs = $assigned_to. (($assigned_toDoc == null) ? '' : ','.$assigned_toDoc);
		}
		if($assignedStaffs != '') {
			$assignee = DB::select( DB::raw("SELECT employees.id AS assigneeId FROM users
							LEFT JOIN employees ON users.context_id=employees.id 
							WHERE users.id IN ($assignedStaffs)") );
			if(count($assignee) > 0)  {	
	
				foreach($assignee as $result) {
					
					DB::table('notifications')->insert([ //Staff notification
						'user_id' => $result->assigneeId,'comments' => $notif_txt,'notif_dat' => date("Y-m-d H:i:s")
					]);
				}
			}	
		}
		// send notification to admins
		$this->sendNotifyAdmins($notif_txt);	
	}
	
	public function update(Request $request)
	{
		if(count($request->all()) > 0) {
			$postData = $request->all();
			$email = $postData['user_email'];
			DB::table('users')->where('id', $postData['user_id'])->update([ 'is_approved' => $postData['user_status'] ]);
		} 
		
		return redirect(config('laraadmin.adminRoute') . '/students');
	}
	
	/**
	 * Remove from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if($id != '') {
			$users = DB::table('users')->find($id);
			if($users) {
				//DB::table('users')->where('id', $id)->update(['deleted_at' => date("Y-m-d H:i:s")]);
				DB::table('users')->where('id', '=', $id)->delete();
			}
		}
		
		return redirect()->route(config('laraadmin.adminRoute') . '.students.index');
	}
	
	public function show($id) 
	{				
		$students = DB::table('users')
					->select('users.email')->addselect('users.user_status')->addselect('data_personal_information.first_name')->addselect('data_personal_information.dob')
					->addselect('data_personal_information.sur_name')->addselect('users.id')->addselect('data_personal_information.middle_name')
					->addselect('data_personal_information.profile_image')->addselect('users.is_approved')
					->addselect('data_follow_up.follow_date')->addselect('data_follow_up.follow_time')
					->leftJoin('data_personal_information', 'users.id', '=', 'data_personal_information.user_id')
					->leftJoin('data_follow_up', 'users.id', '=', 'data_follow_up.user_id')
					->whereNull('users.deleted_at')
					->where('users.id', $id)->get();
					
		$mailDetails = DB::table('mail_settings')
					->select('mail_settings.id')->addselect('mail_settings.mail_content')->addselect('mail_settings.subject')
					->addselect('us.name')->addselect('us.type')->addselect('mail_settings.send_date')
					->leftJoin('users', 'users.id', '=', 'mail_settings.to_user')
					->leftJoin('users AS us', 'us.id', '=', 'mail_settings.from_user')
					->where('mail_settings.is_deleted', 0)
					->where('users.context_id', $id)->orderBy('mail_settings.send_date', 'desc')->get();
					
		$notifications = DB::table('notifications')->select('id')->addselect('notif_dat')->addselect('user_id')
				->addselect('doc_type')->addselect('comments')->addselect('status')->addselect('approval_text')
				->where('is_deleted', 0)->where('user_id', $id)->orderBy('notif_dat', 'desc')->get();

		$notifData = DB::table('notifications')
							->select('notifications.id')->addselect('notifications.comments')->addselect('notifications.notif_dat')->addselect('users.name')
							->leftJoin('users', 'users.id', '=', 'notifications.from_user_id')
							->where('notifications.is_deleted', 0)
							->whereNotNull('notifications.notif_dat')
							->where('notifications.user_id', $id)->orderBy('notifications.notif_dat', 'desc')->get();
	
		return view('la.students.show', [
			'no_header' => true,
			'no_padding' => "no-padding",
			'students' =>!empty($students)?$students[0]:$students,
			'mailDetails' => $mailDetails, 'notifications' => $notifications,
			'notifDetails' => $notifData
		]);
	}
	
	public function studentData($id,$type)
	{
		$st_form = false;
		switch($type) {
			case "tab_1":
				$table = 'data_personal_information';
				break;
			case "tab_2":
				$table = 'data_courses_applied';
				break;
			case "tab_3":
				$table = 'data_school';
				break;
			case "tab_4":
				$table = 'data_eng_lang_proficiency';
				break;
			case "tab_5":
				$table = 'data_employement_history';
				break;
			case "tab_6":
				$table = 'data_further_details';
				break;
			case "tab_7":
				$table = 'data_dependant_det';
				break;
			case "tab_8":
				$table = 'data_parent_det';
				break;
			case "tab_9":
				$table = 'data_immigration_history';
				break;
			case "tab_10":
				$table = 'data_personal_stat';
				break;
			case "tab_11":
				$table = 'data_courses_applied_new';
				break;
			case "tab_12":
				$table = 'data_student_lead';
				break;
			case "tab_13":
				$table = 'data_follow_up';
				break;
			case "std_form":
				$st_form = true;
				break;
			default:
				break;
		}

		if($st_form){
			$data = DB::table('users')
					->select('users.id')->addselect('users.email')->addselect('dp.first_name')->addselect('dp.dob')->addselect('dp.coountry_code')
					->addselect('dp.mobile')->addselect('dp.coountry_code_whatsapp')->addselect('dp.whatsapp')->addselect('dp.current_status')->addselect('ds.end_year')->addselect('ds.grade')->addselect('ds.hse_end_year')->addselect('ds.hse_grade')->addselect('ds.UG_end_year')->addselect('ds.UG_grade')->addselect('ds.ug_university')->addselect('dc.course_country')->addselect('dc.course_title')
					->leftJoin('data_personal_information AS dp', 'dp.user_id', '=', 'users.id')
					->leftJoin('data_school AS ds', 'ds.user_id', '=', 'users.id')
					->leftJoin('data_courses_applied AS dc', 'dc.user_id', '=', 'users.id')
					->where('users.id', $id)->get();
		} else {
			$where = $table.'.user_id';
			$data = DB::table($table)->where($where, $id)->get(); //print_r($data); exit;
		}

		$details = (count($data) > 0) ? $data[0] : $data;

		return json_encode($details,true);
	}
	public function emailValidation()
	{
		$email = DB::table('users')
		->where('email', $this->emailId)
		->get();
				// ->whereNotIn('id', [$this->userId])
			
				
		return count($email);
	}
	
	public function approveDocument(Request $request)
	{
		if(count($request->all()) > 0) {
			$id = $request->all()['id'];
			$type = $request->all()['type'];
			$status = $request->all()['statusvalue'];
			$comments = $request->all()['comment'];
			
			switch($type){
				case "passport-front":
					$table = 'data_passport_details';  $column = "image_front_approved";
					break;
				case "passport-back":
					$table = 'data_passport_details';  $column = "image_back_approved";
					break;
				case "sslc-doc":
					$table = 'data_school';  $column = "sslc_doc_approved";
					break;
				case "hse-doc":
					$table = 'data_higher';  $column = "hsc_doc_approved";
					break;
				case "degree-doc":
					$table = 'data_bachelors';  $column = "degree_doc_approved";
					break;
				case "master-doc":
					$table = 'data_masters';  $column = "master_doc_approved";
					break;
				case "letter-recommend-doc":
					$table = 'data_masters';  $column = "recommd_file_approved";
					break;
				case "statement-purpose-doc":
					$table = 'data_masters';  $column = "purpose_file_approved";
					break;
				case "ielts-doc":
					$table = 'data_ielts';  $column = "ielts_doc";
					break;
				case "misc-exper-doc":
					$table = 'data_misc';  $column = "expr_cert_approved";
					break;
				case "invoice-doc":
					$table = 'data_aplctndetails';  $column = "invoice_doc_approved";
					break;	
				case "flight-doc":
					$table = 'data_aplctndetails';  $column = "flight_doc_approved";
					break;
				case "cas-file-doc":
					$table = 'data_aplctndetails';  $column = "cas_doc_approved";
					break;
				case "tb-report-doc":
					$table = 'data_aplctndetails';  $column = "tb_report_approved";
					break;
				case "maintanence-fund-doc":
					$table = 'data_aplctndetails';  $column = "maintainace_doc_approved";
					break;					
				default:
					break;
			}
			
			$approvalText = $this->docapprovalText($type,$status);

			DB::table($table)->where('user_id', $id)->update([$column => $status]);
			DB::table('notifications')->insert(['user_id' => $id,'user_type' => 'STUDENT','doc_type' => $type,
				'comments' => $comments,'status' => $status,'approval_text' => $approvalText]);
			
			return "SUCCESS"; 
		} else {
			return "FAILED"; 
		}
	}
	
	public function docapprovalText($type,$status)
	{
		$statusText = ($status == '1') ? 'APPROVED' : 'REJECTED';
		// documents types array
		$docarray = array('passport-front' => 'Passport front image', 'passport-back' => 'Passport back image',
				'sslc-doc' => 'School documents', 'hse-doc' => 'Higher secondary documents','degree-doc' => 'Degree documents',
				'master-doc' => 'Master degree documents','letter-recommend-doc' => 'Letter of Recommendation',
				'statement-purpose-doc' => 'Statement of purpose','ielts-doc' => 'IELTS documents','misc-exper-doc' => 'Experience certificates',
				'invoice-doc' => 'Invoice document','flight-doc' => 'Flight ticket','cas-file-doc' => 'CAS documents',
				'tb-report-doc' => 'TB report documents','maintanence-fund-doc' => 'Maintanence fund document'
			);
		$returnText = "Your ".$docarray[$type]." has been ".$statusText;
		
		return $returnText;
	}
	
	public function updateRead($id)
	{
		DB::table('notifications')->where('id', $id)->update(['is_read' => 1]);
	}
	
	/**
	 * delete notifications
	 *
	 * @param  int  $id
	 * @param  int  studentId
	*/
	public function deleteMail($id,$studentId)
	{
		DB::table('notifications')->where('id', $id)->update(['is_deleted' => 1]);
		\Session::flash('success_notif_delete', 'Notification is deleted successfully');
		
		return redirect(config('laraadmin.adminRoute') . '/students/'.$studentId.'#tab-notifications');
	}
	
	public function isUserExist($table,$user_id)
	{
		$user = DB::table($table)
				->where('user_id', [$user_id])
				->get();
				
		return count($user);
	}
	
	public function assignConsultant(Request $request)
	{
		if(count($request->all()) > 0) {
			$postData = $request->all();
			DB::table('users')->where('id', $postData['assign_user_id'])->update([ 'assigned_to' => $postData['assigned_to'] ]);
			\Session::flash('status_success', 'Student has been assigned to a Staff');

			$consultant_email = DB::table('users')->select('email')->addselect('name')->where('id', $postData['assigned_to'])->get();
			$request['staff_email'] = $consultant_email[0]->email; 
			$staffName = $consultant_email[0]->name; 

			$staffs = DB::table('users')->select('context_id')->where('id', $postData['assigned_to'])->get();
			if($staffs && $staffs[0]->context_id != '') {
				$staffId = $staffs[0]->context_id;
			} else {
				$staffId = '';
			}

			$agent_id ='';
			$std_lead = DB::table('data_student_lead')->select('agent_id')->where('user_id', $postData['assign_user_id'])->get();
			if(!$std_lead) { $request['agent_email'] = ''; }
			else if($std_lead && $std_lead[0]->agent_id != '') { 
				$mainagent_email = DB::table('users')->select('email')->addselect('name')->where('id', $std_lead[0]->agent_id)->get();
				$request['agent_email'] = $mainagent_email[0]->email;
				$request['agent_name'] = $mainagent_email[0]->name;
				$request['staff_type'] = 'Counselor';
				$agent_id = $std_lead[0]->agent_id;
			} else { $request['agent_email'] = '';  }

			// send mail
			if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
					//	Send mail of assignees to student
					if($postData['stud_notify'] == 1) {
					// Mail::send('emails.assigne_stud', ['user' => $request->all()['stud_name'],'staff' => $request->all()['staff_name'],'phone' => $request->all()['staff_phone']], function ($m) use ($request) {
					// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
					// 	$m->to($request->all()['assign_email'])->subject('Scholab - Student Assigned');
					// });	
				}
					//	Send mail to assignees
					// Mail::send('emails.assign_staff', ['user' => $request->all()['stud_name'],'staff' => $request->all()['staff_name']], function ($m) use ($request) {
					// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
					// 	$m->to($request['staff_email'])->subject('Scholab - Student Assigned');
					// });
					if($request['agent_email'] != '') {
						//	Send mail to agent
						// Mail::send('emails.assign_agent', ['agent' => $request['agent_name'],'stafftype' => $request['staff_type'],'user' => $request->all()['stud_name'],'staff' => $request->all()['staff_name']], function ($m) use ($request) {
						// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
						// 	$m->to($request['agent_email'])->subject('Scholab - Student Assigned');
						// });
					}
					$manager_email = DB::table('users')->select('email')->addselect('name')->where('type', 'MANAGER')->get();
					foreach($manager_email as $key => $result) {
						$request['admin_name'] = $result->name;
						$request['admin_email'] = $result->email;
						if($request['admin_email'] != '') {
						//	Send mail to manager
							// Mail::send('emails.assign_admin', ['admin' => $request['admin_name'],'stafftype' => $request['staff_type'],'user' => $request->all()['stud_name'],'staff' => $request->all()['staff_name']], function ($m) use ($request) {
							// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
							// 	$m->to($request['admin_email'])->subject('Scholab - Student Assigned');
							// });
						}
					}
			}
			// Send notification when assigning staff to student
			$this->sendAssignNotification($postData['assign_user_id'],$request->all()['stud_name'],Auth::user()->id,Auth::user()->type,Auth::user()->name,$staffId,$staffName,$agent_id);
			// End send notification when assigning staff to student

		} 
		
		return redirect(config('laraadmin.adminRoute') . '/students');
	}

	public function sendAssignNotification($stdId,$stdName,$fromId,$userType,$userName,$staffId,$staffName,$agent_user_id) 
	{	
		// Get agent emp id
		$agents = DB::table('users')->select('context_id')->where('id', $agent_user_id)->get();
		if($agents && $agents[0]->context_id != '') {
			$agent_id = $agents[0]->context_id;
		} else {
			$agent_id = '';
		}
		// Get agent emp id

		// Admin assign student to staff
			// Send notification to staff
			if($staffId != '') {
				$content = 'Student - ' .$stdName.' has been assigned to you by '.$userName. '(Admin)';
				DB::table('notifications')->insert([
							'user_id' => $staffId,
							'comments' => $content,
							'from_user_id' => $fromId,'notif_dat' => date("Y-m-d H:i:s")
						]);
			}
			// Send notification to agent
			if($agent_id != '') {
				$content = 'Student - ' .$stdName.' has been assigned to staff - '.$staffName.' by '.$userName. '(Admin)';
				DB::table('notifications')->insert([
							'user_id' => $agent_id,
							'comments' => $content,
							'from_user_id' => $fromId,'notif_dat' => date("Y-m-d H:i:s")
						]);
			}
			// Send notification to student
			if($stdId != '') {
				$content = 'You are assigned to a staff - ' .$staffName.' by '.$userName. '(Admin)';
				DB::table('notifications')->insert([
							'user_id' => $stdId,
							'comments' => $content,
							'from_user_id' => $fromId,'notif_dat' => date("Y-m-d H:i:s")
						]);
			}
			// Send notification to managers
			$managers = DB::table('users')->select('context_id')->addselect('name')->where('type', 'MANAGER')->get();
			$content = 'Student - ' .$stdName.' has been assigned to staff - '.$staffName.' by '.$userName. '(Admin)';
			foreach($managers as $key => $result) {
				$admin_name = $result->name;
				$admin_id = $result->context_id;
				if($admin_id != '') {
					//	Send notification to manager
					DB::table('notifications')->insert([
						'user_id' => $admin_id,
						'comments' => $content,
						'from_user_id' => $fromId,'notif_dat' => date("Y-m-d H:i:s")
					]);			
				}
			}
	}
	
	public function assignUniversityUser(Request $request)
	{
		if(count($request->all()) > 0) {
			$postData = $request->all();
			DB::table('users')->where('id', $postData['assign_user_id'])->update([ 'assigned_to_agents' => $postData['assigned_to'] ]);
			\Session::flash('status_success', 'Student has been assigned to a University User');



			$consultant_email = DB::table('users')->select('email')->addselect('name')->where('id', $postData['assigned_to'])->get();
			$request['staff_email'] = $consultant_email[0]->email; 
			$staffName = $consultant_email[0]->name; 

			$staffs = DB::table('users')->select('context_id')->where('id', $postData['assigned_to'])->get();
			if($staffs && $staffs[0]->context_id != '') {
				$staffId = $staffs[0]->context_id;
			} else {
				$staffId = '';
			}

			$agent_id ='';
			$std_lead = DB::table('data_student_lead')->select('agent_id')->where('user_id', $postData['assign_user_id'])->get();
			if(!$std_lead) { $request['agent_email'] = ''; }
			else if($std_lead && $std_lead[0]->agent_id != '') { 
				$mainagent_email = DB::table('users')->select('email')->addselect('name')->where('id', $std_lead[0]->agent_id)->get();
				$request['agent_email'] = $mainagent_email[0]->email;
				$request['agent_name'] = $mainagent_email[0]->name;
				$request['staff_type'] = 'University User';
				$agent_id = $std_lead[0]->agent_id;
			} else { $request['agent_email'] = '';  }

			// send mail
			if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
					//	Send mail of assignees to student
					if($postData['stud_notifyy'] == 1) {
					// Mail::send('emails.assigne_stud_unv', ['user' => $request->all()['stud_namee'],'staff' => $request->all()['staff_namee'],'phone' => $request->all()['staff_phonee']], function ($m) use ($request) {
					// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
					// 	$m->to($request->all()['assign_email'])->subject('Scholab - Student Assigned');
					// });	
				}
					//	Send mail to assignees
					// Mail::send('emails.assign_staff_unv', ['user' => $request->all()['stud_namee'],'staff' => $request->all()['staff_namee']], function ($m) use ($request) {
					// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
					// 	$m->to($request['staff_email'])->subject('Scholab - Student Assigned');
					// });
					if($request['agent_email'] != '') {
						//	Send mail to agent
						// Mail::send('emails.assign_agent_unv', ['agent' => $request['agent_name'],'stafftype' => $request['staff_type'],'user' => $request->all()['stud_namee'],'staff' => $request->all()['staff_namee']], function ($m) use ($request) {
						// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
						// 	$m->to($request['agent_email'])->subject('Scholab - Student Assigned');
						// });
					}
					$manager_email = DB::table('users')->select('email')->addselect('name')->where('type', 'MANAGER')->get();
					foreach($manager_email as $key => $result) {
						$request['admin_name'] = $result->name;
						$request['admin_email'] = $result->email;
						if($request['admin_email'] != '') {
						//	Send mail to manager
							// Mail::send('emails.assign_admin_unv', ['admin' => $request['admin_name'],'stafftype' => $request['staff_type'],'user' => $request->all()['stud_namee'],'staff' => $request->all()['staff_namee']], function ($m) use ($request) {
							// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
							// 	$m->to($request['admin_email'])->subject('Scholab - Student Assigned');
							// });
						}
					}
			}
			// Send notification when assigning staff to student
			$this->sendAssignNotificationUniversity($postData['assign_user_id'],$request->all()['stud_namee'],Auth::user()->id,Auth::user()->type,Auth::user()->name,$staffId,$staffName,$agent_id);
			// End send notification when assigning staff to student

		} 
		
		return redirect(config('laraadmin.adminRoute') . '/students');
	}


	public function sendAssignNotificationUniversity($stdId,$stdName,$fromId,$userType,$userName,$staffId,$staffName,$agent_user_id) 
	{	
		// Get agent emp id
		$agents = DB::table('users')->select('context_id')->where('id', $agent_user_id)->get();
		if($agents && $agents[0]->context_id != '') {
			$agent_id = $agents[0]->context_id;
		} else {
			$agent_id = '';
		}
		// Get agent emp id

		// Admin assign student to staff
			// Send notification to staff
			if($staffId != '') {
				$content = 'Student - ' .$stdName.' has been assigned to you by '.$userName. '(Staff)';
				DB::table('notifications')->insert([
							'user_id' => $staffId,
							'comments' => $content,
							'from_user_id' => $fromId,'notif_dat' => date("Y-m-d H:i:s")
						]);
			}
			// Send notification to agent
			if($agent_id != '') {
				$content = 'Student - ' .$stdName.' has been assigned to university user - '.$staffName.' by '.$userName. '(Staff)';
				DB::table('notifications')->insert([
							'user_id' => $agent_id,
							'comments' => $content,
							'from_user_id' => $fromId,'notif_dat' => date("Y-m-d H:i:s")
						]);
			}
			// Send notification to student
			if($stdId != '') {
				$content = 'You are assigned to a university user - ' .$staffName.' by '.$userName. '(Staff)';
				DB::table('notifications')->insert([
							'user_id' => $stdId,
							'comments' => $content,
							'from_user_id' => $fromId,'notif_dat' => date("Y-m-d H:i:s")
						]);
			}
			// Send notification to managers
			$managers = DB::table('users')->select('context_id')->addselect('name')->where('type', 'MANAGER')->get();
			$content = 'Student - ' .$stdName.' has been assigned to university user - '.$staffName.' by '.$userName. '(Staff)';
			foreach($managers as $key => $result) {
				$admin_name = $result->name;
				$admin_id = $result->context_id;
				if($admin_id != '') {
					//	Send notification to manager
					DB::table('notifications')->insert([
						'user_id' => $admin_id,
						'comments' => $content,
						'from_user_id' => $fromId,'notif_dat' => date("Y-m-d H:i:s")
					]);			
				}
			}
	}

	
	/**
     * Change Password
     *
     * @return
     */
	public function change_password($id, Request $request) {
		
		$validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
			'password_confirmation' => 'required|min:6|same:password'
        ]);
		
		if ($validator->fails()) {
			return \Redirect::to(config('laraadmin.adminRoute') . '/students/'.$id.'#tab-account-settings')->withErrors($validator);
		}
		$user = User::where("id", $id)->first();
		$user->password = bcrypt($request->password);
		$user->save();
		
		\Session::flash('success_message', 'Password is successfully changed');
		
		// Send mail to User his new Password
		if(env('MAIL_USERNAME') != null && env('MAIL_USERNAME') != "null" && env('MAIL_USERNAME') != "") {
			// Send mail to User his new Password
			// Mail::send('emails.send_login_cred_change', ['user' => $user, 'password' => $request->password], function ($m) use ($user) {
			// 	$m->from(LAConfigs::getByKey('default_email'), LAConfigs::getByKey('sitename'));
			// 	$m->to($user->email, $user->name)->subject(LAConfigs::getByKey('sitename').' - Login Credentials changed');
			// });
		} else {
			Log::info("User change_password: username: ".$user->email." Password: ".$request->password);
		}
		
		return redirect(config('laraadmin.adminRoute') . '/students/'.$id.'#tab-account-settings');
	}

	/* send notification to admins */
	public function sendNotifyAdmins($notif_txt)
	{
		//$managers = DB::table('users')->select('id')->addselect('name')->where('type', 'MANAGER')->get();
		$managers = DB::table('users')->select('context_id')->addselect('name')->where('type', 'MANAGER')->get();
		if(count($managers) > 0) {
			foreach($managers as $key => $result) {
				$admin_name = $result->name;
				//$admin_id = $result->id;
				$admin_id = $result->context_id;
				if($admin_id != '') {
					//	Send notification to admin
					DB::table('notifications')->insert([
						'user_id' => $admin_id,
						'comments' => $notif_txt,'notif_dat' => date("Y-m-d H:i:s")
					]);			
				}
			}
		}		
	}
}
