<div id="tab_6" class="tab-pane " >
        
<div class="content-head">
                                                <h4>Further Details</h4>
                                                <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                                </div>

                                
            <div class="pane">
				<div class="academic">
                 <div class="mgv-field">
                        <div class="name">
                        How Did You Hear About Us? 
                        </div>
                        <div class="value" id="aboutUs"></div>
                 </div>
                                   
                 <div class="mgv-field">
                        <div class="name">
                        Next of Kin <small>(Name and contact no.of family or friend to be contacted in emergency) </small>
                        </div>
                        <div class="value">
							<span id="nxt_kin_name"></span><br>
                            <span id="nxt_kin_country_code"></span> - <span id="nxt_kin_phone"></span>
                        </div>
                 </div>
                 
                 <div class="mgv-field">
                        <div class="name">
                        Skype ID 
                        </div>
                        <div class="value" id="skype_id"></div>
                 </div>
                 
                 
                 <div class="mgv-field">
                        <div class="name">
                         Skype profile screen shot 
                        </div>
						<!--
                        <div class="value"><img src="../../la-assets/img/Skype-Name1.jpg" alt=""></div>
						-->
                        <div class="value uploaded-file" id="skpe_screenshot" data-type="file"></div>
                 </div>                  
                 <div class="mgv-field">
                        <div class="name">
                        Country of permanent residence  
                        </div>
                        <div class="value" id="country_permanent"></div>
                 </div>                 
                 <div class="mgv-field">
                        <div class="name">
                        Country of birth  
                        </div>
                        <div class="value" id="country_residence"></div>
                 </div>                 
                 <div class="mgv-field">
                        <div class="name">
                        Place of birth  
                        </div>
                        <div class="value" id="birth_place"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                        Nationality  
                        </div>
                        <div class="value" id="Nationality"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                        Nationality  
                        </div>
                        <div class="value" id="prev_study_sector"></div>
                 </div> 				 
                 <div class="mgv-field">
                        <div class="name">
                        Have you also made an application to any University through UCAS?
                        </div>
                        <div class="value" id="ucas_application" data-type="radio"></div>
                 </div> 
                 <div class="mgv-field">
                        <div class="name">
                        UCAS ID
                        </div>
                        <div class="value" id="ucas_id"></div>
                 </div> 				 
                 <div class="mgv-field">
                        <div class="name">
                        Have you lived at any other address in past 2 years? 
                        </div>
                        <div class="value" id="lived_other_address" data-type="radio"></div>
                 </div>        
				<div id="lived_other_address_wrapper" class="hide">
                 <div class="mgv-field">
                        <div class="name">
                         Address above mentioned
                        </div>
                        <div class="value">
							<span id="street_address"></span><br>
							<span id="street_address_line"></span><br>
							<span id="city_other_address"></span><br>
							<span id="state_other_address"></span><br>
							<span id="zip_code_other_address"></span>
                        </div>
                 </div>                
                 <div class="mgv-field">
                        <div class="name">
                        Country
                        </div>
                        <div class="value" id="country_other_address"></div>
                 </div>                
                 <div class="mgv-field">
                        <div class="name">
                        When did you start living at this address? 
                        </div>
                        <div class="value" id="living_addr_start_date"></div>
                 </div> 
                 <div class="mgv-field">
                        <div class="name">
                        When did you stop living at this address?
                        </div>
                        <div class="value" id="living_addr_stop_date"></div>
                 </div>
				</div>
                 <div class="mgv-field">
                        <div class="name">
                        Relationship Status 
                        </div>
                        <div class="value" id="relatiion_status"></div>
                 </div> 
				<div id="spouse-details-wrapper" class="hide">
                 <div class="mgv-field">
                        <div class="name">
                        Name of spouse 
                        </div>
                        <div class="value">
							<span id="spouse_prefix"></span>&nbsp;
							<span id="spouse_firstname"></span>&nbsp;
							<span id="spouse_middlename"></span>&nbsp;
							<span id="spouse_familyname"></span>
						</div>
                 </div>                
                 <div class="mgv-field">
                        <div class="name">
                        Date Of Birth of spouse  
                        </div>
                        <div class="value" id="spouse_birth_date"></div>
                 </div>                 
                 <div class="mgv-field">
                        <div class="name">
                        Do they currently live with you? 
                        </div>
                        <div class="value" id="current_live_status"></div>
                 </div>  
				<div id="spouse-adrress-details-wrapper" class="hide">
                 <div class="mgv-field">
                        <div class="name">
                        Spouse Address
                        </div>
                        <div class="value">
                                <span id="spouse_current_addr"></span><br>
                                <span id="spouse_current_addr_line"></span><br>
                                <span id="spouse_current_city"></span><br>
                                <span id="spouse_current_state"></span><br>
                                <span id="spouse_current_zipcode"></span>
								<span id="spouse_current_country"></span>
                        </div>
                 </div>  
				</div>
                 <div class="mgv-field">
                        <div class="name">
                        Passport number of spouse 
                        </div>
                        <div class="value" id="spouse_passport_no"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                        Will they be travelling with you to the UK?
                        </div>
                        <div class="value" id="spouse_travel_uk"></div>
                 </div> 				 
                 <div class="mgv-field">
                        <div class="name">
                        Passport of spouse 
                        </div>
						<!--
                        <div class="value">
							<img src="../../la-assets/img/Skype-Name1.jpg" alt="">
                        </div>
						-->
						<div class="value uploaded-file" id="spouse_passport" data-type="file"></div>
                 </div>
				</div>
			</div>
            </div>             
</div>

               