@extends("la.layouts.app")

@section("contentheader_title", "Report")
@section("contentheader_description", "Student Report")
@section("section", "Report")
@section("sub_section", "Student")
@section("htmlheader_title", "Student Report")

@section("headerElems")

@endsection

@section("main-content")



@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('status_success'))
	<div class="alert alert-success">{{ session('status_success') }}</div>
@endif





<!--  Agent filter 11111 -->   
@if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER","CONSULTANTS","MAIN_AGENTS"]))
<div class="reportfilter">
	<div class="row">
		<div class="col-md-9">
		<div class="clearfix">
	@if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER","CONSULTANTS"]))

	<div class="col-md-4">
		<label for="start_date">Start Date</label>
		<input class="form-control dateclass placeholderclass" placeholder="Select Date" id="from_date" name="from_date" type="date" value="" required="true"   max="<?= date('Y-m-d'); ?>" onClick="$(this).removeClass('placeholderclass')">
	</div>
	<div class="col-md-4">
		<label for="end_date">End Date</label>
		<input class="form-control dateclass placeholderclass" placeholder="Select Date" id="to_date" name="to_date" type="date" value="" required="true"   max="<?= date('Y-m-d'); ?>"  onClick="$(this).removeClass('placeholderclass')">
	</div>

	<div class="col-md-4">	
		<div class="form-group">
			<!--<select name="agent_id" id="agent_id" class="form-control"> -->
			<select name="agent_id" id="agent_id" class="searchSelect">
				<option value="">Select Agent</option>
				<?php
					foreach($agents as $agentsData) {
						echo '<option id="sale_agnt" value="'.$agentsData->id.'">'.$agentsData->name.'</option>';
					}
				?>
			</select>
		</div>
	</div>
	@else
		<input type="hidden" name="agent_id" id="agent_id">
	@endif
	<div class="col-md-4">	
		<div class="form-group">
			<select name="course_country" id="course_country" class="searchSelect">
				<option value="">Select Country</option>
				<?php
					foreach($countries as $countriesData) {
						echo '<option value="'.$countriesData->country.'">'.$countriesData->country.'</option>';
					}
				?>
			</select>
		</div>
	</div>

	<?php 
		$curr_yr = date('Y');
		$curr_yr2 = date('Y', strtotime('-2 year'));
		$nxt_yr = date('Y', strtotime('+1 year'));
		$nxt_yr2 = date('Y', strtotime('+2 year'));
		$curr_mon = date('m');
    	$months = array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
	?>
	<div class="col-md-4">	
		<div class="form-group">
			<select class="searchSelect" name="entry_date" id="entry_date">
				<option value="">Select Intake</option>
				<?php
					for($i=$curr_yr2; $i<=$nxt_yr2;$i++) {
						foreach ($months as $key => $value) { ?>
							<option value="<?php echo $value.' '.$i; ?>"> <?php echo $value.' '.$i; ?></option>
						<?php	}
					}
				?>
			</select>
		</div>
	</div>

	<div class="col-md-4">	
		<div class="form-group">
			<select class="searchSelect" name="course_university_new" id="course_university_new">
				<option value="">Select University</option>
				<?php
				foreach($university as $universityData) {
					echo '<option value="'.$universityData->university.'">'.$universityData->university.'</option>';
				}
				?>
			</select>
		</div>
	</div>

	<div class="col-md-4">	
		<div class="form-group">
			<select class="searchSelect" name="lead_from" id="lead_from"> 
				<option value="">Select Lead Type</option>
                <option value="Agents"> Agents </option>
                <option value="Facebook"> Facebook </option>
                <option value="Instagram"> Instagram </option>
              	<option value="Google"> Google </option>
                <option value="Email"> Email </option>
                <option value="Help Desk"> Help Desk </option>
                <option value="Reference"> Reference </option>
                <option value="Walk In"> Walk In </option>
                <option value="Personal"> Personal </option>
			</select>
		</div>
	</div>

	<div class="col-md-4">	
		<div class="form-group">
			<select class="searchSelect" name="std_status" id="std_status">
				<option value="">Select Status</option>
				<?php
				foreach($stdstatus as $stdstatusData) {
					echo '<option value="'.$stdstatusData->student_status.'">'.$stdstatusData->student_status.'</option>';
				}
				?>
			</select>
		</div>
	</div>

	@if(Auth::user()->type != 'CONSULTANTS')
	<div class="col-md-4">	
		<div class="form-group">
			<select name="std_counselor" id="std_counselor" class="searchSelect">
				<option value="">Select Staff</option>
				<?php
					foreach($stdcounselor as $stdcounselorData) {
						echo '<option id="stdcouslr" value="'.$stdcounselorData->id.'">'.$stdcounselorData->name.'</option>';
					}
				?>
			</select>
		</div>
	</div>
	@else
		<input type="hidden" name="std_counselor" id="std_counselor">
	@endif

	@if(Auth::user()->type != 'UNIVERSITY_USER')
	<div class="col-md-4">	
		<div class="form-group">
			<select name="std_docstaff" id="std_docstaff" class="searchSelect">
				<option value="">Select University User</option>
				<?php
					foreach($stddocumentation as $stddocumentationData) {
						echo '<option id="stddocs" value="'.$stddocumentationData->id.'">'.$stddocumentationData->name.'</option>';
					}
				?>
			</select>
		</div>
	</div>
	@else
		<input type="hidden" name="std_docstaff" id="std_docstaff">
	@endif

</div>
		</div>
	<div class="col-md-3">
		<div class="col-md-8 pl0"><div class="form-group">
			<button class="btn btn-success btn-sm pull-right searchbtn"><span class="fa fa-search"></span> Search</button>
		</div>
    	</div>
    	<div class="col-md-4 pl0"><div class="form-group">
			<button class="btn btn-success btn-sm pull-right clearbtn"><span class="fa fa-times"></span> Clear</button>
		</div>
		</div>
	</div>
	</div>
</div>
<br>
@endif
<!--  Agent filter ends -->





<!-- <div class="box box-success">
	<div class="box-body"> -->
		<table id="mx-sudents-lists" class="table table-hover mg-table mgt2 display nowrap">
			<thead>
			<tr class="success">
				<th>No:</th>
				<th>Name</th>
				<th>Email</th>
				@if(in_array(Auth::user()->type, ["SUPER_ADMIN","MANAGER"]))
					<th>Assignee</th>
				@endif
				<th>Status</th>
			</tr>
			</thead>
			<tbody></tbody>
		</table>
	<!-- </div>
</div> -->

<!-- common modals -->
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Assign a consultant</h4>
			</div>
			<form action="{{ url(config('laraadmin.adminRoute') . '/students_assign') }}" id="assign-stud-form" method="post">
			{{ csrf_field() }}
			<div class="modal-body">
				<div class="box-body">					
					<?php
					if(count($consultants) > 0) {
					?>
						<input type="hidden" value="" name="assign_user_id" id="assign_user_id">
						<input type="hidden" value="" name="assign_email" id="assign_email">

						
						<input type="hidden" value="" name="stud_name" id="stud_name">
						<input type="hidden" value="" name="staff_name" id="staff_name">
						<input type="hidden" value="" name="staff_phone" id="staff_phone">
						<input type="hidden" value="" name="stud_notify" id="stud_notify">

						<div class="form-group">
							<label for="assigned_to">Select consultant * :</label>
							<select class="form-control stud-assign-select" name="assigned_to" required>
								<option value="">Select consultant</option>
								<?php
								foreach($consultants as $cons) {
								?>
									<option value="<?php echo $cons->id; ?>" data-name="<?php echo $cons->name; ?>" data-phone="<?php echo $cons->mobile; ?>"> <?php echo $cons->name; ?></option>
								<?php
								}
								?>
							</select>
						</div>
					<?php
					} else {
						echo '<h3>No consultants to assign</h3>';
					}
					?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			</form>
		</div>
	</div>
</div>

<!--  Change student status -->
<div class="modal fade" id="AddModal-studentStatus" role="dialog" aria-labelledby="myModalLabel-studentStatus">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel-studentStatus">Assign Student Application Status</h4>
			</div>
			<form action="{{ url(config('laraadmin.adminRoute') . '/students_status') }}" id="assign-stud-status" method="post">
			{{ csrf_field() }}
			<div class="modal-body">
				<div class="box-body">					
					<input type="hidden" value="" name="student_user_id" id="student_user_id">
					<input type="hidden" value="" name="student_user_email" id="student_user_email">
					<input type="hidden" value="" name="student_user_name" id="student_user_name">
					<input type="hidden" value="" name="student_user_notify" id="student_user_notify">
					<?php $studentstatus = DB::table('studentstatus')->select('student_status')->where('status', 1)->get(); ?>
					<div class="form-group">
						<label for="assigned_to">Select status * :</label>
						<select class="form-control" name="user_status_assigned" required>
							<!-- <option value="">Select status</option>
							<option value="APPROVE">Approve</option>
							<option value="REJECT">Reject</option>
							<option value="Processing">Processing</option>
							<option value="Passport processing">Passport processing</option>
							<option value="IELTS processing">IELTS processing</option> -->
							<option value="">Select status</option>
							<?php
								foreach($studentstatus as $studentstatusData) {
									echo '<option value="'.$studentstatusData->student_status.'">'.$studentstatusData->student_status.'</option>';
								}
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			</form>
		</div>
	</div>
</div>



<!--  Change next follow up -->
<div class="modal fade" id="AddModal-studentFollowup" role="dialog" aria-labelledby="myModalLabel-studentFollowup">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel-studentFollowup">Assign Student Next Follow Up</h4>
			</div>
			<form action="{{ url(config('laraadmin.adminRoute') . '/students_followup') }}" id="assign-stud-followup" method="post">
			{{ csrf_field() }}
			<div class="modal-body">
				<div class="box-body">					
					<input type="hidden" value="" name="st_user_id" id="st_user_id">
					<input type="hidden" value="" name="st_user_email" id="st_user_email">
					<input type="hidden" value="" name="st_user_name" id="st_user_name">
					<input type="hidden" value="" name="st_user_notify" id="st_user_notify">
					<?php //$studentFollowDate = DB::table('data_follow_up')->select('follow_date')->where('user_id', 188)->get(); ?>
					<div class="form-group">
						<label for="assigned_to">Date for next follow up * :</label> 
						<!--<input id="follow_date" name="follow_date" type="date" class="datepicker" required> -->
						<input id="follow_date" name="follow_date" type="date" class="" required>
					</div>
					<div class="form-group">
						<label for="assigned_to">Time for next follow up * :</label> 
						<!-- <input id="follow_time" name="follow_time" type="text" class="timepicker" required> -->
						<input id="follow_time" name="follow_time" type="time" class="" required>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			</form>
		</div>
	</div>
</div>



<div class="modal fade" id="universityUserModal" role="dialog" aria-labelledby="universityModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="universityModalLabel">Assign a University user</h4>
			</div>
			<form action="{{ url(config('laraadmin.adminRoute') . '/students_unv_assign') }}" id="assign-univerUser-form" method="post">
			{{ csrf_field() }}
			<div class="modal-body">
				<div class="box-body">					
					<?php
					if(count($universityAgents) > 0) {
					?>
						<input type="hidden" value="" name="assign_user_id" id="assign_unv_user_id">
						<input type="hidden" value="" name="assign_email" id="assign_unv_email">


						<input type="hidden" value="" name="stud_namee" id="stud_namee">
						<input type="hidden" value="" name="staff_namee" id="staff_namee">
						<input type="hidden" value="" name="staff_phonee" id="staff_phonee">
						<input type="hidden" value="" name="stud_notifyy" id="stud_notifyy">

						<div class="form-group">
							<label for="assigned_to">Select University user * :</label>
							<select class="form-control stud-assign-unv-select" name="assigned_to" required>
								<option value="">Select</option>
								<?php
								foreach($universityAgents as $unv) {
								?>
									<option value="<?php echo $unv->id; ?>"> <?php echo $unv->name; ?></option>
								<?php
								}
								?>
							</select>
						</div>
					<?php
					} else {
						echo '<h3>No University user to assign</h3>';
					}
					?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			</form>
		</div>
	</div>
</div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>


<!-- Search -->
<script src="{{ asset('la-assets/stdentsform/js/main.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<!-- Search -->


<!-- Export to excel -->
<script src="{{ asset('la-assets/js/tableexports/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('la-assets/js/tableexports/jszip.min.js') }}"></script>
<script src="{{ asset('la-assets/js/tableexports/pdfmake.min.js') }}"></script>
<script src="{{ asset('la-assets/js/tableexports/vfs_fonts.js') }}"></script>
<script src="{{ asset('la-assets/js/tableexports/buttons.html5.min.js') }}"></script>
<!-- End Export to excel -->


<script>

var logo ="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAcsAAAB5CAYAAABWdwqNAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFxEAABcRAcom8z8AAFpqSURBVHhe7X0HeFxXnb1g2V1gYekkTqQpkmzZo3l1imS5kaWFpSwt9P6HJLRACGGBQAwklISlJQQIuyRAqAkQ0hPL0syoushW77Ll3i2nkuIk8z/nvvvGb0YjayTZluTc833nm/Leu+3de8/93VqkoKCgoKCg8AzEUHn5v7Ybxot7QrEzO8u14sGFxtmdmlbcoeuv3BKJvKgnFPoXeauCgoKCgsLpi0TRquc0L4os3lBhvWXDIuPzGyrMH7QtMf64abFx7+bFRmrzErO1I2S2dVZa7WRX2GrvrjQ39mhmS49mJXs1855u3fh9v25eNWDYnx4wzTd0RbXSm4uK/kl6oaCgoKCgMH/QtiDy/BZYh+vKjHPWlZuXr1to3L1hobGzrcK4f2OF8ejGxeZT7YutdO8SK91faaf7BPE9bKcHSM3hEDisR9JbjEh6qxVJj5iR9JCBa6b15IBh/WPQso8MWtboiGXfutW2Lx2ORmu22faC0VWrniuDoqCgoKCgMHeQCIVe0FiqL28u1S9qKTP+0lqm79200ExvWgQutNKbKkh8Jxeb6XZwMz+XmGlYlOmOSjMNizLdRYatdDcIizLdC/bpoGGlYVGmB8EhE7TsNEQyvdWOpEcjkfT2KBnFb3s7ft+0NWqfvyUSiQ+de+6/yiAqKCgoKCjMDhpLjMqmoHZZS6lxL4TyQDsFEtxYbqbXLzQEN0AwN0Ik2yQpluOF0hQimRFKsJciKYWyX1iUDmFRCrEcpmDaYMROQxjTWyiYsWh6bzyW3lMVS2+LRnZtr4reMRqLfWFvPB6UQVZQUFBQUDj5uH1B5PlNAeN1jUH91uZSYy+EMr2p3Eqvh0C2lhvpdWBGKCmcHqFs8wilEEuIJOlalCSFklalK5SuVSmEUlqVrmVJsRRCCW6FWI6SMYpmJL0bgnlwaTy9Ix59emc8smtXdeSm3dWRZaqbVkFBQUHhpGFNiXVWMqB/qMmvNzUHzfS6UoctZUa6GWyBSHrFkkKZZVVKocwVy8m6X/spllIoBfNYlRRKIZYQyW1CLKPpHVUOd4F7l8bS+yGcO6ujT+9bGr9nT030HbtXRV4uo6agoKCgoDAz3F0e//dUSfjCxgBEMgAxhFA2BSGQJEVSkkK53iuUXqsSAjnOqgzlF0phVUqLso8WJccqPValEErSI5TCogRdodwuhXJnHKyOpndXx9K7IZh7wEPL4uk9NbGnD9TEavfVxN6/o7r4eTKqCgoKCgoKU8PqoqJnpwLafzX49eZGv/akEEmIJYWyqRRCSbpi6VqUHrGkUAqxhEC645SuWLpjlVndr65QesYqvd2v7lhl1jhlTvfrNgolBDIjlLQsc8RSWJk1sfQDy6vSh5bHHsX3NQdXxP9DRltBQUFBQWFycE1kyqfZKZ9+W5PfSG9klysEkmK5vtThBrIcYijZBnHcTC5y2F5hpTsWW+lOcgmsRsmekJXuhUD2Qhz7QC4X6dcghFwqosNqhDgKQhy3mBBDC2IIjnLWK7gd3AFhJHdGIYYQx90khHEPuLcqlt4HYST3QxQPLI2LccuDNfH0IfAwrEryyHJwRTz94Mqq9D9eBdHE98Mr4jccWh4PpYuKniWTQkFBQUFBYTwSRUXPSfi01yT82poGn96X8utdYCfZ6Dc6YVl2wqoUhFUpCKuyE1ZlJ6xKwQ3gxopjbAM3k0uMznawI+SwM2R2dlaand1gD9irgTpoOOw3bMFBcMh0CBHthFUJRgRhVXbCquyEVdm5DYRV2bnDZXW0Exalw6WxTliVgrAsO2FNCh5YFus8uAJcHusaWxHvg4X5t0Or4tVKMBUUFBQUJgR3xGn0aaVJf+WSRHGoPFHm0hJsIYs93yfgpnwM5SeszWxaxzic8z0vqx3u8BL/O9+rx3F/Fq3y/avwuQqf+L4HluXYOZZfiaWCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCQn6MVllL9lXHbtgRjcbkXwoKCgoKCgoudsbt/9hdHe04sqwqvbMq1n1geWyRvKSgoKCgMB/RVhp5EXfjWesLhcgGl+VmNn1maJ2k9/91uQyZoc1TYA9pOhw0DLPbNGNeDpBRh1tgpR2PtOIyXHqMezzcsRKfXuK/g/g8tCxefXhpNJw+77x/kkkzLeyMRz60qyq2m3vNcmP2w/jcuzRWuzMeL5a3KCgoKCjMJyT8xotTPu32Zr9+KAmmwIaAfqiRDBqCTQGw1DjULNlSZhxqBdeVO9wAblwILjIOtYGbKoxDmyXbl4AhsNI41FlpOgybh7rAbs081AP26qBhHuoXtMf6TfvIAGnZRwbBYTLicCQaObIF3Cq5LRY9sp2MR4/sAHdWOYRYHdlV7XAPuTR2ZB9ZAy6LHTlALo8dObg8fuQQeGRl/OHDK2IdR1ZFXjXdbezSkcg/b4tHL4f/j3GDdp5iwhNM4G/6yLJ4el9N5Ib0ST5U+u7y8n+97wz93+YLb35F6AUtxdXPO1FbBzL+bQsiz8/n11wgt4qUQc1CYtWq5+S7f77yepQFGbUZgenCg+Tz+THbZN5lfpNBzQLzM/M178n37FwnyxCiMeMyyXQYmoN1UgLvJV00BaPo5lDoX+rP1q5t9pvpRr8h2BTQ0zyPsjlgpltKzXSryzIzvQ5cXy5PEuHxWguNdNtCK91WYaU3u1zsnCTSscQ5SaQrZKW7K51zKHvDdrpXcyhOEtHt9CDPoZRnUQ4bkfSQFUkPgyO2Q3GiSEQerxWJOsdrSe6IR9I747E0hDENUUzvxictOR6vtQ/WHLlf8iDEimdUQhh5ckh6jFwZTx9ZWZV+7Jzq9MGV8eZDK6rjMmmmjC1VVWdsj9m/2lMVTUOcs4/7qomlIc7C7wPL41+Vj5wUoMHzBTR41qARdO98YMpv1Nb79Wsbys1XyChMGWt9ZigV0N+dDOpXJAP6H/D9Lrh5Xz7/ZpPJgHEPe2dksLOwNmi+Nt8z849Id59xd73PeKOM2pSRWBB5eSKovaYhaFyMvHx9vd+4fS6+z1TAqEV5u4QnMMmgZ9D2ksiLkBa/YP7O9+zcpn5fym/+mY0UGZ2p4ll1gUojEbA+CHeuSvqNP+X3Z/aId7c25auskeGdHMli/WONPv1RZMi0y0YIJZk5uNlzHiUPbh53ePMieR5lzpmUmcObKz1nUspzKXvkuZQTnkkJZg5w9p5L6Z5JSboHOMtzKcXZlNU8lzJHqIRwOmdT7l8O0QIPrnDoCuahFbHfHlo1/S7SHdFoGP7ft786Pu4QaQql8FuIpeAjB2uib5GPnnCgcP6mtdTC++Ph23Of6xFWCFx7bWnIJ6NQMJDpzURAvw6FcZBurQtayLP5/ZkLbAQbfMYyGfwsIC4fzffMfCPTHxVROlmsfUpGrWA0vazihalg+DMNAaMODYtH1+N9tkg38/k12xR512/e1JbHil77yvAZ9QGjn/fke3Yuk+mN9L+/qaLihTI6BSMVsFaiPN8Ijrbi/ZFz8f1tLLXTdX797TLYxwcys1Zfoh+kNekKZQOsSlcoJxLL3MObN7hCKcXSK5SuWHZ6hJIWZtYBzhDKfoolBDJXLEfICQ9wdsQyI5Sg9wBn18KkWAmxhFg5YgkrE0L5wKqq9OHlsUfxe/XuyLRbUEWjdvyc7bFI7/6lcVi4Uqw9/nv9pr8PCnGO9xxcFVksnTihQCa9gZUyMvu8oCiYfn1jqqyyREZhUrQVRf65PogWfUDf7RZGUUHPcSYCxtMpn5m3NVsfMD+c75n5RvEe/PrRRIl+oYxaQagP6FXJgNaIOuhJvlMIZl735xKZ79DI+XU+sWw+Q38lrnVJ4ZlXlGXp4FTE8u7y8n9P+rTv4NmDfH8UpFx35xIZRryft8rgTwy24BIl4bpmCCUydkYox4mlRyjJcUI5mVUZolBKqxIi6bUqe6VQOmI5uVVJoRRiCZHMtSp34tMRS4hUjlWXsexcocQnBevwitjOA8sj75VJMmWsLip69vZI5EOwKsfY7coweMU6y6r0iDQPkH7inGp8Rn8504lE+XC6i2ViUeTluP8mxnE+xZNUYjkeLEewzt4HcTw0Xxo9LpVYOkiVRUtw/x2Ma2NgfsS3ILHkgCsy8leafPoTqRyxzAglRJL0CmXGqoRQumJJoRRiCYF0rUpXLCmUuVZlRihzrUo5ZikIkfRalUIsXaGUYglLzhFLCOQOCiVEytv96Vp13i5QCha7QR+AUB5YHlu/f3kkb3dYIegJhV6w1ba/vjMWfYzjpce6gBEGVyjBLKuS/kMoadHevzK+e2xZ/EOcECSdPGE4ncVybbFxNuJ3azMy+nyqVF0qscwG66KUT/9/yK9H5rolko9KLIuKGksjvkRAr2d3ea47c5kFiWWtf4mVKNH25VqV48YqCxDK3O7X8UIJq1JalKL7FRQWZUYoYVF6rcocoRzX/Qq6E3xE96u0KCcXSk7oqUofgVgdXBa95fCKaMFdfrkYiCx6OcT717ti0Sd3xaVQgl6rln67QinGSIVQwqKFUI6tiPfvX149baGeDKerWHJ2XtJv/O98FUpSiWU2kn7tDah75qVQks90sWyqqHkhG6+cK5DrxlznpGLJ2a/JEv2WFo9Qkl6hFBZlzjilVyynPKlHimVW96tXLHOEckqTeibqfvWKJYSKE3nE+OSK+PfSSAOZHFNGn64vQjhSu+ivGwbh/zGxzhVqiiVn396/soqfa3asssqlcycFp6tYJvz6J5E/n56vQkkqsTyGxNlacdJnjjbNk267fHwmi6XTfW5cxobOfCyTk4plvU9/R4MPliQFUoiknlkq0gKhJFshlK0USdAdo9wAcpkIl4u0QSgpkptJKZIdJESyk4RV2Q2h7Km0IZDOMpE+LhUBB9ylIhDHIZBLRMQyEXALl4mAoyQtyGhUUFiRYGZSj0eksib1gOOEEmIlrMnlsV0HlkU/JpNhWtgSjb5mRzTStxv+sxuYQn0sDB6hhr/e7lexTGVl7KlDy6M/f2ip/krp3EnD6SiWnCkLsdzTMI8rVlKJpQN2v9YHjJ/P14rW5TNZLNcGwzru2z6f6hovjyuWiVDoBcjAa1M+/f6ET9/HygeZeg+Ecw9EU7ApCJY6bC5zuI4sdwjh3LNhkYcVxp5N4OYlDtvJkLGnA+wkK4093SFzV3elsaUrbIz0aqDuENblSL/hcAAcBIdJyxoZsUl7ZCRqj2yN2EOjEXsAYvk4RTMjlLTuJuh+pVhxXeORlex2jXXtWR5dJZNhWoBQfmx7xN5LoRTdwFlCOfE45Rgn9CyPPbZ/RezLO6qrnyedO6k4HcUSefbK+VoovVRi6WCtz7Y5k3k+W5XkM1UsxbwXv/ENCk7us/OFxxVLzrxMnVVZQjaVWGetkeT3vKwojJsmYd9ie0FXOHzGeFZlcYvLKvczfEZfLPayQcM4G5bc1bAqj1KcsoXymFh6rTp38wFYk/fsik5/fLInFPoXCOW3tkUij9FP73KVXLHOtWgfWFnF8dG9B5fZ75POnRIgA5zydZYzmQEnKhOf3t44wTpLxOfFiYDZQ3/yPZ9LFvTc8J1sFrrM4USJJf3LF46TyULjWIhYJv3aZVOZEML8lS9MJ4uF5ucTKZb5wjFblGGecJ2lWCYS0IenkifY2M3n12xxSuss5wNgjb10a9z+2bZo5MlMtydFip8TWJWHxU49sUf3Lov+8GBNzXEHqI+H0VjszBHL+p3oCpbjpeOtSvgN5na/PkihXB5th2W5Ujp3XHRXVpb1GNZ/b4lEXiT/mjaSQeP7yMTbUWFtOyUMGFsSfn0XhSC3kBTCVmTcer9xx/rFi18mo5CFVInxOhTMI4VUYJnCi3AhTEMpSYRtK36fnDQJ6KPgQ7lhyccTZln6jQeEv/nCczIIv5B+8DNPWHI4mViyAoaI3FbopBBRcfuNg+67JPH/yLgwniiKuBr78a6eyg1LLk+cWCK9/MYOpHH+MJ16bk8GzHb2RsroZCHl0+z88RjPY+VW33VK8+wkbA6YO5BXXyejNL+x1TSNrRG7dk885kzqgTi5IpURSleopFjRojxQEx3bUxO5kFahdGrKGIlGwyMROynGSj1CebxxSpJdvxyj3L8seufBc6IV0rnjoidkmL262cox3D7DnvYWYS7u8+nBZLEZS5RY0ZPNep8WaQiEVyV82r0Qqkkrl1yyGy4R0FsSpXpYBn8cIC5fbITbohI+DlOOW2N1gfBnGba6EqMyUayHybUo3LUnKU1YcdQH9FsLGU89EWJJf1IB/ff0N194TgZFHEu03xZiSUwmlmvONhcm/JrYcSnf814yrninv6/3Wa923yVZFzCNfOE8ERRx9WmfRhzun+ydngixZJpCJPfUFWsr84VnNijqD79h3jzB3qnIx58oJC84QydCJC9ivm/02ZF8/s0G1waNOOL4Yhml+YstlvXmrZHI0N4qRyhdkRJC5RFKV6xo0R1eXpXetzQ6vK86+hrpzLQwEou9eti2B3e7QsnJRsfpfnXFkkLNJSL7l0d/cf9y7SXSueOiR7f/o08zR7bICU/9hl0rL80LdJyh/xsnajQGjX9MJmZe8l4xXuU3UpPOgg0aP3G2xMvvlktWSnD3UvnYKQX8/3khlu+JEstkUPu+fOyUgbuznAixbEJFBevwocnc4hIhfLZu9NkL5KOnDA3F4VUI48HJ3umJEst6nz7KXanko3MeiaD+vckaEk59oD+S8Olvko8pnEhwd/otEesLo1H7IS7PcDcfyAgl6F2mQXKbOXa97quJ3ntw6fS3kLv5vPP+acS2zwcP7YzGji1ZyWtVIgzSf1q1Y8KijT20d1nsy5xSLZ2cEBwg7wmZH+7TrAMjwqK0xCzhfsN6pM80ZzQZ6VSBi5FRIO6hheAUjMLIe/kMWtN3JvyhM6VzEwIVzv9NJpZ0E9bWk2wNy8dOKSAOvzylYhnQfygfO2WA+F11IsQyWaKtQCNp0iVAfOdIr+vkY6cUjT7tNYjDoVMllhCUbTwFQz4654H397+TpQ2vI163yUcUTiS22fYCCNXP2PVJkaRIZXW/0qITQgWRkmLJyTSwKp/aXRO77sHly6d9agUy+/OHLOvbI7b12DYIpHcjhMkm9VCoD9bERvctixU0kYcFq1OzvgahfGgIAkmhJLk5A09YGTCtX8pb5yxQkVhJn75+soogH9k1g0rkT5xcJp07LgoVS4TpiVqESz52SqHE8hgnE8t6WG10pxCxRDx/Kh87pVBieXzUB5ztJvPFx6VoEAeMH8hHFE4URnQ9DKGs3UVxlNacEErXmpMi5bXoeLDy3qXRB/cstS8ZOvfcvGfKFYKeWOzMQTNyk1jf6dmHdpxQkh6h5BmVtCghlJv3L4ssl84dFx26/m/dmnkthPJJrjl1hdLdnGErxdKwBvr02CL5yJxD0m+fU+/Xh6cqlKwcpUX565bi0Eulc5NCiWU2lViefCixPD6UWM4Shi3r3C0Re5SCSKvO6fYc3/3qChXHCMeWVfFzy+6ayH9JZ6aFgbClD5l2ageEcqvch3bc1nquYINCKEGekynEuiZyz+5lkYKOlmpfaJzdGzb+MgiRHMiz5d+xjRrspwdt+xPysTmFuqB5HsRujAUhXwGZiKzgUSk8xcpPHipbMJRYZlOJ5cmHEsvjQ4nlKUZLdfHzhmz7c1sjkYcojK5A5VqVQqhcoQQPQKR2VUVSO+NxQzo1LfTo1rlDlj24DUIJqzazvV6WVZlj2bpCua8m9sTupdFfFLo0pbvSNLrDZsMQhDKzibwUy9y9cXdGoulh2/7jqdrEoBBwZjEq84tSfuPhQkTBywYUmsaAcTTl07/HE8qlkwVDiWU2Z0ss+f6UWGZTiWX+OJEUy/qgfrV8RGG66Fu8+GXDEev60aj9xPZo1BEoiFPe7ld8cpxy39J4ek8Nu0CjN26vqTlLOjUtwJo7f8SKHGDXq9iLNkco3XC4YXAnFe0XXb+xh3bVRC9Nr1r1XOnccdGpWa/uDJt9Q3ok6wzPfELJvXG3QrhhbR8aiJyc8y6nioR/1XNhTbKifHSyApJLp1LUn0LhupzuSCenBCWW2ZwtsaSfjt/5w+VSiaWDZ7pYcq/xRFC/Lek33oa0fPtcI3vJCp03MWvgZuQjtpVklysX/HsFyu1+zV1TeYAHK1dHH91VE7lstECRyocWWGsDhvXtEct+bGuOUGaOAcsVbJA7BdGixed+iObbOJtVOnlcdIWN93dWmvtcoczeRD77wGsKpThxBWLJbfUgmu+Szswa+itqXpjyGb9BJflUIRWll6xoILJPoCL44s1FRdM+t1OJZTZnTyy1nxfSWBKVv3jv5sflo1lQYpnN01UsSZEPAsbD+D7niPfyyNoS/c0ySnMPg4bxxq0Re3gnRdJryYEZofRalRApjg1CoHbsWRp/p3RmWuC2ef2W9Qdu1k6RdIVJCGVu96trVYK0KhmGXVXRTTur7Crp3HHBcyq7l5hf7Axbj/eFuaG8c+LKhFalFMst4A6EYThid4zErKh0blZQF7D8sArv5nrIySq2XIpn/MZDKCyflM5NG0osszlbYlkf0P5YyFg1K3+E79GGoPEB+WgWlFhm83QWS75jxm8ucj3X8cLqlVGaO+BOEBCES7ZY1kHOdnUtOVcoM9acFCmuaaRIcQ3l7qpo686qqoJEaiL06lELllwDZ5tyEo0QSpKTeuB/lnUrxZoUYaBQVkf/vrPKXCidOy7aDePF7UvMH3SHracpkt0QyUKEkuFh2gzb1n39WmG7/5ws1AUqDWT21skKej6yMk/59QdY+UvnZgQlltmcNbH0GXcXJpZi+8KH6krz77upxDKbTIvTVSznMic9oms2QPEY0s1fbDXtJ9j1OpJjyWW6Xz0itbvKWcMIa+7m7dbM+pX7Ko3X9enmFq5hpFBybDC3+9UbDnfzAU4m2kPBrIpeu73AHXm4aXxnyPyjsCY12znwelz3K4TSI5aOhQv/OdEoYt80ZJrTXi96IoBKexUqu+HpnHzubF9njOH7CetCVmKZzdkQSw47wO/WQvKEkw764Qaf9mr5eBaUWGZTieXscM6JZa9hVA5bVu12CIG7NMMVSq9YCotSitQ+CCXE8uHd1dEr7i4vn/b6SWbgHs34RL9pPUyhzLLiPLNfRRi84WAYIJQQyQdh1X4OFcWkO/IQmyqtJV1hs7EfQkmRdA+8nmz2K8dOt1r2oyOW9W3uYCSdmxUk/Np7kYH2y63GpkSnUtD31pVob5HOnRAosczmbIglJ0IkAnrfZBU/yUoU8dzdELR0+XgWlFhmU4nl7HBOieWQbb9p2I70iaUZ0pITApUjlN4xQorUnnh018549IPSmWlhS2nkRb26+b1+w3psyMwWyomtSme8VAhldXR0W5X1DuncpGivMF7VVWn10qKkSJITWpUIhyuWFEqkzRFYk5/iEWrSuVMONgjqg/olqYB5ZKprKElnn1d9e8Jnzmhf3nxQYpnN2RDLxlJ9OeK4r5AKUuaf4ebS/AedK7HMphLL2eGcEMv06tXP7jfNz42Y1qHcNYyuVelac0KgQE6koUhBJLu2LY3mrUwKxfpQ7EyI0y0Qp6fZ7ert7hRCKSf15LNsuSsPLMqNe1dUx6Vzk6I9ZJ7XGTJ2C6GszBFKz1ilO/vVFW5nNq61d8CY3RfGgl7n169K+c3HC6nwcykqR78xnCjRC9rFaKpQYpnN2RDLhF+/sMFvPDmZwJGOOJjtE82AVmKZTSWWs8NZF8ueUOilg4Z1HaylJ91Zp7li6QqlK1K7qpzNBvD91j2W5ZdOTQt9uh7u08z1wxCjiSbRjLduOfM2JsU68rfRqqqAdO64SK9a9ZzNS8zPQSwf7fUIpdv9mrEqIZLe7lcK+DY7mh60rG6I5VLp3KzgTt/yl6BCuoEZ36mE82esicgxLFSknbXFlTHp5AmHEstsnmqx5HhlfcD42WTvwKVjWWq3ysfHQYllNpVYzg5nVSw5PjloWve56xezxCnXqpTdr7sgUnuqYo9tj9lXDcXj/y6dmha6dftNEMphnuJBK87t8hRiiXB4u1+94aBQ7opHj+6I2T9Epi/o8OV2v/Hi9pDx/c5K86nuSjO/UGZZlY54u0I5YFn3DZlmSDo3K0BGCfDQ5WZUCIVYDLlsccY1WxuC4bxjUycKSiyzearFsjlg+evRIJqs0nfp5CX9cvn4OCixzOb8FEvjD614P2wYzSUWUgZdzppYDpjmGyAK/aOcSFOgUO6GSO2oihzaWWV/gl230qlpoSusf7ZPsw8MQ5Ay4iSFMrf71TtOycX/O+LR+0fjkYsLHTOE9Xxme4Xxh86Q9TTFsdMjlKL7Fcw3TsmwUMjxeVP/DGf4zhSoCKx6n9HSMoXM5aVTSejJZGlhy2lmAiWW2TzVYgk/39UUMAvqgiUZx2RJ+LXy8XFQYpnNeSqW30n59R7Gb64Q76sD6TmCPFNQXj3lYsmJIYOG9ZkBw7qfaxhzxclrybliSaHkjNft8Wjfrhke1Mz9Uzsr7f/p06zHuUE5xYmkFedalCI8OWFhOGDRcl3jth2xWMELUzeW22WbFxuN3SFakyaE8phV6QolrUpXKN2wjDhp89SAbl2NgjWljcRPNBJBVgTGADNLvkx0PDITUmAb/PqdPM9SOnlSgQytxNLDUymW3KIQfiXEBK48YcmlfA/31561+GXSiXFQYpnN+SiWbaWRF6V89oKEP3bm3GHozLoS6y1oxD9YyJDSKRXLzab5in7duk5YTO7SDClOrlUpBMojlFx0T5HaGovcvSWmz+gYqiEtXtytWTcPaHa6P7fL07HgxlmVGaGERbk1Ym/YZtu2dG5StC3Sl3csNnp7Q7AmIZYdrlBKscwap8yExRJnVA6a1gP9pn2RdGrWUOc334cMsn+yAjwRRder37ilkEObTxSUWGbz1Iql8ZHUFPKKI6r6nYlVq54jnRgHJZbZnI9iOVeR8GvVEMsH5pRYDhtGZY9u3TeSsywjI055ul95qPNo1D66NR65DhZhwecZ5gPEKdoRtpoHtezNyXPHKYVwu1YlwsC9aHmwNITyr6OGUdBEHqKtwjyvY7E12gOhbF8yXijd2a8iLAiHS6YPwrNzwLLeLZ2aFXAHpfqAflEqYD7Ewpkv8xyPrNhkRXjT+sWxCa2GkwElltk8VWK5NhjW6/369qlM5KAw1AeM86UTeaHEMptKLE8c6n3Gsjklln1h87W9ujm8xSOUme5OrzhRLKVQcmwQnw+Mxu3P87gn6dS00F1pvRmW3cggLMrc8cGMUIJZQomwOGIdeWprJPIj/C5oIg+xeaF+EYTyiBBKaVFmxDJXKD0W5ahIH6u73zBeJZ2aFXBjh4TPvLLRbzw+nRlsLMxN/A5BaKqoKOhIshMJJZbZPBVimSizyiF6jZw0kS8M+cij2PC5N1Gsh6UzeaHEMptKLE8c5oxYcncZCMFneg3rQWkxjbfiSI9VKYQSIrU1ao2MRqNvkE5NC+za6ajQP9sVMh/oh0XpitREVqXXwt1JoYxY92+17Ys4FV46eVxwbHHzYvNq8Mku16KUYinGKuG/Vyxd0RZdr2aUBzwnumb5mK17i0MvpdjwuBwWynyZ5nhkpkMl+DQr55nspjQTTEUs6wLmjM44nS5OJ7FMlllL4X4bK/nJBM1LZwxcv3GyfDJFsbxWPnZKwa368E4PFiKWyYB540zFst6nj07nrFeFbCT91tJ6v/7QrIrl5nLzFRCD6/p18yjFKEuYwHxWpViWQaG07fod0ehxW5uTgfvLbg6ZV9OaozXpTqYRyzMgTsKqRJiyhFuGRQhlNDIMwS54G7bWYPiMjYv033UshjhCJIVQyu7XcValGw4hlPCf4dHsP/WEYqdsXC8f6sory+r9xu2TFdaJyMoMBfnJuoD+7UTRxGNQJxtJv/nLQsQSfKrRr1XLx04pUNndWkg6Uyyb/IYpH8vCVMQS7pzwE+hRcZh1fu2bKb8xqUWVS9EQ8xsP1Qe0/5LOTQi2/iGCk85YXId3jnj+ikMI8tFThmTQenMioD8yWaXr7JWr//TmPDPp7/RpXMPcXohYJn36XmVZzhxo4HwM6fn0ZHmLFI274Ak+oqtD1xd1h43aISFIORbcBEIpzqrk2KBt/2p/bGai0VphBGDR/Zk75HRXejYnl0IpdsZxw+QRSh4BBpGGaNvrh6cwkWdTyCrfWGHUdi9xrEmXGYsyRyhd61aIpG491acbPx6Kl89ozehMwe7I+oDeItdCTpkpUUno/0j4w1+WTs4a6oPG99cVEI+mgLBEbpzJ2ZnTQaLEiqJRsnMygWEBTvj1x9dU5F82VLhY8lP/33t89oJUWWXJdNlYZlQiTK9LBbRLIUp/hdtbWLGzFyLXz8nI5yCU97QtiEw605uiDCE6PFl68TrSdUdtsXnSNrzIh5bi4ufBWryhkMYP82WdT/86HhvXW0WRT4kyOLk7eA9PpiY4/1OhMDT5QybKxRanfExOsaFK6QncdaxDt87t0a0Bjk9mFvp7xdIVJ1IKJQVqS8T+x9aI9aWZjk9uKNet9iXGxj6KkxQpr0Dldr+6wj1KyxaCjfD8bUtV+Azp3KTYXGHWbKywOrpDdnozrMqMUOaZ/eoVbe4Y1Kebj/Tq5he5s490blaQLAsvReUnKr5CWlj5yLFNVGj1qNzf0+C33gNr4P0niw0+4wPw59yJKtr6gHEBWoxHC4zL0wm/dnsK7tI6SAast5BoOLw7n99TJc+/c92k+yiY/w0/RwqxxGT4d/79ZfnHfQsVSxLvBg0ZYz/S7cB0CXcOgw/R2mUreyrjk17KeD1a6L7APCe1EIuLlA2D0YTPuNRN90akO4//yvd+pkr48S7v+2S+QePsNljXBeU3pltdycQn7CR95s2FxhPv80Hk3Z8kS5HHPHl3tog8cu71RUXjupeJRKkVFWk1y0z6zfeJz4D2HXB3oXlYDmUcbvCFZr4xDPvgu8LmRRDKMbHQP48wuULpWpUQyPROCOVINLJt1LZnfKjmhgr9zRCqnRTHcQLF8Mgw5Vq522jZRniKR+T7EOuCuzbWlWvv3LzI3N4Fi5JCmSuWYvOBnHCQI0iTfs3ahfC8Rzo1q0Am//V0Tg3JR2YqCsHJpOhu8xmbae3IKGQhWVwZg1ju4735wuglK7hc9082C23JsiCjMvzTRMsqpiKWbjz5fmbGjNhNm2yh1/vNX8poTArOfUCj47fi8N087uWS4cxN85PNQtKEFjgaLQdSQWPCfaRRFi9kY6QQ92Yj705EZ+a7eWSiCX0Jn35dS8DK++xskfkkX7rmo2jA+I3k3WfO8DhE7u/arZnX9Gnm0X4pSOOEUgrTMaGMQChj/N4y0xP+WZlsCumf7VpiHqZQCosOn65VKYRShikzTinDwzMht0Yi949EIhdOxardsFj71KYKc6x9sZXe5BHKrAk9Uigz4aBFyTTRrZ4eXf8P6dSsAy0msddrvkwyF+lkXH3jRGLJyhWVTtN0LZ+5wqagka7za++T0RqHqYjlXCHfHQSjq7Y0NKUNKigiEJsnnO7+/G7PdYoJIgHtzqYJegqIVr8RQFwfLLRBNVcoxf3gRGJZ79evpUDlPjdfKIZ1fMZ3Cj1+MS94Qn9XpXnPkA5riTvi5AhTvu5Xdney23OLbf9hm20vkE5NC+yK27zE/FFnyHqsB+LknUyTr/vVFUqK9rYIrFrb3jFimgXPuuXMvXWL9CshlI93QCjbpFAiDDlieUwoaVVSKEUaaWZLVzg8qzNec3G6iSWR8BsfnUrLca5RtmS76wJLJjwoYL6JpWy8jDUE9TfJKBSMe8+qLKkP6L0iXfK4PdcpLHK/8WQiqH9SRmlC4L3/upBxy7nE01ksnbpRP4jytkpGZ+rorrRf311pDnLbOIqB14Jzxyq9VhwFajtEEpbcw1tt++sJv/+50qlpoS0U8bUvNv7OiTXuDjlu16d3fDBXvBkWhmPIspqGooWvs0ssWvTyDYvMG9ulQFIovValEMscq5Lh6NPYiGAa2X/dbC6fmRl/EnA6iqWYdOE318/HypUVK6woVKzGZ2R08mI+iSUrSojF4wm/OalYTIRkQPvcdCYTzQUyH0IwmmtLJ1+vjbQyUCbHCt0ycC7wdBZLOYP5RhmVqSHN8cmQ9RlYcmM85V9Yb6QUSyFKUpi8ViUFCtbkLlhzE3YtFQqIVgxCtZ5CKZZq5BEpr1XpFUp2vQ5b1u+3TuF4rw0VWsX6RfpdnbQmKxyhdK3KjFDmmdQjhDJsHoWV++MuTXuJdG5O4XQUSyLl0/8DleuB+RQ3UlSsAf2u9Ysn3i+VmC9iSfEHH+Vh4TLo08KO4urnJQL63fOte10IiV9/sA75UUbluOCs2IRf/3JTwDg6X3pHTlexZJjrAvpoS5lVLqNSOLh+sXOJeS2spMf78+yI05cjlK5FuQMCNWJZm0YikWXSqWmjtUJ7y8ZFxo7unIX/XqEUFqUMkzt2KvejfXLQNq/qr6mZcNwgFxsX6lUbFpptnYvtjFC6FqUrlrlhcIWyt9J8Atb3F0dnaEWfTJyuYsnxhYTf+Dwq6mkdUj0bpDWRCBiDieLJC+dcF0tWoPJdHUr4jE/IYM8I9/rDi9k9PV+6KdlQQPl6CpzSPs8UnfpA+I88Ds9xY27zdBRLOSHrEZSzSdcCj8Nm01zYVWmu5cHFPYLZ1pvXgnO7X0cgkttoydn2X6ZiyeUDJ+BAJC+GYD3kXdPoiBQoRSoj4EIoESaEg0eB4fPIkG5dWOiOPMS6hcYbYcXu4kQeIZRSLPMJpdey5Wbt3WHzYI9mvlc6NWdxuoolwXedCIS/zi7AuWyRsLJx1rjq/VxXKIN/XMxlseQ7EpVjUG+tC2grZZBPCOoDS6rwPrs4K1pW0nOSTANYho/BSvyKDPqUQOFJ+rW/sAE11y3M000s+e64NGdtifYRGYXC0cHxyZDZT2uSYpA1JpgjTK5YbhHWpP3YsGlftVef2dZMtGg3LDKuaaswjnbKpRr5LLp8k3rkUWCDg4b9RuncpOBC9ZYy7eMbFhr3b1rkEUowY1W6Yp0Ry2NCic/BTs16tXRuTuN0FksXqHQ+hRb67uluunAyScuBFkR9QK+tC1QWvP3eXBRLNkiE1ec3BxN+7RuNvpMz9MB0YnrRr7loeTGfQUD2JgIzs6ibKmpeiPJ5bcpnPDqXxzBPJ7HkrOWUXx9BHfN2GfwCkS56VkfI+mx3pbWf45MTCaVrVboW5VYI5ZBl7xs0rAumYsnlw4bKaAksytto3eWz6Lzdr+OEkpuTG1ZyKhN5uPRgXbm+etNC/ZHNEMqNeYSS/ueGgZsgsOu1u9Js7QqHdencnAfFkpUcM/x8ICtItNbbpiKWRKJEX47n7+ARUm5rPdftU0VW8Kw8hLXrNw7X+/TVUz3GjGKZz+1TRcaBaejGg/8hLjxQ90uJ0uNvjn4i0Fwq9lL9Biq2g64lyzDlhvNUkWmRETS/eUct8psM6ozANeyJYv09cLezCRX5bMczH2WD5dDxxJJpk+/ZuUDx7lgWObEuYPyeJ+jIoBeGdeXxf28P6deh8n+CW8dRkEjXesta6C+7X4fAUTsKwbR6+m37HOnUtLFxoREHN3YvsfPOPPWOEXoFnII9zM3bDfuPPaHCKyEI5b+vX6j9Cn4+RYtywyLzmFjCbxEG6b8rlrQoeyo5kYdpZP61s1wrls7NC0B4fu10GbkV4NxmqxAYfdNUxZK4+RWhF9QHjTcmfNpdqNj3o6HwD3cReD6/TjTpD/w8Cr8fRMEcTvnN7yX9lUtk8KYECMVH8vlxMkkhQHo9Bb8fZzcV4rEPnxvwPq5OBrUVtWed2uPYiLqAViHSMaAPO+nq7N6UL/wng8w/8PNRlKMDyYB2Z8Knv+lkbHKeWBR5OeJ1ASz29fBzDHF9nO8jX5hONeUs5cMTiWWdT7/OHX+dK2QeYV7Gu3skEdD3JH3GH5NlxjlTXqXRtSiyuHOJeW8vRMjdNo4U1hs4bgINOGRFxDZug5Z99zZdD0qnpo2N5eY7Ny4yt7kzUL1C5YhU/u5XCjasyX/06+b38u3uPxHW+fTgujL97s0UyIVmegNIoRRi6Qol6BVKCjbHb3srrSfbK81rtkzhKK+5AgjHO1HQr6oPGN+dD0wG9atTQePTqKRfLKMwLaR8ixdwI++kP3wJKvsrkAbfy+ffiaW+GpXK+Tzt4ObQeTPa2rHep0Xy+3FyiMrl2wj/5VzCURfU3pfw29UzfQcnEpzTwDBRUFABrq47Be8Twngl8s4lSIe33oP8JINy0sEGQiJofKDeb37VeS/5w3eqyLKDcHxtohNkkGe4Hd6cqmNQ7q9gXm7w6+dOe7igu0J/PYSym/urTrRl2zGhdMYphzkuaNhH8f2avtjMWpfp8877p9aF+iUcL2yvcBb+e4XqeN2vFOsBw9oP4Z7aRJ4yLdJapm/cBIFcv9DICGVu92vuOCUbEj2V1qP4/RV230rnFBQUFBROV1Bc2hYbn25fbB52hDKnm1OKkrAqIZKuVblFTKCxHuwz7E+nZ7gROrt+N5RrP4FoHd0MoXStOiFUoCtSImwIj9fS5ZmZ/bo10mcYr5POFYSmcuN1LaX6jnZYlK5Q5u1+BYVQkvCfs4K7QuahzpAx9RlTCgoKCgrzDx26/m9tFfq1sJye5gn/jiBNblVypiksuSEI5ZQEKh9Si/Tg+nL9tnaIFUXKFavjWZUUSm6IwOPAenWreSBc+OHJ6SJYsKXGR1vL9SObyy0hlBmxdIUyx383DJzshHAMd4Ssc6VzCgoKCgqnO9rLjMrNi40HeIJGriB5rUohlKCwKMUxXHZtv2Zr0plpo7mssmZdud7Wztmn+bpAXaFywwVSwDNbyenm79oN42zp3KRI+Fc9t6XUXL2u3HyoDf6tL88WynFWpfSf5Dhue8ho3RSe2QbwCgoKCgrzDOsXx17WETJqOaNzMqEcMGnJ2Tw94/ot4aqCz36cCK1B/R2tZfpOdoNmjRVKofJadO5aRgol13z2auaTfbp5BbtvpXOTglZ0c6n5y3VlxlH6lyWUx+l+7aRQwv+OJcZtEOaAdE5BQUFB4ZmEzSHzikEI0LjuV1IKpTi02LAe7tfNSyY6Z69Q3BwK/UtTMPwFCOWjwrqjYEGsXIvSHat0hdIr4gOwJrk3bVdY/9iUJvKUx4vXlWq3byyXIimF0vV7nEVLwv+uEPwNmU91VOrXT0WYFRQUFBROM2yq1N8MQXqQ1lOWUNKqBEccodw6GJ75Qc0N5eYrmkuNayhWG3IsO69QZsQSQulO6qGgd1daA13hyGulcwWhuXyJ1VKqN7dBKFvhJ+m1KjdOYFVyD1pYlQ+3L9G+sXom55kpKCgoKMx/tC2KvBwWXNuAFhGi5O1+5aHFEMrGftuOyNunjaYSu6y5zLh9g7DupEVJsfIIlWvRuTNPae1ymYbYc7XSTHUsntoOIc1l+uvh5yAtypY8Qplr0WYLpXFg02Lj/JnuRKSgoKCgcJqgK2T9qJeCJIWS3Z2DEMpezfrNxhke1ExwPWNTqd6T2w3q7QL1jhO2i3FKZ5kGwvV0j2b/pkPXXymdKwhNpdpHYMUepEXZUuYI5TrptxDKHKF2xbonZEOs9VGIdsGHQysoKCgoPAMAIarqChmPcN0iD3XuN+yH+3T969wdQ94yLdAqaw5q72wpNfZSKMd1gXrEKiOU0qrkpKPesPWPrrC9eujccwte+M8wt/jNrzaX6o/SgqVQkq5Qeq3KXP/F8V+Ljbb2UGEnQCgoKCgoPIMAUXt2d8hMjRgRzjbd1qFbH5SXpo3rI5F/bizVv9xaqh9h12tGsHK7QCFStCq9Qsmt5LoqrQMQ8I9K5wpCW2nkRQ0B/dqWoPl0qxTJfN2vueOUFMou+r9Yv6u73C6Tzp2WWBPUtLqA/vV6n/GduhLjdXz38pLYTD7hD7815dOvrA+Ylyf8xqvkJYE6v/be2tKQT/4s4nZR9QH93dzcWv5VlPDHzkwGzfdxQlSd33xffcD473puleY3Lsb3L60NGvFEcag8WaK9hZO9+AzDkAhqr6kvMb4FP7/JfTa93d91Pv2DvC5/CjDsiEPWOanp1aufXe/T31HPrcgC+hfr/PpXuCesvMzwlnKrK8Tve7j2yaYS6yx5qShRZpUn/Nrn6wLapfVB/Qs8RDrfWDXCZ9YFjEvpPr5/HvF/vXd7xaagvaIhYK6SP0WaJgPGu5JB/QrE6+v1pcfCzD01G/z6e7j/J9ORaUU3HepfYRjWBsNnIFzv5Q5XfAb3VTGNUkwn+X7SRat5hudbuW9u7VmLX8at9RJnxzN7FfPZBr/x0bqSykzebg5YfoTns0mf8d2Uz/h0qix63D13EyXhaL1fuywRML6dxHv9rdwHlXkAafFB+J+1BR7CHhT5heHxGeeLTeNLtG9wS7q1Ps0WJ2v4zU8xHVMg88Pd5ccaxQ1BS4dfX0v6tO8gvTPruZEP3pn06Zdn3PNrmR4g5qNapEmtz3wNTxCSfwtwm0D483VuVXe8A5lbioufh3C/D2n4nWTAXM29b+WlDBpRBlJB4zO457tw67ONpZFMmbj3rMoS5PfzvVupJcq1Yvj7QXfv2CbkIbzH1SmfeWVDqSZOKeLESbj5xmSpuZC/ibaiyD/z3fP5lM9egLCc591aTmx9hzKFsvo55hm8n6/ieybvJV4RegHzRd3Zx061YblCHn9LypOmBPMa8lxm/Tj9QXq9O+HD+0ZY3bxGIG6vSaH8yZ/i3mSp/va6MqNS/pVBfSC8Kuk3vpXya99AXKr5X7oo/SyE+wK+R3A1P1PIEy3F1S9FPfFRxgl11BdZluk20wZuvK0poFUIRyWc+LFOivgYnqT/2MEZLcWhlyaC5sf5jpgu3mt1gSV+/P8lJw+xHGmfx7sRdVidz6xB2WK++lbSH14qHshFb6X1wf6wubkvbOW/YQq4j8s0yvRftJYaR9eXOeOFuUIpxAoi5e1+7RQL/y1+9uO/KU3kaSk2zm4K6jevLz1mTZJeocyIpSuUYDv87nDCcAOs0pdK505LNAZMHnG0AbwWmeH7yEAjKASiQdJwpvkKFJbfIeMmIDZX1PuN7+P3hmSJfrUrXPjd4a24uBk43NoIcYzJv3CPVo3M3seMj+8fQeb/JgpbPwToNorXGlS6ohAG9LtZedzoX/Vc+PlDVKitFDFcuzIR0Jrg7o1/RsVFN1FQNuL+IYqd8ARAZv85KrVvyp8ComD59bXw9y9SeL6JAiYOcYUgvBEFfzP++zUq6a8mgvqfIaStruCy0DE9GPdEwLwS7jQhjb7Ka1447uoduHYZ4oR7jbvw3x2Js52N9OH+j+HOtfxOocP3vyPsDQ1Bpqd5PcLRx8qDm2OwEkQ82+vLtAhPmEB4LkO8WuF+Eu58I4FKn+KKtOmkcCOdX4trbXDnKp7owDTBfW+lSMDN2mSJ+VpWKPhvB+L6e4aBcNJF66n3hUXDge7QX/h1C/gdVCa34x10yAp03Bg93Ps83gnT7id1EKiET7sXldtdjLPTwNI3pALah+XtAkwnhPGW5jOX+HG9C2Lxe9FYEJWQbjWUmyHkpZ0UL4TtG3C/DvnkpzczXYr1MP1DnK7BvVcjjUdSQe3jdBf33JkowTuGW06Fdkws7xZ52NiE5wYTCxa9XP5dVItKD25sRvx+gAryh0i3EQqNvJwB3xeu3w2xW0OB4HtEHHr4vpiG4p6S8Gvh93r891u483WE/Xe4Z4NbLpifGrlJPypg/iZ48g2e2UABbUVjJ4X7kSY/TvpM5Ht9uA4iePuCyPNxz9/rS/R3y8eKEqHQC+BPB8T1nEQp3TDWIX6ZRkmiNPxR/Pc1uNWBsNSzrNX7jh0RuBbP4T2MoQzfIv8SYpkMGr9F3HaiwZkRN7jzY8T5Bn5vdRpof6GbjLsonwGtG/n9ago43Hszns+cAJQosaJ4ttNbDxCizPE9osGDPPwzhH0gWRr+T17D8wPw80/IhxBiCqlhOtSH4Nd3KGTIK2uYzmz44P9fIH1+IRyWSJWZNbjelvLFg3QL/l/C/xuC+iLUX41w7/aEL/x1hOH/8GwfwvMZJ/7maxHeUcYL93wTja6L2ahjeiFvb673m1fz3cPtYTRy3yw884Kt4xF96ZTGBfOBFRpE626KlmvdZcYLc8VKCiUn9WSEstKo3RwyQ9K5gtCKVij8bNpAoSw10s0ef3OtSu+kng5uxrDYfGRjBSuv7Jbo6QhkuC+jAPxN/kSGtd7OFrKzGbV+FXjbfR4rMXm2uRCZrguZTZzPh+uduWIJ99q8hQQZnxttD7rWFis/VA630boQNwBOy127R1QQsPDgRnOtRwjXBqvOkJldiCFaywl8H0MG/6m7dAnPXIeCdjm/u+DJAcjg9yZKrffIvwRo5aAwDeD5DzE8/I89HwgDWuXaRlq4EJL3o4DcJ5cIPQvX3oLfvU0vyz5RAf6iktZ/KRsQz6Lgo9DdiIr4FgoawvljIWRFaA2jkOLarTeVl2eWHTWgMkVF0NqASlOIKUQL7yAuLyNNtWsRpu/In0UNaJmjkHfxOxobrDAyIojW9AWoDDkJjWK59phY6m0Iw4FUSaWoeKVYQgz113NGOq734rkLMmkpWu7aV+H2bayg+Z8LNCz+E3HuqQ/YVTLORbcvWMD39juk6+8Zz1QQFnxAu4MWLq/jPbwYlsHaWqTpvWjhI72aci00WnpwowPpJSr/huDiRYjDQG1xZUw0ZtCYEjcCCPt764Pmp/gdfv4NbonvuaBwIhy9zH+wpN8q/6bA/gj5KSMYuA6RcfK0C2md/wZ59c/Iu893/it6VqLMeBUEsLUhGNZpfSMfrYMYXsB48x5+1iM8SM+NtNaTxWYMDZCHEP5NzbJcwIKsQdq2rBdWNqypoIn638FaX/gDLAOwjl6Ee/6+Fo0meUmIJcLUeUws9fVuenmBfPY7pN235E8Bhh1p8V3EpwFlr99tzIk4+fXrcf9D9T7zRpQH0bsjGhH4n/GBP9ch/H/znibSVGpF8R7WUVCEG2j84r4v8lqDX/9fhOsn+JrV0MJ7vwXv5Br5k+/hIpTZd4vnkT5JNDzkJQGUW/rR41rl7PlBmEYoxqlSWL4Brafd21hAg6PeH/6l+O4z/kqxZL2D/9cgnX/k7bmiVc53wCPx6C/LS+5pOQjbDfA/I8holH6SPT/y54lFEwpEc9Bo2whrslmKVkYohWBljxW6ViWFkmyv1H891Yk8yPSvgZ8DFGd8Cj+FWMI/r99CKD1C3bGY45PmwU0V1gWrZSE/3QGhexcy8AAq5/ez0pR/iyU9qEw6WdnIvzLA/V9A4b9DFCK0zguxLFFwBrxiCTf+7hVLVn7IuHetfWXVGSg0axPSavACbrwVmX49xQif9yEM30VlfxetUl6ncE4klim/dSG7YdzCACviYhYgcZMHooIKGLDUjFeJMMH9m2V3p0grFE5uZiFulsD9bMlfzwIv/xINRLgxyE9Yh99HuH7Irmbct2mtz7blbRmI7iOKDFrmwuLJFstrEI5vy59CLGlZ8jvfBa71UcjdCoVgWiPewrJEA2gJ0qqOXXP4bGQjhCKOirO9EfGESJ2PCiYlH82A8WFFxIpF/iUOQ0dlez3icZX8K4O1vlAIfnY3BiqNe/wVATaqGs82RXdfskRbgTRopbhIgaC1/J6WhcbZG332AiEwFMuAscnNJ0g/E3HrTZbrVspnvQPubUUF/o7EgkjGQiTw3m8FL19bbJzN9HMtPgJp99u6QJgW51fRWMgIEhsG+D2ENPkvb7p5kSi2ytmgavRYWy6YT0SDg8MBPoNWf5ZgobH5AoQpUV9svJ8VO643JAPm5RQL0SjzW0tZUdNvvIcPI236aUF6K2t2S+OecZYl3JrQsnSB/HATu+XlTwExHIJGylpYRrh2G9JWCJvoofDpN+C/nyB9b035nLIHt38Acfgp8wveFUTZFl2mXtwKv9vQwOX3pD/8n3C/tsFvnQu323K7SAmE9ZuIazsbaT3I8/JvsdZeWJxovIouardXBmKJ99TlzpXhO4F1iXygRZxnzCbWXby2Dg1Qlh00rESdRbFMBKzP1qIuwvvvTSw61rPggunPPI0wvR55t7HRZ0caF4R87ntAo/RS+l+HBmLTy2ryHjt2QsBKrClg7KR1R6F0u0Fdi5LkWKFr1QmhhEUpJtWEjMdgUX7TbdEVikZYKMhI+yiUTR6L0rUq83W/kpudA6W3t1Xor5dOPSPAjI6CchFbVcioqMDCP2VGFRUPKmGOE8lbM0DBORf3NjuntxttJ1IspVhs8rrpogUFBdZa3724B5VdbUPA+ITTraO30PoVhT1HLFnIKBSIHy2W2xH2XwsryKf/GN+vl7dlAe6t4fgljyxDPPegoN0k6NPbUQBFt428VSCfWA6Vn/uvcGcLxyrh1lXw//vCmkCl444Fwq1PpPzaGhT+O2BV3oVwfqmp3C5j/CcTSzzbxfE8VpR8DgU6Bbeb2K1YW1r6IooP/Dwmln4jJY5z8plX4tlfsVwhzdto3eG5qyDov6XbtS+JvAhW/i9ANDBgKbAx4hnP4/aQCM+dqKw+Lf/KgF11eC+t9BM/nwU//4JnRYUMP76HCvzn/M53BTd6WLGBa3Dt18xzFCXEZx/yxu/Ef35jHb5fzbSkMOH5zyE89ajQW5CmIp/SPYjo7RQ+pMM9cO8uWnz8X3Z593Dsa43T49HrdhOywYX8/UW80wT8asbnD9l7wWsuUgFtJfwaON65hnj2K4jnb3PHstmljrxzE4TwcywL8LupJVT9UqQHwmd+cg0tJsSDjVJaawjnF5DfEDejtS6o/YT/T9QNi3SZlliuYVmBaLPcsosc7/yexKoiWsOwLI3fNMB6Z9oxnyOcQYTlu4jbNShTVYjHBu/YaT4wnegO3sNOvJMsq9YFxQn3cGy9AXmgkd3EbHiw7CCN+pJBlDE0hCGcf6V7fHe4dwT+/1HkCSe+P8k0qNiDAYHn92Sp8TanDnPeF+79K+LII/zeyrS++byJewobfNqrEee9uBfliI0f/cuspxg2NKhWI381oLw04/OK2sUzO00rC4xIIzxrDOiPrKNoSesuV7CEZSeF0h2r7KVQLrEOwKr8cG6ldDw4lYN+Cfx9XPhJofSKJfw8Zs0e634VQllhMRxCKLs07SVcZ7oZmXWzmZ9DJ5B7vFw+Ne5duvSVu1dFXp6WXWfTgZvG7GoUGROVIyvTujMtPyrxbm9F6QIZ6UJkqHtFmucIm+yG3VBfdmwd7po83bB4V3nFcr3T+k3CjXETyjhexwr2Tha4gLEWgvIZ8X/A/CnCRGtnQssSluRnOIbNLhdhVQXFAP+d8rYMOJkDBXU9Koo3wq13o/Cx8CK++jaE8RvytiwgLOPEcj38gbW2hWNAKGAcY/vBGogWw8/xN97DCRqo0PXas/RFSMc/ovL47X3sHi5QLP9cXC3GbwlWQuzGFWnnd8ZxEO+MWCL9GzhuczfyNeJXz8oS7q7jZAtUNmws1fIZtrJrcd89ZUblWiGkxnZUOJkJUeyexX+/gT9XyL8yqCuxyyCW3algRIQdafh+5ic2vPDMOloT/J/xRr5qSfrM83iNosa8VOvXLaTVcGOJ/iGkexvyxx+YV/iMm7acsAe/ed8a5Jdf4/9no8EEC8L4Grvo1oCuZYmGwwXMd7CozHth7UKM+mDhfo7XAOGeyEulYVh+eirp037ufYfMyxDlYQqH/CsDp3dj1XM5YQR+rG1DA0xeEnC6uRFGf/ijshu2lflfTFKilcbJQHhX3rMv4c6LaYUybiC7MV+Md3NbnU87T95SdLto5BgdnLQzVbHkuCTu/UvCH1nMCT3wfyctXF7D/78Bxf0I2zfQUPolBO8a/PcT5hvc24Y8Nm5tvTOhZlWmMSHHLnfyHcu/8oKWGxsjzOsoi0JYaVkiPh9wulqdckpLj412vLuPI49SyP7k1iOEtHq7xJgkGido/F0mLzmWJdK5rqyyBvmw9+95DqRmQ0X0akjLEu6brCdYnnA5kxfE5CCkN0R7I9z9tjefTBscFIVK/xwWZboFIimE0rUqXcHyiJXXquxZIpZp9LZXGudI5woCx0BQYH7QDD/Z7Sr89Aj08axKx28LFq6xu73C6NhE/8klIhy9sG57u/gZxifYDfZqRm+fbgn2k4bdOyA5CA6DIxYYsXu3RCJZ3BqN9G4nqxzuAHeCu6qivbuqwaXR3t3gXrIm2rtPcj94YBm4Itp7cEVM8MjKqsGxlbFfzOSgaVQor0Nm+6T8KcYOWXEwM6Py+BkqpN+zhSsvi4oOGWsjMpWYrEAxqQtYK8VFgBndyXThzPgQ7v0oCtB6tzvzeGJ5s2g5i9b6vd7uNrbwUGgg5Nr/8HcyYCbg90X8zpmjeKYJ73wYlfV/8z8XQixhcTTkjFlylh78HGQhkX8JoJK4EH5sFuOOSAukz909RaF/QcXzEVRgG7yzZV2wokZ8fip/ul2VnHAjunnhx4/gl2gNQ9h/h0oIFeGxCgaFMExxYPcYZxFTUAvphnWEi13FRmaszelqNRMszBTALLH0OeP+cJvW+Cb4uYstalY4cK8fFfM7hCMStb7wRXB/Aydmyb8ExOxTP3sPtMzBCazYEI4fwJ+/dyDt+B++ByjM9b7wD3H/Wk7c4v9iFifyCMWdv11wohfjTosZ189BOo0wbXiN3aVea7YBYWCc2LjBvbd404AQaY38hHyxAe/hZvh/M+IHq0O7V4iWz3wnKr3MM+z2x71rvRVxOs005Bib/ktOYpF/431QrI1b1kL4OJsY/m9C/SO6Al3Arffgvk6O8ad8lTUUS7ccIdyfxzU0vvS22xcsejm7DSns4kEAjYzzkWcSztg3J58da5g0Q+jg9xArdMfyNVrdLkovcsXSseb1bSxD9UFTpAfe/zCs3q8yryBMtOTF/RQGhPcvMHZ2I4w/dnpn9BuRxr/K6uI+s3IJ3LmFvRPyryJOPsN9mzihU/6VBbh3IcKQsZTh7uXIi78X+VXUK5VZs+2dblitm2FoEG5rg5w9Ly8LIwnx/A3e5a/gdlNLWahcXhJiifB9gXUA0qwB4fqavCTAWcfIG7c0n4G0cSbKrUmEVmWNz8tG5fvkT5b1S8G/sCtd/jU9sI8aL+mOdUFYdhAur3VHsXIFy2vVuRN6uh2hrN20sHKJdK4grDtbK27wa39qhp+k8FMKpRBL6a+36zfLohVi6cyA5Ukr3PuVOwS5W/zJTRDEqSbcsN3dnGHQtNND4LAZEQdej4BbbIejZCSS3hYlo+kdsWPcGY+md8Vj6T3VsfRukJ97l8bS+5fG0/trHB4EDyyLpw+Bh5eDK+LpMfDISof3r6xKP/SqKnzGHz+8MpZ5kdMBMycy1DAq9x+jsFyMjNAiW2fPgkD5UGjvQia+E4X2S/jOWWpNuDezLy4y2AAy8P+JZ0sgNMi8aAlfit9tHKNhAcQ9XUl/+GO8n5BieWcK98u/UEGEaYWsQWX1fLbqcP0mVHb1yOSXwd2vMCPD8v0rLQfen/CbCMex2YUUgFa8fxTyL8m/BKRlWYsW5riZjvDjYwhfJwrbj9nF57TmtQ1csiCuB2DhwGpgwef4CsOA/36U2+WGyuerSBtYoEw/7Wtoff4V96ZoqYvrfvNapN/P+N3pCtXqcf1OUVGU6FchnuvZWoUoPU+M1wSMLq+Q4Pp1CNf35M+iRryzRInRx2UxyRLjXXBrB8J+RSpgfhHfYXUYF4jKx2cmOH2eIgn3W9yxN4afAoz7HsWnM76D9MH3bqYBJ9MgbVB5cobn+DFrUYmLZRR6K+7j8hf6+2e8v2a3C5QQFjzSC0LwRMpniu5YglYD7u8CfyGWD6FCQyMhXHc2Z2Yb3UhHYSnh3f8GafFXpgstYKTBDvh3Nd870rMVlpKwIvCO7sAzt4Ofh1uXcKYp8kkUYR9sOkurYFqQtJgRxr5aWFP1pRYqSmMnfl+JvIkKVW/j8iC65wXFDu7WIQ5/RZz/G3HlrOhNDDvTQdwDC7m+hF3KfE9iiQ97Ojrd7lOE7xw8u9Ed26OFg+vIS9oRYdlwWEPMuja+z/CDbIxezHtFY9ZndMEPNLhEI7IRv6/jNdzzKvw3xGVfdajEea871wLv5g+wiK/kdwL3fRJsGkWDhWnB/5BfPowwNzkzXSGWnvsZ5sagcRif/yd+i3QQM47/js8v4/9vofGxEf7cwMaqeAgQ3frIv7mTwlzgmf+H5/EetctQj3zZ6fINi4aGsCAhyMwTJBtKa8tsG3moH+Hj2Dkn2f0MZf8+bwMhWRp+G9L3aaTh72S3sgDS6bZUwL6U37n8A+m6AcbATUwrhJMz+1Ev6VeI+oizwf3GsJg9zDzkMz/udBnr70G+24rG2uVsiCPem3DPlJYvjsPaEm0FIt+REcoc6y63+9W16topkkusp9srtN90OaZvwaDp3YjKu5X+ST9dS9brb77uV1coM0tVuKZSMrP/bL69cbmRvDzwGpZkegCCSeEU4mlBPMER207D2hOEJZkeJWMOt1M0IZg7qqRwVkeFaApCNCmce2ti6X3g/mXg8lj6AAQTlmT6kCDEE4L58KuqIaKxW2fSBUuIDBisjLFgIUP+hJmDlZO8LFqZbF3VB41rkMn+pw5C4r0Okfo0CsxqFDrOCP0CK3C2PnkfRQiZ/wcoBK+HPxmBoZ/0p7HsWLeOsK7QguOyA/EbhQ1WD9fP/Qjus6J4D61LcTOADPwRFKKMoNCaQ/g/4m3lEqKLBYWRa0nlX1mQy1a41g4Vnf5ljm3JS0W0HFDgPuCGieGFHxd71/4RXIaAwgaxooVpXIZ0goCFzpSXEVbrXE4QkD+L2BXKxgMK5bVoNX8fFtOrXYuG440o+Oeze1LcDFCwvN3hnGGJsnYhLUuma6qssqYehR+Nkx8mA9ZbGGf+34Cwo8CX0kKv84c/yqVA0omi9WfFXoa4fJ5rSeVfEPJKI+U3vwL/fwZx+m+WL3lpHJyWvvXqRFCMx17DMd6Up0vRBd6H1oDGxJ2e9KBopILhz+C9wqowvibyTrluMZzw94LMmBMawhCwS9aUVIgGUgssClhw32a+Yhq7+RCV53sR99UUT77LpL/yHMTNRJg+5s7GJVgpotL7MLs6+RtW2TKk4/fw3n8M4fgvr9XkBcUkFdT/H/Lbtbx/7dnaG3LvFT0V9Jv3+LSvuV3tBMISaERYvNYprzf49M/S2hSNirM5Lmh8l2Gp91nv8MzafFYTLW4RTvOnCZ+VWZvJfMD0YTo2iPjrb3bLGd7J28BMDx0tcU7AkT8FaG3CTfHe+CzCmbHqRE8Byhzzk/yriO+H5Q5i8ROUi/9BmXmTt9eJ4PpS5t981i7B8AkrDs8j3D/Ee3ydyMew4pkn8H5QlxhfQ31zWdNCw1z7Sgq5/kn2IPD5u5knIGY0koSDAPNyAxrJ7nt1gXC8qw5Wvfwp3gO7ZfGefso1zqwrOLbMaxz+YD6V7/ByxO2zLC9Mf866ZSMNYftRstR8g/tupgVOpUUG2d8CwWqUzLLuwCzBklZlpzP79EFYlpezspPOFQRGtNFvDOYK5fEm9QihlELtWrWuZZsRypAjlkIovWIJoeRuRq5Y8sBrr1AOeoRyJCLFEiLpFcttEMrtUiiFWIK7PEK5Rwolud8jlgdJKZQkLEpam4ceXFk1JStcQUFB4XQARUx+nR/gAvGkT/tSQ0B7hILVENCzrLsswYJYuUJJi47LNGDN7elYon9wqhFPlejvhlDu5Jhoo9dPcgKh9Iq0a1UKi5J0hVJala5IeoXSPZrMsSphUcquWFcoaVV6hZIiOU4oaVVKi1LQtSo9QiksSklhUWasSseiHFsVTz/giKXoXlBQUFBQmMMQg8A+/RdNfv2oK5S0KHNFK9MNKi07LhWhUEKoBtpCkeXSuYJR59cuhiV7P4XS62fu7Ff6m2tVTnT0ltei9Ha/klndr1Iss7pfpVDmWpUZoZRiSaHc7rEovUKZVyxF96tjUXrF8h+vqk6PrYjes+U1r5n2pB4FBQUFhVMAzgRK+PTbmgNmutGvC9HKa1V6BIti1VbB3XEsfBprN4WOzVoqBJyiXOfTfpD06083+Y8JpRBLiqQUaCGW0t/cST1eq9Ltfs1YlRmxHG9V9nqEsl8KpSuWFMpcq5IcJ5Ru9ystSnxmxipdoZRiOVH3K4XywVVVEMrYrgdWxcctDlZQUFBQmHt4Vr1PXy2EUorlZFblxoUQycXm0xsqzF92hcNZi38nAwf4YcX+gX7RzxQ/XaF0/ZzEqsyd1COEUlqVQixzrMqCJvXkWJWuUHqtym3upB7XqvR2v0qxzCeUuZN6xiCWR1bGnzyyIjZuZxsFBQUFhTkKzoJK+bSb10OoMqLlESxad65YtUGoIFZPQLS+wqUB0omCwKn29T6jkZOHaFGKyTwgrdqWIPd9NdOtZJmZXifJE014/NdGWrMU6UVWehOsWm4+sFksUXEs3E5xoDPEEQLJ5SI9lTYEkseA2eI4sD4eTQa6AsmlIkOWs1RELBexIYxyqQgFkstEMmOTpByb3FUVcyby4JMCuXdp3KFcKkJyqchBEiIpLEmIJAVybGUVRLIq/TCsysMrYz9Nr85etqCgoKCgMMfBnS0aA/p6bmeX1f0KuhZlO4QKn/vbKrIX7RaCBl8olCjRkw0+/X5YkwdTfu1gQ4DUD8O/MRJW5RgEegx+CraC8NvhQmNs/UJzDEI9tpGsMMdgWY5tkoRlOQarcqw9ZI5BOMc6Kx32kGFrrJfUrTFYlGP9IERzDKLp0LLHYFWODdtgxB4bAWFVCm4lo5GxbSCE88iOqsgDO2KRB3bic2d15IFd1dEHdi2NPrAb3FPjcB+5LPoArMoHDiyPCsKyFHxoVdUDh1fG73hwVfaemAoKCgoK8wQQripYejvWc6N0KZRuF2gHLEp8drZWZJ+BWCicjX+tJWu4ILbYKm8pc8g1eVzwnfTrVjNZfowbXC7xfM9hB4nr46hns9fDoYkYdRkVHPVwVyxm7ohGYztisZW74/EVGdY4PJDLlQ5hUWYRwrmKW9vJZFFQUFBQmI9oKtPeAovuMAXT7X7dDKGEdXdP26LwYnmbgoKCgoLCMxutC7WPtJZpD29eZKU3CMHUr+8JZe8pqaCgoKCg8IxHa7n+ybaFxo71C42vuZtmKygoKCgoKORgnWe/TwUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBYVnHNa+MnxGsrjyU/U+7UtJv/6FBr9xcS6bgmC5w5YcbiQXHeOmxQ7b8zFkXNwJ9mjGxb2V5ud6devCHsO6gOz30rIuGMzhSEQybl2wVTBywdZI5ILtJL7vrI5ncY/LZQ73eXgQPLQ89tn7V8XPO1hT8UKZFAoKCgoKCvnRUlz9vKRfe0PSp3fwBBEe5MzP4x7zRbrnUnqO2/Ie4OwetUXmHrXVK4/a6tXNJ/t082i/bh0dMKyjg6RpHR0CRyzQJu2jWyL20a3gKBm1j26LRo5ui0WOQiSP7iSrIkd3VUWP7qp2uAfcuzR6dB9ZEz26f1ns6IFl0aMHl8eePCyO2Kp64tDyaN3hlbGl6Ujkn2VSKCgoKCgoHB8Jv/HipL/ysoaAtnVD0EyvL+WxW8dOLvEKJbfQ45FbrlBuyhHKDkn3TEqKZJZQgrAgBQcN57itYZJnUYJbbDsNcQTlOZTgdjIWEcdtOUdtRdO7yepoeo84asvhPpBnUR4ADy5zDm3mEVsPrapKP7iKx27F+g6tiH/m6XPP/VcZdQUFBQUFhakh4Y8sbgjqVzcH9e1t5Va61SOWwqIk3UOcIZIbaVVCJN1DnCeyKsncA5wplrkHOAvBhFCS4gBniKR7ePNo7uHNICzJ7IObSffwZiGUcQilOJOy/9CK2NcOr4iWyKgqKCgoKCjMDC1BTWss1a9sCmr7KZDCkvQIJa1KIZQeq3KzK5Sh/ELpdr86QnnsIGcK5RBEkhyBVXlcoYRVmU8oM2IpLEqI43IKJA9yjm09WBP/0oHlsUUyagoKCgoKCicW60OhM1sWape2lmntG8rNhzsWWeIQaXecMnes0hVKtws2a6ySQukVS9eilGIprEqIpRBKcKtHKLdRLF2hxKcjlhBJKZa0JMeWV6WPgPh+/75l0XUQyE/sqFanrSgoKCgonCK0LVjw/NaF+tvXLzRvgFXZsxki2bPETncssSbufpXjlXm7XyGQ47pfPVYlhVKIpSuUsCozYklyrFKIZDx9/7JqfI8+ub8m2ravJnbd/qWx16eL1EkrCgoKCgqziA0BrWL9IuNdbRX6tbAs+9j92hOCMJIUxko7a1IPhVJYlRBJkt2vrkVJocztfs1YlFIsM5N6YEXuqXK6Ww8sjbMb9uldVdFNEM2r9i2Nv3VsqeWXQVRQUFBQUJgb4DmZHaX6KzdpdqR9sfH5jpD+N1iVo12V5v2dYesxiuWgDgE0I4Ij8nOLJWlDGMFtJESSM153RqPp3RDGvXGIIoRxNz63xaNP74hHHoVgHtkejwzDsvzDnqro+fuqbW0gEnm5DI6CgoKCgsL8QKKo6Dl9ur6oQ7fe0RU2v9ITNv63R7fu6tPNhn7D2tSv230DhjUyaFrbh0x795Bp7YFVuXvYtrbjc2QkGukdjUQ2gsnRmH3bjqj9M4jlpduro2/eG48HVxcVPVt6paCgoKCgcHphR3H180ZDsTNHTHPhoGkag0YkPmAYywZ0fflIxFi2JRqNDViWPlxtlY8sXfrKoXK1DlJBQUFBQUFBQUFBQWGGKCr6/7xM69aXozVSAAAAAElFTkSuQmCC";
        
    var today = new Date();
	var dd = String(today.getDate()).padStart(2, '0');
	var mm = String(today.getMonth() + 1).padStart(2, '0'); 
	var yyyy = today.getFullYear();
	today = mm + '/' + dd + '/' + yyyy;






var bsurl = $('body').attr("bsurl");
var fm_dropzone_main = null;
var cntFiles = null;


$('[name="from_date"]').change(function () { 
		document.getElementById("to_date").setAttribute("min", $(this).val());
    });

$('[name="to_date"]').change(function () { 
		document.getElementById("from_date").setAttribute("max", $(this).val());
    });


$(function () {
	$("#mx-sudents-lists").DataTable({
		processing: true,
        serverSide: false,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/report_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		columnDefs: [ { orderable: false, targets: [-1] }],

		dom: 'Bfrtip',
        buttons: [
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { 
            	extend: 'pdfHtml5', 
            	footer: true,
            	title: 'Student Report',        	
				customize: function (doc) {
			        doc.pageMargins = [20,20,20,20];
			        doc.defaultStyle.fontSize = 7;
			        doc.styles.tableHeader.fontSize = 7;
			        doc.styles.title.fontSize = 9;
			        doc.content[0].text = doc.content[0].text.trim();
			        // Create a footer
			        doc['footer']=(function(page, pages) {
			            return {
			                columns: [
			                    'Zodocs. Designed by LabGex Innovations',
			                    {
			                        alignment: 'right',
			                        text: ['page ', { text: page.toString() },  ' of ', { text: pages.toString() }]
			                    }
			                ],
			                margin: [10, 0]
			            }
			        });
			        doc['header']=(function(page, pages) {
			            return {
			                columns: [
			                	{
			                        image: logo,
			                        width: 20,
			                        height: 20
			                    },
			                    {
			                        alignment: 'right',
			                        text: ['Created on: ', { text: today }]
			                    }
			                ],
			                margin: [10, 10]
			            }
			        });
			        var objLayout = {};
			        doc.content[1].layout = objLayout;
			    }

        	}
        ]

	});
	
	


	/* Search by Agent, Country, Intake */
	$('body').on('click', '.searchbtn', function() { 
		$("#mx-sudents-lists").dataTable().fnDestroy();
		var from_date = document.getElementById("from_date").value; 
		var to_date = document.getElementById("to_date").value; 
		var agent_id = document.getElementById("agent_id").value;
		var course_country = document.getElementById("course_country").value;
		var entry_date = document.getElementById("entry_date").value;
		var course_university_new = document.getElementById("course_university_new").value;
		var leadFrom = $.trim($("#lead_from").val());
		var std_status = document.getElementById("std_status").value;
		var std_counselor = document.getElementById("std_counselor").value;
		var std_docstaff = document.getElementById("std_docstaff").value;
		var dt = "#mx-sudents-lists";
		$(dt).DataTable({
		processing: true,
        serverSide: false,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "ajax": {
        'type': 'GET',
        'url': "{{ url(config('laraadmin.adminRoute') . '/report_dt_ajax') }}",
        'data': {
        	   "from_date": from_date,
           	   "to_date": to_date,
			   "agent_id"  : agent_id,
			   "course_country"	: course_country,
			   "entry_date"	: entry_date,
			   "course_university_new" : course_university_new,
			   "leadFrom": leadFrom,
			   "std_status": std_status,
			   "std_counselor": std_counselor,
			   "std_docstaff": std_docstaff
			}
        },
        "aaSorting": [],
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		columnDefs: [ { orderable: false, targets: [-1] }],
		columnDefs: [ { orderable: false, targets: [-1] }]
		});
	});

	$('body').on('click', '.clearbtn', function() {
		$("#from_date").removeAttr("max");
    	$("#to_date").removeAttr("min");
		$("#from_date").val('');
		$("#to_date").val('');
		$("#agent_id").val('');
		$("#course_country").val('');
		$("#entry_date").val('');
		$("#course_university_new").val('');
		$("#std_status").val('');
		$("#std_counselor").val('');
		$("#std_docstaff").val('');
		
		$("#mx-sudents-lists").dataTable().fnDestroy();  
		var dt = "#mx-sudents-lists";
		$(dt).DataTable({
		processing: true,
        serverSide: false,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "ajax": {
        'type': 'GET',
        'url': "{{ url(config('laraadmin.adminRoute') . '/report_dt_ajax') }}",
        },
        "aaSorting": [],
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		columnDefs: [ { orderable: false, targets: [-1] }],
		columnDefs: [ { orderable: false, targets: [-1] }]
		});
	});
	/* End Search by Agent */



});
function loadUploadedFiles() {
    // load folder files
    $.ajax({
        dataType: 'json',
        url: "{{ url(config('laraadmin.adminRoute') . '/uploaded_files') }}",
        success: function ( json ) {
            console.log("loadUploadedFiles",json);
            cntFiles = json.uploads;
            $("ul.files_container").empty();
            if(cntFiles.length) {
                for (var index = 0; index < cntFiles.length; index++) {
                    var element = cntFiles[index];
                    var li = formatFile(element);
                    $("ul.files_container").append(li);
                }
            } else {
                $("ul.files_container").html("<div class='text-center text-danger' style='margin-top:40px;'>No Files</div>");
            }
        }
    });
}
function formatFile(upload) {
    var image = '';
    if($.inArray(upload.extension, ["jpg", "jpeg", "png", "gif", "bmp"]) > -1) {
        image = '<img src="'+bsurl+'/files/'+upload.hash+'/'+upload.name+'?s=130">';
    } else {
        switch (upload.extension) {
            case "pdf":
                image = '<i class="fa fa-file-pdf-o"></i>';
                break;
            default:
                image = '<i class="fa fa-file-text-o"></i>';
                break;
        }
    }
    return '<li><a class="fm_file_sel" data-toggle="tooltip" data-placement="top" title="'+upload.name+'" upload=\''+JSON.stringify(upload)+'\'>'+image+'</a></li>';
}
</script>
@endpush