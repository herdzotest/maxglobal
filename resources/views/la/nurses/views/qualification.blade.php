<div id="tab_2" class="tab-pane " >
                                
                                <div class="content-head">
                                                <h4>QUALIFICATIONS</h4>
                                                <!-- <div class="id">Sutdent ID : <b>#12675</b></div> -->
                                        </div>
                        <div class="pane">
        <div class="academic">
                       <h3>Your Qualification Details </h3>
                                   
                 <div class="mgv-field">
                        <div class="name">
                        Qualification gained
                        </div>
                        <div class="value" id="qualification"></div>
                 </div>
                 
                 <div class="mgv-field">
                        <div class="name">
                                Date awarded
                        </div>
                        <div class="value" id="date_awarded"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                            Students reference number
                        </div>
                        <div class="value" id="student_ref_no"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                            Education institute name
                        </div>
                        <div class="value" id="institution"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                        Did you use this qualification to register in the country/state you trained in?
                        </div>
                        <div class="value" id="qualify_register"></div>
                 </div>
                 
                 <div class="mgv-field">
                        <div class="name"> Qualification certificate </div>
					    <div class="value">
							<span id="certificate_upload_name"></span><br>
							<div class="uploaded-file" id="certificate_upload" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
							<div class="file-approve-buttons">
								<!-- <a href="#" class="btn btn-danger mx-approve-doc" data-type="qualification-file" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
								<a href="#" class="btn btn-success mx-approve-doc" data-type="qualification-file" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a> -->
                                                        <a href="#" class="btn btn-danger mx-approve-doc" data-type="certificate_upload" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
                                                        <a href="#" class="btn btn-success mx-approve-doc" data-type="certificate_upload" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
							</div>  	
					    </div>
                 </div>                 
                 <div class="mgv-field">
                        <div class="name"> Please confirm the name your qualification was awarded in   
                        </div>
                        <div class="value" id="confirm_name"></div>
                 </div>
        </div>
		
        <div class="academic">
                       <h3> You have told us your name is different on your evidence  </h3>
                                   
                 <div class="mgv-field">
                        <div class="name">
                        First name
                        </div>
                        <div class="value" id="different_first_name"></div>
                 </div>
                 
                 <div class="mgv-field">
                        <div class="name">
                            Last name
                        </div>
                        <div class="value" id="different_last_name"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name">
                            Reasons name are different
                        </div>
                        <div class="value" id="reasons"></div>
                 </div>
                 <div class="mgv-field">
                        <div class="name"> Upload evidence </div>
					    <div class="value">
							<div class="uploaded-file" id="evidence_upload" data-type="file"><i class="fa fa-file-pdf-o"></i></div>
							<div class="file-approve-buttons">
								<a href="#" class="btn btn-danger mx-approve-doc" data-type="evidence_upload" data-val="0" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-times"></i> REJECT</a>
								<a href="#" class="btn btn-success mx-approve-doc" data-type="evidence_upload" data-val="1" data-toggle="modal" data-target="#AddModal1"><i class="fa fa-check"></i> APPROVE</a>
							</div>  	
					    </div>
                 </div> 			          
        </div>  
	</div>
</div>
				